describe("Votation Page", () => {
  it("Should visit votation page", () => {
    cy.visit("http://localhost:3001/votaciones");
  });

  it("Should render cards", () => {
    cy.get("div[name=card]").should("be.visible");
  });
  it("Card should render Share Button", () => {
    cy.get("button[name=share]").should(
      "have.css",
      "background-color",
      "rgb(222, 222, 222)"
    );

    cy.get("button[name=share]").first().click();

    cy.get("div[name=cardModal]").invoke("show");

    cy.get("a[name=closeModal]").click();
  });
  it("Cards should render Redirect Button and redirect", () => {
    cy.get("button[name=redirect]").first().click();

    cy.location("pathname").should(
      "eq",
      "/votaciones/2c57b0ba-4348-4778-89a0-08ea8b5dd315"
    );
  });
});
