describe("Test para el componente de Login", () => {
  it("deberia de navegar por el modulo de logueo", () => {
    cy.visit("http://localhost:3001/login");
  });

  it("Escribiendo en el formulario y registrando al usuario", () => {
    cy.get('input[name="document"]').type("fakeuser@example.com");
    cy.get('input[name="password"]').type("fakeuser");
    cy.get("form")
      .submit()
      .then(() => {
        cy.url().should("include", "/login");
      });
  });
});
