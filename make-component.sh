#!/bin/bash
COLOR_RESET="\e[m"
COLOR_RED="\e[0;31;40m"
COLOR_GREEN="\e[0;32;40m"

COMPONENT=$1
FOLDER=$2

if [ "x$COMPONENT" = "x" ] || [ "x$FOLDER" = "x" ]; then 
  echo -e "$COLOR_RED [-] Se debe ingresar el Nombre del Componente como primer parametro y la carpeta como segundo parametro $COLOR_RESET"
  exit 1
fi

declare -A FILES

CONTENT_JS='import React from "react";
import styles from "./Componente.module.scss";
/**
 * @param Props Default props of a Componente.
 * @return A Componente
 */
function Componente({  ...props }) { 
    return <span {...props} className={`${styles.componente}`}>Esto es un Componente</span>
}
export default Componente;'
CONTENT_SCSS='@use "/public/styles/theming.scss";
.componente{
  font-family: $theming.$font-family-primary;
  :global { 
    //ACA SE INCLUYEN LAS CLASES DE ANT DESIGN
  }
  //ACA LOS OTROS ESTILOS
}
'
CONTENT_STORIES='import { withDesign } from "storybook-addon-designs"
import Componente  from "./Componente.js";

const urlComponents = "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idComponente = {
  default: "AQUI VA EL ID CORRESPONDIENTE AL FIGMA"
}

export default {
  title: "Components/Componente",
  component: Componente,
  decorators: [withDesign],
  argTypes:{}
};
  
const Template = (args) => <Componente {...args} />;

export const Default = Template.bind({});

Default.args = {};

Default.parameters = {
  design:{
    type :"figma",
    url : `${urlComponents}${idComponente.default}`
  }
}
'
FILES=( [".js"]="$CONTENT_JS" [".module.scss"]="$CONTENT_SCSS" [".stories.js"]="$CONTENT_STORIES")
LOWERCASE_COMPONENT=$(echo "$COMPONENT" | tr '[:upper:]' '[:lower:]')
#Creamos el directorio para guardar los archivos
mkdir $FOLDER/$COMPONENT &> /dev/null
#Aca recorremos el diccionario de extensiones y le cambiamos el contenido con el valor del parametro
echo -e "\n\n[+] Creando archivos para el componente:\n"
for FILE in ${!FILES[@]};do
  sed -e "s/Componente/$COMPONENT/g" -e "s/componente/$LOWERCASE_COMPONENT/g" <<< ${FILES[${FILE}]} > $FOLDER/$COMPONENT/$COMPONENT${FILE}
  echo -e "\t[+] Se ha creado el archivo $FOLDER/$COMPONENT/$COMPONENT${FILE}"
done
echo -e "$COLOR_GREEN\n\n[+] Se han creado con éxito los archivos en $FOLDER/$COMPONENT $COLOR_RESET"
exit 0
