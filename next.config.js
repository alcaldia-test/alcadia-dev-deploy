module.exports = {
  images: {
    domains: [
      "54.89.59.174",
      "static.eldeber.com.bo",
      "encrypted-tbn0.gstatic.com",
      "3.bp.blogspot.com",
      "placehold.jp",
      "54.89.59.174",
      "dummyimage.com",
      "participa.lapaz.bo",
      "dummyimage.com",
      "backalcaldia.blockchainconsultora.com",
      "pbackalcaldia.blockchainconsultora.com",
      "dummyimage.com",
      "backalcaldia.blockchainconsultora.com",
      "alcaldia.swalisoft.com",
    ],
  },
  future: {
    webpack5: true,
  },
  webpack: (config, options) => {
    config.module.rules.push({
      test: /.(png|jpg|gif|svg)$/,
      use: {
        loader: "url-loader",
        options: {
          limit: 100000,
          name: "[name].[ext]",
        },
      },
    });
    return config;
  },
  browsers: ["ChromeHeadlessNoSandbox"],
  customLaunchers: {
    ChromeHeadlessNoSandbox: {
      base: "ChromeHeadless",
      flags: ["--no-sandbox"],
    },
  },
  reactStrictMode: false,
};
