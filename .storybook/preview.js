import { withDesign } from "storybook-addon-designs";
import "tailwindcss/tailwind.css";
import "../public/styles/sb.css";
// Icons
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [withDesign];
