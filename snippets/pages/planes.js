import React, { useEffect } from "react";

import { Wrapper } from "@components/Wrapper";
import Plans from "@containers/Admin/Plans";

import { plansSaga } from "@containers/Admin/Plans/redux/saga";
import { plansReducer } from "@containers/Admin/Plans/redux/reducer";

import injectSaga from "@utils/inject-saga";
import injectReducer from "@utils/inject-reducer";

import { compose } from "redux";
// import { useDispatch } from "react-redux";
// import { requestPlansStart } from "@containers/Admin/Plans/redux/actions";

const PagePlans = () => {
  // const dispatch = useDispatch();

  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    // dispatch(requestPlansStart());
  };
  return (
    <Wrapper>
      <Plans />
    </Wrapper>
  );
};

const withReducerPlans = injectReducer({ key: "plans", reducer: plansReducer });
const withSagaPlans = injectSaga({ key: "Plans", saga: plansSaga });

export default compose(withSagaPlans, withReducerPlans)(PagePlans);
