import React from "react";
import { Button } from "antd";
import { customUseReducer } from "@utils/customHooks";
import Modal from "antd/lib/modal/Modal";
import { SelectComponent } from "@components/SelectComponent";

export const FilterComponent = ({
  initialState = {},
  filter = {},
  handleFilter = () => {},
  visible,
  handleClosed = () => {},
}) => {
  const [state, dispatchComponent] = customUseReducer(filter);

  const applyFilter = () => {
    handleFilter(state);
  };

  const handleReset = () => {
    dispatchComponent(initialState);
  };

  return (
    <Modal
      onCancel={handleClosed}
      title="Filtrado de Ejercicios"
      centered
      visible={visible}
      footer={[
        <Button key={1} onClick={handleClosed}>
          SALIR
        </Button>,
        <Button key={2} type="primary" onClick={applyFilter}>
          APLICAR
        </Button>,
      ]}
    >
      <p className="gx-mb-2">Seleccione un tipo de Ejercicio</p>
      <SelectComponent.TypesExercise
        value={state.typeExerciseId}
        onChange={(typeExerciseId) => dispatchComponent({ typeExerciseId })}
        className="gx-w-100"
      />
      <Button type="dashed" block className="gx-mt-3" onClick={handleReset}>
        LIMPIAR FILTRO
      </Button>
    </Modal>
  );
};
