import React, { useCallback, useEffect } from "react";
import { Row, Col, Divider } from "antd";
import { customUseReducer } from "@utils/customHooks";
import { TableComponent } from "./components/TableComponent";
import ModalComponent from "./components/ModalComponent";
import Widget from "@components/Widget";

const initialState = {
  loading: false,
  modal: false,
  selected: {},
};

export function IndexPage() {
  const [state, dispatchComponent] = customUseReducer(initialState);

  useEffect(() => {}, []);

  const ModalComponentWithCallback = useCallback(
    (props) => ModalComponent(props),
    [state.modal]
  );

  return (
    <Widget>
      <Row>
        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
          <p className="gx-text-grey">Detalles del Contenedor</p>
          <h2 className="gx-text-uppercase gx-text-black gx-font-weight-bold gx-fnd-title">
            REGISTRO DE EJERCICIOS
          </h2>
          <p>
            Este contenedor tiene como funcionalidad el registro de los Ideas
            del Landing Page
          </p>
        </Col>
      </Row>
      <Divider dashed />
      <Row>
        <Col span={24}>
          <h1 className="gx-text-grey">Historial de Registros</h1>
        </Col>
      </Row>
      <TableComponent openModal={dispatchComponent} />
      <ModalComponentWithCallback
        visible={state.modal}
        handleClosed={() => dispatchComponent({ modal: false })}
        selected={state.selected}
      />
    </Widget>
  );
}

IndexPage.propTypes = {};

export default IndexPage;
