import { put, takeLatest, all, call } from "redux-saga/effects";
import request, {
  deleteOptions,
  patchOptions,
  postOptions,
  showMessageError,
} from "@utils/request";

import {
  DELETE_EXERCISES_START,
  REQUEST_EXERCISES_START,
  REGISTER_EXERCISES_START,
  UPDATE_EXERCISES_START,
} from "./constants";
import {
  loadingExercisesShow as showLoader,
  loadingExercisesHide as hideLoader,
  requestExercisesSuccess,
  updateExercisesSuccess,
  registerExercisesSuccess,
  deleteExercisesSuccess,
} from "./actions";
import { message } from "antd";
import { ExercisesServices } from "@services/exercises";
import { requestWorkTeamStart } from "@containers/Admin/WorkTeam/redux/actions";

export function* requestExercises({
  payload: { resolve, reject, ...payload },
}) {
  const exercisesServices = new ExercisesServices();
  try {
    yield !resolve && put(showLoader());

    const _requestExercises = yield exercisesServices.getExercises();
    yield all([
      put(requestExercisesSuccess({ data: _requestExercises })),
      put(requestWorkTeamStart({ resolve: () => {}, reject: () => {} })),
    ]);
  } catch (err) {
    yield showMessageError(err);
    yield !reject && call(reject, "Al parecer hubo un error");
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* registerExercises({
  payload: { resolve, reject, ...payload },
}) {
  console.log(
    "🚀 ~ file: saga.js ~ line 33 ~ function*registerExercises ~ payload",
    payload
  );
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/exercises`;
    const options = yield postOptions(payload);
    const requestExercises = yield call(request, url, options);

    yield put(registerExercisesSuccess(requestExercises));

    yield call(resolve || message.success, "Se registró con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo registrar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* updateExercises({
  payload: { resolve, reject, id, ...payload },
}) {
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/exercises/${id}`;
    const options = yield patchOptions(payload);
    yield call(request, url, options);

    yield put(updateExercisesSuccess({ id, values: payload }));
    yield call(resolve || message.success, "Se modificó con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo modificar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* deleteExercises({ payload: { resolve, reject, id } }) {
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/exercises/${id}`;
    const options = yield deleteOptions();
    yield call(request, url, options);

    yield put(deleteExercisesSuccess({ id }));

    yield call(resolve || message.success, "Se eliminó con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo eliminar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* exercisesSaga() {
  yield takeLatest(REQUEST_EXERCISES_START, requestExercises);
  yield takeLatest(REGISTER_EXERCISES_START, registerExercises);
  yield takeLatest(DELETE_EXERCISES_START, deleteExercises);
  yield takeLatest(UPDATE_EXERCISES_START, updateExercises);
}
