import {
  DELETE_EXERCISES_SUCCESS,
  REQUEST_EXERCISES_SUCCESS,
  LOADING_EXERCISES_HIDE,
  LOADING_EXERCISES_SHOW,
  REGISTER_EXERCISES_SUCCESS,
  UPDATE_EXERCISES_SUCCESS,
} from "./constants";

const initialState = {
  data: [],
  filter: {},
  loading: false,
};

export function exercisesReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_EXERCISES_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        filter: action.payload.filter ?? {},
      };
    case REGISTER_EXERCISES_SUCCESS:
      return { ...state, data: [...state.data, action.payload] };
    case DELETE_EXERCISES_SUCCESS:
      return {
        ...state,
        data: state.data.filter((values) => values.id !== action.payload.id),
      };
    case UPDATE_EXERCISES_SUCCESS:
      return {
        ...state,
        data: state.data.map((values) => {
          return values.id === action.payload.id
            ? { ...values, ...action.payload.values }
            : values;
        }),
      };
    case LOADING_EXERCISES_SHOW:
      return { ...state, loading: true };
    case LOADING_EXERCISES_HIDE:
      return { ...state, loading: false };
    default:
      return state;
  }
}
