import {
  DELETE_CATEGORIES_SUCCESS,
  REQUEST_CATEGORIES_SUCCESS,
  LOADING_CATEGORIES_HIDE,
  LOADING_CATEGORIES_SHOW,
  REGISTER_CATEGORIES_SUCCESS,
  UPDATE_CATEGORIES_SUCCESS,
} from "./constants";

const initialState = {
  data: [],
  filter: {},
  loading: false,
};

export function categoriesReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_CATEGORIES_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        filter: action.payload.filter ?? {},
      };
    case REGISTER_CATEGORIES_SUCCESS:
      return { ...state, data: [...state.data, action.payload] };
    case DELETE_CATEGORIES_SUCCESS:
      return {
        ...state,
        data: state.data.filter((values) => values.id !== action.payload.id),
      };
    case UPDATE_CATEGORIES_SUCCESS:
      return {
        ...state,
        data: state.data.map((values) => {
          return values.id === action.payload.id
            ? { ...values, ...action.payload.values }
            : values;
        }),
      };
    case LOADING_CATEGORIES_SHOW:
      return { ...state, loading: true };
    case LOADING_CATEGORIES_HIDE:
      return { ...state, loading: false };
    default:
      return state;
  }
}
