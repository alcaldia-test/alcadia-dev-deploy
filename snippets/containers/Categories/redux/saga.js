import { put, takeLatest, call } from "redux-saga/effects";
import request, {
  deleteOptions,
  patchOptions,
  postOptions,
  showMessageError,
} from "@utils/request";

import {
  DELETE_CATEGORIES_START,
  REQUEST_CATEGORIES_START,
  REGISTER_CATEGORIES_START,
  UPDATE_CATEGORIES_START,
} from "./constants";
import {
  loadingCategoriesShow as showLoader,
  loadingCategoriesHide as hideLoader,
  requestCategoriesSuccess,
  updateCategoriesSuccess,
  registerCategoriesSuccess,
  deleteCategoriesSuccess,
} from "./actions";
import { message } from "antd";
import { CategoriesServices } from "@services/categories";

export function* requestCategories({
  payload: { resolve, reject, ...payload },
}) {
  const categoriesServices = new CategoriesServices();
  try {
    yield !resolve && put(showLoader());
    const _requestCategories = yield categoriesServices.getCategories();
    yield put(requestCategoriesSuccess({ data: _requestCategories }));
  } catch (err) {
    yield showMessageError(err);
    yield !reject && call(reject, "Al parecer hubo un error");
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* registerCategories({
  payload: { resolve, reject, ...payload },
}) {
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/categories`;
    const options = yield postOptions(payload);
    const requestCategories = yield call(request, url, options);

    yield put(registerCategoriesSuccess(requestCategories));

    yield call(resolve || message.success, "Se registró con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo registrar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* updateCategories({
  payload: { resolve, reject, id, ...payload },
}) {
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/categories/${id}`;
    const options = yield patchOptions(payload);
    yield call(request, url, options);

    yield put(updateCategoriesSuccess({ id, values: payload }));
    yield call(resolve || message.success, "Se modificó con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo modificar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* deleteCategories({ payload: { resolve, reject, id } }) {
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/categories/${id}`;
    const options = yield deleteOptions();
    yield call(request, url, options);

    yield put(deleteCategoriesSuccess({ id }));

    yield call(resolve || message.success, "Se eliminó con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo eliminar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* categoriesSaga() {
  yield takeLatest(REQUEST_CATEGORIES_START, requestCategories);
  yield takeLatest(REGISTER_CATEGORIES_START, registerCategories);
  yield takeLatest(DELETE_CATEGORIES_START, deleteCategories);
  yield takeLatest(UPDATE_CATEGORIES_START, updateCategories);
}
