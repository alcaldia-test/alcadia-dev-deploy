import {
  DELETE_CATEGORIES_START,
  DELETE_CATEGORIES_SUCCESS,
  REQUEST_CATEGORIES_START,
  REQUEST_CATEGORIES_SUCCESS,
  LOADING_CATEGORIES_HIDE,
  LOADING_CATEGORIES_SHOW,
  REGISTER_CATEGORIES_START,
  REGISTER_CATEGORIES_SUCCESS,
  UPDATE_CATEGORIES_START,
  UPDATE_CATEGORIES_SUCCESS,
} from "./constants";

export const requestCategoriesStart = (payload = {}) => ({
  type: REQUEST_CATEGORIES_START,
  payload,
});
export const requestCategoriesSuccess = (payload = {}) => ({
  type: REQUEST_CATEGORIES_SUCCESS,
  payload,
});

export const registerCategoriesStart = (payload = {}) => ({
  type: REGISTER_CATEGORIES_START,
  payload,
});
export const registerCategoriesSuccess = (payload = {}) => ({
  type: REGISTER_CATEGORIES_SUCCESS,
  payload,
});

export const deleteCategoriesStart = (payload = {}) => ({
  type: DELETE_CATEGORIES_START,
  payload,
});
export const deleteCategoriesSuccess = (payload = {}) => ({
  type: DELETE_CATEGORIES_SUCCESS,
  payload,
});

export const updateCategoriesStart = (payload = {}) => ({
  type: UPDATE_CATEGORIES_START,
  payload,
});
export const updateCategoriesSuccess = (payload = {}) => ({
  type: UPDATE_CATEGORIES_SUCCESS,
  payload,
});

export const loadingCategoriesShow = (payload = {}) => ({
  type: LOADING_CATEGORIES_SHOW,
  payload,
});
export const loadingCategoriesHide = (payload = {}) => ({
  type: LOADING_CATEGORIES_HIDE,
  payload,
});
