import React, { useEffect } from "react";
import { Input, Spin, Form, message } from "antd";
import Modal from "antd/lib/modal/Modal";
import { useDispatch } from "react-redux";
import { useForm } from "antd/lib/form/Form";
import {
  registerCategoriesStart,
  updateCategoriesStart,
} from "../redux/actions";
import { customUseReducer } from "@utils/customHooks";

const nameProduct = "Categorias";

const initialState = {
  loading: false,
};

function ModalComponent({ visible, handleClosed, selected }) {
  const [state, dispatchComponent] = customUseReducer(initialState);
  const [form] = useForm();
  const dispatch = useDispatch();
  const isRegister = Object.keys(selected).length === 0;

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...selected,
      ...(!isRegister &&
        {
          // Si es modificación, aquí se agregarán los datos extras
          // priceUp: selected?.inputPrice[0],
          // priceDown: selected?.inputPrice[1],
        }),
    });
  }, [visible]);

  const onFinish = (values) => {
    dispatchComponent({ loading: true });
    return new Promise((resolve, reject) => {
      if (isRegister) {
        dispatch(registerCategoriesStart({ ...values, resolve, reject }));
      } else {
        dispatch(
          updateCategoriesStart({ ...values, id: selected.id, resolve, reject })
        );
      }
    })
      .then((res) => {
        message.success(res);
        handleClosed();
      })
      .catch((err) => {
        message.error(err);
        dispatchComponent({ loading: false });
      });
  };

  return (
    <Modal
      title={isRegister ? `Nuevo ${nameProduct}` : `Modificar ${nameProduct}`}
      centered
      visible={visible}
      onCancel={handleClosed}
      okText={isRegister ? "Publicar" : "Modificar"}
      cancelText="Cerrar"
      onOk={() => form.submit()}
      confirmLoading={state.loading}
    >
      <Spin
        spinning={state.loading}
        tip={`${
          isRegister ? "Registrando" : "Modificando"
        } los datos en backend`}
        size="large"
      >
        <Form
          onFinish={onFinish}
          form={form}
          name="basic"
          className="ant-advanced-search-form"
          layout="vertical"
        >
          <Form.Item
            label="Titulo"
            rules={[
              { required: true, message: "Ingresa un título para el post" },
            ]}
            name="title"
          >
            <Input placeholder="Título" />
          </Form.Item>
        </Form>
      </Spin>
    </Modal>
  );
}

export default ModalComponent;
