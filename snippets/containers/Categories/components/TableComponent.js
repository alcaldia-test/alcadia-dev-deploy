import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Dropdown, Menu } from "antd";
import { deleteCategoriesStart } from "../redux/actions";
import moment from "moment";

const getColumn = ({ handleEdit, handleDelete }) => {
  return [
    {
      width: 90,
      title: "Fecha",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (value) => moment(value).format("YYYY/MM/DD"),
    },
    {
      title: "Titulo",
      dataIndex: "title",
      key: "title",
    },
    {
      width: 60,
      title: "",
      // eslint-disable-next-line react/display-name
      render: (_, record) => (
        <div style={{ marginTop: -5, marginBottom: -5 }}>
          <Dropdown
            overlay={
              <Menu>
                <Menu.Item onClick={() => handleEdit(record)}>Editar</Menu.Item>
                <Menu.Item onClick={() => handleDelete(record)}>
                  Eliminar
                </Menu.Item>
              </Menu>
            }
            placement="bottomRight"
            trigger={["click"]}
            size="small"
          >
            <i className="gx-icon-btn icon icon-ellipse-v" />
          </Dropdown>
        </div>
      ),
    },
  ];
};

export const TableComponent = ({ openModal }) => {
  const dispatch = useDispatch();
  const { categories } = useSelector((store) => store);

  const handleEdit = (values) => {
    openModal({ modal: true, selected: values });
  };

  const handleDelete = (values) => {
    dispatch(deleteCategoriesStart(values));
  };

  const columns = getColumn({ handleDelete, handleEdit });

  return (
    <Table
      size="small"
      loading={categories.loading}
      className="gx-table-responsive"
      columns={columns}
      dataSource={categories?.data.map((video, key) => ({ key, ...video }))}
    />
  );
};
