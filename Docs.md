<details>
Seccion Por revisar
<details>
<summary>
Estructura de carpetas para redux, actions y sagas
</summary>
En este apartado ira la estructura a seguir para las carpetas relacionadas con redux:

Información compartida a través del mediante Redux y Redux Saga. Redux Saga se utiliza para los controles de acceso y los módulos de la aplicación. Todo el código relacionado con Redux se encuentra en la carpeta redux categorizada por módulos utilizados.

```
reducers.js
```

```
sagas.js
```

### Estructura de carpetas:

- redux
- Auth
  - actions.js
  - reducer.js
  - saga.js
- Globals
  - actions.js
  - reducer.js
  - saga.js
- Constants
  - auth.js
  - global.js
- reducers.js
- sagas.js

## Ejemplo de uso

`auth.js`

Para este ejemplo haremos de inicio de sesións, primero debemos crear un `types` llamado `redux/constants/auth.js`

```
export const LOGIN_START = "LOGIN_START";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
```

luego de haber creado los `types` debemos indexarlo, crear un archivo llamado `redux/constants/index.js`

```
export * from "./auth";
```

`action.js`

Despues haber creado los archivos, procedemos a crear un action en `redux/auth/action.js`

```
import { LOGIN_START, LOGIN_SUCCESS } from "@redux/constants";

export const loginStart = (payload) => ({
  type: LOGIN_START,
  payload,
});
export const loginSuccess = (payload) => ({
  type: LOGIN_SUCCESS,
  payload,
});
```

`reducer.js`

Creamos un reducer en `redux/auth/reducer.js`.

```
import { LOGIN_SUCCESS, SIGNOUT_SUCCESS } from "../constants";

export const initialState = {
  tokenUser: null,
  dataUser: {},
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      return { ...state, ...action.payload };
    }
    case SIGNOUT_SUCCESS: {
      return { ...state, ...initialState };
    }
    default:
      return state;
  }
};
export default auth;

```

`saga.js`

Creamos un archivo `redux/auth/saga.js`.

```
import { put, takeLatest, all, call } from "redux-saga/effects";
import { loginSuccess } from "@redux/actions";

export function* Login({ payload }) {
    try {
        // llamado API -> response
        // put(loginSuccess(response))
    } catch (err) {
        // obtener errores
    } finally {
        // accion cuando finalice
    }
}

export function* authSaga() {
  yield takeLatest(LOGIN_START, Login);
}
```

`reducers.js y sagas.js`

una vez teniendo los reducers y sagas, debemos centralizar todo para que esté en la store. creamos los archivos en: `redux/reducers.js` y `redux/sagas.js`

```
reducers.js

import { combineReducers } from 'redux';
import authReducer from './auth/reducer'

const reducers = combineReducers({
  auth: authReducer,
  // agregar mas reducers aqui...
});

```

```
sagas.js

import { all } from 'redux-saga/effects';
import authSagas from './auth/saga'

export default function* rootSaga() {
  yield all([
    authSagas(),
    // aumentar mas sagas aqui...
  ]);
}

```

`login.js`

Para poder usar todo lo que hemos creado hasta ahora debemos ubicarnos en el archivo de login `container/login/index.js`.

```
import { loginStart } from '@redux/auth/action';
import { useDispatch } from 'react-redux';

export default const Index = () => {
    const dispatch = useDispatch();
    ...
    const onLogin = (value) => {
        dispatch(loginStart(value))
    }
    ...
}
```

y con esto ya estaria comunicandose con el API para realizar la acción de logueo

</ol>
</details>
<details>
<summary>
Configurar testing en Proyecto de NextJS
</summary>

### Instalación de dependecias de desarrollo necesarias

```
yarn add --save-dev jest babel-jest @testing-library/react @testing-library/jest-dom identity-obj-proxy react-test-renderer
```

### Configurando Jest

Crear archivo `jest.config.js` en la carpeta root de nuestro proyecto con el siguiente script:

```
module.exports = {
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules/**',
  ],
  moduleNameMapper: {
    /* Controla las importaciones de CSS (con modulos de CSS)
    https://jestjs.io/docs/webpack#mocking-css-modules */
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',

    // Handle CSS imports (without CSS modules)
    '^.+\\.(css|sass|scss)$': '<rootDir>/__mocks__/styleMock.js',

    /* Controla las importaciones de imagenes
    https://jestjs.io/docs/webpack#handling-static-assets */
    '^.+\\.(jpg|jpeg|png|gif|webp|svg)$': '<rootDir>/__mocks__/fileMock.js',
  },
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/.next/'],
  testEnvironment: 'jsdom',
  transform: {

    /* Usa babel-jest para transpilar los tests con next/babel
    https://jestjs.io/docs/configuration#transform-objectstring-pathtotransformer--pathtotransformer-object */
    '^.+\\.(js|jsx|ts|tsx)$': ['babel-jest', { presets: ['next/babel'] }],
  },
  transformIgnorePatterns: [
    '/node_modules/',
    '^.+\\.module\\.(css|sass|scss)$',
  ],
}
```

### Carpetas para manejar estilos e importaciones de imagenes dentro de root

-\_\_mocks\_\_

- fileMock.js
- styleMock.js

Dentro de `fileMock.js`

```
(module.exports = "test-file-stub")
```

Dentro de `styleMock.js`

```
module.exports = {};
```

### Agregar custom matchers de Jest

Crear carpeta`jest.config.js` con el siguiente script:

```
setupFilesAfterEnv: ['<rootDir>/jest.setup.js']
```

Luego dentro de una carpeta llamada `jest.setup.js` añadir:

```
import '@testing-library/jest-dom/extend-expect'
```

### Importaciones absolutas y alias de rutas

Dentro de `jsconfig.json` añadir:

```
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@/components/*": ["components/*"]
    }
  }
}
```

Dentro de `jest.config.js` añadir:

```
moduleNameMapper: {
  '^@/components/(.*)$': '<rootDir>/components/$1',
}
```

### Añadir test script en package.json

```
"scripts": {
  "dev": "next dev",
  "build": "next build",
  "start": "next start",
  "test": "jest --watch"
}
```

Información obtenida de: https://nextjs.org/docs/testing

</details>

<details>
<summary>Ejemplo de test a un componente</summary>

### Ejemplo de un test a un botón

Dentro de `components` creamos una carpeta llamada \__\_tests_\_\_ y dentro de esa carpeta empezamos a crear nuestros tests, por ejemplo `button.test.js`

```
import { screen, render, fireEvent } from "@testing-library/react";
import Button from "@components/Button";

describe("Button", () => {
  // Comprueba si el componente tiene un componente hijo

  it("must have a children", () => {
    const { getByText } = render(<Button>submit</Button>);
    expect(getByText(/submit/i)).toBeInTheDocument();
  });

  // Comprueba si el botón llama al metodo  onClick cuando se le agrega y si funciona correctamente

  it("calls onClick prop when clicked", () => {
    const handleClick = jest.fn();
    render(<Button onClick={handleClick}>Click Me</Button>);
    fireEvent.click(screen.getByText(/click me/i));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
```

### Ejecutamos el comando yarn test

```
yarn test
```

Si todo salio correctamente, los dos tests debieron pasar como aprobados.

</details>
<details>
<summary>
Forma de trabajo de Estilos
</summary>
En este apartado se debe detallar la manera de trabajo que se usara en cuanto a CSS

1. Estilos, varibles y fuentes globales dentro de varibles.sccs, typografies.scss y styles.scss
2. Theming (colores primarios y colores combinados) en theming.css
3. En componentes utilizar estilos provistos desde node_modules de la librería antd y modificar en un SCSS usando vaiables globales.
4. Hacer clases genéricas de cada componente en SCSS modules, le llegaran props a cada componente indicando tamaño, tipo, etc
5. Utilizar tailwindcss para estilizar los containers layouts
</details>

<details>
<summary>
Reglas para el testing 📋
</summary>
Primero para el testing carpetas estaran organizadas de las siguiente manera
	<ul>
        <li>
        	src
            <ul>
                <li>__test__</li>
                <li>__mocks</li>
            </ul>
        </li>
    </ul>
<p>
En la carpeta de __test__ estaremos creando los test ya sea para nuestros saga o nuestros componentes, por eso tendremos dividio las carpetas por modulos.
</p>
<ul>
	<li>
        __test__
        <ul>
            <li>components</li>
            <li>saga</li>
            <li>utils</li>
        </ul>
    </li>
</ul>
<p>
Todos los archivos tendran un nombre seguido por .test.js ---> ejemplo.test.js
</p>
<p>
Los archivos que esten en la carpeta __mocks__ nos serviran para poder simular modulos automaticamente
</p>
<p>
 Para ejecutar los test que nosotros tengamos pondremos el siguente comando
 </p>
 
```
   yarn test
```
    
</details>
