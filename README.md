# Gobierno Autónomo Municipal, Nuestras Señora de La Paz

### 📋 Requirements

```
node.js > 12
yarn
```

## Usage

### First run

```bash
yarn
```

### 📦 Build

```bash
#next js
yarn build
yarn start
#storybook
yarn storybook:build
```

## 🗺️ Deployment

### Gitlab CI/CD

Triggered on every push to master and develop or when is pushed a commit message starts with "@publish " in a feature/\* branch

### Local - Storybook

```bash
yarn chromatic
```

## Development

```bash
#next js
yarn dev
#storybook
yarn storybok
```

## Flujo de trabajo

(revisar documentacion Clickup)
