import React from "react";
import Layout from "@containers/Wrapper/WrapperNologin";

import Container from "@containers/Voting";

const Convocatorias = (props) => {
  return (
    <>
      <Layout
        title="Tu Voto Cuenta"
        description="¡Bienvenido!, Estas son las votaciones actuales"
        defaultImg={true}
        currentRoute="/tu_voto_cuenta"
      >
        <Container {...props} />
      </Layout>
    </>
  );
};

export default Convocatorias;
