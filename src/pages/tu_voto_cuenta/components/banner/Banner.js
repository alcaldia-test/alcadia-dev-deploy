const Banner = () => {
  return (
    <div className="bg-white mb-4">
      <img
        className="w-full bg-cover"
        src="/images/banner-discussions.png"
        alt="banner"
      />
      <section className="absolute right-0 left-0 top-1/3  sm:top-1/4 md:top-2/4">
        <div className="container mx-auto">
          <div className="flex justify-center">
            <div className="text-center">
              <h3 className="text-2xl md:text-4xl font-bold text-white">
                Tu Voto Cuenta
              </h3>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="flex justify-center">
          <h3 className="text-center my-2 text-lg md:text-2xl text-slate-700 font-bold not-italic">
            ¡Bienvenido!, Estas son las votaciones actuales
          </h3>
        </div>
      </section>
    </div>
  );
};

export default Banner;
