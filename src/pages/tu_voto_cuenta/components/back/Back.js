import Link from "next/link";
import { ArrowLeftOutlined } from "@ant-design/icons";
// <- otras votaciones es este componente
const Back = (props) => {
  return (
    <section className="content-btn-back">
      <div className="flex justify-start pb-3 px-2 pt-3">
        <Link href={props.route}>
          <a className="text-tiny text-lightGrey align-text-bottom not-italic">
            <ArrowLeftOutlined />
            {props.text}
          </a>
        </Link>
      </div>
    </section>
  );
};

export default Back;
