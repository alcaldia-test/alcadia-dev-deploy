const BtnPagination = ({ children }) => {
  return (
    <button className="border text-base text-textColor3 border-borderCardColor rounded-sm">
      {children}
    </button>
  );
};

export default BtnPagination;
