import { useRouter } from "next/router";
import Layout from "@containers/Wrapper/WrapperNologin";

import Container from "@containers/Voting/Vote/Statistics";

const Propuesta = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <Layout currentRoute="/tu_voto_cuenta">
        <Container id={id} />
      </Layout>
    </>
  );
};

export default Propuesta;
