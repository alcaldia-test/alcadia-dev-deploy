import Layout from "@containers/Wrapper/WrapperNologin";
import Container from "@containers/Voting/Vote";
import { votingServices } from "@services/voting";
import { defaultValueStorage } from "@utils/with-redux-store";
import { getHostname } from "@utils/utils";
import SEO from "@components/SEO.js";

export const getServerSideProps = async ({ req, params }) => {
  const hostname = getHostname(req, true);
  const { auth } = await defaultValueStorage({ req }, hostname);

  const { id } = params;

  const vote = await votingServices.getVote({ id, userId: auth?.dataUser?.id });

  return {
    props: {
      vote,
      id,
    },
  };
};

const Propuesta = ({ vote, id }) => {
  return (
    <>
      <SEO
        title={`Votación - ${vote?.title}`}
        description={`¿${vote?.question}?`}
        image={vote?.fileLink}
        route={`/tu_voto_cuenta/${id}`}
      />
      <Layout currentRoute="/tu_voto_cuenta">
        <Container id={id} vote={vote} />
      </Layout>
    </>
  );
};

export default Propuesta;
