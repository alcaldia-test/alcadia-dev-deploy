import { useRouter } from "next/router";
import Layout from "@containers/Wrapper/WrapperNoFooter";
import Container from "@containers/User";

const Usuario = (props) => {
  const router = useRouter();
  const { userId } = router.query;

  return (
    <>
      <Layout>
        <Container userId={userId} {...props} />
      </Layout>
    </>
  );
};

export default Usuario;
