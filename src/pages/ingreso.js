import React from "react";
import IngresoAdmin from "@containers/IngresoAdmin";
import SEO from "@components/SEO.js";

const IndexPage = () => {
  return (
    <>
      <SEO
        title="Iniciar sesión como administrador
 - Alcaldía"
        description="Ingresa y administra la participación en los distintos módulos de la plataforma."
        route="/ingreso"
      />
      <div className="h-screen bg-gray-200 grid md:justify-items-center md:items-center">
        <div className=" bg-white md:w-3/4 lg:w-2/3 md:h-2/3 lg:h-2/3 grid md:justify-items-center md:items-center h-full rounded-large">
          <IngresoAdmin />
        </div>
      </div>
    </>
  );
};

export default IndexPage;
