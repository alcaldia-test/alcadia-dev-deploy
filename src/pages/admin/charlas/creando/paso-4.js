import React from "react";
import StepFour from "@containers/Admin/Discussions/Creating/step-4";

export default function stepFour(props) {
  return <StepFour {...props} />;
}
