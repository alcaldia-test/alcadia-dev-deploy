import React from "react";
import StepTwo from "@containers/Admin/Discussions/Creating/step-2";

export default function stepTwo(props) {
  return <StepTwo {...props} />;
}
