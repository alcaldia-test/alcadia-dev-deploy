import React from "react";
import Step from "@containers/Admin/Discussions/Creating/step-3";

export default function stepThree(props) {
  return <Step {...props} />;
}
