import React from "react";
import StepLayout from "@containers/Admin/Discussions/Creating/step-1";

export default function stepOne(props) {
  return (
    <>
      <StepLayout {...props} />
    </>
  );
}
