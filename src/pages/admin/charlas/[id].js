import React, { useEffect } from "react";
import AdminPost from "@containers/Admin/Discussions/AdminPost";
import { editPost } from "@redux/createDiscussion/actions";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

export default function toAdmin() {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    const { id } = router.query;
    id && dispatch(editPost({ id }));
  }, [router.query.id]);

  return <AdminPost />;
}
