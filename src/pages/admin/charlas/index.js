import React from "react";
import Layout from "@containers/Wrapper/WrapperLogin";
import Discussions from "@containers/Admin/Discussions";

const discussions = () => {
  return (
    <Layout
      seo="Charlas | Dashboard"
      defaultKey={"discussions"}
      routeName={"Charlas Virtuales"}
    >
      <Discussions />
    </Layout>
  );
};

export default discussions;
