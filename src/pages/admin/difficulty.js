import React, { useEffect } from "react";

import { Wrapper } from "@components/Wrapper";
import Difficulty from "@containers/Admin/Difficulty";

import { difficultySaga } from "@containers/Admin/Difficulty/redux/saga";
import { difficultyReducer } from "@containers/Admin/Difficulty/redux/reducer";

import injectSaga from "@utils/inject-saga";
import injectReducer from "@utils/inject-reducer";

import { compose } from "redux";
import { useDispatch } from "react-redux";
import { requestDifficultyStart } from "@containers/Admin/Difficulty/redux/actions";

const PageDifficulty = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    dispatch(requestDifficultyStart());
  };
  return (
    <Wrapper>
      <Difficulty />
    </Wrapper>
  );
};

const withReducerDifficulty = injectReducer({
  key: "difficulty",
  reducer: difficultyReducer,
});
const withSagaDifficulty = injectSaga({
  key: "Difficulty",
  saga: difficultySaga,
});

export default compose(
  withSagaDifficulty,
  withReducerDifficulty
)(PageDifficulty);
