import React from "react";
import Layout from "@containers/Wrapper/WrapperLogin";
import Statictic from "@containers/Admin/Statistics";

const AdminStatistics = () => {
  return (
    <Layout defaultKey="complaints" routeName="Denuncias">
      <Statictic mod={"c"} id={true} />
    </Layout>
  );
};

export default AdminStatistics;
