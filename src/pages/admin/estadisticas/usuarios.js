import React from "react";
import Layout from "@containers/Wrapper/WrapperLogin";
import Statictic from "@containers/Admin/Statistics";

const AdminStatistics = () => {
  return (
    <Layout defaultKey="users" routeName="Usuarios">
      <Statictic mod="u" id={true} />
    </Layout>
  );
};

export default AdminStatistics;
