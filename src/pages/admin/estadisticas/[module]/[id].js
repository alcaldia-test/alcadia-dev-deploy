import React from "react";
import { useRouter } from "next/router";
import Layout from "@containers/Wrapper/WrapperLogin";
import Statictic from "@containers/Admin/Statistics";

const AdminNormative = () => {
  const { query } = useRouter();
  let mod;
  let modKey;

  switch (query.module) {
    case "normativas":
      mod = "n";
      modKey = "normatives";
      break;
    case "convocatorias":
      mod = "a";
      modKey = "announcements";
      break;
    case "charlas":
      mod = "d";
      modKey = "discussions";
      break;
    case "votaciones":
      mod = "v";
      modKey = "voting";
      break;
  }

  const title = {
    n: "Normativa Colaborativa",
    a: "Conv. a propuestas",
  };

  return (
    <Layout defaultKey={modKey} routeName={title[mod]}>
      <Statictic mod={mod} id={query.id} />
    </Layout>
  );
};

export default AdminNormative;
