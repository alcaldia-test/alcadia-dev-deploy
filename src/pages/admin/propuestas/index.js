import React from "react";
import AdminAnnouncementsContainer from "@containers/Admin/Announcements";
import Layout from "@containers/Wrapper/WrapperLogin";

const PagePropuestasAdmin = (props) => {
  return (
    <>
      <Layout seo="Convocatorias" defaultKey="votings">
        {
          <AdminAnnouncementsContainer
            {...props}
            key={"ContainerConvocatorias"}
          />
        }
      </Layout>
    </>
  );
};

export default PagePropuestasAdmin;
