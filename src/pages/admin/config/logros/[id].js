import React from "react";
import { useRouter } from "next/router";
import Attainment from "@containers/Admin/Attainment/attainment.js";

const VotingContentPageAdmin = () => {
  const router = useRouter();

  return <Attainment id={router.query.id} />;
};

export default VotingContentPageAdmin;
