import React from "react";

import AttainmentContainer from "@containers/Admin/Attainment";

const PageAttainment = () => {
  return <AttainmentContainer />;
};

export default PageAttainment;
