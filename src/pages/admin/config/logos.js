import React from "react";

import Wrapped from "@containers/Wrapper/WrapperLogin";
import LogosContainer from "@containers/Admin/Logos";

const LogosPage = () => {
  return (
    <Wrapped
      seo="Admin - Home"
      defaultKey="settings"
      routeName="Editar sitio web"
    >
      <LogosContainer />
    </Wrapped>
  );
};

export default LogosPage;
