import React from "react";

import Wrapped from "@containers/Wrapper/WrapperLogin";
import Profile from "@containers/User";
import { useSelector } from "react-redux";

const PageProfile = () => {
  const {
    dataUser: { id },
  } = useSelector((store) => store.auth);

  console.log(id);

  return (
    <Wrapped>
      <Profile userId={id} />
    </Wrapped>
  );
};

export default PageProfile;
