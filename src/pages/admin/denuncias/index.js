import React from "react";

import Wrapped from "@containers/Wrapper/WrapperLogin";
import Complaints from "@containers/Admin/Complaint";

const PageComplaints = () => {
  return (
    <Wrapped defaultKey="complaints" routeName="Denuncias">
      <Complaints />
    </Wrapped>
  );
};

export default PageComplaints;
