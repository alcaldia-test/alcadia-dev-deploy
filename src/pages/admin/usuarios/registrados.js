import React from "react";

import Wrapped from "@containers/Wrapper/WrapperLogin";
import Users from "@containers/Admin/User";

const PageRegistered = () => {
  return (
    <Wrapped defaultKey="registered" routeName="Usuarios registrados">
      <Users />
    </Wrapped>
  );
};

export default PageRegistered;
