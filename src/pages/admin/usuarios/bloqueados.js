import React from "react";

import Wrapped from "@containers/Wrapper/WrapperLogin";
import Users from "@containers/Admin/User";

const PageLocked = () => {
  return (
    <Wrapped defaultKey="locked" routeName="Usuarios bloqueados">
      <Users registered={1} />
    </Wrapped>
  );
};

export default PageLocked;
