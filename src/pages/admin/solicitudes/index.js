import React from "react";
import Layout from "@containers/Wrapper/WrapperLogin";
import AccessRequest from "@containers/AccessRequest";
import { useDispatch } from "react-redux";
import {
  initialRequestAdminRequestStart,
  initialRequestAdminRolesStart,
} from "@redux/accessRequest/actions";

const solicitudes = () => {
  const dispatch = useDispatch();
  dispatch(initialRequestAdminRequestStart());
  dispatch(initialRequestAdminRolesStart());
  return (
    <>
      <Layout seo="Solicitudes de Acceso" routeName="Solicitudes de acceso">
        {<AccessRequest />}
      </Layout>
    </>
  );
};

export default solicitudes;
