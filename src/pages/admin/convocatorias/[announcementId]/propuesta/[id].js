import { useRouter } from "next/router";
import Layout from "@containers/Wrapper/WrapperLogin";

import Proposal from "@containers/Admin/Announcements/components/Proposals/Proposal";

const Propuesta = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <Layout seo="Propuesta">
        <Proposal id={id} />
      </Layout>
    </>
  );
};

export default Propuesta;
