import React from "react";
import ProposalsList from "@containers/Admin/Announcements/components/Proposals/ProposalsList";
import Layout from "@containers/Wrapper/WrapperLogin";

const PagePropuestasAdmin = () => {
  return (
    <>
      <Layout seo="Propuestas">
        <ProposalsList key={"ContainerPropuestas"} />
      </Layout>
    </>
  );
};

export default PagePropuestasAdmin;
