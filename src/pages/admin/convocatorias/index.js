import React from "react";
import AdminAnnouncementsContainer from "@containers/Admin/Announcements";
import Layout from "@containers/Wrapper/WrapperLogin";

const PageConvocatoriasAdmin = (props) => {
  return (
    <>
      <Layout
        seo="Convocatorias"
        defaultKey="announcements"
        routeName="Convocatorias a propuestas"
      >
        {
          <AdminAnnouncementsContainer
            {...props}
            key={"ContainerConvocatorias"}
          />
        }
      </Layout>
    </>
  );
};

export default PageConvocatoriasAdmin;
