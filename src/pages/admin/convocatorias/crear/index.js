import React from "react";
import AdminCreatingAnnouncementScreen from "@containers/Admin/Announcements/components/Create";

const AdminCreatingAnnouncement = () => {
  return (
    <>
      <AdminCreatingAnnouncementScreen key={"CreateContainerPropuestas"} />
    </>
  );
};

export default AdminCreatingAnnouncement;
