import React from "react";

import Wrapped from "@containers/Wrapper/WrapperLogin";

const PageHome = () => {
  return (
    <>
      <Wrapped seo="Admin - Home">
        <div>hello from admin home</div>
      </Wrapped>
    </>
  );
};

export default PageHome;
