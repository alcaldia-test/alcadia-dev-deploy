import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Vote from "@containers/Admin/Voting/vote";
import { useRouter } from "next/router";

import { getPostByID } from "@redux/admin_voting/actions";

const VotingContentPageAdmin = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    const { id } = router.query;
    if (id) dispatch(getPostByID(id));
  }, [router.query.id]);

  return <Vote />;
};

export default VotingContentPageAdmin;
