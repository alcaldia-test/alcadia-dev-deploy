import React from "react";

import AdminVotingContainer from "@containers/Admin/Voting";

const PageVotacionesAdmin = () => {
  return <AdminVotingContainer />;
};

export default PageVotacionesAdmin;
