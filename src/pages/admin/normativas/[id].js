import React, { useEffect } from "react";
import AdminPost from "@containers/Admin/Normatives/AdminPost";
import { getNormativeData } from "@redux/chapters/actions";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { getHostname } from "@utils/utils";

export const getServerSideProps = async ({ req }) => {
  const hostname = getHostname(req, true);
  return {
    props: { hostname },
  };
};

export default function toAdmin({ hostname }) {
  const router = useRouter();
  const dispatch = useDispatch();
  const selector = useSelector((state) => state.chapters.id);
  const userId = useSelector((state) => state.auth.dataUser.id);
  useEffect(() => {
    dispatch(getNormativeData({ id: router.query.id, userId }));
  }, [router.query.id, selector]);

  return <AdminPost hostname={hostname} />;
}
