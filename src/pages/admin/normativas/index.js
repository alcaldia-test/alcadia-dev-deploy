import React from "react";
import Layout from "@containers/Wrapper/WrapperLogin";
import Normatives from "@containers/Admin/Normatives";

const discussions = () => {
  return (
    <Layout
      seo="Normativas | Dashboard"
      defaultKey={"normatives"}
      routeName={"Normativa Colaborativa"}
    >
      <Normatives />
    </Layout>
  );
};

export default discussions;
