import React from "react";
import StepLayout from "@containers/Admin/Normatives/Creating/step-1";

export default function stepOne(props) {
  return (
    <>
      <StepLayout {...props} />
    </>
  );
}
