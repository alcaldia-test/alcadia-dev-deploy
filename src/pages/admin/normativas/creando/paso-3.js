import React from "react";
import Step from "@containers/Admin/Normatives/Creating/step-3";

export default function stepThree(props) {
  return <Step {...props} />;
}
