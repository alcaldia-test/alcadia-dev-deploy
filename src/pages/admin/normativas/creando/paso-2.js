import React from "react";
import StepTwo from "@containers/Admin/Normatives/Creating/step-2";

export default function stepTwo(props) {
  return <StepTwo {...props} />;
}
