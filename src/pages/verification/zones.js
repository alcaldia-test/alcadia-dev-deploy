import RegisterZones from "@containers/Verification/components/RegisterZones";
import {
  initialRequestMacrosStart,
  initialRequestZonesStart,
} from "@redux/verification/actions";
import React from "react";
import { useDispatch } from "react-redux";
const indexPage = () => {
  const dispatch = useDispatch();
  dispatch(initialRequestMacrosStart());
  dispatch(initialRequestZonesStart());

  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <RegisterZones />
      </div>
    </div>
  );
};
export default indexPage;
