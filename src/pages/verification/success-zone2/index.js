import RegisterSuccess from "@containers/Verification/components/RegisterSuccess";
import React, { useEffect } from "react";
import Router from "next/router";
import { useSelector } from "react-redux";

const indexPage = () => {
  const { auth } = useSelector((e) => e);
  useEffect(() => {
    setTimeout(() => {
      if (auth?.RouterLogin) Router.replace(auth?.RouterLogin);
      else Router.replace("/");
    }, 5000);
  }, []);
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <RegisterSuccess />
      </div>
    </div>
  );
};
export default indexPage;
