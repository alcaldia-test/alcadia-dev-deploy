import Verification from "@containers/Verification";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useSelector } from "react-redux";
const indexPage = () => {
  const router = useRouter();
  const { userRegister } = useSelector((datos) => datos.auth);
  useEffect(() => {
    if (!userRegister.email) {
      router.push("/register");
    }
  }, []);
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-screen bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <Verification />
      </div>
    </div>
  );
};
export default indexPage;
