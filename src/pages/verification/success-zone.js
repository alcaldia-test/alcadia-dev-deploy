import RegisterSuccess from "@containers/Verification/components/RegisterSuccess";
import { loginSuccess } from "@redux/auth/actions";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import Router from "next/router";

const indexPage = () => {
  const dispatch = useDispatch();
  const { auth } = useSelector((e) => e);
  const { data } = useSelector((e) => e.verification);
  useEffect(() => {
    dispatch(
      loginSuccess({
        dateLogin: moment().format(),
        tokenUser: data.token,
        dataUser: data,
      })
    );
    setTimeout(() => {
      if (auth?.RouterLogin) Router.replace(auth?.RouterLogin);
      else Router.replace("/");
    }, 5000);
  }, []);
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <RegisterSuccess />
      </div>
    </div>
  );
};
export default indexPage;
