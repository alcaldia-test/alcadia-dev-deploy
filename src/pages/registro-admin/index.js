import AdminRegister from "@containers/AdminRegister";
import React from "react";

const IndexPage = () => {
  return (
    <>
      <div className="h-screen bg-gray-200 grid md:justify-items-center md:items-center">
        <div
          className=" bg-white md:w-3/4 lg:w-2/3 md:h-full lg:h-2/3 grid md:justify-items-center md:items-center banner-pr"
          style={{ borderRadius: "12px" }}
        >
          <AdminRegister />
        </div>
      </div>
    </>
  );
};

export default IndexPage;
