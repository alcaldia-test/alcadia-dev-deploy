import React, { useEffect } from "react";
import Permissions from "@containers/Permissions";
import { useDispatch, useSelector } from "react-redux";
import { initialRequestPermissionsStart } from "@redux/permissions/actions";
import { useRouter } from "next/router";

const indexPage = () => {
  const { adminRegister } = useSelector((state) => state.auth);
  const router = useRouter();
  const dispatch = useDispatch();
  dispatch(initialRequestPermissionsStart());
  useEffect((e) => {
    if (!adminRegister.id) {
      router.push("/registro-admin");
    }
  }, []);
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <Permissions />
      </div>
    </div>
  );
};
export default indexPage;
