/* eslint-disable import/no-unresolved */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable react/jsx-props-no-spreading */

import "tailwindcss/tailwind.css";
import "public/styles/styles.scss";
import "public/styles/theming.scss";

// Icons

import "nprogress/nprogress.css";

import Head from "next/head";

import withReduxStore from "@utils/with-redux-store";
import { Provider } from "react-redux";
import Main from "@containers/Main";

// eslint-disable-next-line react/prop-types
function MyApp({ Component, pageProps, reduxStore }) {
  return (
    <>
      <Head>
        <title>Plataforma de Participación Ciudadana</title>
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="preconnect" href={`${process.env.NEXT_PUBLIC_URL_API}`} />
        <meta
          name="description"
          content="Plataforma de Participación Ciudadana, Gobierno Autónomo Municipal de La Paz, Bolivia"
        />
      </Head>

      <Provider store={reduxStore}>
        <Main component={Component} pageProps={pageProps} />
      </Provider>
    </>
  );
}

export default withReduxStore(MyApp);
