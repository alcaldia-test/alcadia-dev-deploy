import React from "react";
import CompleteSignup from "@containers/Register/CompleteSignup";

const PageLogin = () => {
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <CompleteSignup />
      </div>
    </div>
  );
};

export default PageLogin;
