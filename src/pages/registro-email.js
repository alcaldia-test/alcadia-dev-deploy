import React from "react";
import RegisterEmail from "../containers/Register/components/RegisterEmail";

const indexPage = () => {
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <RegisterEmail />
      </div>
    </div>
  );
};
export default indexPage;
