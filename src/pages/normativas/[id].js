import React from "react";
import { useSelector } from "react-redux";
import Layout from "@containers/Wrapper/WrapperNologin";
import Chapters from "@containers/Normative/ChaptersFlow";
import { NormativesServicess } from "@services/normatives";
import { defaultValueStorage } from "@utils/with-redux-store";
import { getHostname } from "@utils/utils";
import SEO from "@components/SEO.js";
import { parseDraftToText } from "@utils/draftToHtml";

export const getServerSideProps = async ({ req, params }) => {
  const hostname = getHostname(req, true);
  const { auth } = await defaultValueStorage({ req }, hostname);
  const { id } = params;
  const normativesServices = new NormativesServicess();
  const normative = await normativesServices.getNormative({
    id,
    userId: auth?.dataUser?.id,
  });
  return {
    props: { normative, hostname },
  };
};

const Normativa = (props) => {
  const { normative, hostname } = props;
  const { id } = useSelector((state) => state.auth.dataUser);
  const { title, content } = normative || {};

  return (
    <>
      <SEO
        title={title}
        description={parseDraftToText(content)}
        image={"https://participa.lapaz.bo/images/banner.jpg"}
        route={`/normativas/${id}`}
      />
      <Layout>
        <Chapters normative={normative} hostname={hostname} />
      </Layout>
    </>
  );
};

export default Normativa;
