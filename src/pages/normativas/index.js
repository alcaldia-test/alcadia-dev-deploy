import React from "react";
import Layout from "@containers/Wrapper/WrapperNologin";
import NormativesContainer from "@containers/Normative";

const Normatives = () => {
  return (
    <Layout
      title="Normativa Colaborativa"
      description="Danos tu opinión en las siguientes normativas a implementar"
      defaultImg={true}
      currentRoute="/normativas"
    >
      <NormativesContainer />
    </Layout>
  );
};

export default Normatives;
