import Login from "@containers/Login";
import React from "react";

const PageLogin = () => {
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <Login />
      </div>
    </div>
  );
};

export default PageLogin;
