import Permissions from "@containers/Permissions";
import { initialRequestPermissionsStart } from "@redux/permissions/actions";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
const IndexPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(initialRequestPermissionsStart());
  }, []);

  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div
        className="w-screen h-screen bg-white md:w-10/12 grid md:justify-items-center"
        style={{
          paddingLeft: "15px",
          paddingRight: "15px",
          paddingtop: "40px",
          paddingBottom: "40px",
        }}
      >
        <Permissions />
      </div>
    </div>
  );
};

export default IndexPage;
