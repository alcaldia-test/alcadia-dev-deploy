import RegisterData from "@containers/Register/components/RegisterData";
import React from "react";
const IndexPage = () => {
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <RegisterData />
      </div>
    </div>
  );
};

export default IndexPage;
