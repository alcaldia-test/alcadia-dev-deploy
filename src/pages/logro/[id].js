import React from "react";
import Layout from "@containers/Wrapper/WrapperNologin";
import AchieveContainer from "@containers/AchieveContainer";

export default function Logro(props) {
  return (
    <Layout>
      <AchieveContainer {...props} />
    </Layout>
  );
}
