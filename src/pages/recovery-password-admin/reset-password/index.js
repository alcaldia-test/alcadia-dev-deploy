import RestartPassword from "@containers/RecoveryPasswordAdmin/RestartPassword";
import React from "react";

const resetPassword = () => {
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <RestartPassword />
      </div>
    </div>
  );
};
export default resetPassword;
