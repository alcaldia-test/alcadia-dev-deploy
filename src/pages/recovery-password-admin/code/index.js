import RecoveryCode from "@containers/RecoveryPasswordAdmin/RecoveryCode";
import React from "react";

const code = () => {
  return (
    <div className="h-screen bg-gray-200 grid md:justify-items-center">
      <div className="w-screen h-full bg-white md:w-10/12 grid md:justify-items-center px-3 py-5	">
        <RecoveryCode />
      </div>
    </div>
  );
};
export default code;
