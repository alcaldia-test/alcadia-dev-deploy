import Layout from "@containers/Wrapper/WrapperNologin";
import { defaultValueStorage } from "@utils/with-redux-store";
import { getHostname } from "@utils/utils";
import Container from "@containers/Announcements/Proposal";
import { announcementsServices } from "@services";
import { useDispatch } from "react-redux";
import { useMemo } from "react";
import { getProposalSuccess } from "@redux/announcements/actions";
import SEO from "@components/SEO.js";
import moment from "moment";

export const getServerSideProps = async ({ req, params }) => {
  const hostname = getHostname(req, true);
  const { auth } = await defaultValueStorage({ req }, hostname);
  const { id } = params;

  const proposal = await announcementsServices.getProposal({
    id,
    userId: auth?.dataUser?.id,
  });

  return {
    props: {
      proposal,
      id,
    },
  };
};

const Propuesta = ({ proposal, id }) => {
  const dispatch = useDispatch();

  useMemo(() => {
    dispatch(getProposalSuccess(proposal));
  }, [proposal]);

  return (
    <>
      <SEO
        title={proposal?.title}
        description={`Propuesta ciudadana que fue creada el ${moment(
          proposal?.createdAt
        ).format("DD/MM/YYYY")}`}
        image={!proposal?.fileLink ? undefined : proposal.fileLink}
        route={`/tu_voto_cuenta/${id}`}
      />
      <Layout currentRoute="/propuestas_ciudadanas">
        <Container id={id} />
      </Layout>
    </>
  );
};

export default Propuesta;
