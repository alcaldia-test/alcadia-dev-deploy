import React from "react";
import Layout from "@containers/Wrapper/WrapperNologin";

import Container from "@containers/Announcements/Proposal/Create";

const Convocatorias = (props) => {
  return (
    <>
      <Layout currentRoute="/propuestas_ciudadanas">
        <Container {...props} />
      </Layout>
    </>
  );
};

export default Convocatorias;
