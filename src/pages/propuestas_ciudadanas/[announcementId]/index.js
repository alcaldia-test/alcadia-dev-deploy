import Layout from "@containers/Wrapper/WrapperNologin";
import { defaultValueStorage } from "@utils/with-redux-store";
import { getHostname } from "@utils/utils";
import { announcementsServices } from "@services";

import Container from "@containers/Announcements/Announcement";
import SEO from "@components/SEO.js";
import moment from "moment";

export const getServerSideProps = async ({ req, params }) => {
  const hostname = getHostname(req, true);
  const { auth } = await defaultValueStorage({ req }, hostname);
  const { announcementId } = params;

  const announcement = await announcementsServices.getAnnouncement({
    announcementId,
    userId: auth?.dataUser?.id,
  });

  return {
    props: {
      announcement,
      announcementId,
    },
  };
};

const Propuesta = ({ announcementId, announcement }) => {
  return (
    <>
      <SEO
        title={`${announcement?.title}`}
        description={`Convocatoria para crear propuestas ciduadanas, los resultados serán publicados el ${moment(
          announcement?.postResults
        ).format("DD/MM/YYYY")} en la plataforma de participación ciudadana.`}
        image={announcement?.banner}
        route={`/propuestas_ciudadanas/${announcement}`}
      />
      <Layout currentRoute="/propuestas_ciudadanas">
        <Container
          announcementId={announcementId}
          announcement={announcement}
        />
      </Layout>
    </>
  );
};

export default Propuesta;
