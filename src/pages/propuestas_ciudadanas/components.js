import React, { useState } from "react";

import SimpleAreaChart from "@components/SimpleAreaChart";
import Checkboxes from "@components/Checkboxes";
import CardStats from "@components/CardStats";
import Select from "@components/Select";
import Autocomplete from "@components/Autocomplete";
import Footer from "@components/Footer";
import Stepper from "@components/Stepper";
import Share from "@components/Modals/Share";
import VotingOption from "@components/VotingOption";
import Menu from "@components/Menu";
import TextAreaModal from "@components/Modals/TextAreaModal";
import WYSIWYG from "@components/WYSIWYG";

const dataChart = [
  { name: "Jan", visitors: 4000 },
  { name: "Feb", visitors: 3000 },
  { name: "Mar", visitors: 2000 },
  { name: "Apr", visitors: 2780 },
  { name: "May", visitors: 1890 },
  { name: "Jun", visitors: 2390 },
  { name: "Jul", visitors: 3490 },
];

const optionsSelect = [
  { value: "1", label: "One" },
  { value: "2", label: "Two" },
  { value: "3", label: "Three" },
  { value: "4", label: "Four" },
  { value: "5", label: "Five" },
];

const dataAutocomplete = [
  {
    title: "Ubicación 1",
  },
  {
    title: "Ubicación 2",
  },
  {
    title: "Ubicación 3",
  },
];

const voitingResults = {
  title: "Logo N°1",
  totalVotes: 2000,
  votes: 1000,
  winner: true,
};

const menuNotifications = {
  calls: 1,
  talks: 1,
  regulations: 1,
  requests: 1,
};

const ProposalsPage = () => {
  const [showShare, setShowShare] = useState(false);
  const [showTextArea, setShowTextArea] = useState(false);

  return (
    <>
      <SimpleAreaChart
        title="Simple Area Chart"
        data={dataChart}
        xAxisKey="name"
        yAxisKey="visitors"
        color1="#1BC767"
        color2="#65C1D5"
        xAxisLabel="Month"
        yAxisLabel="Visitors"
        type="linear"
      />
      <Checkboxes active={true} type="primary" />
      <CardStats
        value="Card Stats"
        detail="Whatsapp"
        icon="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzYiIGhlaWdodD0iMzYiIHZpZXdCb3g9IjAgMCAzNiAzNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTEwLjg3OTUgMjcuNzQxMUwxMS45NjU1IDI4LjM3NTZDMTMuNzk2OSAyOS40NDM3IDE1Ljg3OTggMzAuMDA0NSAxOCAzMC4wMDAxQzIwLjM3MzMgMzAuMDAwMSAyMi42OTM0IDI5LjI5NjMgMjQuNjY2OCAyNy45Nzc4QzI2LjY0MDIgMjYuNjU5MiAyOC4xNzgzIDI0Ljc4NSAyOS4wODY1IDIyLjU5MjNDMjkuOTk0OCAyMC4zOTk2IDMwLjIzMjQgMTcuOTg2OCAyOS43Njk0IDE1LjY1OUMyOS4zMDY0IDEzLjMzMTMgMjguMTYzNSAxMS4xOTMxIDI2LjQ4NTIgOS41MTQ4NEMyNC44MDcgNy44MzY2MSAyMi42Njg4IDYuNjkzNzIgMjAuMzQxIDYuMjMwN0MxOC4wMTMzIDUuNzY3NjggMTUuNjAwNSA2LjAwNTMyIDEzLjQwNzggNi45MTM1N0MxMS4yMTUgNy44MjE4MiA5LjM0MDkgOS4zNTk4OSA4LjAyMjMyIDExLjMzMzNDNi43MDM3NSAxMy4zMDY3IDUuOTk5OTYgMTUuNjI2NyA1Ljk5OTk2IDE4LjAwMDFDNS45OTk5NiAyMC4xNTQxIDYuNTY1NDYgMjIuMjE5NiA3LjYyNTk2IDI0LjAzNjFMOC4yNTg5NiAyNS4xMjIxTDcuMjc5NDYgMjguNzIzNkwxMC44Nzk1IDI3Ljc0MTFaTTMuMDA1OTYgMzMuMDAwMUw1LjAzMzk2IDI1LjU0ODFDMy42OTc2NyAyMy4yNTc0IDIuOTk1NiAyMC42NTIxIDIuOTk5OTYgMTguMDAwMUMyLjk5OTk2IDkuNzE1NjIgOS43MTU0NiAzLjAwMDEyIDE4IDMuMDAwMTJDMjYuMjg0NSAzLjAwMDEyIDMzIDkuNzE1NjIgMzMgMTguMDAwMUMzMyAyNi4yODQ2IDI2LjI4NDUgMzMuMDAwMSAxOCAzMy4wMDAxQzE1LjM0OTEgMzMuMDA0NCAxMi43NDQ5IDMyLjMwMjkgMTAuNDU1IDMwLjk2NzZMMy4wMDU5NiAzMy4wMDAxWk0xMi41ODY1IDEwLjk2MjFDMTIuNzg3NSAxMC45NDcxIDEyLjk5IDEwLjk0NzEgMTMuMTkxIDEwLjk1NjFDMTMuMjcyIDEwLjk2MjEgMTMuMzUzIDEwLjk3MTEgMTMuNDM0IDEwLjk4MDFDMTMuNjcyNSAxMS4wMDcxIDEzLjkzNSAxMS4xNTI2IDE0LjAyMzUgMTEuMzUzNkMxNC40NzA1IDEyLjM2NzYgMTQuOTA1NSAxMy4zODkxIDE1LjMyNTUgMTQuNDEzNkMxNS40MTg1IDE0LjY0MTYgMTUuMzYzIDE0LjkzNDEgMTUuMTg2IDE1LjIxOTFDMTUuMDY0MiAxNS40MTE4IDE0LjkzMjYgMTUuNTk4MSAxNC43OTE1IDE1Ljc3NzFDMTQuNjIyIDE1Ljk5NDYgMTQuMjU3NSAxNi4zOTM2IDE0LjI1NzUgMTYuMzkzNkMxNC4yNTc1IDE2LjM5MzYgMTQuMTA5IDE2LjU3MDYgMTQuMTY2IDE2Ljc5MTFDMTQuMTg3IDE2Ljg3NTEgMTQuMjU2IDE2Ljk5NjYgMTQuMzE5IDE3LjA5ODZMMTQuNDA3NSAxNy4yNDExQzE0Ljc5MTUgMTcuODgxNiAxNS4zMDc1IDE4LjUzMTEgMTUuOTM3NSAxOS4xNDMxQzE2LjExNzUgMTkuMzE3MSAxNi4yOTMgMTkuNDk1NiAxNi40ODIgMTkuNjYyMUMxNy4xODQgMjAuMjgxNiAxNy45NzkgMjAuNzg3MSAxOC44MzcgMjEuMTYyMUwxOC44NDQ1IDIxLjE2NTFDMTguOTcyIDIxLjIyMDYgMTkuMDM2NSAyMS4yNTA2IDE5LjIyMjUgMjEuMzMwMUMxOS4zMTU1IDIxLjM2OTEgMTkuNDExNSAyMS40MDM2IDE5LjUwOSAyMS40MjkxQzE5LjYwOTcgMjEuNDU0OCAxOS43MTU4IDIxLjQ0OTkgMTkuODEzOCAyMS40MTUyQzE5LjkxMTggMjEuMzgwNSAxOS45OTczIDIxLjMxNzUgMjAuMDU5NSAyMS4yMzQxQzIxLjE0NTUgMTkuOTE4NiAyMS4yNDQ1IDE5LjgzMzEgMjEuMjUzNSAxOS44MzMxVjE5LjgzNjFDMjEuMzI4OSAxOS43NjU4IDIxLjQxODUgMTkuNzEyMyAyMS41MTYzIDE5LjY3OTVDMjEuNjE0MSAxOS42NDY2IDIxLjcxNzggMTkuNjM1MSAyMS44MjA1IDE5LjY0NTZDMjEuOTEwNSAxOS42NTE2IDIyLjAwMiAxOS42NjgxIDIyLjA4NiAxOS43MDU2QzIyLjg4MjUgMjAuMDcwMSAyNC4xODYgMjAuNjM4NiAyNC4xODYgMjAuNjM4NkwyNS4wNTkgMjEuMDMwMUMyNS4yMDYgMjEuMTAwNiAyNS4zMzk1IDIxLjI2NzEgMjUuMzQ0IDIxLjQyNzZDMjUuMzUgMjEuNTI4MSAyNS4zNTkgMjEuNjkwMSAyNS4zMjQ1IDIxLjk4NzFDMjUuMjc2NSAyMi4zNzU2IDI1LjE1OTUgMjIuODQyMSAyNS4wNDI1IDIzLjA4NjZDMjQuOTYyMyAyMy4yNTM1IDI0Ljg1NiAyMy40MDY0IDI0LjcyNzUgMjMuNTM5NkMyNC41NzYxIDIzLjY5ODUgMjQuNDEwNCAyMy44NDMyIDI0LjIzMjUgMjMuOTcxNkMyNC4xNzA5IDI0LjAxNzkgMjQuMTA4NCAyNC4wNjI5IDI0LjA0NSAyNC4xMDY2QzIzLjg1ODQgMjQuMjI1IDIzLjY2NjcgMjQuMzM1MSAyMy40NzA1IDI0LjQzNjZDMjMuMDg0MyAyNC42NDE4IDIyLjY1NzcgMjQuNzU5NSAyMi4yMjEgMjQuNzgxNkMyMS45NDM1IDI0Ljc5NjYgMjEuNjY2IDI0LjgxNzYgMjEuMzg3IDI0LjgwMjZDMjEuMzc1IDI0LjgwMjYgMjAuNTM1IDI0LjY3MjEgMjAuNTM1IDI0LjY3MjFDMTguNDAyMyAyNC4xMTEyIDE2LjQzIDIzLjA2MDMgMTQuNzc1IDIxLjYwMzFDMTQuNDM2IDIxLjMwNDYgMTQuMTIyNSAyMC45ODM2IDEzLjgwMTUgMjAuNjY0MUMxMi40NjY1IDE5LjMzNjYgMTEuNDU4NSAxNy45MDQxIDEwLjg0NjUgMTYuNTUxMUMxMC41MzMyIDE1Ljg4NzEgMTAuMzY0IDE1LjE2NDIgMTAuMzUgMTQuNDMwMUMxMC4zNDM4IDEzLjUxOTQgMTAuNjQxNSAxMi42MzI2IDExLjE5NiAxMS45MTAxQzExLjMwNTUgMTEuNzY5MSAxMS40MDkgMTEuNjIyMSAxMS41ODc1IDExLjQ1MjZDMTEuNzc4IDExLjI3MjYgMTEuODk4IDExLjE3NjYgMTIuMDI4NSAxMS4xMTA2QzEyLjIwMiAxMS4wMjM3IDEyLjM5MTMgMTAuOTcyNiAxMi41ODUgMTAuOTYwNkwxMi41ODY1IDEwLjk2MjFaIiBmaWxsPSIjODU4NTg1Ii8+Cjwvc3ZnPgo="
      />
      <Select options={optionsSelect} value="" />
      <Autocomplete
        dataSource={dataAutocomplete}
        placeholder="Busque la ubicación del domicilio"
      />
      <Footer></Footer>
      <Stepper steps={[1, 2, 3, 4, 5]} step={2} />
      <Share
        onCancel={() => setShowShare(false)}
        href="https://url.example"
        visible={showShare}
        type="link"
        cancelText={<span>Cancelar</span>}
        closable={false}
      />
      <VotingOption results={voitingResults} />
      <Menu
        collapsed={false}
        type="admin"
        notifications={menuNotifications}
        defaultSelectedKey="1"
      />
      <TextAreaModal
        onCancel={() => setShowTextArea(false)}
        visible={showTextArea}
        type="link"
        cancelText="Cerrar"
        closable={false}
        id="1"
        okText="Bloquear"
        placeholder="Contenido"
        text="Motivo"
        title="Motivo del bloqueo a usuario"
      />
      <WYSIWYG />
    </>
  );
};

export default ProposalsPage;
