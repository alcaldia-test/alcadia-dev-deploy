import React from "react";
import Layout from "@containers/Wrapper/WrapperNologin";

import Container from "@containers/Announcements";

const Convocatorias = (props) => {
  return (
    <>
      <Layout
        title="Propuestas Ciudadanas"
        description="Participa en la creación de propuestas para los siguientes proyectos"
        defaultImg={true}
        currentRoute="/propuestas_ciudadanas"
      >
        <Container {...props} />
      </Layout>
    </>
  );
};

export default Convocatorias;
