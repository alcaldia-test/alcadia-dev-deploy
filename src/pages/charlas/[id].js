import React from "react";
import Layout from "@containers/Wrapper/WrapperNologin";
import { Description } from "@containers/DiscussionsUser/Description";
import { discussionsServices } from "@services/discussions";
import { parseDraftToText } from "@utils/draftToHtml";
import SEO from "@components/SEO.js";
import { getHostname } from "@utils/utils";

export const getServerSideProps = async ({ req, params }) => {
  const hostname = getHostname(req, true);

  const { id } = params;
  const discussion = await discussionsServices.getDiscussion({
    idPost: id,
    userId: null,
  });
  return {
    props: { idPost: id, discussion, hostname },
  };
};

const DescriptionPage = (props) => {
  const { idPost, discussion, hostname } = props;
  const { title, content } = discussion || {};

  return (
    <>
      <SEO
        title={title}
        description={parseDraftToText(content)}
        image={"https://participa.lapaz.bo/images/banner.jpg"}
        route={`/charlas/${idPost}`}
      />
      <Layout>
        <Description discussion={discussion} hostname={hostname} />
      </Layout>
    </>
  );
};

export default DescriptionPage;
