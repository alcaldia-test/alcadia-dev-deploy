import React from "react";

import { DiscussionsUser } from "@containers/DiscussionsUser";
import WrapperNologin from "@containers/Wrapper/WrapperNologin";

const Charlas = (props) => {
  return (
    <WrapperNologin title="Charlas" description="Charlas">
      <DiscussionsUser {...props} />;
    </WrapperNologin>
  );
};

export default Charlas;
