import React from "react";
import Layout from "@containers/Wrapper/WrapperNologin";
import LandingContainer from "@containers/LandingUser";

const Index = (props) => {
  return (
    <Layout
      title="Inicio - Plataforma de Participación Ciudadana"
      description="Te invitamos a que propongas ideas para mejorar la ciudad"
      defaultImg={true}
    >
      <LandingContainer {...props} />
    </Layout>
  );
};

export default Index;
