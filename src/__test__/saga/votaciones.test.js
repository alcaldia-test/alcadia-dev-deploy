import sagaHelper from "redux-saga-testing";
import { put, call, all } from "redux-saga/effects";
import { VoteServices } from "@services/vote";

import { expect } from "@jest/globals";
import payload from "../../__mocks__/fakeUserMock";

import { getPostById, getPostsPerPage } from "@redux/votations/saga";
import {
  putAllVotes,
  putSingleVote,
  setTotalPosts,
} from "@redux/votations/actions";
require("jest-fetch-mock").enableMocks();
process.env.NEXT_PUBLIC_URL_API =
  "https://backalcaldia.blockchainconsultora.com/";

// eslint-disable-next-line react-hooks/exhaustive-deps
fetchMock.enableMocks();

describe("Contenido de votaciones", () => {
  const Services = new VoteServices();
  const OLD_ENV = process.env;
  beforeEach(() => {
    fetch.resetMocks();
    process.env = { ...OLD_ENV };
  });

  afterAll(() => {
    process.env = OLD_ENV;
  });
  const num = 0;
  const it = sagaHelper(getPostsPerPage(num));
  let response;

  it("debería mostrar un loader", (result) => {
    expect(result).toEqual(put({ type: "SHOW_LOADER" }));
  });
  it("debería traer votes", (result, num) => {
    expect(result).toEqual(Services.getVotes(num));
    return num;
  });
  it("debería traer el total de los votes", (result) => {
    response = Services.getTotalPosts();
    return response;
  });
  it("debería setear el total de los votes", (result, response) => {
    expect(result).toEqual(put(setTotalPosts(response)));
  });
  it("debería setear los votes", (result, num) => {
    expect(result).toEqual(put(putAllVotes(num)));
  });
  it("debería ocultar el loader", (result) => {
    expect(result).toEqual(put({ type: "HIDE_LOADER" }));
  });
});

describe("Contenido de votacion", () => {
  const Services = new VoteServices();
  const OLD_ENV = process.env;
  beforeEach(() => {
    fetch.resetMocks();
    process.env = { ...OLD_ENV };
  });

  afterAll(() => {
    process.env = OLD_ENV;
  });

  const it = sagaHelper(getPostById(payload));

  it("debería mostrar un loader", (result) => {
    expect(result).toEqual(put({ type: "SHOW_LOADER" }));
    return payload;
  });
  it("debería pedir el post por ID a la API", (result, payload) => {
    const fetch = Services.getVoteById(payload);
    expect(result).toEqual(fetch);
    return payload;
  });
  it("debería comprobar si el usuario tiene votos", (result, payload) => {
    const fetch = Services.userHasVoted(payload);
    expect(result).toEqual(fetch);
    return payload;
  });

  it("debería setear el Vote y si el usuario ha participado", async (result, payload) => {
    const response = await Services.getVoteById(payload);
    const suffrages = await Services.userHasVoted(payload);
    expect(result).toEqual(
      put(
        putSingleVote({
          postData: response,
          hasVoted: suffrages.some(
            (suffrage) => suffrage.voteId === response.id
          ),
        })
      )
    );
  });
});
