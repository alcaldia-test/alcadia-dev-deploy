import Avatar from "@components/Avatar";
import { screen, render } from "@testing-library/react";

const image = {
  src: "/images/logo.svg",
};

describe("Avatar", () => {
  it("must show an avatar", () => {
    render(<Avatar src={image.src} />);
    const avatar = screen.getByRole("img");
    expect(avatar).toHaveAttribute("src");
  });
});

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: function () {},
      removeListener: function () {},
    };
  };
