import {
  ConvocationCard,
  DashboardCard,
  ProposalCard,
  VotationsCard,
} from "@components/Cards";
import { render } from "@testing-library/react";

const cardProps = {
  imageSource: "/images/banner.jpg",
  title: "titulo",
  state: {
    event: "En Proceso",
    Value: "example",
  },
  zone: "Varias Zonas",
  description:
    "orep ipsum dolor a met orep ipsum dolor a metlorep ipsum dolor a met.",
  action: "Ver Más",
  comments: 40,
  reports: 40,
  author: {
    name: "John Doe",
    img: "https://upload.wikimedia.org/wikipedia/commons/5/5a/John_Doe%2C_born_John_Nommensen_Duchac.jpg",
  },
};

describe("Votation Card", () => {
  // Comprueba si el componente se renderiza

  it("must render", () => {
    const { getByText } = render(
      <VotationsCard props={cardProps}></VotationsCard>
    );
    expect(getByText("titulo")).toBeInTheDocument();
  });
});
describe("Dashboard Card", () => {
  // Comprueba si el componente se renderiza

  it("must render", () => {
    const { getByText } = render(<DashboardCard props={cardProps} />);
    expect(getByText("titulo")).toBeInTheDocument();
  });
});
describe("Proposals Card", () => {
  // Comprueba si el componente se renderiza

  it("must render", () => {
    const { getByText } = render(<ProposalCard props={cardProps} />);
    expect(getByText("titulo")).toBeInTheDocument();
  });
});
describe("Convocation Card", () => {
  // Comprueba si el componente se renderiza

  it("must render", () => {
    const { getByText } = render(
      <ConvocationCard props={cardProps}></ConvocationCard>
    );
    expect(getByText("titulo")).toBeInTheDocument();
  });
});

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: function () {},
      removeListener: function () {},
    };
  };
