import { screen, render, fireEvent } from "@testing-library/react";
import Button from "@components/Button";

describe("Button", () => {
  it("it must render", () => {
    const { getByText } = render(<Button>hola</Button>);
    expect(getByText(/hola/i)).toBeInTheDocument();
  });

  it("calls onClick prop when clicked", () => {
    const handleClick = jest.fn();
    render(<Button onClick={handleClick}>Click Me</Button>);
    fireEvent.click(screen.getByText(/click me/i));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
