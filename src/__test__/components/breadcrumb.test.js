import { screen, render } from "@testing-library/react";
import Breadcrumbs from "@components/BreadCrumb";

const pages = [
  {
    name: "Página1",
    path: "/Hola",
  },
];

describe("BreadCrumbs", () => {
  // Comprueba si el componente tiene un componente hijo

  it("must render anchors", () => {
    render(<Breadcrumbs pages={pages} />);
    const anchor = screen.getByRole("link");
    expect(anchor).toHaveAttribute("href");
  });
});
