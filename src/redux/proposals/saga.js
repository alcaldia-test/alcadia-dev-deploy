import {
  GET_ANNOUNCEMENTS,
  GET_BY_ACTIVES,
  GET_BY_MORE_RECENT,
  GET_BY_LESS_RECENT,
  GET_BY_MY_ZONES,
  GET_BY_FINISHED,
  GET_ANNOUNCEMENT,
} from "./constants";
import { getAnnouncementsSuccess, getAnnouncementSuccess } from "./actions";
import { put, call, takeLatest } from "@redux-saga/core/effects";
import { fetchCustom } from "@utils/fetch_custom";
import { customFilter } from "./filter";

// Single requests
export function* getAnnouncementRequestSaga({ payload }) {
  console.log("getAnnouncementRequestSaga", payload);
  try {
    const result = yield call(fetchCustom, {
      endpoint: `/announcements/${payload}`,
      method: "GET",
    });

    yield put(getAnnouncementSuccess(result));
  } catch (error) {
    console.log("Error en saga get announcement", error);
  }
}

// Multiple requests and filters
export function* getAnnouncementsRequestSaga({ payload }) {
  let result;
  const filter = customFilter(payload);

  try {
    result = yield call(fetchCustom, {
      endpoint: "/announcements",
      method: "GET",
      filter: filter,
    });

    yield put(getAnnouncementsSuccess(result));
  } catch (error) {
    console.log("Error en saga get announcement list", error);
  }

  yield put(getAnnouncementsSuccess(result));
}

export default function* rootSaga() {
  yield takeLatest(GET_ANNOUNCEMENTS, getAnnouncementsRequestSaga);
  yield takeLatest(GET_BY_ACTIVES, getAnnouncementsRequestSaga);
  yield takeLatest(GET_BY_MORE_RECENT, getAnnouncementsRequestSaga);
  yield takeLatest(GET_BY_LESS_RECENT, getAnnouncementsRequestSaga);
  yield takeLatest(GET_BY_MY_ZONES, getAnnouncementsRequestSaga);
  yield takeLatest(GET_BY_FINISHED, getAnnouncementsRequestSaga);
  yield takeLatest(GET_ANNOUNCEMENT, getAnnouncementRequestSaga);
}
