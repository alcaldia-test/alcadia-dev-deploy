import {
  GET_ANNOUNCEMENT,
  GET_ANNOUNCEMENT_SUCCESS,
  GET_ANNOUNCEMENTS,
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_BY_ACTIVES,
  GET_BY_MORE_RECENT,
  GET_BY_LESS_RECENT,
  GET_BY_MY_ZONES,
  GET_BY_FINISHED,
} from "./constants";

// Single requests
export const getAnnouncement = (payload) => ({
  type: GET_ANNOUNCEMENT,
  payload,
});

export const getAnnouncementSuccess = (payload) => ({
  type: GET_ANNOUNCEMENT_SUCCESS,
  payload,
});

// export const getPost = (payload) => ({
//   type: GET_POST,
//   payload,
// });

// export const getPostSuccess = (payload) => ({
//   type: GET_POST_SUCCESS,
//   payload,
// });

// Multiple requests
export const getAnnouncements = (payload) => ({
  type: GET_ANNOUNCEMENTS,
  payload,
});

export const getAnnouncementsSuccess = (payload) => ({
  type: GET_ANNOUNCEMENTS_SUCCESS,
  payload,
});

// Filters
export const getAnnouncementsByActives = (payload) => ({
  type: GET_BY_ACTIVES,
  payload,
});

export const getAnnouncementsByMoreRecent = (payload) => ({
  type: GET_BY_MORE_RECENT,
  payload,
});

export const getAnnouncementsByLessRecent = (payload) => ({
  type: GET_BY_LESS_RECENT,
  payload,
});

export const getAnnouncementsByMyZones = (payload) => ({
  type: GET_BY_MY_ZONES,
  payload,
});

export const getAnnouncementsByFinished = (payload) => ({
  type: GET_BY_FINISHED,
  payload,
});
