import {
  GET_ANNOUNCEMENT_SUCCESS,
  GET_ANNOUNCEMENTS_SUCCESS,
} from "./constants";

const initialState = {
  announcements: [],
  announcement: {},
  count: 0,
  sectionsSizes: {
    card: 290,
  },
};

export default function announcements(state = initialState, action) {
  switch (action.type) {
    case GET_ANNOUNCEMENTS_SUCCESS:
      return {
        ...state,
        announcements: action.payload,
        count: action.payload.length,
      };
    case GET_ANNOUNCEMENT_SUCCESS:
      return { ...state, announcement: action.payload };
    default:
      return state;
  }
}
