import {
  GET_ALL_VOTES,
  GET_SINGLE_VOTE,
  PUT_ALL_VOTES,
  PUT_SINGLE_VOTE,
  SEARCH_BY_ORDER,
  SEARCH_BY_STATUS,
  SEARCH_BY_TITLE,
  SEARCH_BY_ZONE,
  SET_TOTAL_POSTS,
  SET_VOTED,
  SUFFRAGE_VOTE_USER,
} from "./constants";

export const getAllVotes = (payload) => ({
  type: GET_ALL_VOTES,
  payload,
});
export const putAllVotes = (payload) => ({
  type: PUT_ALL_VOTES,
  payload,
});
export const getSingleVote = (payload) => ({
  type: GET_SINGLE_VOTE,
  payload,
});
export const putSingleVote = (payload) => ({
  type: PUT_SINGLE_VOTE,
  payload,
});

export const setTotalPosts = (payload) => ({
  type: SET_TOTAL_POSTS,
  payload,
});

export const fakeDataUser = (payload) => ({
  type: "SET_FAKE_DATA",
  payload,
});

export const suffrageVoteUser = (payload) => ({
  type: SUFFRAGE_VOTE_USER,
  payload,
});
export const setVoted = (payload) => ({
  type: SET_VOTED,
  payload,
});

export const getPostsByTitle = (payload) => ({
  type: SEARCH_BY_TITLE,
  action: {
    type: SEARCH_BY_TITLE,
    payload,
  },
});
export const searchByOrder = (payload) => ({
  type: SEARCH_BY_ORDER,
  action: {
    type: SEARCH_BY_ORDER,
    payload,
  },
});
export const searchByStatus = (payload) => ({
  type: SEARCH_BY_STATUS,
  action: {
    type: SEARCH_BY_STATUS,
    payload,
  },
});
export const searchByZone = (payload) => ({
  type: SEARCH_BY_ZONE,
  action: {
    type: SEARCH_BY_ZONE,
    payload,
  },
});
