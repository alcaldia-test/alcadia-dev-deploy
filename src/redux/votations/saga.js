import { call, put, takeEvery, takeLatest } from "@redux-saga/core/effects";
import { hideLoader, showLoader } from "@redux/common/actions";
import { VoteServices } from "@services/vote";
import fetchWrap from "@utils/client";

import { putAllVotes, putSingleVote, setTotalPosts, setVoted } from "./actions";
import {
  GET_ALL_VOTES,
  GET_SINGLE_VOTE,
  SEARCH_BY_ORDER,
  SEARCH_BY_STATUS,
  SEARCH_BY_TITLE,
  SUFFRAGE_VOTE_USER,
} from "./constants";

export function* getPostsPerPage({ payload }) {
  const Services = new VoteServices();

  try {
    yield put(showLoader());
    const posts = yield Services.getVotes(payload);
    const { count } = yield Services.getTotalPosts();
    yield put(setTotalPosts(count));
    yield put(putAllVotes(posts));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(hideLoader());
  }
}
export function* getPostById({ payload }) {
  const Services = new VoteServices();

  try {
    yield put(showLoader());
    const response = yield Services.getVoteById(payload.voteId);
    const suffrages = yield Services.userHasVoted(payload.userId);

    yield put(
      putSingleVote({
        postData: response,
        hasVoted: suffrages.some((suffrage) => suffrage.voteId === response.id),
      })
    );
  } catch (error) {
    console.log(error);
  } finally {
    yield put(hideLoader());
  }
}

export function* getByFilters({ action }) {
  const Services = new VoteServices();
  let response;
  try {
    yield put(showLoader());
    if (action.type === "SEARCH_BY_TITLE") {
      response = yield Services.getPostsByTitle(action.payload);
    }
    if (action.type === "SEARCH_BY_ORDER") {
      response = yield Services.getPostsInOrder(action.payload);
    }
    if (action.type === "SEARCH_BY_STATUS") {
      response = yield Services.getPostsByStatus(action.payload);
    }
    if (action.type === "SEARCH_BY_ZONE") {
      response = yield Services.getPostsByStatus(action.payload);
    }
    if (response) yield put(putAllVotes(response));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(hideLoader());
  }
}

export function* postSuffrageOfLoggedUser({ payload }) {
  try {
    yield put(showLoader());
    yield call(fetchWrap, "/suffrages", {
      method: "POST",
      body: payload,
      token: false,
    });
    yield put(setVoted(true));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(hideLoader());
  }
}

export default function* rootSaga() {
  yield takeLatest(SEARCH_BY_TITLE, getByFilters);
  yield takeEvery(SEARCH_BY_ORDER, getByFilters);
  yield takeEvery(SEARCH_BY_STATUS, getByFilters);
  yield takeLatest(GET_ALL_VOTES, getPostsPerPage);
  yield takeLatest(GET_SINGLE_VOTE, getPostById);
  yield takeLatest(SUFFRAGE_VOTE_USER, postSuffrageOfLoggedUser);
}
