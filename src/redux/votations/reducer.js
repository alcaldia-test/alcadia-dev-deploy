import {
  PUT_SINGLE_VOTE,
  PUT_ALL_VOTES,
  SET_TOTAL_POSTS,
  SET_VOTED,
} from "./constants";

export const initialState = {
  allVotes: [],
  totalVotes: 0,
  order: {
    dir: null,
    propertyName: null,
  },
  postsStatus: null,
  singleVoteContent: {
    hasVoted: false,
    postData: {},
  },
};

const posts = (state = initialState, action) => {
  if (action.type === PUT_ALL_VOTES) {
    return { ...state, allVotes: action.payload };
  }
  if (action.type === PUT_SINGLE_VOTE) {
    return {
      ...state,
      singleVoteContent: action.payload,
    };
  }
  if (action.type === SET_TOTAL_POSTS) {
    return { ...state, totalVotes: action.payload };
  }
  if (action.type === SET_VOTED) {
    state.singleVoteContent.hasVoted = action.payload;
    return { ...state };
  } else {
    return state;
  }
};
export default posts;
