import {
  LOADING_GLOBAL_HIDE,
  LOADING_GLOBAL_SHOW,
  REQUEST_GLOBAL_SUCCESS,
  CHANGE_CURRENT_PAGE_SUCCES,
  DELETE_BANNER_SUCCESS,
  PATCH_BANNER_SUCCESS,
  PATCH_LANDING_SUCCESS,
  PATCH_PTUTORIAL_SUCCESS,
  INITIAL_REQUEST_SECRET_PASS_SUCCESS,
  PATCH_SECRET_PASS_SUCCESS,
  GET_NOTIFICATION_SUCCESS,
  EMPTY_NOTIFICATION_ARRAY,
  INCREASE_USER_NOTIFICATION,
  DECREASE_USER_NOTIFICATION,
  NOTITFICATION_COUNT_END,
} from "./constants";

const initialState = {
  banners: [],
  tags: [],
  tutorials: [],
  userNotifications: [],
  notifications: {
    client: 0,
    accessControl: 0,
  },
  worldTime: new Date().toISOString(),
  currentPage: "",
  secretPass: "",
};

export function globalReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_GLOBAL_SUCCESS:
      return { ...state, ...action.payload };
    case LOADING_GLOBAL_SHOW:
      return { ...state, loading: true };
    case DELETE_BANNER_SUCCESS:
      return { ...state, banners: action.payload };
    case PATCH_BANNER_SUCCESS:
      return { ...state, banners: action.payload };
    case PATCH_PTUTORIAL_SUCCESS:
      return { ...state, tutorials: action.payload };
    case LOADING_GLOBAL_HIDE:
      return { ...state, loading: false };
    case PATCH_LANDING_SUCCESS:
      return { ...state, banners: action.payload };
    case CHANGE_CURRENT_PAGE_SUCCES:
      return { ...state, loading: false, ...action.payload };
    case INITIAL_REQUEST_SECRET_PASS_SUCCESS:
      return { ...state, secretPass: action.payload };
    case PATCH_SECRET_PASS_SUCCESS:
      return { ...state, secretPass: action.payload };
    case EMPTY_NOTIFICATION_ARRAY:
      return { ...state, userNotifications: [] };
    case NOTITFICATION_COUNT_END:
      return { ...state, notifications: action.payload };
    case INCREASE_USER_NOTIFICATION:
      state.notifications.client++;
      return { ...state };
    case DECREASE_USER_NOTIFICATION:
      state.notifications.client--;
      return { ...state };
    case GET_NOTIFICATION_SUCCESS:
      return {
        ...state,
        userNotifications: [...state.userNotifications, ...action.payload],
      };
    default:
      return state;
  }
}

export default globalReducer;
