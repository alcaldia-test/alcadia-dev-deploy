import { call, put, takeLatest, select, all } from "redux-saga/effects";

import {
  REQUEST_GLOBAL_START,
  CHANGE_CURRENT_PAGE,
  DELETE_BANNER_START,
  PATCH_BANNER_START,
  PATCH_LANDING_START,
  DELETE_PTUTORIAL_START,
  PATCH_PTUTORIAL_START,
  INITIAL_REQUEST_SECRET_PASS_START,
  PATCH_SECRET_PASS_START,
  GET_NOTIFICATION_START,
  DELETE_NOTIFICATION_START,
} from "./constants";
import {
  requestGlobalSuccess,
  changeCurrentPageSuccess,
  changeLandingSuccess,
  changePTuotialSuccess,
  initialRequestSecretPassSuccess,
  patchSecretPassSuccess,
  getNotificationSuccess,
  getNotificationStart,
} from "@redux/global/actions";

import request, {
  deleteOptions,
  getOptions,
  getOptionsWithoutToken,
  patchOptions,
  postOptions,
  postOptionsFormData,
} from "@utils/request";
import { hideLoader, showLoader } from "@redux/common/actions";

import { landingServices } from "@services/landing";

export function* requestGlobal() {
  try {
    const options = getOptionsWithoutToken();
    const url = `${process.env.NEXT_PUBLIC_URL_API}/settings`;
    const settings = yield call(request, url, options);

    const bannerTypes = [
      "landing",
      "discussion",
      "announcement",
      "voting",
      "normative",
    ];
    const banners = settings.filter(({ key }) => bannerTypes.includes(key));
    const tags = settings.filter(({ key }) => key === "tags");
    const tutorials = settings.filter(({ key }) => key === "proposal-tutorial");

    yield put(
      requestGlobalSuccess({
        banners,
        tags: tags[0].content,
        tutorials,
      })
    );
  } catch (err) {
    console.log(err);
  }
}

export function* changeCurrentPage({ payload }) {
  console.log(payload);
  yield put(changeCurrentPageSuccess({ currentPage: payload }));
}

export function* changeBanner({
  payload: { file, id, fileId, resolve, reject },
}) {
  let { banners } = yield select((storage) => storage.global);
  let url, options, msg;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    url = `${server}/file-storages/${fileId}`;
    options = deleteOptions();
    yield call(request, url, options);

    url = `${server}/containers/setting/upload/banner`;
    const formdata = new FormData();
    formdata.append(id, file);

    options = postOptionsFormData(formdata);
    const [newfile] = yield call(request, url, options);

    for (const banner of banners) {
      if (banner?.id === id) {
        const currentFiles = banner?.fileStorages;

        if (fileId)
          currentFiles?.forEach((fl) => {
            if (fl.id === fileId) {
              fl.id = newfile.id;
              fl.link = newfile.link;
            }
          });
        else
          currentFiles?.unshift({
            id: newfile.id,
            link: newfile.link,
            settingId: newfile.settingId,
          });

        break;
      }
    }

    msg = msg
      ? "¡Datos e imagen guardados exitosamente!"
      : "¡La imagen se guardó exitosamente!";

    banners = banners.concat([]);

    yield all([
      put(changeLandingSuccess(banners)),
      call(resolve, msg ?? "Sin cambios"),
    ]);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export function* deleteBanner({ payload: { id, fileId, resolve, reject } }) {
  let { banners } = yield select((storage) => storage.global);
  let url, options;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    url = `${server}/file-storages/${fileId}`;
    options = deleteOptions();
    yield call(request, url, options);

    for (const banner of banners) {
      if (banner?.id === id) {
        const currentFiles = banner?.fileStorages;

        currentFiles?.forEach((fl, i) => {
          if (fl.id === fileId) currentFiles.splice(i, 1);
        });

        break;
      }
    }

    banners = banners.concat([]);

    yield all([
      put(changeLandingSuccess(banners)),
      call(resolve, "Se eliminó exitosamente"),
    ]);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export function* changeLandig({
  payload: { file, changedData, id, fileId, resolve, reject, ...others },
}) {
  let { banners } = yield select((storage) => storage.global);
  let url, options, msg;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    // yield put(showLoader());
    if (changedData) {
      url = `${server}/settings/${id}`;
      options = patchOptions(others);
      yield call(request, url, options);

      for (const i in banners) {
        if (banners[i]?.id === id) {
          const currentBanner = banners[i];
          banners.splice(i, 1);

          banners = [{ ...currentBanner, ...others }, ...banners];
          break;
        }
      }

      msg = "¡Datos guardados exitosamente!";
    }

    if (file) {
      if (fileId) {
        url = `${server}/file-storages/${fileId}`;
        options = deleteOptions();
        yield call(request, url, options);
      }

      url = `${server}/containers/setting/upload/banner`;
      const formdata = new FormData();
      formdata.append(id, file);

      options = postOptionsFormData(formdata);
      const [newfile] = yield call(request, url, options);

      for (const banner of banners) {
        if (banner?.id === id) {
          const currentFiles = banner?.fileStorages;

          if (fileId)
            currentFiles?.forEach((fl) => {
              if (fl.id === fileId) {
                fl.id = newfile.id;
                fl.link = newfile.link;
              }
            });
          else
            currentFiles?.unshift({
              id: newfile.id,
              link: newfile.link,
              settingId: newfile.settingId,
            });

          break;
        }
      }

      msg = msg
        ? "¡Datos e imagen guardados exitosamente!"
        : "¡Imagen guardada exitosamente!";

      banners = banners.concat([]);
    }

    yield all([
      put(changeLandingSuccess(banners)),
      call(resolve, msg ?? "Sin cambios"),
    ]);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export function* changePTutorial({
  payload: { resolve, reject, id, ...others },
}) {
  let { tutorials } = yield select((storage) => storage.global);
  let url, options;
  const server = process.env.NEXT_PUBLIC_URL_API;
  let msg;

  try {
    if (id) {
      url = `${server}/settings/${id}`;
      options = patchOptions(others);
      yield call(request, url, options);

      for (const tuto of tutorials) {
        if (tuto?.id === id) tuto.content = others.content;
      }

      msg = "Se actualizó exitosamente el tutorial";
    } else {
      url = `${server}/settings`;
      options = postOptions(others);
      const newdata = yield call(request, url, options);

      tutorials.push(newdata);
      tutorials = tutorials.concat([]);
      msg = "Se agrego exitosamente el tutorial";
    }

    yield all([put(changePTuotialSuccess(tutorials)), call(resolve, msg)]);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export function* deletePTutorial({ payload: { id, resolve, reject } }) {
  let { tutorials } = yield select((storage) => storage.global);
  let url, options;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    url = `${server}/settings/${id}`;
    options = deleteOptions();
    yield call(request, url, options);

    tutorials?.forEach(({ id: extId }, i) => {
      if (extId === id) tutorials.splice(i, 1);
    });

    tutorials = tutorials.concat([]);

    yield all([
      put(changePTuotialSuccess(tutorials)),
      call(resolve, "Se eliminó exitosamente"),
    ]);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export function* requestSecretPasswordStart({ payload }) {
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/users/admin/phrase-secret`;
    const options = getOptions();
    const settings = yield call(request, url, options);
    yield all([put(initialRequestSecretPassSuccess(settings))]);
    yield put(hideLoader());
  } catch (err) {
    yield put(hideLoader());
    console.log(err);
  }
}

export function* patchSecretPasSaga({ payload: { resolve, reject, code } }) {
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/access-controls/phrase-secret`;
    const options = patchOptions(code);
    const settings = yield call(request, url, options);
    yield all([put(patchSecretPassSuccess(settings))]);
    yield call(resolve, "Se modificó con éxito");
    yield put(hideLoader());
  } catch (err) {
    yield put(hideLoader());
    yield call(reject, "No se pudo cambiar la palabra secreta");
  }
}

export function* getNotifications({ payload: { resolve, reject, ...others } }) {
  const {
    dataUser: { id },
  } = yield select((storage) => storage.auth);

  try {
    const allTasks = [call(resolve, undefined)];

    if (id) {
      const notifications = yield landingServices.getNotifies({
        userId: id,
        ...others,
      });

      allTasks.push(put(getNotificationSuccess(notifications)));
    }

    yield all(allTasks);
  } catch (err) {
    yield call(reject, "Tenemos problemas.");
    console.log(err);
  }
}

export function* deleteNotifications({ payload: { resolve, reject, id } }) {
  const server = process.env.NEXT_PUBLIC_URL_API;
  let url, options;

  try {
    url = `${server}/notifications/${id}`;
    options = deleteOptions();
    yield call(request, url, options);

    yield put(getNotificationStart({ resolve, reject }));
  } catch (err) {
    yield call(reject, "Tenemos problemas.");
    console.log(err);
  }
}

export default function* rootSaga() {
  yield takeLatest(REQUEST_GLOBAL_START, requestGlobal);
  yield takeLatest(CHANGE_CURRENT_PAGE, changeCurrentPage);
  yield takeLatest(DELETE_BANNER_START, deleteBanner);
  yield takeLatest(PATCH_BANNER_START, changeBanner);
  yield takeLatest(PATCH_LANDING_START, changeLandig);
  yield takeLatest(DELETE_PTUTORIAL_START, deletePTutorial);
  yield takeLatest(PATCH_PTUTORIAL_START, changePTutorial);
  yield takeLatest(GET_NOTIFICATION_START, getNotifications);
  yield takeLatest(DELETE_NOTIFICATION_START, deleteNotifications);
  yield takeLatest(
    INITIAL_REQUEST_SECRET_PASS_START,
    requestSecretPasswordStart
  );
  yield takeLatest(PATCH_SECRET_PASS_START, patchSecretPasSaga);
}
