import {
  LOADING_GLOBAL_HIDE,
  LOADING_GLOBAL_SHOW,
  REQUEST_GLOBAL_START,
  REQUEST_GLOBAL_SUCCESS,
  CHANGE_CURRENT_PAGE,
  CHANGE_CURRENT_PAGE_SUCCES,
  PATCH_BANNER_START,
  PATCH_BANNER_SUCCESS,
  DELETE_BANNER_START,
  DELETE_BANNER_SUCCESS,
  PATCH_LANDING_START,
  PATCH_LANDING_SUCCESS,
  DELETE_PTUTORIAL_START,
  DELETE_PTUTORIAL_SUCCESS,
  PATCH_PTUTORIAL_START,
  PATCH_PTUTORIAL_SUCCESS,
  INITIAL_REQUEST_SECRET_PASS_START,
  INITIAL_REQUEST_SECRET_PASS_SUCCESS,
  PATCH_SECRET_PASS_START,
  PATCH_SECRET_PASS_SUCCESS,
  GET_NOTIFICATION_START,
  GET_NOTIFICATION_SUCCESS,
  DELETE_NOTIFICATION_START,
  EMPTY_NOTIFICATION_ARRAY,
  INCREASE_USER_NOTIFICATION,
  DECREASE_USER_NOTIFICATION,
  NOTITFICATION_COUNT_END,
} from "./constants";

export const notificationCountSuccess = (payload) => ({
  type: NOTITFICATION_COUNT_END,
  payload: payload,
});

export const increaseUserNotify = () => ({
  type: INCREASE_USER_NOTIFICATION,
});
export const decreaseUserNotify = () => ({
  type: DECREASE_USER_NOTIFICATION,
});

export const emptyNotification = () => ({
  type: EMPTY_NOTIFICATION_ARRAY,
});

export const deleteNotificationStart = (payload) => ({
  type: DELETE_NOTIFICATION_START,
  payload: payload,
});

export const getNotificationStart = (payload) => ({
  type: GET_NOTIFICATION_START,
  payload: payload,
});
export const getNotificationSuccess = (payload) => ({
  type: GET_NOTIFICATION_SUCCESS,
  payload: payload,
});

export const deletePTuotialStart = (payload) => ({
  type: DELETE_PTUTORIAL_START,
  payload: payload,
});
export const deletePTuotialSuccess = (payload) => ({
  type: DELETE_PTUTORIAL_SUCCESS,
  payload: payload,
});
export const changePTuotialStart = (payload) => ({
  type: PATCH_PTUTORIAL_START,
  payload: payload,
});
export const changePTuotialSuccess = (payload) => ({
  type: PATCH_PTUTORIAL_SUCCESS,
  payload: payload,
});

export const requestGlobalStart = (payload) => ({
  type: REQUEST_GLOBAL_START,
  payload: payload,
});
export const requestGlobalSuccess = (payload) => ({
  type: REQUEST_GLOBAL_SUCCESS,
  payload: payload,
});

export const loadingGlobalStart = (payload) => ({
  type: LOADING_GLOBAL_SHOW,
  payload: payload,
});
export const loadingGlobalSuccess = (payload) => ({
  type: LOADING_GLOBAL_HIDE,
  payload: payload,
});
export const changeCurrentPage = (payload) => ({
  type: CHANGE_CURRENT_PAGE,
  payload: payload,
});
export const changeCurrentPageSuccess = (payload) => ({
  type: CHANGE_CURRENT_PAGE_SUCCES,
  payload: payload,
});

export const changeBannerStart = (payload) => ({
  type: PATCH_BANNER_START,
  payload: payload,
});
export const changeBannerSuccess = (payload) => ({
  type: PATCH_BANNER_SUCCESS,
  payload: payload,
});

export const deleteBannerStart = (payload) => ({
  type: DELETE_BANNER_START,
  payload: payload,
});
export const deleteBannerSuccess = (payload) => ({
  type: DELETE_BANNER_SUCCESS,
  payload: payload,
});

export const changeLandingStart = (payload) => ({
  type: PATCH_LANDING_START,
  payload: payload,
});
export const changeLandingSuccess = (payload) => ({
  type: PATCH_LANDING_SUCCESS,
  payload: payload,
});

export const initialRequestSecretPassStart = (payload) => ({
  type: INITIAL_REQUEST_SECRET_PASS_START,
  payload: payload,
});
export const initialRequestSecretPassSuccess = (payload) => ({
  type: INITIAL_REQUEST_SECRET_PASS_SUCCESS,
  payload: payload,
});

export const patchSecretPassStart = (payload) => ({
  type: PATCH_SECRET_PASS_START,
  payload: payload,
});
export const patchSecretPassSuccess = (payload) => ({
  type: PATCH_SECRET_PASS_SUCCESS,
  payload: payload,
});
