import {
  SET_NORMATIVE_DATA,
  SET_NORMATIVE_DATA_SUCCESS,
  POST_NORMATIVE_DATA,
  POST_NORMATIVE_DATA_SUCCESS,
  UPDATE_NORMATIVE_DATA,
  UPDATE_NORMATIVE_DATA_SUCCESS,
  POST_NORMATIVE_CHAPTER,
  POST_NORMATIVE_CHAPTER_SUCCESS,
  UPDATE_NORMATIVE_CHAPTER,
  UPDATE_NORMATIVE_CHAPTER_SUCCESS,
  DELETE_NORMATIVE_DATA,
  DELETE_NORMATIVE_DATA_SUCCESS,
  PUBLISH_NORMATIVE,
  PUBLISH_NORMATIVE_SUCCESS,
  SET_EDITING_NORMATIVE,
  SET_EDITING_NORMATIVE_SUCCESS,
  GET_NORMATIVE_EDITING_DATA,
  GET_NORMATIVE_EDITING_DATA_SUCCESS,
  DELETE_CHAPTER,
  DELETE_CHAPTER_SUCCESS,
  DELETE_ARTICLE,
  DELETE_ARTICLE_SUCCESS,
  FLUSH_DATA,
  FLUSH_DATA_SUCCESS,
} from "./constants";

export const setNormativeData = (payload) => ({
  type: SET_NORMATIVE_DATA,
  payload,
});

export const setNormativeDataSuccess = (payload) => ({
  type: SET_NORMATIVE_DATA_SUCCESS,
  payload,
});

export const postNormativeData = (payload) => ({
  type: POST_NORMATIVE_DATA,
  payload,
});

export const postNormativeDataSuccess = (payload) => ({
  type: POST_NORMATIVE_DATA_SUCCESS,
  payload,
});

export const updateNormativeData = (payload) => ({
  type: UPDATE_NORMATIVE_DATA,
  payload,
});

export const updateNormativeDataSuccess = (payload) => ({
  type: UPDATE_NORMATIVE_DATA_SUCCESS,
  payload,
});

export const postNormativeChapter = (payload) => ({
  type: POST_NORMATIVE_CHAPTER,
  payload,
});

export const postNormativeChapterSuccess = (payload) => ({
  type: POST_NORMATIVE_CHAPTER_SUCCESS,
  payload,
});

export const updateNormativeChapter = (payload) => ({
  type: UPDATE_NORMATIVE_CHAPTER,
  payload,
});

export const updateNormativeChapterSuccess = (payload) => ({
  type: UPDATE_NORMATIVE_CHAPTER_SUCCESS,
  payload,
});

export const deleteNormative = (payload) => ({
  type: DELETE_NORMATIVE_DATA,
  payload,
});

export const deleteNormativeSuccess = (payload) => ({
  type: DELETE_NORMATIVE_DATA_SUCCESS,
  payload,
});

export const plublishNormative = (payload) => ({
  type: PUBLISH_NORMATIVE,
  payload,
});

export const plublishNormativeSuccess = (payload) => ({
  type: PUBLISH_NORMATIVE_SUCCESS,
  payload,
});

export const setEditingNormative = (payload) => ({
  type: SET_EDITING_NORMATIVE,
  payload,
});

export const setEditingNormativeSuccess = (payload) => ({
  type: SET_EDITING_NORMATIVE_SUCCESS,
  payload,
});

export const getNormativeEditingData = (payload) => ({
  type: GET_NORMATIVE_EDITING_DATA,
  payload,
});

export const getNormativeEditingDataSuccess = (payload) => ({
  type: GET_NORMATIVE_EDITING_DATA_SUCCESS,
  payload,
});

export const deleteChapter = (payload) => ({
  type: DELETE_CHAPTER,
  payload,
});

export const deleteChapterSuccess = (payload) => ({
  type: DELETE_CHAPTER_SUCCESS,
  payload,
});

export const deleteArticle = (payload) => ({
  type: DELETE_ARTICLE,
  payload,
});

export const deleteArticleSuccess = (payload) => ({
  type: DELETE_ARTICLE_SUCCESS,
  payload,
});

export const flushCreatingData = () => ({
  type: FLUSH_DATA,
  payload: "",
});

export const flushCreatingDataSuccess = (payload) => ({
  type: FLUSH_DATA_SUCCESS,
  payload,
});
