import {
  SET_NORMATIVE_DATA_SUCCESS,
  POST_NORMATIVE_DATA_SUCCESS,
  UPDATE_NORMATIVE_DATA_SUCCESS,
  POST_NORMATIVE_CHAPTER_SUCCESS,
  UPDATE_NORMATIVE_CHAPTER_SUCCESS,
  DELETE_NORMATIVE_DATA_SUCCESS,
  PUBLISH_NORMATIVE_SUCCESS,
  SET_EDITING_NORMATIVE_SUCCESS,
  GET_NORMATIVE_EDITING_DATA_SUCCESS,
  DELETE_CHAPTER_SUCCESS,
  DELETE_ARTICLE_SUCCESS,
  FLUSH_DATA_SUCCESS,
} from "./constants";

const initialState = {
  title: "",
  description: "",
  currentNormativeEditingId: "",
  chapters: [],
  editing: {
    normativeId: "",
    title: "",
    description: {
      text: "",
      rawData: {},
    },
    tags: [],
    files: [],
    chapters: [],
    state: "CREATING",
  },
};

export function normatives(state = initialState, action) {
  switch (action.type) {
    case SET_NORMATIVE_DATA_SUCCESS:
      return { ...state, ...action.payload };
    case POST_NORMATIVE_DATA_SUCCESS:
      return { ...state, ...action.payload };
    case UPDATE_NORMATIVE_DATA_SUCCESS:
      return { ...state, ...action.payload };
    case POST_NORMATIVE_CHAPTER_SUCCESS:
      return { ...state, ...action.payload };
    case UPDATE_NORMATIVE_CHAPTER_SUCCESS:
      return { ...state, ...action.payload };
    case DELETE_NORMATIVE_DATA_SUCCESS:
      return { ...state, ...action.payload };
    case SET_EDITING_NORMATIVE_SUCCESS:
      return { ...state, ...action.payload };
    case GET_NORMATIVE_EDITING_DATA_SUCCESS:
      return { ...state, ...action.payload };
    case DELETE_CHAPTER_SUCCESS:
      return { ...state, ...action.payload };
    case DELETE_ARTICLE_SUCCESS:
      return { ...state };
    case PUBLISH_NORMATIVE_SUCCESS:
      return { ...initialState };
    case FLUSH_DATA_SUCCESS:
      return { ...action.payload };
    default:
      return { ...state };
  }
}

export default normatives;
