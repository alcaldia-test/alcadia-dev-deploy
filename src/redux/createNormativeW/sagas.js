import {
  SET_NORMATIVE_DATA,
  POST_NORMATIVE_CHAPTER,
  UPDATE_NORMATIVE_CHAPTER,
  POST_NORMATIVE_DATA,
  UPDATE_NORMATIVE_DATA,
  DELETE_NORMATIVE_DATA,
  PUBLISH_NORMATIVE,
  SET_EDITING_NORMATIVE,
  GET_NORMATIVE_EDITING_DATA,
  DELETE_CHAPTER,
  DELETE_ARTICLE,
  FLUSH_DATA,
} from "./constants";
import {
  setNormativeDataSuccess,
  postNormativeDataSuccess,
  postNormativeChapterSuccess,
  updateNormativeChapterSuccess,
  updateNormativeDataSuccess,
  deleteNormativeSuccess,
  plublishNormativeSuccess,
  setEditingNormativeSuccess,
  getNormativeEditingDataSuccess,
  deleteChapterSuccess,
  deleteArticleSuccess,
  flushCreatingDataSuccess,
} from "./actions";
import { getNormativesSuccess } from "@redux/normatives/actions";
import { put, takeLatest } from "@redux-saga/core/effects";
import { NormativesServicess } from "@services/normatives";

export function* setNormativeData({ payload }) {
  yield put(
    setNormativeDataSuccess({
      ...payload.data,
    })
  );
}

export function* postNormative({ payload }) {
  console.log(payload);
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.createNormative({
    data: { ...payload.data },
    file: {
      data: { ...payload.file },
      container: "posts",
      tag: "attached",
    },
  });
  console.log(response);
  payload.currentState.normativeId = response.id;
  yield put(
    postNormativeDataSuccess({
      editing: { ...payload.currentState },
    })
  );
}

export function* postNormativeChapter({ payload }) {
  const normativesServices = new NormativesServicess();
  console.log(payload.data);
  console.log(payload.currentState);
  const response = yield normativesServices.createNormativeChapter({
    data: { ...payload.data },
  });
  console.log(response);
  const index = payload.currentState.chapters.findIndex(
    (chapter) => chapter.cod === payload.data.cod
  );
  payload.currentState.chapters[index] = {
    ...payload.currentState.chapters[index],
    id: response.id,
    articles: payload.currentState.chapters[index].articles.map(
      (article, i) => ({ ...article, id: response.articles[i].id })
    ),
  };
  yield put(
    postNormativeChapterSuccess({
      editing: {
        ...payload.currentState,
        normativeId: response.normativeId,
        status: response.states,
      },
    })
  );
}

export function* updateNormative({ payload }) {
  console.log(payload);
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.updateNormative({
    id: payload.id,
    body: payload.body,
  });
  console.log(response);
  yield put(
    updateNormativeDataSuccess({
      editing: { ...payload.currentState },
    })
  );
}

export function* updateChapter({ payload }) {
  const normativesServices = new NormativesServicess();
  console.log(payload);
  payload.data.forEach((article, i) => {
    console.log(article);
    if (article.text.length < 1) return;
    if (!article.id) {
      console.log("nuevo articulo", article);
      normativesServices
        .createArticle({
          body: {
            content: article.text,
            status: true,
            chapterId: payload.currentState.chapters[payload.index].id,
            cod: payload.currentState.chapters[payload.index].cod,
            title: payload.currentState.chapters[payload.index].title,
          },
        })
        .then((response) => {
          payload.currentState.chapters[payload.index].articles[i].id =
            response.id;
          console.log(payload.currentState.chapters[payload.index].articles[i]);
        });

      return;
    }
    normativesServices
      .updateAticle({
        id: article.id,
        body: {
          content: article.text,
        },
      })
      .then((response) => console.log(response));
  });
  yield put(
    updateNormativeChapterSuccess({
      editing: { ...payload.currentState },
    })
  );
}

export function* deleteNormative({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.deleteNomative({ id: payload.id });
  console.log(response);
  const data = yield normativesServices.getFilterNormatives({
    order: "DESC",
    content: "",
    offset: 0,
    limit: 8,
  });
  yield put(getNormativesSuccess({ loading: true }));
  yield put(
    getNormativesSuccess({
      ...data,
      loading: false,
    })
  );

  yield put(
    deleteNormativeSuccess({
      ...payload.editing,
    })
  );
}

export function* publishNomative({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.updateNormative({
    id: payload.id,
    body: {
      state: payload.state,
    },
  });
  yield put(plublishNormativeSuccess({ response }));
}

export function* setNormativeEditing({ payload }) {
  yield put(
    setEditingNormativeSuccess({ currentNormativeEditingId: payload.id })
  );
}
// función para rescatar el editing
export function* getEditingData({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.getNormative({ id: payload.id });
  console.log(response);
  yield put(
    getNormativeEditingDataSuccess({
      editing: {
        normativeId: payload.id,
        title: response.title,
        description: {
          text: response.content,
          rawData: {},
        },
        tags: response.tags,
        chapters: response.chapters.map((chapter) => ({
          normativeId: chapter.normativeId,
          cod: chapter.cod,
          id: chapter.id,
          title: chapter.articles[0].title || "",
          articles: chapter.articles.map((article) => ({
            text: article.content,
            id: article.id,
            title: article.title,
            rawData: {},
          })),
        })),
      },
    })
  );
}

export function* deleteChapter({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.deleteChapter({ id: payload.id });
  console.log(response);
  yield put(deleteChapterSuccess({ editing: { ...payload.data } }));
}

export function* deleteArticle({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.deleteArticle({ id: payload.id });
  console.log(response);
  yield put(deleteArticleSuccess({ editing: { ...payload.data } }));
}

export function* flushData() {
  yield put(
    flushCreatingDataSuccess({
      title: "",
      description: "",
      currentNormativeEditingId: "",
      chapters: [],
      editing: {
        normativeId: "",
        title: "",
        description: {
          text: "",
          rawData: {},
        },
        tags: [],
        files: [],
        chapters: [],
        state: "CREATING",
      },
    })
  );
}

export default function* rootSaga() {
  yield takeLatest(SET_NORMATIVE_DATA, setNormativeData);
  yield takeLatest(POST_NORMATIVE_DATA, postNormative);
  yield takeLatest(POST_NORMATIVE_CHAPTER, postNormativeChapter);
  yield takeLatest(UPDATE_NORMATIVE_DATA, updateNormative);
  yield takeLatest(UPDATE_NORMATIVE_CHAPTER, updateChapter);
  yield takeLatest(DELETE_NORMATIVE_DATA, deleteNormative);
  yield takeLatest(PUBLISH_NORMATIVE, publishNomative);
  yield takeLatest(SET_EDITING_NORMATIVE, setNormativeEditing);
  yield takeLatest(GET_NORMATIVE_EDITING_DATA, getEditingData);
  yield takeLatest(DELETE_CHAPTER, deleteChapter);
  yield takeLatest(DELETE_ARTICLE, deleteArticle);
  yield takeLatest(FLUSH_DATA, flushData);
}
