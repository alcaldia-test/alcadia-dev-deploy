import { GET_DISCUSSIONS_DATA_SUCCESS } from "./constants";

const initialState = {
  createdAt: "",
  title: "",
  content: "",
  postId: "",
  posts: {
    comments: [],
    id: "",
  },
  attached: "",
  comments: 0,
  chapters: [],
  likes: 0,
  dislikes: 0,
};

export function chaptersDiscussions(state = initialState, action) {
  switch (action.type) {
    case GET_DISCUSSIONS_DATA_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return { ...state };
  }
}

export default chaptersDiscussions;
