import { GET_DISCUSSIONS_DATA } from "./constants";
import { getDiscussionsDataSuccess } from "./actions";
import { put, takeLatest } from "@redux-saga/core/effects";
import { discussionsIdServices } from "@services/discussions[id]";

export function* getDiscussionDataSaga(payload) {
  const response = yield discussionsIdServices.getDiscussion({
    id: payload.payload,
  });
  yield put(getDiscussionsDataSuccess({ ...response }));
}

export default function* rootSaga() {
  yield takeLatest(GET_DISCUSSIONS_DATA, getDiscussionDataSaga);
}
