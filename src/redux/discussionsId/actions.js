import {
  GET_DISCUSSIONS_DATA,
  GET_DISCUSSIONS_DATA_SUCCESS,
} from "./constants";

export const getDiscussionsData = (payload) => ({
  type: GET_DISCUSSIONS_DATA,
  payload,
});

export const getDiscussionsDataSuccess = (payload) => ({
  type: GET_DISCUSSIONS_DATA_SUCCESS,
  payload,
});
