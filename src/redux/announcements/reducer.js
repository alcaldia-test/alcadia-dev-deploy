import {
  LOADING_ANNOUNCEMENTS,
  LOADING_PROPOSALS,
  WAITING_FROM_SERVER,
  CHANGE_FILTER_ANNOUNCEMENTS,
  CHANGE_FILTER_PROPOSALS,
  CHANGE_STEP_SUCCESS,
  CHANGE_DATA_LOCAL_PROPOSAL_SUCCESS,
  GET_TOTAL_ANNOUNCEMENTS_SUCCESS,
  GET_TOTAL_PROPOSALS_SUCCESS,
  GET_COUNT_ANNOUNCEMENTS_SUCCESS,
  GET_FAVORITES_SUCCESS,
  GET_ANNOUNCEMENT_SUCCESS,
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_PROPOSALS_SUCCESS,
  GET_PROPOSAL_SUCCESS,
  GET_COUNT_PROPOSALS_SUCCESS,
  POST_PROPOSAL_SUCCESS,
  CHANGE_DETAIL_ANNOUNCEMENT_SUCCESS,
  CHANGE_CREATE_ANNOUNCEMENT_SUCCESS,
  CHANGE_DATA_LOCAL_ANNOUNCEMENT_SUCCESS,
  GET_PROPOSALS_ADMIN_SUCCESS,
  POST_ANNOUNCEMENT_SUCCESS,
  DELETE_ANNOUNCEMENT_FILE_SUCCESS,
  EDIT_POST_ANNOUNCEMENT_SUCCESS,
} from "./constants";

const initialState = {
  announcements: [],
  announcement: {},
  totalAnnouncements: 0,
  totalProposals: 0,
  totalFinishedProposals: 0,
  count: 0,
  countProposals: 0,
  countFinishedProposals: 0,
  proposals: [],
  adminProposalsReceived: [],
  adminProposalsRejected: [],
  adminProposalsPublished: [],
  adminProposalsFinished: [],
  proposal: {},
  waiting: false,
  loadingAnnouncements: true,
  loadingProposals: true,
  finishedProposal: {
    winners: [],
    others: [],
  },
  loading: false,
  filterAnnouncements: {},
  filterProposals: {},
  creatingAnnouncement: false,
  detailsAnnouncement: {},
  favorites: false,
  localProposal: {
    step: 0,
    announcementId: "",
    title: "",
    errorTitle: "",
    description: {
      blocks: [
        {
          data: {},
          depth: 0,
          entityRanges: [],
          inlineStyleRanges: [],
          key: "4sd3u",
          text: "",
          type: "unstyled",
        },
      ],
    },
    errorDescription: "",
    file: "",
    videoLink: "",
    errorImgOrVideo: "",
    location: "",
    errorLocation: "",
  },
  localAnnouncement: {
    step: 0,
    title: "",
    content: "",
    directPosts: false,
    supportingLimitStart: "",
    supportingLimitEnd: "",
    receptionStart: "",
    receptionEnd: "",
    postResults: "",
    reviewLimitStart: "",
    reviewLimitEnd: "",
    zonesId: undefined,
    macrosId: [],
    tags: [],
    file: "",
    extraFile: "",
    errors: [],
  },
  macroZones: [],
};

export default function announcements(state = initialState, action) {
  switch (action.type) {
    case LOADING_ANNOUNCEMENTS:
      return {
        ...state,
        loadingAnnouncements: action.payload,
      };
    case LOADING_PROPOSALS:
      return {
        ...state,
        loadingProposals: action.payload,
      };
    case WAITING_FROM_SERVER:
      return {
        ...state,
        waiting: action.payload,
      };
    case CHANGE_FILTER_ANNOUNCEMENTS:
      return {
        ...state,
        filterAnnouncements: action.payload,
      };
    case CHANGE_FILTER_PROPOSALS:
      return {
        ...state,
        filterProposals: action.payload,
      };
    case CHANGE_STEP_SUCCESS:
      return {
        ...state,
        localProposal: {
          ...state.localProposal,
          step: action.payload,
        },
      };
    case CHANGE_DATA_LOCAL_PROPOSAL_SUCCESS:
      return {
        ...state,
        localProposal: {
          ...state.localProposal,
          ...action.payload,
        },
      };
    case GET_TOTAL_ANNOUNCEMENTS_SUCCESS:
      return {
        ...state,
        totalAnnouncements: action.payload.count,
      };
    case GET_TOTAL_PROPOSALS_SUCCESS:
      return {
        ...state,
        totalProposals: action.payload.count,
      };
    case GET_ANNOUNCEMENTS_SUCCESS:
      return {
        ...state,
        announcements: action.payload,
      };
    case GET_COUNT_ANNOUNCEMENTS_SUCCESS:
      return {
        ...state,
        count: action.payload.count,
      };
    case GET_FAVORITES_SUCCESS:
      return {
        ...state,
        favorites: action.payload,
      };
    case GET_ANNOUNCEMENT_SUCCESS:
      return {
        ...state,
        announcement: action.payload,
      };
    case GET_PROPOSALS_SUCCESS:
      return {
        ...state,
        proposals: action.payload,
      };
    case GET_PROPOSALS_ADMIN_SUCCESS:
      return {
        ...state,
        [action.payload?.fieldState]: action.payload?.payload,
      };
    case POST_PROPOSAL_SUCCESS:
      return {
        ...state,
        localProposal: {
          step: 0,
          title: "",
          errorTitle: "",
          description: {
            blocks: [
              {
                data: {},
                depth: 0,
                entityRanges: [],
                inlineStyleRanges: [],
                key: "4sd3u",
                text: "",
                type: "unstyled",
              },
            ],
          },
          errorDescription: "",
          file: "",
          urlVideo: "",
          errorImgOrVideo: "",
          // Coordenadas de La Paz Bolivia
          location: {
            lat: 0,
            lng: 0,
          },
        },
      };
    case GET_PROPOSAL_SUCCESS:
      return { ...state, proposal: action.payload };
    case GET_COUNT_PROPOSALS_SUCCESS:
      return {
        ...state,
        countProposals: action.payload.count,
      };
    case CHANGE_DETAIL_ANNOUNCEMENT_SUCCESS:
      return {
        ...state,
        detailsAnnouncement: action.payload,
        adminProposalsFinished: [],
        adminProposalsPublished: [],
        adminProposalsReceived: [],
        adminProposalsRejected: [],
      };
    case CHANGE_CREATE_ANNOUNCEMENT_SUCCESS:
      return { ...state, creatingAnnouncement: action.payload };
    case CHANGE_DATA_LOCAL_ANNOUNCEMENT_SUCCESS:
      return {
        ...state,
        localAnnouncement: {
          ...state.localAnnouncement,
          ...action.payload,
        },
      };
    case POST_ANNOUNCEMENT_SUCCESS:
      return {
        ...state,
      };
    case DELETE_ANNOUNCEMENT_FILE_SUCCESS:
      return {
        ...state,
        localAnnouncement: {
          ...state.localAnnouncement,
          [action.payload.name]: action.payload.value,
        },
      };
    case EDIT_POST_ANNOUNCEMENT_SUCCESS:
      return {
        ...state,
        localAnnouncement: action.payload,
      };
    default:
      return state;
  }
}
