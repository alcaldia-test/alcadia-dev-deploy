import moment from "moment";
import {
  LOADING_ANNOUNCEMENTS,
  LOADING_PROPOSALS,
  WAITING_FROM_SERVER,
  CHANGE_FILTER_ANNOUNCEMENTS,
  CHANGE_FILTER_PROPOSALS,
  CHANGE_STEP,
  CHANGE_STEP_SUCCESS,
  CHANGE_DATA_LOCAL_PROPOSAL,
  CHANGE_DATA_LOCAL_PROPOSAL_SUCCESS,
  GET_TOTAL_ANNOUNCEMENTS,
  GET_TOTAL_ANNOUNCEMENTS_SUCCESS,
  GET_TOTAL_PROPOSALS,
  GET_TOTAL_PROPOSALS_SUCCESS,
  GET_COUNT_ANNOUNCEMENTS,
  GET_COUNT_ANNOUNCEMENTS_SUCCESS,
  GET_FAVORITES,
  GET_FAVORITES_SUCCESS,
  GET_ANNOUNCEMENT,
  GET_ANNOUNCEMENT_SUCCESS,
  GET_ANNOUNCEMENTS,
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_PROPOSALS,
  GET_PROPOSALS_SUCCESS,
  GET_PROPOSAL,
  GET_PROPOSAL_SUCCESS,
  GET_COUNT_PROPOSALS,
  GET_COUNT_PROPOSALS_SUCCESS,
  POST_SUPPORT_PROPOSAL,
  POST_PROPOSAL,
  POST_PROPOSAL_SUCCESS,
  CHANGE_DETAIL_ANNOUNCEMENT,
  CHANGE_DETAIL_ANNOUNCEMENT_SUCCESS,
  CHANGE_CREATE_ANNOUNCEMENT,
  CHANGE_CREATE_ANNOUNCEMENT_SUCCESS,
  CHANGE_DATA_LOCAL_ANNOUNCEMENT,
  CHANGE_DATA_LOCAL_ANNOUNCEMENT_SUCCESS,
  POST_ANNOUNCEMENT,
  POST_ANNOUNCEMENT_SUCCESS,
  POST_ACTION_PROPOSAL,
  POST_ACTION_PROPOSAL_SUCCESS,
  GET_PROPOSALS_ADMIN,
  GET_PROPOSALS_ADMIN_SUCCESS,
  DELETE_ANNOUNCEMENT,
  DELETE_ANNOUNCEMENT_SUCCESS,
  DELETE_ANNOUNCEMENT_FILE,
  DELETE_ANNOUNCEMENT_FILE_SUCCESS,
  EDIT_POST_ANNOUNCEMENT,
  EDIT_POST_ANNOUNCEMENT_SUCCESS,
} from "./constants";

export const loadingAnnouncements = (payload) => ({
  type: LOADING_ANNOUNCEMENTS,
  payload,
});

export const loadingProposals = (payload) => ({
  type: LOADING_PROPOSALS,
  payload,
});

export const waitingFromServer = (payload) => ({
  type: WAITING_FROM_SERVER,
  payload,
});

export const changeFilterAnnouncements = (payload) => ({
  type: CHANGE_FILTER_ANNOUNCEMENTS,
  payload,
});

export const changeFilterProposals = (payload) => ({
  type: CHANGE_FILTER_PROPOSALS,
  payload,
});

// Creating a proposal
export const changeStep = (payload) => ({
  type: CHANGE_STEP,
  payload,
});

export const changeStepSuccess = (payload) => ({
  type: CHANGE_STEP_SUCCESS,
  payload,
});

export const changeDataLocalProposal = (payload) => ({
  type: CHANGE_DATA_LOCAL_PROPOSAL,
  payload,
});

export const changeDataLocalProposalSuccess = (payload) => ({
  type: CHANGE_DATA_LOCAL_PROPOSAL_SUCCESS,
  payload,
});

// Single requests
export const getTotalAnnouncements = () => ({
  type: GET_TOTAL_ANNOUNCEMENTS,
});

export const getTotalAnnouncementsSuccess = (payload) => ({
  type: GET_TOTAL_ANNOUNCEMENTS_SUCCESS,
  payload,
});

export const getTotalProposals = (payload) => ({
  type: GET_TOTAL_PROPOSALS,
  payload,
});

export const getTotalProposalsSuccess = (payload) => ({
  type: GET_TOTAL_PROPOSALS_SUCCESS,
  payload,
});

export const getFavorites = (payload) => ({
  type: GET_FAVORITES,
  payload,
});

export const getFavoritesSuccess = (payload) => ({
  type: GET_FAVORITES_SUCCESS,
  payload,
});

export const getAnnouncement = (payload) => ({
  type: GET_ANNOUNCEMENT,
  payload,
});

export const getAnnouncementSuccess = (payload) => ({
  type: GET_ANNOUNCEMENT_SUCCESS,
  payload,
});

export const getProposal = (payload) => ({
  type: GET_PROPOSAL,
  payload,
});

export const getProposalSuccess = (payload) => ({
  type: GET_PROPOSAL_SUCCESS,
  payload,
});

// POST requests
export const postProposalSupport = (payload) => ({
  type: POST_SUPPORT_PROPOSAL,
  payload,
});

export const postProposal = (proposalToCreateRaw) => {
  const proposalToUpload = {
    announcementId: proposalToCreateRaw?.announcementId,
    content: JSON.stringify(proposalToCreateRaw?.description),
    title: proposalToCreateRaw?.title,
    creatorId: proposalToCreateRaw?.creatorId,
    // state: "APPROVED",
    state: "WAITING",
    status: false,
  };

  if (proposalToCreateRaw?.location) {
    proposalToUpload.location = proposalToCreateRaw?.location;
  }

  const upload = {};

  if (proposalToCreateRaw?.file) {
    upload.file = proposalToCreateRaw?.file?.originFileObj;
    upload.tag = "banner";
  }

  if (proposalToCreateRaw?.videoLink) {
    proposalToUpload.videoLink = proposalToCreateRaw?.videoLink;
  }

  return {
    type: POST_PROPOSAL,
    payload: { proposalToUpload, upload },
  };
};

export const postProposalSuccess = (payload) => ({
  type: POST_PROPOSAL_SUCCESS,
  payload,
});

// Multiple requests
export const getCountAnnouncements = (payload) => ({
  type: GET_COUNT_ANNOUNCEMENTS,
  payload,
});

export const getCountAnnouncementsSuccess = (payload) => ({
  type: GET_COUNT_ANNOUNCEMENTS_SUCCESS,
  payload,
});

export const getAnnouncements = (payload) => ({
  type: GET_ANNOUNCEMENTS,
  payload,
});

export const getAnnouncementsSuccess = (payload) => ({
  type: GET_ANNOUNCEMENTS_SUCCESS,
  payload,
});

export const getProposals = (payload) => ({
  type: GET_PROPOSALS,
  payload,
});

export const getProposalsSuccess = (payload) => ({
  type: GET_PROPOSALS_SUCCESS,
  payload,
});

export const getCountProposals = (payload) => ({
  type: GET_COUNT_PROPOSALS,
  payload,
});

export const getCountProposalsSuccess = (payload) => ({
  type: GET_COUNT_PROPOSALS_SUCCESS,
  payload,
});

export const changeDetailAnnouncement = (payload) => ({
  type: CHANGE_DETAIL_ANNOUNCEMENT,
  payload,
});

export const changeDetailAnnouncementSuccess = (payload) => ({
  type: CHANGE_DETAIL_ANNOUNCEMENT_SUCCESS,
  payload,
});

export const changeCreateAnnouncement = (payload) => ({
  type: CHANGE_CREATE_ANNOUNCEMENT,
  payload,
});

export const changeCreateAnnouncementSuccess = (payload) => ({
  type: CHANGE_CREATE_ANNOUNCEMENT_SUCCESS,
  payload,
});

export const changeDataLocalAnnouncement = (payload) => ({
  type: CHANGE_DATA_LOCAL_ANNOUNCEMENT,
  payload,
});

export const changeDataLocalAnnouncementSuccess = (payload) => ({
  type: CHANGE_DATA_LOCAL_ANNOUNCEMENT_SUCCESS,
  payload,
});

export const pachtAnnouncement = (AnnouncementToCreateRaw) => {
  const announcementToUpload = {
    winnerMessage: AnnouncementToCreateRaw?.winnerMessage,
  };

  return {
    type: POST_ANNOUNCEMENT,
    payload: {
      announcementToUpload,
      id: AnnouncementToCreateRaw?.id,
    },
  };
};

export const postAnnouncement = (AnnouncementToCreateRaw) => {
  let announcementToUpload = {};
  // State creating
  if (AnnouncementToCreateRaw?.completed === false) {
    announcementToUpload = {
      completed: false,
      content:
        AnnouncementToCreateRaw?.content !== ""
          ? JSON.stringify(AnnouncementToCreateRaw?.content)
          : "",
      title: AnnouncementToCreateRaw?.title,
      directPosts: AnnouncementToCreateRaw?.directPosts,
      receptionStart:
        AnnouncementToCreateRaw?.receptionStart !== ""
          ? moment(AnnouncementToCreateRaw?.receptionStart).format(
              "YYYY-MM-DD HH:hh"
            )
          : moment().format("YYYY-MM-DD HH:hh"),
      receptionEnd:
        AnnouncementToCreateRaw?.receptionEnd !== ""
          ? moment(AnnouncementToCreateRaw?.receptionEnd).format(
              "YYYY-MM-DD HH:hh"
            )
          : moment().format("YYYY-MM-DD HH:hh"),
      postResults:
        AnnouncementToCreateRaw?.postResults !== ""
          ? moment(AnnouncementToCreateRaw?.postResults).format(
              "YYYY-MM-DD HH:hh"
            )
          : moment().format("YYYY-MM-DD HH:hh"),
      tags: AnnouncementToCreateRaw.tags,
    };

    if (AnnouncementToCreateRaw?.directPosts === false) {
      if (AnnouncementToCreateRaw?.supportingLimitStart !== "") {
        moment(AnnouncementToCreateRaw?.supportingLimitStart).format(
          "YYYY-MM-DD HH:hh"
        );
      }

      if (AnnouncementToCreateRaw?.supportingLimitEnd !== "") {
        moment(AnnouncementToCreateRaw?.supportingLimitEnd).format(
          "YYYY-MM-DD HH:hh"
        );
      }

      if (AnnouncementToCreateRaw?.reviewLimitStart !== "") {
        moment(AnnouncementToCreateRaw?.reviewLimitStart).format(
          "YYYY-MM-DD HH:hh"
        );
      }

      if (AnnouncementToCreateRaw?.reviewLimitEnd !== "") {
        moment(AnnouncementToCreateRaw?.reviewLimitEnd).format(
          "YYYY-MM-DD HH:hh"
        );
      }
    }
  } else {
    // State normal
    announcementToUpload = {
      completed: true,
      content:
        AnnouncementToCreateRaw?.content !== ""
          ? JSON.stringify(AnnouncementToCreateRaw?.content)
          : "",
      title: AnnouncementToCreateRaw?.title,
      directPosts: AnnouncementToCreateRaw?.directPosts,
      receptionStart:
        AnnouncementToCreateRaw?.receptionStart !== ""
          ? moment(AnnouncementToCreateRaw?.receptionStart).format(
              "YYYY-MM-DD HH:hh"
            )
          : "",
      receptionEnd:
        AnnouncementToCreateRaw?.receptionEnd !== ""
          ? moment(AnnouncementToCreateRaw?.receptionEnd).format(
              "YYYY-MM-DD HH:hh"
            )
          : "",
      postResults:
        AnnouncementToCreateRaw?.postResults !== ""
          ? moment(AnnouncementToCreateRaw?.postResults).format(
              "YYYY-MM-DD HH:hh"
            )
          : "",
      tags: AnnouncementToCreateRaw.tags,
    };

    if (AnnouncementToCreateRaw?.directPosts === false) {
      announcementToUpload.supportingStart =
        AnnouncementToCreateRaw?.supportingLimitStart !== ""
          ? moment(AnnouncementToCreateRaw?.supportingLimitStart).format(
              "YYYY-MM-DD HH:hh"
            )
          : "";
      announcementToUpload.supportingEnd =
        AnnouncementToCreateRaw?.supportingLimitEnd !== ""
          ? moment(AnnouncementToCreateRaw?.supportingLimitEnd).format(
              "YYYY-MM-DD HH:hh"
            )
          : "";
      announcementToUpload.reviewStart =
        AnnouncementToCreateRaw?.reviewLimitStart !== ""
          ? moment(AnnouncementToCreateRaw?.reviewLimitStart).format(
              "YYYY-MM-DD HH:hh"
            )
          : "";
      announcementToUpload.reviewEnd =
        AnnouncementToCreateRaw?.reviewLimitEnd !== ""
          ? moment(AnnouncementToCreateRaw?.reviewLimitEnd).format(
              "YYYY-MM-DD HH:hh"
            )
          : "";
    }
  }

  if (AnnouncementToCreateRaw?.id)
    announcementToUpload.id = AnnouncementToCreateRaw.id;

  const zonesIds = [];
  const macrosId = [];
  const files = {
    main: [],
  };

  // banner
  if (AnnouncementToCreateRaw?.file?.originFileObj) {
    files?.main?.push({
      tag: "banner",
      file: AnnouncementToCreateRaw?.file?.originFileObj,
    });
  }

  if (AnnouncementToCreateRaw?.extraFile?.originFileObj) {
    files?.main?.push({
      tag: "attached",
      file: AnnouncementToCreateRaw?.extraFile?.originFileObj,
    });
  }

  AnnouncementToCreateRaw?.zonesId?.forEach((zone) => {
    if (!zone.selectedAll) {
      zone?.selected?.forEach((subZone) => {
        zonesIds.push(subZone?.id);
      });
    }
    macrosId.push(zone?.id);
  });

  if (zonesIds.length > 0) {
    announcementToUpload.zonesId = zonesIds.sort((a, b) => {
      return a - b;
    });
  } else {
    announcementToUpload.zonesId = [];
  }

  if (macrosId.length > 0) {
    announcementToUpload.macrosId = macrosId.sort((a, b) => {
      return a - b;
    });
  } else {
    announcementToUpload.macrosId = [];
  }

  return {
    type: POST_ANNOUNCEMENT,
    payload: {
      announcementToUpload,
      files,
      id: AnnouncementToCreateRaw?.id,
      postsId: AnnouncementToCreateRaw?.postsId,
    },
  };
};

export const postAnnouncementSuccess = (payload) => ({
  type: POST_ANNOUNCEMENT_SUCCESS,
  payload,
});

export const postProposalAction = (payload) => {
  return {
    type: POST_ACTION_PROPOSAL,
    payload,
  };
};

export const postProposalActionSuccess = (payload) => ({
  type: POST_ACTION_PROPOSAL_SUCCESS,
  payload,
});

export const getProposalsAdmin = (payload, fieldState) => ({
  type: GET_PROPOSALS_ADMIN,
  payload: { payload, fieldState },
});

export const getProposalsAdminSuccess = (payload, fieldState) => ({
  type: GET_PROPOSALS_ADMIN_SUCCESS,
  payload: { payload, fieldState },
});

export const deleteAnnouncement = (id) => ({
  type: DELETE_ANNOUNCEMENT,
  payload: { id },
});

export const deleteAnnouncementSuccess = (payload) => ({
  type: DELETE_ANNOUNCEMENT_SUCCESS,
  payload,
});

export const deleteFile = (payload) => ({
  type: DELETE_ANNOUNCEMENT_FILE,
  payload,
});

export const deleteFileSuccess = (payload) => ({
  type: DELETE_ANNOUNCEMENT_FILE_SUCCESS,
  payload,
});

export const editPost = (payload) => ({
  type: EDIT_POST_ANNOUNCEMENT,
  payload,
});

export const editPostSuccess = (payload) => ({
  type: EDIT_POST_ANNOUNCEMENT_SUCCESS,
  payload,
});
