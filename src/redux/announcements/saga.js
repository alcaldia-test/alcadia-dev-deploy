import {
  CHANGE_STEP,
  CHANGE_DATA_LOCAL_PROPOSAL,
  GET_COUNT_ANNOUNCEMENTS,
  GET_FAVORITES,
  GET_ANNOUNCEMENT,
  GET_ANNOUNCEMENTS,
  GET_PROPOSALS,
  GET_PROPOSAL,
  GET_TOTAL_ANNOUNCEMENTS,
  GET_TOTAL_PROPOSALS,
  GET_COUNT_PROPOSALS,
  POST_SUPPORT_PROPOSAL,
  POST_PROPOSAL,
  CHANGE_DETAIL_ANNOUNCEMENT,
  CHANGE_CREATE_ANNOUNCEMENT,
  CHANGE_DATA_LOCAL_ANNOUNCEMENT,
  POST_ANNOUNCEMENT,
  POST_ACTION_PROPOSAL,
  GET_PROPOSALS_ADMIN,
  DELETE_ANNOUNCEMENT,
  DELETE_ANNOUNCEMENT_FILE,
  EDIT_POST_ANNOUNCEMENT,
} from "./constants";
import {
  loadingAnnouncements,
  loadingProposals,
  changeStepSuccess,
  changeDataLocalProposalSuccess,
  getTotalAnnouncementsSuccess,
  getTotalProposalsSuccess,
  getCountAnnouncementsSuccess,
  getFavoritesSuccess,
  getAnnouncementSuccess,
  getAnnouncementsSuccess,
  getProposalsSuccess,
  getProposalSuccess,
  getCountProposalsSuccess,
  changeDetailAnnouncementSuccess,
  changeCreateAnnouncementSuccess,
  changeDataLocalAnnouncementSuccess,
  postAnnouncementSuccess,
  postProposalActionSuccess,
  getProposalsAdminSuccess,
  deleteAnnouncementSuccess,
  waitingFromServer,
  deleteFileSuccess,
  editPostSuccess,
} from "./actions";
import { put, takeLatest, select } from "@redux-saga/core/effects";
import { announcementsServices } from "@services";
import { hideLoader, showLoader } from "@redux/common/actions";
import moment from "moment";
import message from "@components/Message";

// Create a proposal
export function* changeStepRequestSaga({ payload }) {
  try {
    yield put(changeStepSuccess(payload));
  } catch (e) {
    message.e(e.message);
  }
}

export function* changeDataLocalProposalRequestSaga({ payload }) {
  try {
    yield put(changeDataLocalProposalSuccess(payload));
  } catch (e) {
    message.e(e.message);
  }
}

// Single requests
export function* getFavoritesRequestSaga({ payload }) {
  try {
    const response = yield announcementsServices.getFavorites(payload);
    yield put(getFavoritesSuccess(response));
  } catch (e) {
    message.e(e.message);
  }
}

export function* getTotalAnnouncementsRequestSaga({ payload }) {
  try {
    const result = yield announcementsServices.getTotalAnnouncements(payload);
    yield put(getTotalAnnouncementsSuccess(result));
  } catch (e) {
    message.e(e.message);
  }
}

export function* getTotalProposalsRequestSaga({ payload }) {
  try {
    const result = yield announcementsServices.getTotalProposals(payload);
    yield put(getTotalProposalsSuccess(result));
  } catch (e) {
    message.e(e.message);
  }
}

export function* getAnnouncementRequestSaga({ payload }) {
  try {
    yield put(loadingAnnouncements(true));
    yield put(showLoader());
    const result = yield announcementsServices.getAnnouncement(payload);

    yield put(getAnnouncementSuccess(result));
  } catch (e) {
    message.e(e.message);
  } finally {
    yield put(loadingAnnouncements(false));
    yield put(hideLoader());
  }
}

export function* getProposalRequestSaga({ payload }) {
  try {
    yield put(loadingProposals(true));
    yield put(showLoader());
    const result = yield announcementsServices.getProposal(payload);

    yield put(getProposalSuccess(result));
  } catch (e) {
    message.e(e.message);
  } finally {
    yield put(loadingProposals(false));
    yield put(hideLoader());
  }
}

// POST requests
export function* postProposalSupportRequestSaga({ payload }) {
  try {
    const request = yield announcementsServices.postProposalSupport(payload);
    if (payload.announcementId) {
      const result = {};
      result.winners = request.filter(
        (proposal) => proposal.state === "WINNER"
      );
      result.others = request.filter(
        (proposal) => proposal.state === "APPROVED"
      );

      yield put(getProposalsSuccess(result));
    } else if (payload.proposalId) {
      yield put(getProposalSuccess(request));
    }
  } catch (e) {
    message.e(e.message);
  }
}

export function* postProposalRequestSaga({ payload }) {
  try {
    yield put(showLoader());
    const result = yield announcementsServices.createProposalUploadData(
      payload?.proposalToUpload
    );

    if (!payload?.videoLink) {
      yield announcementsServices.createProposalUploadFiles({
        file: payload?.upload?.file,
        tag: payload?.upload?.tag,
        container: "proposal",
        id: result?.id,
      });
    }
  } catch (e) {
    message.e(e.message);
  } finally {
    yield put(hideLoader());
  }
}

// Multiple requests and filters
export function* getCountAnnouncementsRequestSaga({ payload }) {
  try {
    yield put(loadingAnnouncements(true));
    const result = yield announcementsServices.getCountAnnouncements(payload);

    yield put(getCountAnnouncementsSuccess(result));
  } catch (e) {
    message.e(e.message);
  } finally {
    yield put(loadingAnnouncements(false));
  }
}

export function* getAnnouncementsRequestSaga({ payload }) {
  try {
    yield put(loadingAnnouncements(true));
    yield put(showLoader());
    const result = yield announcementsServices.getAnnouncements(payload);

    yield put(getAnnouncementsSuccess(result));
  } catch (e) {
    message.e(e.message);
  } finally {
    yield put(loadingAnnouncements(false));
    yield put(hideLoader());
  }
}

export function* getCountProposalsRequestSaga({ payload }) {
  try {
    const result = yield announcementsServices.getCountProposals(payload);

    yield put(getCountProposalsSuccess(result));
  } catch (e) {
    message.error(e.message);
  }
}

export function* getProposalsRequestSaga({ payload }) {
  try {
    yield put(loadingProposals(true));
    const result = {};
    const request = yield announcementsServices.getProposals(payload);

    result.winners = request.filter((proposal) => proposal.state === "WINNER");
    result.others = request.filter((proposal) => proposal.state === "APPROVED");

    yield put(getProposalsSuccess(result));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(loadingProposals(false));
  }
}

export function* getProposalsAdminRequestSaga({ payload }) {
  try {
    yield put(loadingProposals(true));
    yield put(showLoader());
    const result = yield announcementsServices.getProposalsAdmin(
      payload?.payload
    );

    yield put(getProposalsAdminSuccess(result, payload?.fieldState));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(loadingProposals(false));
    yield put(hideLoader());
  }
}

export function* changeDetailAnnouncementRequestSaga({ payload }) {
  try {
    yield put(changeDetailAnnouncementSuccess(payload));
  } catch (e) {
    message.error(e.message);
  }
}

export function* changeCreategAnnouncementRequestSaga({ payload }) {
  try {
    yield put(changeCreateAnnouncementSuccess(payload));
  } catch (e) {
    message.error(e.message);
  }
}

export function* changeDataLocalAnnouncementRequestSaga({ payload }) {
  try {
    yield put(changeDataLocalAnnouncementSuccess(payload));
  } catch (e) {
    message.error(e.message);
  }
}

export function* postAnnouncementRequestSaga({ payload }) {
  try {
    yield put(waitingFromServer(true));
    yield put(showLoader());
    let result = "";
    if (payload?.id) {
      result = yield announcementsServices.patchAnnouncementUpdate(
        payload?.announcementToUpload,
        payload?.id
      );
    } else {
      result = yield announcementsServices.createAnnouncementUploadData(
        payload?.announcementToUpload
      );
    }

    for (let index = 0; index < payload?.files?.main?.length; index++) {
      try {
        const element = payload?.files?.main[index];
        yield announcementsServices.createProposalUploadFiles({
          file: element?.file,
          tag: element?.tag,
          container: "posts",
          id: result?.postsId || payload?.postsId || payload?.id,
        });
      } catch (e) {}
    }

    yield put(postAnnouncementSuccess());
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(waitingFromServer(false));
    yield put(hideLoader());
  }
}

export function* postProposalActionRequestSaga({ payload }) {
  try {
    yield put(waitingFromServer(true));
    yield put(showLoader());
    const result = yield announcementsServices.patchProposalAction(payload);

    yield put(postProposalActionSuccess(result));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(waitingFromServer(false));
    yield put(hideLoader());
  }
}

export function* deleteAnnouncementRequestSaga({ payload }) {
  try {
    yield put(showLoader());
    yield put(waitingFromServer(true));
    const result = yield announcementsServices.deleteAnnouncement({
      id: payload.id,
    });
    yield put(deleteAnnouncementSuccess(result));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(hideLoader());
    yield put(waitingFromServer(false));
  }
}

export function* pachAnnouncementRequestSaga({ payload }) {
  try {
    const result = yield announcementsServices.patchProposalAction(payload);

    yield put(postProposalActionSuccess(result));
  } catch (e) {
    message.error(e.message);
  }
}

export function* delteFileSaa({ payload }) {
  try {
    yield put(showLoader());
    const { tag, id, PostionFieldName, value, isProposal } = payload;
    yield announcementsServices.deleteFile({ id, tag, isProposal });
    yield put(deleteFileSuccess({ name: PostionFieldName, value }));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(hideLoader());
  }
}

export function* editPostSaga({ payload }) {
  try {
    yield put(showLoader());
    const result = yield announcementsServices.getAnnouncement(payload);
    const getMacro = (state) => state.adminVoting.macroZones;
    const macros = yield select(getMacro);

    const _localPost = {
      step: 0,
      title: result?.title,
      directPosts: result?.directPosts,
      supportingLimitStart:
        result?.supportingStart && result?.supportingStart !== ""
          ? moment(result?.supportingStart.split("T")[0])
          : "",
      supportingLimitEnd:
        result?.supportingEnd && result?.supportingEnd !== ""
          ? moment(result?.supportingEnd.split("T")[0])
          : "",
      receptionStart:
        result?.receptionStart && result?.receptionStart !== ""
          ? moment(result?.receptionStart.split("T")[0])
          : "",
      receptionEnd:
        result?.receptionEnd && result?.receptionEnd !== ""
          ? moment(result?.receptionEnd.split("T")[0])
          : "",
      postResults:
        result?.postResults && result?.postResults !== ""
          ? moment(result.postResults.split("T")[0])
          : "",
      reviewLimitStart:
        result?.reviewStart && result?.reviewStart !== ""
          ? moment(result?.reviewStart.split("T")[0])
          : "",
      reviewLimitEnd:
        result?.reviewEnd && result?.reviewEnd !== ""
          ? moment(result?.reviewEnd.split("T")[0])
          : "",
      tags: result.tags || [],
      file: "",
      extraFile: "",
      fileLink: result?.banner,
      attacher: result?.attacher || "",
      id: result?.id,
      postsId: result?.postsId,
      es: [],
    };

    try {
      _localPost.content = JSON.parse(result?.content);
      console.log("here");
    } catch (e) {
      _localPost.content = result?.content;
    }

    _localPost.zonesId = setLocalPostZonesId(
      macros,
      result?.zones,
      result?.macros
    );
    yield put(editPostSuccess(_localPost));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(hideLoader());
  }
}

function setLocalPostZonesId(macros, resultZones, resultMacros) {
  const zoneId = [];

  const macrosInResult = macros?.filter((x) =>
    resultMacros?.map((r) => r.id).includes(x.id)
  );

  for (let i = 0; i < macrosInResult.length; i++) {
    let zones = macrosInResult[i].zones;
    let selectedAll = true;
    if (resultZones.length > 0) {
      zones = macrosInResult[i].zones.filter((z) =>
        resultZones.map((rz) => rz.id).includes(z.id)
      );
      selectedAll = zones.length === 0;
    }

    if (selectedAll) {
      zones = macrosInResult[i].zones;
    }

    zoneId.push({
      id: macrosInResult[i].id,
      name: macrosInResult[i].name,
      selectedAll,
      selected: zones.map((z) => ({
        id: z.id,
        zone: z.name,
        checked: true,
      })),
    });
  }

  return zoneId.length > 0 ? zoneId : undefined;
}

export default function* rootSaga() {
  yield takeLatest(CHANGE_STEP, changeStepRequestSaga);
  yield takeLatest(
    CHANGE_DATA_LOCAL_PROPOSAL,
    changeDataLocalProposalRequestSaga
  );
  yield takeLatest(GET_FAVORITES, getFavoritesRequestSaga);
  yield takeLatest(GET_COUNT_ANNOUNCEMENTS, getCountAnnouncementsRequestSaga);
  yield takeLatest(GET_ANNOUNCEMENT, getAnnouncementRequestSaga);
  yield takeLatest(GET_ANNOUNCEMENTS, getAnnouncementsRequestSaga);
  yield takeLatest(GET_PROPOSALS, getProposalsRequestSaga);
  yield takeLatest(GET_PROPOSAL, getProposalRequestSaga);
  yield takeLatest(GET_TOTAL_ANNOUNCEMENTS, getTotalAnnouncementsRequestSaga);
  yield takeLatest(GET_TOTAL_PROPOSALS, getTotalProposalsRequestSaga);
  yield takeLatest(GET_COUNT_PROPOSALS, getCountAnnouncementsRequestSaga);
  yield takeLatest(POST_SUPPORT_PROPOSAL, postProposalSupportRequestSaga);
  yield takeLatest(POST_PROPOSAL, postProposalRequestSaga);
  yield takeLatest(
    CHANGE_DETAIL_ANNOUNCEMENT,
    changeDetailAnnouncementRequestSaga
  );
  yield takeLatest(
    CHANGE_CREATE_ANNOUNCEMENT,
    changeCreategAnnouncementRequestSaga
  );
  yield takeLatest(
    CHANGE_DATA_LOCAL_ANNOUNCEMENT,
    changeDataLocalAnnouncementRequestSaga
  );
  yield takeLatest(POST_ANNOUNCEMENT, postAnnouncementRequestSaga);
  yield takeLatest(POST_ACTION_PROPOSAL, postProposalActionRequestSaga);
  yield takeLatest(GET_PROPOSALS_ADMIN, getProposalsAdminRequestSaga);
  yield takeLatest(DELETE_ANNOUNCEMENT, deleteAnnouncementRequestSaga);
  yield takeLatest(DELETE_ANNOUNCEMENT_FILE, delteFileSaa);
  yield takeLatest(EDIT_POST_ANNOUNCEMENT, editPostSaga);
}
