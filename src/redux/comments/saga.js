import { call, put, takeLatest } from "@redux-saga/core/effects";
import fetchWrap from "@utils/client";
import { CommentServices } from "@services/comments";
import {
  handleDeleteReaction,
  handleGetComments,
  handleNewComment,
  handleReactToAnswer,
  handleReactToComment,
  handleSendAnswer,
  handleSetCommentsCount,
  handleSetMoreAnswers,
  handleDeleteAnswerReaction,
} from "./actions";
import {
  ANSWER_COMMENT,
  DELETE_ANSWER_REACTION,
  DELETE_COMMENT_REACTION,
  GET_COMMENTS,
  GET_COMMENTS_COUNT,
  GET_MORE_ANSWERS,
  HANDLE_SEND_COMMENT,
  REACT_TO_ANSWER,
  REACT_TO_COMMENT,
} from "./constants";

export function* getComments({ payload }) {
  try {
    const commentServices = new CommentServices();
    const response = yield commentServices.getComments(payload);
    yield put(handleGetComments(response));
  } catch (error) {
    console.log(error);
  }
}

export function* getCommentsCount({ payload }) {
  try {
    const response = yield call(fetchWrap, "/comments/count", {
      method: "GET",
      token: false,
      query: {
        where: JSON.stringify({
          postsId: payload,
        }),
      },
    });
    yield put(handleSetCommentsCount(response));
  } catch (error) {
    console.log(error);
  }
}

export function* answerComment({ payload }) {
  try {
    const response = yield call(fetchWrap, "/answers", {
      method: "POST",
      token: false,
      body: payload,
    });
    const responseB = yield call(fetchWrap, `/users/${payload.userId}`, {
      method: "GET",
      query: {},
    });

    yield put(handleSendAnswer({ ...response, user: responseB }));
  } catch (error) {
    console.log(error);
  }
}

export function* newComment({ payload }) {
  try {
    const responseA = yield call(fetchWrap, "/comments", {
      method: "POST",
      token: false,
      body: payload,
    });
    const responseB = yield call(fetchWrap, `/users/${payload.userId}`, {
      method: "GET",
      query: {},
    });

    yield put(handleNewComment({ ...responseA, user: responseB }));
  } catch (error) {
    console.log(error);
  }
}

export function* getMoreAnswers({ payload }) {
  try {
    const commentServices = new CommentServices();
    const response = yield commentServices.getMoreAnswers(payload);

    yield put(
      handleSetMoreAnswers({
        answers: response,
        commentId: payload.id,
        noMoreAnswers: response.length < payload.limit,
      })
    );
  } catch (error) {
    console.log(error);
  }
}

export function* reactToComment({ payload }) {
  try {
    let response;
    if (payload.check) {
      yield call(fetchWrap, `/comments/${payload.id}/reactions`, {
        method: "PATCH",
        token: false,
        body: {
          commentId: payload.id,
          emoticonId: payload.emoticonId,
          userId: payload.userId,
        },
      });
      yield put(
        handleReactToComment({
          commentId: payload.id,
          emoticonId: payload.emoticonId,
          userId: payload.userId,
        })
      );
    } else {
      response = yield call(fetchWrap, `/comments/${payload.id}/reactions`, {
        method: "POST",
        token: false,
        body: {
          commentId: payload.id,
          emoticonId: payload.emoticonId,
          userId: payload.userId,
        },
      });
      yield put(handleReactToComment(response));
    }
  } catch (error) {
    console.log(error);
  }
}

export function* reactToAnswer({ payload }) {
  try {
    let response;

    if (payload.check) {
      yield call(fetchWrap, `/answers/${payload.id}/reactions`, {
        method: "PATCH",
        token: false,
        body: {
          answerId: payload.id,
          emoticonId: payload.emoticonId,
          userId: payload.userId,
        },
      });
      yield put(
        handleReactToAnswer({
          answerId: payload.id,
          commentId: payload.commentId,
          emoticonId: payload.emoticonId,
          userId: payload.userId,
        })
      );
    } else {
      response = yield call(fetchWrap, `/answers/${payload.id}/reactions`, {
        method: "POST",
        token: false,
        body: {
          answerId: payload.id,
          emoticonId: payload.emoticonId,
          userId: payload.userId,
        },
      });
      yield put(
        handleReactToAnswer({ ...response, commentId: payload.commentId })
      );
    }
  } catch (error) {
    console.log(error);
  }
}

export function* deleteAnswerReaction({ payload }) {
  try {
    yield call(fetchWrap, `/answers/${payload.id}/reactions`, {
      method: "DELETE",
      token: false,
      query: {
        where: JSON.stringify({
          answerId: payload.id,

          userId: payload.userId,
        }),
      },
    });
    yield put(
      handleDeleteAnswerReaction({
        answerId: payload.id,
        commentId: payload.commentId,
        userId: payload.userId,
      })
    );
  } catch (error) {
    console.log(error);
  }
}
export function* deleteCommentReaction({ payload }) {
  try {
    yield call(fetchWrap, `/comments/${payload.id}/reactions`, {
      method: "DELETE",
      token: false,
      query: {
        where: JSON.stringify({
          commentId: payload.id,

          userId: payload.userId,
        }),
      },
    });
    yield put(
      handleDeleteReaction({
        commentId: payload.id,
        emoticonId: payload.emoticonId,
        userId: payload.userId,
      })
    );
  } catch (error) {
    console.log(error);
  }
}

export default function* rootSaga() {
  yield takeLatest(GET_COMMENTS, getComments);
  yield takeLatest(GET_COMMENTS_COUNT, getCommentsCount);
  yield takeLatest(ANSWER_COMMENT, answerComment);
  yield takeLatest(HANDLE_SEND_COMMENT, newComment);
  yield takeLatest(GET_MORE_ANSWERS, getMoreAnswers);
  yield takeLatest(REACT_TO_COMMENT, reactToComment);
  yield takeLatest(DELETE_COMMENT_REACTION, deleteCommentReaction);
  yield takeLatest(DELETE_ANSWER_REACTION, deleteAnswerReaction);
  yield takeLatest(REACT_TO_ANSWER, reactToAnswer);
}
