import {
  HANDLE_CLICK_ANSWER,
  HANDLE_COMMENT_DATA,
  HANDLE_GET_COMMENTS,
  HANDLE_GET_COMMENTS_COUNT,
  HANDLE_NEW_COMMENT,
  HANDLE_SEND_ANSWER,
  HANDLE_SET_MORE_ANSWERS,
  HANDLE_WRITE_ANSWER,
  HANDLE_WRITE_COMMENT,
  HANDLE_DELETE_REACTION,
  HANDLE_REACT_TO_COMMENT,
  HANDLE_REACT_TO_ANSWER,
  HANDLE_DELETE_ANSWER_REACTION,
} from "./constants";
const initialState = {
  comments: [],
  count: 0,
  answerData: null,

  commentData: null,
};

const comments = (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_GET_COMMENTS:
      return {
        ...state,
        comments: [...state.comments, ...action.payload],
      };
    case HANDLE_GET_COMMENTS_COUNT:
      return {
        ...state,
        count: action.payload.count,
      };
    case HANDLE_CLICK_ANSWER:
      return {
        ...state,
        answerData: {
          commentId: action.payload.id,
          content: "",
          userId: "ebcfa943-b173-465d-af7b-54f9b5149963",
        },
      };
    case HANDLE_WRITE_ANSWER:
      return {
        ...state,
        answerData: {
          ...state.answerData,
          content: action.payload,
        },
      };
    case HANDLE_SEND_ANSWER:
      return {
        ...state,
        comments: state.comments.map((comment) =>
          comment.id === action.payload.commentId
            ? {
                ...comment,
                answers: comment.answers
                  ? [action.payload, ...comment.answers]
                  : [action.payload],
              }
            : comment
        ),
      };

    case HANDLE_COMMENT_DATA:
      return {
        ...state,
        commentData: action.payload,
      };

    case HANDLE_NEW_COMMENT:
      return {
        ...state,
        comments: [action.payload, ...state.comments],
      };
    case HANDLE_WRITE_COMMENT:
      return {
        ...state,
        commentData: {
          ...state.commentData,
          content: action.payload,
        },
      };
    case HANDLE_SET_MORE_ANSWERS:
      return {
        ...state,
        comments: state.comments.map((comment) =>
          comment.id === action.payload.commentId
            ? {
                ...comment,
                answers: [...comment.answers, ...action.payload.answers],
                noMoreAnswers: action.payload.noMoreAnswers,
              }
            : comment
        ),
      };
    case HANDLE_REACT_TO_COMMENT:
      return {
        ...state,
        comments: state.comments.map((comment) => {
          if (comment.id === action.payload.commentId) {
            if (!comment.reactions) {
              return { ...comment, reactions: [action.payload] };
            }
            const index = comment.reactions.findIndex(
              (x) => x.userId === action.payload.userId
            );
            if (index === -1) {
              comment.reactions = [...comment.reactions, action.payload];
            } else {
              comment.reactions[index] = action.payload;
            }

            return {
              ...comment,
              reactions: comment.reactions,
            };
          } else {
            return comment;
          }
        }),
      };
    case HANDLE_DELETE_REACTION:
      return {
        ...state,
        comments: state.comments.map((comment) => {
          if (comment.id === action.payload.commentId) {
            return {
              ...comment,
              reactions: comment.reactions.filter(
                (x) => x.userId !== action.payload.userId
              ),
            };
          } else {
            return comment;
          }
        }),
      };

    case HANDLE_REACT_TO_ANSWER:
      return {
        ...state,
        comments: state.comments.map((comment) => {
          if (comment.id === action.payload.commentId) {
            return {
              ...comment,
              answers: comment.answers.map((answer) => {
                if (answer.id === action.payload.answerId) {
                  if (!answer.reactions) {
                    return { ...answer, reactions: [action.payload] };
                  }
                  const index = answer.reactions.findIndex(
                    (x) => x.userId === action.payload.userId
                  );
                  if (index === -1) {
                    answer.reactions = [...answer.reactions, action.payload];
                  } else {
                    answer.reactions[index] = action.payload;
                  }
                  return {
                    ...answer,
                    reactions: answer.reactions,
                  };
                } else {
                  return answer;
                }
              }),
            };
          } else {
            return comment;
          }
        }),
      };
    case HANDLE_DELETE_ANSWER_REACTION:
      return {
        ...state,
        comments: state.comments.map((comment) => {
          if (comment.id === action.payload.commentId) {
            return {
              ...comment,
              answers: comment.answers.map((answer) =>
                answer.id === action.payload.answerId
                  ? {
                      ...answer,
                      reactions: answer.reactions.filter(
                        (y) => y.userId !== action.payload.userId
                      ),
                    }
                  : answer
              ),
            };
          } else {
            return comment;
          }
        }),
      };
    default:
      return state;
  }
};

export default comments;
