import {
  ANSWER_COMMENT,
  DELETE_ANSWER_REACTION,
  DELETE_COMMENT_REACTION,
  GET_COMMENTS,
  GET_COMMENTS_COUNT,
  GET_MORE_ANSWERS,
  HANDLE_CLICK_ANSWER,
  HANDLE_COMMENT_DATA,
  HANDLE_DELETE_REACTION,
  HANDLE_GET_COMMENTS,
  HANDLE_GET_COMMENTS_COUNT,
  HANDLE_NEW_COMMENT,
  HANDLE_REACT_TO_ANSWER,
  HANDLE_REACT_TO_COMMENT,
  HANDLE_SEND_ANSWER,
  HANDLE_SEND_COMMENT,
  HANDLE_SET_MORE_ANSWERS,
  HANDLE_WRITE_ANSWER,
  HANDLE_WRITE_COMMENT,
  REACT_TO_ANSWER,
  REACT_TO_COMMENT,
  HANDLE_DELETE_ANSWER_REACTION,
} from "./constants";

export const handleGetComments = (payload) => ({
  type: HANDLE_GET_COMMENTS,
  payload,
});

export const getCommentsByPost = (payload) => ({
  type: GET_COMMENTS,
  payload,
});

export const handleSetCommentsCount = (payload) => ({
  type: HANDLE_GET_COMMENTS_COUNT,
  payload,
});
export const getCommentsCount = (payload) => ({
  type: GET_COMMENTS_COUNT,
  payload,
});

export const handleClickAnswer = (payload) => ({
  type: HANDLE_CLICK_ANSWER,
  payload,
});

export const handleWriteAnswer = (payload) => ({
  type: HANDLE_WRITE_ANSWER,
  payload,
});

export const handleSendAnswer = (payload) => ({
  type: HANDLE_SEND_ANSWER,
  payload,
});

export const answerComment = (payload) => ({
  type: ANSWER_COMMENT,
  payload,
});

export const handleWriteComment = (payload) => ({
  type: HANDLE_WRITE_COMMENT,
  payload,
});

export const handleSendComment = (payload) => ({
  type: HANDLE_SEND_COMMENT,
  payload,
});

export const handleNewComment = (payload) => ({
  type: HANDLE_NEW_COMMENT,
  payload,
});

export const handleCommentData = (payload) => ({
  type: HANDLE_COMMENT_DATA,
  payload,
});

export const getMoreAnswers = (payload) => ({
  type: GET_MORE_ANSWERS,
  payload,
});

export const handleSetMoreAnswers = (payload) => ({
  type: HANDLE_SET_MORE_ANSWERS,
  payload,
});

export const reactToComment = (payload) => ({
  type: REACT_TO_COMMENT,
  payload,
});

export const handleReactToComment = (payload) => ({
  type: HANDLE_REACT_TO_COMMENT,
  payload,
});

export const deleteCommentReaction = (payload) => ({
  type: DELETE_COMMENT_REACTION,
  payload,
});

export const handleDeleteReaction = (payload) => ({
  type: HANDLE_DELETE_REACTION,
  payload,
});

export const deleteAnswerReaction = (payload) => ({
  type: DELETE_ANSWER_REACTION,
  payload,
});

export const handleReactToAnswer = (payload) => ({
  type: HANDLE_REACT_TO_ANSWER,
  payload,
});
export const reactToAnswer = (payload) => ({
  type: REACT_TO_ANSWER,
  payload,
});

export const handleDeleteAnswerReaction = (payload) => ({
  type: HANDLE_DELETE_ANSWER_REACTION,
  payload,
});
