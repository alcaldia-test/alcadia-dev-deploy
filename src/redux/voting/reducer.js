import {
  LOADING_VOTES,
  CHANGE_FILTER,
  GET_VOTE_SUCCESS,
  POST_VOTE_SUCCESS,
  PATCH_USER_ZONE_SUCCESS,
  POST_USER_ZONE_SUCCESS,
  GET_VOTES_SUCCESS,
  GET_TOTAL_VOTES_SUCCESS,
  GET_COUNT_VOTES_SUCCESS,
  GET_FAVORITES_SUCCESS,
  GET_ZONES_OF_MACRO_SUCCESS,
  GET_STATISTICS_SUCCESS,
} from "./constants";

export const initialState = {
  votes: [],
  vote: {
    alreadyVoted: false,
    completed: false,
    content: "",
    id: "",
    macros: [],
    postsId: "",
    proposals: [],
    question: "",
    startDate: "",
    tags: [],
    title: "",
    zones: [],
  },
  zonesOfMacro: [],
  favorites: [],
  count: 0,
  total: 0,
  loadingVotes: false,
  filters: {},
  statistics: {},
};

const clientVoting = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_VOTES:
      return {
        ...state,
        loadingVotes: action.payload,
      };
    case CHANGE_FILTER:
      return {
        ...state,
        filters: action.payload,
      };
    case GET_COUNT_VOTES_SUCCESS:
      return {
        ...state,
        count: action.payload.count,
      };
    case GET_TOTAL_VOTES_SUCCESS:
      return {
        ...state,
        total: action.payload.count,
      };
    case GET_VOTE_SUCCESS:
      return {
        ...state,
        vote: action.payload,
        zonesOfMacro: [],
      };
    case POST_VOTE_SUCCESS:
      return {
        ...state,
        vote: action.payload,
      };
    case GET_VOTES_SUCCESS:
      return {
        ...state,
        votes: action.payload,
      };
    case PATCH_USER_ZONE_SUCCESS:
      return {
        ...state,
        vote: action.payload,
      };
    case POST_USER_ZONE_SUCCESS:
      return {
        ...state,
        vote: action.payload,
      };
    case GET_FAVORITES_SUCCESS:
      return {
        ...state,
        favorites: action?.payload,
      };
    case GET_ZONES_OF_MACRO_SUCCESS:
      return {
        ...state,
        zonesOfMacro: action?.payload.zones,
      };
    case GET_STATISTICS_SUCCESS:
      return {
        ...state,
        statistics: action.payload,
      };
    default:
      return state;
  }
};

export default clientVoting;
