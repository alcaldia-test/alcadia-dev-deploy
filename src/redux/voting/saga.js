import {
  GET_TOTAL_VOTES,
  GET_COUNT_VOTES,
  GET_VOTE,
  POST_VOTE,
  PATCH_USER_ZONE,
  POST_USER_ZONE,
  GET_VOTES,
  GET_FAVORITES,
  GET_ZONES_OF_MACRO,
  GET_STATISTICS,
} from "./constants";
import {
  loadingVotes,
  getTotalVotesSuccess,
  getCountVotesSuccess,
  getVoteSuccess,
  postVoteSuccess,
  patchUserZoneSuccess,
  postUserZoneSuccess,
  getVotesSuccess,
  getFavoritesSuccess,
  getZonesOfMacroSuccess,
  getStatisticsSuccess,
} from "./actions";

import { put, takeLatest, select, call } from "redux-saga/effects";
import { hideLoader, showLoader } from "@redux/common/actions";
import { votingServices } from "@services/voting";
import message from "@components/Message";
import request, { getOptions } from "@utils/request";
import { changeUserInfo } from "@redux/auth/actions";

export function* getTotalVotesRequestSaga() {
  try {
    const response = yield votingServices.getTotalVotes();
    yield put(getTotalVotesSuccess(response));
  } catch (e) {
    message.error(e.message);
  }
}

export function* getCountVotesRequestSaga({ payload }) {
  try {
    yield put(loadingVotes(true));
    const response = yield votingServices.getCountVotes(payload);
    yield put(getCountVotesSuccess(response));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(loadingVotes(false));
  }
}

// Single request
export function* getVoteRequestSaga({ payload }) {
  try {
    yield put(loadingVotes(true));
    const response = yield votingServices.getVote(payload);
    yield put(getVoteSuccess(response));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(loadingVotes(false));
  }
}

export function* postVoteRequestSaga({
  payload: { resolve, reject, ...others },
}) {
  try {
    yield put(showLoader());
    const response = yield votingServices.postVote(others);

    yield put(postVoteSuccess(response));
    yield call(resolve);
  } catch (e) {
    console.log(e);
    message.error("No es posible emitir el voto.");
    yield call(reject);
  } finally {
    yield put(hideLoader());
  }
}

export function* patchUserZoneRequestSaga({ payload }) {
  try {
    yield put(showLoader());
    const response = yield votingServices.patchUserZone(payload);

    const options = yield getOptions();
    const url = `${process.env.NEXT_PUBLIC_URL_API}/whoAmI`;
    const userInfo = yield request(url, options);

    yield put(changeUserInfo(userInfo));
    yield put(patchUserZoneSuccess(response));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(hideLoader());
  }
}

export function* postUserZoneRequestSaga({ payload }) {
  try {
    yield put(showLoader());
    const response = yield votingServices.postUserZone(payload);

    const options = yield getOptions();
    const url = `${process.env.NEXT_PUBLIC_URL_API}/whoAmI`;
    const userInfo = yield request(url, options);

    yield put(changeUserInfo(userInfo));
    yield put(postUserZoneSuccess(response));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(hideLoader());
  }
}

export function* getFavoritesRequestSaga({ payload }) {
  try {
    let { favorites } = yield select((state) => state.voting);
    if (favorites.length === 0) {
      favorites = yield votingServices.getFavorites(payload);
    }
    yield put(getFavoritesSuccess(favorites));
  } catch (e) {
    message.error(e.message);
  }
}

export function* getZonesOfMacroRequestSaga({ payload }) {
  try {
    const response = yield votingServices.getZonesOfMacro(payload);
    yield put(getZonesOfMacroSuccess(response));
  } catch (e) {
    message.error(e.message);
  }
}

export function* getStatisticsRequestSaga({ payload }) {
  try {
    yield put(loadingVotes(true));
    const response = yield votingServices.getStatistics(payload);
    yield put(getStatisticsSuccess(response));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(loadingVotes(false));
  }
}

// Multiple requests and filters
export function* getVotesRequestSaga({ payload }) {
  try {
    yield put(loadingVotes(true));
    const result = yield votingServices.getVotes(payload);

    yield put(getVotesSuccess(result));
  } catch (e) {
    message.error(e.message);
  } finally {
    yield put(loadingVotes(false));
  }
}

export default function* rootSaga() {
  yield takeLatest(GET_TOTAL_VOTES, getTotalVotesRequestSaga);
  yield takeLatest(GET_COUNT_VOTES, getCountVotesRequestSaga);
  yield takeLatest(GET_VOTE, getVoteRequestSaga);
  yield takeLatest(POST_VOTE, postVoteRequestSaga);
  yield takeLatest(PATCH_USER_ZONE, patchUserZoneRequestSaga);
  yield takeLatest(POST_USER_ZONE, postUserZoneRequestSaga);
  yield takeLatest(GET_VOTES, getVotesRequestSaga);
  yield takeLatest(GET_FAVORITES, getFavoritesRequestSaga);
  yield takeLatest(GET_ZONES_OF_MACRO, getZonesOfMacroRequestSaga);
  yield takeLatest(GET_STATISTICS, getStatisticsRequestSaga);
}
