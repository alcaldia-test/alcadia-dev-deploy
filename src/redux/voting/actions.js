import {
  GET_TOTAL_VOTES,
  GET_TOTAL_VOTES_SUCCESS,
  GET_COUNT_VOTES,
  GET_COUNT_VOTES_SUCCESS,
  GET_VOTE,
  GET_VOTE_SUCCESS,
  POST_VOTE,
  POST_VOTE_SUCCESS,
  PATCH_USER_ZONE,
  PATCH_USER_ZONE_SUCCESS,
  POST_USER_ZONE,
  POST_USER_ZONE_SUCCESS,
  GET_VOTES,
  GET_VOTES_SUCCESS,
  GET_FAVORITES,
  GET_FAVORITES_SUCCESS,
  GET_ZONES_OF_MACRO,
  GET_ZONES_OF_MACRO_SUCCESS,
  GET_STATISTICS,
  GET_STATISTICS_SUCCESS,
  LOADING_VOTES,
  CHANGE_FILTER,
} from "./constants";

export const loadingVotes = (payload) => ({
  type: LOADING_VOTES,
  payload,
});

export const changeFilter = (payload) => ({
  type: CHANGE_FILTER,
  payload,
});

// Single request
export const getVote = (payload) => ({
  type: GET_VOTE,
  payload,
});

export const getVoteSuccess = (payload) => ({
  type: GET_VOTE_SUCCESS,
  payload,
});

export const postVote = (payload) => ({
  type: POST_VOTE,
  payload,
});

export const postVoteSuccess = (payload) => ({
  type: POST_VOTE_SUCCESS,
  payload,
});

export const patchUserZone = (payload) => ({
  type: PATCH_USER_ZONE,
  payload,
});

export const postUserZone = (payload) => ({
  type: POST_USER_ZONE,
  payload,
});

export const postUserZoneSuccess = (payload) => ({
  type: POST_USER_ZONE_SUCCESS,
  payload,
});

export const patchUserZoneSuccess = (payload) => ({
  type: PATCH_USER_ZONE_SUCCESS,
  payload,
});

export const getFavorites = (payload) => ({
  type: GET_FAVORITES,
  payload,
});

export const getFavoritesSuccess = (payload) => ({
  type: GET_FAVORITES_SUCCESS,
  payload,
});

export const getZonesOfMacro = (payload) => ({
  type: GET_ZONES_OF_MACRO,
  payload,
});

export const getZonesOfMacroSuccess = (payload) => ({
  type: GET_ZONES_OF_MACRO_SUCCESS,
  payload,
});

export const getStatistics = (payload) => ({
  type: GET_STATISTICS,
  payload,
});

export const getStatisticsSuccess = (payload) => ({
  type: GET_STATISTICS_SUCCESS,
  payload,
});

// Multiple requests
export const getTotalVotes = () => ({
  type: GET_TOTAL_VOTES,
});

export const getTotalVotesSuccess = (payload) => ({
  type: GET_TOTAL_VOTES_SUCCESS,
  payload,
});

export const getCountVotes = (payload) => ({
  type: GET_COUNT_VOTES,
  payload,
});

export const getCountVotesSuccess = (payload) => ({
  type: GET_COUNT_VOTES_SUCCESS,
  payload,
});

export const getVotes = (payload) => ({
  type: GET_VOTES,
  payload,
});

export const getVotesSuccess = (payload) => ({
  type: GET_VOTES_SUCCESS,
  payload,
});
