import { CONTEND_DELIVERED } from "./contants";

const initialState = [];

export function relatedContent(state = initialState, action) {
  switch (action.type) {
    case CONTEND_DELIVERED:
      return [...action.payload];
    default:
      return [...state];
  }
}

export default relatedContent;
