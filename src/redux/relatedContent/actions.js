import { GET_CONTEND, CONTEND_DELIVERED } from "./contants";

export const getContent = (payload) => ({
  type: GET_CONTEND,
  payload,
});

export const contentDelivered = (payload) => ({
  type: CONTEND_DELIVERED,
  payload,
});
