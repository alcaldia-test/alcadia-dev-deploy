import { GET_CONTEND } from "./contants";
import { contentDelivered } from "./actions";
import { put, takeLatest } from "@redux-saga/core/effects";
import { NormativesServicess } from "@services/normatives";

export function* setRelatedContent({ payload }) {
  const { id, tags } = payload;
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.enlistRelatedNormatives({
    tags,
    id,
  });
  const contentData = [...response.slice(0, 3)];
  yield put(contentDelivered(contentData));
}

export default function* rootSaga() {
  yield takeLatest(GET_CONTEND, setRelatedContent);
}
