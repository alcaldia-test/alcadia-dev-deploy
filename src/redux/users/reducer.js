import {
  REQUEST_USERS_SUCCESS,
  DELETE_USERS_SUCCESS,
  PATCH_USERS_SUCCESS,
  SET_COUNT_USERS,
  REQUEST_LOCKEDS_SUCCESS,
  NEW_REQUEST_REQUIRED,
} from "./constants";

const initialState = {
  datas: [],
  lockeds: [],
  datasCount: 0,
  lockedsCount: 0,
  newset: true,
};

export function globalReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_USERS_SUCCESS:
      return { ...state, datas: action.payload };
    case REQUEST_LOCKEDS_SUCCESS:
      return { ...state, lockeds: action.payload };
    case NEW_REQUEST_REQUIRED:
      return { ...state, newset: action.payload };
    case SET_COUNT_USERS:
      return { ...state, ...action.payload };
    case DELETE_USERS_SUCCESS:
      return { ...state, ...action.payload };
    case PATCH_USERS_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

export default globalReducer;
