export const REQUEST_USERS_START = "REQUEST_USERS_START";
export const REQUEST_USERS_SUCCESS = "REQUEST_USERS_SUCCESS";
export const REQUEST_LOCKEDS_SUCCESS = "REQUEST_LOCKEDS_SUCCESS";
export const CHANGE_USER_AVATAR = "CHANGE_USER_AVATAR";

export const NEW_REQUEST_REQUIRED = "NEW_REQUEST_REQUIRED";

export const SET_COUNT_USERS = "SET_COUNT_USERS";

export const PATCH_USERS_START = "PATCH_USERS_START";
export const PATCH_USERS_SUCCESS = "PATCH_USERS_SUCCESS";

export const DELETE_USERS_START = "DELETE_USERS_START";
export const DELETE_USERS_SUCCESS = "DELETE_USERS_SUCCESS";

export const REQUEST_SINGLE_USER = "REQUEST_SINGLE_USER";
export const REQUEST_USER_PROFILE = "REQUEST_USER_PROFILE";
