import {
  REQUEST_USERS_START,
  REQUEST_USERS_SUCCESS,
  PATCH_USERS_START,
  PATCH_USERS_SUCCESS,
  CHANGE_USER_AVATAR,
  DELETE_USERS_START,
  DELETE_USERS_SUCCESS,
  SET_COUNT_USERS,
  REQUEST_LOCKEDS_SUCCESS,
  NEW_REQUEST_REQUIRED,
  REQUEST_SINGLE_USER,
  REQUEST_USER_PROFILE,
} from "./constants";

export const requestSingleUser = (payload) => ({
  type: REQUEST_SINGLE_USER,
  payload: payload,
});
export const requestUserProfile = (payload) => ({
  type: REQUEST_USER_PROFILE,
  payload: payload,
});
export const requestUsersStart = (payload) => ({
  type: REQUEST_USERS_START,
  payload: payload,
});
export const requestUsersSuccess = (payload) => ({
  type: REQUEST_USERS_SUCCESS,
  payload: payload,
});
export const requestLockedsSuccess = (payload) => ({
  type: REQUEST_LOCKEDS_SUCCESS,
  payload: payload,
});
export const newRequestRequired = (payload) => ({
  type: NEW_REQUEST_REQUIRED,
  payload: payload,
});
export const changeUserAvatar = (payload) => ({
  type: CHANGE_USER_AVATAR,
  payload: payload,
});

export const setUsersCount = (payload) => ({
  type: SET_COUNT_USERS,
  payload: payload,
});

export const changeUsersStart = (payload) => ({
  type: PATCH_USERS_START,
  payload: payload,
});
export const changeUsersSuccess = (payload) => ({
  type: PATCH_USERS_SUCCESS,
  payload: payload,
});

export const deleteUsersStart = (payload) => ({
  type: DELETE_USERS_START,
  payload: payload,
});
export const deleteUsersSuccess = (payload) => ({
  type: DELETE_USERS_SUCCESS,
  payload: payload,
});
