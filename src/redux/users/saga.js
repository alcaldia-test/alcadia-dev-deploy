import { call, put, takeLatest, select, all } from "redux-saga/effects";
import { UsersServices } from "@services/index";
import moment from "moment";

import {
  REQUEST_USERS_START,
  DELETE_USERS_START,
  PATCH_USERS_START,
  CHANGE_USER_AVATAR,
  REQUEST_SINGLE_USER,
  REQUEST_USER_PROFILE,
} from "./constants";
import {
  requestUsersSuccess,
  setUsersCount,
  requestUsersStart,
  requestLockedsSuccess,
  newRequestRequired,
} from "./actions";
import { changeUserAvatarSuccess } from "../auth/actions";
import request, {
  patchOptions,
  postOptions,
  deleteOptions,
  postOptionsFormData,
} from "@utils/request";
import { hideLoader, showLoader } from "@redux/common/actions";

export function* getAllUsers({
  payload: { resolve, reject, withCount, newset, ...filter },
}) {
  const allEffects = [];
  const service = new UsersServices();

  try {
    const users = yield service.getUsers(filter);

    if (filter?.enable === 1)
      allEffects.push(put(requestLockedsSuccess(users)));
    else allEffects.push(put(requestUsersSuccess(users)));

    if (withCount) {
      const count = yield service.getUsersCount(filter);

      if (filter?.enable === 1)
        allEffects.push(put(setUsersCount({ lockedsCount: count })));
      else allEffects.push(put(setUsersCount({ datasCount: count })));
    }

    if (typeof newset === "boolean")
      allEffects.push(put(newRequestRequired(newset)));

    allEffects.push(call(resolve, undefined));

    yield all(allEffects);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al traer datos");
  }
}

export function* unLockUser({ payload: { userId, resolve, reject } }) {
  let url, options;

  const server = process.env.NEXT_PUBLIC_URL_API;
  try {
    url = `${server}/users/unlock`;
    options = patchOptions({ userId });
    yield call(request, url, options);

    yield put(
      requestUsersStart({
        resolve,
        reject,
        withCount: true,
        newset: true,
        enable: 1,
      })
    );
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export function* lockUser({ payload: { resolve, reject, ...others } }) {
  const { dataUser } = yield select((storage) => storage.auth);
  let url, options;

  const server = process.env.NEXT_PUBLIC_URL_API;
  try {
    url = `${server}/users/lock`;
    options = postOptions({ ...others, reviewerId: dataUser.id });

    yield call(request, url, options);

    yield put(
      requestUsersStart({ resolve, reject, withCount: true, newset: true })
    );
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export function* changeUserAvatar({
  payload: { file, userId, resolve, reject },
}) {
  const service = new UsersServices();
  let url, options, msg;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    yield put(showLoader());
    const [oldAvatar] = yield service.getUserAvatar(userId);

    if (oldAvatar) {
      url = `${server}/file-storages/${oldAvatar.id}`;
      options = deleteOptions();
      yield call(request, url, options);
    }

    url = `${server}/containers/user/upload/avatar`;
    const fd = new FormData();
    fd.append(userId, file);

    options = postOptionsFormData(fd);

    const [newImage] = yield call(request, url, options);

    url = `${server}/signup/complete/${userId}`;
    options = patchOptions({ avatar: newImage.link, updatedAt: moment() });
    const response = yield call(request, url, options);

    if (response) {
      msg = "¡Imagen actualizada exitosamente!";
      yield put(changeUserAvatarSuccess(newImage.link));
      yield call(resolve, msg ?? "Error al subir datos");
    } else {
      msg = "Error al subir datos";
      yield call(reject, msg);
    }
  } catch (e) {
    console.log(e);
    yield call(reject, "Error al subir datos");
  } finally {
    yield put(hideLoader());
  }
}

export function* getSingleUser({ payload: { resolve, reject, id } }) {
  yield put(showLoader());

  const service = new UsersServices();

  try {
    const result = yield service.getSingleUser(id);

    yield call(resolve, result);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  } finally {
    yield put(hideLoader());
  }
}

export function* getUserProfile({ payload: { resolve, reject, id } }) {
  yield put(showLoader());

  const service = new UsersServices();

  try {
    const result = yield service.getUserProfile(id);

    yield call(resolve, result);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  } finally {
    yield put(hideLoader());
  }
}

export default function* rootSaga() {
  yield takeLatest(REQUEST_USERS_START, getAllUsers);
  yield takeLatest(DELETE_USERS_START, lockUser);
  yield takeLatest(PATCH_USERS_START, unLockUser);
  yield takeLatest(CHANGE_USER_AVATAR, changeUserAvatar);
  yield takeLatest(REQUEST_SINGLE_USER, getSingleUser);
  yield takeLatest(REQUEST_USER_PROFILE, getUserProfile);
}
