import {
  ENLIST_NORMATIVES,
  GET_FILTER_NORMATIVE,
  GET_FILTER_NORMATIVES_ADMIN,
  BLOCK_USER,
  DELETE_COMMENT_COMPLAINT,
} from "./constants";
import {
  getNormativesSuccess,
  getFilterNormativeSuccess,
  getFilterNormativeAdminSuccess,
  blockUserSuccess,
  deleteCommentSuccess,
} from "./actions";
import { hideLoader, showLoader } from "@redux/common/actions";
import { put, takeLatest } from "@redux-saga/core/effects";
import { NormativesServicess } from "@services/normatives";

export function* getNormativesList({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.enlistNormatives({
    limit: payload.limit,
  });
  yield put(getNormativesSuccess({ ...response }));
}

export function* getFilteredNormative({ payload }) {
  try {
    yield put(showLoader());
    const { order, content, currentPage, state, limit } = payload;
    const normativesServices = new NormativesServicess();
    const response = yield normativesServices.getFilterNormatives({
      order,
      content,
      offset: limit * (currentPage - 1),
      state,
      limit,
    });
    yield put(getFilterNormativeSuccess({ ...response, loading: false }));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(hideLoader());
  }
}

export function* getFilteredNormativeAdmin({ payload }) {
  try {
    yield put(showLoader());
    const limit = 8;
    const normativesServices = new NormativesServicess();
    const response = yield normativesServices.getFilterNormativesAdmin({
      state: payload.state,
      order: payload.order,
      content: payload.content === "" ? undefined : payload.content,
      offset: payload.offset,
      limit,
    });
    yield put(getFilterNormativeAdminSuccess(response));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(hideLoader());
  }
}

export function* blockUser({ payload }) {
  const normativesServices = new NormativesServicess();
  yield normativesServices.deleteComplaint({
    type: payload.type,
    id: payload.id,
    reviewerId: payload.reviewerId,
    message: payload.message,
  });
  yield put(blockUserSuccess({}));
}

export function* deleteComment({ payload }) {
  const normativesServices = new NormativesServicess();
  yield normativesServices.deleteComplaint({
    type: payload.type,
    id: payload.id,
    reviewerId: payload.reviewerId,
    message: payload.message,
  });
  yield put(deleteCommentSuccess({}));
}

export default function* rootSaga() {
  yield takeLatest(ENLIST_NORMATIVES, getNormativesList);
  yield takeLatest(GET_FILTER_NORMATIVE, getFilteredNormative);
  yield takeLatest(GET_FILTER_NORMATIVES_ADMIN, getFilteredNormativeAdmin);
  yield takeLatest(BLOCK_USER, blockUser);
  yield takeLatest(DELETE_COMMENT_COMPLAINT, deleteComment);
}
