import {
  ENLIST_NORMATIVES_SUCCESS,
  GET_FILTER_NORMATIVE_SUCCESS,
  GET_FILTER_NORMATIVES_ADMIN_SUCCESS,
  BLOCK_USER,
  DELETE_COMMENT_COMPLAINT,
  RELOAD_NORMATIVE_CARDS,
} from "./constants";

const initialState = {
  normatives: [],
  normativesCount: 0,
  loading: true,
  reloadCards: Date.now(),
};

export function normatives(state = initialState, action) {
  switch (action.type) {
    case ENLIST_NORMATIVES_SUCCESS:
      return { ...state, ...action.payload };
    case GET_FILTER_NORMATIVE_SUCCESS:
      console.log("GET_FILTER_NORMATIVE_SUCCESS", action.payload);
      return { ...action.payload };
    case GET_FILTER_NORMATIVES_ADMIN_SUCCESS:
      return { ...state, ...action.payload };
    case BLOCK_USER:
      return { ...state };
    case DELETE_COMMENT_COMPLAINT:
      return { ...state };
    case RELOAD_NORMATIVE_CARDS:
      return { ...state, reloadCards: Date.now() };
    default:
      return { ...state };
  }
}

export default normatives;
