import {
  ENLIST_NORMATIVES,
  ENLIST_NORMATIVES_SUCCESS,
  GET_FILTER_NORMATIVE,
  GET_FILTER_NORMATIVE_SUCCESS,
  GET_FILTER_NORMATIVES_ADMIN,
  GET_FILTER_NORMATIVES_ADMIN_SUCCESS,
  DELETE_COMMENT_COMPLAINT,
  DELETE_COMMENT_COMPLAINT_SUCCESS,
  BLOCK_USER,
  BLOCK_USER_SUCCESS,
  RELOAD_NORMATIVE_CARDS,
} from "./constants";

export const getNormatives = (payload) => ({
  type: ENLIST_NORMATIVES,
  payload,
});

export const getNormativesSuccess = (payload) => ({
  type: ENLIST_NORMATIVES_SUCCESS,
  payload,
});

export const getFilterNormative = (payload) => ({
  type: GET_FILTER_NORMATIVE,
  payload,
});

export const getFilterNormativeSuccess = (payload) => ({
  type: GET_FILTER_NORMATIVE_SUCCESS,
  payload,
});

export const getFilterNormativeAdmin = (payload) => ({
  type: GET_FILTER_NORMATIVES_ADMIN,
  payload,
});

export const getFilterNormativeAdminSuccess = (payload) => ({
  type: GET_FILTER_NORMATIVES_ADMIN_SUCCESS,
  payload,
});

export const deleteComment = (payload) => ({
  type: DELETE_COMMENT_COMPLAINT,
  payload,
});

export const deleteCommentSuccess = (payload) => ({
  type: DELETE_COMMENT_COMPLAINT_SUCCESS,
  payload,
});

export const blockUser = (payload) => ({
  type: BLOCK_USER,
  payload,
});

export const blockUserSuccess = (payload) => ({
  type: BLOCK_USER_SUCCESS,
  payload,
});

export const reloadNormativeCards = (payload) => ({
  type: RELOAD_NORMATIVE_CARDS,
  payload,
});
