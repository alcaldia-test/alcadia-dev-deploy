import moment from "moment";
import { getErrorMessages } from "./auxiliars";
import {
  CHANGE_DATA_LOCAL_POST,
  CHANGE_DATA_LOCAL_POST_SUCCESS,
  CHANGE_STEP_CREATING,
  CHANGE_STEP_CREATING_SUCCESS,
  DELETE_DISCUSSION_FILE_SUCCESS,
  DELETE_DISCUSSION_FILE,
  WAITING_FROM_SERVER,
  CREATE_DISCUSSION,
  CREATE_DISCUSSION_SUCCESS,
  LOAD_DISCUSSION,
  EDIT_POST_DISCUSSION,
  EDIT_POST_DISCUSSION_SUCCESS,
  DELETE_POST_DISCUSSION,
  EMPTY_LOCALPOST,
  SET_MESSAGES_ERROR,
  MODAL_SAVE_ON_EXIT,
} from "./constants";

export const emptyLocalPost = (payload) => ({
  type: EMPTY_LOCALPOST,
  payload: payload,
});

export const modalSaveOnExit = (payload) => ({
  type: MODAL_SAVE_ON_EXIT,
  payload: payload,
});

export const changeDataLocalPost = (step) => ({
  type: CHANGE_DATA_LOCAL_POST,
  payload: step,
});

export const changeDataLocalPostSuccess = (payload) => ({
  type: CHANGE_DATA_LOCAL_POST_SUCCESS,
  payload,
});

export const changeCreatingStep = (step) => ({
  type: CHANGE_STEP_CREATING,
  payload: step,
});

export const changeCreatingStepSuccess = (payload) => ({
  type: CHANGE_STEP_CREATING_SUCCESS,
  payload,
});

export const deleteFile = (payload) => ({
  type: DELETE_DISCUSSION_FILE,
  payload,
});

export const deleteFileSuccess = (payload) => ({
  type: DELETE_DISCUSSION_FILE_SUCCESS,
  payload,
});

export const waitingFromServer = (payload) => ({
  type: WAITING_FROM_SERVER,
  payload,
});
export const creatingDiscussionSuccess = (payload) => ({
  type: CREATE_DISCUSSION_SUCCESS,
  payload,
});
export const loadingDiscussion = (payload) => ({
  type: LOAD_DISCUSSION,
  payload,
});

export const editPost = (payload) => ({
  type: EDIT_POST_DISCUSSION,
  payload,
});

export const deleteDiscussion = (payload) => ({
  type: DELETE_POST_DISCUSSION,
  payload,
});

export const editPostSuccess = (payload) => ({
  type: EDIT_POST_DISCUSSION_SUCCESS,
  payload,
});

export const errorMessages = (messagesError) => ({
  type: SET_MESSAGES_ERROR,
  payload: messagesError,
});

export const creatingDiscussion = (dataToPost) => {
  const {
    title,
    content,
    tags,
    dueDate,
    extraFile,
    zonesId,
    id,
    postsId,
    completed,
  } = dataToPost;
  const messagesError = getErrorMessages(dataToPost);

  if (completed) {
    if (messagesError.length > 0) {
      return errorMessages(messagesError);
    }
  } else {
    if (title.error) {
      return errorMessages("Debe ingresar un título");
    }
  }
  const postToUpload = {
    title: title?.value,
    content: content ? JSON.stringify(content.value) : undefined,
    completed,
    tags: tags?.value || [],
    dueDate: dueDate
      ? moment(dueDate.value).endOf("day").format("YYYY-MM-DD HH:mm")
      : moment().endOf("day").format("YYYY-MM-DD HH:mm"),
    macrosId: [],
  };
  const zonesIds = [];
  const macrosId = [];
  const files = {
    main: [],
    options: [],
  };

  // Attached step 2
  if (extraFile?.value?.originFileObj) {
    files.main.push({
      tag: "attached",
      file: extraFile?.value?.originFileObj,
    });
  }

  zonesId?.value?.forEach((zone) => {
    if (!zone.selectedAll) {
      zone?.selected?.forEach((subZone) => {
        zonesIds.push(subZone?.id);
      });
    }
    macrosId.push(zone?.id);
  });

  if (zonesIds.length > 0) {
    postToUpload.zonesId = zonesIds;
  }

  if (macrosId.length > 0) {
    postToUpload.macrosId = macrosId;
  }
  console.warn({ postToUpload, files, id, postsId });

  return {
    type: CREATE_DISCUSSION,
    payload: {
      postToUpload,
      files,
      id: id,
      postsId: postsId,
    },
  };
};
