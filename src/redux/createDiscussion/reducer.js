import { checkErrorFields } from "@utils/validateFields";
import {
  CHANGE_DATA_LOCAL_POST,
  CREATE_DISCUSSION_SUCCESS,
  LOAD_DISCUSSION,
  EDIT_POST_DISCUSSION_SUCCESS,
  DELETE_DISCUSSION_FILE_SUCCESS,
  EMPTY_LOCALPOST,
  SET_MESSAGES_ERROR,
  MODAL_SAVE_ON_EXIT,
} from "./constants";

const initialState = {
  localPost: {},
  loading: false,
  showModalCreated: false,
  messagesError: [],
  saveOnExit: false,
};

function discussions(state = initialState, action) {
  switch (action.type) {
    case CHANGE_DATA_LOCAL_POST:
      // eslint-disable-next-line no-case-declarations
      const { name, value } = action.payload;
      return {
        ...state,
        localPost: {
          ...state.localPost,
          [name]: {
            value: value,
            error: checkErrorFields(name, value),
          },
        },
      };
    case CREATE_DISCUSSION_SUCCESS:
      return {
        ...state,
        showModalCreated: true,
      };
    case EDIT_POST_DISCUSSION_SUCCESS:
      return {
        ...state,
        localPost: action.payload,
        loading: false,
      };
    case LOAD_DISCUSSION:
      return {
        ...state,
        loading: action.payload,
      };
    case DELETE_DISCUSSION_FILE_SUCCESS:
      delete state.localPost[action.payload.name];
      return {
        ...state,
      };
    case SET_MESSAGES_ERROR:
      return {
        ...state,
        messagesError: action.payload,
      };
    case MODAL_SAVE_ON_EXIT:
      console.log("REDUCER", action.payload);
      return {
        ...state,
        saveOnExit: action.payload,
      };
    case EMPTY_LOCALPOST:
      return { ...initialState };
    default:
      return { ...state };
  }
}

export default discussions;
