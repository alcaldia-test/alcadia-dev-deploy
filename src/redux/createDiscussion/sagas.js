import { put, takeLatest, takeEvery, select } from "redux-saga/effects";
import message from "@components/Message";
import { AdminDiscussionServices } from "@services/adminDiscussion";
import { AdminVotingServices } from "@services/adminVoting";
import { showLoader } from "@redux/common/actions";
import moment from "moment";
import {
  CHANGE_STEP_CREATING,
  DELETE_DISCUSSION_FILE,
  CREATE_DISCUSSION,
  EDIT_POST_DISCUSSION,
  DELETE_POST_DISCUSSION,
} from "./constants";

import {
  changeCreatingStepSuccess,
  deleteFileSuccess,
  creatingDiscussionSuccess,
  loadingDiscussion,
  editPostSuccess,
  emptyLocalPost,
} from "./actions";
import { reloadCards } from "../discussions/actions";

export function* changinStepCreating({ payload }) {
  try {
    yield put(changeCreatingStepSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* delteFileSaga({ payload }) {
  try {
    const { tag, id, PostionFieldName, value } = payload;
    console.log("Borra el archivo", tag, id, PostionFieldName, value);
    yield put(loadingDiscussion(true));
    const services = new AdminDiscussionServices();
    yield services.deleteFile({ id, tag });
    yield put(deleteFileSuccess({ name: PostionFieldName, value }));
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  } finally {
    yield put(loadingDiscussion(false));
  }
}

export function* createDiscussion({ payload }) {
  try {
    yield put(loadingDiscussion(true));
    const ds = new AdminDiscussionServices();
    const vs = new AdminVotingServices();
    console.warn("Previous send sagas discussion", payload);
    let result = "";
    if (payload?.id) {
      result = yield ds.updateDiscussion(payload?.postToUpload, payload?.id);
    } else {
      result = yield ds.postDiscussion(payload?.postToUpload);
    }
    console.warn("Post send Discussion result:", result);
    // Files of the main Post
    for (let index = 0; index < payload?.files?.main?.length; index++) {
      try {
        const element = payload?.files?.main[index];
        yield vs.createPostUploadFiles({
          file: element?.file,
          tag: element?.tag,
          container: "posts",
          id: result?.postsId || payload?.postsId?.value,
        });
      } catch (error) {}
    }
    yield put(creatingDiscussionSuccess());
  } catch (error) {
    console.log(error);
    message.error("Error al crear Charla");
  } finally {
    yield put(loadingDiscussion(false));
  }
}

export function* editPostSaga({ payload }) {
  try {
    yield put(loadingDiscussion(true));
    yield put(emptyLocalPost());
    const ads = new AdminDiscussionServices();
    const result = yield ads.getPostById(payload.id);
    const getMacro = (state) => state.discussion.macroZones;
    const macros = yield select(getMacro);
    console.warn("Result to edit: ", result);

    const _localPost = {
      ...result,
      id: payload.id,
      fileLink: {
        value: result?.fileLink,
        error: false,
      },
      dueDate: {
        value: result?.dueDate,
        error: moment() > moment(result?.dueDate),
      },
      tags: {
        value: result?.tags,
        error: result?.tags?.length === 0,
      },
      title: {
        value: result?.title,
        error: result?.title === "",
      },
      postsId: { value: result?.postsId, error: false },
    };

    _localPost.zonesId = setLocalPostZonesId(
      macros,
      result?.zones,
      result?.macros
    );
    _localPost.macros = {
      value: result?.macros,
      error: false,
    };

    try {
      if (result.attached) {
        _localPost.attached = {
          value: result?.attached,
          error: false,
        };
      }

      const contentParsed = JSON?.parse(result?.content);
      _localPost.content = {
        value: contentParsed,
        error:
          !contentParsed ||
          contentParsed === "" ||
          result?.content === undefined,
      };
    } catch (error) {
      _localPost.content = result?.content;
    }

    console.log("editPostSuccess", { _localPost, result });
    yield put(editPostSuccess(_localPost));
  } catch (error) {
    console.log("Error intentando editar: ", error);
    message.error("Error recuperando datos, revise su conexión.");
    yield put(loadingDiscussion(false));
  } finally {
    yield put(loadingDiscussion(false));
  }
}

export function* deletePostSaga({ payload }) {
  console.warn("payload ID", payload);
  try {
    yield put(showLoader());
    const ads = new AdminDiscussionServices();
    yield ads.deletePost(payload.id);
    yield put(reloadCards());
  } catch (error) {
    console.log(error);
  }
}

function setLocalPostZonesId(macros, resultZones, resultMacros) {
  const zoneId = [];

  const macrosInResult = macros?.filter((x) =>
    resultMacros?.map((r) => r.id).includes(x.id)
  );
  for (let i = 0; i < macrosInResult.length; i++) {
    let zones = macrosInResult[i].zones;
    let selectedAll = true;
    if (resultZones.length > 0) {
      zones = macrosInResult[i].zones.filter((z) =>
        resultZones.map((rz) => rz.id).includes(z.id)
      );
      selectedAll = false;
    }

    zoneId.push({
      id: macrosInResult[i].id,
      name: macrosInResult[i].name,
      selectedAll,
      selected: zones.map((z) => ({
        id: z.id,
        zone: z.name,
        checked: true,
      })),
    });
  }

  return { value: zoneId.length > 0 ? zoneId : undefined, error: false };
}

export default function* rootSaga() {
  yield takeLatest(CHANGE_STEP_CREATING, changinStepCreating);
  yield takeEvery(DELETE_DISCUSSION_FILE, delteFileSaga);
  yield takeLatest(CREATE_DISCUSSION, createDiscussion);
  yield takeLatest(EDIT_POST_DISCUSSION, editPostSaga);
  yield takeLatest(DELETE_POST_DISCUSSION, deletePostSaga);
}
