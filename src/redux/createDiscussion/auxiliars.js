const setMessages = (fieldType) => {
  switch (fieldType) {
    case "title":
      return "Título no válido";
    case "dueDate":
      return "Fecha no válida para la culminación de la charla";
    case "content":
      return "Explicación de la charla, no válida";
    case "zonesId":
      return "Zonas no definidas para participar en la charla";
    case "tags":
      return "No se ha definido etiquetas para esta publicación";
    default:
      return "";
  }
};

export const getErrorMessages = (localPost) => {
  const messages = [];
  for (const field in localPost) {
    if (localPost[field].error) {
      messages.push(setMessages(field));
    }
  }
  return messages;
};

// export const validateLocalpost = (localPost) => {
//   const messages = [];
//   for (const field in localPost) {
//     if (localPost[field].error) {
//       messages.push(setMessages(field));
//     }
//   }
//   return messages;
// };
