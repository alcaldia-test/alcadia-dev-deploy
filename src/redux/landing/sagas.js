import { put, takeLatest } from "redux-saga/effects";
import { showMessageError } from "@utils/request";
import { showLoader, hideLoader } from "@redux/common/actions";
import { REQUEST_LANDING_START } from "./constants";
import { requestLandingSuccess } from "./actions";
import { landingServices } from "@services/landing";

export function* requestLanding({ payload }) {
  try {
    yield put(showLoader());
    const result = yield landingServices.getPosts(payload);
    yield put(requestLandingSuccess(result));
  } catch (err) {
    yield showMessageError(err);
  } finally {
    yield put(hideLoader());
  }
}

export default function* landingSaga() {
  yield takeLatest(REQUEST_LANDING_START, requestLanding);
}
