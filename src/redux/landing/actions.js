import { REQUEST_LANDING_START, REQUEST_LANDING_SUCCESS } from "./constants";

export const requestLandingStart = (payload) => ({
  type: REQUEST_LANDING_START,
  payload,
});
export const requestLandingSuccess = (payload) => ({
  type: REQUEST_LANDING_SUCCESS,
  payload,
});
