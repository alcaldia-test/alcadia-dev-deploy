import { REQUEST_LANDING_SUCCESS } from "./constants";

const initialState = {
  achievements: [],
  announcements: [],
  discussions: [],
  votings: [],
  normatives: [],
};

export function landing(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LANDING_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}
export default landing;
