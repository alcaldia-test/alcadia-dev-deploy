import { all } from "redux-saga/effects";
import announcementsSagas from "./announcements/saga";
import attainmentsSagas from "./attainments/saga";
import authSagas from "./auth/saga";
import globalSagas from "./global/saga";
import postsSagas from "./votations/saga";
import discussionSagas from "./discussions/saga";
import discussionIdSagas from "./discussionsId/sagas";
import commentSagas from "./comments/saga";
import normativeComments from "./normativesComments/sagas";
import relatedContent from "./relatedContent/sagas";
import chapters from "./chapters/sagas";
import normatives from "./normatives/sagas";
import voteNormative from "./normativesVote/sagas";
// eslint-disable-next-line no-unused-vars
import landingSaga from "./landing/sagas";
import creatingNormative from "./createNormative/sagas";
import creatingDiscussion from "./createDiscussion/sagas";
import usersModule from "./users/saga";
import complaints from "./complaints/saga";
// import clientVotingSaga from "./client-voting/saga";
import adminvotingSaga from "./admin_voting/saga"; // Agregado
import uservoting from "./voting/saga";
import permissions from "./permissions/saga"; // Agregado
import verification from "./verification/saga";
import accessRequest from "./accessRequest/saga";
import recoveyPassword from "./recoveyPassword/saga";
import common from "./common/saga";

export default function* rootSaga() {
  yield all([
    announcementsSagas(),
    attainmentsSagas(),
    authSagas(),
    globalSagas(),
    postsSagas(),
    discussionSagas(),
    commentSagas(),
    normativeComments(),
    relatedContent(),
    chapters(),
    normatives(),
    adminvotingSaga(),
    uservoting(),
    creatingNormative(),
    creatingDiscussion(),
    voteNormative(),
    landingSaga(),
    permissions(),
    verification(),
    usersModule(),
    accessRequest(),
    discussionIdSagas(),
    complaints(),
    recoveyPassword(),
    common(),
  ]);
}
