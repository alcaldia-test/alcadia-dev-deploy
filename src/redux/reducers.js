import { combineReducers } from "redux";
import common from "@redux/common/reducer";
import announcements from "./announcements/reducer";
import attainment from "./attainments/reducer";
import auth from "@redux/auth/reducer";
import settings from "@redux/settings/reducer";
import global from "@redux/global/reducer";
import vote from "@redux/votations/reducer";
import voting from "@redux/voting/reducer";
import discussion from "@redux/discussions/reducer";
import discussionsId from "@redux/discussionsId/reducers";
import comments from "@redux/comments/reducer";
import normativeComments from "@redux/normativesComments/reducer";
import relatedContent from "@redux/relatedContent/reducer";
import chapters from "@redux/chapters/reducers";
import normatives from "@redux/normatives/reducer";
import landing from "@redux/landing/reducer";
import normativeVote from "@redux/normativesVote/reducer";
import creatingNormative from "@redux/createNormative/reducer";
import creatingDiscussion from "@redux/createDiscussion/reducer";
import adminVoting from "@redux/admin_voting/reducer";
import permissions from "@redux/permissions/reducer";
import verification from "@redux/verification/reducer";
import accessRequest from "@redux/accessRequest/reducer";
import users from "@redux/users/reducer";
import complaints from "@redux/complaints/reducer";
import recoveyPassword from "@redux/recoveyPassword/reducers";

export default function createReducer(asyncReducers) {
  return combineReducers({
    users,
    announcements,
    attainment,
    auth,
    vote,
    voting,
    common,
    settings,
    global,
    discussion,
    discussionsId,
    comments,
    normativeComments,
    relatedContent,
    chapters,
    landing,
    normatives,
    adminVoting,
    permissions,
    verification,
    creatingNormative,
    creatingDiscussion,
    normativeVote,
    accessRequest,
    complaints,
    recoveyPassword,
    ...asyncReducers,
  });
}
