import {
  GET_ATTAINMENT_SUCCESS,
  GET_ATTAINMENT_DETAIL,
  GET_ATTAINMENTS_START,
  GET_ATTAINMENTS_SUCCESS,
  SHOW_HIDE_CREATING_SECTION,
  SET_ATTAINTMENTS_COUNT,
  DELETE_ATTAINMENTS_START,
  PATCH_ATTAINMENTS_START,
  POST_ATTAINMENTS_START,
} from "./constants";

export const postAttainment = (payload) => ({
  type: POST_ATTAINMENTS_START,
  payload,
});
export const deleteAttainment = (payload) => ({
  type: DELETE_ATTAINMENTS_START,
  payload,
});
export const editAttainment = (payload) => ({
  type: PATCH_ATTAINMENTS_START,
  payload,
});

export const showHideCreatingSection = (payload) => ({
  type: SHOW_HIDE_CREATING_SECTION,
  payload,
});

export const setAttainmentCount = (payload) => ({
  type: SET_ATTAINTMENTS_COUNT,
  payload,
});

export const getAttainmentSucess = (payload) => ({
  type: GET_ATTAINMENT_SUCCESS,
  payload,
});
export const getAttainmentDetail = (payload) => ({
  type: GET_ATTAINMENT_DETAIL,
  payload,
});

export const getAttainmentsSucess = (payload) => ({
  type: GET_ATTAINMENTS_SUCCESS,
  payload,
});
export const getAttainmentsStart = (payload) => ({
  type: GET_ATTAINMENTS_START,
  payload,
});
