import { put, takeLatest, call, all } from "@redux-saga/core/effects";
import {
  getAttainmentSucess,
  getAttainmentsSucess,
  setAttainmentCount,
  getAttainmentsStart,
} from "./actions";
import {
  GET_ATTAINMENT_DETAIL,
  GET_ATTAINMENTS_START,
  DELETE_ATTAINMENTS_START,
  PATCH_ATTAINMENTS_START,
  POST_ATTAINMENTS_START,
} from "./constants";
import { AttainementServices } from "@services/attainements";
import request, {
  deleteOptions,
  postOptions,
  patchOptions,
  postOptionsFormData,
} from "@utils/request";

export function* getAttainment({ payload }) {
  try {
    const Attainement = new AttainementServices();
    const attainementDetail = yield Attainement.getAttainement(payload.id);
    yield put(getAttainmentSucess({ ...attainementDetail }));
  } catch (error) {
    console.log(error);
  }
}

export function* getAttainments({
  payload: { resolve, reject, withCount, ...filter },
}) {
  try {
    const allEffects = [call(resolve)];
    const attainement = new AttainementServices();
    const attainements = yield attainement.getAttainements(filter);

    if (withCount) {
      const count = yield attainement.getAttainmentCount(filter);

      allEffects.unshift(put(setAttainmentCount(count)));
    }

    allEffects.unshift(put(getAttainmentsSucess(attainements)));

    yield all(allEffects);
  } catch (error) {
    console.log(error);
    yield call(reject, "Problemas con el Server");
  }
}

export function* deleteAttainmentSaga({
  payload: { resolve, reject, id, ...body },
}) {
  let url, options;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    url = `${server}/attainments/${id}`;
    options = deleteOptions();
    yield call(request, url, options);

    yield put(getAttainmentsStart({ withCount: true, resolve, reject }));
  } catch (error) {
    console.log(error);
    yield call(reject, "Problemas con el Server");
  }
}

export function* editAttainmentSaga({
  payload: { resolve, fileLink, reject, id, tag, postsId, banner, ...body },
}) {
  let url, options;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    url = `${server}/attainments/${id}`;
    options = patchOptions(body);
    yield call(request, url, options);

    if (!fileLink) {
      const where = encodeURI(JSON.stringify({ postsId, tag: "banner" }));
      url = `${server}/file-storages?where=${where}`;
      options = deleteOptions();
      yield call(request, url, options);
    }

    if (banner) {
      url = `${server}/containers/posts/upload/banner`;
      const formdata = new FormData();
      formdata.append(postsId, banner?.originFileObj);

      options = postOptionsFormData(formdata);
      yield call(request, url, options);
    }

    yield put(getAttainmentsStart({ withCount: true, resolve, reject }));
  } catch (error) {
    console.log(error);
    yield call(reject, "Problemas con el Server");
  }
}

export function* postAttainmentSaga({
  payload: { resolve, reject, banner, ...body },
}) {
  let url, options;
  const server = process.env.NEXT_PUBLIC_URL_API;

  try {
    url = `${server}/attainments`;
    options = postOptions(body);
    const created = yield call(request, url, options);

    if (banner) {
      url = `${server}/containers/posts/upload/banner`;
      const formdata = new FormData();
      formdata.append(created?.postsId, banner?.originFileObj);

      options = postOptionsFormData(formdata);
      yield call(request, url, options);
    }

    yield put(getAttainmentsStart({ withCount: true, resolve, reject }));
  } catch (error) {
    console.log(error);
    yield call(reject, "Problemas con el Server");
  }
}

export default function* rootSaga() {
  yield takeLatest(GET_ATTAINMENT_DETAIL, getAttainment);
  yield takeLatest(GET_ATTAINMENTS_START, getAttainments);
  yield takeLatest(DELETE_ATTAINMENTS_START, deleteAttainmentSaga);
  yield takeLatest(PATCH_ATTAINMENTS_START, editAttainmentSaga);
  yield takeLatest(POST_ATTAINMENTS_START, postAttainmentSaga);
}
