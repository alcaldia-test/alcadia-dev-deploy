import {
  SHOW_HIDE_CREATING_SECTION,
  GET_ATTAINMENT_SUCCESS,
  GET_ATTAINMENTS_SUCCESS,
  SET_ATTAINTMENTS_COUNT,
} from "./constants";

const initialState = {
  attainment: null,
  datas: [],
  count: 0,
  creating: false,
};

const attainment = (state = initialState, action) => {
  switch (action.type) {
    case GET_ATTAINMENT_SUCCESS:
      return {
        ...state,
        attainment: action.payload,
      };
    case GET_ATTAINMENTS_SUCCESS:
      return {
        ...state,
        datas: action.payload,
      };
    case SET_ATTAINTMENTS_COUNT:
      return {
        ...state,
        count: action.payload,
      };
    case SHOW_HIDE_CREATING_SECTION:
      return {
        ...state,
        creating: action.payload,
      };
    default:
      return state;
  }
};

export default attainment;
