import {
  SHOW_HIDE_CREATING_SECTION_SUCCESS,
  CHANGE_STEP_CREATING_SUCCESS,
  GET_VOTING_LIST_SUCCESS,
  CHANGE_DATA_LOCAL_POST_SUCCESS,
  CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE_SUCCESS,
  CHANGE_DATA_LOCAL_FILTER_SUCCESS,
  WAITING_FROM_SERVER,
  CLOSE_CREATED_MODAL_SUCCESS,
  GET_ZONE_SUCCESS,
  GET_MACRO_ZONES_SUCCESS,
  GET_TOTAL_POST_SUCCESS,
  GET_POST_BY_ID_SUCCESS,
  CREATE_VOTING_PROPOSAL_SUCCESS,
  UPLOAD_VOTING_PROPOSAL_ATTACHED_SUCCESS,
  DELETE_VOTING_PROPOSAL_SUCCESS,
  OPEN_DELETE_MODAL_SUCCESS,
  CLOSE_DELETE_MODAL_SUCCESS,
  OPEN_ERROR_MODAL_SUCCESS,
  CLOSE_ERROR_MODAL_SUCCESS,
  EDIT_POST_SUCCESS,
  DELETE_VOTING_FILE_SUCCESS,
  ADD_PROPOSAL_IN_SERVER_SUCCESS,
  EDIT_PROPOSAL_IN_SERVER_SUCCESS,
  DELETE_PROPOSAL_IN_SERVER_SUCCESS,
  CHANGE_VALIDATINFIELD_STATE_SUCCESS,
} from "./constants";

export const initialState = {
  zones: [],
  macroZones: [],
  textInAutoComplete: "",
  error: null,
  validateField: false,
  filters: {
    page: 1,
    order: "DESC",
    like: "",
    macroZone: [],
    completed: null,
  },
  creating: false,
  deleting: false,
  total: 0,
  creatingStep: 0,
  secctionsSizes: {
    header: 50,
    filter: 70,
    footer: 50,
  },
  loading: false,
  activePost: undefined,
  listOfVoting: [],
  localPost: {},
  showModalCreated: false,
};

const adminVoting = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_HIDE_CREATING_SECTION_SUCCESS:
      return {
        ...state,
        creating: !state.creating,
        localPost: {},
        creatingStep: 0,
      };

    case CHANGE_STEP_CREATING_SUCCESS:
      if (state.loading) {
        return { ...state };
      } else {
        return { ...state, creatingStep: action.payload };
      }

    case CHANGE_DATA_LOCAL_POST_SUCCESS:
      return {
        ...state,
        localPost: {
          ...state.localPost,
          [action.payload.name]: action.payload.value,
        },
      };

    case CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE_SUCCESS:
      return {
        ...state,
        textInAutoComplete: action.payload,
      };

    case CHANGE_DATA_LOCAL_FILTER_SUCCESS:
      return {
        ...state,
        filters: {
          ...state.filters,
          page: 1,
          like: state.textInAutoComplete,
          [action.payload.name]: action.payload.value,
        },
      };

    case WAITING_FROM_SERVER:
      return { ...state, loading: true };

    case CLOSE_CREATED_MODAL_SUCCESS:
      return {
        ...state,
        showModalCreated: false,
        creating: false,
        localPost: {},
        creatingStep: 0,
      };

    case GET_VOTING_LIST_SUCCESS:
      return { ...state, loading: false, listOfVoting: action.payload };

    case GET_ZONE_SUCCESS:
      return { ...state, loading: false, zones: action.payload };

    case GET_MACRO_ZONES_SUCCESS:
      return { ...state, loading: false, macroZones: action.payload };

    case GET_TOTAL_POST_SUCCESS:
      return { ...state, loading: false, total: action.payload };

    case GET_POST_BY_ID_SUCCESS:
      return { ...state, loading: false, activePost: action.payload };

    case CREATE_VOTING_PROPOSAL_SUCCESS:
      return {
        ...state,
        loading: false,
        showModalCreated: true,
        validateField: false,
      };

    case UPLOAD_VOTING_PROPOSAL_ATTACHED_SUCCESS:
      return { ...state, loading: false };

    case DELETE_VOTING_PROPOSAL_SUCCESS:
      return {
        ...state,
        loading: false,
        deleting: false,
        localPost: {},
        listOfVoting: state.listOfVoting.filter((x) => x.id !== action.payload),
      };

    case OPEN_DELETE_MODAL_SUCCESS:
      return {
        ...state,
        deleting: true,
        localPost: action.payload,
      };

    case CLOSE_DELETE_MODAL_SUCCESS:
      return {
        ...state,
        deleting: false,
        localPost: {},
      };

    case CHANGE_VALIDATINFIELD_STATE_SUCCESS:
      return {
        ...state,
        validateField: !state.validateField,
      };

    case OPEN_ERROR_MODAL_SUCCESS:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case CLOSE_ERROR_MODAL_SUCCESS:
      return {
        ...state,
        error: null,
      };

    case EDIT_POST_SUCCESS:
      return {
        ...state,
        creating: true,
        localPost: action.payload,
        loading: false,
      };

    case DELETE_VOTING_FILE_SUCCESS:
      return {
        ...state,
        localPost: {
          ...state.localPost,
          [action.payload.name]: action.payload.value,
        },
        loading: false,
      };

    case ADD_PROPOSAL_IN_SERVER_SUCCESS:
      return { ...state, loading: false };

    case EDIT_PROPOSAL_IN_SERVER_SUCCESS:
      return { ...state, loading: false };

    case DELETE_PROPOSAL_IN_SERVER_SUCCESS:
      return {
        ...state,
        localPost: { ...state.localPost, proposals: action.payload },
        loading: false,
      };

    default:
      return state;
  }
};

export default adminVoting;
