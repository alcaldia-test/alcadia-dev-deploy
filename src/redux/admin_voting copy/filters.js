export const filterGetProposal = ({ page = 0, limit = 10 }) => {
  const _skip = page * limit - limit;

  return {
    limit: limit,
    skip: _skip > 0 ? _skip : 0,
    fields: {
      reviewerId: false,
    },
    include: [
      {
        relation: "posts",
        scope: {
          fields: {
            id: true,
            title: true,
            content: true,
          },
          include: [
            {
              relation: "fileStorages",
            },
            {
              relation: "macros",
              scope: {
                fields: {
                  id: true,
                  name: true,
                },
              },
            },
            {
              relation: "zones",
              scope: {
                fields: {
                  id: true,
                  name: true,
                },
              },
            },
          ],
        },
      },
      {
        relation: "proposals",
        scope: {
          fields: {
            id: true,
            title: true,
            content: true,
            voteId: true,
            fileLink: true,
          },
        },
      },
    ],
  };
};

export const filterGetMacroZones = () => {
  return {
    fields: {},
    include: [
      {
        relation: "zones",
      },
    ],
  };
};

export const filterGetPostById = () => {
  return {
    fields: {},
    include: [
      {
        relation: "fileStorages",
      },
    ],
  };
};

export const whereToTotalPost = () => {
  return {
    type: "vote",
  };
};

export const filterGetProposalStaticFilter = ({
  limit = 10,
  page = 0,
  order = "ASC",
  like = "",
  macros = [],
}) => {
  const _skip = page * limit - limit;

  const filter = {
    limit,
    skip: _skip > 0 ? _skip : 0,
    order,
    where: {
      like,
      //  "zones":[],
      //  "macros":[1],
      //   "status":"", //active , finished supporting review
    },
  };

  if (macros.length > 0) {
    filter.where.macros = macros;
  }

  // console.log(filter);

  return filter;
};
