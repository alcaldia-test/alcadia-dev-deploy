import { put, takeLatest, takeEvery, select } from "redux-saga/effects";
import { ZonesServices } from "@services/zones";
import { AdminVotingServices } from "@services/adminVoting";

import {
  SHOW_HIDE_CREATING_SECTION,
  CHANGE_STEP_CREATING,
  CHANGE_DATA_LOCAL_POST,
  CHANGE_DATA_LOCAL_FILTER,
  CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE,
  CLOSE_CREATED_MODAL,
  GET_VOTING_LIST,
  GET_ZONE,
  GET_MACRO_ZONES,
  GET_TOTAL_POST,
  GET_POST_BY_ID,
  CREATE_VOTING_PROPOSAL,
  UPLOAD_VOTING_PROPOSAL_ATTACHED,
  OPEN_DELETE_MODAL,
  CLOSE_DELETE_MODAL,
  DELETE_VOTING_PROPOSAL,
  OPEN_ERROR_MODAL,
  CLOSE_ERROR_MODAL,
  EDIT_POST,
  DELETE_VOTING_FILE,
  DELETE_PROPOSAL_IN_SERVER,
  ADD_PROPOSAL_IN_SERVER,
  CHANGE_VALIDATINFIELD_STATE,
} from "./constants";
import {
  showHideCreatingSectionSuccess,
  changeCreatingStepSuccess,
  getVotingList,
  getVotingListSuccess,
  changeDataLocalPostSuccess,
  changeDataLocalTextInAutoCompleteSuccess,
  closeCreatingModalSuccess,
  waitingFromServer,
  changeDataLocalFilterSuccess,
  getZoneSuccess,
  getMacroZonesSuccess,
  getTotalPost,
  getTotalPostSuccess,
  getPostByIDSuccess,
  creatingVotingProposalSuccess,
  openDeleteModalSuccess,
  closeDeleteModalSuccess,
  deleteProposalSuccess,
  closeCreatingModal,
  openErrorModalSuccess,
  closeErrorModalSuccess,
  openErrorModal,
  editPostSuccess,
  deleteFileSuccess,
  deleteproposalinServerSuccess,
  editPost,
  addproposalinServerSuccess,
  changevalidatingFieldSuccess,
} from "./actions";
import moment from "moment";

export function* showHideCreating() {
  try {
    yield put(showHideCreatingSectionSuccess());
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* changinStepCreating({ payload }) {
  try {
    yield put(changeCreatingStepSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* changeValidatingStateSaga({ payload }) {
  try {
    yield put(changevalidatingFieldSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* changeLocalPostSaga({ payload }) {
  try {
    yield put(changeDataLocalPostSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* changeLocalTextInAutoCompleteSaga({ payload }) {
  try {
    yield put(changeDataLocalTextInAutoCompleteSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* changeLocalFilterSaga({ payload }) {
  try {
    yield put(changeDataLocalFilterSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* closeLocalModalCreated({ payload }) {
  try {
    yield put(closeCreatingModalSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* getVotingListSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const getFilters = (state) => state.adminVoting.filters;
    const filter = yield select(getFilters);
    const services = new AdminVotingServices();
    const result = yield services.getVotingListOptions({
      page: filter?.page,
      order: filter?.order,
      like: filter?.like,
      macros: filter?.macroZone,
      completed: filter?.completed,
    });
    yield put(getVotingListSuccess(result));
  } catch (error) {
    console.log("Error en saga get voting list");
    console.log(error);
  }
}

export function* getZonesSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const zones = new ZonesServices();
    const result = yield zones.getZones();
    // const result = yield call(fetchCustom, {
    //   endpoint: "zones",
    // });
    yield put(getZoneSuccess(result));
  } catch (error) {
    console.log("Error en saga get zones");
    console.log(error);
  }
}

export function* getMacroZonesSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const zones = new ZonesServices();
    const result = yield zones.getMacroZones();
    // const result = yield call(fetchCustom, {
    //   filter: filterGetMacroZones(),
    //   endpoint: "macros",
    // });
    yield put(getMacroZonesSuccess(result));
  } catch (error) {
    console.log("Error en saga get zones");
    console.log(error);
  }
}

export function* getTotalPostSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const services = new AdminVotingServices();
    const getFilters = (state) => state.adminVoting.filters;
    const filter = yield select(getFilters);
    const result = yield services.getTotalPost({
      page: filter?.page,
      order: filter?.order,
      like: filter?.like,
      macros: filter?.macroZone,
      completed: filter?.completed,
    });
    yield put(getTotalPostSuccess(result?.count));
  } catch (error) {
    console.log("Error en saga get zones");
    console.log(error);
  }
}

export function* getPostByIdSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const services = new AdminVotingServices();
    const result = yield services.getPostById(payload);
    yield put(getPostByIDSuccess(result));
  } catch (error) {
    console.log("Error en saga get zones");
    console.log(error);
  }
}

// =========Create/save
export function* createVotingSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const services = new AdminVotingServices();

    let result = "";
    if (payload?.id) {
      for (
        let index = 0;
        index < payload?.postToUpload?.options?.length;
        index++
      ) {
        try {
          const option = payload?.postToUpload?.options[index];
          if (typeof option?.id === "number") {
            const data = {
              voteId: payload?.id,
              title: option?.title,
              content: option?.content,
            };
            yield services.createOptionToProposal(data);
          } else {
            const data = {
              title: option?.title,
              content: option?.content,
            };
            yield services.updateOptionToProposal(data, option?.id);
          }
        } catch (error) {}
      }
      yield services.updatePostData(payload?.postToUpload, payload?.id);
      result = yield services.getPostById(payload?.id);
    } else {
      const data = { ...payload?.postToUpload };
      for (let index = 0; index < data?.options?.length; index++) {
        const option = data.options[index];
        delete option.id;
      }
      result = yield services.createPostUploadData(data);
    }

    // Files of the main Post
    for (let index = 0; index < payload?.files?.main?.length; index++) {
      try {
        const element = payload?.files?.main[index];
        // console.log({
        //   file: element?.file,
        //   tag: element?.tag,
        //   container: "posts",
        //   id: result?.postsId || payload?.postsId,
        // });
        yield services.createPostUploadFiles({
          file: element?.file,
          tag: element?.tag,
          container: "posts",
          id: result?.postsId || payload?.postsId,
        });
      } catch (error) {}
    }

    // Files of the options
    for (let index = 0; index < payload?.files?.options?.length; index++) {
      try {
        const element = payload?.files?.options[index];
        const resultOption = result?.options
          ? result?.options[element?.indexProposal]
          : result?.proposals[element?.indexProposal];
        yield services.createPostUploadFiles({
          file: element?.file,
          tag: element?.tag,
          container: "proposal",
          id: resultOption?.id,
        });
      } catch (error) {
        // console.log({
        //   element: payload?.files?.options,
        //   resultOption: result?.options,
        // });
        console.log(error);
      }
    }

    yield put(getVotingList());
    yield put(getTotalPost());
    yield put(creatingVotingProposalSuccess());
  } catch (error) {
    yield put(openErrorModal("Error Creando la votacion"));
    console.log("Error en saga creating");
    console.log(error);
  }
}

export function* openDeleteModalSaga({ payload }) {
  try {
    yield put(openDeleteModalSuccess(payload));
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export function* closeDeleteModalSaga() {
  try {
    yield put(closeDeleteModalSuccess());
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export function* deleteSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    yield put(closeCreatingModal());
    const services = new AdminVotingServices();
    yield services.delete(payload);
    yield put(getVotingList());
    yield put(getTotalPost());
    yield put(deleteProposalSuccess());
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export function* openErrorSaga({ payload }) {
  try {
    yield put(openErrorModalSuccess(payload));
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export function* closeErrorSaga({ payload }) {
  try {
    yield put(closeErrorModalSuccess(payload));
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export function* editPostSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const services = new AdminVotingServices();
    const result = yield services.getPostById(payload);
    const getZones = (state) => state.adminVoting.zones;
    const zones = yield select(getZones);
    const getMacro = (state) => state.adminVoting.macroZones;
    const macros = yield select(getMacro);
    const _localPost = {
      id: payload,
      fileLink: result?.fileLink,
      attached: result?.attached,
      completed: result?.completed,
      // content: JSON?.parse(result?.content),
      dueDate: moment(result?.dueDate),
      proposals: [],
      question: result?.question,
      startDate: moment(result?.startDate),
      tags: result?.tags,
      title: result?.title,
      postsId: result?.postsId,
    };
    try {
      _localPost.content = JSON?.parse(result?.content);
    } catch (error) {
      _localPost.content = result?.content;
    }
    for (let index = 0; index < result?.proposals?.length; index++) {
      const proposals = result.proposals[index];
      const _proposal = {
        fileLink: proposals?.fileLink,
        id: proposals?.id,
        title: proposals?.title,
        attached: proposals?.attached,
      };
      try {
        _proposal.content = JSON?.parse(proposals?.content);
      } catch (error) {
        _proposal.content = proposals?.content;
      }
      _localPost.proposals.push(_proposal);
    }
    let _zonesid = [];

    for (let index = 0; index < result?.zones?.length; index++) {
      const zone = result.zones[index];
      const macroId = zones.find((x) => x.id === zone.id).macroId;
      const macro = macros.find((x) => x.id === macroId);
      // check if the macro is previous added
      const _addedPrevious =
        _zonesid.find((x) => x.id === macroId)?.selected || [];
      const _previosMacroWithNewZone = {
        id: macroId,
        name: macro.name,
        selected: [
          ..._addedPrevious,
          {
            id: zone.id,
            zone: zone.name,
            checked: true,
          },
        ],
        selectedAll: false,
      };
      _zonesid = _zonesid.filter((x) => x.id !== macroId);
      _zonesid.push(_previosMacroWithNewZone);
    }

    for (let index = 0; index < result?.macros?.length; index++) {
      const macro = result?.macros[index];
      const zonesOfMacro = zones.filter((x) => x.macroId === macro.id);
      const __selected = [];

      for (let index = 0; index < zonesOfMacro.length; index++) {
        const zoneToAdd = zonesOfMacro[index];
        __selected.push({
          id: zoneToAdd.id,
          zone: zoneToAdd.name,
          checked: true,
        });
      }

      const _MacroWithNewZone = {
        id: macro.id,
        name: macro.name,
        selected: __selected,
      };
      _zonesid = _zonesid.filter((x) => x.id !== macro.id);
      _zonesid.push(_MacroWithNewZone);
    }

    _localPost.zonesId = _zonesid;
    // console.log({ _localPost, result });
    yield put(editPostSuccess(_localPost));
  } catch (error) {
    console.log(error);
    yield put(openErrorModal("Error Intentando Editar"));
  }
}

export function* delteFileSaa({ payload }) {
  try {
    const { tag, id, PostionFieldName, value, isProposal } = payload;
    yield put(waitingFromServer());
    const services = new AdminVotingServices();
    yield services.deleteFile({ id, tag, isProposal });
    yield put(deleteFileSuccess({ name: PostionFieldName, value }));
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export function* deleteProposalSaga({ payload }) {
  try {
    yield put(waitingFromServer());
    const services = new AdminVotingServices();
    yield services.deleteProposalInServer(payload.id);
    yield put(deleteproposalinServerSuccess(payload.remaininProposal));
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export function* addProposalSaga({ payload }) {
  try {
    const { vootingId, proposalOrdered } = payload;
    yield put(waitingFromServer());
    const services = new AdminVotingServices();

    let result = "";
    if (payload?.id) {
      result = yield services.updateProposalData(proposalOrdered, payload?.id);
    } else {
      result = yield services.createPostUploadProposal(proposalOrdered);
    }
    // console.log({ payload, result });
    // Files of the options
    for (let index = 0; index < payload?.files?.length; index++) {
      try {
        const element = payload?.files[index];
        yield services.createPostUploadFiles({
          file: element?.file,
          tag: element?.tag,
          container: "proposal",
          id: result?.id || payload?.id,
        });
      } catch (error) {}
    }

    yield put(editPost(vootingId));
    yield put(addproposalinServerSuccess());
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  }
}

export default function* rootSaga() {
  yield takeLatest(SHOW_HIDE_CREATING_SECTION, showHideCreating);
  yield takeLatest(CHANGE_STEP_CREATING, changinStepCreating);
  yield takeLatest(CLOSE_CREATED_MODAL, closeLocalModalCreated);
  yield takeLatest(CHANGE_DATA_LOCAL_FILTER, changeLocalFilterSaga);
  yield takeLatest(
    CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE,
    changeLocalTextInAutoCompleteSaga
  );
  yield takeLatest(GET_VOTING_LIST, getVotingListSaga);
  yield takeLatest(GET_ZONE, getZonesSaga);
  yield takeLatest(GET_MACRO_ZONES, getMacroZonesSaga);
  yield takeLatest(GET_TOTAL_POST, getTotalPostSaga);
  yield takeLatest(GET_POST_BY_ID, getPostByIdSaga);
  yield takeLatest(CREATE_VOTING_PROPOSAL, createVotingSaga);
  yield takeLatest(UPLOAD_VOTING_PROPOSAL_ATTACHED, createVotingSaga);
  yield takeEvery(CHANGE_DATA_LOCAL_POST, changeLocalPostSaga);
  yield takeEvery(OPEN_DELETE_MODAL, openDeleteModalSaga);
  yield takeEvery(CLOSE_DELETE_MODAL, closeDeleteModalSaga);
  yield takeEvery(DELETE_VOTING_PROPOSAL, deleteSaga);
  yield takeEvery(OPEN_ERROR_MODAL, openErrorSaga);
  yield takeEvery(CLOSE_ERROR_MODAL, openErrorSaga);
  yield takeEvery(EDIT_POST, editPostSaga);
  yield takeEvery(DELETE_VOTING_FILE, delteFileSaa);
  yield takeEvery(DELETE_PROPOSAL_IN_SERVER, deleteProposalSaga);
  yield takeEvery(ADD_PROPOSAL_IN_SERVER, addProposalSaga);
  yield takeEvery(CHANGE_VALIDATINFIELD_STATE, changeValidatingStateSaga);
}
