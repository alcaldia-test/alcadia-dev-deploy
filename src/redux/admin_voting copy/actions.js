import moment from "moment";
import {
  SHOW_HIDE_CREATING_SECTION,
  SHOW_HIDE_CREATING_SECTION_SUCCESS,
  CHANGE_STEP_CREATING,
  CHANGE_STEP_CREATING_SUCCESS,
  CHANGE_DATA_LOCAL_POST,
  CHANGE_DATA_LOCAL_POST_SUCCESS,
  CHANGE_DATA_LOCAL_FILTER,
  CHANGE_DATA_LOCAL_FILTER_SUCCESS,
  CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE,
  CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE_SUCCESS,
  WAITING_FROM_SERVER,
  CLOSE_CREATED_MODAL,
  CLOSE_CREATED_MODAL_SUCCESS,
  GET_VOTING_LIST,
  GET_VOTING_LIST_SUCCESS,
  GET_ZONE,
  GET_ZONE_SUCCESS,
  GET_MACRO_ZONES,
  GET_MACRO_ZONES_SUCCESS,
  GET_TOTAL_POST,
  GET_TOTAL_POST_SUCCESS,
  GET_POST_BY_ID,
  GET_POST_BY_ID_SUCCESS,
  CREATE_VOTING_PROPOSAL,
  CREATE_VOTING_PROPOSAL_SUCCESS,
  CHANGE_VALIDATINFIELD_STATE,
  CHANGE_VALIDATINFIELD_STATE_SUCCESS,
  UPLOAD_VOTING_PROPOSAL_ATTACHED,
  UPLOAD_VOTING_PROPOSAL_ATTACHED_SUCCESS,
  DELETE_VOTING_PROPOSAL,
  DELETE_VOTING_PROPOSAL_SUCCESS,
  OPEN_DELETE_MODAL,
  OPEN_DELETE_MODAL_SUCCESS,
  CLOSE_DELETE_MODAL,
  CLOSE_DELETE_MODAL_SUCCESS,
  OPEN_ERROR_MODAL,
  OPEN_ERROR_MODAL_SUCCESS,
  CLOSE_ERROR_MODAL,
  CLOSE_ERROR_MODAL_SUCCESS,
  EDIT_POST,
  EDIT_POST_SUCCESS,
  DELETE_VOTING_FILE,
  DELETE_VOTING_FILE_SUCCESS,
  ADD_PROPOSAL_IN_SERVER,
  ADD_PROPOSAL_IN_SERVER_SUCCESS,
  EDIT_PROPOSAL_IN_SERVER,
  EDIT_PROPOSAL_IN_SERVER_SUCCESS,
  DELETE_PROPOSAL_IN_SERVER,
  DELETE_PROPOSAL_IN_SERVER_SUCCESS,
} from "./constants";

export const showHideCreatingSection = () => ({
  type: SHOW_HIDE_CREATING_SECTION,
});

export const showHideCreatingSectionSuccess = () => ({
  type: SHOW_HIDE_CREATING_SECTION_SUCCESS,
});

export const changeCreatingStep = (step) => ({
  type: CHANGE_STEP_CREATING,
  payload: step,
});

export const changeCreatingStepSuccess = (payload) => ({
  type: CHANGE_STEP_CREATING_SUCCESS,
  payload,
});

export const changevalidatingField = (step) => ({
  type: CHANGE_VALIDATINFIELD_STATE,
  payload: step,
});

export const changevalidatingFieldSuccess = (payload) => ({
  type: CHANGE_VALIDATINFIELD_STATE_SUCCESS,
  payload,
});

export const changeDataLocalPost = (step) => ({
  type: CHANGE_DATA_LOCAL_POST,
  payload: step,
});

export const changeDataLocalPostSuccess = (payload) => ({
  type: CHANGE_DATA_LOCAL_POST_SUCCESS,
  payload,
});

export const changeDataLocalTextInAutoComplete = (payload) => ({
  type: CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE,
  payload,
});

export const changeDataLocalTextInAutoCompleteSuccess = (payload) => ({
  type: CHANGE_DATA_LOCAL_TEXT_IN_AUTOCOMPLETE_SUCCESS,
  payload,
});

export const changeDataLocalFilter = (payload) => ({
  type: CHANGE_DATA_LOCAL_FILTER,
  payload,
});

export const changeDataLocalFilterSuccess = (payload) => ({
  type: CHANGE_DATA_LOCAL_FILTER_SUCCESS,
  payload,
});

export const waitingFromServer = (payload) => ({
  type: WAITING_FROM_SERVER,
  payload,
});

export const closeCreatingModal = (payload) => ({
  type: CLOSE_CREATED_MODAL,
  payload,
});

export const closeCreatingModalSuccess = (payload) => ({
  type: CLOSE_CREATED_MODAL_SUCCESS,
  payload,
});

export const getVotingList = (payload) => ({
  type: GET_VOTING_LIST,
  payload,
});

export const getVotingListSuccess = (payload) => ({
  type: GET_VOTING_LIST_SUCCESS,
  payload,
});

export const getZone = (payload) => ({
  type: GET_ZONE,
  payload,
});

export const getZoneSuccess = (payload) => ({
  type: GET_ZONE_SUCCESS,
  payload,
});

export const getMacroZones = (payload) => ({
  type: GET_MACRO_ZONES,
  payload,
});

export const getMacroZonesSuccess = (payload) => ({
  type: GET_MACRO_ZONES_SUCCESS,
  payload,
});

export const getTotalPost = (payload) => ({
  type: GET_TOTAL_POST,
  payload,
});

export const getTotalPostSuccess = (payload) => ({
  type: GET_TOTAL_POST_SUCCESS,
  payload,
});

export const getPostByID = (payload) => ({
  type: GET_POST_BY_ID,
  payload,
});

export const getPostByIDSuccess = (payload) => ({
  type: GET_POST_BY_ID_SUCCESS,
  payload,
});

export const creatingVotingProposal = (postToCreateRaw) => {
  const postToUpload = {
    title:
      postToCreateRaw?.title ||
      `Borrador creado el ${moment().format("MMMM Do YYYY, h:mm:ss a")}`,
    content: JSON.stringify(postToCreateRaw?.content || ""),
    startDate: moment(postToCreateRaw?.startDate).format("YYYY-MM-DD HH:hh"),
    dueDate: moment(postToCreateRaw?.dueDate).format("YYYY-MM-DD HH:hh"),
    question: postToCreateRaw?.question || "",
    tags: postToCreateRaw?.tags || [],
    completed: postToCreateRaw?.completed,
    // zonesId:[],
    // macrosId: [],
    // options: [],
  };
  const options = [];
  const zonesId = [];
  const macrosId = [];
  const files = {
    main: [],
    options: [],
  };

  // banner
  if (postToCreateRaw?.banner?.originFileObj) {
    files?.main?.push({
      tag: "banner",
      file: postToCreateRaw?.banner?.originFileObj,
    });
  }

  // Attached step 2
  if (postToCreateRaw?.extraFile?.originFileObj) {
    files?.main?.push({
      tag: "attached",
      file: postToCreateRaw?.extraFile?.originFileObj,
    });
  }

  postToCreateRaw?.proposals?.forEach((proposal, i) => {
    options.push({
      id: proposal?.id,
      title: proposal?.title,
      content: JSON.stringify(proposal?.content),
    });
    // Image of the options
    if (proposal?.image?.originFileObj) {
      files?.options?.push({
        tag: "banner",
        file: proposal?.image?.originFileObj,
        indexProposal: i,
      });
    }
    // Attached
    if (proposal?.file?.originFileObj) {
      files?.options?.push({
        tag: "attached",
        file: proposal?.file?.originFileObj,
        indexProposal: i,
      });
    }
  });

  if (options.length > 0) {
    postToUpload.options = options;
  }

  const macroSelectedAll = postToCreateRaw?.zonesId?.filter(
    (x) => x.selectedAll === true
  )?.length;
  const totalmacro = postToCreateRaw?.zonesId?.length;
  const sendMacrosId = macroSelectedAll === totalmacro;

  postToCreateRaw?.zonesId?.forEach((zone) => {
    if (sendMacrosId) {
      macrosId.push(zone?.id);
    } else {
      zone?.selected?.forEach((subZone) => {
        zonesId.push(subZone?.id);
      });
      // if (zone.selectedAll) {
      //   macrosId.push(zone?.id);
      // } else {
      //   zone?.selected?.forEach((subZone) => {
      //     zonesId.push(subZone?.id);
      //   });
      // }
    }
  });

  if (zonesId.length > 0) {
    postToUpload.zonesId = zonesId;
  } else {
    postToUpload.zonesId = undefined;
  }

  if (zonesId.length === 0 && macrosId.length > 0) {
    postToUpload.macrosId = macrosId;
  } else {
    postToUpload.macrosId = undefined;
  }

  return {
    type: CREATE_VOTING_PROPOSAL,
    payload: {
      postToUpload,
      files,
      id: postToCreateRaw?.id,
      postsId: postToCreateRaw?.postsId,
    },
  };
};

export const creatingVotingProposalSuccess = (payload) => ({
  type: CREATE_VOTING_PROPOSAL_SUCCESS,
  payload,
});

export const uploadingProposalAttached = (payload) => ({
  type: UPLOAD_VOTING_PROPOSAL_ATTACHED,
  payload,
});

export const uploadingProposalAttachedSuccess = (payload) => ({
  type: UPLOAD_VOTING_PROPOSAL_ATTACHED_SUCCESS,
  payload,
});

export const deleteProposal = (payload) => ({
  type: DELETE_VOTING_PROPOSAL,
  payload,
});

export const deleteProposalSuccess = (payload) => ({
  type: DELETE_VOTING_PROPOSAL_SUCCESS,
  payload,
});

export const openDeleteModal = (payload) => ({
  type: OPEN_DELETE_MODAL,
  payload,
});

export const openDeleteModalSuccess = (payload) => ({
  type: OPEN_DELETE_MODAL_SUCCESS,
  payload,
});

export const closeDeleteModal = (payload) => ({
  type: CLOSE_DELETE_MODAL,
  payload,
});

export const closeDeleteModalSuccess = (payload) => ({
  type: CLOSE_DELETE_MODAL_SUCCESS,
  payload,
});

export const openErrorModal = (payload) => ({
  type: OPEN_ERROR_MODAL,
  payload,
});

export const openErrorModalSuccess = (payload) => ({
  type: OPEN_ERROR_MODAL_SUCCESS,
  payload,
});

export const closeErrorModal = (payload) => ({
  type: CLOSE_ERROR_MODAL,
  payload,
});

export const closeErrorModalSuccess = (payload) => ({
  type: CLOSE_ERROR_MODAL_SUCCESS,
  payload,
});

export const editPost = (payload) => ({
  type: EDIT_POST,
  payload,
});

export const editPostSuccess = (payload) => ({
  type: EDIT_POST_SUCCESS,
  payload,
});

export const deleteFile = (payload) => ({
  type: DELETE_VOTING_FILE,
  payload,
});

export const deleteFileSuccess = (payload) => ({
  type: DELETE_VOTING_FILE_SUCCESS,
  payload,
});

export const addproposalinServer = ({ vootingId, proposalRaw, id }) => {
  const proposalOrdered = {
    title: proposalRaw.title,
    content: JSON.stringify(proposalRaw?.content),
    voteId: vootingId,
  };
  const files = [];

  // Image of the options
  if (proposalRaw?.image?.originFileObj) {
    files?.push({
      tag: "banner",
      file: proposalRaw?.image?.originFileObj,
    });
  }
  // Attached
  if (proposalRaw?.file?.originFileObj) {
    files?.push({
      tag: "attached",
      file: proposalRaw?.file?.originFileObj,
    });
  }

  return {
    type: ADD_PROPOSAL_IN_SERVER,
    payload: {
      vootingId,
      proposalOrdered,
      files,
      id,
    },
  };
};

export const addproposalinServerSuccess = (payload) => ({
  type: ADD_PROPOSAL_IN_SERVER_SUCCESS,
  payload,
});

export const editproposalinServer = (payload) => ({
  type: EDIT_PROPOSAL_IN_SERVER,
  payload,
});

export const editproposalinServerSuccess = (payload) => ({
  type: EDIT_PROPOSAL_IN_SERVER_SUCCESS,
  payload,
});

export const deleteproposalinServer = (payload) => ({
  type: DELETE_PROPOSAL_IN_SERVER,
  payload,
});

export const deleteproposalinServerSuccess = (payload) => ({
  type: DELETE_PROPOSAL_IN_SERVER_SUCCESS,
  payload,
});
