import { put, takeLatest, takeEvery } from "redux-saga/effects";
import { AdminDiscussionServices } from "@services/adminDiscussion";
import { NormativesServicess } from "@services/normatives";
import { AdminVotingServices } from "@services/adminVoting";
import { hideLoader, showLoader } from "@redux/common/actions";
import { reloadNormativeCards } from "../normatives/actions";
import message from "@components/Message";
import moment from "moment";
import { ResultPostChapter, updateChapters } from "./auxiliars";
import {
  CHANGE_STEP_CREATING,
  DELETE_NORMATIVE_FILE,
  CREATE_NORMATIVE,
  EDIT_POST_NORMATIVE,
  DELETE_POST_NORMATIVE,
  POST_NORMATIVE_CHAPTER,
  SET_NORMATIVE_DATA,
  DELETE_ARTICLE,
} from "./constants";

import {
  changeCreatingStepSuccess,
  deleteFileSuccess,
  createdNormativeSuccess,
  loadingNormative,
  editPostSuccess,
  emptyLocalPost,
  postNormativeChapterSuccess,
  setNormativeDataSuccess,
  deleteArticleSuccess,
} from "./actions";

export function* changinStepCreating({ payload }) {
  try {
    yield put(changeCreatingStepSuccess(payload));
  } catch (error) {
    console.log("Error en saga");
    console.log(error);
  }
}

export function* delteFileSaga({ payload }) {
  try {
    const { tag, id, PostionFieldName, value } = payload;
    console.log("Borra el archivo", tag, id, PostionFieldName, value);
    yield put(loadingNormative(true));
    const services = new AdminDiscussionServices();
    yield services.deleteFile({ id, tag });
    yield put(deleteFileSuccess({ name: PostionFieldName, value }));
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  } finally {
    yield put(loadingNormative(false));
  }
}

export function* createNormative({ payload }) {
  try {
    yield put(loadingNormative(true));
    const ns = new NormativesServicess();
    const vs = new AdminVotingServices();
    console.warn(" *** Previous post sagas normative", payload);
    let resultPostNormative = "";
    if (payload?.id) {
      resultPostNormative = yield ns.updateNormative({
        body: payload?.postToUpload,
        id: payload?.id,
      });
      const ResultPostChaps = yield updateChapters(
        payload?.chapters,
        payload?.id
      );
      console.warn("ResultPostChaps", ResultPostChaps);
    } else {
      resultPostNormative = yield ns.postNormative(payload?.postToUpload);
      const ResultPostChaps = payload?.chapters
        ? yield ResultPostChapter(payload?.chapters, resultPostNormative.id)
        : null;
      console.warn("ResultPostChaps", ResultPostChaps);
    }
    // Files of the main Post
    for (let index = 0; index < payload?.files?.main?.length; index++) {
      try {
        const element = payload?.files?.main[index];
        const resultPostFileNormative = yield vs.createPostUploadFiles({
          file: element?.file,
          tag: element?.tag,
          container: "posts",
          id: resultPostNormative?.postsId || payload?.postsId?.value,
        });
        console.warn("posting files", { resultPostFileNormative });
      } catch (error) {}
    }

    yield put(createdNormativeSuccess());
  } catch (error) {
    console.log(error);
    message.error(error);
  } finally {
    yield put(loadingNormative(false));
  }
}

export function* editPostSaga({ payload }) {
  const getChapters = (chapters) => {
    return chapters.map((chapter) => ({
      ...chapter,
      title: chapter.articles ? chapter.articles[0].title : "",
    }));
  };

  try {
    yield put(emptyLocalPost());
    yield put(loadingNormative(true));
    const ns = new NormativesServicess();
    const result = yield ns.getPostById(payload.id);
    console.warn("Normative to edit: ", result);

    const _localPost = {
      ...result,
      id: payload.id,
      fileLink: {
        value: result?.fileLink,
        error: false,
      },
      dueDate: {
        value: result?.dueDate,
        error: moment() > moment(result?.dueDate),
      },
      tags: {
        value: result?.tags,
        error: result?.tags?.length === 0,
      },
      title: {
        value: result?.title,
        error: result?.title === "",
      },
      chapters: {
        value: getChapters(result?.chapters),
        error: result?.title === "",
      },
      postsId: { value: result?.postsId, error: false },
    };

    try {
      if (result.attached) {
        _localPost.attached = {
          value: result?.attached,
          error: false,
        };
      }

      const contentParsed = JSON?.parse(result?.content);
      _localPost.content = {
        value: contentParsed,
        error:
          !contentParsed ||
          contentParsed === "" ||
          result?.content === undefined,
      };
    } catch (error) {
      _localPost.content = result?.content;
    }

    console.log("editPostSuccess", { _localPost, result });
    yield put(editPostSuccess(_localPost));
  } catch (error) {
    console.log("Error intentando editar: ", error);
  } finally {
    yield put(loadingNormative(false));
  }
}

export function* deletePostSaga({ payload }) {
  console.warn("payload ID", payload);
  try {
    yield put(showLoader());
    const ns = new NormativesServicess();
    yield ns.deleteNomative(payload.id);
    yield put(reloadNormativeCards());
    yield put(hideLoader());
  } catch (error) {
    yield put(hideLoader());
    console.log(error);
  }
}

export function* postNormativeChapter({ payload }) {
  try {
    yield put(loadingNormative(true));
    const normativesServices = new NormativesServicess();
    console.log("postNormativeChapter payload.data", payload.data);
    console.log(
      "postNormativeChapter payload.currentState",
      payload.currentState
    );
    const response = yield normativesServices.createNormativeChapter({
      data: { ...payload.data },
    });
    console.log("postNormativeChapter response:", response);
    const index = payload.currentState.chapters.findIndex(
      (chapter) => chapter.cod === payload.data.cod
    );
    payload.currentState.chapters[index] = {
      ...payload.currentState.chapters[index],
      id: response.id,
      articles: payload.currentState.chapters[index].articles.map(
        (article, i) => ({ ...article, id: response.articles[i].id })
      ),
    };
    yield put(
      postNormativeChapterSuccess({
        editing: {
          ...payload.currentState,
          normativeId: response.normativeId,
          status: response.states,
        },
      })
    );
  } catch (error) {
    console.log("Error en open delete modal");
    console.log(error);
  } finally {
    yield put(loadingNormative(false));
  }
}

export function* setNormativeData({ payload }) {
  yield put(
    setNormativeDataSuccess({
      ...payload.data,
    })
  );
}

export function* deleteArticle({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.deleteArticle({ id: payload.id });
  console.log(response);
  yield put(deleteArticleSuccess({ editing: { ...payload.data } }));
}

export default function* rootSaga() {
  yield takeLatest(CHANGE_STEP_CREATING, changinStepCreating);
  yield takeEvery(DELETE_NORMATIVE_FILE, delteFileSaga);
  yield takeLatest(CREATE_NORMATIVE, createNormative);
  yield takeLatest(EDIT_POST_NORMATIVE, editPostSaga);
  yield takeLatest(DELETE_POST_NORMATIVE, deletePostSaga);
  yield takeLatest(POST_NORMATIVE_CHAPTER, postNormativeChapter);
  yield takeLatest(SET_NORMATIVE_DATA, setNormativeData);
  yield takeLatest(DELETE_ARTICLE, deleteArticle);
}
