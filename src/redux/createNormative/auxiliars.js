import { AdminVotingServices } from "@services/adminVoting";
import { NormativesServicess } from "@services/normatives";

const setMessages = (fieldType) => {
  switch (fieldType) {
    case "title":
      return "Título no válido";
    case "dueDate":
      return "Fecha no válida para la culminación de la normativa";
    case "content":
      return "Explicación de la normativa, no válida";
    case "tags":
      return "No se ha definido etiquetas para esta publicación";
    case "chapters":
      return "Se debe añadir al menos un capítulo y artículo";
    default:
      return "";
  }
};

export const getErrorMessages = (localPost) => {
  const messages = [];
  for (const field in localPost) {
    if (localPost[field].error) {
      messages.push(setMessages(field));
    }
  }
  return messages;
};

export const postFiles = (files, id) => {
  const vs = new AdminVotingServices();
  const promises = [];
  for (let index = 0; index < files.length; index++) {
    try {
      const element = files[index];
      promises.push(
        vs.createPostUploadFiles({
          file: element?.file,
          tag: element?.tag,
          container: "posts",
          id,
        })
      );
    } catch (e) {
      return new Error(e.message);
    }
  }

  return Promise.all(promises);
};

export const updateChapters = async (chaptersIn, normativeId) => {
  const ns = new NormativesServicess();
  try {
    // deleting chapters
    const normative = await ns.getNormative({ id: normativeId });
    const idsChapterIn = chaptersIn.map((chapter) => chapter.id);
    await Promise.all(
      normative.chapters
        .filter((chapter) => !idsChapterIn.includes(chapter.id))
        .map((chapter) => deleteChapter(chapter.id))
    );

    // updating chapters
    const chapterToUpdate = chaptersIn.filter(
      (chapterIn) => chapterIn.normativeId !== undefined
    );
    await Promise.all(
      chapterToUpdate.map((chapterIn) =>
        updateArticlesChapter(chapterIn, normative.chapters)
      )
    );

    // inserting new chapters
    const newChapters = chaptersIn.filter(
      (chapterIn) => chapterIn.normativeId === undefined
    );
    await ResultPostChapter(newChapters, normativeId);

    return true;
  } catch (e) {
    return new Error(e.message);
  }
};

export const ResultPostChapter = (chapters, normativeId) => {
  const service = new NormativesServicess();

  try {
    const chaptersAdjusted = chapters.map(({ articles, cod }) => ({
      cod,
      normativeId,
      articles,
    }));
    console.warn("chaptersAdjusted in ResultPostChapter", chaptersAdjusted);

    return Promise.all(chaptersAdjusted.map((ca) => service.postChapter(ca)));
  } catch (e) {
    return new Error(e.message);
  }
};

export const updateArticlesChapter = (chapter, cloudChapters) => {
  const service = new NormativesServicess();
  const { articles, id, cod, title } = chapter;

  service.updateChapter({ id, body: { cod } });

  try {
    const promisesToUpdate = articles
      .filter((article) => article.id)
      .map((article) =>
        service.updateAticle({
          id: article.id,
          body: {
            content: article.content,
          },
        })
      );

    const promisesToCreate = articles
      .filter((article) => !article.id)
      .map((article) =>
        service.createArticle({
          body: {
            content: article.content,
            status: true,
            chapterId: id,
            cod,
            title,
          },
        })
      );

    // deleting articles
    const similarChapterInCloud = cloudChapters.find((ch) => ch.id === id);
    const articlesIds = articles.map((article) => article.id);
    const articlesToDelete = similarChapterInCloud.articles.filter(
      (articleInCloud) => !articlesIds.includes(articleInCloud.id)
    );
    console.warn("similarChapterInCloud", articlesToDelete, id);
    const promisesToDelete = articlesToDelete.map((article) =>
      service.deleteArticle(article.id)
    );

    return [...promisesToUpdate, ...promisesToCreate, ...promisesToDelete];
  } catch (e) {
    return new Error(e.message);
  }
};

export const deleteChapter = (id) => {
  try {
    const service = new NormativesServicess();
    service.deleteChapter(id);
  } catch (e) {
    return new Error(e.message);
  }
};
