import moment from "moment";
import { getErrorMessages } from "./auxiliars";
import {
  CHANGE_NORMATIVE_LOCAL_POST,
  CHANGE_NORMATIVE_LOCAL_POST_SUCCESS,
  CHANGE_STEP_CREATING,
  CHANGE_STEP_CREATING_SUCCESS,
  DELETE_NORMATIVE_FILE_SUCCESS,
  DELETE_NORMATIVE_FILE,
  WAITING_FROM_SERVER,
  CREATE_NORMATIVE,
  CREATE_NORMATIVE_SUCCESS,
  LOAD_NORMATIVE,
  EDIT_POST_NORMATIVE,
  EDIT_POST_NORMATIVE_SUCCESS,
  DELETE_POST_NORMATIVE,
  EMPTY_LOCALPOST,
  SET_MESSAGES_ERROR,
  MODAL_SAVE_ON_EXIT,
  SET_NORMATIVE_DATA,
  SET_NORMATIVE_DATA_SUCCESS,
  POST_NORMATIVE_CHAPTER,
  POST_NORMATIVE_CHAPTER_SUCCESS,
  UPDATE_NORMATIVE_CHAPTER,
  UPDATE_NORMATIVE_CHAPTER_SUCCESS,
  DELETE_CHAPTER,
  DELETE_CHAPTER_SUCCESS,
  DELETE_ARTICLE,
  DELETE_ARTICLE_SUCCESS,
  FLUSH_DATA,
  FLUSH_DATA_SUCCESS,
} from "./constants";

export const setNormativeData = (payload) => ({
  type: SET_NORMATIVE_DATA,
  payload,
});

export const setNormativeDataSuccess = (payload) => ({
  type: SET_NORMATIVE_DATA_SUCCESS,
  payload,
});

export const postNormativeChapter = (payload) => ({
  type: POST_NORMATIVE_CHAPTER,
  payload,
});

export const postNormativeChapterSuccess = (payload) => ({
  type: POST_NORMATIVE_CHAPTER_SUCCESS,
  payload,
});

export const updateNormativeChapter = (payload) => ({
  type: UPDATE_NORMATIVE_CHAPTER,
  payload,
});

export const updateNormativeChapterSuccess = (payload) => ({
  type: UPDATE_NORMATIVE_CHAPTER_SUCCESS,
  payload,
});

export const deleteChapter = (payload) => ({
  type: DELETE_CHAPTER,
  payload,
});

export const deleteChapterSuccess = (payload) => ({
  type: DELETE_CHAPTER_SUCCESS,
  payload,
});

export const deleteArticle = (payload) => ({
  type: DELETE_ARTICLE,
  payload,
});

export const deleteArticleSuccess = (payload) => ({
  type: DELETE_ARTICLE_SUCCESS,
  payload,
});

export const flushCreatingData = () => ({
  type: FLUSH_DATA,
  payload: "",
});

export const flushCreatingDataSuccess = (payload) => ({
  type: FLUSH_DATA_SUCCESS,
  payload,
});

// old ======

export const emptyLocalPost = (payload) => ({
  type: EMPTY_LOCALPOST,
  payload: payload,
});

export const modalSaveOnExit = (payload) => ({
  type: MODAL_SAVE_ON_EXIT,
  payload: payload,
});

export const changeDataLocalPost = (step) => ({
  type: CHANGE_NORMATIVE_LOCAL_POST,
  payload: step,
});

export const changeDataLocalPostSuccess = (payload) => ({
  type: CHANGE_NORMATIVE_LOCAL_POST_SUCCESS,
  payload,
});

export const changeCreatingStep = (step) => ({
  type: CHANGE_STEP_CREATING,
  payload: step,
});

export const changeCreatingStepSuccess = (payload) => ({
  type: CHANGE_STEP_CREATING_SUCCESS,
  payload,
});

export const deleteFile = (payload) => ({
  type: DELETE_NORMATIVE_FILE,
  payload,
});

export const deleteFileSuccess = (payload) => ({
  type: DELETE_NORMATIVE_FILE_SUCCESS,
  payload,
});

export const waitingFromServer = (payload) => ({
  type: WAITING_FROM_SERVER,
  payload,
});
export const createdNormativeSuccess = (payload) => ({
  type: CREATE_NORMATIVE_SUCCESS,
  payload,
});
export const loadingNormative = (payload) => ({
  type: LOAD_NORMATIVE,
  payload,
});

export const editPost = (payload) => ({
  type: EDIT_POST_NORMATIVE,
  payload,
});

export const deleteNormative = (payload) => ({
  type: DELETE_POST_NORMATIVE,
  payload,
});

export const editPostSuccess = (payload) => ({
  type: EDIT_POST_NORMATIVE_SUCCESS,
  payload,
});

export const errorMessages = (messagesError) => ({
  type: SET_MESSAGES_ERROR,
  payload: messagesError,
});

export const creatingNormative = (dataToPost) => {
  let {
    title,
    content,
    tags,
    dueDate,
    extraFile,
    chapters,
    id,
    postsId,
    completed,
  } = dataToPost;
  const messagesError = getErrorMessages(dataToPost);

  if (completed) {
    if (messagesError.length > 0) {
      return {
        type: SET_MESSAGES_ERROR,
        payload: messagesError,
      };
    }
  } else {
    if (title.error) {
      return errorMessages("Debe ingresar un título");
    }
  }

  const postToUpload = {
    title: title?.value,
    content: content?.value ? JSON.stringify(content.value) : "",
    tags: tags?.value || [],
    state: completed ? "PUBLISHED" : "CREATING",
    dueDate: dueDate
      ? moment(dueDate.value).endOf("day").format("YYYY-MM-DD HH:mm")
      : moment().endOf("day").format("YYYY-MM-DD HH:mm"),
  };
  chapters = chapters?.value || null;
  console.warn("Chapters in action:", chapters);

  const files = {
    main: [],
    options: [],
  };

  // Attached step 2
  if (extraFile?.value?.originFileObj) {
    files.main.push({
      tag: "attached",
      file: extraFile?.value?.originFileObj,
    });
  }

  return {
    type: CREATE_NORMATIVE,
    payload: {
      postToUpload,
      chapters,
      files,
      id,
      postsId: postsId,
    },
  };
};
