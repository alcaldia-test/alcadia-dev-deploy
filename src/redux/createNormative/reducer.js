import { checkErrorFields } from "@utils/validateFields";
import {
  CHANGE_NORMATIVE_LOCAL_POST,
  CREATE_NORMATIVE_SUCCESS,
  LOAD_NORMATIVE,
  EDIT_POST_NORMATIVE_SUCCESS,
  DELETE_NORMATIVE_FILE_SUCCESS,
  EMPTY_LOCALPOST,
  SET_MESSAGES_ERROR,
  MODAL_SAVE_ON_EXIT,
  POST_NORMATIVE_CHAPTER_SUCCESS,
  UPDATE_NORMATIVE_CHAPTER_SUCCESS,
  SET_NORMATIVE_DATA_SUCCESS,
  DELETE_ARTICLE_SUCCESS,
  FLUSH_DATA_SUCCESS,
} from "./constants";

const initialState = {
  localPost: {},
  loading: false,
  showModalCreated: false,
  messagesError: [],
  saveOnExit: false,

  editing: {
    normativeId: "",
    chapters: [],
    state: "CREATING",
  },
};

function normatives(state = initialState, action) {
  switch (action.type) {
    case CHANGE_NORMATIVE_LOCAL_POST:
      // eslint-disable-next-line no-case-declarations
      const { name, value } = action.payload;
      return {
        ...state,
        localPost: {
          ...state.localPost,
          [name]: {
            value: value,
            error: checkErrorFields(name, value),
          },
        },
      };
    case CREATE_NORMATIVE_SUCCESS:
      return {
        ...state,
        showModalCreated: true,
      };
    case EDIT_POST_NORMATIVE_SUCCESS:
      return {
        ...state,
        localPost: action.payload,
        loading: false,
      };
    case LOAD_NORMATIVE:
      return {
        ...state,
        loading: action.payload,
      };
    case DELETE_NORMATIVE_FILE_SUCCESS:
      delete state.localPost[action.payload.name];
      return {
        ...state,
      };
    case SET_MESSAGES_ERROR:
      return {
        ...state,
        messagesError: action.payload,
      };
    case MODAL_SAVE_ON_EXIT:
      return {
        ...state,
        saveOnExit: action.payload,
      };
    case POST_NORMATIVE_CHAPTER_SUCCESS:
      return { ...state, ...action.payload };
    case UPDATE_NORMATIVE_CHAPTER_SUCCESS:
      return { ...state, ...action.payload };
    case SET_NORMATIVE_DATA_SUCCESS:
      return { ...state, ...action.payload };
    case DELETE_ARTICLE_SUCCESS:
      return { ...state };
    case FLUSH_DATA_SUCCESS:
      return { ...action.payload };
    case EMPTY_LOCALPOST:
      return { ...initialState };

    default:
      return { ...state };
  }
}

export default normatives;
