import { call, takeLatest, put, select } from "redux-saga/effects";
import request, {
  deleteOptions,
  getOptions,
  getOptionsWithoutToken,
  patchOptions,
} from "@utils/request";

import {
  DELETE_ADMIN_ROLES_START,
  DELETE_REQUEST_ADMIN_ROLES_START,
  INITIAL_REQUEST_ADMIN_REQUEST_START,
  INITIAL_REQUEST_ADMIN_ROLES_START,
  UPDATE_ADMIN_ROLES_START,
  UPDATE_REQUEST_ADMIN_ROLES_START,
} from "./constants";
import {
  deleteRequestAdminRolesSuccess,
  initialRequestAdminRequestStart,
  initialRequestAdminRequestSuccess,
  initialRequestAdminRolesStart,
  initialRequestAdminRolesSuccess,
} from "./actions";
import { hideLoader, showLoader } from "@redux/common/actions";
import message from "@components/Message";

export function* initialAdminRequestSaga() {
  let url, options;
  try {
    yield put(showLoader());
    url = `${process.env.NEXT_PUBLIC_URL_API}/access-controls`;
    options = yield getOptions();
    const requestAdmin = yield call(request, url, options);
    url = `${process.env.NEXT_PUBLIC_URL_API}/permissions/old`;
    options = yield getOptionsWithoutToken();
    const requestPermissions = yield call(request, url, options);
    requestPermissions.forEach((e) => {
      e.value = false;
    });
    requestAdmin.forEach((e, i) => {
      e.roles = requestPermissions;
      e.key = i;
      e.name = e?.user?.firstName;
      e.email = e?.user?.email;
      e.createdRun = e.createdAt;
      e.createdAt = e?.createdAt?.substring(0, 10);
      e.updatedAt = e?.updatedAt?.substring(0, 10);
    });
    yield put(hideLoader());
    requestAdmin.reverse();
    yield put(initialRequestAdminRequestSuccess(requestAdmin));
  } catch (err) {
    yield put(hideLoader());
    yield message.error("No se pudo traer los datos");
  }
}
export function* initialAdminRolesSaga({ payload }) {
  let url, options;
  try {
    yield put(showLoader());
    url = `${process.env.NEXT_PUBLIC_URL_API}/access-controls?filter=%7B%0A%20%20%22state%22%3A%20%22APPROVED%22%0A%7D`;
    options = yield getOptions();
    let requestAdmin = yield call(request, url, options);
    url = `${process.env.NEXT_PUBLIC_URL_API}/permissions/old`;
    options = yield getOptionsWithoutToken();
    const requestPermissions = yield call(request, url, options);
    requestPermissions.forEach((e) => {
      e.value = false;
    });
    requestAdmin.forEach((e, i) => {
      e.roles = requestPermissions;
      e.key = i;
      e.name = e?.user?.firstName;
      e.email = e?.user?.email;
      e.createdRun = e.createdAt;
      e.createdAt = e?.createdAt?.substring(0, 10);
      e.updatedAt = e?.updatedAt?.substring(0, 10);
    });
    requestAdmin = requestAdmin.filter((e) => e.email !== "master@gmail.com");
    requestAdmin.reverse();
    yield put(initialRequestAdminRolesSuccess(requestAdmin));
    yield put(hideLoader());
  } catch (err) {
    yield put(hideLoader());
    yield message.error("No se pudo traer los datos");
  }
}

export function* deleteAdminRolesSaga({
  payload: { resolve, reject, id, requestSend },
}) {
  const { adminRequest } = yield select((state) => state.accessRequest);
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/access-controls/${id}/assign-permissions`;
    const options = yield patchOptions(requestSend);
    // eslint-disable-next-line no-unused-vars
    const requestAdmin = yield call(request, url, options);
    yield put(
      deleteRequestAdminRolesSuccess(adminRequest.filter((e) => e.id !== id))
    );
    yield put(hideLoader());
    yield call(resolve, "Se rechazó la solicitud con éxito");
  } catch (err) {
    yield call(reject, "Al parecer,hubo un error al rechazar esta solicitud");
    yield put(hideLoader());
  }
}

export function* updateAdminRolesSaga({
  payload: { resolve, reject, id, requestSend },
}) {
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/access-controls/${id}/assign-permissions`;
    const options = yield patchOptions(requestSend);
    // eslint-disable-next-line no-unused-vars
    const requestAdmin = yield call(request, url, options);
    yield put(initialRequestAdminRequestStart());
    yield put(hideLoader());
    yield call(resolve, "Se aprobó con éxito la solicitud");
  } catch (err) {
    yield call(reject, "No se pudo aprobar la solicitud");
    yield put(hideLoader());
  }
}

export function* updateAdminRoles({
  payload: { resolve, reject, id, requestSend },
}) {
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/access-controls/${id}/assign-permissions`;
    const options = yield patchOptions(requestSend);
    // eslint-disable-next-line no-unused-vars
    const requestAdmin = yield call(request, url, options);
    yield put(initialRequestAdminRolesStart());
    yield put(hideLoader());
    yield call(resolve, "Se actualizó con éxito los roles");
  } catch (err) {
    yield call(reject, "No se pudo actualizar los roles");
    yield put(hideLoader());
  }
}
export function* deleteAdminRoles({ payload: { resolve, reject, id } }) {
  const { adminSuccess } = yield select((state) => state.accessRequest);
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/access-controls/${id}`;
    const options = yield deleteOptions();
    // eslint-disable-next-line no-unused-vars
    const requestAdmin = yield call(request, url, options);
    yield put(
      initialRequestAdminRolesStart(adminSuccess.filter((e) => e.id !== id))
    );
    yield put(hideLoader());
    yield call(resolve, "Se eliminó con éxito");
  } catch (err) {
    yield call(reject, "No se pudo eliminar");
    yield put(hideLoader());
  }
}
export default function* rootSaga() {
  yield takeLatest(
    INITIAL_REQUEST_ADMIN_REQUEST_START,
    initialAdminRequestSaga
  );
  yield takeLatest(INITIAL_REQUEST_ADMIN_ROLES_START, initialAdminRolesSaga);
  yield takeLatest(DELETE_REQUEST_ADMIN_ROLES_START, deleteAdminRolesSaga);
  yield takeLatest(UPDATE_REQUEST_ADMIN_ROLES_START, updateAdminRolesSaga);
  yield takeLatest(UPDATE_ADMIN_ROLES_START, updateAdminRoles);
  yield takeLatest(DELETE_ADMIN_ROLES_START, deleteAdminRoles);
}
