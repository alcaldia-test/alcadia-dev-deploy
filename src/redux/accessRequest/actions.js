import {
  DELETE_REQUEST_ADMIN_ROLES_START,
  DELETE_REQUEST_ADMIN_ROLES_SUCCESS,
  INITIAL_REQUEST_ADMIN_REQUEST_START,
  INITIAL_REQUEST_ADMIN_REQUEST_SUCCESS,
  INITIAL_REQUEST_ADMIN_ROLES_START,
  INITIAL_REQUEST_ADMIN_ROLES_SUCCESS,
  UPDATE_REQUEST_ADMIN_ROLES_START,
  UPDATE_REQUEST_ADMIN_ROLES_SUCCESS,
  UPDATE_ADMIN_ROLES_START,
  UPDATE_ADMIN_ROLES_SUCCESS,
  DELETE_ADMIN_ROLES_START,
  DELETE_ADMIN_ROLES_SUCCESS,
  ORDER_ADMIN_REQUEST_START,
  ORDER_ADMIN_ROLES_START,
} from "./constants";

export const initialRequestAdminRequestStart = (payload) => ({
  type: INITIAL_REQUEST_ADMIN_REQUEST_START,
  payload,
});

export const initialRequestAdminRequestSuccess = (payload) => ({
  type: INITIAL_REQUEST_ADMIN_REQUEST_SUCCESS,
  payload,
});

export const initialRequestAdminRolesStart = (payload) => ({
  type: INITIAL_REQUEST_ADMIN_ROLES_START,
  payload,
});

export const initialRequestAdminRolesSuccess = (payload) => ({
  type: INITIAL_REQUEST_ADMIN_ROLES_SUCCESS,
  payload,
});

export const updateRequestAdminRolesStart = (payload) => ({
  type: UPDATE_REQUEST_ADMIN_ROLES_START,
  payload,
});

export const updateRequestAdminRolesSuccess = (payload) => ({
  type: UPDATE_REQUEST_ADMIN_ROLES_SUCCESS,
  payload,
});

export const deleteRequestAdminRolesStart = (payload) => ({
  type: DELETE_REQUEST_ADMIN_ROLES_START,
  payload,
});

export const deleteRequestAdminRolesSuccess = (payload) => ({
  type: DELETE_REQUEST_ADMIN_ROLES_SUCCESS,
  payload,
});

export const updateAdminRolesStart = (payload) => ({
  type: UPDATE_ADMIN_ROLES_START,
  payload,
});

export const updateAdminRolesSuccess = (payload) => ({
  type: UPDATE_ADMIN_ROLES_SUCCESS,
  payload,
});

export const deleteAdminRolesStart = (payload) => ({
  type: DELETE_ADMIN_ROLES_START,
  payload,
});

export const deleteAdminRolesSuccess = (payload) => ({
  type: DELETE_ADMIN_ROLES_SUCCESS,
  payload,
});

export const orderAdminRequestStart = (payload) => ({
  type: ORDER_ADMIN_REQUEST_START,
  payload,
});
export const orderAdminRolesStart = (payload) => ({
  type: ORDER_ADMIN_ROLES_START,
  payload,
});
