import {
  DELETE_ADMIN_ROLES_SUCCESS,
  DELETE_REQUEST_ADMIN_ROLES_SUCCESS,
  INITIAL_REQUEST_ADMIN_REQUEST_SUCCESS,
  INITIAL_REQUEST_ADMIN_ROLES_SUCCESS,
  ORDER_ADMIN_REQUEST_START,
  ORDER_ADMIN_ROLES_START,
  UPDATE_ADMIN_ROLES_SUCCESS,
  UPDATE_REQUEST_ADMIN_ROLES_SUCCESS,
} from "./constants";

const initialState = {
  adminRequest: [],
  adminSuccess: [],
};

export function accessRequest(state = initialState, action) {
  switch (action.type) {
    case INITIAL_REQUEST_ADMIN_REQUEST_SUCCESS:
      return {
        ...state,
        adminRequest: action.payload,
      };
    case INITIAL_REQUEST_ADMIN_ROLES_SUCCESS:
      return {
        ...state,
        adminSuccess: action.payload,
      };
    case UPDATE_ADMIN_ROLES_SUCCESS:
      return {
        ...state,
        adminSuccess: action.payload,
      };
    case DELETE_ADMIN_ROLES_SUCCESS:
      return {
        ...state,
        adminSuccess: action.payload,
      };
    case UPDATE_REQUEST_ADMIN_ROLES_SUCCESS:
      return {
        ...state,
        adminRequest: action.payload,
      };
    case DELETE_REQUEST_ADMIN_ROLES_SUCCESS:
      return {
        ...state,
        adminRequest: action.payload,
      };
    case ORDER_ADMIN_ROLES_START:
      return {
        ...state,
        adminSuccess: action.payload,
      };
    case ORDER_ADMIN_REQUEST_START:
      return {
        ...state,
        adminRequest: action.payload,
      };
    default:
      return state;
  }
}

export default accessRequest;
