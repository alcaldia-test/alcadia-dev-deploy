import { VOTE_NORMATIVE_SUCCESS } from "./constans";

const initialState = {
  selected: false,
};

export function voteNormatives(state = initialState, action) {
  switch (action.type) {
    case VOTE_NORMATIVE_SUCCESS:
      return { ...action.payload };
    default:
      return { ...state };
  }
}

export default voteNormatives;
