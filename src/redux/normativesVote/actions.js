import { VOTE_NORMATIVE, VOTE_NORMATIVE_SUCCESS } from "./constans";

export const voteNormative = (payload) => ({
  type: VOTE_NORMATIVE,
  payload,
});

export const voteNormativeSuccess = (payload) => ({
  type: VOTE_NORMATIVE_SUCCESS,
  payload,
});
