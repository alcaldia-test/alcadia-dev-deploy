import { VOTE_NORMATIVE } from "./constans";
import { voteNormativeSuccess } from "./actions";
import { getNormativeDataSuccess } from "../chapters/actions";
import { NormativesServicess } from "@services/normatives";
import { put, takeLatest } from "@redux-saga/core/effects";

export function* voteNormative({ payload }) {
  console.log(payload);
  const normativesServices = new NormativesServicess();
  yield normativesServices.voteNormative({
    id: payload.id,
    vote: payload.vote,
    endpoint: payload.endpoint,
    userId: payload.userId,
  });
  if (payload.endpoint === "chapters") {
    const index = payload.currentState.chapters.findIndex(
      ({ id }) => id === payload.id
    );
    console.log(index);
    payload.currentState.chapters[index].reaction =
      payload.vote === "omit" ? "any" : payload.vote;
    yield put(getNormativeDataSuccess({ ...payload.currentState }));
    yield put(voteNormativeSuccess({ selected: payload.vote !== "omit" }));
    return;
  }
  payload.currentState.reaction =
    payload.vote === "omit" ? "any" : payload.vote;
  yield put(getNormativeDataSuccess({ ...payload.currentState }));
  yield put(voteNormativeSuccess({ selected: payload.vote !== "omit" }));
}

export default function* rootSaga() {
  yield takeLatest(VOTE_NORMATIVE, voteNormative);
}
