import {
  GET_NORMATIVE_DATA,
  GET_NORMATIVE_DATA_SUCCESS,
  GET_NORMATIVE_COMPLAINT,
  GET_NORMATIVE_COMPLAINT_SUCCESS,
  FLUSH_COMPLAINT,
  FLUSH_COMPLAINT_SUCCESS,
} from "./constants";

export const getNormativeData = (payload) => ({
  type: GET_NORMATIVE_DATA,
  payload,
});

export const getNormativeDataSuccess = (payload) => ({
  type: GET_NORMATIVE_DATA_SUCCESS,
  payload,
});

export const getNormativeComplaint = (payload) => ({
  type: GET_NORMATIVE_COMPLAINT,
  payload,
});

export const getNormativeComplaintSuccess = (payload) => ({
  type: GET_NORMATIVE_COMPLAINT_SUCCESS,
  payload,
});

export const flushComplaint = (payload) => ({
  type: FLUSH_COMPLAINT,
  payload,
});

export const flushComplaintSuccess = (payload) => ({
  type: FLUSH_COMPLAINT_SUCCESS,
  payload,
});
