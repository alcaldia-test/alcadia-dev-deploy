import {
  GET_NORMATIVE_DATA_SUCCESS,
  GET_NORMATIVE_COMPLAINT_SUCCESS,
  FLUSH_COMPLAINT_SUCCESS,
} from "./constants";

const initialState = {
  id: "",
  tags: [],
  createdAt: "",
  title: "",
  content: "",
  postId: "",
  posts: {
    comments: [],
    id: "",
  },
  attached: "",
  comments: 0,
  chapters: [],
  likes: 0,
  dislikes: 0,
  handlingComplaint: {
    id: "",
    type: "",
    complaintAmount: 0,
    currentComplaints: [],
  },
};

export function chapters(state = initialState, action) {
  switch (action.type) {
    case GET_NORMATIVE_DATA_SUCCESS:
      return { ...state, ...action.payload };
    case GET_NORMATIVE_COMPLAINT_SUCCESS:
      return { ...state, ...action.payload };
    case FLUSH_COMPLAINT_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return { ...state };
  }
}

export default chapters;
