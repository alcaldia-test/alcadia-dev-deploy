import message, { textError } from "@components/Message";
import {
  GET_NORMATIVE_DATA,
  GET_NORMATIVE_COMPLAINT,
  FLUSH_COMPLAINT,
} from "./constants";
import {
  getNormativeDataSuccess,
  getNormativeComplaintSuccess,
  flushComplaintSuccess,
} from "./actions";
import { put, takeLatest } from "@redux-saga/core/effects";
import { NormativesServicess } from "@services/normatives";

export function* getNormativeDataSaga({ payload }) {
  try {
    const normativesServices = new NormativesServicess();
    const response = yield normativesServices.getNormative({
      id: payload.id,
      userId: payload.userId,
    });
    yield put(getNormativeDataSuccess({ ...response }));
  } catch (error) {
    message.error(textError("Normativa", error.message));
  }
}

export function* getNormativeComplaint({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.getComplaints({
    id: payload.id,
    endpoint: payload.endpoint,
  });
  yield put(
    getNormativeComplaintSuccess({
      handlingComplaint: {
        currentComplaints: response,
        id: payload.id,
        type: payload.endpoint,
        complaintAmount: payload.complaintsAmount,
      },
    })
  );
}

export function* flushComplaint() {
  yield put(
    flushComplaintSuccess({
      handlingComplaint: {
        id: "",
        type: "",
        currentComplaints: [],
      },
    })
  );
}

export default function* rootSaga() {
  yield takeLatest(GET_NORMATIVE_DATA, getNormativeDataSaga);
  yield takeLatest(GET_NORMATIVE_COMPLAINT, getNormativeComplaint);
  yield takeLatest(FLUSH_COMPLAINT, flushComplaint);
}
