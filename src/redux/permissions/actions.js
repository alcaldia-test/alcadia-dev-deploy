import {
  INITIAL_REQUEST_PERMISSIONS_START,
  INITIAL_REQUEST_PERMISSIONS_SUCCESS,
  REGISTER_PERMISSIONS_ADMIN_START,
  REGISTER_PERMISSIONS_ADMIN_SUCCESS,
} from "./constants";

export const initialRequestPermissionsStart = (payload) => ({
  type: INITIAL_REQUEST_PERMISSIONS_START,
  payload,
});

export const initialRequestPermissionsSuccess = (payload) => ({
  type: INITIAL_REQUEST_PERMISSIONS_SUCCESS,
  payload,
});

export const registerPermissionsAdminStart = (payload) => ({
  type: REGISTER_PERMISSIONS_ADMIN_START,
  payload,
});

export const registerPermissionsAdminSuccess = (payload) => ({
  type: REGISTER_PERMISSIONS_ADMIN_SUCCESS,
  payload,
});
