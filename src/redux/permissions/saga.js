import { call, takeLatest, put } from "redux-saga/effects";
import request, {
  showMessageError,
  getOptionsWithoutToken,
  postOptions,
} from "@utils/request";

import {
  INITIAL_REQUEST_PERMISSIONS_START,
  REGISTER_PERMISSIONS_ADMIN_START,
} from "./constants";
import { initialRequestPermissionsSuccess } from "./actions";
import Router from "next/router";

export function* initialRequesPermissions() {
  try {
    const url = `${process.env.NEXT_PUBLIC_URL_API}/permissions/old`;
    const options = yield getOptionsWithoutToken();
    const requestPermissions = yield call(request, url, options);
    requestPermissions.forEach((e) => {
      e.value = false;
    });
    yield put(initialRequestPermissionsSuccess(requestPermissions));
  } catch (err) {
    yield showMessageError(err);
  }
}
export function* registerAdminSaga({ payload }) {
  try {
    const url = `${process.env.NEXT_PUBLIC_URL_API}/users/${payload.id}/request-permissions`;
    const options = yield postOptions(payload.values);
    // eslint-disable-next-line no-unused-vars
    const requestPermissions = yield call(request, url, options);
    yield call(Router.replace("/registro-admin/permissionSuccess"));
  } catch (err) {
    yield showMessageError(err);
  }
}
export default function* rootSaga() {
  yield takeLatest(INITIAL_REQUEST_PERMISSIONS_START, initialRequesPermissions);
  yield takeLatest(REGISTER_PERMISSIONS_ADMIN_START, registerAdminSaga);
}
