import { INITIAL_REQUEST_PERMISSIONS_SUCCESS } from "./constants";

const initialState = {
  data: [],
};

export function permissions(state = initialState, action) {
  switch (action.type) {
    case INITIAL_REQUEST_PERMISSIONS_SUCCESS:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
}

export default permissions;
