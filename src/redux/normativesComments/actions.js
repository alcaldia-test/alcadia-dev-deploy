import {
  GET_COMMENTS,
  GET_COMMENTS_SUCCESS,
  GET_CHAPTER_COMMENTS,
  GET_CHAPTER_COMMENTS_SUCCESS,
  GET_MORE_COMMENTS,
  GET_MORE_COMMENTS_SUCCESS,
  POST_COMMENTS,
  POST_COMMENTS_SUCCESS,
  SET_LOADING,
  GET_ANSWER,
  GET_ANSWER_SUCCESS,
  LIKE_COMMENT,
  LIKE_COMMENT_SUCCESS,
  POST_ANSWER,
  POST_ANSWER_SUCCESS,
  COMMENT_COMPLAINT,
  COMMENT_COMPLAINT_SUCCESS,
  GET_COMPLAINTS,
  GET_COMPLAINTS_SUCCESS,
} from "./constans";

export const loading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const getComments = (payload) => ({
  type: GET_COMMENTS,
  payload,
});

export const getCommentsSuccess = (payload) => ({
  type: GET_COMMENTS_SUCCESS,
  payload,
});

export const getChapterComments = (payload) => ({
  type: GET_CHAPTER_COMMENTS,
  payload,
});

export const getChapterCommentsSuccess = (payload) => ({
  type: GET_CHAPTER_COMMENTS_SUCCESS,
  payload,
});

export const getMoreComments = (payload) => ({
  type: GET_MORE_COMMENTS,
  payload,
});

export const getMoreCommentsSuccess = (payload) => ({
  type: GET_MORE_COMMENTS_SUCCESS,
  payload,
});

export const postComments = (payload) => ({
  type: POST_COMMENTS,
  payload,
});

export const postCommentsSuccess = (payload) => ({
  type: POST_COMMENTS_SUCCESS,
  payload,
});

export const getAnswer = (payload) => ({
  type: GET_ANSWER,
  payload,
});

export const getAnswerSuccess = (payload) => ({
  type: GET_ANSWER_SUCCESS,
  payload,
});

export const likeComment = (payload) => ({
  type: LIKE_COMMENT,
  payload,
});

export const likeCommentSuccess = (payload) => ({
  type: LIKE_COMMENT_SUCCESS,
  payload,
});

export const postAnswer = (payload) => ({
  type: POST_ANSWER,
  payload,
});

export const postAnswerSuccess = (payload) => ({
  type: POST_ANSWER_SUCCESS,
  payload,
});

export const complaintComment = (payload) => ({
  type: COMMENT_COMPLAINT,
  payload,
});

export const complaintCommentSuccess = (payload) => ({
  type: COMMENT_COMPLAINT_SUCCESS,
  payload,
});

export const getComplaints = (payload) => ({
  type: GET_COMPLAINTS,
  payload,
});

export const getComplaintsSuccess = (payload) => ({
  type: GET_COMPLAINTS_SUCCESS,
  payload,
});
