import {
  GET_COMMENTS_SUCCESS,
  GET_CHAPTER_COMMENTS_SUCCESS,
  GET_MORE_COMMENTS_SUCCESS,
  POST_COMMENTS_SUCCESS,
  SET_LOADING,
  GET_ANSWER_SUCCESS,
  LIKE_COMMENT_SUCCESS,
  POST_ANSWER_SUCCESS,
  COMMENT_COMPLAINT_SUCCESS,
  GET_COMPLAINTS_SUCCESS,
} from "./constans";

const initialState = {
  comments: [],
  currentEndpoint: "",
  loading: true,
};

export function normativeCommentsReducer(state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return { ...state, ...action.payload };
    case LIKE_COMMENT_SUCCESS:
      return { ...state, ...action.payload };
    case GET_COMMENTS_SUCCESS:
      return { ...state, ...action.payload };
    case GET_CHAPTER_COMMENTS_SUCCESS:
      return { ...state, ...action.payload };
    case GET_ANSWER_SUCCESS:
      return { ...action.payload };
    case GET_MORE_COMMENTS_SUCCESS:
      return { ...action.payload, ...state };
    case POST_COMMENTS_SUCCESS:
      return { ...action.payload, ...state };
    case POST_ANSWER_SUCCESS:
      return { ...state, ...action.payload };
    case COMMENT_COMPLAINT_SUCCESS:
      return { ...state };
    case GET_COMPLAINTS_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return { ...state };
  }
}

export default normativeCommentsReducer;
