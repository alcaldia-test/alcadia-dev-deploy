import {
  GET_COMMENTS,
  GET_CHAPTER_COMMENTS,
  GET_MORE_COMMENTS,
  POST_COMMENTS,
  GET_ANSWER,
  LIKE_COMMENT,
  POST_ANSWER,
  COMMENT_COMPLAINT,
  GET_COMPLAINTS,
} from "./constans";
import {
  getCommentsSuccess,
  getChapterCommentsSuccess,
  getMoreCommentsSuccess,
  postCommentsSuccess,
  getAnswerSuccess,
  likeCommentSuccess,
  postAnswerSuccess,
  complaintCommentSuccess,
  getComplaintsSuccess,
} from "./actions";
import { NormativesServicess } from "@services/normatives";
import { put, takeLatest } from "@redux-saga/core/effects";

export function* getNormativesComments({ payload }) {
  if (payload.id === undefined || payload.id === "") {
    return;
  }
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.getNormativeComments({
    id: payload.id,
    endpoint: payload.endpoint,
    userId: payload.userId,
    limit: payload.limit,
  });
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.answers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getCommentsSuccess({
      comments: [...formatedData],
      currentEnpoint: payload.endpoint,
      loading: false,
    })
  );
}

export function* getNormativesChaptersComments({ payload }) {
  if (payload.id === undefined) {
    return;
  }
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.getNormativeComments({
    id: payload.id,
    endpoint: "chapters",
    userId: payload.userId,
    limit: payload.limit,
  });
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.answers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getChapterCommentsSuccess({
      comments: [...formatedData],
      currentEnpoint: payload.endpoint,
      loading: false,
    })
  );
}

export function* getMoreComments({ payload }) {
  if (payload.id === undefined) {
    return;
  }
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.getMoreNormativeComments({
    id: payload.id,
    endpoint: payload.endpoint,
    limit: payload.limit,
    userId: payload.userId,
  });
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.numberOfAnsers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getMoreCommentsSuccess({
      comments: [...formatedData],
      loading: false,
    })
  );
}

export function* getAnswer({ payload }) {
  const normativesServices = new NormativesServicess();
  const data = yield normativesServices.getAnswer({
    id: payload.id,
    endpoint: payload.endpoint,
    userId: payload.userId,
  });
  const index = payload.currentState.comments.findIndex(
    (comment) => comment.id === payload.id
  );
  const formatedAnswer = data.map((el) => ({
    user: {
      fullName: el.user.firstName,
      userImg: el.user.avatar,
      id: el.user.id,
    },
    text: el.content,
    dislikes: el.dislikes,
    likes: el.likes,
    numberOfAnsers: 0,
    id: el.id,
    fileLink: el.fileLink,
    answers: [],
    reaction: el.reaction,
  }));
  payload.currentState.comments[index].answers = [...formatedAnswer];
  payload.currentState.comments[index].answersOpen = true;
  yield put(getAnswerSuccess({ ...payload.currentState }));
}

export function* postComments({ payload }) {
  if (payload.content === "" || !payload.content) {
    yield put(postCommentsSuccess({}));
  }
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.postComments({
    fileData: payload.fileData,
    content: payload.content,
    userId: payload.userId,
    postsId: payload.postsId,
    chapterId: payload.chapterId,
    id: payload.id,
    endpoint: payload.endpoint,
    limit: payload.limit + 3,
  });
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.answers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getChapterCommentsSuccess({
      comments: [...formatedData],
      currentEnpoint: payload.endpoint,
      loading: false,
    })
  );
  yield put(postCommentsSuccess({ comments: formatedData }));
}

export function* likeComments({ payload }) {
  if (!payload.id || !payload.userId) {
    yield put(likeCommentSuccess({}));
    return;
  }
  const normativesServices = new NormativesServicess();
  yield normativesServices.voteNormative({
    id: payload.id,
    vote: payload.vote,
    endpoint: payload.endpoint,
    userId: payload.userId,
  });
  yield normativesServices.getNormativeComments({
    id: payload.currentId,
    endpoint: payload.currentEnpoint,
    limit: payload.limit,
  });

  if (payload.endpoint === "comments") {
    const index = payload.currentState.findIndex(
      (comment) => comment.id === payload.id
    );
    payload.currentState[index].reaction = payload.vote;
    yield put(likeCommentSuccess({ comments: [...payload.currentState] }));
  }

  if (payload.endpoint === "answers") {
    const index = payload.currentState.findIndex((comment) =>
      comment.answers.some((an) => an.id === payload.id)
    );
    const answerIndex = payload.currentState[index].answers.findIndex(
      ({ id }) => id === payload.id
    );
    payload.currentState[index].answersOpen = true;
    payload.currentState[index].answers[answerIndex].reaction = payload.vote;
    yield put(likeCommentSuccess({ comments: [...payload.currentState] }));
  }
}

export function* postAnswers({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.postAnswer({
    commentId: payload.commentId,
    content: payload.content,
    userId: payload.userId,
    fileData: payload.fileData,
  });
  const formatedData = response.map((el) => ({
    user: {
      fullName: el.user.firstName,
      userImg: el.user.avatar,
      id: el.user.id,
    },
    text: el.content,
    dislikes: el.dislikes,
    likes: el.likes,
    numberOfAnsers: 0,
    id: el.id,
    fileLink: el.fileLink,
    answers: [],
    reaction: el.reaction,
  }));
  const index = payload.currentState.findIndex(
    (comment) => comment.id === payload.commentId
  );
  payload.currentState[index].answers = [...formatedData];
  payload.currentState[index].answersOpen = true;
  yield put(postAnswerSuccess({ comments: [...payload.currentState] }));
}

export function* complaintComment({ payload }) {
  payload.denuncias.forEach(async (denuncia) => {
    const normativesServices = new NormativesServicess();
    await normativesServices
      .complaintComment({
        answerId: payload.answerId,
        commentId: payload.commentId,
        type:
          denuncia === "Spam"
            ? "SPAM"
            : denuncia === "Lenguaje Inapropiado"
            ? "AMISS"
            : "RACISM",
      })
      .then((data) => console.log(data));
  });
  yield put(complaintCommentSuccess({}));
}

export function* getComplaintsComments({ payload }) {
  const normativesServices = new NormativesServicess();
  const response = yield normativesServices.getComplaints({
    id: payload.id,
    endpoint: payload.endpoint,
    offset: payload.skip,
    limit: payload.limit,
  });
  const formatedData = response.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.answers.length,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: comment.answers.map((answer) => ({
      user: {
        fullName: answer.user.firstName,
        userImg: answer.user.avatar,
        id: answer.user.id,
      },
      text: answer.content,
      dislikes: answer.dislikes,
      likes: answer.likes,
      numberOfAnsers: 0,
      id: answer.id,
      fileLink: answer.fileLink,
      answers: [],
      reaction: answer.reaction,
      denunciasAmount: answer.complaints,
    })),
    reaction: comment.reaction,
    answersOpen: false,
    denunciasAmount: comment.complaints,
  }));

  console.log("***    ***   ***", formatedData);

  yield put(
    getComplaintsSuccess({
      comments: formatedData,
      loading: false,
      currentEnpoint: "complaints",
    })
  );
}

export default function* rootSaga() {
  yield takeLatest(GET_COMMENTS, getNormativesComments);
  yield takeLatest(GET_CHAPTER_COMMENTS, getNormativesChaptersComments);
  yield takeLatest(GET_MORE_COMMENTS, getMoreComments);
  yield takeLatest(POST_COMMENTS, postComments);
  yield takeLatest(GET_ANSWER, getAnswer);
  yield takeLatest(LIKE_COMMENT, likeComments);
  yield takeLatest(POST_ANSWER, postAnswers);
  yield takeLatest(COMMENT_COMPLAINT, complaintComment);
  yield takeLatest(GET_COMPLAINTS, getComplaintsComments);
}
