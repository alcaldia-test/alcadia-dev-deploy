export const GET_COMMENTS = "GET_COMMENTS_NORMATIVES";
export const GET_COMMENTS_SUCCESS = "GET_COMMENTS_SUCCESS";
export const GET_CHAPTER_COMMENTS = "GET_CHAPTER_COMMENTS";
export const GET_CHAPTER_COMMENTS_SUCCESS = "GET_CHAPTER_COMMENTS_SUCCESS";
export const GET_MORE_COMMENTS = "GET_MORE_COMMENTS";
export const GET_MORE_COMMENTS_SUCCESS = "GET_MORE_COMMENTS_SUCCESS";
export const POST_COMMENTS = "POST_COMMENTS";
export const POST_COMMENTS_SUCCESS = "POST_COMMENTS_SUCCESS";
export const POST_ANSWER = "POST_ANSWER";
export const POST_ANSWER_SUCCESS = "POST_ANSWER_SUCCESS";
export const SET_LOADING = "SET_LOADING";
export const GET_ANSWER = "GET_ANSWER";
export const GET_ANSWER_SUCCESS = "GET_ANSWER_SUCCESS";
export const LIKE_COMMENT = "LIKE_COMMENT";
export const LIKE_COMMENT_SUCCESS = "LIKE_COMMENT_SUCCESS";
export const COMMENT_COMPLAINT = "COMMENT_COMPLAINT";
export const COMMENT_COMPLAINT_SUCCESS = "COMMENT_COMPLAINT_SUCCESS";
export const GET_COMPLAINTS = "GET_COMPLAINTS";
export const GET_COMPLAINTS_SUCCESS = "GET_COMPLAINTS_SUCCESS";
