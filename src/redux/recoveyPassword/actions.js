import {
  RECOVERY_PASSWORD_CODE_START,
  RECOVERY_PASSWORD_CODE_SUCCESS,
  RECOVERY_PASSWORD_EMAIL_START,
  RECOVERY_PASSWORD_EMAIL_SUCCESS,
  RESET_PASSWORD_START,
  RESET_PASSWORD_SUCCESS,
} from "./constants";

export const recoveryPasswordEmailStart = (payload) => ({
  type: RECOVERY_PASSWORD_EMAIL_START,
  payload,
});

export const recoveryPasswordEmailSuccess = (payload) => ({
  type: RECOVERY_PASSWORD_EMAIL_SUCCESS,
  payload,
});

export const recoveryPasswordCodeStart = (payload) => ({
  type: RECOVERY_PASSWORD_CODE_START,
  payload,
});

export const recoveryPasswordCodeSuccess = (payload) => ({
  type: RECOVERY_PASSWORD_CODE_SUCCESS,
  payload,
});

export const resetPasswordStart = (payload) => ({
  type: RESET_PASSWORD_START,
  payload,
});

export const resetPasswordSuccess = (payload) => ({
  type: RESET_PASSWORD_SUCCESS,
  payload,
});
