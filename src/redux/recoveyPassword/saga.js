import {
  RECOVERY_PASSWORD_CODE_START,
  RECOVERY_PASSWORD_EMAIL_START,
  RESET_PASSWORD_START,
} from "./constants";
import { call, takeLatest, put } from "redux-saga/effects";
import { hideLoader, showLoader } from "@redux/common/actions";
import request, {
  getOptionsWithoutToken,
  postOptionsWithoutToken,
} from "@utils/request";
import {
  recoveryPasswordCodeSuccess,
  recoveryPasswordEmailSuccess,
} from "./actions";

export function* recoveryEmailSaga({
  payload: { resolve, reject, identityCard, email },
}) {
  try {
    yield put(showLoader());
    let url = `${process.env.NEXT_PUBLIC_URL_API}/users/recover-password`;

    if (identityCard) url += `/${identityCard}?admin=false`;
    else url += `/${email}?admin=true`;

    const options = yield getOptionsWithoutToken();
    const requestAdmin = yield call(request, url, options);
    yield put(recoveryPasswordEmailSuccess({ ...requestAdmin, identityCard }));
    yield call(resolve, "Se envió un código de recuperación a su email");
    yield put(hideLoader());
  } catch (err) {
    yield call(reject, "No se pudo enviar el código de recuperacion");
    yield put(hideLoader());
  }
}

export function* recoveryCodeSaga({ payload: { resolve, reject, id, code } }) {
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/users/${id}/recover-password/${code}`;
    const options = yield getOptionsWithoutToken();
    // eslint-disable-next-line no-unused-vars
    const requestAdmin = yield call(request, url, options);
    yield put(recoveryPasswordCodeSuccess(code));
    yield call(resolve, "El código introducido es correcto.");
    yield put(hideLoader());
  } catch (err) {
    yield call(reject, "No se pudo registrar el código");
    yield put(hideLoader());
  }
}

export function* resetCodeSaga({ payload: { resolve, reject, ...payload } }) {
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/users/reset-password`;
    const options = yield postOptionsWithoutToken(payload);
    // eslint-disable-next-line no-unused-vars
    const requestAdmin = yield call(request, url, options);
    yield call(resolve, "Se cambió la contraseña correctamente");
    yield put(hideLoader());
  } catch (err) {
    yield call(reject, "No se pudo cambiar la contraseña correctamente");
    yield put(hideLoader());
  }
}

export default function* rootSaga() {
  yield takeLatest(RECOVERY_PASSWORD_EMAIL_START, recoveryEmailSaga);
  yield takeLatest(RECOVERY_PASSWORD_CODE_START, recoveryCodeSaga);
  yield takeLatest(RESET_PASSWORD_START, resetCodeSaga);
}
