import {
  RECOVERY_PASSWORD_CODE_SUCCESS,
  RECOVERY_PASSWORD_EMAIL_SUCCESS,
  RESET_PASSWORD_SUCCESS,
} from "./constants";

const initialState = {
  email: "",
  userId: "",
  identityCard: "",
  password: {},
  response: {},
};

export function recoveyPassword(state = initialState, action) {
  switch (action.type) {
    case RECOVERY_PASSWORD_EMAIL_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case RECOVERY_PASSWORD_CODE_SUCCESS:
      return {
        ...state,
        password: action.payload,
      };
    case RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        response: action.payload,
      };
    default:
      return { ...state };
  }
}

export default recoveyPassword;
