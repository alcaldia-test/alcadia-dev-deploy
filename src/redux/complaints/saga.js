import { call, put, takeLatest, select, all } from "redux-saga/effects";
import { ComplaitService } from "@services/index";

import {
  REQUEST_COMPLAINTS_START,
  TAKE_COMPLAINTS_START,
  GET_LIST_COMPLAINTS,
  GET_SINGLE_COMMENT,
} from "./constants";
import { requestComplaintsSuccess, setComplaintsCount } from "./actions";

import request, { postOptions } from "@utils/request";

export function* getListComplaints({
  payload: { resolve, reject, ...filter },
}) {
  const service = new ComplaitService();

  try {
    const complaints = yield service.getCommentComplaints(filter);

    yield call(resolve, complaints);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al traer datos");
  }
}

export function* getSingleComment({ payload: { resolve, reject, ...filter } }) {
  const service = new ComplaitService();

  try {
    const comment = yield service.getSingleComment(filter);

    yield call(resolve, comment);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al traer datos");
  }
}

export function* getAllComplaints({
  payload: { resolve, reject, withCount, ...filter },
}) {
  const allEffects = [];
  const service = new ComplaitService();

  try {
    const complaints = yield service.getDatas(filter);

    allEffects.push(put(requestComplaintsSuccess(complaints)));

    if (withCount) {
      const count = yield service.getCount(filter);

      allEffects.push(put(setComplaintsCount(count)));
    }

    allEffects.push(call(resolve, undefined));

    yield all(allEffects);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al traer datos");
  }
}

export function* takeComplaint({
  payload: { resolve, reject, notRender, lock, ...others },
}) {
  const {
    auth: { dataUser },
    complaints: { datas },
  } = yield select((storage) => storage);
  let url, options;

  const server = process.env.NEXT_PUBLIC_URL_API;
  try {
    if (lock) url = `${server}/users/lock`;
    else url = `${server}/complaints/delete-comment`;

    options = postOptions({ ...others, reviewerId: dataUser.id });

    yield call(request, url, options);

    const newDatas = datas.map(({ commentid, answerid, reviewed, ...rest }) => {
      if (
        (commentid && commentid === others.commentId) ||
        (answerid && answerid === others.answerId)
      )
        reviewed = true;

      return { ...rest, commentid, answerid, reviewed };
    });

    yield all([
      call(resolve, undefined),
      put(requestComplaintsSuccess(newDatas)),
    ]);
  } catch (err) {
    console.log(err);
    yield call(reject, "Error al subir datos");
  }
}

export default function* rootSaga() {
  yield takeLatest(REQUEST_COMPLAINTS_START, getAllComplaints);
  yield takeLatest(GET_LIST_COMPLAINTS, getListComplaints);
  yield takeLatest(TAKE_COMPLAINTS_START, takeComplaint);
  yield takeLatest(GET_SINGLE_COMMENT, getSingleComment);
}
