import {
  REQUEST_COMPLAINTS_SUCCESS,
  PATCH_COMPLAINTS_SUCCESS,
  SET_COUNT_COMPLAINTS,
} from "./constants";

const initialState = {
  datas: [],
  count: 0,
};

export function globalReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_COMPLAINTS_SUCCESS:
      return { ...state, datas: action.payload };
    case SET_COUNT_COMPLAINTS:
      return { ...state, count: action.payload };
    case PATCH_COMPLAINTS_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

export default globalReducer;
