import {
  REQUEST_COMPLAINTS_START,
  REQUEST_COMPLAINTS_SUCCESS,
  PATCH_COMPLAINTS_START,
  PATCH_COMPLAINTS_SUCCESS,
  TAKE_COMPLAINTS_START,
  SET_COUNT_COMPLAINTS,
  REQUEST_LOCKEDS_SUCCESS,
  GET_LIST_COMPLAINTS,
  GET_SINGLE_COMMENT,
} from "./constants";

export const getListComplaints = (payload) => ({
  type: GET_LIST_COMPLAINTS,
  payload: payload,
});

export const getSingleComment = (payload) => ({
  type: GET_SINGLE_COMMENT,
  payload: payload,
});

export const requestComplaintsStart = (payload) => ({
  type: REQUEST_COMPLAINTS_START,
  payload: payload,
});

export const requestComplaintsSuccess = (payload) => ({
  type: REQUEST_COMPLAINTS_SUCCESS,
  payload: payload,
});
export const requestLockedsSuccess = (payload) => ({
  type: REQUEST_LOCKEDS_SUCCESS,
  payload: payload,
});

export const setComplaintsCount = (payload) => ({
  type: SET_COUNT_COMPLAINTS,
  payload: payload,
});

export const changeComplaintsStart = (payload) => ({
  type: PATCH_COMPLAINTS_START,
  payload: payload,
});
export const changeComplaintsSuccess = (payload) => ({
  type: PATCH_COMPLAINTS_SUCCESS,
  payload: payload,
});

export const takeComplaintsStart = (payload) => ({
  type: TAKE_COMPLAINTS_START,
  payload: payload,
});
