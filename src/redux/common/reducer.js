import {
  HIDE_LOADER,
  SHOW_LOADER,
  POST_REACTION_SUCCESS,
  GET_RELATED_POSTS_SUCCESS,
} from "./constants";

const initialState = {
  loader: false,
  theme: false, // false = Modo normal, true = Modo noche o modo oscuro
  likes: null,
  reaction: null,
  dislikes: null,
  relatedPosts: null,
};

const updateLikes = (action) => {
  const { likes, dislikes, reaction } = action.payload;
  console.log("Likes previos", likes, dislikes, reaction);
  return {
    likes: reaction === "like" ? likes + 1 : likes,
    dislikes: reaction === "like" ? dislikes : dislikes + 1,
    reaction,
  };
};

export function CommonReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loader: true };
    case HIDE_LOADER:
      return { ...state, loader: false };
    case POST_REACTION_SUCCESS:
      return {
        ...updateLikes(action),
      };
    case GET_RELATED_POSTS_SUCCESS:
      return { ...state, relatedPosts: action.payload };
    default:
      return state;
  }
}

export default CommonReducer;
