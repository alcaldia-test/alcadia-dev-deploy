import { put, takeLatest } from "@redux-saga/core/effects";
import { discussionsServices } from "@services/discussions";
import { POST_REACTION, GET_RELATED_POSTS } from "./constants";
import { getDiscussion } from "@redux/discussions/actions";
import { getNormativeData } from "@redux/chapters/actions";
import { getRelatedPostsSuccess } from "@redux/common/actions";

import message, { textError } from "@components/Message";

export function* postReaction({ payload }) {
  console.log("Receiving sagas payload:", payload);
  const { id, userId } = payload;

  try {
    if (payload.module === "discussion") {
      yield discussionsServices.postReaction(payload);
      yield put(getDiscussion({ idPost: id, userId }));
    } else {
      if (payload.module === "normatives") {
        yield discussionsServices.postReaction(payload);
      } else if (payload.module === "chapter") {
        yield discussionsServices.postReactionChapter(payload);
      }
      yield put(getNormativeData({ id, userId }));
    }
  } catch (error) {
    message.error(textError("Normativa", error.message));
  }
}

export function* getRelatedPosts({ payload }) {
  try {
    const result = yield discussionsServices.getRelatedPosts(payload);
    yield put(getRelatedPostsSuccess(result));
  } catch (error) {
    console.log("Error en saga get discussions", error);
  }
}

export default function* rootSaga() {
  yield takeLatest(POST_REACTION, postReaction);
  yield takeLatest(GET_RELATED_POSTS, getRelatedPosts);
}
