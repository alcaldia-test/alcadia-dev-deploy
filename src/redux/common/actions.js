import {
  HIDE_LOADER,
  SHOW_LOADER,
  POST_REACTION_SUCCESS,
  POST_REACTION,
  GET_RELATED_POSTS,
  GET_RELATED_POSTS_SUCCESS,
} from "./constants";

export const showLoader = () => ({
  type: SHOW_LOADER,
});

export const hideLoader = () => ({
  type: HIDE_LOADER,
});

export const postReaction = (payload) => ({
  type: POST_REACTION,
  payload,
});
export const postReactionSuccess = (payload) => ({
  type: POST_REACTION_SUCCESS,
  payload,
});
export const getRelatedPosts = (payload) => ({
  type: GET_RELATED_POSTS,
  payload,
});
export const getRelatedPostsSuccess = (payload) => ({
  type: GET_RELATED_POSTS_SUCCESS,
  payload,
});
