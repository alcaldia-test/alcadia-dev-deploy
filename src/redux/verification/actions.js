import {
  INITIAL_REQUEST_MACROS_START,
  INITIAL_REQUEST_MACROS_SUCCESS,
  INITIAL_REQUEST_ZONES_START,
  INITIAL_REQUEST_ZONES_SUCCESS,
  REGISTER_CODE_START,
  REGISTER_CODE_SUCCESS,
  REGISTER_ZONE_START,
  REGISTER_ZONE_SUCCESS,
} from "./constants";

export const registerCodeStart = (payload) => ({
  type: REGISTER_CODE_START,
  payload,
});

export const registerCodeSuccess = (payload) => ({
  type: REGISTER_CODE_SUCCESS,
  payload,
});

export const initialRequestMacrosStart = (payload) => ({
  type: INITIAL_REQUEST_MACROS_START,
  payload,
});
export const initialRequestMacrosSuccess = (payload) => ({
  type: INITIAL_REQUEST_MACROS_SUCCESS,
  payload,
});

export const initialRequestZonesStart = (payload) => ({
  type: INITIAL_REQUEST_ZONES_START,
  payload,
});
export const initialRequestZonesSuccess = (payload) => ({
  type: INITIAL_REQUEST_ZONES_SUCCESS,
  payload,
});

export const registerZoneStart = (payload) => ({
  type: REGISTER_ZONE_START,
  payload,
});
export const registerZoneSuccess = (payload) => ({
  type: REGISTER_ZONE_SUCCESS,
  payload,
});
