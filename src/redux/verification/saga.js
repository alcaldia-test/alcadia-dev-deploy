import {
  INITIAL_REQUEST_MACROS_START,
  INITIAL_REQUEST_ZONES_START,
  REGISTER_CODE_START,
  REGISTER_ZONE_START,
} from "./constants";
import request, {
  getOptionsWithoutToken,
  getOptionsWithToken,
  postOptions,
} from "@utils/request";
import {
  initialRequestMacrosSuccess,
  initialRequestZonesSuccess,
  registerCodeSuccess,
} from "./actions";
import { put, takeLatest, call, select, all } from "redux-saga/effects";
import { showLoader, hideLoader } from "@redux/common/actions";
import { loginSuccess } from "@redux/auth/actions";
import moment from "moment";
export function* registerCodeSagas({ payload: { resolve, reject, code, id } }) {
  try {
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/users/${id}/verified-email/${code}`;
    const options = yield getOptionsWithoutToken();
    const requestToken = yield call(request, url, options);
    yield put(registerCodeSuccess(requestToken));
    yield call(resolve, "¡Se registró correctamente el código!");
    yield put(hideLoader());
  } catch (error) {
    yield call(reject, "¡El código ingresado es erróneo !");
    yield put(hideLoader());
  }
}
export function* requestMacrosSaga() {
  try {
    const url = `${process.env.NEXT_PUBLIC_URL_API}/macros`;
    const options = yield getOptionsWithoutToken();
    const requestMacros = yield call(request, url, options);
    requestMacros.forEach((e) => {
      e.value = e.id;
      e.label = e.name;
    });
    yield put(initialRequestMacrosSuccess(requestMacros));
  } catch (error) {
    console.log(error);
  }
}
export function* requestZonesSaga() {
  try {
    const url = `${process.env.NEXT_PUBLIC_URL_API}/zones`;
    const options = yield getOptionsWithoutToken();
    const requestZones = yield call(request, url, options);
    requestZones.forEach((e) => {
      e.value = e.id;
      e.label = e.name;
    });
    yield put(initialRequestZonesSuccess(requestZones));
  } catch (error) {
    console.log(error);
  }
}
export function* registerZonesSaga({
  payload: { resolve, reject, ...payload },
}) {
  const { auth, verification } = yield select((state) => state);
  try {
    yield put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/favorites`;

    const options = yield postOptions({
      macroId: payload.HOME.id,
      tag: "HOME",
      // macroId: payload.HOME.macroId,
      userId: auth.dataUser?.id,
    });

    yield call(request, url, options);

    if (payload.FAMILY.id !== 0) {
      const options2 = yield postOptions({
        macroId: payload.FAMILY.id,
        tag: "FAMILY",
        // macroId: payload.FAMILY.macroId,
        userId: auth.dataUser?.id,
      });

      yield call(request, url, options2);
    }

    if (payload.BUSINESS.id !== 0) {
      const options3 = yield postOptions({
        macroId: payload.BUSINESS.id,
        tag: "BUSINESS",
        // macroId: payload.BUSINESS.macroId,
        userId: auth.dataUser?.id,
      });

      yield call(request, url, options3);
    }

    if (auth?.tokenUser) {
      const url4 = `${process.env.NEXT_PUBLIC_URL_API}/whoAmI`;
      const options4 = yield getOptionsWithToken(auth.tokenUser);
      const requestUser = yield call(request, url4, options4);

      console.log(requestUser);

      yield put(
        loginSuccess({
          dateLogin: moment().format(),
          tokenUser: auth.tokenUser,
          dataUser: requestUser,
        })
      );
    } else {
      const url4 = `${process.env.NEXT_PUBLIC_URL_API}/whoAmI`;
      const options4 = yield getOptionsWithToken(verification.data.token);
      const requestUser = yield call(request, url4, options4);
      yield all([
        put(
          registerCodeSuccess({
            token: verification.data.token,
            ...requestUser,
          })
        ),
        put(
          loginSuccess({
            dateLogin: moment().format(),
            tokenUser: verification.data.token,
            dataUser: requestUser,
          })
        ),
      ]);
    }

    yield call(resolve, "¡Se registró correctamente sus zonas!");
    yield put(hideLoader());
  } catch (error) {
    yield call(reject, "¡No se pudo registrar las zonas!");
    yield put(hideLoader());
  }
}

export default function* rootSaga() {
  yield takeLatest(REGISTER_CODE_START, registerCodeSagas);
  yield takeLatest(INITIAL_REQUEST_MACROS_START, requestMacrosSaga);
  yield takeLatest(INITIAL_REQUEST_ZONES_START, requestZonesSaga);
  yield takeLatest(REGISTER_ZONE_START, registerZonesSaga);
}
