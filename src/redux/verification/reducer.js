import {
  INITIAL_REQUEST_MACROS_SUCCESS,
  INITIAL_REQUEST_ZONES_SUCCESS,
  REGISTER_CODE_SUCCESS,
} from "./constants";

const initialState = {
  data: [],
  zones: [],
  macros: [],
};

export function verification(state = initialState, action) {
  switch (action.type) {
    case REGISTER_CODE_SUCCESS:
      return { ...state, data: action.payload };
    case INITIAL_REQUEST_ZONES_SUCCESS:
      return { ...state, zones: action.payload };
    case INITIAL_REQUEST_MACROS_SUCCESS:
      return { ...state, macros: action.payload };
    default:
      return { ...state };
  }
}

export default verification;
