import {
  GET_COMMENTS,
  GET_CHAPTER_COMMENTS,
  GET_MORE_COMMENTS,
  POST_COMMENTS,
  GET_ANSWER,
  LIKE_COMMENT,
  POST_ANSWER,
} from "./constans";
import {
  getCommentsSuccess,
  getChapterCommentsSuccess,
  getMoreCommentsSuccess,
  postCommentsSuccess,
  getAnswerSuccess,
  likeCommentSuccess,
  postAnswerSuccess,
  loading,
} from "./actions";
import { discussionsServices } from "@services/discussions";
import { put, takeLatest } from "@redux-saga/core/effects";

export function* getDiscussionsComments({ payload }) {
  if (payload.id === undefined || payload.id === "") {
    return;
  }
  const response = yield discussionsServices.getDiscussionsComments({
    id: payload.id,
    endpoint: payload.endpoint,
    userId: payload.userId,
    limit: payload.limit,
  });
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.answers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getCommentsSuccess({
      comments: [...formatedData],
      currentEnpoint: payload.endpoint,
      loading: false,
    })
  );
}

export function* getNormativesChaptersComments({ payload }) {
  if (payload.id === undefined) {
    return;
  }
  yield put(loading({ loading: true }));
  const response = yield discussionsServices.getNormativeComments({
    id: payload.id,
    endpoint: payload.endpoint,
    userId: payload.userId,
    limit: payload.limit,
  });
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.answers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getChapterCommentsSuccess({
      comments: [...formatedData],
      currentEnpoint: payload.endpoint,
      loading: false,
    })
  );
}

export function* getMoreComments({ payload }) {
  if (payload.id === undefined) {
    return;
  }
  yield put(loading({ loading: true }));
  const response = yield discussionsServices.getMoreNormativeComments({
    id: payload.id,
    endpoint: payload.endpoint,
    limit: payload.limit,
  });
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.numberOfAnsers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getMoreCommentsSuccess({
      comments: [...formatedData],
      loading: false,
    })
  );
}

export function* getAnswer({ payload }) {
  const data = yield discussionsServices.getAnswer({
    id: payload.id,
    endpoint: payload.endpoint,
  });
  const index = payload.currentState.comments.findIndex(
    (comment) => comment.id === payload.id
  );
  const formatedAnswer = data.map((el) => ({
    user: {
      fullName: el.user.firstName,
      userImg: el.user.avatar,
      id: el.user.id,
    },
    text: el.content,
    dislikes: el.dislikes,
    likes: el.likes,
    numberOfAnsers: 0,
    id: el.id,
    fileLink: el.fileLink,
    answers: [],
    reaction: el.reaction,
  }));
  payload.currentState.comments[index].answers = [...formatedAnswer];
  payload.currentState.comments[index].answersOpen = true;
  yield put(getAnswerSuccess({ ...payload.currentState }));
}

export function* postComments({ payload }) {
  if (payload.content === "" || !payload.content) {
    yield put(postCommentsSuccess({}));
  }

  const response = yield discussionsServices.postComments({
    fileData: payload.fileData,
    content: payload.content,
    userId: payload.userId,
    postsId: payload.postsId,
    chapterId: payload.chapterId,
    id: payload.id,
    endpoint: payload.endpoint,
    limit: payload.limit + 3,
  });
  yield put(loading({ loading: true }));
  console.log(response);
  const formatedData = response.comments.map((comment) => ({
    user: {
      fullName: comment.user.firstName,
      userImg: comment.user.avatar,
      id: comment.user.id,
    },
    text: comment.content,
    dislikes: comment.dislikes,
    likes: comment.likes,
    numberOfAnsers: comment.answers,
    id: comment.id,
    fileLink: comment.fileLink,
    answers: [],
    reaction: comment.reaction,
    answersOpen: false,
  }));
  yield put(
    getChapterCommentsSuccess({
      comments: [...formatedData],
      currentEnpoint: payload.endpoint,
      loading: false,
    })
  );
  yield put(loading({ loading: false }));
  yield put(postCommentsSuccess({ comments: formatedData }));
}

export function* likeComments({ payload }) {
  if (!payload.id || !payload.userId) {
    yield put(likeCommentSuccess({}));
    return;
  }
  yield put(loading({ loading: true }));

  yield discussionsServices.voteNormative({
    id: payload.id,
    vote: payload.vote,
    endpoint: payload.endpoint,
    userId: payload.userId,
  });
  const commentsResponse = yield discussionsServices.getNormativeComments({
    id: payload.currentId,
    endpoint: payload.currentEnpoint,
    limit: payload.limit,
  });
  console.log(commentsResponse);

  if (payload.endpoint === "comments") {
    const index = payload.currentState.findIndex(
      (comment) => comment.id === payload.id
    );
    payload.currentState[index].reaction = payload.vote;
    yield put(likeCommentSuccess({ comments: [...payload.currentState] }));
  }

  if (payload.endpoint === "answers") {
    const index = payload.currentState.findIndex((comment) =>
      comment.answers.some((an) => an.id === payload.id)
    );
    const answerIndex = payload.currentState[index].answers.findIndex(
      ({ id }) => id === payload.id
    );
    payload.currentState[index].answersOpen = true;
    payload.currentState[index].answers[answerIndex].reaction = payload.vote;
    yield put(likeCommentSuccess({ comments: [...payload.currentState] }));
  }

  yield put(loading({ loading: false }));
}

export function* postAnswers({ payload }) {
  yield put(loading({ loading: true }));
  const response = yield discussionsServices.postAnswer({
    commentId: payload.commentId,
    content: payload.content,
    userId: payload.userId,
    fileData: payload.fileData,
  });
  const formatedData = response.map((el) => ({
    user: {
      fullName: el.user.firstName,
      userImg: el.user.avatar,
      id: el.user.id,
    },
    text: el.content,
    dislikes: el.dislikes,
    likes: el.likes,
    numberOfAnsers: 0,
    id: el.id,
    fileLink: el.fileLink,
    answers: [],
    reaction: el.reaction,
  }));
  const index = payload.currentState.findIndex(
    (comment) => comment.id === payload.commentId
  );
  payload.currentState[index].answers = [...formatedData];
  payload.currentState[index].answersOpen = true;
  yield put(postAnswerSuccess({ comments: [...payload.currentState] }));
  yield put(loading({ loading: false }));
}

export default function* rootSaga() {
  yield takeLatest(GET_COMMENTS, getDiscussionsComments);
  yield takeLatest(GET_CHAPTER_COMMENTS, getNormativesChaptersComments);
  yield takeLatest(GET_MORE_COMMENTS, getMoreComments);
  yield takeLatest(POST_COMMENTS, postComments);
  yield takeLatest(GET_ANSWER, getAnswer);
  yield takeLatest(LIKE_COMMENT, likeComments);
  yield takeLatest(POST_ANSWER, postAnswers);
}
