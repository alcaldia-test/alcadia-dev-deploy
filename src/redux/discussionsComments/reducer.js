import {
  GET_COMMENTS_SUCCESS,
  GET_CHAPTER_COMMENTS_SUCCESS,
  GET_MORE_COMMENTS_SUCCESS,
  POST_COMMENTS_SUCCESS,
  SET_LOADING,
  GET_ANSWER_SUCCESS,
  LIKE_COMMENT_SUCCESS,
  POST_ANSWER_SUCCESS,
} from "./constans";

const initialState = {
  comments: [],
  currentEndpoint: "",
  loading: true,
};

export function discussionsCommentsReducer(state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return { ...state, ...action.payload };
    case LIKE_COMMENT_SUCCESS:
      return { ...state, ...action.payload };
    case GET_COMMENTS_SUCCESS:
      return { ...state, ...action.payload };
    case GET_CHAPTER_COMMENTS_SUCCESS:
      return { ...action.payload };
    case GET_ANSWER_SUCCESS:
      return { ...action.payload };
    case GET_MORE_COMMENTS_SUCCESS:
      return { ...action.payload, ...state };
    case POST_COMMENTS_SUCCESS:
      return { ...action.payload, ...state };
    case POST_ANSWER_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return { ...state };
  }
}

export default discussionsCommentsReducer;
