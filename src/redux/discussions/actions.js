import {
  CREATE_DISCUSSION,
  GET_ALL_DISCUSSIONS,
  GET_ALL_DISCUSSIONS_SUCCESS,
  GET_DISCUSSION,
  HANDLE_DELETE_TAG,
  HANDLE_DISCUSSION,
  HANDLE_GET_DISCUSSION,
  HANDLE_NEW_TAG,
  NEW_DISCUSSION,
  GET_DISCUSSION_SUCCESS,
  GET_ALL_COMMENTS,
  GET_ALL_COMMENTS_SUCCESS,
  POST_COMMENT,
  POST_COMMENT_SUCCESS,
  DISCUSSIONS_COUNT,
  DISCUSSIONS_COUNT_SUCCESS,
  GET_FILTER_DISCUSSIONS_ADMIN,
  GET_FILTER_DISCUSSIONS_ADMIN_SUCCESS,
  GET_ADMIN_DISCUSSIONS_SUCCESS,
  GET_ZONE,
  GET_ZONE_SUCCESS,
  GET_MACRO_ZONES,
  GET_MACRO_ZONES_SUCCESS,
  RELOAD_CARDS,
} from "./constants";

export const createDiscussion = (payload) => ({
  type: CREATE_DISCUSSION,
  payload,
});

export const createInitialDiscussion = (payload) => ({
  type: NEW_DISCUSSION,
  payload,
});

export const handleDiscussionChange = (payload) => ({
  type: HANDLE_DISCUSSION,
  payload,
});

export const handleNewTag = (payload) => ({
  type: HANDLE_NEW_TAG,
  payload,
});

export const handleDeleteTag = (payload) => ({
  type: HANDLE_DELETE_TAG,
  payload,
});

export const handleGetDiscussion = (payload) => ({
  type: HANDLE_GET_DISCUSSION,
  payload,
});

export const getDiscussionById = (payload) => ({
  type: GET_DISCUSSION,
  payload,
});
export const getAllDiscussions = (payload) => ({
  type: GET_ALL_DISCUSSIONS,
  payload,
});
export const getAllDiscussionsSuccess = (payload) => ({
  type: GET_ALL_DISCUSSIONS_SUCCESS,
  payload,
});
export const getAdminDiscussionsSuccess = (payload) => ({
  type: GET_ADMIN_DISCUSSIONS_SUCCESS,
  payload,
});
export const getDiscussion = (payload) => ({
  type: GET_DISCUSSION,
  payload,
});
export const getDiscussionSuccess = (payload) => ({
  type: GET_DISCUSSION_SUCCESS,
  payload,
});
export const getAllComments = (payload) => ({
  type: GET_ALL_COMMENTS,
  payload,
});
export const getAllCommentsSuccess = (payload) => ({
  type: GET_ALL_COMMENTS_SUCCESS,
  payload,
});
export const postComment = (payload) => ({
  type: POST_COMMENT,
  payload,
});
export const postCommentSuccess = (payload) => ({
  type: POST_COMMENT_SUCCESS,
  payload,
});

export const discussionsCount = (payload) => ({
  type: DISCUSSIONS_COUNT,
  payload,
});
export const discussionsCountSuccess = (payload) => ({
  type: DISCUSSIONS_COUNT_SUCCESS,
  payload,
});

export const getFilterDiscussionsAdmin = (payload) => ({
  type: GET_FILTER_DISCUSSIONS_ADMIN,
  payload,
});

export const getFilterDiscussionsAdminSuccess = (payload) => ({
  type: GET_FILTER_DISCUSSIONS_ADMIN_SUCCESS,
  payload,
});
export const getZone = (payload) => ({
  type: GET_ZONE,
  payload,
});

export const getZoneSuccess = (payload) => ({
  type: GET_ZONE_SUCCESS,
  payload,
});

export const getMacroZones = (payload) => ({
  type: GET_MACRO_ZONES,
  payload,
});

export const getMacroZonesSuccess = (payload) => ({
  type: GET_MACRO_ZONES_SUCCESS,
  payload,
});
export const reloadCards = (payload) => ({
  type: RELOAD_CARDS,
  payload,
});
