import { call, put, takeLatest } from "@redux-saga/core/effects";
import { hideLoader, showLoader } from "@redux/common/actions";
import fetchWrap from "@utils/client";
import { ZonesServices } from "@services/zones";
import {
  createDiscussion,
  getAllDiscussionsSuccess,
  getDiscussionSuccess,
  getAllCommentsSuccess,
  postCommentSuccess,
  discussionsCountSuccess,
  getAdminDiscussionsSuccess,
  getZoneSuccess,
  getMacroZonesSuccess,
} from "./actions";
import {
  GET_ALL_DISCUSSIONS,
  GET_DISCUSSION,
  NEW_DISCUSSION,
  GET_ALL_COMMENTS,
  POST_COMMENT,
  DISCUSSIONS_COUNT,
  GET_FILTER_DISCUSSIONS_ADMIN,
  GET_MACRO_ZONES,
} from "./constants";
import { discussionsServices } from "@services/discussions";

export function* createNewDiscussion() {
  try {
    const response = yield call(fetchWrap, "/discussions", {
      method: "POST",
      token: false,
      body: {},
    });
    yield put(createDiscussion(response));
  } catch (error) {
    console.log(error);
  }
}

export function* getAllDiscussions({ payload }) {
  try {
    const result = yield discussionsServices.getDiscussions(payload);
    yield put(getAllDiscussionsSuccess(result));
  } catch (error) {
    console.log("Error en saga get discussions", error);
  }
}
export function* getDiscussion({ payload }) {
  try {
    const result = yield discussionsServices.getDiscussion(payload);
    yield put(getDiscussionSuccess(result));
  } catch (error) {
    console.log("Error en saga get discussions", error);
  }
}
export function* getAllComments({ payload }) {
  try {
    const result = yield discussionsServices.getAllComments(payload);

    yield put(getAllCommentsSuccess(result));
  } catch (error) {
    console.log("Error en saga get discussions", error);
  }
}
export function* postComments({ payload }) {
  try {
    const result = yield discussionsServices.postComment(payload);

    yield put(postCommentSuccess(result));
  } catch (error) {
    console.log("Error en saga get discussions", error);
  }
}

export function* discussionsCount() {
  try {
    const result = yield discussionsServices.discussionsCount();
    yield put(discussionsCountSuccess(result));
  } catch (error) {
    console.log("Error en saga get discussions", error);
  }
}

export function* getFilteredDiscussionsAdmin({ payload }) {
  try {
    yield put(showLoader());
    const limit = 8;
    const response = yield discussionsServices.getFilterDiscussionsAdmin({
      status: payload.status,
      order: payload.order,
      content: payload.content === "" ? undefined : payload.content,
      offset: payload.offset,
      limit,
    });
    yield put(getAdminDiscussionsSuccess(response));
  } catch (error) {
    console.log(error);
  } finally {
    yield put(hideLoader());
  }
}

export function* getZonesSaga({ payload }) {
  try {
    const zones = new ZonesServices();
    const result = yield zones.getZones();
    yield put(getZoneSuccess(result));
  } catch (error) {
    console.log("Error en saga get zones");
    console.log(error);
  }
}

export function* getMacroZonesSaga({ payload }) {
  try {
    const zones = new ZonesServices();
    const result = yield zones.getMacroZones();
    yield put(getMacroZonesSuccess(result));
  } catch (error) {
    console.log("Error en saga get zones");
    console.log(error);
  }
}

export default function* rootSaga() {
  yield takeLatest(GET_MACRO_ZONES, getMacroZonesSaga);
  yield takeLatest(NEW_DISCUSSION, createNewDiscussion);
  yield takeLatest(GET_ALL_DISCUSSIONS, getAllDiscussions);
  yield takeLatest(GET_DISCUSSION, getDiscussion);
  yield takeLatest(GET_ALL_COMMENTS, getAllComments);
  yield takeLatest(POST_COMMENT, postComments);
  yield takeLatest(DISCUSSIONS_COUNT, discussionsCount);
  yield takeLatest(GET_FILTER_DISCUSSIONS_ADMIN, getFilteredDiscussionsAdmin);
}
