import {
  CREATE_DISCUSSION,
  HANDLE_DISCUSSION,
  HANDLE_DELETE_TAG,
  HANDLE_NEW_TAG,
  HANDLE_GET_DISCUSSION,
  GET_ALL_DISCUSSIONS_SUCCESS,
  GET_ADMIN_DISCUSSIONS_SUCCESS,
  GET_DISCUSSION_SUCCESS,
  GET_ALL_COMMENTS_SUCCESS,
  POST_COMMENT_SUCCESS,
  DISCUSSIONS_COUNT_SUCCESS,
  GET_ZONE_SUCCESS,
  GET_MACRO_ZONES_SUCCESS,
  RELOAD_CARDS,
} from "./constants";

const initialState = {
  discussion: null,
  discussions: null,
  comments: null,
  postComments: null,
  discussionsCount: 0,
  zones: [],
  macroZones: [],
  reloadCards: Date.now(),
};

const discussion = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_DISCUSSION:
      return {
        ...state,
        discussion: { ...state.discussion, ...action.payload },
      };
    case HANDLE_DISCUSSION:
      return {
        ...state,
        discussion: {
          ...state.discussion,
          [action.payload.name]: action.payload.value,
        },
      };
    case HANDLE_DELETE_TAG:
      return {
        ...state,
        discussion: {
          ...state.discussion,
          tags: state.discussion.tags.filter(
            (tag, index) => index !== action.payload
          ),
        },
      };
    case HANDLE_NEW_TAG:
      return {
        ...state,
        discussion: {
          ...state.discussion,
          tags: [...state.discussion.tags, action.payload],
        },
      };
    case HANDLE_GET_DISCUSSION:
      return {
        ...state,
        singleDiscussion: action.payload,
      };
    case GET_ALL_DISCUSSIONS_SUCCESS:
      return {
        ...state,
        discussions: action.payload,
      };
    case GET_ADMIN_DISCUSSIONS_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case GET_DISCUSSION_SUCCESS:
      return {
        ...state,
        discussion: action.payload,
      };
    case GET_ALL_COMMENTS_SUCCESS:
      return {
        ...state,
        comments: action.payload,
      };
    case POST_COMMENT_SUCCESS:
      return {
        ...state,
        postComments: action.payload,
      };

    case DISCUSSIONS_COUNT_SUCCESS:
      return {
        ...state,
        discussionsCount: action.payload,
      };
    case GET_ZONE_SUCCESS:
      return { ...state, zones: action.payload };

    case GET_MACRO_ZONES_SUCCESS:
      return { ...state, macroZones: action.payload };
    case RELOAD_CARDS:
      return { ...state, reloadCards: Date.now() };
    default:
      return state;
  }
};

export default discussion;
