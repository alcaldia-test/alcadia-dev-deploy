import { RESEND_PASSWORD_START } from "@redux/verification/constants";
import {
  LOGIN_START,
  LOGIN_SUCCESS,
  SIGNOUT_START,
  SIGNOUT_SUCCESS,
  LOGIN_ADMIN_START,
  LOGIN_ADMIN_SUCCESS,
  REGISTER_USER_START,
  REGISTER_USER_SUCCESS,
  REGISTER_ADMIN_START,
  REGISTER_ADMIN_SUCCESS,
  LOGIN_GOOGLE_START,
  LOGIN_GOOGLE_SUCCESS,
  LOGIN_DATA_START,
  REGISTER_EXTRA_LOGIN_START,
  REGISTER_EXTRA_LOGIN_SUCCESS,
  CHANGE_USER_AVATAR_SUCCESS,
  SPIN_LOGIN_VERIFICATED,
  ROUTE_REDIRECT_REGISTER,
  CHANGE_USER_INFO,
  UPDATE_USER_PERMISSION,
  CHANGE_PASSWORD_START,
} from "./constants";

export const loginStart = (payload) => ({
  type: LOGIN_START,
  payload,
});

export const changeUserInfo = (payload) => ({
  type: CHANGE_USER_INFO,
  payload,
});

export const updateUserPerminsion = () => ({
  type: UPDATE_USER_PERMISSION,
});

export const loginDataStart = (payload) => ({
  type: LOGIN_DATA_START,
  payload,
});

export const loginSuccess = (payload) => ({
  type: LOGIN_SUCCESS,
  payload,
});

export const loginGoogleStart = (payload) => ({
  type: LOGIN_GOOGLE_START,
  payload,
});

export const loginGoogleSuccess = (payload) => ({
  type: LOGIN_GOOGLE_SUCCESS,
  payload,
});

export const loginAdminStart = (payload) => ({
  type: LOGIN_ADMIN_START,
  payload,
});
export const loginAdminSuccess = (payload) => ({
  type: LOGIN_ADMIN_SUCCESS,
  payload,
});

export const registerUserStart = (payload) => ({
  type: REGISTER_USER_START,
  payload,
});
export const registerUserSuccess = (payload) => ({
  type: REGISTER_USER_SUCCESS,
  payload,
});

export const signOutStart = (payload) => ({
  type: SIGNOUT_START,
  payload,
});
export const signOutSuccess = () => ({
  type: SIGNOUT_SUCCESS,
});

export const registerAdminStart = (payload) => ({
  type: REGISTER_ADMIN_START,
  payload,
});
export const registerAdminSuccess = (payload) => ({
  type: REGISTER_ADMIN_SUCCESS,
  payload,
});
export const resendPasswordStart = (payload) => ({
  type: RESEND_PASSWORD_START,
  payload,
});

export const registerExtraLoginStart = (payload) => ({
  type: REGISTER_EXTRA_LOGIN_START,
  payload,
});
export const registerExtraLoginSuccess = (payload) => ({
  type: REGISTER_EXTRA_LOGIN_SUCCESS,
  payload,
});
export const changeUserAvatarSuccess = (payload) => ({
  type: CHANGE_USER_AVATAR_SUCCESS,
  payload,
});

export const spinLoginVerificated = (payload) => ({
  type: SPIN_LOGIN_VERIFICATED,
  payload,
});

export const routeRedirectRegister = (payload) => ({
  type: ROUTE_REDIRECT_REGISTER,
  payload,
});

export const changePassowordStart = (payload) => ({
  type: CHANGE_PASSWORD_START,
  payload,
});
