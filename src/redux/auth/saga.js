import { put, takeLatest, call } from "redux-saga/effects";
import { showLoader, hideLoader } from "@redux/common/actions";
import {
  loginAdminSuccess,
  loginSuccess,
  registerAdminSuccess,
  registerUserSuccess,
  changeUserInfo,
} from "@redux/auth/actions";
import {
  CHANGE_PASSWORD_START,
  LOGIN_ADMIN_START,
  LOGIN_DATA_START,
  LOGIN_START,
  REGISTER_ADMIN_START,
  REGISTER_EXTRA_LOGIN_START,
  REGISTER_USER_START,
  UPDATE_USER_PERMISSION,
} from "./constants";

import request, {
  postOptionsWithoutToken,
  getOptionsWithToken,
  getOptions,
  patchOptions,
  postOptions,
} from "@utils/request";
import moment from "moment";
import { RESEND_PASSWORD_START } from "@redux/verification/constants";
import message from "@components/Message";
import { registerCodeSuccess } from "@redux/verification/actions";

export function* Login({ payload: { resolve, reject, ...payload } }) {
  let url, options;
  try {
    yield put(showLoader());

    url = `${process.env.NEXT_PUBLIC_URL_API}/users/login`;
    options = yield postOptionsWithoutToken(payload);
    const requestToken = yield call(request, url, options);

    let successLogin = {};

    if (requestToken.token) {
      url = `${process.env.NEXT_PUBLIC_URL_API}/whoAmI`;
      options = yield getOptionsWithToken(requestToken.token);

      const requestUser = yield call(request, url, options);

      successLogin = {
        dateLogin: moment().format(),
        tokenUser: requestToken.token,
        dataUser: { ...requestUser },
        typeLogin: "User",
      };

      yield call(resolve, "/");
    } else {
      successLogin = {
        dateLogin: moment().format(),
        tokenUser: requestToken.token,
        dataUser: { ...requestToken },
        typeLogin: "User",
      };

      yield call(resolve, "/registro");
    }

    yield put(loginSuccess(successLogin));
  } catch (error) {
    yield call(reject, error.toString());
  } finally {
    yield put(hideLoader());
  }
}

export function* LoginAdmin({ payload: { resolve, reject, ...payload } }) {
  let url, options;
  try {
    yield put(showLoader());
    url = `${process.env.NEXT_PUBLIC_URL_API}/users/admin/login`;
    options = yield postOptionsWithoutToken(payload);
    const requestToken = yield call(request, url, options);
    url = `${process.env.NEXT_PUBLIC_URL_API}/whoAmI`;
    options = yield getOptionsWithToken(requestToken.token);
    const requestUser = yield call(request, url, options);
    yield put(
      loginAdminSuccess({
        dateLogin: moment().format(),
        tokenUser: requestToken.token,
        dataUser: { ...requestUser },
        typeLogin: "Admin",
      })
    );
    yield put(hideLoader());
    yield call(resolve, "¡Inicio de sesión exitoso!");
  } catch (error) {
    yield call(reject, error.toString());
    yield put(hideLoader());
  }
}

export function* Register({ payload: { resolve, reject, ...payload } }) {
  try {
    if (payload?.formData?.complement) {
      delete payload?.formData?.complement;
    }
    yield put(showLoader());

    if (payload.formData.name) {
      const name = payload.formData.name.split(" ");

      payload.formData.firstName = name[0];
      payload.formData.paternalLastName = name[0];

      if (name[1]) payload.formData.paternalLastName = name[1];
    }

    delete payload.formData.name;

    const url = `${process.env.NEXT_PUBLIC_URL_API}/users/signup`;
    const options = yield postOptionsWithoutToken(payload.formData);

    const requestToken = yield call(request, url, options);
    yield put(registerUserSuccess(requestToken));
    yield call(resolve, "¡Se registró con éxito!");
    yield put(hideLoader());
  } catch (err) {
    yield call(reject, err.toString());
    yield put(hideLoader());
  }
}

export function* RegisterAdmin({ payload: { resolve, reject, ...payload } }) {
  try {
    yield put(showLoader());
    const name = payload.formData.name.split(" ");
    if (name[0]) {
      payload.formData.firstName = name[0];
      payload.formData.paternalLastName = name[0];
    }
    if (name[1]) {
      payload.formData.firstName = name[0];
      payload.formData.paternalLastName = name[1];
    }
    delete payload.formData.name;
    const url = `${process.env.NEXT_PUBLIC_URL_API}/users/admin/signup`;
    const options = yield postOptionsWithoutToken(payload.formData);
    const requestToken = yield call(request, url, options);
    console.log(requestToken);
    yield put(registerAdminSuccess(requestToken));
    yield call(resolve, "¡Se registró con éxito!");
    yield put(hideLoader());
  } catch (err) {
    console.log(err);
    yield call(reject, err.toString());
    yield put(hideLoader());
  }
}

export function* ResendPasswordSaga({
  payload: { resolve, reject, email, identityCard },
}) {
  let url, options;
  try {
    yield put(showLoader());
    url = `${process.env.NEXT_PUBLIC_URL_API}/users/resend-code`;

    if (identityCard) url += `/${identityCard}?admin=false`;
    else url += `/${email}?admin=true`;

    options = yield getOptions();
    yield call(request, url, options);
    yield call(resolve, "¡Se envió un nuevo código de verificación!");
    yield put(hideLoader());
  } catch (err) {
    yield put(hideLoader());

    yield call(
      reject,
      "¡Ocurrió un error y no se pudo enviar el nuevo código!"
    );
  }
}

export function* changePassoword({ payload: { resolve, reject, ...others } }) {
  let url, options;
  try {
    url = `${process.env.NEXT_PUBLIC_URL_API}/users/change-password`;

    options = yield postOptions(others);
    yield call(request, url, options);

    yield call(resolve, "¡Se cambió la contraseña exitosamente!");
  } catch (err) {
    yield call(reject, err);
  }
}

export function* LoginDataSaga({ payload }) {
  try {
    yield put(showLoader());
    yield put(
      loginSuccess({
        dateLogin: moment().format(),
        tokenUser: payload.token,
        dataUser: { ...payload.whoAmi },
        typeLogin: "User",
      })
    );
    yield put(hideLoader());
    // yield put(spinLoginVerificated({ spinLogin: "block" }));
    // message.success("Inicio de sesion exitoso");
  } catch (error) {
    yield put(hideLoader());
    message.error("Error al iniciar sesion");
  }
}
export function* registerExtraSaga({
  payload: { resolve, reject, formData, id, payload },
}) {
  try {
    if (payload?.formData?.complement) {
      delete payload?.formData?.complement;
    }
    yield put(showLoader());
    const url = `${process.env.NEXT_PUBLIC_URL_API}/signup/complete/${id}`;
    const options = yield patchOptions(formData);
    yield call(request, url, options);
    yield put(registerCodeSuccess({ id, formData }));
    yield call(resolve, "Se registró con éxito los datos");
    yield put(hideLoader());
  } catch (err) {
    yield put(hideLoader());
    yield call(reject, err.toString());
  }
}

export function* updatePerminsion() {
  try {
    const options = yield getOptions();
    const url = `${process.env.NEXT_PUBLIC_URL_API}/whoAmI`;
    const userInfo = yield request(url, options);

    yield put(changeUserInfo(userInfo));
  } catch (e) {
    message.error(e.message);
  }
}

export default function* rootSaga() {
  yield takeLatest(LOGIN_START, Login);
  yield takeLatest(LOGIN_ADMIN_START, LoginAdmin);
  yield takeLatest(REGISTER_USER_START, Register);
  yield takeLatest(UPDATE_USER_PERMISSION, updatePerminsion);
  yield takeLatest(CHANGE_PASSWORD_START, changePassoword);
  yield takeLatest(REGISTER_ADMIN_START, RegisterAdmin);
  yield takeLatest(RESEND_PASSWORD_START, ResendPasswordSaga);
  yield takeLatest(LOGIN_DATA_START, LoginDataSaga);
  yield takeLatest(REGISTER_EXTRA_LOGIN_START, registerExtraSaga);
}
