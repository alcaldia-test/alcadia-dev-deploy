import {
  LOGIN_SUCCESS,
  SIGNOUT_START,
  LOGIN_ADMIN_SUCCESS,
  SIGNOUT_USER_SUCCESS,
  REGISTER_USER_SUCCESS,
  REGISTER_ADMIN_SUCCESS,
  REGISTER_EXTRA_LOGIN_SUCCESS,
  CHANGE_USER_AVATAR_SUCCESS,
  SPIN_LOGIN_VERIFICATED,
  ROUTE_REDIRECT_REGISTER,
  CHANGE_USER_INFO,
} from "./constants";

export const initialState = {
  tokenUser: "",
  dataUser: {},
  dateLogin: "",
  userRegister: {},
  adminRegister: {},
  spinLogin: "hidden",
  RouterLogin: false,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const { tokenUser, dataUser, dateLogin } = action.payload;
      return {
        ...state,
        dateLogin,
        tokenUser,
        dataUser: { ...state.dataUser, ...dataUser },
      };
    }
    case SIGNOUT_USER_SUCCESS: {
      return {
        ...state,
        tokenUser: null,
        dataUser: {},
        initURL: "/",
        loader: false,
        notifications: [],
      };
    }
    case LOGIN_ADMIN_SUCCESS: {
      const { tokenUser, dataUser, dateLogin } = action.payload;
      return {
        ...state,
        dateLogin,
        tokenUser,
        dataUser: { ...state.dataUser, ...dataUser },
      };
    }
    case CHANGE_USER_INFO: {
      return {
        ...state,
        dataUser: { ...state.dataUser, ...action.payload },
      };
    }
    case SIGNOUT_START: {
      return { ...state, ...initialState };
    }
    case REGISTER_USER_SUCCESS: {
      return {
        ...state,
        userRegister: action.payload,
      };
    }
    case REGISTER_ADMIN_SUCCESS: {
      return {
        ...state,
        adminRegister: action.payload,
      };
    }
    case "SET_FAKE_DATA": {
      return { ...state, ...action.payload };
    }
    case CHANGE_USER_AVATAR_SUCCESS: {
      return {
        ...state,
        dataUser: { ...state.dataUser, avatar: action.payload },
      };
    }
    case REGISTER_EXTRA_LOGIN_SUCCESS: {
      return {
        ...state,
        userRegister: action.payload,
      };
    }
    case SPIN_LOGIN_VERIFICATED: {
      return { ...state, ...action.payload };
    }
    case ROUTE_REDIRECT_REGISTER: {
      return { ...state, RouterLogin: action.payload };
    }
    default:
      return state;
  }
};

export default auth;
