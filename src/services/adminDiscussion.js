import { Middleware } from "./middleware";

export class AdminDiscussionServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  postDiscussion(data) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`discussions`, { data }).then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  updateDiscussion(data, id) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`discussions/${id}`, { data }, "PATCH").then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        });
      } catch (error) {
        console.log("error en el patch endpoint");
        reject(error);
      }
    });
  }

  getPostById(idPost) {
    return new Promise((resolve, reject) => {
      try {
        const filter = {
          fields: {},
          include: [
            {
              relation: "posts",
            },
          ],
        };

        this.setFilterStatic(filter);
        this.getFetchStatic(`discussions/${idPost}`).then((post) => {
          resolve(post);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  deletePost(id) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`discussions/${id}`, {}, "DELETE").then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  deleteFile({ id, tag }) {
    return new Promise((resolve, reject) => {
      try {
        const where = {
          tag,
        };
        where.postsId = id;

        this.deleteFetchEndpointWithWhere("file-storages", where).then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }
}
