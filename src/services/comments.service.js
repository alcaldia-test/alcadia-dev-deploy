import request, {
  deleteOptions,
  postOptions,
  postOptionsFormData,
} from "@utils/request";
import { Middleware } from "./middleware";

class CommentServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getAnswers({ limit, skip, cid }) {
    const filter = {
      limit: limit ?? 5,
      skip: skip ?? 0,
      order: "DESC",
    };

    try {
      this.setFilterStatic(filter);

      return await this.getFetchStatic(`comments/${cid}/answers`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getComments({
    limit = 5,
    order = "DESC",
    byLikes,
    skip = 0,
    id,
    module,
    userId,
    type = "comments",
  }) {
    const filter = {
      limit,
      skip,
      order,
      byLikes,
    };

    if (["chapter", "proposal"].includes(module)) module += "s";

    try {
      this.setFilterStatic(filter);

      const url = `${module}/${id}/${type}?userId=${userId}`;

      const datas = await this.getFetchStaticWithUserId(url);

      if (byLikes && order === "DESC")
        return datas?.sort((a, b) => b.likes - a.likes);
      if (byLikes && order === "ASC")
        return datas?.sort((a, b) => a.likes - b.likes);
      else return datas;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async createAnswer(body, file) {
    let url, options;

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;
      url = `${server}/comments/${body.commentId}/answer`;
      options = postOptions(body);

      const created = await request(url, options);

      if (file) {
        url = `${server}/containers/answer/upload/attached`;
        const formdata = new FormData();
        formdata.append(created?.id, file);

        options = postOptionsFormData(formdata);
        await request(url, options);
      }

      return created;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async createComment(body, file) {
    let url, options;

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;
      url = `${server}/comments`;
      options = postOptions(body);

      const created = await request(url, options);

      if (file) {
        url = `${server}/containers/comment/upload/attached`;
        const formdata = new FormData();
        formdata.append(created?.id, file);

        options = postOptionsFormData(formdata);
        const [newfile] = await request(url, options);
        console.log(newfile);
      }

      return created;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async reactComment(body, commentId, answerId) {
    let url, options;

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;

      if (commentId) url = `${server}/comments/${commentId}/react`;
      else url = `${server}/answers/${answerId}/react`;

      options = postOptions(body);

      await request(url, options);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async verifyComplainted(body) {
    let url, options;

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;

      url = `${server}//verify-complainted`;
      options = postOptions(body);

      return await request(url, options);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async takeComplaint({ lock, ...others }) {
    let url, options;

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;

      if (lock) url = `${server}/users/lock`;
      else url = `${server}/complaints/delete-comment`;

      console.log(others);
      options = postOptions(others);

      await request(url, options);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async deleteComment(commentId, answerId) {
    let url, options;

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;

      if (commentId) url = `${server}/comments/${commentId}`;
      else url = `${server}/answers/${answerId}`;

      options = deleteOptions();

      await request(url, options);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async complaintComment(body) {
    let url, options;

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;

      url = `${server}/complaints`;

      options = postOptions(body);

      await request(url, options);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}

export default CommentServices;
