import { Middleware } from "./middleware";

class LandingServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getNoticationCount(id) {
    const where = { userId: id };

    try {
      const resp = await this.fetchWithWhere(`notifications/count`, where);

      return resp.count;
    } catch (e) {
      console.log(e);
    }
  }

  async getNotifies({ limit = 4, skip = 0, userId }) {
    const filter = {
      limit,
      skip,
      order: ["createdAt DESC"],
    };

    this.setFilterStatic(filter);

    try {
      return this.getFetchStatic(`/users/${userId}/notifications`);
    } catch (e) {
      console.log(e);
    }
  }

  getAchievements({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    state = "PUBLISHED",
    macros = undefined,
    zones = undefined,
  }) {
    const filter = {
      limit,
      skip,
      order,
      state,
      where: {
        like,
        macros,
        zones,
      },
    };

    this.setFilterStatic(filter);

    try {
      return this.getFetchStatic(`attainments/`);
    } catch (e) {
      console.log(e);
    }
  }

  getLanding({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    status = undefined,
    macros = undefined,
    zones = undefined,
  }) {
    const filter = {
      limit,
      skip,
      order,
      where: {
        like,
        status,
        macros,
        zones,
      },
    };

    this.setFilterStatic(filter);

    try {
      return this.getFetchStatic(`landing-page/`);
    } catch (e) {
      console.log(e);
    }
  }

  async getPosts(payload) {
    try {
      const forLanding = await Promise.all([
        this.getAchievements(payload),
        this.getLanding(payload),
      ]);
      return { achievements: forLanding[0], ...forLanding[1] };
    } catch (e) {
      console.log(e);
    }
  }
}

export const landingServices = new LandingServices();
