import { Middleware } from "./middleware";

class AnnouncementsServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getTotalAnnouncements() {
    try {
      return await this.getFetchNoFilter(`announcements/count`);
    } catch (e) {
      console.log(e);
    }
  }

  async getTotalProposals(id) {
    try {
      return await this.getFetchNoFilter(`announcements/${id}/proposals/count`);
    } catch (e) {
      console.log(e);
    }
  }

  async getCountAnnouncements() {
    try {
      return await this.getFetchStatic(`announcements/count`);
    } catch (e) {
      console.log(e);
    }
  }

  async getCountProposals(id) {
    try {
      return await this.getFetchStatic(`announcements/${id}/proposals/count`);
    } catch (e) {
      console.log(e);
    }
  }

  async getFavorites(userId = undefined) {
    const filter = {
      where: {
        userId,
      },
      include: [
        {
          relation: "zone",
        },
      ],
    };

    if (!userId) return [];

    this.setFilterStatic(filter);

    try {
      return await this.getFetchStatic(`favorites`);
    } catch (e) {
      console.log("error en el get favorites", e);
    }
  }

  async getAnnouncement({ announcementId, userId = undefined }) {
    try {
      if (userId) {
        return await this.getFetchStaticWithUserId(
          `announcements/${announcementId}?userId=${userId}`
        );
      } else {
        return await this.getFetchStatic(`announcements/${announcementId}`);
      }
    } catch (e) {
      console.log(e);
    }
  }

  async getProposal({ id, userId }) {
    try {
      if (userId) {
        return await this.getFetchStaticWithUserId(
          `proposals/${id}?userId=${userId}`
        );
      }
      return await this.getFetchStatic(`proposals/${id}`);
    } catch (e) {
      console.log(e);
    }
  }

  async postProposalSupport({
    id,
    reaction,
    userId = undefined,
    announcementId = undefined,
    proposalId = undefined,
  }) {
    const data = {
      reaction,
      userId,
    };

    try {
      await this.postEndpoint(`proposals/${id}/react`, {
        data,
      });

      if (announcementId) {
        return await this.getFetchStaticWithUserId(
          `announcements/${announcementId}/proposals?userId=${userId}`
        );
      } else if (proposalId) {
        return await this.getFetchStaticWithUserId(
          `proposals/${proposalId}?userId=${userId}`
        );
      }
    } catch (e) {
      console.log(e);
    }
  }

  async getAnnouncements({
    limit = 10,
    skip = 0,
    like = undefined,
    order = "DESC",
    status = "created",
    macros = undefined,
    zones = undefined,
  }) {
    const filter = {
      limit,
      skip,
      order,
      where: {
        like,
        status: status === "all" ? undefined : status,
        macros,
        zones,
      },
    };

    if (zones && macros) {
      filter.status = "created";
      filter.where.and = [
        {
          macros: [],
        },
        {
          zones: [],
        },
      ];
    }

    this.setFilterStatic(filter);

    try {
      return await this.getFetchStatic(`announcements`);
    } catch (e) {
      console.log(e);
    }
  }

  async getProposals({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    status = undefined,
    macros = undefined,
    zones = undefined,
    id = undefined,
    userId = undefined,
  }) {
    const filter = {
      limit,
      skip,
      order,
      where: {
        like,
        status,
        macros,
        zones,
      },
    };

    this.setFilterStatic(filter);

    try {
      if (userId) {
        return await this.getFetchStaticWithUserId(
          `announcements/${id}/proposals?userId=${userId}`
        );
      }
      return await this.getFetchStatic(`announcements/${id}/proposals`);
    } catch (e) {
      console.log(e);
    }
  }

  async getProposalsAdmin({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    status = undefined,
    macros = undefined,
    zones = undefined,
    id = undefined,
    userId = undefined,
    state = undefined,
  }) {
    const filter = {
      limit,
      skip,
      order,
      state,
      where: {
        like,
        status,
        macros,
        zones,
      },
    };

    this.setFilterStatic(filter);

    try {
      return await this.getFetchStatic(`announcements/${id}/admin-proposals`);
    } catch (e) {
      console.log(e);
    }
  }

  async createProposalUploadData(data) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`proposals`, { data }).then((x) => {
          resolve(x);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  async createAnnouncementUploadData(data) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`announcements`, { data }).then((x) => {
          resolve(x);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  updateAnnouncementData(data, id) {
    return new Promise((resolve, reject) => {
      console.log({ location: "esta en update", data, id });

      try {
        this.postEndpoint(`announcements/${id}`, { data }, "PATCH").then(
          (x) => {
            if (x.length === 0) {
              reject(new Error());
            } else {
              resolve(x);
            }
          }
        );
      } catch (error) {
        console.log("error en el patch endpoint");
        reject(error);
      }
    });
  }

  async createProposalUploadFiles({ id, file, container, tag }) {
    console.log({ id, file, container, tag });
    return new Promise((resolve, reject) => {
      try {
        const formData = new FormData();
        formData.append(`${id}`, file);
        console.log(formData);
        this.postUploadFile(formData, container, tag).then((x) => {
          resolve(x);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   * Comments
   */

  async getComments({ id, limit = 10, skip = 0 }) {
    console.log({ id, limit, skip });
    const filter = {
      limit,
      skip,
      order: "DESC",
    };

    try {
      this.setFilterStatic(filter);

      return await this.getFetchStatic(`proposals/${id}/comments`);
    } catch (e) {
      console.log(e);
    }
  }

  async postComment({
    postsId,
    content,
    userId,
    fileData,
    limit,
    endpoint,
    id,
  }) {
    console.log("service post comment ", postsId, content, userId, fileData);

    try {
      const data = await this.postEndpoint(`comments`, {
        data: {
          status: true,
          content,
          userId,
          postsId,
        },
      });
      console.log(data);
      if (fileData.file) {
        const fileUpload = await this.postUploadFile(
          fileData.file,
          fileData.container,
          fileData.tag
        );
        console.log(fileUpload);
      }
    } catch (e) {
      console.log(e);
    }
    const filter = {
      limit,
      order: "DESC",
    };
    try {
      this.setFilterStatic(filter);
      const comments = await this.getFetchStatic(`${endpoint}/${id}/comments`);
      return {
        comments,
      };
    } catch (e) {
      console.log(e);
    }
  }

  async patchProposalAction({ id, action, message, userId }) {
    const data = {
      state: action, // "APPROVED"
      status: true,
      message: message,
      userId: userId,
    };

    try {
      return await this.patchEndpoint(`proposals/${id}/action`, {
        data,
      });
    } catch (e) {
      console.log(e);
    }
  }

  async deleteAnnouncement({ id }) {
    try {
      const response = await this.postEndpoint(
        `announcements/${id}`,
        {},
        "DELETE"
      );
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async patchAnnouncementUpdate(data, id) {
    try {
      return await this.patchEndpoint(`announcements/${id}`, {
        data,
      });
    } catch (e) {
      console.log(e);
    }
  }

  deleteFile({ id, tag, isProposal = false }) {
    return new Promise((resolve, reject) => {
      try {
        const where = {
          tag,
        };

        if (isProposal) {
          where.proposalId = id;
        } else {
          where.postsId = id;
        }

        this.deleteFetchEndpointWithWhere("file-storages", where).then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }
}

export const announcementsServices = new AnnouncementsServices();
