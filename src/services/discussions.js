import { Middleware } from "./middleware";

class DiscussionsServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getDiscussion(ids) {
    try {
      return await this.getFetchNoFilter(
        `discussions/${ids.idPost}?userId=${ids.userId}`
      );
    } catch (e) {
      console.log(e);
    }
  }

  async getAllComments(id) {
    try {
      return await this.getFetchStatic(`posts/${id}/comments`);
    } catch (e) {
      console.log(e);
    }
  }

  async getDiscussions({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    status = "created",
    macros = undefined,
    zones = undefined,
  }) {
    const filter = {
      limit,
      skip,
      order,
      where: {
        like,
        status,
        macros,
        zones,
      },
    };

    this.setFilterStatic(filter);

    try {
      return await this.getFetchStatic(`discussions/`);
    } catch (e) {
      console.log(e);
    }
  }

  async getRelatedPosts({ limit = 3, endpoint, post }) {
    // console.warn("getRelatedPosts service", { endpoint, post });
    const filter = {
      limit,
      where: {
        status: "active",
        tags: post.tags,
      },
    };
    this.setFilterStatic(filter);

    try {
      let result = [];

      if (post?.tags?.length)
        result = await this.getFetchStatic(
          `${endpoint}/${post.postsId}/related`
        );

      const filtered = result.filter((postResult) => postResult.id !== post.id);

      return filtered;
    } catch (e) {
      console.log(e);
    }
  }

  async postComment({ content, userId, postsId, chapterId, proposalId }) {
    const data = {
      content,
      userId,
      postsId,
      chapterId,
      proposalId,
    };
    try {
      return await this.postEndpoint(`comments`, {
        data,
      });
    } catch (e) {
      console.log(e);
    }
  }

  async postReaction(payload) {
    const { postsId, userId, reaction } = payload;
    console.log({ postsId, userId, reaction }, "postReaction SERVICE");
    try {
      return await this.postEndpoint(`posts/${postsId}/react`, {
        data: {
          userId,
          reaction,
        },
      });
    } catch (e) {
      console.log(e);
      throw new Error(e.message);
    }
  }

  async postReactionChapter(payload) {
    const { postsId, userId, reaction } = payload;
    console.log({ postsId, userId, reaction }, "postReactionChapter SERVICE");
    try {
      return await this.postEndpoint(`chapters/${postsId}/react`, {
        data: {
          userId,
          reaction,
        },
      });
    } catch (e) {
      console.log(e);
      throw new Error(e.message);
    }
  }

  async discussionsCount() {
    try {
      return await this.getFetchStatic("discussions/count");
    } catch (e) {
      console.log(e);
    }
  }

  async getFilterDiscussionsAdmin({
    order = undefined,
    content = undefined,
    status = null,
    limit = 6,
    offset = 0,
  }) {
    const filter = {
      order: order,
      limit,
      skip: offset,
      where: {
        like: content,
      },
    };
    if (status !== null) filter.where.status = status;

    try {
      this.setFilterStatic(filter);
      const response = await Promise.all([
        this.getFetchStatic(`discussions/count`),
        this.getFetchStatic(`discussions/`),
      ]);
      return { discussionsCount: response[0].count, discussions: response[1] };
    } catch (e) {
      console.log(e);
    }
  }
}

export const discussionsServices = new DiscussionsServices();
