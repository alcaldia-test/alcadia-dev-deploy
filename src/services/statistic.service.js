import { Middleware } from "./middleware";
import request, { postOptions } from "@utils/request";

export class StatisticServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getReportUrl(body, userId, mod) {
    let url, options;

    if (mod === "n") mod = "normative";
    if (mod === "v") mod = "vote";
    if (mod === "a") mod = "announcement";
    if (mod === "d") mod = "discussion";
    if (mod === "u") mod = "user";

    try {
      const server = process.env.NEXT_PUBLIC_URL_API;
      url = `${server}/statistics/generate-report?userId=${userId}&module=${mod}`;
      options = postOptions(body);

      const excelUrl = await request(url, options);

      return excelUrl;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async postShare({
    socialNetwork = "",
    proposalId = "",
    postsId = "",
    userId = "",
  }) {
    const data = {
      socialNetwork,
      userId,
    };

    if (proposalId) data.proposalId = proposalId;
    if (postsId) data.postsId = postsId;

    try {
      return await this.postEndpoint(`shareds`, {
        data,
      });
    } catch (e) {
      console.log(e);
    }
  }

  async getStatistics({ nid, did, aid, vid, uid, cid, lowDate, highDate }) {
    let endpoint = "";
    const filter = {
      offsetTime: new Date().getTimezoneOffset(),
      where: {
        between: [lowDate, highDate],
      },
    };

    this.setFilterStatic(filter);

    if (nid) endpoint = `/statistics/normatives/${nid}`;
    if (vid) endpoint = `/statistics/voting/${vid}`;
    if (aid) endpoint = `/statistics/announcements/${aid}`;
    if (did) endpoint = `/statistics/discussions/${did}`;
    if (uid) endpoint = `/statistics/users`;
    if (cid) endpoint = `/statistics/complaints`;

    try {
      const data = await this.getFetchStatic(endpoint);
      let points = [];
      let areas = [];

      data?.areas?.forEach(({ location, name, cant, id }) => {
        if (cant > 0) {
          const [lat, lon] = location.split(",");

          points.push({
            id,
            name,
            lat: parseFloat(lat),
            long: parseFloat(lon),
            votes: cant,
          });
        }
      });

      data?.macros?.forEach(({ name }) => areas.push(name));
      data?.zones?.forEach(({ name }) => areas.push(name));

      points = points.length > 0 ? points : undefined;

      let chart;

      if (data?.daysChart) {
        let total = 0;
        let module = "usuarios";

        if (aid) {
          module = "Propuestas";
          data.createdAt = data?.receptionStart;
          data.dueDate = data?.postResults;
        } else if (cid) module = "denuncias";

        const datas = data.daysChart.map(({ day, cant }) => {
          total += cant;

          return {
            name: "Dia " + day,
            [module]: cant,
          };
        });

        const sufix = module === "usuarios" ? "creados" : "creadas";
        chart = {
          title: `${total} ${module} ${sufix}`,
          yAxis: module,
          data: datas,
        };
      }

      if (vid) data.createdAt = data?.startDate;

      if (areas.length === 0 && !nid) areas.push("Todas las zonas");
      if (nid) areas.push(`${data.chapters?.length ?? 0} Capítulos`);

      if (uid || cid) areas = undefined;

      return { ...data, areas, points, chart };
    } catch (e) {
      console.log(e);
    }
  }
}
