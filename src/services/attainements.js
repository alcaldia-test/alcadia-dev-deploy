import { Middleware } from "./middleware";

export class AttainementServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getAttainement(id) {
    try {
      const tagsDic = {
        PUBLISHED: "Activo",
        DISABLED: "Ocultado",
        CREATING: "En Creación",
      };
      const stateDic = {
        PUBLISHED: "active",
        DISABLED: "finished",
        CREATING: "creating",
      };

      const data = await this.getFetchStatic(`attainments/${id}`);

      return {
        ...data,
        state: stateDic[data?.state],
        tag: tagsDic[data?.state],
      };
    } catch (e) {
      console.log(e);
    }
  }

  async getAttainmentCount({ like = undefined, order = undefined, state }) {
    const filter = {
      order,
      state,
      where: {
        like,
      },
    };

    this.setFilterStatic(filter);

    try {
      const data = await this.getFetchStatic(`attainments/count`);

      return data?.count ?? 0;
    } catch (e) {
      console.log(e);
    }
  }

  async getAttainements({
    like,
    limit = 6,
    skip = 0,
    order = "DESC",
    ...others
  }) {
    const filter = {
      limit,
      skip,
      order,
      ...others,
      where: {
        like,
      },
    };

    this.setFilterStatic(filter);

    try {
      const datas = await this.getFetchStatic(`attainments`);
      const tagsDic = {
        PUBLISHED: "Activo",
        DISABLED: "Ocultado",
        CREATING: "En Creación",
      };
      const stateDic = {
        PUBLISHED: "active",
        DISABLED: "finished",
        CREATING: "creating",
      };

      return datas?.map((item) => {
        const id = item.id;
        const image = item?.fileLink ? item?.fileLink : "/images/banner.jpg";
        const title = item?.title || "";

        return {
          id,
          fileLink: image,
          content: item.content,
          postsId: item.postsId,
          title,
          state: stateDic[item?.state],
          tag: tagsDic[item?.state],
        };
      });
    } catch (e) {
      console.log(e);
    }
  }
}
