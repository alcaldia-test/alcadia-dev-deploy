import { Middleware } from "./middleware";

export class NormativesServicess extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getChapters(normativeId) {
    try {
      return await this.getFetchNoFilter(`normatives/${normativeId}`);
    } catch (e) {
      return new Error(e.message);
    }
  }

  async getPostById(id) {
    try {
      const response = await this.getFetchNoFilter(`normatives/${id}`);
      const post = await this.getFetchStatic(`posts/${response.postsId}`);
      return {
        posts: { ...post },
        ...response,
      };
    } catch (e) {
      return e;
    }
  }

  async postNormative(postToUpload) {
    try {
      return await this.postEndpoint(`normatives`, {
        data: postToUpload,
      });
    } catch (e) {
      return new Error(e.message);
    }
  }

  async postChapter(data) {
    try {
      return await this.postEndpoint(`chapters`, {
        data,
      });
    } catch (e) {
      return new Error(e.message);
    }
  }

  async createArticle({ body }) {
    console.log(body);
    try {
      const response = await this.postEndpoint(
        `articles`,
        { data: { ...body } },
        "POST"
      );
      return response;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  deleteNomative(id) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`normatives/${id}`, {}, "DELETE").then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  async deleteChapter(id) {
    try {
      const response = await this.postEndpoint(`chapters/${id}`, {}, "DELETE");
      return response;
    } catch (error) {
      throw new Error(error.message);
    }
  }

  async deleteArticle(id) {
    try {
      const response = await this.postEndpoint(`articles/${id}`, {}, "DELETE");
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async updateNormative({ id, body }) {
    console.log(body);
    try {
      const response = await this.postEndpoint(
        `normatives/${id}`,
        { data: { ...body } },
        "PATCH"
      );
      console.log(response);
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async updateChapter({ id, body }) {
    try {
      const response = await this.postEndpoint(
        `chapters/${id}`,
        { data: { ...body } },
        "PATCH"
      );
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async updateAticle({ id, body }) {
    try {
      const response = await this.postEndpoint(
        `articles/${id}`,
        { data: { ...body } },
        "PATCH"
      );
      return response;
    } catch (e) {
      throw new Error(e.message);
    }
  }

  async enlistNormatives({ limit = 6 }) {
    const filter = {
      order: "DESC",
      limit,
    };

    this.setFilterStatic(filter);
    try {
      const count = await this.getFetchStatic(`normatives/count`);
      const normatives = await this.getFetchStatic(`normatives`);

      return {
        ...count,
        normatives,
      };
    } catch (e) {
      console.log(e);
    }
  }

  async enlistRelatedNormatives({ tags, id }) {
    const filter = {
      where: {
        tags,
      },
    };

    try {
      this.setFilterStatic(filter);
      const response = await this.getFetchStatic(`normative/${id}/related`);
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async getNormative({ id, userId }) {
    try {
      return await this.getFetchNoFilter(`normatives/${id}?userId=${userId}`);
    } catch (error) {
      throw new Error(error.message);
    }
  }

  async getNormativeContend({ id }) {
    try {
      const data = await this.getFetchStatic(`posts/${id}`);
      return data;
    } catch (e) {
      return console.log(e);
    }
  }

  async getNormativeComments({ id, endpoint, limit = 3, userId = "" }) {
    const filter = {
      limit,
      order: "DESC",
    };
    try {
      this.setFilterStatic(filter);
      const comments = await this.getFetchStatic(
        `${endpoint}/${id}/comments?userId=${userId}`
      );
      return {
        comments,
      };
    } catch (e) {
      console.log(e);
    }
  }

  async getMoreNormativeComments({ id, endpoint, limit, userId = "" }) {
    const filter = {
      limit,
      order: "ASC",
    };
    try {
      this.setFilterStatic(filter);
      const comments = await this.getFetchStatic(
        `${endpoint}/${id}/comments?userId=${userId}`
      );
      return {
        comments,
      };
    } catch (e) {
      return e;
    }
  }

  async voteNormative({ id, vote, endpoint, userId }) {
    try {
      const response = await this.postEndpoint(`${endpoint}/${id}/react`, {
        data: { reaction: vote, userId },
      });
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async postComments({
    postsId,
    content,
    userId,
    chapterId = "",
    fileData,
    limit,
    endpoint,
    id,
  }) {
    console.log(
      "service post comment ",
      postsId,
      content,
      userId,
      chapterId,
      fileData
    );
    try {
      const data = await this.postEndpoint(`comments`, {
        data: {
          status: true,
          content,
          userId,
          postsId,
          chapterId,
        },
      });
      console.log(data);
      if (fileData.file) {
        console.log(fileData);
        const img = new FormData();
        img.append(`${data.id}`, fileData.file);
        const fileUpload = await this.postUploadFile(
          img,
          fileData.container,
          fileData.tag
        );
        console.log(fileUpload);
      }
    } catch (e) {
      console.log(e);
    }
    const filter = {
      limit,
      order: "DESC",
    };
    try {
      this.setFilterStatic(filter);
      const comments = await this.getFetchStatic(`${endpoint}/${id}/comments`);
      return {
        comments,
      };
    } catch (e) {
      console.log(e);
    }
  }

  async postAnswer({ commentId, content, userId, fileData }) {
    console.log("service post comment ", commentId, content, userId, fileData);
    try {
      const data = await this.postEndpoint(`comments/${commentId}/answer`, {
        data: {
          commentId,
          content,
          userId,
        },
      });
      console.log(data);
      if (fileData.file) {
        console.log(fileData);
        const img = new FormData();
        img.append(`${data.id}`, fileData.file);
        const fileUpload = await this.postUploadFile(
          img,
          "answer",
          fileData.tag
        );
        console.log(fileUpload);
      }
    } catch (e) {
      return e;
    }
    try {
      const filter = {
        order: "DESC",
      };
      this.setFilterStatic(filter);
      const response = await this.getFetchStatic(
        `comments/${commentId}/answers`
      );
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async getFilterNormatives({
    order = undefined,
    content = undefined,
    limit = 6,
    offset = 0,
    state = undefined,
  }) {
    let count;
    try {
      const filt = {
        module: "normative",
        where: {
          like: content,
        },
      };
      if (state) filt.state = state;

      this.setFilterStatic(filt);
      count = await this.getFetchStatic(`normatives/count`);
    } catch (e) {
      console.log(e);
    }

    try {
      const filter = {
        order: order,
        limit,
        skip: offset,
        state: "PUBLISHED",
        where: {
          like: content,
        },
      };
      if (state) filter.state = state;
      this.setFilterStatic(filter);

      const response = await this.getFetchStatic(`normatives`);
      return {
        normatives: [...response],
        count: count.count,
      };
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getFilterNormativesAdmin({
    order = undefined,
    content = undefined,
    state = null,
    limit = 6,
    offset = 0,
  }) {
    const filter = {
      order: order,
      limit,
      skip: offset,
      where: {
        like: content,
      },
    };
    if (state !== null) filter.state = state;

    try {
      this.setFilterStatic(filter);
      const response = await Promise.all([
        this.getFetchStatic(`normatives/count`),
        this.getFetchStatic(`normatives/`),
      ]);
      return {
        normativesCount: response[0].count,
        normatives: response[1],
      };
    } catch (e) {
      console.log(e);
    }
  }

  async getAnswer({ id, endpoint, userId = "" }) {
    try {
      const response = await this.getFetchStatic(
        `${endpoint}/${id}/answers?userId=${userId}`
      );
      return response;
    } catch (e) {
      return e;
    }
  }

  async complaintComment({ commentId, answerId, type }) {
    try {
      const response = await this.postEndpoint(`complaints`, {
        data: {
          answerId,
          commentId,
          type,
        },
      });
      console.log(response);
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async getComplaints({ id, endpoint, limit = 4, offset = 0 }) {
    const filter = {
      skip: offset,
      limit,
    };
    try {
      this.setFilterEndpoint(filter);
      const response = await this.getFetchStatic(
        `${endpoint}/${id}/complaints`
      );
      return response;
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async deleteComplaint({ type, id, reviewerId, message }) {
    try {
      const { user } = await this.getFetchNoFilter(`${type}/${id}`);

      const response = await this.postEndpoint(`complaints/delete-comment`, {
        data: {
          userId: user.id,
          commentId: type === "comments" ? id : "",
          answerId: type === "comments" ? "" : id,
          message,
          reviewerId,
        },
      });
      return response;
    } catch (e) {
      console.log(e);
    }
  }

  async blockUser({ type, id, reviewerId, message }) {
    try {
      const { user } = await this.getFetchNoFilter(`${type}/${id}`);

      const response = await this.postEndpoint(`complaints/delete-comment`, {
        data: {
          userid: user.id,
          commentId: type === "comments" ? id : "",
          answerId: type === "comments" ? "" : id,
          message,
          reviewerId,
        },
      });
      return response;
    } catch (e) {
      console.log(e);
    }
  }
}
