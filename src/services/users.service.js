import request, { getOptions } from "@utils/request";
import { Middleware } from "./middleware";

export class UsersServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getSingleUser(id) {
    const server = process.env.NEXT_PUBLIC_URL_API;
    try {
      const options = getOptions();
      const data = await request(`${server}/users/${id}`, options);
      const zones = [];

      const tagDic = {
        HOME: "Por domicilio",
        FAMILY: "Por familiar",
        BUSINESS: "Por negocio",
      };

      if (data.favorites) {
        data.favorites.forEach(({ tag, zone, macro }) => {
          let index;
          if (zone)
            index = zones.push({
              name: zone.name,
              tag: tagDic[tag],
            });
          if (macro)
            if (index) zones[index - 1].name += ` (${macro.name})`;
            else
              zones.push({
                name: macro.name,
                tag: tagDic[tag],
              });
        });
      }
      const defaultAvatar =
        "https://robohash.org/amhvbWFuaWRldiU0MGdtYWlsLmNvbQ==.png?size=150x150";

      return {
        ...data,
        name: `${data.firstName} ${data.paternalLastName ?? ""}`.toLowerCase(),
        zones,
        avatar: data.avatar ?? defaultAvatar,
      };
    } catch (e) {
      console.log(e);
    }
  }

  async getUserProfile(id) {
    const server = process.env.NEXT_PUBLIC_URL_API;
    try {
      const options = getOptions();
      const data = await request(`${server}/users/${id}`, options);
      const zones = [];

      const tagDic = {
        HOME: "Domicilio",
        FAMILY: "Familiares cercanos",
        BUSINESS: "Negocio",
      };

      if (data.favorites) {
        data.favorites.forEach(({ tag, macro }) => {
          zones.push({
            name: macro.name,
            tag: tagDic[tag],
          });
        });
      }
      const defaultAvatar =
        "https://robohash.org/amhvbWFuaWRldiU0MGdtYWlsLmNvbQ==.png?size=150x150";

      return {
        ...data,
        name: `${data.firstName ?? ""} ${
          data.paternalLastName ?? ""
        }`.toLowerCase(),
        zones,
        avatar: data.avatar ?? defaultAvatar,
      };
    } catch (e) {
      console.log(e);
    }
  }

  async getUsersCount({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    enable = 0,
  }) {
    const filter = {
      limit,
      skip,
      order,
      where: {
        like,
        enable: enable === 0,
      },
    };

    this.setFilterStatic(filter);

    try {
      const data = await this.getFetchStatic(`users/count`);

      return data?.count ?? 0;
    } catch (e) {
      console.log(e);
    }
  }

  async getUserAvatar(id) {
    const filter = {
      where: {
        userId: id,
        tag: "avatar",
      },
    };

    try {
      this.setFilterStatic(filter);

      const data = await this.getFetchStatic("file-storages");

      return data;
    } catch (e) {
      console.log(e);
    }
  }

  async getUsers({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    enable = 0,
  }) {
    const filter = {
      limit,
      skip,
      order,
      where: {
        like,
        enable: enable === 0,
      },
    };

    this.setFilterStatic(filter);

    try {
      const data = (await this.getFetchStatic(`users`)) ?? [];

      const defaultAvatar =
        "https://robohash.org/amhvbWFuaWRldiU0MGdtYWlsLmNvbQ==.png?size=150x150";

      return data.map(
        (
          { avatar, firstName, paternalLastName, complaints, ...others },
          i
        ) => ({
          ...others,
          key: i,
          complaints: complaints ?? 0,
          user: {
            name: `${firstName} ${paternalLastName ?? ""}`.toLowerCase(),
            avatar: avatar ?? defaultAvatar,
          },
        })
      );
    } catch (e) {
      console.log(e);
    }
  }
}
