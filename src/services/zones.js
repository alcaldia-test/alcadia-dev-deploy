import { Middleware } from "./middleware";

export class ZonesServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  getZones() {
    return new Promise((resolve, reject) => {
      try {
        this.getFetchStatic(`zones`).then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  }

  getMacroZones(id = undefined) {
    return new Promise((resolve, reject) => {
      try {
        const filter = {
          include: [
            {
              relation: "zones",
            },
          ],
        };
        this.setFilterStatic(filter);

        if (id) {
          this.getFetchStatic(`macros/${id}`).then((x) => {
            resolve(x);
          });
        } else {
          this.getFetchStatic(`macros`).then((x) => {
            resolve(x);
          });
        }
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  }
}
