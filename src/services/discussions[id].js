import { Middleware } from "./middleware";

class DiscussionsId extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getDiscussion({ id }) {
    try {
      const response = await this.getFetchStatic(`discussions/${id}`);
      const post = await this.getFetchStatic(`posts/${response.postsId}`);
      return {
        posts: { ...post },
        ...response,
      };
    } catch (e) {
      return e;
    }
  }
}
export const discussionsIdServices = new DiscussionsId();
