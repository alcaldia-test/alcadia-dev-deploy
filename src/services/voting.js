import { Middleware } from "./middleware";

export class VotingServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getTotalVotes() {
    try {
      return await this.getFetchNoFilter("/voting/count");
    } catch (e) {
      console.log(e);
    }
  }

  async getCountVotes() {
    try {
      return await this.getFetchStatic("voting/count");
    } catch (e) {
      console.log(e);
    }
  }

  async getVote({ id = undefined, userId = undefined }) {
    try {
      if (userId) {
        return await this.getFetchStaticWithUserId(
          `voting/${id}?userId=${userId}`
        );
      }
      return await this.getFetchStatic(`voting/${id}`);
    } catch (e) {
      console.log(e);
    }
  }

  async postVote({ optionId = undefined, id = undefined, userId = undefined }) {
    await this.postEndpointWithoutBody(`vote/${optionId}`);

    return await this.getFetchStaticWithUserId(`voting/${id}?userId=${userId}`);
  }

  async patchUserZone({
    favoriteId = undefined,
    data = undefined,
    votingId = undefined,
    userId = undefined,
  }) {
    try {
      await this.postEndpoint(`favorites/${favoriteId}`, { data }, "PATCH");

      return await this.getFetchStaticWithUserId(
        `voting/${votingId}?userId=${userId}`
      );
    } catch (e) {
      console.log("error en el patch macro/zone", e);
    }
  }

  async postUserZone({
    data = undefined,
    votingId = undefined,
    userId = undefined,
  }) {
    try {
      await this.postEndpoint(`favorites`, { data }, "POST");

      return await this.getFetchStaticWithUserId(
        `voting/${votingId}?userId=${userId}`
      );
    } catch (e) {
      console.log("error en el post macro/zone", e);
    }
  }

  async getFavorites(userId = undefined) {
    const filter = {
      where: {
        userId,
      },
      include: [
        {
          relation: "zone",
        },
      ],
    };

    if (!userId) return [];

    this.setFilterStatic(filter);

    try {
      return await this.getFetchStatic(`favorites`);
    } catch (e) {
      console.log("error en el get favorites", e);
    }
  }

  async getZonesOfMacro(macroId = undefined) {
    const filter = {
      include: [
        {
          relation: "zones",
        },
      ],
    };

    this.setFilterStatic(filter);

    try {
      return await this.getFetchStatic(`macros/${macroId}`);
    } catch (e) {
      console.log("error en el get zones of macro", e);
    }
  }

  async getStatistics(id = undefined) {
    try {
      return await this.getFetchStatic(`statistics/voting/${id}/`);
    } catch (e) {
      console.log("error en el get statistics", e);
    }
  }

  // Multiple requests
  async getVotes({
    limit = 10,
    skip = 0,
    like = undefined,
    order = "DESC",
    status = "created",
    macros = undefined,
    zones = undefined,
  }) {
    try {
      const filter = {
        limit,
        skip,
        order,
        where: {
          like,
          status,
          macros,
          zones,
        },
      };

      if (zones || macros) {
        filter.status = "created";
        filter.where.and = [
          {
            macros: [],
          },
          {
            zones: [],
          },
        ];
      }

      this.setFilterStatic(filter);

      return await this.getFetchStatic("voting");
    } catch (e) {
      console.log(e);
    }
  }
}

export const votingServices = new VotingServices();
