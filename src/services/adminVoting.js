import { Middleware } from "./middleware";

export class AdminVotingServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  getVotingListOptions({
    limit = 9,
    page = 0,
    order = "ASC",
    like = "",
    macros = [],
    completed = null,
  }) {
    return new Promise((resolve, reject) => {
      try {
        const _skip = page * limit - limit;
        const filter = {
          limit,
          skip: _skip > 0 ? _skip : 0,
          order,
          completed,
          where: {
            like,
            //  "zones":[],
            //  "macros":[1],
            //   "status":"", //active , finished supporting review
          },
        };

        if (macros.length > 0) {
          filter.where.macros = macros;
        }

        if (completed !== null) {
          filter.where.status = completed;
        }

        this.setFilterStatic(filter);
        this.getFetchStatic(`voting`).then((x) => {
          resolve(x);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  getTotalPost({
    limit = 9,
    page = 0,
    order = "ASC",
    like = "",
    macros = [],
    completed = null,
  }) {
    return new Promise((resolve, reject) => {
      try {
        const _skip = page * limit - limit;
        const filter = {
          limit,
          skip: _skip > 0 ? _skip : 0,
          order,
          where: {
            like,
            //  "zones":[],
            //  "macros":[1],
            //   "status":"", //active , finished supporting review
          },
        };

        if (macros.length > 0) {
          filter.where.macros = macros;
        }

        if (completed !== null) {
          filter.where.status = completed;
        }

        this.setFilterStatic(filter);
        this.getFetchStatic(`voting/count`).then((x) => {
          resolve(x);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  getPostById(idPost) {
    return new Promise((resolve, reject) => {
      try {
        this.getFetchStatic(`voting/${idPost}`).then((x) => {
          resolve(x);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  createPostUploadData(data) {
    // console.log({ ubicación: "creating", data });
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`voting`, { data }).then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  createOptionToProposal(data) {
    return new Promise((resolve, reject) => {
      // console.log({ location: "add options to vote", data });
      this.postEndpoint("proposals", { data })
        .then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        })
        .catch((error) => {
          console.log("error en el patch endpoint");
          reject(error);
        });
    });
  }

  updateOptionToProposal(data, id) {
    return new Promise((resolve, reject) => {
      // console.log({ location: "update options to vote", data, id });
      this.postEndpoint(`proposals/${id}`, { data }, "PATCH")
        .then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        })
        .catch((error) => {
          console.log("error en el patch endpoint");
          reject(error);
        });
    });
  }

  updatePostData(data, id) {
    return new Promise((resolve, reject) => {
      console.log({ location: "esta en update", data, id });

      try {
        this.postEndpoint(`voting/${id}`, { data }, "PATCH").then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        });
      } catch (error) {
        console.log("error en el patch endpoint");
        reject(error);
      }
    });
  }

  createPostUploadFiles({ id, file, container, tag }) {
    return new Promise((resolve, reject) => {
      try {
        const formData = new FormData();
        formData.append(`${id}`, file);
        this.postUploadFile(formData, container, tag).then((x) => {
          resolve(x);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`voting/${id}`, {}, "DELETE").then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  deleteFile({ id, tag, isProposal = false }) {
    return new Promise((resolve, reject) => {
      try {
        const where = {
          tag,
        };

        if (isProposal) {
          where.proposalId = id;
        } else {
          where.postsId = id;
        }

        this.deleteFetchEndpointWithWhere("file-storages", where).then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  deleteProposalInServer(id) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`proposals/${id}`, {}, "DELETE").then((x) => {
          resolve(x);
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  createPostUploadProposal(data) {
    return new Promise((resolve, reject) => {
      try {
        this.postEndpoint(`proposals`, { data }).then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        });
      } catch (error) {
        console.log("error en el post endpoint");
        reject(error);
      }
    });
  }

  updateProposalData(data, id) {
    return new Promise((resolve, reject) => {
      // console.log({ location: "esta en update", data, id });

      try {
        this.postEndpoint(`proposals/${id}`, { data }, "PATCH").then((x) => {
          if (x.length === 0) {
            reject(new Error());
          } else {
            resolve(x);
          }
        });
      } catch (error) {
        console.log("error en el patch endpoint");
        reject(error);
      }
    });
  }
}
