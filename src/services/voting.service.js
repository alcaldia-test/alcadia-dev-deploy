/* eslint-disable lines-between-class-members */
import { Middleware } from "./middleware";

class VotingClientService extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getRelatedContent({ tags = [], id = "" }) {
    const filter = {
      where: {
        tags,
      },
    };
    // this.setFilterEndpoint(filter);
    this.setFilterStatic(filter);
    try {
      return await this.getFetchEndpoint(`voting/${id}/related`);
    } catch (e) {
      console.log(e);
    }
  }

  async getVoting({
    limit = 10,
    skip = 0,
    like = undefined,
    order = undefined,
    status = undefined,
    macros = undefined,
    zones = undefined,
    id = undefined,
  }) {
    const filter = {
      limit,
      skip,
      order, // ASC | DESC
      where: {
        like,
        status,
        macros,
        zones,
      },
    };
    this.setFilterEndpoint(filter);
    // this.setFilterStatic(filter);
    try {
      //    this.setFilterEndpoint(filter);
      //  return await this.getFetchEndpoint(`posts`);
      return await this.getFetchEndpoint(`voting`);
    } catch (e) {
      console.log(e);
    }
  }
}
export const votingService = new VotingClientService();
