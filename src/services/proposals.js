import { Middleware } from "./middleware";

export class ProposalsServices extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getProposals(pageNumber) {
    const filter = {
      include: [
        {
          relation: "post",
          scope: {
            fields: {
              id: true,
              title: true,
            },
          },
        },
      ],
    };
    try {
      return await this.getFetchEndpoint(`announcements`, filter);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getTotalPosts() {
    try {
      return await this.getFetchEndpoint(`announcements/count`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
