import { Middleware } from "./middleware";

export class ComplaitService extends Middleware {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  async getCount({ limit = 10, skip = 0, complaintType, ...others }) {
    const [complaintTypeF, took] = complaintType.split(".");

    const filter = {
      limit,
      skip,
      complaintType: complaintTypeF,
      took,
      ...others,
    };

    this.setFilterStatic(filter);

    try {
      const data = await this.getFetchStatic(`complaints/count`);

      return data?.count ?? 0;
    } catch (e) {
      console.log(e);
    }
  }

  async getSingleComment({ type, id }) {
    try {
      const data = (await this.getFetchStatic(`${type}s/${id}`)) ?? {};

      return { ...data };
    } catch (e) {
      console.log(e);
    }
  }

  async getCommentComplaints({ limit = 10, skip = 0, id, model }) {
    const filter = {
      limit,
      skip,
    };

    this.setFilterStatic(filter);

    try {
      const datas =
        (await this.getFetchStatic(`${model}s/${id}/complaints`)) ?? [];

      return datas;
    } catch (e) {
      console.log(e);
    }
  }

  async getDatas({
    limit = 10,
    skip = 0,
    complaintType = "comment",
    ...others
  }) {
    const [complaintTypeFormated, took] = complaintType.split(".");

    const filter = {
      limit,
      skip,
      complaintType: complaintTypeFormated,
      took,
      ...others,
    };

    this.setFilterStatic(filter);

    try {
      const data = (await this.getFetchStatic(`complaints`)) ?? [];

      const defaultAvatar =
        "https://robohash.org/amhvbWFuaWRldiU0MGdtYWlsLmNvbQ==.png?size=150x150";

      return data.map(
        (
          { avatar, firstname, paternallastname, reviewerid, ...others },
          i
        ) => ({
          ...others,
          key: i,
          reviewed: reviewerid !== null,
          type:
            complaintTypeFormated === "comment" ? "Comentario" : "Respuesta",
          user: {
            name: `${firstname} ${paternallastname ?? ""}`,
            avatar: avatar ?? defaultAvatar,
          },
        })
      );
    } catch (e) {
      console.log(e);
    }
  }
}
