export const textCapitalize = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
};

export const slicePhrase = (string, maxLength) => {
  const trimmedString = string.substr(0, maxLength);
  const trimedBySpace = trimmedString.substr(0, trimmedString.lastIndexOf(" "));

  return string.length === trimmedString.length
    ? string
    : trimedBySpace + " ...";
};
