const allowCustom = (canEdit = false, canCreate = false, canDelete = false) => {
  return {
    canEdit,
    canCreate,
    canDelete,
  };
};

export const checkPermision = (dataUser, moduleName) => {
  try {
    const isAdmin = dataUser.permissions.filter((x) => x.module === ".*");
    if (isAdmin.length > 0) {
      return allowCustom(true, true, true);
    }
    const modulePermission = dataUser.permissions.filter(
      (x) => x.module === moduleName
    );
    if (modulePermission) {
      const canEdit =
        modulePermission.filter((x) => x.action === "PATCH")?.length > 0;
      const canCreate =
        modulePermission.filter((x) => x.action === "POST")?.length > 0;
      const canDelete =
        modulePermission.filter((x) => x.action === "DELETE")?.length > 0;
      return allowCustom(canEdit, canCreate, canDelete);
    }
    return allowCustom();
  } catch (error) {}
};
