import draftToHtml from "draftjs-to-html";

export const parseDraft = (content) => {
  try {
    return draftToHtml(JSON.parse(content));
  } catch (error) {
    return draftToHtml(content);
  }
};

export const parseDraftToText = (content) => {
  try {
    const blocks = JSON.parse(content).blocks;
    return blocks.map((block) => block.text).join(" ");
  } catch (error) {
    return content;
  }
};
