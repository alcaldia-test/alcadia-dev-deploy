import { getOrCreateStore } from "@utils/with-redux-store";
import { message } from "antd";
import { signOutStart } from "@redux/auth/actions";

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  try {
    if (response?.status === 403) getOrCreateStore().dispatch(signOutStart());

    return response?.status === 204 ? "ok" : response?.json();
  } catch (error) {
    return "";
  }
}

function checkStatus(response) {
  if (response?.status >= 200 && response?.status < 300) {
    return response;
  } else {
    const error = new Error(response.status);
    error.response = response;
    throw error;
  }
  // const error = new Error(response.status);
  // error.response = response;
  // throw error;
  // throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default async function request(url, options) {
  const requestHeader = await fetch(url, options);
  checkStatus(requestHeader);

  return parseJSON(requestHeader);
}

export async function getWorldTime() {
  const datas = await request(
    "https://worldtimeapi.org/api/timezone/america/la_paz"
  );

  return datas.utc_datetime;
}

export function postOptionsFormData(body = {}, method = "POST", headers = {}) {
  const { auth } = getOrCreateStore().getState();
  return {
    method,
    headers: {
      ...headers,
      Authorization: `Bearer ${auth.tokenUser}`,
    },
    body,
  };
}

export function postOptionsWithoutToken(
  body = {},
  method = "POST",
  headers = {}
) {
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };
}

export function getOptions(method = "GET", headers = {}) {
  const { auth } = getOrCreateStore().getState();
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${auth.tokenUser}`,
    },
  };
}

export function getOptionsWithToken(token = "", method = "GET", headers = {}) {
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  };
}

export function getOptionsWithoutToken(method = "GET", headers = {}) {
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };
}

export function postOptions(body = {}, method = "POST", headers = {}) {
  const { auth } = getOrCreateStore().getState();
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${auth.tokenUser}`,
    },
    body: JSON.stringify(body),
  };
}

export function putOptionsWithoutToken(
  body = {},
  method = "PUT",
  headers = {}
) {
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };
}

export function putOptions(body = {}, method = "PUT", headers = {}) {
  const { auth } = getOrCreateStore().getState();
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${auth.tokenUser}`,
    },
    body: JSON.stringify(body),
  };
}

export function patchOptions(body = {}, method = "PATCH", headers = {}) {
  const { auth } = getOrCreateStore().getState();
  return {
    method,
    headers: {
      ...headers,
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${auth.tokenUser}`,
    },
    body: JSON.stringify(body),
  };
}

export function deleteOptions(body, method = "DELETE", headers = {}) {
  const { auth } = getOrCreateStore().getState();
  return {
    method,
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${auth.tokenUser}`,
    },
    body: JSON.stringify(body),
  };
}

export async function showMessageError(err) {
  if (typeof err === "string") {
    console.log(err);
    message.error(err);
    return;
  }
  if (err.response) {
    const errResp = await err.response.json();
    if (errResp.error) {
      switch (errResp.error.statusCode) {
        case 422:
          message.error(errResp.error.details[0].message);
          return;
        case 500:
          message.error(errResp.error.message);
          return;
        default:
          if (errResp.error.message) {
            message.error(errResp.error.message);
            return;
          }
          message.error(err.message);
      }
    }
  }
}
