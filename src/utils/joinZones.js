export const useJoinByComma = (zones) => {
  if (zones.length > 0) {
    const zonesCapitalized = zones.map((zone) => {
      const words = zone
        .split(" ")
        .map((word) => {
          return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
        })
        .join(" ");
      return words;
    });

    if (zonesCapitalized.length === 1) return zonesCapitalized[0];
    if (zonesCapitalized.length === 2)
      return `${zonesCapitalized[0]} y ${zonesCapitalized[1]}`;
    if (zonesCapitalized.length > 2)
      return `${zonesCapitalized[0]}, ${zonesCapitalized[1]}, ${zonesCapitalized[2]}...`;
  } else {
    return "Todas las zonas";
  }
};
