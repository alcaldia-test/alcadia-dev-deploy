/**
 * Esto valida si el usuario no está logeado y si el usuario
 * no pertenece a ninguna zona o macrozona
 */

export const validateIfUserIsInZone = ({
  favorites = [],
  zones = [],
  macros = [],
}) => {
  if (!zones?.length > 0 && !macros?.length > 0) {
    return true;
  } else if (favorites?.length > 0 && zones?.length > 0) {
    return zones.some((zone) =>
      favorites.some((favorite) => favorite.zoneId === zone.id)
    );
  } else if (favorites?.length > 0 && macros?.length > 0) {
    return macros.some((macro) =>
      favorites.some((favorite) => favorite?.macroId === macro.id)
    );
  } else {
    return false;
  }
};

/**
 *
 * @param {*} zonesId Objeto que en value trae un array de macrozonas, el cual a su vez tiene zonas
 * @param {*} favorites Zonas que el usuario registró
 * @returns Booleano para verificar si favorites está incluido en zonesId
 */

export const getNearly = (zonesId, favorites) => {
  const macros = zonesId?.value ? zonesId.value : [];
  const zones = zonesId?.value
    ? zonesId?.value
        .map((macro) => macro.selected)
        .reduce((a, b) => a.concat(b))
    : [];
  return validateIfUserIsInZone({
    favorites,
    zones,
    macros,
  });
};
