const baseSideBar = [];

const otherPermissions = [
  {
    label: "Convocatorias a Propuestas",
    path: "/admin/convocatorias",
    key: "announcements",
  },
  {
    label: "Normativa Colaborativa",
    path: "/admin/normativas",
    key: "normatives",
  },
  {
    label: "Tu voto cuenta (Votaciones)",
    path: "/admin/votaciones",
    key: "voting",
  },
  {
    label: "Charlas Virtuales",
    path: "/admin/charlas",
    key: "discussions",
  },
  {
    label: "Denuncias",
    path: "/admin/denuncias",
    key: "complaints",
  },
  {
    label: "Solicitudes de Acceso",
    path: "/admin/solicitudes",
    key: "access-controls",
  },
  [
    {
      label: "Usuarios",
      key: "users",
    },
    {
      label: "Registrados",
      path: "/admin/usuarios/registrados",
      key: "registered",
    },
    {
      label: "Bloqueados",
      path: "/admin/usuarios/bloqueados",
      key: "locked",
    },
  ],
  [
    {
      label: "Editar Sitio",
      key: "sites",
    },
    {
      label: "Editar Contenido Web",
      path: "/admin/config/logos",
      key: "settings",
    },
    {
      label: "Logros",
      path: "/admin/config/logros",
      key: "attainments",
    },
  ],
];

export const getUserSidebar = (permissions) => {
  let resp = baseSideBar;
  const sites = ["settings", "attainments", "noticies"];
  const toContact = [];

  const existModule = (module) => {
    const found = toContact.find((item) => {
      if (item instanceof Array) {
        const found = item.find((j) => j.key === module);

        return found !== undefined;
      } else return item.key === module;
    });

    return found !== undefined;
  };

  permissions.forEach(({ module }) => {
    if (module === ".*") resp = resp.concat(otherPermissions);
    else {
      if (module === "announcements" && !existModule("announcements"))
        toContact.unshift(otherPermissions[0]);
      if (module === "normatives" && !existModule("normatives"))
        toContact.unshift(otherPermissions[1]);
      if (module === "discussions" && !existModule("discussions"))
        toContact.unshift(otherPermissions[3]);
      if (module === "voting" && !existModule("voting"))
        toContact.unshift(otherPermissions[2]);
      if (module === "complaints") toContact.push(otherPermissions[4]);
      if (module === "access-controls") toContact.push(otherPermissions[5]);
      if (module === "users") toContact.push(otherPermissions[6]);
      if (sites.includes(module)) {
        let len = -1;

        toContact.forEach((item, i) => {
          if (item instanceof Array) if (item[0].key === "sites") len = i;
        });

        if (len === -1) len = toContact.push([otherPermissions[7][0]]);

        if (module === "settings" && !existModule("settings"))
          toContact[len - 1].push(otherPermissions[7][1]);
        if (module === "attainments" && !existModule("attainments"))
          toContact[len - 1].push(otherPermissions[7][2]);
      }
    }
  });

  return resp.concat(toContact);
};

export const allowedPath = (permissions) => {
  const paths = [];
  const routes = getUserSidebar(permissions);

  routes.forEach((item) => {
    if (item instanceof Array) {
      const onlyItems = item.slice(1);

      onlyItems.forEach(({ path }) => paths.push(path));
    } else paths.push(item.path);
  });

  permissions.forEach(({ module }) => {
    if (["statistics", ".*"].includes(module))
      paths.push("/admin/estadisticas");
  });

  paths.push("/admin/perfil");
  return paths;
};
