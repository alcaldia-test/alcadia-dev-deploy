export const base64ToFile = (base64, filename = "banner.png") => {
  const arr = base64.split(",");
  const mine = base64.match(/:(.*?);/);

  const buf = Buffer.from(arr[1], "base64");

  return new File([buf.buffer], filename, {
    type: mine,
  });
};
