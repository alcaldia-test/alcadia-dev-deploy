export const randomLetter = (size = 10) => {
  let result = "";

  const enableChars = "qwertyuioplkjhgfdsazxcvbnmASDFGHJKLQWERTYUIOPMNBVCXZ";
  const length = enableChars.length;

  for (let i = 0; i < size; i++) {
    const index = Math.floor(Math.random() * length);
    result += enableChars[index];
  }

  return result;
};
