const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImMzNGU0MmZhLTk4OGItNGI5Ni05YzU2LTRmYTIwNmIzNjQ3ZCIsIm5hbWUiOiJzdXBlcm9vdCIsImVtYWlsIjoiYWRtaW4uZ2FtbHBAZ2FtbHAuY29tIiwiaWF0IjoxNjQwNjcyNDczLCJleHAiOjE2NjIyNzI0NzN9.IdhqlD4yy5aOhX1o4Iz6-1sjJd5zsRrJW_GnPcLCV8I";

const url = process.env.NEXT_PUBLIC_URL_API;

export const fetchCustom = ({
  filter = null,
  method = "GET",
  body = {},
  endpoint = "",
  tagInUri = "filter",
}) => {
  return new Promise((resolve, reject) => {
    try {
      let _url = `${url}/${endpoint}`;

      if (filter !== null) {
        const encodedFilter = encodeURIComponent(JSON.stringify(filter));
        _url = `${_url}?${tagInUri}=${encodedFilter}`;
      }
      if (method === "GET") {
        fetch(_url, {
          method: method,
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }).then(async (res) => {
          let response = res;
          try {
            response = await res.json();
          } catch (error) {}
          resolve(response);
        });
      } else {
        fetch(_url, {
          method: method,
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(body),
        }).then(async (res) => {
          let response = res;
          try {
            response = await res.json();
          } catch (error) {}
          resolve(response);
        });
      }
    } catch (error) {
      console.log(error);
      // eslint-disable-next-line prefer-promise-reject-errors
      reject({});
    }
  });
};

export const fetchFiles = ({ file, tag = "", container = "", id = "" }) => {
  return new Promise((resolve, reject) => {
    try {
      const _url = `${url}/containers/${container}/upload/${tag}`;
      const formData = new FormData();
      formData.append(`${id}`, file);
      const request = new XMLHttpRequest();
      request.open("POST", _url);
      request.onloadend = () => {
        const response = JSON.parse(request.response);
        resolve(response);
      };
      request.send(formData);
    } catch (error) {
      console.log(error);
      // eslint-disable-next-line prefer-promise-reject-errors
      reject(false);
    }
  });
};
