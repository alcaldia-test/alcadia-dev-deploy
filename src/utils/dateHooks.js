import moment from "moment";

// Return the date in format DD/MM/YYYY
export const useFormateDate = (date) => {
  moment.locale("es");
  return moment(date).format("DD MM YYYY").split(" ").join("/");
};

export const useDateIsBetween = (start, end) => {
  const now = moment();
  const startDate = moment(start);
  const endDate = moment(end);
  return now.isBetween(startDate, endDate);
};

export const useDateIsAfter = (date, currentDate) => {
  const now = moment(currentDate);
  const dateToCompare = moment(date);

  return now.isAfter(dateToCompare);
};

export const useDateIsBefore = (date) => {
  const now = moment();
  const dateToCompare = moment(date);

  return now.isBefore(dateToCompare);
};

export const useDateTocards = (dueDateAsString) => {
  const dueDate = moment(dueDateAsString);
  const now = moment();
  const _diffDays = dueDate.diff(now, "days");
  return _diffDays < 0 ? 0 : _diffDays;
};

// Return the date in format DD/MM/YYYY Not Hours
export const useFormateStringDate = (date) => {
  return date.split("T")[0];
};
