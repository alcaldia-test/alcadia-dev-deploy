import moment from "moment";
const getBlocksText = (e) => {
  return e.blocks.reduce((a, b) => ({ text: `${a.text} ${b.text}` }), {
    text: "",
  }).text;
};
const validateContent = (content) =>
  content === undefined ||
  getBlocksText(content).length < 100 ||
  getBlocksText(content).length > 6000;

export const checkErrorFields = (field, value) => {
  switch (field) {
    case "title":
      return value === undefined || value.length < 1 || value.length > 100;
    case "content":
      return value === undefined || validateContent(value);
    case "dueDate":
      return value === undefined || moment() > moment(new Date(value));
    case "tags":
      return value === undefined || value.length === 0;
    case "chapters":
      return value === undefined || value.length === 0; // || value.map()

    default:
      return false;
  }
};

export const setDataDispatch = (dispatch, action, name, value) => {
  dispatch(
    action({
      name,
      value,
    })
  );
};
