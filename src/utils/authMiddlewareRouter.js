import redirectTo from "./redirectTo";
import { allowedPath } from "./generateSidebar";

// TODO: Aquí se coloca todas las rutas que sean disponibles para todos.
export const availableToAll = [
  "/register-datas",
  "/verification",
  "/verification/zones",
  "/verification/success-zone",
  "/charlas/crear",
  "/normativas",
  "/charlas",
  "/charlas/[id]",
  "/logro/[id]",
  "/normativas/[id]",
  "/recovery-password",
  "/recovery-password/code",
  "/recovery-password/reset-password",
  "/recovery-password-admin",
  "/recovery-password-admin/code",
  "/recovery-password-admin/reset-password",
  "/charlas",
  "/charlas[id]",
  "/propuestas_ciudadanas",
  "/propuestas_ciudadanas/[announcementId]",
  "/propuestas_ciudadanas/[announcementId]/crear",
  "/propuesta/[id]",
  "/usuario/[userId]",
  "/_error",
  "/404",
  "/",
  "/tu_voto_cuenta",
  "/tu_voto_cuenta/[id]",
  "/tu_voto_cuenta/[id]/estadisticas",
  "/convocatorias",
  "/convocatorias/crear",
  "/convocatorias/[announcementId]/estadisticas",
  "/convocatorias/[announcementId]",
  "/convocatorias/[announcementId]/propuestas/[tab]",
  "/convocatorias/[announcementId]/propuesta/[id]",
  "/registro-admin/permission",
  "/registro-admin/permissionSuccess",
  "/registro-email",
  "/registro-terminos",
];

let adminPaths;

// TODO: Todas las rutas que no requieren petición a backend
export const noRequireRequest = [
  "/registro",
  "/registro-admin",
  "/ingreso",
  "/login",
  "/logout",
  "/_error",
  "/404",
  "/normativas",
  "/prueba",
  "/normativas/numero",
  "/normativas/[id]",
];

const AuthMiddlewareRouter = (ctx) => {
  const { pathname, reduxStore: store } = ctx;

  const {
    tokenUser: token,
    dataUser: { roleMapping, permissions },
  } = store.getState().auth;

  const auth = () => {
    //eslint-disable-line
    if (!token) {
      redirectTo("/login", { res: ctx.res, status: 301 });
    }
  };

  const redirectAccourdingRole = () => {
    if (token && roleMapping === "ADMIN") {
      if (!adminPaths) adminPaths = allowedPath(permissions);

      redirectTo(adminPaths[0], { res: ctx.res, status: 301 });
    } else if (token) {
      redirectTo("/", { res: ctx.res, status: 301 });
    }
  };

  switch (pathname) {
    case "/register":
    case "/registro-admin":
    case "/registro-admin/permission":
    case "/registro-admin/permissionSuccess":
    case "/login":
    case "/ingreso":
    case "/registro":
      redirectAccourdingRole();
      return;
  }

  if (availableToAll.includes(pathname)) return;

  if (!token) return auth();

  if (roleMapping === "ADMIN") {
    if (!adminPaths) adminPaths = allowedPath(permissions);

    const found = adminPaths.find((item) => {
      const regex = new RegExp(`^${item}`);

      if (pathname.match(/^\/admin\/estadisticas/)) return true;

      return !!pathname.match(regex);
    });

    if (!found) redirectAccourdingRole();
  } else if (pathname.split("/")[1] === "admin") redirectAccourdingRole();
};

export default AuthMiddlewareRouter;
