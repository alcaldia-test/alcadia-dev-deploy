import moment from "moment";
import {
  useDateIsBetween,
  useDateIsAfter,
  useDateIsBefore,
  useFormateStringDate,
} from "@utils/dateHooks";

// NO EDITAR ESTE SNIPPET
export const validateAnnouncementState = ({
  receptionStart = undefined,
  receptionEnd = undefined,
  reviewStart = undefined,
  reviewEnd = undefined,
  supportingStart = undefined,
  supportingEnd = undefined,
  postResults = undefined,
  directPosts = undefined,
}) => {
  if (!directPosts) {
    const waitingReception = receptionStart
      ? useDateIsBefore(
          moment(useFormateStringDate(receptionStart)).startOf("day")
        )
      : false;
    const isReception =
      receptionStart && receptionEnd
        ? useDateIsBetween(
            moment(useFormateStringDate(receptionStart)).startOf("day"),
            moment(useFormateStringDate(receptionEnd)).endOf("day")
          )
        : false;
    const waitingReview =
      receptionEnd && reviewStart
        ? useDateIsBetween(
            moment(useFormateStringDate(receptionEnd)).endOf("day"),
            moment(useFormateStringDate(reviewStart)).startOf("day")
          )
        : false;
    const isReview =
      reviewStart && reviewEnd
        ? useDateIsBetween(
            moment(useFormateStringDate(reviewStart)).startOf("day"),
            moment(useFormateStringDate(reviewEnd)).endOf("day")
          )
        : false;
    const waitingSupport =
      reviewEnd && supportingStart
        ? useDateIsBetween(
            moment(useFormateStringDate(reviewEnd)).endOf("day"),
            moment(useFormateStringDate(supportingStart)).startOf("day")
          )
        : false;
    const isSupporting =
      supportingStart && supportingEnd
        ? useDateIsBetween(
            moment(useFormateStringDate(supportingStart)).startOf("day"),
            moment(useFormateStringDate(supportingEnd)).endOf("day")
          )
        : false;
    const isEvaluating =
      supportingEnd && postResults
        ? useDateIsBetween(
            moment(useFormateStringDate(supportingEnd)).endOf("day"),
            moment(useFormateStringDate(postResults)).endOf("day")
          )
        : false;
    const isFinished = postResults
      ? useDateIsAfter(moment(useFormateStringDate(postResults)).endOf("day"))
      : false;

    return {
      waitingReception,
      isReception,
      waitingReview,
      isReview,
      waitingSupport,
      isSupporting,
      isEvaluating,
      isFinished,
    };
  } else {
    const waitingReception = receptionStart
      ? useDateIsBefore(
          moment(useFormateStringDate(receptionStart)).startOf("day")
        )
      : false;
    const isDirectReception =
      receptionStart && receptionEnd
        ? useDateIsBetween(
            moment(useFormateStringDate(receptionStart)).startOf("day"),
            moment(useFormateStringDate(receptionEnd)).endOf("day")
          )
        : false;
    const isEvaluating =
      receptionEnd && postResults
        ? useDateIsBetween(
            moment(useFormateStringDate(receptionEnd)).endOf("day"),
            moment(useFormateStringDate(postResults)).endOf("day")
          )
        : false;
    const isFinished = postResults
      ? useDateIsAfter(moment(useFormateStringDate(postResults)).endOf("day"))
      : false;

    return {
      waitingReception,
      isDirectReception,
      isEvaluating,
      isFinished,
    };
  }
};
