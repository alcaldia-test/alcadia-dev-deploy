import React from "react";
import { Table as AntdTable } from "antd";
import "antd/lib/table/style/index.css";

import styles from "./table.module.scss";

export const Table = ({ data, columns, ...rest }) => {
  return (
    <AntdTable
      className={styles.Table}
      dataSource={data}
      pagination={false}
      columns={columns}
      size="small"
      {...rest}
    />
  );
};

export default Table;
