import React from "react";
import { withDesign } from "storybook-addon-designs";
import Table from "./index";

export default {
  title: "Components/Table",
  component: Table,
  decorators: [withDesign],
};

const Template = (args) => <Table {...args} />;

export const Default = Template.bind({});

Default.args = {
  columns: [
    {
      title: "Usuario",
      dataIndex: "name",
    },
    {
      title: "Edad",
      dataIndex: "edad",
      sorter: (a, b) => a.propuesta - b.propuesta,
    },
    {
      title: "Direccion",
      dataIndex: "direccion",
      filters: [
        {
          text: "avenida",
          value: "Avenida",
        },
        {
          text: "calle",
          value: "calle",
        },
      ],
      onFilter: (value, record) => record.direccion.indexOf(value) === 0,
    },
    {
      title: "Profesion",
      dataIndex: "profesion",
      sorter: (a, b) => a.profesion - b.profesion,
    },
    {
      title: "Acciones",
      key: "action",
      render: () => (
        <span className="actionButton">
          <a>Ocultar Propuesta</a>
        </span>
      ),
    },
  ],
  data: [
    {
      key: "1",
      name: "john Doe",
      edad: "27",
      direccion: "Avenida Principal 1",
      profesion: "Ing. Civil",
    },
    {
      key: "2",
      name: "Jim Es",
      edad: "32",
      direccion: "calle colina, Urb. Naranjos",
      profesion: "Educador",
    },
  ],
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=12%3A6088",
  },
};
