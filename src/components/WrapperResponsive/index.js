import React from "react";
import { Col, Row } from "antd";

/**
 * Function for Wrapper with responsive
 */

export const WrapperResponsive = ({ children, ...otherProps }) => {
  return (
    <Row
      justify="center"
      {...otherProps}
      className={otherProps.className + " gx-mr-0 gx-ml-0"}
    >
      <Col xxl={18} xl={24} lg={24} md={24} sm={24} xs={24}>
        {children}
      </Col>
    </Row>
  );
};

WrapperResponsive.propTypes = {
  ...Row.propTypes,
};

export default WrapperResponsive;
