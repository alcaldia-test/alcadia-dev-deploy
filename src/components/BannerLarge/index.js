import { useState, useEffect } from "react";
import draftToHtml from "draftjs-to-html";
import Link from "next/link";
import moment from "moment";

import { useFormateDate } from "@utils/dateHooks";
import { validateAnnouncementState } from "@utils/announcementStates";

import { Check, Download, Warning, ArrowLeft } from "@assets/icons";

import styles from "./BannerLarge.module.scss";
import Button from "@components/Button";
import PopUp from "@components/ProposalsPopUp";
import ZonesModal from "@components/Modals/ZonesModal/MacrosModal";

/**
 * @typedef BannerLarge
 * @param {receptionStart} [params.receptionStart="12/12/2021"] - Date in which the proposal starts
 * @param {receptionEnd} [params.receptionEnd="01/01/2022"] - Date in which the proposal ends
 * @param {reviewLimit} [params.reviewLimit="12/01/2022"] - Date in which the proposals are reviewed
 * @param {supportingDays} [params.supportingDays="01/02/2022"] - Date in which the proposal is supported by the user
 * @param {string} [params.image="/images/info1.png"] - Image to be displayed
 * @param {string} [params.title="Propuesta para ..."] - Title of the banner
 * @param {string} [params.subtitle="Hemos notado que ..."] - Subtitle of the banner
 * @param {boolean} [params.created=false] - True if the user has already created a proposal
 *
 */

const Banner = ({
  announcementId,
  directPosts,
  hasCreated,
  subtitle,
  attached,
  userId,
  image,
  title,
  receptionStart,
  receptionEnd,
  reviewStart,
  reviewEnd,
  supportingStart,
  supportingEnd,
  postResults,
  isInVotingZone,
  zones = undefined,
  macros = undefined,
}) => {
  const [popUpOpen, setPopUpOpen] = useState(false);
  const [showPopUp, setShowPopUp] = useState(true);
  const [showZonesModal, setShowZonesModal] = useState(false);

  const {
    waitingReception = false,
    isReception = false,
    isDirectReception = false,
    waitingReview = false,
    isReview = false,
    waitingSupport = false,
    isSupporting = false,
    isEvaluating = false,
    isFinished = false,
  } = validateAnnouncementState({
    receptionStart,
    receptionEnd,
    reviewStart,
    reviewEnd,
    supportingStart,
    supportingEnd,
    postResults,
    directPosts,
  });

  useEffect(() => {
    if (localStorage.getItem("showPopUpProposal") === "false") {
      setShowPopUp(false);
    } else {
      setShowPopUp(true);
    }
  }, [popUpOpen]);

  return (
    <>
      <PopUp
        visible={popUpOpen}
        onCancel={() => setPopUpOpen(false)}
        id={announcementId}
        title="¡Bienvenido! ¿Deseas saber cómo crear tu propuesta?"
      />
      <div className={styles.banner}>
        {image && (
          <div className={styles.banner__content}>
            <img src={image} alt={title} className={styles.banner__image} />
            <h2 className={styles.banner__title}>{title}</h2>
          </div>
        )}
        <div className={styles.banner__bottom}>
          <div className={styles.top}>
            <Link href="/propuestas_ciudadanas" as={`/propuestas_ciudadanas`}>
              <a className={styles.back}>
                <ArrowLeft /> Otras Convocatorias
              </a>
            </Link>
            <div
              className={styles.zone}
              onClick={() => setShowZonesModal(true)}
            >
              Ver zonas
            </div>
          </div>
          {userId && !isInVotingZone && !isFinished && !isEvaluating && (
            <p className={styles.not_in_zone}>
              <Warning /> Esta convocatoria está limitada a ciertas zonas
            </p>
          )}
          {isFinished && (
            <p className={styles.banner__finished}>
              <Check />
              Esta convocatoria ya finalizó
            </p>
          )}
          {isEvaluating && (
            <p className={styles.banner__evaluating}>
              <Check />
              Esta convocatoria está evaluando los ganadores
            </p>
          )}
          <h6 className={styles.banner__subtitle}>
            {typeof subtitle === "object" ? (
              <div
                dangerouslySetInnerHTML={{
                  __html: draftToHtml(subtitle),
                }}
              />
            ) : (
              subtitle
            )}
          </h6>
          <div className={styles.banner__status}>
            {waitingReception && (
              <p className={styles.banner__aproving_days}>
                La recepción de propuestas inicia el{" "}
                {useFormateDate(moment(receptionStart).startOf("day"))}
              </p>
            )}
            {isReception && !directPosts && (
              <p className={styles.banner__reception_days}>
                La recepción de propuestas termina el{" "}
                {useFormateDate(moment(receptionEnd).endOf("day"))}
              </p>
            )}
            {isDirectReception && directPosts && (
              <p className={styles.banner__reception_days}>
                La recepción de propuestas termina el{" "}
                {useFormateDate(moment(receptionEnd).endOf("day"))}
              </p>
            )}
            {waitingReview && (
              <p className={styles.banner__aproving_days}>
                La aprobación de propuestas inicia el{" "}
                {useFormateDate(moment(reviewStart).startOf("day"))}
              </p>
            )}
            {isReview && (
              <p className={styles.banner__aproving_days}>
                La recepción de propuestas ha finalizado, las propuestas se
                publicarán el {useFormateDate(moment(reviewEnd).endOf("day"))}
              </p>
            )}
            {waitingSupport && (
              <p className={styles.banner__aproving_days}>
                La apoyo de propuestas inicia el{" "}
                {useFormateDate(moment(supportingStart).startOf("day"))}
              </p>
            )}
            {(isEvaluating || isSupporting) && !isFinished && (
              <div className={styles.banner__direct_posts}>
                Las propuestas ganadoras se anunciarán el{" "}
                {useFormateDate(moment(postResults).endOf("day"))}
              </div>
            )}
          </div>
          {hasCreated && !isFinished && !isEvaluating && (
            <div className={styles.banner__created}>
              <Check />
              Hemos recibido tu propuesta
            </div>
          )}
          <div
            className={`${styles.banner__buttons} ${
              directPosts ? styles.banner__buttons__direct : null
            }`}
          >
            {attached && (
              <Button
                type="secondary"
                size="small"
                href={`${attached[0]}?name=${attached[1]}`}
                className={styles.banner__button_download}
              >
                Descargar Documento
                <Download />
              </Button>
            )}
            {!isFinished &&
              !isEvaluating &&
              (isReception || isDirectReception) &&
              (showPopUp && userId ? (
                <Button
                  type="primary"
                  size="small"
                  className={`${
                    directPosts
                      ? styles.banner__button
                      : styles.banner__button_create
                  }`}
                  disabled={hasCreated || !isInVotingZone}
                  onClick={() => setPopUpOpen(true)}
                >
                  Crear mi propuesta
                </Button>
              ) : (
                <Link
                  href={
                    userId
                      ? `/propuestas_ciudadanas/${announcementId}/crear`
                      : "/login"
                  }
                >
                  <a>
                    <Button
                      type="primary"
                      size="small"
                      disabled={hasCreated || !isInVotingZone}
                    >
                      Crear mi propuesta
                    </Button>
                  </a>
                </Link>
              ))}
          </div>
        </div>
      </div>
      {showZonesModal && (
        <ZonesModal
          closable={false}
          onCancel={() => setShowZonesModal(false)}
          macros={macros}
        />
      )}
    </>
  );
};
export default Banner;
