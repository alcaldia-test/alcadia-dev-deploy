import React from "react";

import styles from "./banner.module.scss";

/**
 * @typedef Banner
 * @param {string} [params.image="/images/info1.png"] - Image to be displayed
 * @param {string} [params.subtitle="Text"] - Subtitle of the banner
 * @param {string} [params.title="Text"] - Title of the banner
 *
 */

const Banner = ({ image, title, subtitle }) => {
  return (
    <div className={styles.banner}>
      {image ? (
        <div className={styles.banner__content}>
          <img src={image} alt={title} className={styles.banner__image} />
          <h1 className={styles.banner__title}>{title}</h1>
        </div>
      ) : (
        <div className={styles.banner__content}>
          <h3 className={styles.banner__title}>{title}</h3>
        </div>
      )}
      <div className={styles.banner__bottom}>
        <h6 className={styles.banner__subtitle}>{subtitle}</h6>
      </div>
    </div>
  );
};
export default Banner;
