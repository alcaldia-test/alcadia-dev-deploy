import { DatePicker, Space } from "antd";
import moment from "moment";
import "antd/lib/date-picker/style/index.css";
import "antd/lib/style/index.css";

const { RangePicker } = DatePicker;

const dateFormat = "YYYY/MM/DD";

const RangePickerComponent = ({
  setRango = () => {},
  rango = [moment(new Date(), dateFormat), moment(new Date(), dateFormat)],
}) => {
  return (
    <Space direction="vertical" size={12}>
      <RangePicker
        value={rango}
        format={dateFormat}
        onChange={(e) => {
          setRango(e);
        }}
        placeholder={["Fecha de inicio", "Fecha Final"]}
      />
    </Space>
  );
};

export default RangePickerComponent;
