import { withDesign } from "storybook-addon-designs";

import Upload from "./index.js";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUpload = {
  default: "677%3A6873",
};

export default {
  title: "Components/Upload",
  component: Upload,
  decorators: [withDesign],
  argTypes: {
    textButton: "string",
    typeButton: {
      options: ["primary", "secondary", "link"],
      control: "select",
    },
    sizeButton: {
      options: ["large", "medium", "small", "small-x", "super-small"],
      control: "select",
    },
    action: "string",
  },
};

const Template = (args) => <Upload {...args} />;

const defaultProps = {
  accept: "image/*,application/pdf",
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  onChange({ file, fileList }) {
    if (file.status !== "uploading") {
      console.log(file, fileList);
    }
  },
  defaultFileList: [
    {
      uid: "1",
      name: "xxx.png",
      status: "done",
      response: "", // custom error message to show
      url: "http://www.baidu.com/xxx.png",
    },
    {
      uid: "2",
      name: "yyy.png",
      status: "done",
      url: "http://www.baidu.com/yyy.png",
    },
    {
      uid: "3",
      name: "zzz.png",
      status: "error",
      response: "", // custom error message to show
      url: "http://www.baidu.com/zzz.png",
    },
  ],
  showUploadList: {
    showDownloadIcon: false,
    showRemoveIcon: true,
    showDeleteIcon: false,
  },
  locale: {
    uploading: "",
    removeFile: "",
    downloadFile: "",
    uploadError: "",
    previewFile: "",
  },
};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
  textButton: "Subir archivo",
  typeButton: "secondary",
  sizeButton: "medium",
};
Default.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idUpload.default}`,
  },
};
