import React, { useState } from "react";
import "antd/lib/upload/style/index.css";
import styles from "./Upload.module.scss";
import { Upload as AntdUpload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import Button from "@components/Button";
import DeleteModal from "@components/deleteModal";
import message from "@components/Message";

/**
 * @param typeButton Type of Button Upload, can only be [ primary, secondary, link ]
 * @param sizeButton Size of Button Upload, can only be [ large , medium , small , small-x , super-small ]
 * @param textButton String with text content of button
 * @param props Rest of Props of an Upload Component in AntDesign
 * @return An upload component using AntDesign
 */
const Upload = ({
  textButton = "Subir archivo",
  typeButton = "secondary",
  sizeButton = "medium",
  disabled = false,
  allowAddMoreToServer = true,
  filesInServer = [],
  onDeleteFunction = () => {},
  onDeleteClick = () => {},
  deleteTag = "",
  onlyImages = false,
  ...props
}) => {
  const [showDeleteModal, setshowDeleteModal] = useState(false);

  const modalDeleteOk = () => {
    setshowDeleteModal(false);
    onDeleteFunction(deleteTag);
  };

  const checkFile = (file) => {
    return checkFileType(file) && checkFileSize(file);
  };

  const checkFileSize = (file) => {
    const isLt2M = file.size / 1024 / 1024 < 3;
    if (!isLt2M) {
      message.error("El archivo es muy grande, el tamaño máximo es de 3MB");
      return false;
    } else {
      return true;
    }
  };

  const checkFileType = (file) => {
    const allowedTypes = ["image/png", "image/jpg", "image/jpeg"];
    if (!onlyImages) allowedTypes.push("application/pdf");
    const isAllowed = allowedTypes.includes(file.type);
    if (!isAllowed && !onlyImages) {
      message.error("Solo están permitidos archivos tipo PNG, JPG, JPEG y PDF");
      return false;
    } else if (!isAllowed && onlyImages) {
      message.error("Solo están permitidos archivos tipo PNG, JPG y JPEG");
      return false;
    } else {
      return true;
    }
  };

  return (
    <>
      {(allowAddMoreToServer || filesInServer.length === 0) && (
        <AntdUpload
          className={`${styles.upload}`}
          beforeUpload={checkFile}
          {...props}
        >
          <Button
            type={typeButton}
            size={sizeButton}
            className={`${styles["button-upload"]}`}
            disabled={disabled}
          >
            {textButton} <UploadOutlined />
          </Button>
        </AntdUpload>
      )}
      {filesInServer.length > 0 && (
        <div className="flex flex-wrap">
          {filesInServer.map((file, i) => (
            <div key={i} className={styles.imageContainer}>
              {["jpeg", "jpg", "gif", "png"].some((el) => file.includes(el)) ? (
                <>
                  <span className={styles.deleteIcon}>
                    <img
                      className={styles.imageDeleteIcon}
                      src="/icons/delete-bin.svg"
                      onClick={() => {
                        if (onDeleteFunction) setshowDeleteModal(true);
                        else onDeleteClick();
                      }}
                    />
                  </span>
                  <a href={file} target="noopener noreferrer">
                    <img src={file} className={styles.image} alt={file} />
                  </a>
                </>
              ) : (
                <div className={styles.attachContainer}>
                  <a
                    href={`${file[0]}?name=${file[1]}`}
                    download
                    className={styles.downloadFileContainer}
                  >
                    <img
                      href={file[0]}
                      src="/icons/download-cloud-2-line.svg"
                      className={styles.attache}
                      alt={file[1]}
                    />
                    <span className={styles.downloadFileName}>{file[1]}</span>
                  </a>
                  <span className={styles.deleteIconInAttache}>
                    <img
                      className={styles.imageDeleteIcon}
                      src="/icons/delete-bin.svg"
                      onClick={() => {
                        if (onDeleteFunction) setshowDeleteModal(true);
                        else onDeleteClick();
                      }}
                    />
                  </span>
                </div>
              )}
            </div>
          ))}
        </div>
      )}
      {showDeleteModal && (
        <DeleteModal
          title="¿Seguro que deseas eliminar este archivo?"
          closeModal={() => setshowDeleteModal(false)}
          delteFuntion={modalDeleteOk}
          simple={true}
        />
      )}
    </>
  );
};

export default Upload;
