import React from "react";
import "antd/lib/upload/style/index.css";
import styles from "./Upload.module.scss";
import { Upload as AntdUpload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import Button from "@components/Buttons";

/**
 * @param typeButton Type of Button Upload, can only be [ primary, secondary, link ]
 * @param sizeButton Size of Button Upload, can only be [ large , medium , small , small-x , super-small ]
 * @param textButton String with text content of button
 * @param props Rest of Props of an Upload Component in AntDesign
 * @return An upload component using AntDesign
 */
const Upload = ({
  textButton = "Subir archivo",
  typeButton = "secondary",
  sizeButton = "medium",
  ...props
}) => {
  return (
    <AntdUpload className={`${styles.upload}`} {...props}>
      <Button
        type={typeButton}
        size={sizeButton}
        className={`${styles["button-upload"]}`}
      >
        {textButton} <UploadOutlined />
      </Button>
    </AntdUpload>
  );
};

export default Upload;
