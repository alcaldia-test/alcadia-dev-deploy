import React from "react";
import { withDesign } from "storybook-addon-designs";
import Saving from "./index";

export default {
  title: "Components/Saving",
  component: Saving,
  decorators: [withDesign],
  argTypes: {
    tip: "string",
    iconType: "save",
    size: { options: ["default", "small", "large"], control: "select" },
    // value: "string"
  },
};

const Template = (args) => <Saving {...args} />;
export const Default = Template.bind({});

Default.args = {
  tip: "Guardando",
  iconType: "save",
  size: "large",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7172",
  },
};
