import { Icon, Spin } from "antd";
import React from "react";
import "antd/lib/spin/style/css";
import styles from "./Saving.module.scss";

const Saving = ({ tip, iconType, size, ...args }) => {
  // const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
  return (
    <div className={styles.saving}>
      <Spin tip={tip + "..."} size={size} indicator={<Icon type={iconType} />}>
        {/* <Alert
          message="Alert message title"
          description="Further details about the context of this alert."
          type="info"
        /> */}
      </Spin>
    </div>
  );
};
export default Saving;
