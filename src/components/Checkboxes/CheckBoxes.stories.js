import { withDesign } from "storybook-addon-designs";

import CheckBoxes from "./index.js";

export default {
  title: "Components/CheckBoxes",
  component: CheckBoxes,
  decorators: [withDesign],
  argTypes: {
    disabled: {
      control: "boolean",
    },
    active: {
      control: "boolean",
    },
    label: {
      control: "text",
    },
    type: {
      control: {
        options: ["primary", "secondary"],
        control: "select",
      },
    },
  },
};

const Template = (args) => <CheckBoxes {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlCheckBoxes = {
  default: "9%3A2995",
};

export const Primary = Template.bind({});
Primary.args = {
  active: true,
  disabled: false,
  label: "Label opcional",
  type: "primary",
};

Primary.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlCheckBoxes.default}`,
  },
};

export const Secondary = Template.bind({});
Secondary.args = {
  active: true,
  disabled: false,
  label: "Label opcional",
  type: "secondary",
};
