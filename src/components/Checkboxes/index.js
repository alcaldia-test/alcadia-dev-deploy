import React from "react";
import { Checkbox } from "antd";
import "antd/lib/checkbox/style/index.css";
import styles from "./CheckBoxes.module.scss";
/**
 * @param type String that define a color for this checkbox, can be [ primary , secondary ]
 * @return A Checkbox component using Ant Design
 */
function Checkboxes({ type = "primary", children, ...props }) {
  return (
    <Checkbox {...props} className={`${styles.checkbox} ${styles[type]}`}>
      {children}
    </Checkbox>
  );
}
export default Checkboxes;
