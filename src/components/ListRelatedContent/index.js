import React, { useEffect } from "react";
import Styles from "./index.module.scss";
import moment from "moment";
import "moment/locale/es";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { getRelatedPosts } from "@redux/common/actions";
import Skeleton from "@components/Skeleton";

const ListRelatedContent = ({ post, module }) => {
  const { relatedPosts } = useSelector((state) => state.common);
  const dispatch = useDispatch();

  useEffect(() => {
    // console.warn("Post", post);
    dispatch(
      getRelatedPosts({
        endpoint: getEndpoint(module),
        post: post,
      })
    );
  }, [post.id]);

  return (
    relatedPosts?.length > 0 && (
      <>
        <h5 className={`${Styles.card_title} `}>Contenido relacionado</h5>
        <hr style={{ color: "#0004" }} />
        <div className={`${Styles.box_content}`}>
          {relatedPosts ? (
            relatedPosts.map((post) => {
              return (
                <RelatedElement
                  key={post.id}
                  module={module}
                  date={moment(post.dueDate)}
                  title={post.title}
                  id={post.id}
                />
              );
            })
          ) : (
            <Skeleton active />
          )}
        </div>
      </>
    )
  );
};
export default ListRelatedContent;

const RelatedElement = ({ module, date, title, id }) => {
  return (
    <Link href={`/${module}/${id}`} as={`/${module}/${id}`}>
      <a className="w-100">
        <div
          className={`${Styles.card_border} h-auto border-solid border-2 px-64 py-64`}
        >
          <p className={`${Styles.text_date} font-bold`}>
            {date.format("MMMM")} {date.format("DD")}
          </p>
          <p className={`${Styles.card_description} font-bold`}>{title}</p>
        </div>
      </a>
    </Link>
  );
};

const getEndpoint = (module) => {
  switch (module) {
    case "charlas":
      return "discussions";
    case "normativas":
      return "normative";
    case "tu_voto_cuenta":
      return "voting";
    default:
      return "";
  }
};
