import Card from "@components/Cards/Card";
import styles from "./CardStats.module.scss";

const CardStats = ({ value, detail, icon, svg }) => (
  <div className={styles.card__stats}>
    <Card>
      {icon && <img src={icon} className="image" />}
      {svg && svg}
      <span>
        <h6>{value}</h6>
        <p>{detail}</p>
      </span>
    </Card>
  </div>
);

export default CardStats;
