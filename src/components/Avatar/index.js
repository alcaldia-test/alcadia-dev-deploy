import React from "react";
import { Avatar as AntdAvatar } from "antd";

import "antd/lib/avatar/style/index.css";
import styles from "./Avatar.module.scss";
/**
 * @param src Source of the image
 * @param size Size of image, only can be ( large , medium , small , small-x , super-small )
 * @param props Rest props of Avatar in AntDesign
 * @returns An Avatar component using AntDesign
 */
function Avatar({ src, size, ...props }) {
  return (
    <AntdAvatar
      {...props}
      className={`${styles[size]}`}
      src={src || "/images/icon/user.svg"}
    />
  );
}

export default Avatar;
