import React from "react";
import styles from "./InputField.module.scss";

const InputField = ({
  rightIcon = "",
  leftIcon = "",
  label,
  type,
  onChange,
  name,
  colorText = "#545454",
  defaultValue,
  error = false,
  colorInput = "rgba(26, 26, 26, 0.4)",
  disabled = false,
  style = {},
}) => {
  return (
    <div
      className={`z-0 relative rounded-2xl content-center w-full items-stretch ${styles.floating}`}
      style={{
        borderRadius: "16px",
        color: "rgba(156, 156, 156, 0.8)",
      }}
    >
      <span
        className={`flex z-10 h-full leading-snug font-normal absolute text-center content-center justify-center w-8 pl-3 py-3 `}
        style={{
          width: "32px",
          paddingLeft: "12px",
          paddingTop: "12px",
          paddingBottom: "12px",
        }}
      >
        <img src={rightIcon} />
      </span>
      <span
        className={`flex right-0 z-10 h-full leading-snug font-normal absolute text-center content-center justify-center w-8 pr-3 py-3 `}
        style={{
          right: "0px",
          width: "32px",
          paddingRight: "12px",
          paddingTop: "12px",
          paddingBottom: "12px",
        }}
      >
        <img src={leftIcon} />
      </span>
      <input
        name={name}
        type={type}
        value={defaultValue}
        onChange={onChange}
        placeholder={label}
        className="display flex rounded-xl px-4 py-4 border-2  border-gray-400 placeholder-blueGray-300 font-semibold text-blueGray-600 relative bg-white text-md focus:ring w-full p-3 h-16 pl-10 body-medium"
        style={{
          borderRadius: "12px",
          paddingRight: "16px",
          paddingTop: "16px",
          paddingBottom: "16px",
          borderWidth: "2px",
          padding: "12px",
          height: "64px",
          paddingLeft: rightIcon ? "40px" : "16px",
          color: error ? "#fe4752" : colorText,
          borderColor: error ? "#fe4752" : colorInput,
          ...style,
        }}
        disabled={disabled}
      />
      <label
        className="absolute top-0 left-7 px-3 py-5 h-full pointer-events-none transform origin-left font-semibold text-md transition-all duration-100 ease-in-out "
        style={{
          top: "0px",
          left: rightIcon ? "28px" : "0px",
          paddingRight: "12px",
          paddingLeft: "12px",
          paddingTop: "20px",
          paddingBottom: "20px",
          color: "rgba(26, 26, 26, 0.4)",
        }}
      >
        {label}
      </label>
    </div>
  );
};

export default InputField;
