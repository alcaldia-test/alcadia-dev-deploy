import React from "react";
import Image from "next/image";
import styles from "./Logo.module.scss";

function Logo({ sizes, src, alt, ...props }) {
  return (
    <div className={` ${styles.logoContainer} ${styles[sizes]}`}>
      <Image
        src={src}
        alt={alt}
        size={sizes}
        {...props}
        layout="fill"
        objectFit="cover"
      />
    </div>
  );
}

export default Logo;
