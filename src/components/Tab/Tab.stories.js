import { DislikeFilled, LikeFilled } from "@ant-design/icons";
import { withDesign } from "storybook-addon-designs";

import Tab from "./index";

export default {
  title: "Components/Tab",
  component: Tab,
  decorators: [withDesign],
  argType: {
    content: { data: "array", control: "object" },
  },
};
const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idTab = {
  default: "748%3A7317",
};

const Template = (args) => <Tab {...args} />;

export const Default = Template.bind({});
Default.args = {
  content: [
    {
      title: "Esto es",
      children: "una prueba",
    },
    {
      title: "Titulo",
      children: <LikeFilled />,
    },
    {
      title: "Otro Titulo",
      children: <DislikeFilled />,
    },
  ],
};
Default.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idTab.default}`,
  },
};
