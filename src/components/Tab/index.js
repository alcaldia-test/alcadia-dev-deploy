import React from "react";
import { Tabs } from "antd";
import "antd/lib/tabs/style/index.css";
import styles from "./Tab.module.scss";
import { UserOutlined } from "@ant-design/icons";
const { TabPane } = Tabs;

const defaultContent = [
  { title: "Titulo 1", children: <UserOutlined /> },
  { title: "Titulo 2", children: "Texto 2" },
];
/**
 *
 * @param content -> Array of objects with title, children properties. Example:
 * content = [
 * {title:"Titulo 1",children:"Texto1"},
 * {title:"Titulo 2",children:<ReactComponent>}
 * ]
 * @param props Rest props of Tabs in AntDesign
 * @returns A Tab component using AntDesign
 */
function Tab({ defaultActiveKey = "0", content = defaultContent, ...props }) {
  return (
    <Tabs
      {...props}
      defaultActiveKey={defaultActiveKey}
      className={`${styles.tab}`}
    >
      {content?.map((element, index) => (
        <TabPane key={`${index}`} tab={element?.title}>
          {element?.children}
        </TabPane>
      ))}
    </Tabs>
  );
}

export default Tab;
