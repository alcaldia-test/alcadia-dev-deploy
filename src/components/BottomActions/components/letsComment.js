import React from "react";
import styles from "./components.module.scss";

export const LetsComment = () => {
  return (
    <button className={styles.commentButton}>
      <span>Déjanos tu opinión en los comentarios</span>
    </button>
  );
};
