import React from "react";
import Image from "next/image";
import styles from "./components.module.scss";
const getLikePercent = (likes, dislikes) => {
  const votesLikes = parseInt(likes);
  const votesDislikes = parseInt(dislikes);
  const total = votesLikes + votesDislikes;
  return total > 0
    ? Math.round((votesLikes * 100) / (votesLikes + votesDislikes))
    : 50;
};

const Buttons = ({
  _props: { inSide, likes, dislikes, reaction, handleButton, voted },
}) => {
  const total = likes + dislikes;
  const likePercent = getLikePercent(likes, dislikes);
  const disLikePercent = 100 - likePercent;
  const yes = voted
    ? {
        text: total > 0 ? `${likePercent}%` : "0%",
        _style: inSide
          ? {
              width: `100%`,
            }
          : {
              flexBasis: `${likePercent * 0.95}%`,
              marginRight: likePercent < 20 ? "10px" : "0px",
            },
        class: `${styles.yes} ${reaction === "like" ? styles.bgYes : ""}`,
      }
    : {
        text: "Si",
        _style: inSide ? { width: "100%" } : { width: "48%" },
        class: "",
      };

  const no = voted
    ? {
        text: total > 0 ? `${disLikePercent}%` : "0%",
        _style: inSide
          ? {
              width: `100%`,
            }
          : {
              flexBasis: `${disLikePercent * 0.95}%`,
              marginLeft: likePercent > 80 ? "10px" : "0px",
            },
        class: `${styles.no} ${reaction === "dislike" ? styles.bgNo : ""}`,
      }
    : {
        text: "No",
        _style: inSide ? { width: "100%" } : { width: "48%" },
        class: "",
      };

  return (
    <div
      className={`${styles.buttonsContainer} ${inSide ? styles.block : ""} ${
        voted ? styles.voted : styles.notVoted
      }`}
    >
      <button
        onClick={() => handleButton(true)}
        style={yes._style}
        className={yes.class}
      >
        <Image src="/icons/like.svg" width={30} height={30} />
        <span>{yes.text}</span>
      </button>

      <button
        onClick={() => handleButton(false)}
        style={no._style}
        className={no.class}
      >
        <Image src="/icons/dislike.svg" width={30} height={30} />
        <span>{no.text}</span>
      </button>
    </div>
  );
};

export default Buttons;
