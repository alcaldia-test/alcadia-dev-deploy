import React from "react";
import styles from "./components.module.scss";
const TitleAsMessage = ({ title }) => {
  return (
    <div className={styles.infoGhost}>
      <h2>{title}</h2>
    </div>
  );
};
export default TitleAsMessage;
