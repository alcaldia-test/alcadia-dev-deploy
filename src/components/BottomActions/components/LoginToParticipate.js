import { routeRedirectRegister } from "@redux/auth/actions";
import Link from "next/link";
import React from "react";
import { useDispatch } from "react-redux";

export const LoginToParticipate = () => {
  const goto = encodeURI(window.location.pathname);
  const dispatch = useDispatch();
  const setUrl = (e) => {
    if (e.target.tagName === "A") {
      dispatch(routeRedirectRegister(window?.location.pathname));
    }
  };
  return (
    <div>
      <div>
        <span onClick={setUrl}>
          Necesitasas <Link href={`/login?goto=${goto}`}>iniciar sesión</Link> o{" "}
          <Link href="/register">registrarte</Link> para participar
        </span>
      </div>
    </div>
  );
};
