import React, { useState, memo, useEffect } from "react";
import styles from "./bottomActions.module.scss";
import Modal from "@components/Modals/SupportModal";
import message from "@components/Message";
import Spin from "@components/Spin";
import Alert from "@components/Alert";
import { Buttons, TitleAsMessage } from "./components";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { postReaction } from "@redux/common/actions";
import { LetsComment } from "./components/letsComment";
import moment from "moment";
/**
 *
 * @param  localPost post que minimamente deberia tener { postsId, likes, dislikes, reaction }
 * @param  title testo antes de votar
 * @param  bodyMessage objeto html para el caso yes y no
 * @param  isNearly booleano para determinar si estás en la zona
 * @returns los botones y toda su interacción
 */

const BottomActions = ({
  bottomProps: {
    inSide,
    isAdmin,
    title,
    titleStats,
    okButton,
    dataAuth,
    localPost,
    bodyMessage,
    isNearly,
    module,
  },
}) => {
  const { normativeId, id, postsId, likes, dislikes, reaction, dueDate } =
    localPost;

  const voted =
    isAdmin || reaction !== "any" || moment()._d > moment(dueDate)._d;

  const router = useRouter();
  const dispatch = useDispatch();
  const buttonLabel = okButton || {
    yes: "Confirmar",
    no: "Confirmar",
  };

  const [showModal, setShowModal] = useState(false);
  const [okText, setOkText] = useState(buttonLabel.yes);
  const [vote, setVote] = useState(true);
  const [loading, setLoading] = useState(false);
  const isAuth = !!dataAuth?.tokenUser;

  const handleModalConfirm = (vote) => {
    setLoading(true);
    const payload = {
      id: normativeId !== undefined ? normativeId : id,
      postsId: postsId,
      userId: dataAuth.dataUser?.id,
      reaction: vote ? "like" : "dislike",
      likes,
      dislikes,
      module,
    };
    dispatch(postReaction(payload));
    setShowModal(false);
  };

  const handleButton = (vote) => {
    if (isAdmin) {
      message.error("Como administrador, no puede votar");
      return;
    }

    if (!isAuth) {
      router.push({
        pathname: "/login",
        query: { goto: encodeURI(window.location.pathname) },
      });
      return;
    }

    if (voted) {
      (reaction === "like" || reaction === "dislike") &&
        message.error("Usted ya participó");
    } else {
      setOkText(vote ? buttonLabel.yes : buttonLabel.no);
      setVote(vote);
      setShowModal(true);
    }
  };

  useEffect(() => {
    setLoading(false);
  }, [reaction]);

  return isNearly || isAdmin || !isAuth ? (
    <div className={styles.actionsContainer}>
      <TitleAsMessage title={voted ? titleStats : title} />
      <Spin
        tip="Procesando ..."
        size="small"
        spinning={loading}
        wrapperClassName={styles.spin}
      >
        <Buttons
          _props={{ inSide, voted, likes, dislikes, reaction, handleButton }}
        />
      </Spin>
      <LetsComment />
      <Modal
        visible={showModal}
        closable={false}
        onCancel={() => setShowModal(false)}
        cancelText="Cancelar"
        okText={okText}
        support={vote}
        onOk={() => handleModalConfirm(vote)}
        message={bodyMessage}
      />
    </div>
  ) : (
    <>
      <LetsComment />
      <Alert
        message="Votación restringida zonalmente"
        type="warning"
        showIcon
      />
    </>
  );
};

export default memo(BottomActions);
