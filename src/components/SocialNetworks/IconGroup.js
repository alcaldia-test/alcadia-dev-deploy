import {
  FacebookShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";
import { Whatsapp, Facebook, Twitter, Telegram, Link } from "@assets/icons";
import { StatisticServices } from "@services";
import { useRouter } from "next/router";
import message from "@components/Message";

import styles from "./IconGroup.module.scss";

const IconGroup = ({ href, userId, postsId, proposalId }) => {
  const router = useRouter();

  const handleShare = async (network) => {
    const service = new StatisticServices();

    try {
      await service.postShare({
        socialNetwork: network,
        userId,
        postsId,
        proposalId,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const LoggedInButtons = () => (
    <div className={styles.share__buttons}>
      <WhatsappShareButton url={href} onClick={() => handleShare("WHATSAPP")}>
        <div className={`${styles.containerSvg} ${styles.whatsapp}`}>
          <Whatsapp />
        </div>
      </WhatsappShareButton>

      <FacebookShareButton url={href} onClick={() => handleShare("FACEBOOK")}>
        <div className={`${styles.containerSvg} ${styles.facebook}`}>
          <Facebook />
        </div>
      </FacebookShareButton>

      <TwitterShareButton url={href} onClick={() => handleShare("TWITTER")}>
        <div className={`${styles.containerSvg} ${styles.twitter}`}>
          <Twitter />
        </div>
      </TwitterShareButton>

      <TelegramShareButton url={href} onClick={() => handleShare("TELEGRAM")}>
        <div className={`${styles.containerSvg} ${styles.telegram}`}>
          <Telegram />
        </div>
      </TelegramShareButton>

      <button
        onClick={() => {
          navigator?.clipboard?.writeText(href);
          handleShare("LINK");
          message.success("¡Enlace copiado!");
        }}
      >
        <div className={`${styles.containerSvg} ${styles.link}`}>
          <Link />
        </div>
      </button>
    </div>
  );

  const NotLoggedInButtons = () => {
    const goto = {
      pathname: "/login",
      query: { goto: encodeURI(href) },
    };

    return (
      <div className={styles.share__buttons}>
        <button onClick={() => router.push(goto)}>
          <div className={`${styles.containerSvg} ${styles.whatsapp}`}>
            <Whatsapp />
          </div>
        </button>
        <button onClick={() => router.push(goto)}>
          <div className={`${styles.containerSvg} ${styles.facebook}`}>
            <Facebook />
          </div>
        </button>
        <button onClick={() => router.push(goto)}>
          <div className={`${styles.containerSvg} ${styles.twitter}`}>
            <Twitter />
          </div>
        </button>
        <button onClick={() => router.push(goto)}>
          <div className={`${styles.containerSvg} ${styles.telegram}`}>
            <Telegram />
          </div>
        </button>
        <button onClick={() => router.push(goto)}>
          <div className={`${styles.containerSvg} ${styles.link}`}>
            <Link />
          </div>
        </button>
      </div>
    );
  };

  console.warn({ userId });
  return userId ? <LoggedInButtons /> : <NotLoggedInButtons />;
};

export default IconGroup;
