import React, { useState } from "react";
import CardVoting from "@components/Cards/Voting";
import Button from "@components/Button";
import Link from "next/link";
import Share from "@components/Modals/Share";
import { useSelector } from "react-redux";

export default function VotesFeatured({ elements }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalData, setModalData] = useState({
    href: "",
    postsId: "",
  });
  const { dataUser } = useSelector((storage) => storage.auth);

  return (
    <>
      <Share
        {...modalData}
        visible={modalOpen}
        onCancel={() => setModalOpen(false)}
        closable={false}
        userId={dataUser?.id}
        cancelText="Cancelar"
      />
      {elements.map(
        ({
          completed,
          title,
          content,
          postsId,
          dueDate,
          fileLink,
          areas,
          id,
        }) => (
          <Link
            key={id}
            href="/tu_voto_cuenta/[id]"
            as={`/tu_voto_cuenta/${id}`}
          >
            <a>
              <CardVoting
                completed={completed}
                postsId={postsId}
                dueDate={dueDate}
                content={content}
                image={fileLink}
                title={title}
                zones={areas}
                key={id}
                id={id}
              >
                <Button type="primary" shape="round" size="small">
                  Ingresa y participa
                </Button>
                <Button
                  type="secondary"
                  shape="round"
                  size="small"
                  onClick={(e) => {
                    e.preventDefault();
                    setModalData({
                      href: `${location.origin}/tu_voto_cuenta/${id}`,
                      postsId,
                    });
                    setModalOpen(!modalOpen);
                  }}
                >
                  Comparte
                </Button>
              </CardVoting>
            </a>
          </Link>
        )
      )}
    </>
  );
}
