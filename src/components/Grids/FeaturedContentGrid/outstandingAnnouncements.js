import React, { useState } from "react";
import CardAnnouncements from "@components/Cards/Announcement";
import Button from "@components/Button";
import Share from "@components/Modals/Share";
import Link from "next/link";

export default function OutstandingAnnouncements({ elements }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");

  return (
    <>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
      />
      {elements.map(
        ({
          supportingLimit,
          receptionStart,
          receptionEnd,
          reviewLimit,
          directPosts,
          proposals,
          fileLink,
          title,
          areas,
          id,
        }) => (
          <Link
            href="/propuestas_ciudadanas/[announcementId]"
            as={`/propuestas_ciudadanas/${id}`}
            key={id}
          >
            <a>
              <CardAnnouncements
                supportingLimit={supportingLimit}
                receptionStart={receptionStart}
                receptionEnd={receptionEnd}
                reviewLimit={reviewLimit}
                directPosts={directPosts}
                proposals={proposals}
                image={fileLink}
                title={title}
                zones={areas}
              >
                <Button type="primary" shape="round" size="small">
                  Ingresa y participa
                </Button>
                <Button
                  type="secondary"
                  shape="round"
                  size="small"
                  onClick={(e) => {
                    e.preventDefault();
                    setModalURL(
                      `${location.origin}/propuestas_ciudadanas/${id}`
                    );
                    setModalOpen(!modalOpen);
                  }}
                >
                  Comparte
                </Button>
              </CardAnnouncements>
            </a>
          </Link>
        )
      )}
    </>
  );
}
