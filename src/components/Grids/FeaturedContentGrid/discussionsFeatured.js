import React, { useState } from "react";
import { ProposalCardComment } from "@components/Cards";
import { useDateIsBefore } from "@utils/dateHooks";
import Link from "next/link";
import Share from "@components/Modals/Share";
import { useRouter } from "next/router";

export default function VotesFeatured({ elements }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");
  const router = useRouter();

  const onClick = (isFinished, id) => {
    router.push(`charlas/${id}`);
  };

  return (
    <>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
      />
      {elements &&
        elements.map((discussion, i) => {
          return (
            <Link
              href="/charlas/[id]"
              as={`/charlas/${discussion.id}`}
              key={discussion.id}
            >
              <a>
                <ProposalCardComment
                  description={{
                    text: discussion.content,
                    link: `/charlas/${discussion.id}`,
                  }}
                  numberComments={discussion.comments}
                  title={discussion.title}
                  zones={discussion.areas}
                  isFinished={!useDateIsBefore(discussion.dueDate)}
                  width="100%"
                  height="420px"
                  onClickShare={(e) => {
                    e.preventDefault();
                    setModalOpen(!modalOpen);
                    setModalURL(`${location.origin}/charlas/${discussion.id}`);
                  }}
                  onClickLogin={() =>
                    onClick(!useDateIsBefore(discussion.dueDate), discussion.id)
                  }
                  shareName="charla"
                />
              </a>
            </Link>
          );
        })}
    </>
  );
}
