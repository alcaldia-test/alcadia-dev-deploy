import React, { useState } from "react";
import { ProposalCardComment } from "@components/Cards";
import { useDateIsBefore } from "@utils/dateHooks";
import Link from "next/link";
import Share from "@components/Modals/Share";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

export default function OutstandingDiscussions({ elements }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");
  const router = useRouter();
  const { dataUser } = useSelector((storage) => storage.auth);

  const onClick = (isFinished, id) => {
    router.push(`charlas/${id}`);
  };

  return (
    <>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
        userId={dataUser.id}
      />
      {elements &&
        elements.map((discussion, i) => {
          console.log(discussion.comments);
          return (
            <Link
              href="/charlas/[id]"
              as={`/charlas/${discussion.id}`}
              key={discussion.id}
            >
              <a>
                <ProposalCardComment
                  description={{
                    text: discussion.content,
                    link: `/charlas/${discussion.id}`,
                  }}
                  numberComments={discussion.comments}
                  title={discussion.title}
                  zones={discussion.areas}
                  isFinished={!useDateIsBefore(discussion.dueDate)}
                  width="100%"
                  height="500px"
                  onClickShare={(e) => {
                    e.preventDefault();
                    setModalOpen(!modalOpen);
                    setModalURL(`${location.origin}/charlas/${discussion.id}`);
                  }}
                  onClickLogin={() =>
                    onClick(!useDateIsBefore(discussion.dueDate), discussion.id)
                  }
                  shareName="charla"
                />
              </a>
            </Link>
          );
        })}
    </>
  );
}
