import React from "react";
import Title from "antd/lib/typography/Title";
import Button from "@components/Button";
import styles from "./featured.module.scss";
import Link from "next/link";
import { Icon } from "antd";

export default function FeaturedContentGrid({
  title,
  elements = [],
  linkLandingModule,
  mas,
  cardsType,
}) {
  return (
    <section className={styles.filter__container}>
      <Title className="text-2xl mb-1 text-center">{title}</Title>
      <div className={styles.filter__content}>
        {elements.length > 0 ? (
          <>{cardsType}</>
        ) : (
          <div className={styles.no__results}>Sin contenido por ahora</div>
        )}
      </div>
      <div className="flex justify-center mt-3 mb-1">
        <Button type="danger" size="small-x">
          <Link href={linkLandingModule}>{mas}</Link>{" "}
          <Icon type="arrow-right" />
        </Button>
      </div>
    </section>
  );
}
