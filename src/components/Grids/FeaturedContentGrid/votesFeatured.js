import React, { useState } from "react";
import CardVoting from "@components/Cards/Voting";
import Button from "@components/Button";
import Link from "next/link";
import Share from "@components/Modals/Share";

export default function VotesFeatured({ elements }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");

  return (
    <>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
      />
      {elements.map(
        ({
          completed,
          title,
          content,
          postsId,
          dueDate,
          fileLink,
          areas,
          id,
        }) => (
          <Link
            key={id}
            href="/tu_voto_cuenta/[id]"
            as={`/tu_voto_cuenta/${id}`}
          >
            <a>
              <CardVoting
                completed={completed}
                postsId={postsId}
                dueDate={dueDate}
                content={content}
                image={fileLink}
                title={title}
                zones={areas}
                key={id}
                id={id}
              >
                <Button type="primary" shape="round" size="small">
                  Ingresa y participa
                </Button>
                <Button
                  type="secondary"
                  shape="round"
                  size="small"
                  onClick={(e) => {
                    e.preventDefault();
                    setModalURL(`${location.origin}/tu_voto_cuenta/${id}`);
                    setModalOpen(!modalOpen);
                  }}
                >
                  Comparte
                </Button>
              </CardVoting>
            </a>
          </Link>
        )
      )}
    </>
  );
}
