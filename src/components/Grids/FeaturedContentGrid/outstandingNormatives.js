import React, { useState } from "react";
import ProposalCard from "@components/Cards/Proposals/ProposalCardComment";
import Share from "@components/Modals/Share";
import Router from "next/router";
import { useSelector } from "react-redux";

export default function OutstandingVotes({ elements }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");
  const { dataUser } = useSelector((storage) => storage.auth);

  return (
    <>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
        userId={dataUser.id}
      />
      {elements.map((norm) => (
        <ProposalCard
          key={norm.id}
          width={"100%"}
          height={"max-content"}
          title={norm.title}
          description={{
            text: `${norm.content}`,
            link: `/normativas/${norm.id}`,
          }}
          isFinished={norm.status}
          numberComments={norm.comments}
          zones={["all"]}
          onClickLogin={() => Router.push(`/normativas/${norm.id}`)}
          onClickShare={(e) => {
            e.preventDefault();
            setModalURL(`${location.origin}/normativas/${norm.id}`);
            setModalOpen(!modalOpen);
          }}
        />
      ))}
    </>
  );
}
