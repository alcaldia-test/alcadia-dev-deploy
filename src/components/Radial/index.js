import React, { useState } from "react";
import { Checkbox } from "antd";
import "antd/lib/checkbox/style/index.css";
import styles from "./Radial.module.scss";
import { textCapitalize } from "@utils/capitalizeFirstLetter";

function Radial({
  title,
  value,
  id,
  onChange = () => {},
  disabled,
  name,
  ...args
}) {
  const [active, setActive] = useState(false);
  return (
    <div className={styles.radialContainer}>
      <Checkbox
        onClick={() => setActive(!active)}
        value={value}
        id={id}
        disabled={disabled}
        name={name}
        onChange={() => onChange(value)}
        className={`${styles.radial} ? ${active && styles.active} `}
        {...args}
      >
        {textCapitalize(title)}
      </Checkbox>
    </div>
  );
}

export default Radial;
