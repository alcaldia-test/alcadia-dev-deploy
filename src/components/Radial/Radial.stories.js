import React from "react";
import { withDesign } from "storybook-addon-designs";
import Radial from "./index";

export default {
  title: "Components/Radial",
  component: Radial,
  decorators: [withDesign],
  argTypes: {
    title: "string",
    value: "string",
  },
};

const Template = (args) => <Radial {...args} />;
export const Default = Template.bind({});

Default.args = {
  title: "titulo del radio button",
  value: "value del check",
  disabled: false,
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=10%3A2996",
  },
};
