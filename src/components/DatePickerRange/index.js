import React from "react";
import { DatePicker } from "antd";
import moment from "moment";
import "antd/lib/date-picker/style/index.css";
import "moment/locale/es";

const DatePickerRange = ({
  dateFormat,
  fechaInicial,
  onChange,
  onOk,
  fechaFinal,
  placeholder,
  showTime = true,
  ...args
}) => {
  const { RangePicker } = DatePicker;
  const _initialDate = moment(fechaInicial, dateFormat).isValid()
    ? moment(fechaInicial, dateFormat)
    : undefined;
  const _finalDate = moment(fechaFinal, dateFormat).isValid()
    ? moment(fechaFinal, dateFormat)
    : undefined;

  return (
    <>
      <RangePicker
        showTime={showTime ? { format: "HH:mm" } : undefined}
        onChange={onChange}
        onOk={onOk}
        placeholder={
          placeholder ?? ["Fecha de Inicio", "Fecha de Finalización"]
        }
        defaultValue={_initialDate ? [_initialDate, _finalDate] : undefined}
        format={dateFormat}
        {...args}
      />
    </>
  );
};

export default DatePickerRange;
