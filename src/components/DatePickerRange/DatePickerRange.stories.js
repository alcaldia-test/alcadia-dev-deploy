import React from "react";
import { withDesign } from "storybook-addon-designs";
import DatePickerRange from "./index";

export default {
  title: "Components/DatePickerRange",
  component: DatePickerRange,
  decorators: [withDesign],
};

const Template = (args) => <DatePickerRange {...args} />;
export const Default = Template.bind({});

Default.args = {
  dateFormat: "YYYY-MM-DD",
  fechaInicial: "2020-12-23",
  fechaFinal: "2020-12-24",
  showTime: true,
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=6%3A2809",
  },
};
