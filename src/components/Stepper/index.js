import { Steps } from "antd";
import "antd/lib/steps/style/index.css";
import "antd/lib/style/index.css";
import styles from "./Stepper.module.scss";

import { LikeOutlined } from "@ant-design/icons";

/**
 * @param {number} step - Current step
 * @param {array} steps - Array of steps
 * @param {function} setStep - State hook/function to set the current step
 *
 * @returns {component} - Stepper component
 */

const Stepper = ({ steps = [1, 2, 3, 4, 5], setStep = () => {}, step = 2 }) => {
  const { Step } = Steps;

  return (
    <div className={styles.stepper}>
      <Steps current={step} onChange={(e) => setStep(e)}>
        {steps.map((el, i) => (
          <Step
            key={i}
            icon={
              step > i ? (
                <div id="step" className={`${styles.step} ${styles.prev}`}>
                  <LikeOutlined />
                </div>
              ) : step === i ? (
                <div id="step" className={`${styles.step} ${styles.current}`}>
                  {i + 1}
                </div>
              ) : (
                <div id="step" className={`${styles.step} ${styles.next}`}>
                  {i + 1}
                </div>
              )
            }
          />
        ))}
      </Steps>
    </div>
  );
};

export default Stepper;
