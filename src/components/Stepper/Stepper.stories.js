import { withDesign } from "storybook-addon-designs";

import Stepper from "./index.js";

export default {
  title: "Components/Stepper",
  component: Stepper,
  decorators: [withDesign],
  argTypes: {
    steps: Array,
    step: Number,
  },
};

const Template = (args) => <Stepper {...args}>Label</Stepper>;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlStepper = {
  default: "10%3A3239",
  //   large: "10%3A3390",
};

export const Default = Template.bind({});
Default.args = {
  steps: [1, 2, 3, 4, 5],
  step: 2,
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlStepper.default}`,
  },
};
