import { CaretDownOutlined, CaretRightOutlined } from "@ant-design/icons";
import { useState, useEffect } from "react";

import "antd/lib/collapse/style/index.css";

const SelectCollapse = ({
  el = {
    name: "Zona central",
    zones: [
      { id: 1, zone: "San Pedro", checked: false },
      { id: 2, zone: "Prado", checked: false },
    ],
  },
  onSelect,
  name,
  showZones,
  ...props
}) => {
  const [area, setArea] = useState(el);
  const [dropDown, setDropDown] = useState(false);

  useEffect(() => {
    setArea({
      ...area,
      zones: el.zones,
      checked: el.checked,
    });
  }, [el]);

  const handleDown = () => {
    setDropDown((prev) => !prev);
  };

  const handleSelectAll = (e) => {
    el.zones = area.zones.map((x) => ({ ...x, checked: e.target.checked }));
    setArea({
      ...area,
      zones: el.zones,
      checked: el.checked,
    });
    if (onSelect) {
      onSelect({
        id: el.id,
        name: el.name,
        selected: el.zones.filter((x) => x.checked === true),
        selectedAll: true,
      });
    }
  };

  const handleSelect = (e) => {
    el.zones = area.zones.map((x) =>
      x.id === +e.target.value ? { ...x, checked: e.target.checked } : x
    );
    setArea({
      ...area,
      zones: el.zones,
    });
    if (onSelect) {
      onSelect({
        id: el.id,
        name: el.name,
        selected: el.zones.filter((x) => x.checked === true),
        selectedAll:
          el.zones.filter((x) => x.checked === true).length === el.zones.length,
      });
    }
  };

  return (
    <div {...props} className="flex flex-wrap w-full">
      <div
        className="flex-initial w-full items-center flex"
        style={{ marginBottom: 4 }}
      >
        {el.zones.length && dropDown && showZones ? (
          <CaretDownOutlined onClick={handleDown} />
        ) : el.zones.length && showZones ? (
          <CaretRightOutlined onClick={handleDown} />
        ) : null}
        &nbsp;
        <label>
          <input
            type="checkbox"
            onChange={handleSelectAll}
            checked={area.checked}
          />
          &nbsp;{area.name}
        </label>
        {/* <span></span> */}
      </div>
      {showZones &&
        area.zones.map((x, i) => (
          <div
            key={i}
            className="flex-initial w-full ml-5 items-center"
            style={{ display: dropDown ? "flex" : "none" }}
          >
            <input
              type="checkbox"
              value={x.id}
              checked={x.checked}
              onChange={handleSelect}
            />
            &nbsp;
            <span>{x.zone}</span>
          </div>
        ))}
    </div>
  );
};

export default SelectCollapse;
