import React, { useEffect } from "react";
import SelectCollapse from "./SelectCollapse";

/**
 *
 * @param list Base list of data recivec by the component
 * @param setList not in use
 * @param onSelect That is callback which return ara array on object {name:MacroZone name,zones:[{id,name}]}
 * @param name The name will return as a tag name
 * @param props Rest props of Button in AntDesign
 * @returns Dropdownlist with twi level
 */

const SelectList = ({
  list = [
    {
      name: "Zona central",
      zones: [
        { id: 1, zone: "San Pedro", checked: false },
        { id: 2, zone: "Prado", checked: false },
      ],
    },
    {
      name: "Macrodistrito",
      zones: [
        { id: 1, zone: "Distrito 1", checked: false },
        { id: 2, zone: "Distrito 2", checked: false },
      ],
    },
  ],
  setList,
  onSelect,
  name = "selectedList",
  showZones = true,
  ...props
}) => {
  useEffect(() => {
    if (setList) {
      setList(list);
    }
  }, [list]);

  return (
    <>
      {list.map((el, i) => (
        <SelectCollapse
          key={i}
          i={i}
          el={el}
          onSelect={onSelect}
          name={name}
          showZones={showZones}
          {...props}
        />
      ))}
    </>
  );
};

export default SelectList;
