import {
  InfoCircleOutlined,
  CheckCircleFilled,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import React from "react";
import styles from "./info.module.scss";
import { useCheckCircle, useCheckDefaultType } from "./hooks";

function Info({ children, type }) {
  const simbolType = useCheckCircle(type);
  const infoType = useCheckDefaultType(type);

  return (
    <div
      className={`${styles[infoType]} ${styles.container} flex items-center justify-start w-full`}
    >
      {type === "info" ? (
        <InfoCircleOutlined
          className={`${styles.infoIcon} pr-2 pl-1 font-bold`}
        />
      ) : simbolType ? (
        <CheckCircleFilled
          className={`${styles.infoIcon} pr-2 pl-1 font-bold`}
        />
      ) : (
        <ExclamationCircleOutlined
          className={`${styles.infoIcon} pr-2 pl-1 font-bold`}
        />
      )}

      <div className={`${styles.messageText}  font-bold`}>{children}</div>
    </div>
  );
}

export default Info;
