import React from "react";
import { withDesign } from "storybook-addon-designs";
import Info from "./index";

export default {
  title: "Components/Info",
  component: Info,
  decorators: [withDesign],
};

const Template = (args) => <Info {...args}>{args.children}</Info>;

export const Default = Template.bind({});
Default.args = {
  children: "default info text",
  type: "info",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A12969",
  },
};

export const info = Template.bind({});
info.args = {
  children: " info text with info circle outlined",
  type: "info",
};

info.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A12969",
  },
};

export const warn = Template.bind({});
warn.args = {
  children: "warn text with info circle outlined",
  type: "warn",
};

warn.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A12969",
  },
};

export const done = Template.bind({});
done.args = {
  children: "done text info with check circle",
  type: "done",
};

done.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A12969",
  },
};

export const error = Template.bind({});
error.args = {
  children: "error text info with check circle",
  type: "error",
};

done.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A12969",
  },
};
