export function useCheckCircle(type) {
  switch (type) {
    case "done":
      return true;
    case "error":
      return true;
    default:
      return false;
  }
}

export function useCheckDefaultType(type) {
  switch (type) {
    case null:
      return "info";
    case undefined:
      return "info";
    case typeof type !== "string":
      return "info";
    default:
      return type;
  }
}
