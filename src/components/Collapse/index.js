import React from "react";
import { Collapse as AntdCollapse } from "antd";
import Checkboxes from "@components/Checkboxes";
import "antd/lib/collapse/style/index.css";
import styles from "./Collapse.module.scss";
const { Panel } = AntdCollapse;
const defaultItems = ["Item1", "Item2", "Item3"];
const defaultHeader = "Collapse";
/**
 * @param header Header or Tittle of this Collapse component
 * @param items Array of Strings with each item
 * @param typeCheckBox String that denotes type of this Checkbox
 * @param props Default props of a Collapse Component of Ant Design.
 * @return An Collapse Component with one text and one checkbox.
 */
function Collapse({
  header = defaultHeader,
  items = defaultItems,
  typeCheckBox = "secondary",
  onChange,
  ...props
}) {
  return (
    <AntdCollapse
      {...props}
      className={`${styles.collapse}`}
      defaultActiveKey={["1"]}
    >
      <Panel header={header} onclick={props.onclick} key="1">
        {items.map((item, index) => (
          <div key={index}>
            <Checkboxes
              type={typeCheckBox}
              onChange={onChange}
              defaultChecked={item.value}
              value={item.id}
            >
              <span className={`${styles["collapse-item"]}`}>{item.name}</span>
            </Checkboxes>
          </div>
        ))}
      </Panel>
    </AntdCollapse>
  );
}
export default Collapse;
