import { withDesign } from "storybook-addon-designs";

import Collapse from "./index.js";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idCollapse = {
  default: "745%3A7090",
};

export default {
  title: "Components/Collapse",
  component: Collapse,
  decorators: [withDesign],
  argTypes: {
    items: "array",
    header: "string",
    typeCheckBox: { options: ["primary", "secondary"], control: "select" },
    accordion: { control: "boolean" },
    bordered: { control: "boolean" },
  },
};

const Template = (args) => <Collapse {...args} />;

export const Default = Template.bind({});
Default.args = {
  items: ["Item", "Item", "Item"],
  header: "Collapse",
  typeCheckBox: "primary",
};
Default.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idCollapse.default}`,
  },
};
