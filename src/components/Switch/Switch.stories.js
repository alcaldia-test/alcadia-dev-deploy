import React from "react";
// Se importa decorador para anadir link de figma
import { withDesign } from "storybook-addon-designs";
// importamos componente al que le crearemos la story
import Switch from "./index";
// Se importa el hook del switch para su uso en los templates
import useSwitch from "./hooks.js";

// Datos necesarios para las stories
export default {
  title: "Components/Switch (Toggle Switch)",
  component: Switch,
  // agregamos el decorador de figma
  decorators: [withDesign],
};

// Definimos un template para poder crear vistas por estados
const Template = (args) => {
  // Separar estado inicial de resto de argumentos
  const { state, ...rest } = args;
  // Se pasa el estado inicial al hook y el resultado y demás argumentos al componente
  const switchOne = useSwitch(state);
  return (
    <>
      <Switch {...switchOne} {...rest} />
    </>
  );
};

/**
 * Default state
 *
 * @param state: 'Off'
 * @param size: 'md'
 * @param disabled: false
 * @param loading: false
 */

// Creamos la vista Default
export const Default = Template.bind({});

// Definimos los valores por defecto segun el estado
Default.args = {
  size: "md",
  state: "Off",
  disabled: false,
  loading: false,
};

// Link del embebido en figma para mostrar en la pestaña Design
Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};
