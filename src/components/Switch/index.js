import "antd/lib/switch/style/index.css";
import { Switch as AntdSwitch } from "antd";

import styles from "./Switch.module.scss";

// Parsed size of switch (according to ANTD Switch)
const parsedSize = {
  sm: "small",
  md: "default",
};

/**
 * Toggle Switch Presentational Component
 * Powered by ANTD Switch component
 */
const Switch = ({ size = "md", isChecked = false, toggle, ...rest }) => {
  return (
    <div className={styles.switch}>
      <AntdSwitch
        checked={isChecked}
        onClick={toggle}
        size={parsedSize[size]}
        {...rest}
      />
    </div>
  );
};

export default Switch;
