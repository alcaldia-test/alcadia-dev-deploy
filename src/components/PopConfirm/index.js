import React from "react";

import { Popconfirm } from "antd";

import "antd/lib/popconfirm/style/css";
import styles from "./PopConfirm.module.scss";
import buttonStyles from "@components/Button/Button.module.scss";
/**
 * @param sizeButton String that define size of these buttons, can only be [ large , medium , small , small-x , super-small ]
 * @param typeButton String that define type of these buttons, can only be [ primary , secondary , link ]
 * @param props Rest props of a PopConfirm in Ant Design.
 * @return A PopConfirm
 */

function PopConfirm({
  sizeButton,
  typeButton,
  children,
  cancelButtonProps,
  okButtonProps,
  ...props
}) {
  const buttonProps = {
    className: `${buttonStyles[typeButton]} ${buttonStyles[sizeButton]}`,
  };
  return (
    <Popconfirm
      cancelButtonProps={{
        ...cancelButtonProps,
        ...buttonProps,
        type: undefined,
        size: undefined,
      }}
      okButtonProps={{
        ...okButtonProps,
        ...buttonProps,
        type: undefined,
        size: undefined,
      }}
      className={`${styles.popconfirm}`}
      {...props}
    >
      {children}
    </Popconfirm>
  );
}
export default PopConfirm;
