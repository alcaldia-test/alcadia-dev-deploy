import React from "react";
import { withDesign } from "storybook-addon-designs";
import PopConfirm from "./index.js";

export default {
  title: "Components/PopConfirm",
  component: PopConfirm,
  decorators: [withDesign],
  argTypes: {
    cancelText: "string",
    okText: "string",
    title: "string",
    typeButton: {
      options: ["primary", "secondary", "link"],
      control: "select",
    },
    sizeButton: {
      options: ["large", "medium", "small", "small-x", "super-small"],
      control: "select",
    },
    trigger: {
      options: ["click", "focus", "contextMenu", "hover"],
      control: "select",
    },
  },
};

const Template = (args) => (
  <PopConfirm {...args}>
    <a>Presiona aqui</a>
  </PopConfirm>
);

export const Default = Template.bind({});
Default.args = {
  cancelText: "No",
  okText: "Si",
  title: "¿Desea quitar la aprobación de esta propuesta?",
  sizeButton: "small-x",
  typeButton: "secondary",
  trigger: "click",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/CgPBHjaHbUBF8imE32lMNe/Alcald%C3%ADa-%2F-5.-Dashboard-Product?node-id=155%3A36260",
  },
};
