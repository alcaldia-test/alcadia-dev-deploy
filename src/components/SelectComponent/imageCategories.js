import React, { useEffect, useState } from "react";
import { Select, SelectProps } from "antd";
import { batch } from "react-redux";
import { CATEGORIES_IMAGES } from "../../config";

const { Option } = Select;

const ImageCategories = ({
  onChange,
  all = false,
  defaultValue,
  className,
  ...otherProps
}) => {
  const [state, setState] = useState();

  useEffect(() => {
    setState(CATEGORIES_IMAGES.length === 0 ? undefined : defaultValue);
  }, []);

  const handleChange = (value) => {
    batch(() => {
      setState(value);
      onChange(value);
    });
  };

  return (
    <Select
      value={state}
      className={className || "gx-w-100"}
      placeholder="Seleccione una Categoría"
      optionFilterProp="children"
      onChange={handleChange}
      {...otherProps}
    >
      {all && <Option value="">Todos</Option>}
      {CATEGORIES_IMAGES.map((values) => (
        <Option key={values.value} value={values.value}>
          {values.label}
        </Option>
      ))}
    </Select>
  );
};

ImageCategories.propTypes = {
  ...SelectProps,
};

export default ImageCategories;
