import React, { useEffect, useState } from "react";
import { Select } from "antd";
import { batch, useSelector } from "react-redux";

const { Option } = Select;

export const Difficulties = ({
  onChange,
  all = false,
  defaultValue,
  className,
  ...otherProps
}) => {
  const { difficulties } = useSelector(({ global }) => global);
  const [state, setState] = useState();

  useEffect(() => {
    setState(difficulties.length === 0 ? undefined : defaultValue);
  }, [difficulties]);

  const handleChange = (value) => {
    batch(() => {
      setState(value);
      onChange(value);
    });
  };

  return (
    <Select
      value={state}
      className={className || "gx-w-100"}
      placeholder="Seleccione un nivel de Dificultad"
      optionFilterProp="children"
      onChange={handleChange}
      {...otherProps}
    >
      {all && <Option value="">Todos</Option>}
      {difficulties.map((values) => (
        <Option key={values.id} value={values.id}>
          {values.title}
        </Option>
      ))}
    </Select>
  );
};
