import { TypesExercise } from "./typesExercise";
import { Intensities } from "./intensities";
import { Difficulties } from "./difficulties";
import ImageCategories from "./imageCategories";
import Exercises from "./exercises";
import Blocks from "./blocks";
import Trainings from "./trainings";

export const SelectComponent = {
  Intensities,
  Difficulties,
  TypesExercise,
  ImageCategories,
  Exercises,
  Blocks,
  Trainings,
};
