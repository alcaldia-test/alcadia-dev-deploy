import React, { useEffect } from "react";
import { Divider, Button, Row, Col, Image, Input, Tabs } from "antd";
import Modal from "antd/lib/modal/Modal";
import { batch, useDispatch, useSelector } from "react-redux";
import { PlusOutlined, RetweetOutlined } from "@ant-design/icons";
import { customUseReducer } from "@utils/customHooks";
import { TableApp } from "@components/TableApp";
import { SelectComponent } from "@components/SelectComponent";
import { requestExercisesStart } from "@containers/Admin/Exercises/redux/actions";
const { TabPane } = Tabs;

const initialState = {
  search: "",
  modal: false,
  value: [],
  filter: {
    typeExerciseId: undefined,
  },
};

function Exercise({ value = [], onChange = () => {} }) {
  const dispatch = useDispatch();
  const [state, dispatchComponent] = customUseReducer(initialState);
  const exercises = useSelector(({ exercises }) => exercises);
  const blocks = useSelector(({ blocks }) => blocks);

  useEffect(() => {
    dispatchComponent({ value });
  }, [value]);

  const handleSelected = (exerciseId) => {
    const isExist = state.value.includes(exerciseId);
    if (isExist)
      return dispatchComponent({
        value: state.value.filter((values) => values !== exerciseId),
      });
    dispatchComponent({ value: [...state.value, exerciseId] });
  };

  const handleSave = () => {
    batch(() => {
      onChange(state.value);
      dispatchComponent({ modal: false });
    });
  };

  const initialRequest = async () => {
    dispatch(requestExercisesStart());
  };

  return (
    <>
      <Row
        className="focus-container-select noselect"
        style={{ flexDirection: "row", marginBottom: 16 }}
        onClick={() => dispatchComponent({ modal: true })}
      >
        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
          <div className="container-select">
            <div style={{ marginTop: 8 }}>
              <PlusOutlined style={{ fontSize: 32 }} />
            </div>
            <h3>Click aquí para seleccionar</h3>
            <p className="gx-text-muted" style={{ textAlign: "center" }}>
              Aquí podrá seleccionar los ejercicios disponibles
            </p>
          </div>
        </Col>
      </Row>
      <Modal
        title="Lista de Ejercicios"
        centered
        visible={state.modal}
        onCancel={() => dispatchComponent({ modal: false })}
        footer={[
          <Button
            key="close"
            onClick={() => dispatchComponent({ modal: false })}
          >
            SALIR
          </Button>,
          <Button key="ok" onClick={handleSave} type="primary">
            GUARDAR
          </Button>,
        ]}
        confirmLoading={blocks.loading}
        width="100%"
      >
        <p className="gx-text-grey">Detalles del Contenedor</p>
        <h2 className="gx-text-uppercase gx-text-black gx-font-weight-bold gx-fnd-title">
          LISTA DE EJERCICIOS
        </h2>
        <p>
          Este contenedor tiene como funcionalidad la selección de ejercicios
          disponibles
        </p>
        <Row gutter={24} style={{ flexDirection: "row" }}>
          <Col xl={22} lg={22} md={22} sm={24} xs={24}>
            <Input.Search
              placeholder="Buscar por nombre de ejercicio"
              allowClear
              enterButton="Buscar"
              onSearch={(search) => dispatchComponent({ search })}
            />
          </Col>
          <Col xl={2} lg={2} md={2} sm={24} xs={24}>
            <Button
              block
              type="dashed"
              icon={<RetweetOutlined />}
              title="Actualizar"
              onClick={initialRequest}
            />
          </Col>
        </Row>

        <Tabs>
          <TabPane tab="Ejercicios" style={{ padding: 4 }}>
            <p className="gx-text-grey">Filtro de busqueda</p>
            <Row gutter={24} style={{ flexDirection: "row", marginBottom: 16 }}>
              <Col span={24}>
                <SelectComponent.TypesExercise
                  value={state.filter.typeExerciseId}
                  onChange={(typeExerciseId) =>
                    dispatchComponent({ filter: { typeExerciseId } })
                  }
                  className="gx-w-100"
                />
              </Col>
            </Row>
            <Divider dashed />
            <p>EJERCICIOS</p>
            <TableApp
              loading={exercises.loading}
              dataSource={exercises.data
                .filter((exercise) =>
                  state.filter.typeExerciseId === "" ||
                  !state.filter.typeExerciseId
                    ? true
                    : exercise.type.id === state.filter.typeExerciseId
                )
                .filter((exercise) =>
                  state.search === ""
                    ? true
                    : exercise.title.includes(state.search)
                )}
              renderItem={(exercise) => {
                const isSelected = state.value.includes(exercise.id);
                return (
                  <div
                    className="container-list-image"
                    style={isSelected ? { border: "3px dashed #038fde" } : {}}
                  >
                    <div style={{ position: "relative", textAlign: "center" }}>
                      <Image
                        src={exercise.image.link}
                        style={{
                          height: 150,
                          borderTopLeftRadius: 8,
                          borderTopRightRadius: 8,
                          objectFit: "cover",
                        }}
                      />
                      <div className="container-name-image">
                        {exercise.title}
                      </div>
                    </div>
                    <div
                      className="container-buttom-image"
                      onClick={() => handleSelected(exercise.id)}
                    >
                      {isSelected
                        ? "Quitar Ejercicio"
                        : "Seleccionar Ejercicio"}
                    </div>
                  </div>
                );
              }}
            />
          </TabPane>
        </Tabs>
      </Modal>
    </>
  );
}

export default Exercise;
