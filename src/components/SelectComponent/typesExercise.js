import React, { useEffect, useState } from "react";
import { Select } from "antd";
import { batch, useSelector } from "react-redux";

const { Option } = Select;

export const TypesExercise = ({
  onChange,
  all = false,
  defaultValue,
  className,
  ...otherProps
}) => {
  const { typesExercise } = useSelector(({ global }) => global);
  const [state, setState] = useState();

  useEffect(() => {
    setState(typesExercise.length === 0 ? undefined : defaultValue);
  }, [typesExercise]);

  const handleChange = (value) => {
    batch(() => {
      setState(value);
      onChange(value);
    });
  };

  return (
    <Select
      value={state}
      className={className || "gx-w-100"}
      placeholder="Seleccione un Tipo"
      optionFilterProp="children"
      onChange={handleChange}
      {...otherProps}
    >
      {all && <Option value="">Todos</Option>}
      {typesExercise.map((values) => (
        <Option key={values.id} value={values.id}>
          {values.title}
        </Option>
      ))}
    </Select>
  );
};
