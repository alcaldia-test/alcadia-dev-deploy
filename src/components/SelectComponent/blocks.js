import React, { useEffect } from "react";
import { Divider, Button, Row, Col, Input, Tabs } from "antd";
import Modal from "antd/lib/modal/Modal";
import { batch, useDispatch, useSelector } from "react-redux";
import {
  PlusOutlined,
  RetweetOutlined,
  GroupOutlined,
} from "@ant-design/icons";
import { customUseReducer } from "@utils/customHooks";
import { TableApp } from "@components/TableApp";
import { SelectComponent } from "@components/SelectComponent";
import { requestBlocksStart } from "@containers/Admin/Blocks/redux/actions";
const { TabPane } = Tabs;

const initialState = {
  search: "",
  modal: false,
  value: [],
  filter: {
    typeExerciseId: undefined,
  },
};

function Blocks({
  value,
  onChange = () => {},
  disabled,
  defaultValueOfType = {},
}) {
  const dispatch = useDispatch();
  const [state, dispatchComponent] = customUseReducer(initialState);
  const blocks = useSelector(({ blocks }) => blocks);
  const isEmptyBlocks = blocks.data.length === 0;

  useEffect(() => {
    state.modal && isEmptyBlocks && initialRequest();
  }, [state.modal]);

  useEffect(() => {
    dispatchComponent({ filter: { ...state.filter, ...defaultValueOfType } });
  }, []);

  useEffect(() => {
    value && dispatchComponent({ value });
  }, [value]);

  const handleSelected = (blockId) => {
    const ifExistBlock = state.value.includes(blockId);
    if (ifExistBlock)
      return dispatchComponent({
        value: state.value.filter((values) => values !== blockId),
      });
    dispatchComponent({ value: [...state.value, blockId] });
  };

  const handleSave = () => {
    batch(() => {
      onChange(state.value);
      dispatchComponent({ modal: false });
    });
  };

  const initialRequest = async () => {
    dispatch(requestBlocksStart());
  };

  return (
    <>
      <Row
        className={`focus-container-select noselect ${
          disabled ? "ant-input-disabled" : "gx-pointer"
        }`}
        style={{ flexDirection: "row", marginBottom: 16 }}
        onClick={() => !disabled && dispatchComponent({ modal: true })}
      >
        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
          <div className="container-select">
            <div style={{ marginTop: 8 }}>
              <PlusOutlined style={{ fontSize: 32 }} />
            </div>
            <h3>Click aquí para seleccionar</h3>
            <p className="gx-text-muted" style={{ textAlign: "center" }}>
              Aquí podrá seleccionar los Bloques disponibles
            </p>
          </div>
        </Col>
      </Row>
      <Modal
        title="Lista de Bloques"
        centered
        visible={state.modal}
        onCancel={() => dispatchComponent({ modal: false })}
        footer={[
          <Button
            key="close"
            onClick={() => dispatchComponent({ modal: false })}
          >
            SALIR
          </Button>,
          <Button key="ok" onClick={handleSave} type="primary">
            GUARDAR
          </Button>,
        ]}
        confirmLoading={blocks.loading}
        width="100%"
      >
        <p className="gx-text-grey">Detalles del Contenedor</p>
        <h2 className="gx-text-uppercase gx-text-black gx-font-weight-bold gx-fnd-title">
          LISTA DE BLOQUES
        </h2>
        <p>
          Este contenedor tiene como funcionalidad la selección de bloques
          disponibles
        </p>
        <Row gutter={24} style={{ flexDirection: "row" }}>
          <Col xl={22} lg={22} md={22} sm={24} xs={24}>
            <Input.Search
              placeholder="Buscar por nombre de bloque"
              allowClear
              enterButton="Buscar"
              onSearch={(search) => dispatchComponent({ search })}
            />
          </Col>
          <Col xl={2} lg={2} md={2} sm={24} xs={24}>
            <Button
              block
              type="dashed"
              icon={<RetweetOutlined />}
              title="Actualizar"
              onClick={initialRequest}
            />
          </Col>
        </Row>

        <Tabs>
          <TabPane tab="Bloques" style={{ padding: 4 }}>
            <p className="gx-text-grey">Filtro de busqueda</p>
            <Row gutter={24} style={{ flexDirection: "row", marginBottom: 16 }}>
              <Col span={24}>
                <SelectComponent.TypesExercise
                  value={state.filter.typeExerciseId}
                  onChange={(typeExerciseId) =>
                    dispatchComponent({ filter: { typeExerciseId } })
                  }
                  className="gx-w-100"
                />
              </Col>
            </Row>
            <Divider dashed />
            <p>BLOQUES</p>
            <TableApp
              loading={blocks.loading}
              dataSource={blocks.data
                .filter((block) =>
                  state.filter.typeExerciseId === "" ||
                  !state.filter.typeExerciseId
                    ? true
                    : block.type.id === state.filter.typeExerciseId
                )
                .filter((block) =>
                  state.search === ""
                    ? true
                    : block.title.includes(state.search)
                )}
              renderItem={(block) => {
                const isSelected = state.value.includes(block.id);
                return (
                  <div
                    className="container-list-image"
                    style={isSelected ? { border: "3px dashed #038fde" } : {}}
                  >
                    <div style={{ position: "relative", textAlign: "center" }}>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <GroupOutlined style={{ fontSize: 72, margin: 8 }} />
                      </div>
                      <div className="container-name-image">{block.title}</div>
                    </div>
                    <div
                      className="container-buttom-image"
                      onClick={() => handleSelected(block.id)}
                    >
                      {isSelected ? "Quitar Bloque" : "Seleccionar Bloque"}
                    </div>
                  </div>
                );
              }}
            />
          </TabPane>
        </Tabs>
      </Modal>
    </>
  );
}

export default Blocks;
