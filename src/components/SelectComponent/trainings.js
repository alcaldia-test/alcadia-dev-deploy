import React, { useEffect } from "react";
import { Divider, Button, Row, Col, Input, Tabs, Image } from "antd";
import Modal from "antd/lib/modal/Modal";
import { batch, useDispatch, useSelector } from "react-redux";
import { PlusOutlined, RetweetOutlined } from "@ant-design/icons";
import { customUseReducer } from "@utils/customHooks";
import { TableApp } from "@components/TableApp";
import { SelectComponent } from "@components/SelectComponent";
import { requestTrainingsStart } from "@containers/Admin/Trainings/redux/actions";
const { TabPane } = Tabs;

const initialState = {
  search: "",
  modal: false,
  value: [],
  filter: {
    difficultyId: undefined,
    intensityId: undefined,
  },
};

function Trainings({
  value,
  onChange = () => {},
  disabled,
  defaultValueOfType = {},
}) {
  const dispatch = useDispatch();
  const [state, dispatchComponent] = customUseReducer(initialState);
  const trainings = useSelector(({ trainings }) => trainings);

  useEffect(() => {
    dispatchComponent({ filter: { ...state.filter, ...defaultValueOfType } });
  }, []);

  useEffect(() => {
    state.modal && initialRequest();
  }, [state.modal]);

  useEffect(() => {
    value && dispatchComponent({ value });
  }, [value]);

  const handleSelected = (trainingId) => {
    const ifExistBlock = state.value.includes(trainingId);
    if (ifExistBlock)
      return dispatchComponent({
        value: state.value.filter((values) => values !== trainingId),
      });
    dispatchComponent({ value: [...state.value, trainingId] });
  };

  const handleSave = () => {
    batch(() => {
      onChange(state.value);
      dispatchComponent({ modal: false });
    });
  };

  const initialRequest = async () => {
    dispatch(requestTrainingsStart());
  };

  const handleChangeFilter = (filter) => {
    dispatchComponent((state) => ({ filter: { ...state.filter, ...filter } }));
  };

  const applyFilter = (data) => {
    return data
      .filter(
        (trainings) =>
          (state.filter.difficultyId === "" || !state.filter.difficultyId
            ? true
            : trainings.difficulty.id === state.filter.difficultyId) &&
          (state.filter.intensityId === "" || !state.filter.intensityId
            ? true
            : trainings.intensity.id === state.filter.intensityId) &&
          (state.search === "" ? true : trainings.title.includes(state.search))
      )
      .map((values, key) => ({ key, ...values }));
  };

  return (
    <>
      <Row
        className={`focus-container-select noselect ${
          disabled ? "ant-input-disabled" : "gx-pointer"
        }`}
        style={{ flexDirection: "row", marginBottom: 16 }}
        onClick={() => !disabled && dispatchComponent({ modal: true })}
      >
        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
          <div className="container-select">
            <div style={{ marginTop: 8 }}>
              <PlusOutlined style={{ fontSize: 32 }} />
            </div>
            <h3>Click aquí para seleccionar</h3>
            <p className="gx-text-muted" style={{ textAlign: "center" }}>
              Aquí podrá seleccionar los Entrenamientos disponibles
            </p>
          </div>
        </Col>
      </Row>
      <Modal
        title="Lista de Entrenamientos"
        centered
        visible={state.modal}
        onCancel={() => dispatchComponent({ modal: false })}
        footer={[
          <Button
            key="close"
            onClick={() => dispatchComponent({ modal: false })}
          >
            SALIR
          </Button>,
          <Button key="ok" onClick={handleSave} type="primary">
            GUARDAR
          </Button>,
        ]}
        confirmLoading={trainings.loading}
        width="100%"
      >
        <p className="gx-text-grey">Detalles del Contenedor</p>
        <h2 className="gx-text-uppercase gx-text-black gx-font-weight-bold gx-fnd-title">
          LISTA DE ENTRENAMIENTOS
        </h2>
        <p>
          Este contenedor tiene como funcionalidad la selección de bloques
          disponibles
        </p>
        <Row gutter={24} style={{ flexDirection: "row" }}>
          <Col xl={22} lg={22} md={22} sm={24} xs={24}>
            <Input.Search
              placeholder="Buscar por nombre de bloque"
              allowClear
              enterButton="Buscar"
              onSearch={(search) => dispatchComponent({ search })}
            />
          </Col>
          <Col xl={2} lg={2} md={2} sm={24} xs={24}>
            <Button
              block
              type="dashed"
              icon={<RetweetOutlined />}
              title="Actualizar"
              onClick={initialRequest}
            />
          </Col>
        </Row>

        <Tabs>
          <TabPane tab="Entrenamientos" style={{ padding: 4 }}>
            <p className="gx-text-grey">Filtro de busqueda</p>
            <Row gutter={24} style={{ flexDirection: "row", marginBottom: 16 }}>
              <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                <SelectComponent.Difficulties
                  all={true}
                  value={state.filter.difficultyId}
                  onChange={(difficultyId) =>
                    handleChangeFilter({ difficultyId })
                  }
                  className="gx-w-100"
                />
              </Col>
              <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                <SelectComponent.Intensities
                  all={true}
                  value={state.filter.intensityId}
                  onChange={(intensityId) =>
                    handleChangeFilter({ intensityId })
                  }
                  className="gx-w-100"
                />
              </Col>
            </Row>
            <Divider dashed />
            <p>ENTRENAMIENTOS</p>
            <TableApp
              loading={trainings.loading}
              dataSource={applyFilter(trainings.data)}
              renderItem={(training) => {
                const isSelected = state.value.includes(training.id);
                return (
                  <div
                    className="container-list-image"
                    style={isSelected ? { border: "3px dashed #038fde" } : {}}
                  >
                    <div style={{ position: "relative", textAlign: "center" }}>
                      <Image
                        src={training.image.link}
                        style={{
                          height: 150,
                          borderTopLeftRadius: 8,
                          borderTopRightRadius: 8,
                          objectFit: "cover",
                        }}
                      />
                      <div className="container-name-image">
                        {training.title}
                      </div>
                    </div>
                    <div
                      className="container-buttom-image"
                      onClick={() => handleSelected(training.id)}
                    >
                      {isSelected
                        ? "Quitar Entrenamiento"
                        : "Seleccionar Entrenamiento"}
                    </div>
                  </div>
                );
              }}
            />
          </TabPane>
        </Tabs>
      </Modal>
    </>
  );
}

export default Trainings;
