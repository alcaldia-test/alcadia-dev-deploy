import React, { useEffect, useState } from "react";
import { Select } from "antd";
import { batch, useSelector } from "react-redux";

const { Option } = Select;

export const Intensities = ({
  onChange,
  all = false,
  defaultValue,
  className,
  ...otherProps
}) => {
  const { intensities } = useSelector(({ global }) => global);
  const [state, setState] = useState();

  useEffect(() => {
    setState(intensities.length === 0 ? undefined : defaultValue);
  }, [intensities]);

  const handleChange = (value) => {
    batch(() => {
      setState(value);
      onChange(value);
    });
  };

  return (
    <Select
      value={state}
      className={className || "gx-w-100"}
      placeholder="Seleccione un nivel de Intensidad"
      optionFilterProp="children"
      onChange={handleChange}
      {...otherProps}
    >
      {all && <Option value="">Todos</Option>}
      {intensities.map((values) => (
        <Option key={values.id} value={values.id}>
          {values.title}
        </Option>
      ))}
    </Select>
  );
};
