import React from "react";
import stylesUser from "./socialNetwork.module.scss";
import IconGroup from "@components/SocialNetworks/IconGroup";
import Image from "next/image";

export default function SocialNetwork({
  url,
  comments,
  month,
  day,
  userId,
  postsId,
  proposalId,
}) {
  return (
    <div className="flex flex-col md:flex-row items-center justify-between py-48 my-32">
      <div className="flex">
        <p className={stylesUser.text_date}>
          {capitalizeFirstLetter(month)} {day}
        </p>
        <div className="flex  items-center ml-5">
          <span className="mr-1">{comments}</span>
          <Image
            alt="arrow-icon"
            src="/icons/chat-4-line.svg"
            width={18}
            height={18}
          />
        </div>
      </div>
      <div className={stylesUser.spacer}></div>
      <div className={stylesUser.rightContainer}>
        <IconGroup
          href={url}
          userId={userId}
          postsId={postsId}
          proposalId={proposalId}
        />
      </div>
    </div>
  );
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
