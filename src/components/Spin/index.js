import { Spin } from "antd";
import "antd/lib/spin/style/index.css";

export const Spinner = (props) => {
  return <Spin {...props} />;
};

export default Spin;
