import { useState, useEffect } from "react";
import Link from "next/link";
import { useTextFormater } from "@components/Cards/Dashboard/hook";
import { useDispatch } from "react-redux";

import Button from "@components/Button";
import ChangeZone from "@components/Modals/ChangeZone";
import Modal from "@components/Modals/VoteModal";
import { useRouter } from "next/router";

import { Crown } from "@assets/icons";

import styles from "./VotingOption.module.scss";
import { routeRedirectRegister } from "@redux/auth/actions";

const VotingOption = ({
  alreadyVoted,
  completed,
  content,
  title,
  fileLink,
  index,
  userId,
  isFinished,
  onVote,
  id,
  openModal,
  isWinner,
  votes,
  totalVotes,
  votingId,
  isInVotingZone,
  macros,
  zones,
  zoneRelation,
  setZoneRelation,
}) => {
  const [color, setColor] = useState("bg-redCard border-redCard");
  const [showModal, setShowModal] = useState(false);
  const [contentText, setContentText] = useState("");
  const [modalChangeZone, setModalChangeZone] = useState(false);
  const [showText, setShowText] = useState("");
  const router = useRouter();
  const dispatch = useDispatch();
  const setUrl = () => {
    dispatch(routeRedirectRegister(window?.location.pathname));
  };
  useEffect(() => {
    try {
      if (Number.isInteger(index / 3))
        setColor("bg-purpleCard border-purpleCard");
      else if (Number.isInteger(index / 2))
        setColor("bg-greenCard border-greenCard");
    } catch (error) {}
  }, [index]);

  useEffect(() => {
    if (localStorage.getItem(`${votingId}_${userId}`) === "NO") {
      setZoneRelation(false);
    } else {
      setZoneRelation(true);
    }
  }, [modalChangeZone]);

  useEffect(() => {
    try {
      const obj = JSON.parse(content);
      const text = obj?.blocks?.reduce((acc, curr) => acc + curr.text, "");
      const { shortText, showText } = useTextFormater(text, 70);
      setContentText(shortText);
      setShowText(showText);
    } catch (e) {
      const { shortText, showText } = useTextFormater(content, 70);
      setContentText(shortText);
      setShowText(showText);
    }
  }, [content]);

  if (isFinished) {
    return (
      <div
        className={`${styles.container} ${isWinner ? styles.winner : ""}`}
        onClick={() => openModal()}
      >
        <div className={`border rounded-card p-3 ${color} ${styles.content}`}>
          <h6 className={styles.title}>
            {isWinner && (
              <span>
                <Crown /> Ganadora: {title}
              </span>
            )}
            {!isWinner && title}
          </h6>
          <a className={styles.link} onClick={() => openModal()}>
            Ver más información
          </a>
          <div className={styles.button__container}>
            <div className={styles.results}>
              {totalVotes
                ? Number.isInteger((votes / totalVotes) * 100)
                  ? (votes / totalVotes) * 100
                  : ((votes / totalVotes) * 100).toFixed(2)
                : 0}
              %
            </div>
          </div>
        </div>
      </div>
    );
  }
  return (
    <>
      <div className={styles.container} onClick={() => openModal()}>
        <div className={`border rounded-card p-3 ${color} ${styles.content}`}>
          <h6>{title}</h6>
          {fileLink && (
            <img
              alt={`Propuesta Alcaldia Bolivia ${title}`}
              src={fileLink}
              className={styles.image}
            />
          )}
          <p className={styles.description}>
            {contentText && contentText}
            {showText && showText}
          </p>
          <a className={styles.link}>{showText ? "Ver más información" : ""}</a>
          <div className={styles.button__container}>
            <div className={styles.button} onClick={(e) => e.stopPropagation()}>
              {userId ? (
                !isInVotingZone ? (
                  !alreadyVoted &&
                  zoneRelation && (
                    <Button
                      size="medium"
                      type="primary"
                      block={true}
                      onClick={() => setModalChangeZone(true)}
                    >
                      Votar esta opción
                    </Button>
                  )
                ) : (
                  !alreadyVoted && (
                    <Button
                      size="medium"
                      type="primary"
                      block={true}
                      onClick={() => setShowModal(true)}
                    >
                      Votar esta opción
                    </Button>
                  )
                )
              ) : (
                <Link href={`/login?goto=${router.asPath}`}>
                  <a>
                    <Button
                      onClick={setUrl}
                      size="small"
                      type="primary"
                      block={true}
                    >
                      Votar esta opción
                    </Button>
                  </a>
                </Link>
              )}
            </div>
          </div>
        </div>
      </div>
      <Modal
        visible={showModal}
        closable={false}
        onCancel={() => setShowModal(false)}
        cancelText="Cancelar"
        okText="Si, votar"
        onOk={() => {
          onVote({ optionId: id, id: votingId, userId });
          setShowModal(false);
        }}
        title={title}
      />
      <ChangeZone
        visible={modalChangeZone}
        closable={false}
        onCancel={() => setModalChangeZone(false)}
        cancelText="Cancelar"
        okText="Continuar"
        onOk={() => {
          // console.log({ optionId: id, id: votingId, userId });
          onVote({ optionId: id, id: votingId, userId });
          setModalChangeZone(false);
        }}
        macros={macros}
        zones={zones}
        userId={userId}
        votingId={votingId}
      />
    </>
  );
};

export default VotingOption;
