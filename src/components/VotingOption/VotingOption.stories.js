import { withDesign } from "storybook-addon-designs";
import VotingOption from "./index";

export default {
  title: "Components/VotingOption",
  component: VotingOption,
  decorators: [withDesign],
  argTypes: {
    voted: {
      control: { type: "boolean" },
    },
    isLoggedIn: {
      control: { type: "boolean" },
    },
    canVote: {
      control: { type: "boolean" },
    },
    content: {
      control: { type: "object" },
    },
    // dueDate: {
    //   control: { type: "string" },
    // },
    onClick: {
      control: { type: "function" },
    },
    openPopup: {
      control: { type: "function" },
    },
    dueDate: {
      control: { type: "number" },
    },
    results: {
      control: { type: "object" },
    },
  },
};

const Template = (args) => <VotingOption {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlVotingOption = {
  SinVotar: "10%3A5059",
  VotadoYDashboard: "2771%3A8593",
  Resultados: "10%3A5051",
};

export const SinVotar = Template.bind({});
SinVotar.args = {
  voted: false,
  isLoggedIn: true,
  canVote: true,
  content: {
    description:
      "Este logo parece una buena elección porque representa la calma y la fuerza del pueblo...",
    fileLink: "https://participa01.lapaz.bo/images/banner.jpg",
    id: "1",
    title: "Logo N°1",
  },
  onClick: () => {},
  openPopup: () => {},
  dueDate: 1640056896,
};

SinVotar.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlVotingOption.SinVotar}`,
  },
};

export const VotadoYDashBoard = Template.bind({});
VotadoYDashBoard.args = {
  voted: true,
  isLoggedIn: true,
  canVote: false,
  content: {
    description:
      "Este logo parece una buena elección porque representa la calma y la fuerza del pueblo...",
    fileLink: "https://participa01.lapaz.bo/images/banner.jpg",
    id: "1",
    title: "Logo N°1",
  },
  onClick: () => {},
  openPopup: () => {},
  dueDate: 1640056896,
};

VotadoYDashBoard.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlVotingOption.VotadoYDashboard}`,
  },
};

export const Resultados = Template.bind({});
Resultados.args = {
  results: {
    title: "Logo N°1",
    totalVotes: 2000,
    votes: 1000,
    winner: true,
  },
  openPopup: () => {},
};

Resultados.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlVotingOption.Resultados}`,
  },
};
