import React from "react";
import Select from "react-select";

const SelectSearch = ({ onchange, data, placeholder }) => {
  const colourStyles = {
    control: (styles, { hover }) => {
      return {
        ...styles,
        borderColor: hover ? "red" : null,
        borderRadius: "100rem",
        width: "100%",
        height: "55rem",
        fontWeight: "600",
        cursor: "pointer",
        paddingLeft: "5rem",
      };
    },
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        color: "#858585",
        cursor: "pointer",
        width: "100%",
        overflowY: "hidden",
        backgroundColor: isFocused ? "#dedede" : null,
        borderRadius: "10rem",
      };
    },
    menu: (provided, state) => {
      return {
        ...provided,
        width: "100%",
        overflowY: "hidden",
        borderRadius: "10rem",
        // height: "200rem",
      };
    },
    singleValue: (provided, state) => {
      return { ...provided, color: "#00B3BA", fontWeight: "600" };
    },
    indicatorSeparator: (status) => {
      return { ...status, backgroundColor: "white" };
    },
    indicatorsContainer: (status) => {
      return { ...status, color: "red" };
    },
  };
  return (
    <Select
      className="w-full"
      menuColor="red"
      styles={colourStyles}
      placeholder={placeholder}
      options={data}
      onChange={onchange}
    />
  );
};
export default SelectSearch;
