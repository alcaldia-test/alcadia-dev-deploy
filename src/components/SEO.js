import React from "react";
import Head from "next/head";

const SEO = ({
  title = "Alcaldía - La Paz",
  image = "https://participa.lapaz.bo/images/banner.jpg",
  description = "Plataforma de Participación Ciudadana, Gobierno Autónomo Municipal de La Paz, Bolivia",
  twitterUser = "@lapaz",
  lang = "ES",
  route = "/",
  imageWidth = 600,
  imageHeight = 300,
  // video,
}) => (
  <Head>
    <title>{title}</title>
    <meta name="description" content={description} />
    <meta name="image" content={image} />
    <meta property="og:title" content={title} />
    <meta property="og:description" content={description} />
    <meta property="og:image:width" content={imageWidth} />
    <meta property="og:image:height" content={imageHeight} />
    <meta property="og:image:alt" content={title} />
    <meta property="og:image" content={image} />
    {/* {video && <meta property="og:video" content={video} />} */}
    <meta property="og:locale" content={lang === "ES" ? "es_ES" : "en_US"} />
    <meta property="og:site_name" content={title} />
    <meta property="og:url" content={`https://participa.lapaz.bo${route}`} />
    <meta name="twitter:title" content={title} />
    <meta name="twitter:description" content={description} />
    <meta name="twitter:image" content={image} />
    <meta name="twitter:site" content={twitterUser} />
    <meta name="twitter:creator" content={twitterUser} />
    <meta name="twitter:card" content="summary" />
  </Head>
);

export default SEO;
