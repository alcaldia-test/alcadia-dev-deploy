import React, { memo, useEffect, useState } from "react";
import message from "@components/Message";
import Spin from "@components/Spin";
import styles from "./comments.module.scss";
import Comment from "./SingleComment";
import confirm from "@components/Modals/confirm";
import CommentService from "@services/comments.service";
import SelectWraper from "@components/Select";
import { useSelector } from "react-redux";
import CommentField from "@components/CommentField";

const optionsSelect2 = [
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más Antiguas" },
  { value: "moreLikes", label: "Más Likes" },
  { value: "lessLikes", label: "Menos Likes" },
];

const optionsSelect = [
  { value: "complaints", label: "Denuncias" },
  { value: "comments", label: "Comentarios" },
];

const CommentsComponent = ({
  mid = "4a43fd0c-5693-4ae5-ab95-88fbe9c23afa",
  isAdmin = false,
  endMessage,
  ncomments,
  updateComments = () => {},
  module = "posts",
  onTakeComplaint = () => {},
}) => {
  const [datas, setDatas] = useState([]);
  let [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState("");
  const [commentType, setCommentType] = useState(
    isAdmin ? "complaints" : "comments"
  );

  const { dataUser } = useSelector((storage) => storage.auth);

  useEffect(() => {
    initComponent();
    getComments({ type: commentType, npage: 1 });
  }, [mid]);

  const getComments = async ({ npage, ...filter } = {}) => {
    const service = new CommentService();
    let addition = 0;
    const cpage = npage || page;

    if (cpage !== 1) addition = 1;

    try {
      setLoading(true);
      const resp = await service.getComments({
        ...{ type: commentType },
        ...filter,
        module,
        id: mid,
        limit: cpage === 1 ? 4 : 3,
        skip: (cpage - 1) * 3 + addition,
        userId: dataUser.id,
      });

      setDatas(datas.concat(resp));
    } catch (error) {
      console.log(error);
      message.error("Problemas al obtener comentarios.");
    }

    setLoading(false);
  };

  const initComponent = () => {
    datas.length = 0;
    setPage(1);
    setValue("");
  };

  const onDeleteComment = async (id) => {
    const result = await confirm(
      "¿Seguro(a) que deseas eliminar el comentario?"
    );
    if (!result) return;

    const service = new CommentService();

    try {
      initComponent();
      setLoading(true);

      await service.deleteComment(id);
      await getComments({ npage: 1 });
      updateComments(--ncomments);
    } catch (error) {
      console.log(error);
      message.error("Problemas al obterner comentarios.");
    }
  };

  const onCreateComment = async (file) => {
    const service = new CommentService();

    if (!value) return message.info("Agregue un contenido al comentario.");

    const body = {
      [module + "Id"]: mid,
      content: value,
      userId: dataUser.id,
    };

    try {
      initComponent();
      setLoading(true);

      await service.createComment(body, file);
      await getComments({ npage: 1 });
      updateComments(++ncomments);
    } catch (error) {
      console.log(error);
      message.error("Problemas al obterner comentarios.");
    }
  };

  const refreshState = (i, answers) => {
    datas[i].answers = answers;
    setDatas(datas?.concat([]));
  };

  const handleLoading = async (state) => {
    console.log(state);

    datas.length = 0;
    setLoading(state);

    if (!state) onTakeComplaint();
  };

  const Content = (
    <>
      <div className={styles.commentContainer}>
        {datas.map((comment, i) => (
          <React.Fragment key={i}>
            {i !== page * 3 ? (
              <Comment
                {...comment}
                commentId={comment.id}
                currentUser={dataUser}
                setComments={refreshState}
                index={i}
                mid={mid}
                module={module}
                isAdmin={isAdmin}
                endMessage={endMessage}
                onDelete={onDeleteComment}
                handleLoading={handleLoading}
              />
            ) : (
              <></>
            )}
          </React.Fragment>
        ))}
        {datas?.length === 0 && (
          <h1 className="text-center my-5" style={{ fontSize: 20 }}>
            Sin datos para mostrar...
          </h1>
        )}
      </div>
      {loading && (
        <div style={{ textAlign: "center" }}>
          <Spin tip="Cargando..." size="large"></Spin>
        </div>
      )}
      {datas.length > page * 3 && (
        <span
          className={styles.MoreCommentsButton}
          onClick={() => {
            setPage(++page);
            getComments();
          }}
        >
          Ver m&aacute;s comentarios
        </span>
      )}
    </>
  );

  return (
    <div className={styles.commentSection}>
      <div className={styles.topTitle}>
        <h5>Comentarios</h5>
        {!isAdmin && !endMessage && dataUser.id ? (
          <CommentField
            placeholder={"Deja un Comentario"}
            onChange={(e) => setValue(`${e.target.value}`)}
            value={value}
            src={dataUser.avatar}
            handleUpload={onCreateComment}
            avatarSize="medium"
            userId={dataUser.id}
          />
        ) : (
          <hr style={{ color: "#0004" }} />
        )}
      </div>
      <header>
        <div className={styles.selectionGroup}>
          {isAdmin && (
            <SelectWraper
              onChange={(data) => {
                const selected = data[0].value;

                initComponent();
                getComments({ type: selected, order: "DESC", npage: 1 });
                setCommentType(selected);
              }}
              className={styles.commentSelector}
              defaultValue={0}
              options={optionsSelect}
            ></SelectWraper>
          )}

          {commentType === "comments" && (
            <SelectWraper
              onChange={(data) => {
                const selected = data[0].value;
                let byLikes;
                let order;

                if (selected === "moreLikes") {
                  order = "DESC";
                  byLikes = true;
                } else if (selected === "lessLikes") {
                  order = "ASC";
                  byLikes = true;
                } else order = data[0].value;

                initComponent();
                getComments({ byLikes, order, npage: 1 });
              }}
              className={styles.commentSelector}
              defaultValue={0}
              options={optionsSelect2}
            ></SelectWraper>
          )}
        </div>
        {ncomments && <h6>{ncomments} comentario(s)</h6>}
      </header>
      {loading && datas.length === 0 ? (
        <div style={{ textAlign: "center" }}>
          <Spin tip="Procesando..." size="large"></Spin>
        </div>
      ) : (
        Content
      )}
    </div>
  );
};

export default memo(CommentsComponent);
