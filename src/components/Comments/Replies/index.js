import React, { useEffect, useState } from "react";

import styles from "./replies.module.scss";
import Comment from "../SingleComment";
import CommentField from "@components/CommentField";

import CommentService from "@services/comments.service";
import confirm from "@components/Modals/confirm";
import Spin from "@components/Spin";
import message from "@components/Message";

const RepliesComponent = ({
  answers,
  cid,
  answerData,
  currentUser = {},
  handleLoading,
  isAdmin = false,
  setComments,
  index,
  endMessage,
  disabled,
  mid,
  module,
}) => {
  const [value, setValue] = useState("");
  const [datas, setDatas] = useState(answerData);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    console.log(answerData);
  }, []);

  useEffect(() => {
    if (answers > 0) getComments();
  }, []);

  const getComments = async () => {
    const service = new CommentService({});

    try {
      setLoading(true);

      const datas = await service.getAnswers({ cid });

      setDatas(datas ?? []);
    } catch (error) {
      console.log(error);
      message.error("Problemas al obterner comentarios.");
    }

    setLoading(false);
  };

  const onDeleteComment = async (id) => {
    const result = await confirm("¿Seguro(a) en eliminar el tutorial?");
    if (!result) return;

    const service = new CommentService();

    try {
      datas.length = 0;
      setLoading(true);

      await service.deleteComment(undefined, id);

      await getComments();
      setComments(index, answers - 1);
    } catch (error) {
      console.log(error);
      message.error("Problemas al obterner comentarios.");
    }
  };

  const onCreateComment = async (file) => {
    const service = new CommentService();

    if (!value) return message.info("Agregue un contenido al comentario.");

    const body = {
      content: value,
      commentId: cid,
      userId: currentUser.id,
    };

    try {
      datas.length = 0;
      setLoading(true);

      await service.createAnswer(body, file);
      setValue("");
      await getComments();
      setComments(index, answers + 1);
    } catch (error) {
      console.log(error);
      message.error("Problemas al obterner comentarios.");
    }
  };

  const onReplayComment = (value) => {
    const { top } = document.getElementById(cid).getClientRects()[0];

    const y = window.scrollY;
    const targetScroll = top - 200;

    window.scrollTo(0, y + targetScroll);

    setValue(value);
  };

  return (
    <div className={styles.repliesContainer}>
      {(!disabled || endMessage) && currentUser.id ? (
        <div className={styles.commentInputContainer}>
          <CommentField
            placeholder={"Responder..."}
            onChange={(e) => setValue(`${e.target.value}`)}
            value={value}
            src={currentUser.avatar}
            handleUpload={onCreateComment}
            id={cid}
            avatarSize={"medium"}
          />
        </div>
      ) : undefined}
      <div className="mt-4">
        {datas.map((answer, i) => {
          return (
            <Comment
              {...answer}
              key={i}
              commentId={undefined}
              answerId={answer.id}
              isAdmin={isAdmin}
              currentUser={currentUser}
              endMessage={endMessage}
              handleLoading={handleLoading}
              responseAnswer={onReplayComment}
              onDelete={onDeleteComment}
              mid={mid}
              module={module}
            />
          );
        })}
      </div>
      {loading && (
        <div style={{ textAlign: "center" }}>
          <Spin tip="Cargando..." size="large"></Spin>
        </div>
      )}
    </div>
  );
};

export default RepliesComponent;
