export default function useCheckTextLength(text, returnFullText) {
  const maxTextLength = 55;
  const textSplited = text.split(" ");

  if (textSplited.length > maxTextLength && returnFullText === true) {
    return {
      commentText: text,
      buttonText: " Mostrar menos",
    };
  }

  if (textSplited.length > maxTextLength && returnFullText === false) {
    return {
      commentText: textSplited.slice(0, maxTextLength).join(" "),
      buttonText: "... Leer mas ",
    };
  }

  return {
    commentText: text,
    buttonText: "",
  };
}
