import { useState } from "react";
import Link from "next/link";
import Avatar from "@components/Avatar";
import Interactions from "./Interactions";
import {
  PaperClipOutlined,
  ArrowUpOutlined,
  ArrowDownOutlined,
} from "@ant-design/icons";

import ComplaintModal from "./Complaints/ComplaintsModal";
import DetailsModal from "./Complaints/DetailsModal";
import message from "@components/Message";

import useCheckTextLength from "./hook";
import styles from "./comments.module.scss";
import Replies from "./Replies";

import Popover from "@components/Popover";
import CommentService from "@services/comments.service";
import moment from "moment";

moment.locale("es");
const Comment = ({
  createdAt,
  commentId,
  answerId,
  user = {},
  content = "",
  fileLink,
  answers = 0,
  likes,
  dislikes,
  reaction,
  complaints,
  currentUser = {},
  onDelete = () => {},
  setComments,
  index,
  isAdmin,
  endMessage,
  responseAnswer,
  handleLoading,
  mid,
  module,
}) => {
  const [isOpen, setIsOpen] = useState(answers?.length > 0);
  const [showFulltext, setShowFulltext] = useState(false);
  const [openOptions, setOpenOptions] = useState(false);
  const { commentText, buttonText } = useCheckTextLength(content, showFulltext);
  const [openModal, setOpenModal] = useState(false);
  const [showComplaint, setShowComplaint] = useState(false);

  const handleFocus = (ev) => {
    const { top } = ev?.target?.getClientRects()[0];
    const y = window.scrollY;
    const targetScroll = top - 200;

    window.scrollTo(0, y + targetScroll);
  };

  const handleShowResponse = (ev) => {
    if (responseAnswer) responseAnswer(user.firstName + " ");
    else setIsOpen(!isOpen);

    if (!isOpen && !responseAnswer) handleFocus(ev);
  };

  const handleReact = async (type) => {
    const service = new CommentService();
    let reaction;

    if (type === "liked") reaction = "like";
    else if (type === "disliked") reaction = "dislike";
    else reaction = "omit";

    try {
      await service.reactComment(
        {
          reaction,
          userId: currentUser.id,
        },
        commentId,
        answerId
      );
    } catch (error) {
      console.log(error);
      message.error("Problemas al obterner comentarios.");
    }
  };

  return (
    <div className={styles.commentLayout}>
      {openModal && (
        <ComplaintModal
          setOpenModal={setOpenModal}
          commentId={commentId}
          answerId={answerId}
          userId={currentUser.id}
          mid={mid}
          module={module}
        />
      )}
      {showComplaint && (
        <DetailsModal
          userName={user.firstName}
          total={complaints}
          commentId={commentId}
          answerId={answerId}
          userId={user.id}
          handleLoading={handleLoading}
          onCancel={() => setShowComplaint(false)}
        />
      )}

      <div className="avartarContainer">
        <Link href={`/usuario/${user.id}`}>
          <a>
            <Avatar src={user.avatar} size="medium" alt="user avatar" />
          </a>
        </Link>
      </div>
      <div className={`${styles.descriptionContainer} mb-4`}>
        <div
          className={`${styles.descriptionContainer} ${
            isAdmin && complaints > 0 ? styles.denunciasBorder : ""
          }`}
        >
          <div className={styles.commentHeader}>
            <div>
              <Link href={`/usuario/${user.id}`}>
                <a>
                  <small>{user.firstName} </small>
                </a>
              </Link>
              <span role="log">&nbsp;·&nbsp;{moment(createdAt).fromNow()}</span>
            </div>
            {isAdmin && complaints > 0 ? (
              <span
                className={styles.denunciasButton}
                onClick={() => setShowComplaint(true)}
              >
                Atender Denuncias {`(${complaints})`}
              </span>
            ) : (
              <Popover
                content={
                  <>
                    {!isAdmin && (
                      <span
                        className={styles.denuncias}
                        onClick={() => {
                          setOpenModal(true);
                          setOpenOptions(false);
                        }}
                      >
                        Denunciar
                      </span>
                    )}
                    {currentUser.id === user.id && (
                      <span
                        className={styles.denuncias}
                        onClick={() => {
                          setOpenOptions(false);
                          onDelete(answerId || commentId);
                        }}
                      >
                        Eliminar
                      </span>
                    )}
                  </>
                }
                placement="bottomRight"
                trigger="click"
                visible={openOptions}
                onVisibleChange={(vis) => setOpenOptions(vis)}
              >
                {currentUser.id &&
                complaints === undefined &&
                (!isAdmin || currentUser.id === user.id) ? (
                  <span
                    role="icon"
                    onClick={() => {
                      setOpenOptions(true);
                    }}
                  >
                    ⋯
                  </span>
                ) : undefined}
              </Popover>
            )}
          </div>
          <div className={styles.commentText}>
            <span className={!isOpen ? "ellipsisText" : "textNoEllipsis"}>
              {commentText}
              <a onClick={() => setShowFulltext(!showFulltext)}>{buttonText}</a>
            </span>
            {fileLink && (
              <a
                className={styles.fileLinkButton}
                href={fileLink}
                target="_blank"
                rel="noreferrer"
                download={true}
              >
                <PaperClipOutlined /> Ver archivo adjunto
              </a>
            )}
          </div>
        </div>
        <Interactions
          logged={!!currentUser.id}
          likes={likes}
          dislikes={dislikes}
          reaction={reaction}
          endedMessage={endMessage}
          openResponce={isOpen}
          showReplies={handleShowResponse}
          commentResponse={(data, ...arg) => console.log(data)}
          likeResponse={handleReact}
          disabled={complaints !== undefined}
        />
        {answers > 0 && (
          <span
            className={styles.showResponsesButton}
            onClick={() => setIsOpen(!isOpen)}
          >
            {isOpen ? <ArrowUpOutlined /> : <ArrowDownOutlined />}
            {isOpen ? "Ocultar Respuestas" : "Mostrar Respuestas"}
            &nbsp;&nbsp;({answers})
          </span>
        )}
        <div
          className={
            isOpen ? styles.answersContainer : styles.answersContainerHiden
          }
        >
          {isOpen && (
            <Replies
              cid={commentId}
              isAdmin={isAdmin}
              disabled={complaints !== undefined}
              answers={answers}
              answerData={typeof answers !== "number" ? answers : []}
              setComments={setComments}
              currentUser={currentUser}
              index={index}
              mid={mid}
              module={module}
              endMessage={endMessage}
              handleLoading={handleLoading}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default Comment;
