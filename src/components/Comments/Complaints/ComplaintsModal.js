import React, { useState, useEffect } from "react";
import Button from "@components/Button";

import RadiaGroup from "@components/RadioGroup";

import styles from "../comments.module.scss";
import Modal from "@components/Modals/SimpleModal";
import Spin from "@components/Spin";
import CommentService from "@services/comments.service";

import message from "@components/Message";

let value = "";
const ComplaintModal = ({
  mid,
  module,
  setOpenModal,
  commentId,
  answerId,
  userId,
}) => {
  const [loading, setLoading] = useState(false);
  const [complainted, setComplainted] = useState(false);

  const radioOptions = [
    { id: "SPAM", name: "Spam" },
    { id: "AMISS", name: "Lenguaje Inapropiado" },
    { id: "RACISM", name: "Racismo" },
  ];

  useEffect(() => {
    initFetch();
  }, []);

  const initFetch = async () => {
    const service = new CommentService({});

    try {
      setLoading(true);
      const comp = await service.verifyComplainted({
        commentId,
        answerId,
        userId,
      });
      setComplainted(comp?.found);
    } catch (error) {
      console.log(error);
      message.error("Problemas al obterner comentarios.");
    }

    setLoading(false);
  };

  const handleComplaint = async () => {
    const service = new CommentService({});

    try {
      setLoading(true);

      await service.complaintComment({
        type: value,
        commentId,
        answerId,
        userId,
        postsId: module === "posts" ? mid : undefined,
      });

      message.success("Se registró con éxito su denuncia.");
    } catch (error) {
      console.log(error);
      message.error("Problemas al obtener comentarios.");
    }

    setOpenModal(false);
  };

  return (
    <Modal visible={true} onCancel={() => setOpenModal(false)}>
      <Spin spinning={loading} tip="Cargando..." size="large">
        {complainted ? (
          <h5 className={`${styles.modaltitle} mb-2`}>
            Usted ya denunció este <br /> comentario
          </h5>
        ) : (
          <>
            <h5 className={styles.modaltitle}>
              ¿Por qu&eacute; desea denunciar este comentario?
            </h5>
            <RadiaGroup
              className={styles.checkBoxContainer}
              onChange={(e) => (value = e.target.id)}
              options={radioOptions}
            />
            <Button block size="small" onClick={handleComplaint}>
              Denunciar
            </Button>
          </>
        )}
      </Spin>
    </Modal>
  );
};

export default ComplaintModal;
