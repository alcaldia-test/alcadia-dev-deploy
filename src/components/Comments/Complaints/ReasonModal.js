import React from "react";
import Modal from "@components/Modals/SimpleModal";
import Button from "@components/Button";
import styles from "./details.module.scss";
import { useSelector } from "react-redux";
import message from "@components/Message";
import CommentService from "@services/comments.service";

const currectState = {
  message: "",
};

function ReasonModal({ user = {}, onCancel, handleLoading = () => {} }) {
  const { dataUser } = useSelector((storage) => storage.auth);

  const handleChange = (ev) => {
    currectState[ev.target.name] = ev.target.value;
  };

  const handleSave = async () => {
    if (!currectState.message) return message.error("¡Introduzca el motivo!");

    const payload = {
      userId: user.id,
      commentId: user.commentId,
      answerId: user.answerId,
      lock: user.lock,
      reviewerId: dataUser.id,
      ...currectState,
    };

    const service = new CommentService();

    onCancel();
    try {
      handleLoading(true);

      await service.takeComplaint(payload);

      message.success("Acción realizada con éxito.");

      currectState.message = "";

      handleLoading(false);
    } catch (error) {
      message.error(error);
    }
  };

  return (
    <Modal visible={true} onCancel={onCancel}>
      <div className={styles["input-section"]}>
        <h3>
          {user.lock
            ? `Bloquear a ${user.name}`
            : `Quitar comentario de ${user.name}`}
        </h3>
        <label>
          Motivo
          <textarea
            name="message"
            placeholder="Contenido..."
            onChange={handleChange}
          />
        </label>
      </div>
      <div className="flex justify-around">
        <Button onClick={onCancel} type="secondary" size="small">
          Cancelar
        </Button>
        <Button onClick={handleSave} type="primary" size="small">
          {user.lock ? `Bloquear` : `Quitar`}
        </Button>
      </div>
    </Modal>
  );
}

export default ReasonModal;
