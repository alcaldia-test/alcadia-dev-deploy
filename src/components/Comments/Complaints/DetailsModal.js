import React, { useState } from "react";
import Modal from "@components/AntModal";
import Button from "@components/Button";
import styles from "./details.module.scss";

import ReasonModal from "./ReasonModal";

import ComplaintsList from "./ComplaintsList";

function DetailsModal({
  total,
  commentId,
  answerId,
  userId,
  userName,
  handleLoading,
  onCancel = () => {},
}) {
  const [current, setCurrent] = useState(undefined);

  console.log(total, commentId, answerId);

  const handleAction = async (lock) => {
    setCurrent({
      name: userName,
      id: userId,
      commentId: commentId,
      answerId: answerId,
      lock,
      model: commentId ? "comment" : "answer",
    });
  };

  return (
    <>
      {current ? (
        <ReasonModal
          handleLoading={handleLoading}
          user={current}
          onCancel={onCancel}
        />
      ) : (
        <Modal
          className="simple-modal"
          footer={false}
          visible={true}
          onCancel={onCancel}
        >
          <div className={styles["title-section"]}>
            <h3>Denuncias</h3>
            <span>Cantidad de denuncias {total}</span>
          </div>
          <div className={styles["modal-body-section"]}>
            <div className={styles["complaints-list"]}>
              <ComplaintsList
                count={total}
                model={commentId ? "comment" : "answer"}
                foreignId={commentId ?? answerId}
              />
            </div>
          </div>
          <div className={styles["section-buttons"]}>
            <Button
              onClick={handleAction.bind({}, true)}
              type="secondary"
              size="small"
            >
              Bloquear Usuario
            </Button>
            <Button
              className="ml-2"
              onClick={handleAction.bind({}, false)}
              type="secondary"
              size="small"
            >
              Quitar comentario
            </Button>
          </div>
        </Modal>
      )}
    </>
  );
}

export default DetailsModal;
