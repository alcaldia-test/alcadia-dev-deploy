import React from "react";
import { withDesign } from "storybook-addon-designs";
import TextInput from "./index";
import { SettingOutlined, UserOutlined } from "@ant-design/icons";

export default {
  title: "Components/TextInput",
  decorators: [withDesign],
  component: TextInput,
  argTypes: {
    type: { options: ["text", "password", "email"], control: "select" },
    maxLength: "number",
    disabled: "boolean",
    label: "string",
    prefix: { ReactNode: "element" },
    suffix: { ReactNode: "element" },
    helpText: "string",
    id: "string",
    validateStatus: { options: ["error"], control: "select" },
  },
};

const Template = (args) => <TextInput {...args} />;
export const Default = Template.bind({});

Default.args = {
  type: "text",
  disabled: false,
  maxLength: 15,
  value: "",
  label: "Ingrese su username",
  prefix: <UserOutlined />,
  suffix: <SettingOutlined />,
  helpText: "help text",
  id: "username",
  validateStatus: "default",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=5%3A2932",
  },
};
