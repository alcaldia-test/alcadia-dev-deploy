import React, { useState } from "react";
import styles from "./TextInput.module.scss";

const FloatingLabel = ({ children, label, value }) => {
  const [focus, setFocus] = useState(false);
  const labelClass =
    focus || (value && value.length !== 0)
      ? `${styles.label}+ ${styles.labelFloat}`
      : `${styles.label} `;
  return (
    <div
      className={styles.floatLabel}
      onBlur={() => setFocus(false)}
      onFocus={() => setFocus(true)}
    >
      {children}
      <label className={labelClass}>{label}</label>
    </div>
  );
};

export default FloatingLabel;
