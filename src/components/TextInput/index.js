import React, { useState } from "react";
import { Form, Input } from "antd";
import "antd/lib/input/style/css";
import styles from "./TextInput.module.scss";
import FloatingLabel from "./FloatingLabel";

const TextInput = ({
  type,
  maxLength,
  validateStatus,
  inputName,
  value,
  id,
  prefix,
  suffix,
  disabled,
  helpText,
  label,
  ...args
}) => {
  const [formValue, setformValue] = useState(value);
  return (
    <div className={`${styles.textinput}`}>
      <Form.Item
        validateStatus={validateStatus}
        className={`${styles[validateStatus]}`}
      >
        <FloatingLabel label={label} name={inputName} value={formValue}>
          <Input
            id={validateStatus}
            prefix={prefix}
            suffix={suffix}
            maxLength={maxLength}
            disabled={disabled}
            value={formValue}
            onChange={(e) => setformValue(e.target.value)}
            {...args}
          />
        </FloatingLabel>
        <span className={styles.helpText}>{helpText}</span>
      </Form.Item>
    </div>
  );
};

export default TextInput;
