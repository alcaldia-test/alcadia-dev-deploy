import React, { useState } from "react";
import { Form, Input } from "antd";
import "antd/lib/input/style/css";
import styles from "./TextInput.module.scss";
import FloatingLabel from "./FloatingLabel";

const TextInput = ({
  type,
  maxLength,
  validateStatus,
  inputName,
  value,
  id,
  prefix,
  suffix,
  disabled,
  helpText,
  label,
  ...rest
}) => {
  const [formValue, setformValue] = useState(value);

  if (type === "password")
    return (
      <div className={styles.inputContianer}>
        <Form.Item
          validateStatus={validateStatus}
          className={`${styles[validateStatus]}`}
        >
          <FloatingLabel label={label} name={inputName} value={formValue}>
            <Input.Password
              id={validateStatus}
              prefix={prefix}
              suffix={suffix}
              maxLength={maxLength}
              disabled={disabled}
              value={formValue}
              onInput={(e) => setformValue(e.target.value)}
              {...rest}
            />
          </FloatingLabel>
          <span className={styles.helpText}>{helpText}</span>
        </Form.Item>
      </div>
    );

  return (
    <div className={styles.inputContianer}>
      <Form.Item
        validateStatus={validateStatus}
        className={`${styles[validateStatus]}`}
      >
        <FloatingLabel label={label} name={inputName} value={formValue}>
          <Input
            id={validateStatus}
            prefix={prefix}
            suffix={suffix}
            maxLength={maxLength}
            disabled={disabled}
            value={formValue}
            onInput={(e) => setformValue(e.target.value)}
            {...rest}
          />
        </FloatingLabel>
        <span className={styles.helpText}>{helpText}</span>
      </Form.Item>
    </div>
  );
};

export default TextInput;
