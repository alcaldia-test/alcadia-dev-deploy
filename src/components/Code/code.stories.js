import React from "react";
import { withDesign } from "storybook-addon-designs";

import Code from "./index";

export default {
  title: "Components/Code",
  component: Code,
  decorators: [withDesign],
};

const Template = (args) => <Code {...args} />;

export const Default = Template.bind({});

Default.args = {
  codeSize: 4,
  handleInput: (e) => console.log(e),
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7329",
  },
};

export const withAutoFocus = Template.bind({});

withAutoFocus.args = {
  codeSize: 4,
  handleInput: (e) => console.log(e),
  autoFocus: true,
};

withAutoFocus.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7329",
  },
};
