import React from "react";
import CodeInput from "react-code-input";

import styles from "./code.module.scss";

/**
 * @param {handleInput} function funcion cb que recibe como valor un string con los valores de los inputs
 * @param {inputsNumber} int numero de inputs que se van a renderizar
 */

const Code = ({ codeSize = 4, handleInput, ...rest }) => {
  return (
    <div className={styles.codeContainer}>
      <CodeInput
        type="text"
        fields={codeSize}
        className={styles.codeEl}
        {...rest}
        onChange={(e) => handleInput(e)}
      />
    </div>
  );
};

export default Code;
