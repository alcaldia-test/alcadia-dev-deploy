import React from "react";
import Select from "@components/Select";

export default {
  title: "3-Components/Select",
  component: Select,
  argTypes: {
    options: {
      control: {
        type: "array",
        options: [
          { value: "1", label: "One" },
          { value: "2", label: "Two" },
          { value: "3", label: "Three" },
          { value: "4", label: "Four" },
          { value: "5", label: "Five" },
        ],
      },
    },
    value: {
      control: {
        type: "select",
        options: [
          { value: "1", label: "One" },
          { value: "2", label: "Two" },
        ],
      },
    },
  },
};

const Template = (args) => <Select {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlCheckBoxes = {
  default: "10%3A3160",
};

export const Default = Template.bind({});
Default.args = {
  options: [
    { value: "1", label: "One" },
    { value: "2", label: "Two" },
    { value: "3", label: "Three" },
    { value: "4", label: "Four" },
    { value: "5", label: "Five" },
  ],
  value: "",
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlCheckBoxes.default}`,
  },
};
