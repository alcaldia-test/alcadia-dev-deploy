import React from "react";
import Select from "react-dropdown-select";
import styles from "./Select.module.scss";

const SelectWraper = ({
  options,
  placeholder,
  onChange,
  defaultValue = 0,
  style,
  className,
}) =>
  options.length > 0 ? (
    <Select
      className={`${styles.select} ${className}`}
      options={options}
      values={
        options.length > defaultValue ? [options[defaultValue]] : [options[0]]
      }
      style={style}
      placeholder={placeholder}
      backspaceDelete={false}
      onChange={onChange}
    />
  ) : (
    <></>
  );

export default SelectWraper;
