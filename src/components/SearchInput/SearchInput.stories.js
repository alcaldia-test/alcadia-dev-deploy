import { withDesign } from "storybook-addon-designs";

import SearchInput from "./index.js";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idSearchInput = {
  default: "748%3A7183",
};

export default {
  title: "Components/SearchInput",
  component: SearchInput,
  decorators: [withDesign],
  argTypes: {
    placeholder: "string",
    enterButton: { control: "boolean" },
    loading: { control: "boolean" },
    allowClear: { control: "boolean" },
    bordered: { control: "boolean" },
    disabled: { control: "boolean" },
    maxLength: { control: "number" },
    size: { options: ["large", "middle", "small"], control: "select" },
  },
};

const Template = (args) => <SearchInput {...args} />;

export const Default = Template.bind({});
Default.args = {
  placeholder: "Buscar",
};
Default.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idSearchInput.default}`,
  },
};
