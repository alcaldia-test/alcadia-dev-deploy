import React from "react";
import Search from "antd/lib/input/Search";
import "antd/lib/button/style/index.css";
import styles from "./SearchInput.module.scss";
/**
 * @param Props Default props of a input.
 * @return An input text with a svg that indicates search.
 */
function SearchInput({ ...props }) {
  return (
    <Search
      {...props}
      enterButton={false}
      className={`${styles["search-input"]}`}
    />
  );
  // enterButton es una prop que desmaquetiza el componente, por eso lleva el valor de false
}
export default SearchInput;
