import React, { useEffect } from "react";
import {
  Input,
  Spin,
  Button,
  Row,
  Col,
  Form,
  Divider,
  Tabs,
  Image,
} from "antd";
import Modal from "antd/lib/modal/Modal";
import { batch, useDispatch, useSelector } from "react-redux";
import { useForm } from "antd/lib/form/Form";
import { customUseReducer } from "@utils/customHooks";
import { RetweetOutlined, PlayCircleOutlined } from "@ant-design/icons";
import { SelectComponent } from "@components/SelectComponent";
import { requestFilesStorageStart } from "@containers/Admin/FilesStorage/redux/actions";
import { set } from "lodash";

import { filesStorageSaga } from "@containers/Admin/FilesStorage/redux/saga";
import { filesStorageReducer } from "@containers/Admin/FilesStorage/redux/reducer";

import injectSaga from "@utils/inject-saga";
import injectReducer from "@utils/inject-reducer";

import { compose } from "redux";
import TableApp from "@components/TableApp";
import { CATEGORIES_IMAGES } from "../../config";

const { TabPane } = Tabs;

const initialState = {
  modal: false,
  loading: false,
  search: "",
  filter: {
    video: {
      type: "",
      category: "",
    },
    image: {
      type: "",
      category: "",
    },
  },
};

function SearchFilesStorage({
  value,
  onChange,
  textButtom = "SELECCIONAR IMAGEN",
  type = "",
}) {
  const [form] = useForm();
  const [state, dispatchComponent] = customUseReducer(initialState);
  const filesStorage = useSelector(({ filesStorage }) => filesStorage);
  const typesExercise = useSelector(({ global }) => global.typesExercise);
  const dispatch = useDispatch();

  const defaultSelect = {
    imageCategories: CATEGORIES_IMAGES.find(
      (values) => values.label === "Phone"
    )?.value,
    typesExercise: typesExercise.find((values) => values.title === "Normal")
      ?.id,
  };

  useEffect(() => {
    state.modal && initialRequest();
  }, [state.modal]);

  const initialRequest = () => {
    dispatch(requestFilesStorageStart());
  };

  const handleSelectImage = (fileStorage) => {
    batch(() => {
      dispatchComponent({ modal: false });
      onChange(fileStorage);
    });
  };

  return (
    <>
      <Input.Group compact style={{ marginBottom: -16 }}>
        <Input
          style={{ width: "60%" }}
          value={value?.name}
          placeholder="Nombre del archivo"
          disabled
          addonBefore={
            value?.name && (
              <div
                style={{
                  width: 30,
                  height: "100%",
                  flexDirection: "row",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {value?.contentType?.includes("image") && (
                  <img
                    src={value?.link}
                    style={{ width: 30, height: 30, objectFit: "cover" }}
                    alt="Logo"
                  />
                )}
                {value?.contentType?.includes("video") && (
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <PlayCircleOutlined style={{ fontSize: 30 }} />
                  </div>
                )}
              </div>
            )
          }
        />
        <Button
          onClick={() => dispatchComponent({ modal: true })}
          style={{ userSelect: "none", cursor: "pointer", width: "40%" }}
        >
          {textButtom}
        </Button>
      </Input.Group>

      <Row justify="center">
        <Col xxl={16} xl={24} lg={24} md={24} sm={24} xs={24}>
          <Modal
            title="Buscador de archivos Multimedia"
            centered
            visible={state.modal}
            onCancel={() => dispatchComponent({ modal: false })}
            footer={false}
            confirmLoading={filesStorage.loading}
            width="80%"
          >
            <Spin
              spinning={filesStorage.loading}
              tip="Obteniendo datos del servidor"
              size="large"
            >
              <Row>
                <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                  <p className="gx-text-grey">Detalles del Contenedor</p>
                  <h2 className="gx-text-uppercase gx-text-black gx-font-weight-bold gx-fnd-title">
                    LISTA MULTIMEDIA
                  </h2>
                  <p>
                    Este contenedor tiene como funcionalidad la selección de
                    archivos multimedías
                  </p>
                </Col>
              </Row>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  marginBottom: -8,
                }}
              >
                <Input.Group compact>
                  <Input.Search
                    placeholder="Buscar por Nombre"
                    allowClear
                    enterButton="Buscar"
                    onSearch={(search) => dispatchComponent({ search })}
                    style={{ width: "100%" }}
                  />
                </Input.Group>
                <Button
                  style={{ marginLeft: 8 }}
                  type="dashed"
                  icon={<RetweetOutlined />}
                  onClick={initialRequest}
                />
              </div>
              <Tabs>
                {(type === "" || type === "image") && (
                  <TabPane tab="Imagenes" key="1" style={{ padding: 4 }}>
                    <Form
                      form={form}
                      name="basic"
                      className="ant-advanced-search-form"
                      layout="vertical"
                    >
                      <p className="gx-text-grey">Filtro de busqueda</p>
                      <Row gutter={24} style={{ flexDirection: "row" }}>
                        <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                          <SelectComponent.ImageCategories
                            defaultValue={defaultSelect.imageCategories}
                            onChange={(value) =>
                              dispatchComponent(
                                set(state, "filter.image.category", value)
                              )
                            }
                          />
                        </Col>
                        <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                          <SelectComponent.TypesExercise
                            defaultValue={defaultSelect.typesExercise}
                            onChange={(value) =>
                              dispatchComponent(
                                set(state, "filter.image.type", value)
                              )
                            }
                          />
                        </Col>
                      </Row>
                    </Form>
                    <Divider dashed />
                    <p>GALERIA DE ARCHIVOS</p>
                    <TableApp
                      dataSource={filesStorage.data
                        .filter((fileStorage) =>
                          fileStorage.contentType.includes("image")
                        )
                        .filter((fileStorage) =>
                          state.search === ""
                            ? true
                            : fileStorage.name.includes(state.search)
                        )
                        .filter((filesStorage) =>
                          state.filter.image.type === ""
                            ? true
                            : filesStorage.type.id === state.filter.image.type
                        )
                        .filter((filesStorage) =>
                          state.filter.image.category === ""
                            ? true
                            : filesStorage.category ===
                              state.filter.image.category
                        )}
                      renderItem={(fileStorage) => (
                        <div
                          className="container-list-image"
                          style={
                            fileStorage.id === value?.id
                              ? { border: "3px dashed #038fde" }
                              : {}
                          }
                        >
                          <div
                            style={{
                              position: "relative",
                              textAlign: "center",
                            }}
                          >
                            <Image
                              src={fileStorage.link}
                              style={{
                                height: 150,
                                borderTopLeftRadius: 8,
                                borderTopRightRadius: 8,
                                objectFit: "cover",
                              }}
                            />
                            <div className="container-name-image">
                              {fileStorage.name}
                            </div>
                          </div>
                          <div
                            className="container-buttom-image"
                            onClick={() => handleSelectImage(fileStorage)}
                          >
                            Seleccionar imagen
                          </div>
                        </div>
                      )}
                    />
                  </TabPane>
                )}

                {(type === "" || type === "video") && (
                  <TabPane tab="Videos" key="2">
                    <Form
                      form={form}
                      name="basic"
                      className="ant-advanced-search-form"
                      layout="vertical"
                    >
                      <p className="gx-text-grey">Filtro de busqueda</p>
                      <Row gutter={24} style={{ flexDirection: "row" }}>
                        <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                          <SelectComponent.ImageCategories
                            all={true}
                            onChange={(value) =>
                              dispatchComponent(
                                set(state, "filter.video.category", value)
                              )
                            }
                          />
                        </Col>
                        <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                          <SelectComponent.TypesExercise
                            all={true}
                            onChange={(value) =>
                              dispatchComponent(
                                set(state, "filter.video.type", value)
                              )
                            }
                          />
                        </Col>
                      </Row>
                    </Form>
                    <Divider dashed />
                    <p>GALERIA DE ARCHIVOS</p>
                    <Row gutter={24} style={{ flexDirection: "row" }}>
                      {filesStorage.data
                        .filter((fileStorage) =>
                          fileStorage.contentType.includes("video")
                        )
                        .filter((fileStorage) =>
                          state.search === ""
                            ? true
                            : fileStorage.name.includes(state.search)
                        )
                        .filter((filesStorage) =>
                          state.filter.video.type === ""
                            ? true
                            : filesStorage.type.id === state.filter.video.type
                        )
                        .filter((filesStorage) =>
                          state.filter.video.category === ""
                            ? true
                            : filesStorage.category ===
                              state.filter.video.category
                        )
                        .map((fileStorage) => (
                          <Col
                            key={fileStorage.id}
                            xxl={3}
                            xl={6}
                            lg={6}
                            md={6}
                            sm={12}
                            xs={24}
                          >
                            <div className="container-list-image">
                              <div
                                style={{
                                  position: "relative",
                                  textAlign: "center",
                                }}
                              >
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <PlayCircleOutlined
                                    style={{ fontSize: 72, margin: 8 }}
                                  />
                                </div>
                                <div className="container-name-image">
                                  {fileStorage.name}
                                </div>
                              </div>
                              <div
                                className="container-buttom-image"
                                onClick={() => handleSelectImage(fileStorage)}
                              >
                                Seleccionar Video
                              </div>
                            </div>
                          </Col>
                        ))}
                    </Row>
                  </TabPane>
                )}
              </Tabs>
            </Spin>
          </Modal>
        </Col>
      </Row>
    </>
  );
}

const withReducerFilesStorage = injectReducer({
  key: "filesStorage",
  reducer: filesStorageReducer,
});
const withSagaFilesStorage = injectSaga({
  key: "FilesStorage",
  saga: filesStorageSaga,
});

export default compose(
  withReducerFilesStorage,
  withSagaFilesStorage
)(SearchFilesStorage);
