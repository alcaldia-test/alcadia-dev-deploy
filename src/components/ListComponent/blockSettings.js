import React from "react";
import { Typography, Row, Col, Input, Button } from "antd";
const { Title } = Typography;

const ListBlockSettings = ({
  handleModal = () => {},
  handleDeleteBlock = () => {},
  handleChangeRepetition = () => {},
  ...params
}) => {
  return (
    <Row
      style={{
        flexDirection: "row",
        border: "1px solid #cecece",
        marginBottom: 16,
      }}
    >
      <Col xl={18} lg={18} md={18} sm={24} xs={24} className="row-col-default">
        <div style={{ padding: 8, height: "100%" }}>
          <p style={{ margin: 0 }} className="gx-text-muted">
            Título del bloque
          </p>
          <Title level={4} style={{ margin: 0 }}>
            {params.title}
          </Title>
          <Input
            disabled={params.type.title.toLowerCase() !== "normal"}
            defaultValue={params.value ?? 1}
            addonBefore="Rondas"
            placeholder="Cantidad de repeticiones del Bloque"
            type="number"
            onChange={handleChangeRepetition}
          />
        </div>
      </Col>
      <Col
        xl={6}
        lg={6}
        md={6}
        sm={24}
        xs={24}
        style={{
          display: "flex",
          justifyContent: "space-evenly",
          flexDirection: "column",
        }}
      >
        {/* <Button block type="dashed" style={{ margin: "4px 0 4px 0" }} onClick={() => handleConfigBlock(params.id)}> */}
        <Button
          block
          type="dashed"
          style={{ margin: "4px 0 4px 0" }}
          onClick={handleModal}
        >
          MODIFICAR
        </Button>
        <Button
          block
          type="danger"
          style={{ margin: "4px 0 4px 0" }}
          onClick={() => handleDeleteBlock(params.id)}
        >
          ELIMINAR
        </Button>
      </Col>
    </Row>
  );
};

export default ListBlockSettings;
