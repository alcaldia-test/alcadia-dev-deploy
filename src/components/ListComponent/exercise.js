import React from "react";
import { Typography, Row, Col } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
const { Paragraph, Title } = Typography;

const ListExercise = ({
  title,
  description,
  image,
  equipment,
  id,
  handleDelete,
}) => {
  return (
    <Row
      style={{
        flexDirection: "row",
        border: "1px solid #cecece",
        marginBottom: 16,
      }}
    >
      <Col xl={6} lg={6} md={6} sm={24} xs={24}>
        <div style={{ position: "relative" }}>
          <img
            src={image.link}
            style={{ height: 104, objectFit: "cover", width: "100%" }}
          />
          <div className="badge-exercise">{equipment.title}</div>
        </div>
      </Col>
      <Col xl={16} lg={16} md={16} sm={24} xs={24} className="row-col-default">
        <div style={{ paddingTop: 8, paddingBottom: 8, height: "100%" }}>
          <Title level={4}>{title}</Title>
          <Paragraph ellipsis={{ rows: 2 }}>{description}</Paragraph>
        </div>
      </Col>
      <Col xl={2} lg={2} md={2} sm={24} xs={24}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            cursor: "pointer",
            alignItems: "center",
            height: "100%",
          }}
          onClick={() => handleDelete(id)}
        >
          <DeleteOutlined
            style={{
              fontSize: 18,
              marginBottom: 8,
              marginTop: 8,
              color: "red",
            }}
          />
        </div>
      </Col>
    </Row>
  );
};

export default ListExercise;
