import Exercise from "./exercise";
import ExerciseSettings from "./exerciseSettings";
import BlockSettings from "./blockSettings";
import BlocksForTrainings from "./blocksForTrainings";
import Trainings from "./trainings";

export const ListComponent = {
  Exercise,
  ExerciseSettings,
  BlockSettings,
  BlocksForTrainings,
  Trainings,
};
