import React from "react";
import { Typography, Row, Col, Select, Input } from "antd";
const { Paragraph, Title } = Typography;

const ListExerciseSettings = ({
  title,
  description,
  image,
  equipment,
  repetitions,
  typeExercise,
  id,
  handleChange = () => {},
}) => {
  return (
    <div style={{ position: "relative", padding: "0 16px 0 16px" }}>
      <Row
        style={{
          flexDirection: "row",
          border: "1px solid #cecece",
          marginBottom: 16,
        }}
      >
        <Col xl={6} lg={6} md={6} sm={24} xs={24}>
          <div style={{ position: "relative" }}>
            <img
              src={image.link}
              style={{ height: 104, objectFit: "cover", width: "100%" }}
            />
            <div className="badge-exercise">{equipment.title}</div>
          </div>
        </Col>
        <Col
          xl={12}
          lg={12}
          md={12}
          sm={24}
          xs={24}
          className="row-col-default"
        >
          <div style={{ paddingTop: 8, paddingBottom: 8, height: "100%" }}>
            <Title level={4}>{title}</Title>
            <Paragraph ellipsis={{ rows: 2 }}>{description}</Paragraph>
          </div>
        </Col>
        <Col
          xl={6}
          lg={6}
          md={6}
          sm={24}
          xs={24}
          style={{
            display: "flex",
            justifyContent: "space-evenly",
            flexDirection: "column",
          }}
        >
          <Select
            value={typeExercise ?? true}
            onChange={(value) => handleChange("typeExercise", value, id)}
            defaultValue={typeExercise ?? true}
          >
            <Select.Option value={true}>Repeticiones</Select.Option>
            <Select.Option value={false}>Segundos</Select.Option>
          </Select>
          <Input
            value={repetitions ?? 10}
            defaultValue={repetitions ?? 10}
            type="number"
            placeholder="Ingrese una cantidad"
            onChange={({ target }) =>
              handleChange("repetitions", parseInt(target.value), id)
            }
          />
        </Col>
      </Row>
    </div>
  );
};

export default ListExerciseSettings;
