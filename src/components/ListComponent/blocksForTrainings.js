import React, { useEffect } from "react";
import { Divider, Modal } from "antd";
import { customUseReducer } from "@utils/customHooks";
import { ListComponent } from "@components/ListComponent";
import { batch } from "react-redux";
import { isEqual } from "lodash";
import { defaultValues } from "../../config";

const initialState = {
  modal: false,
  blockValues: {},
};

const ListBlocksForTrainings = ({
  handleDeleteBlock = () => {},
  handleConfigBlock = () => {},
  blockValues = {},
  ...params
}) => {
  const [state, dispatchComponent] = customUseReducer(initialState);
  useEffect(() => {
    !isEqual(blockValues, {}) && dispatchComponent({ blockValues });
  }, [blockValues]);

  const handleChange = (nameParam, value, exerciseId) => {
    dispatchComponent({
      blockValues: {
        ...state.blockValues,
        exercises: state.blockValues.exercises.map((values) => {
          if (values.id !== exerciseId) return values;
          return {
            ...values,
            [nameParam]: value,
          };
        }),
      },
    });
  };

  const handleChangeRepetition = (params) => {
    handleConfigBlock({
      ...blockValues,
      repetitions: parseInt(params.target.value),
    });
  };

  const saveChangedValues = () => {
    batch(() => {
      handleConfigBlock(state.blockValues);
      dispatchComponent({ modal: false });
    });
  };

  const cancelChangedValues = () => {
    dispatchComponent({ modal: false, blockValues });
  };

  return (
    <>
      <ListComponent.BlockSettings
        {...params}
        handleModal={() => dispatchComponent({ modal: true })}
        handleDeleteBlock={handleDeleteBlock}
        handleChangeRepetition={handleChangeRepetition}
        value={blockValues.repetitions ?? defaultValues.blockRepeats}
      />
      {/* Esto hay que pasarlo a un modal en el padre */}
      <Modal
        title="Configurar Bloque"
        centered
        visible={state.modal}
        onCancel={cancelChangedValues}
        onOk={saveChangedValues}
        okText="Guardar"
        cancelText="Cerrar"
        width="100%"
      >
        <p className="gx-text-grey">Titulo del Bloque</p>
        <h2 className="gx-text-uppercase gx-text-black gx-font-weight-bold gx-fnd-title">
          {params.title}
        </h2>
        <Divider dashed />
        <h3>LISTA DE EJERCICIOS</h3>
        <p>
          Por favor, indique la cantidad de repeticiones o tiempo que durará
          cada ejercicio
        </p>
        {state?.blockValues?.exercises?.map((props, key) => {
          return (
            <ListComponent.ExerciseSettings
              key={key}
              handleChange={handleChange}
              {...props}
            />
          );
        })}
      </Modal>
    </>
  );
};

export default ListBlocksForTrainings;
