import { Tag } from "antd";
import "antd/lib/tag/style/index.css";

const SimpleTag = (props) => <Tag {...props} style={{ borderRadius: 20 }} />;

export default SimpleTag;
