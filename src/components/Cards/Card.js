import "antd/lib/card/style/index.css";
import { Card as AntdCard } from "antd";
import styles from "./Cards.module.scss";

/**
 * Base Card Main Component
 */
const Card = ({ children = null, ...rest }) => {
  return (
    <div className={styles.antd}>
      <AntdCard {...rest}>{children}</AntdCard>
    </div>
  );
};

export default Card;

/**
 * Meta Subcomponent for Cards Usage
 */
const Meta = (props) => {
  return (
    <>
      <AntdCard.Meta {...props} />
    </>
  );
};

/**
 * Grid Subcomponent for Cards Usage
 */
const Grid = (props) => {
  return (
    <>
      <AntdCard.Grid {...props} />
    </>
  );
};

export { Grid, Meta };
