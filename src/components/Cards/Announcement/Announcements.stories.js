import React from "react";
import { withDesign } from "storybook-addon-designs";
import Button from "@components/Button";

import VotationsCard from "./index.js";

export default {
  title: "Components/Cards/Card Votations",
  component: VotationsCard,
  decorators: [withDesign],
};

const ExampleCover = (
  <img
    alt="card cover img"
    src="https://static.eldeber.com.bo/Files/Sizes/2021/5/4/el-nuevo-distintivo-en-la-paz-i-archivo._597856594_1140x520.jpg"
  />
);

const Template = (args) => {
  const { children, ...rest } = args;
  return (
    <>
      <VotationsCard {...rest}>{children}</VotationsCard>
    </>
  );
};

export const Default = Template.bind({});

Default.args = {
  children: (
    <>
      <Button
        key={`${Math.random()}`}
        type="primary"
        shape="round"
        size="small"
      >
        Ver resultado
      </Button>
      <Button
        key={`${Math.random()}`}
        type="secondary"
        shape="round"
        size="small"
      >
        comparte
      </Button>
    </>
  ),
  title:
    "Elección para ver si se va a asfaltar la calle mendieta o se arreglará  la plaza Iturralde",
  content:
    "Varios ciudadanos habian declarado que la plaza Iturralde debía ser arreglada para que los ciudadanos puedan ir a visitarla en buenas condiciones mientras que otros sostenían que la calle Mendieta necesitaba ser asfaltada. En esta primera votación veremos que es mas urgente y que prefieren los ciudadanos que se haga en primer lugar. A continuación se mostrará más información al respecto del estado actual de ambos lugares.",
  zonas: {
    zones: ["Achachicala", "Vino Tinto", "Villafatima"],
    zonesLink: "#",
  },
  startDate: "2021-12-01T13:41:08.000Z",
  dueDate: "2021-12-30T13:41:08.000Z",
  withStatus: true,
  hoverable: false,
  style: {
    width: "382rem",
  },
  cover: ExampleCover,
  loading: false,
  voted: false,
  onChange: () => console.log("cambio de vista"),
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};

export const moreThanThreeZones = Template.bind({});

moreThanThreeZones.args = {
  children: (
    <>
      <Button
        key={`${Math.random()}`}
        type="primary"
        shape="round"
        size="small"
      >
        Ver resultado
      </Button>
      <Button
        key={`${Math.random()}`}
        type="secondary"
        shape="round"
        size="small"
      >
        comparte
      </Button>
    </>
  ),
  title:
    "Elección para ver si se va a asfalt sar la calle mendieta o se arreglará  la plaza Iturralde",
  content:
    "Varios ciudadanos habian declarado que la plaza Iturralde debía ser arreglada para que los ciudadanos puedan ir a visitarla en buenas condiciones mientras que otros sostenían que la calle Mendieta necesitaba ser asfaltada. En esta primera votación veremos que es mas urgente y que prefieren los ciudadanos que se haga en primer lugar. A continuación se mostrará más información al respecto del estado actual de ambos lugares.",
  zonas: {
    zones: [
      "Achachicala",
      "Vino Tinto",
      "Villafatima",
      "otra zona",
      "segunda zona",
    ],
    zonesLink: "#",
  },
  startDate: "2021-12-01T13:41:08.000Z",
  dueDate: "2021-12-30T13:41:08.000Z",
  withStatus: true,
  hoverable: false,
  style: {
    width: "382rem",
  },
  cover: ExampleCover,
  loading: false,
  voted: false,
  onChange: () => console.log("cambio de vista"),
};

moreThanThreeZones.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};

export const allZonesCard = Template.bind({});

allZonesCard.args = {
  children: (
    <>
      <Button
        key={`${Math.random()}`}
        type="primary"
        shape="round"
        size="small"
      >
        Ver resultado
      </Button>
      <Button
        key={`${Math.random()}`}
        type="secondary"
        shape="round"
        size="small"
      >
        comparte
      </Button>
    </>
  ),
  title:
    "Elección para ver si se va a asfaltar la calle mendieta o se arreglará  la plaza Iturralde",
  content:
    "Varios ciudadanos habian declarado que la plaza Iturralde debía ser arreglada para que los ciudadanos puedan ir a visitarla en buenas condiciones mientras que otros sostenían que la calle Mendieta necesitaba ser asfaltada. En esta primera votación veremos que es mas urgente y que prefieren los ciudadanos que se haga en primer lugar. A continuación se mostrará más información al respecto del estado actual de ambos lugares.",
  zonas: {
    zones: ["all"],
    zonesLink: "#",
  },
  startDate: "2021-12-01T13:41:08.000Z",
  dueDate: "2021-12-30T13:41:08.000Z",
  withStatus: true,
  hoverable: false,
  style: {
    width: "382rem",
  },
  cover: ExampleCover,
  loading: false,
  voted: false,
  onChange: () => console.log("cambio de vista"),
};

allZonesCard.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};

export const VotationsEnded = Template.bind({});

VotationsEnded.args = {
  children: (
    <>
      <Button
        key={`${Math.random()}`}
        type="primary"
        shape="round"
        size="small"
      >
        Ver resultado
      </Button>
      <Button
        key={`${Math.random()}`}
        type="secondary"
        shape="round"
        size="small"
      >
        comparte
      </Button>
    </>
  ),
  title:
    "Elección para ver si se va a asfaltar la calle mendieta o se arreglará la plaza Iturralde",
  content:
    "Varios ciudadanos habian declarado que la plaza Iturralde debía ser arreglada para que los ciudadanos puedan ir a visitarla en buenas condiciones mientras que otros sostenían que la calle Mendieta necesitaba ser asfaltada. En esta primera votación veremos que es mas urgente y que prefieren los ciudadanos que se haga en primer lugar. A continuación se mostrará más información al respecto del estado actual de ambos lugares.",
  hoverable: false,
  style: {
    width: "382rem",
  },
  cover: ExampleCover,
  loading: false,
  zonas: {
    zones: ["Achachicala", "Vino Tinto", "Villafatima"],
    zonesLink: "#",
  },
  withStatus: true,
  startDate: "2021-10-01T13:41:08.000Z",
  dueDate: "2021-10-30T13:41:08.000Z",
  onChange: () => console.log("cambio de vista"),
};

VotationsEnded.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};

export const VotationsJustEndDate = Template.bind({});

VotationsJustEndDate.args = {
  children: (
    <>
      <Button
        key={`${Math.random()}`}
        type="primary"
        shape="round"
        size="small"
      >
        Ver resultado
      </Button>
      <Button
        key={`${Math.random()}`}
        type="secondary"
        shape="round"
        size="small"
      >
        comparte
      </Button>
    </>
  ),
  title:
    "Elección para ver si se va a asfaltar la calle mendieta o se arreglará la plaza Iturralde",
  content:
    "Varios ciudadanos habian declarado que la plaza Iturralde debía ser arreglada para que los ciudadanos puedan ir a visitarla en buenas condiciones mientras que otros sostenían que la calle Mendieta necesitaba ser asfaltada. En esta primera votación veremos que es mas urgente y que prefieren los ciudadanos que se haga en primer lugar. A continuación se mostrará más información al respecto del estado actual de ambos lugares.",
  hoverable: false,
  zonas: {
    zones: ["Achachicala", "Vino Tinto", "Villafatima"],
    zonesLink: "#",
  },
  style: {
    width: "382rem",
  },
  dueDate: "08-25-2021",
  cover: ExampleCover,
  loading: false,
  voted: false,
  onChange: () => console.log("cambio de vista"),
};

VotationsJustEndDate.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};

export const VotationNoZones = Template.bind({});

VotationNoZones.args = {
  children: (
    <>
      <Button
        key={`${Math.random()}`}
        type="primary"
        shape="round"
        size="small"
      >
        Ver resultado
      </Button>
      <Button
        key={`${Math.random()}`}
        type="secondary"
        shape="round"
        size="small"
      >
        comparte
      </Button>
    </>
  ),
  title:
    "Elección para ver si se va asfaltar la calle mendieta o se arreglará la plaza Iturralde",
  content:
    "Varios ciudadanos habian declarado que la plaza Iturralde debía ser arreglada para que los ciudadanos puedan ir a visitarla en buenas condiciones mientras que otros sostenían que la calle Mendieta necesitaba ser asfaltada. En esta primera votación veremos que es mas urgente y que prefieren los ciudadanos que se haga en primer lugar. A continuación se mostrará más información al respecto del estado actual de ambos lugares.",
  hoverable: false,
  style: {
    width: "382rem",
  },
  dueDate: "08-25-2021",
  cover: ExampleCover,
  loading: false,
  voted: false,
  onChange: () => console.log("cambio de vista"),
};

VotationNoZones.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};
