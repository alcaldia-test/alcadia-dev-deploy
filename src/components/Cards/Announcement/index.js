import { useFormateDate, useFormateStringDate } from "@utils/dateHooks";
import { useJoinByComma } from "@utils/joinZones";
import { validateAnnouncementState } from "@utils/announcementStates";
import moment from "moment";

import Card from "@components/Cards";
import Button from "@components/Button";
import { PaperLine } from "@assets/icons";

import styles from "./Announcements.module.scss";

export default function AnnouncementCard({
  title,
  zones,
  image,
  receptionStart,
  receptionEnd,
  reviewStart,
  reviewEnd,
  supportingStart,
  supportingEnd,
  postResults,
  directPosts,
  proposals,
  buttonChildren,
  children,
}) {
  const {
    waitingReception = false,
    isReception = false,
    isDirectReception = false,
    waitingReview = false,
    isReview = false,
    waitingSupport = false,
    isSupporting = false,
    isEvaluating = false,
    isFinished = false,
  } = validateAnnouncementState({
    receptionStart,
    receptionEnd,
    reviewStart,
    reviewEnd,
    supportingStart,
    supportingEnd,
    postResults,
    directPosts,
  });
  const zonesText = useJoinByComma(zones);

  return (
    <Card className={styles.card__announcement}>
      <img
        src={image || "http://placehold.jp/63452c/ffffff/640x480.png"}
        alt={title}
        className={styles.card__announcement_image}
      />
      <span className={styles.card__title}>{title}</span>
      <div className={styles.card__status}>
        {waitingReception && (
          <div className={styles.card__aproving_days}>
            La recepción de propuestas inicia el{" "}
            {useFormateDate(
              moment(useFormateStringDate(receptionStart)).startOf("day")
            )}
          </div>
        )}
        {isReception && (
          <div className={styles.card__reception_days}>
            Recepción de propuestas hasta{" "}
            {useFormateDate(
              moment(useFormateStringDate(receptionEnd)).endOf("day")
            )}
          </div>
        )}
        {waitingReview && (
          <div className={styles.card__aproving_days}>
            La aprobación de propuestas inicia el{" "}
            {useFormateDate(
              moment(useFormateStringDate(reviewStart)).startOf("day")
            )}
          </div>
        )}
        {isReview && (
          <div className={styles.card__aproving_days}>
            Aprobación de Propuestas finaliza el{" "}
            {useFormateDate(
              moment(useFormateStringDate(reviewEnd)).endOf("day")
            )}
          </div>
        )}
        {waitingSupport && (
          <div className={styles.card__aproving_days}>
            La recolección de apoyos inicia el{" "}
            {useFormateDate(
              moment(useFormateStringDate(supportingStart)).startOf("day")
            )}
          </div>
        )}
        {isSupporting && (
          <div className={styles.card__content_flexible}>
            <div className={styles.card__support_days}>
              Recolección de apoyos hasta{" "}
              {useFormateDate(
                moment(useFormateStringDate(supportingEnd)).endOf("day")
              )}
            </div>
          </div>
        )}
        {isEvaluating && (
          <div className={styles.card__evaluating_days}>
            Convocatoria evaluando ganadores...
          </div>
        )}
        {isDirectReception && (
          <div className={styles.card__reception_days}>
            Recepción de propuestas hasta{" "}
            {useFormateDate(
              moment(useFormateStringDate(receptionEnd)).endOf("day")
            )}
          </div>
        )}
        {isFinished && (
          <div className={styles.card__finished}>Convocatoria finalizada</div>
        )}
      </div>
      <div className={styles.card__items_container}>
        <span className={styles.card__recolection_data}>
          <PaperLine /> {proposals || 0}
        </span>
        {zonesText && <a className={styles.card__zones_link}>{zonesText}</a>}
      </div>
      <div className={styles.card__buttons_containers}>
        {buttonChildren && (
          <Button type="primary" shape="round" size="small">
            {isFinished
              ? "Ingresa y ve los ganadores"
              : isEvaluating
              ? "Ingresa y ve las propuestas"
              : "Ingresa y participa"}
          </Button>
        )}
        {children}
      </div>
    </Card>
  );
}
