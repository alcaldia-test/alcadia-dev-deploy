import Card from "@components/Cards";
import styles from "./DashboardCard.module.scss";
import { getColor, useTextFormater } from "./hook";
import { MessageOutlined } from "@ant-design/icons";

export const STATUS_DISCUSSION = {
  CREATING: -1,
  FINISHED: 0,
  PROGRESS: 1,
};
/**
 * @param width String
 * @param height String
 * @param vignette Button that it's in top right, it's like hamburguer nav, but with three dots.
 * @param title Title for this comment card
 * @param description Description for this comment card
 * @param status Object that denotes status from this card, must have two properties: "code" and "text", explanation:
 * * * import STATUS_DISCUSSION for get these constants, Its
 * * * code === STATUS_DISCUSSION.CREATING === -1 ( It's creating )
 * * * code === STATUS_DISCUSSION.FINISHED === 0  ( It's finished )
 * * * code === STATUS_DISCUSSION.PROGRESS === 1  ( It's in progress )
 * * * * Example:
 * * * * status = {
 * * * *    code: STATUS_DISCUSSION.PROGRESS
 * * * *    text: "En progreso"
 * * * *  }
 * @param numberComments Number for comments
 * @param numberComplaints Number for complaints
 * @param button Last button that show that is next
 * @param props Rest props of a Card.
 * @return A DashboardCardComment
 */
function DashboardCardComment({
  width,
  height,
  title,
  description,
  status,
  numberComments,
  numberComplaints,
  button,
  vignette,
  ...props
}) {
  const color = getColor(status?.code);
  const { shortText, showText } = useTextFormater(description, 65);
  const isCreating = status?.code === -1;
  if (numberComments === 0) numberComments = "Sin";
  if (numberComplaints === 0) numberComplaints = "Sin";

  const Vignette = () => (
    <div className={`${styles["dashboardcard-vignette"]}`}>{vignette}</div>
  );
  const Title = () => (
    <div className={`${styles["dashboardcardcomment-title"]}`}>{title}</div>
  );
  const Description = () => (
    <div className={`${styles["dashboardcardcomment-description"]}`}>
      {shortText}
      {showText && <a>{showText}</a>}
    </div>
  );
  const Info = () => (
    <div className={`${styles["dashboardcardcomment-info"]}`}>
      <span className={`${styles["dashboardcardcomment-duration"]} ${color}`}>
        {status?.text}
      </span>
      {!isCreating && (
        <div className={`${styles["dashboardcardcomment-debate"]}`}>
          <div className={`${styles["dashboardcardcomment-comment"]}`}>
            <MessageOutlined></MessageOutlined>
            <span>
              {numberComments} comentario{numberComments !== 1 ? "s" : ""}
            </span>
          </div>
          <div className={`${styles["dashboardcardcomment-complaint"]}`}>
            <span>
              {numberComplaints} denuncia{numberComplaints !== 1 ? "s" : ""}
            </span>
          </div>
        </div>
      )}
    </div>
  );
  const Button = () => (
    <div className={`${styles["dashboardcard-button"]}`}> {button} </div>
  );

  return (
    <Card
      className={`${styles["dashboardcard-container"]}`}
      style={{ width: width, height: height }}
      {...props}
    >
      <Vignette />
      <Title />
      <Description />
      <Info />
      <Button />
    </Card>
  );
}
export default DashboardCardComment;
