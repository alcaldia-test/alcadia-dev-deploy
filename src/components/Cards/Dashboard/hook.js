import styles from "./DashboardCard.module.scss";
import { STATUS_TASK } from "./DashboardCard";
import draftToHtml from "draftjs-to-html";
import moment from "moment";
/**
 *
 * @param {*timeTask} Number time in miliseconds
 * @returns time with this format (DAYS)d: (HOURS)h: (MINUTES)min
 */
export const useDuration = (timeTask) => {
  if (isNaN(timeTask)) return null;
  else if (timeTask === STATUS_TASK.CREATING) return "En Creación";
  else if (timeTask === STATUS_TASK.FINISHED) return "Finalizado";
  const dayToMs = 86400000;
  const hourToDay = 24;
  const minToHour = 60;
  const days = parseInt(timeTask) / dayToMs;
  const hours = (days - parseInt(days)) * hourToDay;
  const minutes = (hours - parseInt(hours)) * minToHour;

  return `${parseInt(days)}d: ${parseInt(hours)}h: ${parseInt(minutes)}min`;
};
/**
 * @param time Number time of a task with these specifications:
 * timeTask === -1 : TASK IS CREATING (state-creating)
 * timeTask ===  0 : TASK IS FINISHED (state-finished)
 * else timeTask   : TASK IS WAITING  (state-waiting)
 * @returns A string that Hash a scss class with his styles
 */
export const getColor = (timeTask) => {
  return timeTask === "Finalizado"
    ? styles["state-finished"]
    : timeTask === "En Creación"
    ? styles["state-creating"]
    : styles["state-waiting"];
};
/**
 * @param {*text} String text to evaluate
 * @param {*maxChars} Number max lenght permitted
 * @returns an Object with a substring Text and an shortener that is "...Leer mas"
 */
export const useTextFormater = (text, maxChars) => {
  let contentAdjusted;
  try {
    contentAdjusted = JSON.parse(text);
    const t = contentAdjusted.blocks[0].text.substring(0, maxChars) + " ...";
    contentAdjusted.blocks[0].text = t;
    contentAdjusted.blocks = [contentAdjusted.blocks[0]];
    contentAdjusted = draftToHtml(contentAdjusted);
  } catch (error) {
    contentAdjusted =
      text && text.length > maxChars ? text.substring(0, maxChars) : text;
  }

  const showText = text && text.length > maxChars ? "... Leer más" : null;
  return {
    shortText: contentAdjusted,
    showText: showText,
  };
};

/**
 * @param Array An array with all zones
 * @returns An Object with small and large text, all with join with Comma
 */
export const useJoinByComma = (array) => {
  let largeTextZones = null;
  let shortTextZones = "Todas las zonas";
  if (!Array.isArray(array))
    return { largeTextZones: null, shortTextZones: null };
  else if (array[0] === "all") shortTextZones = "Todas las Zonas";
  else if (array.length > 2) {
    largeTextZones = array.join(", ");
    shortTextZones = `Varias Zonas (${array.length})`;
  } else if (array.length > 1) shortTextZones = array.join(", ");
  else if (array.length === 1) shortTextZones = array[0];

  return {
    largeTextZones: largeTextZones,
    shortTextZones: shortTextZones,
  };
};

export const remainingTime = (
  dueDateAsString,
  startDateAsString,
  currentTime
) => {
  const dueDate = moment(dueDateAsString);
  const startDate = moment(startDateAsString);

  if (startDate.isValid()) {
    const now = moment(currentTime);
    if (startDate.diff(now, "seconds") > 0) {
      return "Sin empezar";
    }
  }

  if (dueDate.isValid()) {
    const now = moment(currentTime);
    const _diffSecondsTotal = dueDate.diff(now, "seconds"); // That give the result in seconds.
    const _diffDays = _diffSecondsTotal / (60 * 60 * 24); // we get the different divide into 86400 (number of seconds in a day)
    const _diffHours = (_diffDays % 1) * 24; // we get decimal part and multiply it by 24 to convert that tu hours
    const _diffSeconds = (_diffHours % 1) * 60; // we get the decimal part of the remanin hours and conert that to minutes

    if (_diffDays < 0) {
      return "Finalizado";
    } else {
      return `${Math.trunc(_diffDays)}d: ${Math.trunc(
        _diffHours
      )}h: ${Math.trunc(_diffSeconds)}min`;
    }
  } else {
    return -1;
  }
};

export const filterMenuByPermission = (permission, options, filter) => {
  let allOptions = [...options];
  if (filter) {
    if (!permission?.canEdit) {
      allOptions = allOptions.filter((x) => x.permission !== "edit");
    }
    if (!permission?.canCreate) {
      allOptions = allOptions.filter((x) => x.permission !== "create");
    }
    if (!permission?.canDelete) {
      allOptions = allOptions.filter((x) => x.permission !== "delete");
    }
  }
  return allOptions;
};
