import { withDesign } from "storybook-addon-designs";
import DashboardCard from "./DashboardCard";
import DashboardCardComment from "./DashboardCardComment";
import Button from "@components/Button";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idDashboardCard = {
  default: "16%3A9112",
};

export default {
  title: "Components/Cards/DashboardCard",
  component: DashboardCard,
  decorators: [withDesign],
  argTypes: {},
};

const image = (
  <img src="https://s3-alpha-sig.figma.com/img/7561/2fc2/03b7ab7e3bc48a297f657f31b7cc76b2?Expires=1639958400&Signature=TBr-NPYVpKNy1BNy8X5t-Sh6Wgdc9u1puBibJJuetjPmYXBRuhgEMKhPJ5zGYTVzvzfQUU90DuIZXM2wHqkMeepeODQcCdGEJ080D7i3SlEWDXh7J0aOdmlZk2iG7RFPjiIOn2PBwLSEmL1l1JjwazGDIQ7tXlxD9-oJ15z9RmN2c-8r2H0WX52itR8ITPiy1qbM42WVMhGGy5D4PXEoyhOPhSQkLJLEv~f~JWNLAAoscBcBcWigwgSO0yx~TtBwTr~bjWF8X4YMZOh50J2Rh7UrjscVtqMrIzN-KNToJ0IxrpUN0z~pksepc2IH1Ub~aa2Ln~uXTHkyEBV~wba6Ew__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" />
);

const Template = (args) => <DashboardCard {...args} />;
const TemplateComment = (args) => <DashboardCardComment {...args} />;

export const ConcursoEnCurso = Template.bind({});

ConcursoEnCurso.args = {
  image: image,
  width: "300rem",
  height: "max-content",
  title: "Propuestas ciudadanas para mejorar las zonas transitadas",
  timeTask:
    new Date("December 5, 2021 18:30:00").getTime() -
    new Date("December 3, 2021 14:30:00").getTime(),
  zones: {
    zones: ["Achachicala", "Vino Tinto", "Villafatima", "La Paz", "Otra Zona"],
    zonesLink: "#",
  },
  button: (
    <Button type="secondary" size="small">
      Administrar Propuestas &gt;{" "}
    </Button>
  ),
  vignette: <span>...</span>,
};

ConcursoEnCurso.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idDashboardCard.default}`,
  },
};

export const ConcursoSinPublicar = Template.bind({});

ConcursoSinPublicar.args = {
  image: image,
  width: "300rem",
  height: "max-content",
  title: "Propuestas ciudadanas para mejorar las zonas transitadas",
  timeTask: -1,
  zones: {
    zones: ["Achachicala", "Vino Tinto", "Villafatima", "La Paz", "Otra Zona"],
    zonesLink: "#",
  },
  button: (
    <Button type="secondary" size="small">
      Continuar &gt;{" "}
    </Button>
  ),
  vignette: <span>...</span>,
};

ConcursoSinPublicar.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idDashboardCard.default}`,
  },
};

export const ConcursoFinalizado = Template.bind({});

ConcursoFinalizado.args = {
  image: image,
  width: "300rem",
  height: "max-content",
  title: "Propuestas ciudadanas para mejorar las zonas transitadas",
  timeTask: 0,
  zones: {
    zones: ["Achachicala", "Vino Tinto", "Villafatima", "La Paz", "Otra Zona"],
    zonesLink: "#",
  },
  button: (
    <Button type="secondary" size="small">
      Administrar Propuestas &gt;{" "}
    </Button>
  ),
  vignette: <span>...</span>,
};

ConcursoFinalizado.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idDashboardCard.default}`,
  },
};

export const ConcursoLogrado = Template.bind({});

ConcursoLogrado.args = {
  image: image,
  width: "300rem",
  height: "max-content",
  title: "Se mejoró la calle de la zona de Miraflores",
  button: (
    <Button type="secondary" size="small">
      Ver publicación &gt;{" "}
    </Button>
  ),
  vignette: <span>...</span>,
};

ConcursoLogrado.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idDashboardCard.default}`,
  },
};

export const DebateEnCurso = TemplateComment.bind({});

DebateEnCurso.args = {
  width: "300rem",
  height: "max-content",
  title: "Ley de gobierno autonomo municipal",
  description:
    "Es necesario y de extrema urgencia, la construcción ningunear y menguar los recursos de Madrid, tanto ciudad como Comunidad Autónoma. Generalmente, los aspectos más sonantes versan sobre nuestra fiscalidad",
  status: { code: 1, text: "En progreso" },
  numberComments: 30,
  comments: <span>Comentarios</span>,
  numberComplaints: 0,
  complaints: <span>Denuncias</span>,
  button: (
    <Button type="secondary" size="small">
      Administrar Normativa &gt;{" "}
    </Button>
  ),
  vignette: <span>...</span>,
};

DebateEnCurso.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idDashboardCard.default}`,
  },
};
export const DebateSinPublicar = TemplateComment.bind({});

DebateSinPublicar.args = {
  width: "300rem",
  height: "max-content",
  title: "Ley de gobierno autonomo municipal",
  description:
    "Es necesario y de extrema urgencia, la construcción ningunear y menguar los recursos de Madrid, tanto ciudad como Comunidad Autónoma. Generalmente, los aspectos más sonantes versan sobre nuestra fiscalidad",
  status: { code: -1, text: "En Creación..." },
  numberComments: 30,
  comments: <span>Comentarios</span>,
  numberComplaints: 0,
  complaints: <span>Denuncias</span>,
  button: (
    <Button type="secondary" size="small">
      Continuar &gt;{" "}
    </Button>
  ),
  vignette: <span>...</span>,
};

DebateSinPublicar.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idDashboardCard.default}`,
  },
};

export const DebateFinalizado = TemplateComment.bind({});

DebateFinalizado.args = {
  width: "300rem",
  title: "Ley de gobierno autonomo municipal",
  description:
    "Es necesario y de extrema urgencia, la construcción ningunear y menguar los recursos de Madrid, tanto ciudad como Comunidad Autónoma. Generalmente, los aspectos más sonantes versan sobre nuestra fiscalidad",
  status: { code: 0, text: "Finalizada" },
  numberComments: 30,
  comments: <span>Comentarios</span>,
  numberComplaints: 1,
  complaints: <span>Denuncias</span>,
  button: (
    <Button type="secondary" size="small">
      Ver estadísticas &gt;{" "}
    </Button>
  ),
  vignette: <span>...</span>,
};

DebateFinalizado.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idDashboardCard.default}`,
  },
};
