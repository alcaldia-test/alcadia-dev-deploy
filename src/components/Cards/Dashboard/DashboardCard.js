import React, { useState } from "react";
import ImageNext from "next/image";
import Card from "../Card";
import styles from "./DashboardCard.module.scss";
import {
  remainingTime,
  getColor,
  useTextFormater,
  useJoinByComma,
  filterMenuByPermission,
} from "./hook";

export const STATUS_TASK = {
  CREATING: -1,
  FINISHED: 0,
};
/**
 * @param width String
 * @param height String
 * @param vignette Button that it's in top right, it's like hamburger nav, but with three dots.
 * @param image Image that show this task
 * @param title Title for this task
 * @param timeTask Number that denotes duration for this task in MILISECONDS, representing like that :
 * * import STATUS_TASK for get these constants, Its
 * *    * if timeTask === STATUS_TASK.CREATING -> TASK IS CREATING, his value its -1
 * *    *
 * *    * if timeTask === STATUS_TASK.FINISHED -> TASK IS FINISHED, his value its 0
 * *    *
 * *    * if timeTask !== 0 && timeTask !==-1 TIME TASK IS SHOW IN FORMAT:
 * *    * DAYS - MINUTES - SECONDS, his value its
 * *    *
 * @param zones object trae dos propiedades zones que es un arreglo de lugares y zonesLink
 * zonesLink ques es un string con el link que se usa en el href
 * quedaria algo como este ejemplo
 * {
 *  zones: ["Achachicala", "Vino Tinto", "Villafatima"],
 *  zonesLink: "https://[algo].[algo]",
 * }
 * de no traer zones el valor por defecto seria "varias zonas"
 * @param button Last button that show that is next
 * @param props Rest props of a Card.
 * @param state encurso|encreacion|finalizada
 * @param menustatuscurso menustatuscurso
 * @param menuencreacion menuencreacion
 * @param menufinalizado menufinalizado
 * @param statusCard statusCard
 * @return A DashboardCard
 */

// menu default
let menuEnCurso = [
  { tag: "content", name: "Ver Contenido", permission: "any" },
  // { tag: "stadistic", name: "Ver estadisticas", permission: "any" },
  // { tag: "edit", name: "Editar", permission: "edit" },
  { tag: "delete", name: "Eliminar Votación", permission: "delete" },
];

let menuEnCreacion = [
  { tag: "edit", name: "Continuar Creando", permission: "edit" },
  { tag: "delete", name: "Eliminar Votación", permission: "delete" },
];

let menuFinalizado = [
  { tag: "stadistic", name: "Ver Estadísticas", permission: "any" },
  { tag: "content", name: "Ver Resultados", permission: "any" },
  { tag: "delete", name: "Eliminar Votación", permission: "delete" },
];

function DashboardCard({
  width,
  height,
  image,
  title,
  timeTask,
  zones,
  button,
  vignette,
  state,
  onGoTo,
  startDate,
  menustatuscurso,
  menuencreacion,
  menufinalizado,
  statusCard,
  userPermision,
  withInfo = true,
  filterMenuBypermision = false,
  onMenuClick = () => {},
  remainingFunction = null,
  now,
  ...props
}) {
  // menu dinamic
  menuEnCurso = menustatuscurso || menuEnCurso;
  menuEnCreacion = menuencreacion || menuEnCreacion;
  menuFinalizado = menufinalizado || menuFinalizado;

  const duration =
    state === "creating"
      ? "En Creación"
      : remainingFunction
      ? remainingFunction()
      : remainingTime(timeTask, startDate, now);
  state =
    state === undefined
      ? duration === "Finalizado"
        ? "finished"
        : "active"
      : state;
  const color = getColor(duration);
  const { shortTextZones } = useJoinByComma(zones?.zones || zones);
  const { shortText, showText } = useTextFormater(title, 70);
  const [showMenu, setShowMenu] = useState(false);
  const [showZones, setShowZones] = useState(false);
  const [zonesView, setZonesView] = useState(0);

  const handleManageMenuAndZoneShow = (
    _showMenu,
    _showZones,
    navigateToVote = false
  ) => {
    setShowMenu(_showMenu);
    setShowZones(_showZones);
    if (navigateToVote) {
      onMenuClick("content");
    }
  };

  const handleMoveZoneInView = (e) => {
    const newZone = zonesView + e;
    if (newZone < zones.length - 1 && newZone > -1) {
      setZonesView(newZone);
    }
  };

  const Vignette = () => (
    <div
      className={`${styles["dashboardcard-vignette"]}`}
      onClick={() => handleManageMenuAndZoneShow(!showMenu, false)}
    >
      {vignette}
    </div>
  );

  const Status = () => <div className="text-center">{statusCard || ""}</div>;

  const Menu = () => (
    <div className={`${styles["dashboardcard-menu"]}`}>
      {state === "active" &&
        filterMenuByPermission(
          userPermision,
          menuEnCurso,
          filterMenuBypermision
        ).map((x, i) => (
          <span
            key={i}
            className={`${styles["dashboardcard-menu-item"]}`}
            onClick={() => {
              setShowMenu(false);
              onMenuClick(x.tag);
            }}
          >
            {x.name}
          </span>
        ))}
      {state === "creating" &&
        filterMenuByPermission(
          userPermision,
          menuEnCreacion,
          filterMenuBypermision
        ).map((x, i) => (
          <span
            key={i}
            className={`${styles["dashboardcard-menu-item"]}`}
            onClick={() => {
              setShowMenu(false);
              onMenuClick(x.tag);
            }}
          >
            {x.name}
          </span>
        ))}
      {state === "finished" &&
        filterMenuByPermission(
          userPermision,
          menuFinalizado,
          filterMenuBypermision
        ).map((x, i) => (
          <span
            key={i}
            className={`${styles["dashboardcard-menu-item"]}`}
            onClick={() => {
              setShowMenu(false);
              onMenuClick(x.tag);
            }}
          >
            {x.name}
          </span>
        ))}
    </div>
  );

  const Image = () => (
    <div
      style={{ height: height / 2.5 }}
      className={`${styles["dashboardcard-image"]}`}
      onClick={() => handleManageMenuAndZoneShow(false, false, true)}
    >
      {image}
    </div>
  );

  const Title = () => (
    <div
      className={`${styles["dashboardcard-title"]}`}
      onClick={() => {
        handleManageMenuAndZoneShow(false, false);
        if (onGoTo) onGoTo();
      }}
    >
      {shortText}
      {showText && <a>{showText}</a>}
    </div>
  );

  const Info = () => (
    <div className={`${styles["dashboardcard-info"]}`}>
      {duration && (
        <span
          className={`${styles["dashboardcard-duration"]} ${color}`}
          onClick={() => handleManageMenuAndZoneShow(false, false)}
        >
          {duration}
        </span>
      )}
      <div
        className={`${styles["dashboardcard-zones"]}`}
        onClick={() =>
          shortTextZones !== "Todas las zonas" &&
          handleManageMenuAndZoneShow(false, !showZones)
        }
      >
        {shortTextZones === "Todas las zonas" ? (
          <a>{shortTextZones}</a>
        ) : (
          <a href={zones?.zonesLink}>{shortTextZones}</a>
        )}
      </div>
      {showZones && (
        // <div className={`${styles["dashboardcard-zones-tooltip"]}`}>
        //   {zones.join(" , ").substring(0, 45)}{" "}
        //   {zones.join(" , ").length > 45 && "...otras"}
        // </div>
        <div className={`${styles["dashboardcard-zones-tooltip"]}`}>
          <span
            className="border-b-2 border-gray1 items-center flex justify-center h-2 cursor-pointer hover:bg-redCard"
            onClick={() => handleMoveZoneInView(-1)}
          >
            <ImageNext
              src={"/icons/arrow-up.svg"}
              layout="fixed"
              width={17}
              height={17}
              alt="xboxIcon"
            />
          </span>
          <div className="">
            <div className="">{zones[zonesView]}</div>
            <div className="">{zones[zonesView + 1]}</div>
          </div>
          <span
            className="border-t-2 border-gray1 items-center flex justify-center h-2 cursor-pointer hover:bg-redCard"
            onClick={() => handleMoveZoneInView(1)}
          >
            <ImageNext
              src={"/icons/arrow-down.svg"}
              layout="fixed"
              width={20}
              height={20}
              alt="xboxIcon"
            />
          </span>
        </div>
      )}
    </div>
  );

  const Button = () => (
    <div
      className={`${styles["dashboardcard-button"]}`}
      onClick={() => setShowMenu(false)}
    >
      {button}
    </div>
  );

  return (
    <Card
      onMouseLeave={() => handleManageMenuAndZoneShow(false, false)}
      style={{ width: width, height: height }}
      className={`${styles["dashboardcard-container"]}`}
      {...props}
    >
      <Vignette />
      {showMenu && <Menu />}
      <Image />
      <Title />
      <Status />
      {withInfo && <Info />}
      <Button />
    </Card>
  );
}
export default DashboardCard;
