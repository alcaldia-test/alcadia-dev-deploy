import { useState, useEffect } from "react";
import { useFormateDate, useDateIsAfter } from "@utils/dateHooks";
import { useJoinByComma } from "@utils/joinZones";
import { useTextFormater } from "../Dashboard/hook";

import Card from "@components/Cards";
import { getWorldTime } from "@utils/request";

import styles from "./Voting.module.scss";

export default function VotingCard({
  dueDate,
  title,
  zones,
  image,
  content,
  children,
}) {
  const [description, setDescription] = useState("");
  const [showText, setShowText] = useState("");
  const due = useFormateDate(dueDate);
  const zonesText = useJoinByComma(zones);
  const [isFinished, setIsFinished] = useState(false);

  useEffect(() => {
    if (content.includes(`"blocks"`)) {
      const textObj = JSON.parse(content);
      const textFormated = useTextFormater(textObj.blocks[0].text, 70);
      setDescription(textFormated.shortText);
      setShowText(textFormated.showText);
    } else {
      const textFormated = useTextFormater(content, 70);
      setDescription(textFormated.shortText);
      setShowText(textFormated.showText);
    }

    if (dueDate) setWorldDate();
  }, [content]);

  const setWorldDate = async () => {
    const worldDate = await getWorldTime();

    const finished = useDateIsAfter(dueDate, worldDate);

    setIsFinished(finished);
  };

  return (
    <Card className={styles.card}>
      <img
        src={image || "http://placehold.jp/63452c/ffffff/640x480.png"}
        alt={title}
        className={styles.card__image}
      />
      <h2 className={styles.card__title}>{title}</h2>
      <div className={styles.card__description}>
        {description}
        {showText && <span>{showText}</span>}
      </div>
      {!isFinished ? (
        <span className={styles.card__active}>Votación en proceso para:</span>
      ) : (
        <>
          <span className={styles.card__finished}>
            Votación finalizada para:
          </span>
        </>
      )}
      <div className={styles.card__items_container}>
        {zonesText && <a className={styles.card__zones_link}>{zonesText}</a>}
      </div>
      <div className={styles.card__dates}>
        {!isFinished ? (
          <span>La votación finaliza el {due}</span>
        ) : (
          <>
            <span>{due}</span>
          </>
        )}
      </div>
      <div className={styles.card__buttons_containers}>{children}</div>
    </Card>
  );
}
