import Card from "./Card";
import DashboardCard from "./Dashboard/DashboardCard";
import DashboardCardComment from "./Dashboard/DashboardCardComment";
import VotationsCard from "./Votations";
import ProposalCard from "./Proposals/ProposalCard";
import ProposalCardComment from "./Proposals/ProposalCardComment";
import CarouselCard from "./Carousel/CarouselCard";
import CardNormatives from "./CardNormativesAdmin";

export default Card;

export {
  CardNormatives,
  DashboardCard,
  DashboardCardComment,
  VotationsCard,
  ProposalCard,
  ProposalCardComment,
  CarouselCard,
};
