import React, { useState } from "react";
import Button from "@components/Button";
import { RightOutlined } from "@ant-design/icons";
import draftToHtml from "draftjs-to-html";
import styles from "./normativesCard.module.scss";
const adjustContent = (content) => {
  let contentAdjusted;
  try {
    contentAdjusted = JSON.parse(content);
    contentAdjusted.blocks[0].text = `${contentAdjusted.blocks[0].text.substring(
      0,
      65
    )} ...`;
    contentAdjusted.blocks = [contentAdjusted.blocks[0]];
  } catch (error) {
    contentAdjusted = content;
  }
  return contentAdjusted;
};

const setTextState = (state) => {
  switch (state) {
    case "FINISHED":
      return { command: " Ver estadísticas", tag: "Finalizada" };
    case "PUBLISHED":
      return { command: " Administrar", tag: "Publicada" };
    default:
      return { command: " Continuar", tag: "En Creación" };
  }
};
const setStyleState = (state) => {
  switch (state) {
    case "FINISHED":
      return styles.finalizada;
    case "PUBLISHED":
      return styles.inProgress;
    default:
      return styles.creating;
  }
};

const CardNormatives = ({
  id,
  title,
  content,
  state,
  comments,
  complaints,
  handleDelete,
  handleStatistics,
  handleEdit,
  handleAdmin,
  permissions,
}) => {
  const [openMenu, setOpenMenu] = useState(false);
  const contentAdjusted = adjustContent(content);
  const Menu = () => (
    <span
      className={styles.vignette}
      onClick={() => {
        setOpenMenu(!openMenu);
      }}
      onMouseLeave={() => {
        setOpenMenu(!openMenu);
      }}
    >
      <div className={styles.vignetteItem}></div>
      <div className={styles.vignetteItem}></div>
      <div className={styles.vignetteItem}></div>
      <div className={openMenu ? styles.menuOptions : styles.hideMenu}>
        {state !== "FINISHED" && permissions?.canEdit ? (
          <span className={styles.options} onClick={() => handleEdit(id)}>
            Editar
          </span>
        ) : (
          <span
            className={styles.options}
            onClick={() => {
              handleStatistics({ id });
            }}
          >
            Ver estadisticas
          </span>
        )}
        <span
          className={styles.options}
          onClick={() => {
            handleAdmin({ id });
          }}
        >
          Administrar
        </span>
        <span
          className={styles.options}
          onClick={() => {
            handleDelete({ title, id });
          }}
        >
          Eliminar
        </span>
      </div>
    </span>
  );
  const mainButton = (state) => {
    let handler;
    switch (state) {
      case "FINISHED":
        handler = handleStatistics;
        break;
      case "PUBLISHED":
        handler = handleAdmin;
        break;
      default:
        handler = handleEdit;
    }

    return (
      <Button type="secondary" size="small" onClick={handler}>
        <>
          {setTextState(state).command}
          <RightOutlined />
        </>
      </Button>
    );
  };

  return (
    <div className={`${styles.card} place-self-center`}>
      <div className={styles.elementsContainer}>
        <header>
          <h6 onClick={() => handleAdmin(id)}>{title}</h6>
          <Menu />
        </header>
        {/* // content */}
        {typeof contentAdjusted === "object" ? (
          <p
            dangerouslySetInnerHTML={{
              __html: draftToHtml(contentAdjusted),
            }}
          />
        ) : (
          <p onClick={() => handleAdmin(id)}>
            {contentAdjusted.split(" ").splice(0, 12).join(" ")}
          </p>
        )}

        {/* // state */}
        <span className={setStyleState(state)}>{setTextState(state).tag}</span>

        {/* // comments and denounces */}
        <div
          className={
            state === "FINISHED" || state === "PUBLISHED"
              ? styles.status
              : styles.noStatus
          }
        >
          <span className={styles.comments}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              width="16"
              height="16"
            >
              <path fill="none" d="M0 0h24v24H0z" />
              <path
                d="M6.455 19L2 22.5V4a1 1 0 0 1 1-1h18a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H6.455zm-.692-2H20V5H4v13.385L5.763 17zM11 10h2v2h-2v-2zm-4 0h2v2H7v-2zm8 0h2v2h-2v-2z"
                fill="rgba(84,84,84,1)"
              />
            </svg>
            {`${comments} comentarios`}
          </span>
          <span className={styles.complaints}>
            {complaints > 0
              ? `${complaints} denuncia${complaints > 1 ? "s" : ""}`
              : "Sin denuncias"}
          </span>
        </div>
      </div>

      <div className={styles.buttonContainer}>{mainButton(state)}</div>
    </div>
  );
};

export default CardNormatives;
