import React from "react";
import styles from "./CarouselCard.module.scss";
import Card from "@components/Cards";
/**
 * @param props Rest of props of a Card.
 * @return A CarouselCard
 */
function CarouselCard({
  width = "100%",
  height = "190rem",
  children,
  ...props
}) {
  const sizeStyle = { width: width, height: height };
  return (
    <Card style={sizeStyle} className={`${styles.carouselcard}`} {...props}>
      {children}
    </Card>
  );
}
export default CarouselCard;
