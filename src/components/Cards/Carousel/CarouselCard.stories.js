import { withDesign } from "storybook-addon-designs";
import CarouselCard from "./CarouselCard.js";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idCarouselCard = {
  default: "2528%3A8683",
};

export default {
  title: "Components/Cards/Carousel Card",
  component: CarouselCard,
  decorators: [withDesign],
  argTypes: {},
};

const Template = (args) => <CarouselCard {...args} />;

export const Default = Template.bind({});

const contentCarousel = (
  <div style={{ width: "100%", height: "100%", fontWeight: "normal" }}>
    <div style={{ width: "100%", height: "50%" }}>
      <div
        style={{
          width: "100%",
          height: "100%",
          background:
            "linear-gradient(180.1deg, rgba(163, 169, 175, 0) 0.09%, #1E1E1E 110.88%)",
        }}
      ></div>
    </div>
    <div style={{ width: "100%", height: "50%", padding: "4rem" }}>
      <div style={{ fontSize: "14rem" }} className="old-primary-red">
        Categoria o zona
      </div>
      <div style={{ fontSize: "16rem" }}>Este es un texto de pruebas</div>
    </div>
  </div>
);

Default.args = {
  children: contentCarousel,
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idCarouselCard.default}`,
  },
};
