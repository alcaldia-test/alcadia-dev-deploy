import Card from "@components/Cards";
import styles from "./ProposalCardComment.module.scss";
import { useTextFormater } from "../Dashboard/hook";
import Button from "@components/Button";

/**
 * @param width String
 * @param height String
 * This component has an object with constants that use named "PROPOSAL_CONTANTS", IMPORT TOO!!!!
 * @param title Title for this Card Proposal
 * @param description Description for this Card Proposal, its an object with two properties:
 * description = {
 * * text : "Texto Descriptivo",
 * * link : "www.en-caso-de-que-el-texto-sea-muy-largo.com",
 * }
 * @param isFinished Boolean that denotes if this discussion was finished
 * @param numberComments Number
 * @param zones Array of String, if the first element is "all", show "Todas las zonas",
 * else if length of this array is more than 2, show "Varias Zonas", else show the name
 * @param onClickLogin Calback for Login
 * @param onClickShare Callback for invite friends
 * @param props Rest props of a Card.
 * @return A Proposal Card
 */
function ProposalCardComment({
  width = "382rem",
  height = "536rem",
  title,
  description,
  isFinished,
  numberComments,
  zones,
  onClickLogin,
  onClickShare,
  shareName = "normativa",
  ...props
}) {
  // const { shortText, showText } = useTextFormater(description.text, 90);
  const { shortText } = useTextFormater(description.text, 90);

  const Title = () => (
    <div
      onClick={onClickLogin}
      className={`${styles["proposalcardcomment-title"]}`}
    >
      {title}{" "}
    </div>
  );
  const Description = () => {
    // const content = showText && <Link href={description.link}>{showText}</Link>;

    return (
      <div
        onClick={onClickLogin}
        className={`${styles["proposalcardcomment-description"]}`}
        dangerouslySetInnerHTML={{
          __html: shortText,
        }}
      />
    );
  };
  const InfoContainer = () => (
    <div
      onClick={onClickLogin}
      className={`${styles["proposalcardcomment-info"]}`}
    >
      <div className={`${styles["proposalcardcomment-comment"]}`}>
        <span>
          {numberComments} Comentario{numberComments !== 1 ? "s" : ""}
        </span>
      </div>
    </div>
  );
  const DiscussionClosed = () => (
    <div className={`${styles["proposalcardcomment-closed"]}`}>
      <span>Charla finalizada</span>
    </div>
  );
  const Spacer = () => (
    <div className={`${styles.spacer}`}>
      <span>.</span>
    </div>
  );
  const Login = () => (
    <Button type="primary" size="small" onClick={onClickLogin}>
      Ingresa{!isFinished ? " y comenta" : ""}
    </Button>
  );
  const Share = () => (
    <Button type="secondary" size="small" onClick={onClickShare}>
      Invita a tus amigos a la {shareName}
    </Button>
  );

  return (
    <Card
      className={`${styles["proposalcardcomment-container"]}`}
      style={{ width: width, height: "auto" }}
      {...props}
    >
      <Title />
      <Description />
      <InfoContainer />
      {isFinished ? <DiscussionClosed /> : <Spacer />}
      <div className={`${styles["proposalcardcomment-buttons"]}`}>
        <Login />
        <Share />
      </div>
    </Card>
  );
}
export default ProposalCardComment;
