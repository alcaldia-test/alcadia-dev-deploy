import { useState, useEffect } from "react";
import Link from "next/link";
import { useTextFormater } from "../Dashboard/hook";
import { Like, DisLike } from "@assets/icons";
import ReactPlayer from "react-player";
import { useRouter } from "next/router";

import Avatar from "@components/Avatar";
import Button from "@components/Button";
import Card from "@components/Cards";
import Modal from "@components/Modals/SupportModal";
import Share from "@components/Modals/Share";

import styles from "./ProposalCard.module.scss";
import { useDispatch } from "react-redux";
import { routeRedirectRegister } from "@redux/auth/actions";

const defaultProfile = {
  image: "/images/avatar.png",
  name: "Nombre",
};

const STATES = {
  CLOSED: "closed",
  ACTIVE: "active",
  REVIEW: "review",
};

/**
 * @param image Image that show this Card Proposal
 * @param title Title for this Card Proposal
 * @param stage Stage for this Card Proposal
 * @param description Description for this Card Proposal, its an object with two properties:
 * @param user Object with the data of the profile of the user that create this Card Proposal
 * @param reaction The state of the reaction of the user that see this Card Proposal
 * @param announcementId The id of the announcement that this Card Proposal belongs to
 * @param id The id of the Card Proposal
 *
 * @returns A Card Proposal
 *
 */
function ProposalCard({
  image,
  title,
  user,
  reaction,
  description,
  id,
  onOk,
  userId,
  stage,
  likes,
  dislikes,
  isInVotingZone,
  video,
}) {
  const router = useRouter();
  const [content, setContent] = useState("");
  const [showText, setShowText] = useState("");
  const [shareModal, setShareModal] = useState(false);
  const [reactionModal, setReactionModal] = useState(false);
  const [supportModal, setSupportModal] = useState(true);
  const [okText, setOkText] = useState("Si, la apoyo");
  const [modalURL, setModalURL] = useState("");
  const dispatch = useDispatch();
  const setUrl = () => {
    dispatch(routeRedirectRegister(window?.location.pathname));
  };
  useEffect(() => {
    try {
      const textObj = JSON.parse(description);
      const text = textObj.blocks.reduce((a, c) => a + c.text, "");
      const textFormated = useTextFormater(text, 70);
      setContent(textFormated.shortText);
      setShowText(textFormated.showText);
    } catch (error) {
      const textFormated = useTextFormater(description, 70);
      setContent(textFormated.shortText);
      setShowText(textFormated.showText);
    }
  }, [description]);

  const AgreeVoted = () => (
    <div
      className={styles.card__agreements__agree}
      onClick={(e) => {
        e.preventDefault();
        onOk({ id: id, reaction: "omit", userId: userId });
      }}
      disabled={!isInVotingZone}
    >
      <Like />
      <span>Apoyas la propuesta</span>
    </div>
  );
  const DisagreeVoted = () => (
    <div
      className={styles.card__agreements__disagree}
      onClick={(e) => {
        e.preventDefault();
        onOk({ id: id, reaction: "omit", userId: userId });
      }}
      disabled={!isInVotingZone}
    >
      <DisLike />
      <span>No apoyas la propuesta</span>
    </div>
  );
  const Vote = () => (
    <div
      className={
        userId
          ? styles.card__agreements__buttons
          : styles.card__agreements__buttons_no_login
      }
    >
      {userId ? (
        <>
          <Button
            type="secondary"
            size="super-small"
            className={styles.card__agreements__button}
            onClick={(e) => {
              e.preventDefault();
              setReactionModal(true);
              setSupportModal(true);
              setOkText("Si, la apoyo");
            }}
            disabled={!isInVotingZone}
          >
            <Like />
            <span>La apoyo</span>
          </Button>
          <Button
            type="secondary"
            size="super-small"
            className={styles.card__agreements__button}
            onClick={(e) => {
              e.preventDefault();
              setReactionModal(true);
              setSupportModal(false);
              setOkText("No la apoyo");
            }}
            disabled={!isInVotingZone}
          >
            <DisLike />
            <span>No la apoyo</span>
          </Button>
        </>
      ) : (
        <>
          <Link
            onClick={(e) => {
              e.stopPropagation();
            }}
            href={`/login?goto=${encodeURI(window.location.pathname)}`}
            as={`/login?goto=${encodeURI(window.location.pathname)}`}
          >
            <a>
              <Button onClick={setUrl} type="primary" size="small" block={true}>
                <span>Inicia sesión para participar</span>
              </Button>
            </a>
          </Link>
        </>
      )}
    </div>
  );

  const Agreements = () => (
    <>
      {reaction === ("any" || "omit" || "") && <Vote />}
      {reaction === "like" && <AgreeVoted />}
      {reaction === "dislike" && <DisagreeVoted />}
    </>
  );

  const Invitate = () => (
    <div
      className={styles.card__invite}
      onClick={(e) => {
        e.preventDefault();
        if (userId) {
          setShareModal(true);
          setModalURL(`${location.origin}/propuesta/${id}`);
        } else {
          router.push({
            pathname: "/login",
            query: { goto: encodeURI(window.location.pathname) },
          });
        }
      }}
    >
      Invitar a mis amigos a participar
    </div>
  );

  const FinishedStats = () => (
    <div className={styles.card__finished__stats}>
      <p className={styles.card__finished__stats__like}>
        <Like />
        <span>{likes}</span>
      </p>
      <p className={styles.card__finished__stats__dislike}>
        <DisLike />
        <span>{dislikes}</span>
      </p>
    </div>
  );

  const FinishedInvitate = () => (
    <div
      className={`${styles.card__invite__finished} ${styles.card__invite}`}
      onClick={(e) => {
        e.preventDefault();
        if (userId) {
          setShareModal(true);
          setModalURL(`${location.origin}/propuesta/${id}`);
        } else {
          router.push({
            pathname: "/login",
            query: { goto: encodeURI(window.location.pathname) },
          });
        }
      }}
    >
      Invitar a mis amigos a ver los resultados
    </div>
  );

  return (
    <>
      <Card className={styles.card}>
        {image && <img src={image} alt={title} className={styles.card_image} />}
        {video && (
          <div className={styles.video__container}>
            <ReactPlayer
              url={video}
              playing={false}
              width="100%"
              height="100%"
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                borderRadius: "15px",
                overflow: "hidden",
              }}
              controls={true}
            />
          </div>
        )}

        <div className={styles.card__content}>
          <div className={styles.card__profile}>
            <Link
              onClick={(e) => e.stopPropagation()}
              href="/usuario/[id]"
              as={`/usuario/${user?.id}`}
            >
              <a>
                <Avatar
                  src={user?.avatar || defaultProfile.image}
                  size="small"
                  className={styles.card__profile_avatar}
                />
              </a>
            </Link>
            <span className={styles.card__profile_name}>
              {user?.firstName || defaultProfile.name}
            </span>
          </div>
          <div className={styles.card__title}>{title}</div>
          <div className={styles.card__description}>
            {content}
            {showText && <span>{showText}</span>}
          </div>
          <div className={styles.card__buttons}>
            {(stage === STATES.ACTIVE || stage === STATES.REVIEW) && (
              <>
                <Agreements />
                <Invitate />
              </>
            )}
            {stage === STATES.CLOSED && (
              <>
                <FinishedStats />
                <FinishedInvitate />
              </>
            )}
          </div>
        </div>
      </Card>
      <div onClick={(e) => e.stopPropagation()}>
        <Share
          visible={shareModal}
          href={modalURL}
          onCancel={() => {
            setShareModal(false);
          }}
          closable={false}
          cancelText="Cancelar"
          userId={userId}
          proposalId={id}
        />
        <Modal
          visible={reactionModal}
          closable={false}
          onCancel={() => {
            setReactionModal(false);
          }}
          cancelText="Cancelar"
          okText={okText}
          support={supportModal}
          onOk={() => {
            setReactionModal(false);
            supportModal
              ? onOk({ id: id, reaction: "like", userId: userId })
              : onOk({ id: id, reaction: "dislike", userId: userId });
          }}
        />
      </div>
    </>
  );
}
export default ProposalCard;
