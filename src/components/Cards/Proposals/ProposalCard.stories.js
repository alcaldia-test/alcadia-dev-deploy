import { withDesign } from "storybook-addon-designs";
import ProposalsCardComment from "./ProposalCardComment.js";
import ProposalsCard, { PROPOSAL_CONSTANTS } from "./ProposalCard.js";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idProposalsCard = {
  default: "16%3A10214",
};

export default {
  title: "Components/Cards/ProposalsCard",
  component: ProposalsCard,
  decorators: [withDesign],
  argTypes: {},
};

const image = (
  <img src="https://s3-alpha-sig.figma.com/img/734b/5874/49c9007d6c0feb4f36fd2448e8d11bc0?Expires=1639958400&Signature=DS402lnBaqLaKFvmOOCkRsE7Y4JDR6SgWoJoYJJchvFhI9NWyyE3~FCS9jxXbUjYtRW0RlkuYXd-eWpRLTr4o-7CEoPuBv-uPSw8ojd9T6u0Lyd5Z0rbx0BlOoRJ-aTmE1Uy6GGYGRPk7XWHb~zUpK7Ov8EXYx3SKJMIzZev791OcEw00pqqaVtc6HDLytv7gsEHSfADSmmLrBQu4RSvyJ3D0snZkpTjDGjSvVHxAsbwCs1VaOm1Rq0j1f-kq3HTM-uh8wFfAbY9pzLPpFJpprCP9PN70ZpH~EV3U8sFR793V-KhjmjXl~laNaLBglt024dKAPerXDI82yND407oPg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"></img>
);

const Template = (args) => <ProposalsCard {...args} />;
const TemplateComment = (args) => <ProposalsCardComment {...args} />;

export const SinVotar = Template.bind({});

SinVotar.args = {
  width: "382rem",
  height: "536rem",
  image: image,
  profile: {
    image:
      "https://s3-alpha-sig.figma.com/img/532a/82c6/ead2b66cdf154f171f47293d8beca457?Expires=1639958400&Signature=QnR8Xa908YhoarMhBsPurZ2Rz3mWtFiC-gke2kBruTBB4l0Tb7Q01I0U3CBABxG-nOOgmHO5k0cDLjlXD3FoCYPs7Ab0hV7dTWShSh96f8IQ1FMMW2WAkk5p-JaF7Zzpz~D6WtVaLFNT4b87md4zcEu2qJuMALAexXaxgBgLSG0J9-1FK9Hcf7A0KqfCT5Xavg6u14UMw7HD2I40qk4YvnoxtpX0KGbOc~y3e-3Yjjbd0Dk4GENRJDhv71IVI0fzH-HzUHB2miU5GwS2O9C406HLtQ1SNQJVmiHmuE8CefEpGKOANhorx2-slwLdtcAKN3VCGBQrFcD4kJSybfc~cQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
    name: "Matilda Cepedez",
  },
  title: "Necesidad de basureros en la zona de Zopocachi",
  description: {
    text: "La zona de Obrajes necesita dos basureros en la esquina de libertador y calama, toman por pasar con vehículos por la zona para llevar cargamento.",
    link: "#",
  },
  status: PROPOSAL_CONSTANTS.NOT_VOTED,
  onClickAgree: () => console.log("Estoy de acuerdo papi"),
  onClickDisagree: () => console.log("No estoy de acuerdo papi"),
  onClickShare: () => console.log("Vamos a invitar a unos panas"),
  onClickLogin: () => console.log("A iniciar sesion rey"),
};

SinVotar.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idProposalsCard.default}`,
  },
};

export const VotoEnApoyo = Template.bind({});

VotoEnApoyo.args = {
  image: image,
  profile: {
    image:
      "https://s3-alpha-sig.figma.com/img/532a/82c6/ead2b66cdf154f171f47293d8beca457?Expires=1639958400&Signature=QnR8Xa908YhoarMhBsPurZ2Rz3mWtFiC-gke2kBruTBB4l0Tb7Q01I0U3CBABxG-nOOgmHO5k0cDLjlXD3FoCYPs7Ab0hV7dTWShSh96f8IQ1FMMW2WAkk5p-JaF7Zzpz~D6WtVaLFNT4b87md4zcEu2qJuMALAexXaxgBgLSG0J9-1FK9Hcf7A0KqfCT5Xavg6u14UMw7HD2I40qk4YvnoxtpX0KGbOc~y3e-3Yjjbd0Dk4GENRJDhv71IVI0fzH-HzUHB2miU5GwS2O9C406HLtQ1SNQJVmiHmuE8CefEpGKOANhorx2-slwLdtcAKN3VCGBQrFcD4kJSybfc~cQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
    name: "Matilda Cepedez",
  },
  title: "Necesidad de basureros en la zona de Zopocachi",
  description: {
    text: "La zona de Obrajes necesita dos basureros en la esquina de libertador y calama, toman por pasar con vehículos por la zona para llevar cargamento.",
    link: "#",
  },
  status: PROPOSAL_CONSTANTS.AGREE_VOTED,
  onClickAgree: () => console.log("Estoy de acuerdo papi"),
  onClickDisagree: () => console.log("No estoy de acuerdo papi"),
  onClickShare: () => console.log("Vamos a invitar a unos panas"),
  onClickLogin: () => console.log("A iniciar sesion rey"),
};

VotoEnApoyo.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idProposalsCard.default}`,
  },
};

export const VotoEnDesacuerdo = Template.bind({});

VotoEnDesacuerdo.args = {
  image: image,
  profile: {
    image:
      "https://s3-alpha-sig.figma.com/img/532a/82c6/ead2b66cdf154f171f47293d8beca457?Expires=1639958400&Signature=QnR8Xa908YhoarMhBsPurZ2Rz3mWtFiC-gke2kBruTBB4l0Tb7Q01I0U3CBABxG-nOOgmHO5k0cDLjlXD3FoCYPs7Ab0hV7dTWShSh96f8IQ1FMMW2WAkk5p-JaF7Zzpz~D6WtVaLFNT4b87md4zcEu2qJuMALAexXaxgBgLSG0J9-1FK9Hcf7A0KqfCT5Xavg6u14UMw7HD2I40qk4YvnoxtpX0KGbOc~y3e-3Yjjbd0Dk4GENRJDhv71IVI0fzH-HzUHB2miU5GwS2O9C406HLtQ1SNQJVmiHmuE8CefEpGKOANhorx2-slwLdtcAKN3VCGBQrFcD4kJSybfc~cQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
    name: "Matilda Cepedez",
  },
  title: "Necesidad de basureros en la zona de Zopocachi",
  description: {
    text: "La zona de Obrajes necesita dos basureros en la esquina de libertador y calama, toman por pasar con vehículos por la zona para llevar cargamento.",
    link: "#",
  },
  status: PROPOSAL_CONSTANTS.DISAGREE_VOTED,
  onClickAgree: () => console.log("Estoy de acuerdo papi"),
  onClickDisagree: () => console.log("No estoy de acuerdo papi"),
  onClickShare: () => console.log("Vamos a invitar a unos panas"),
  onClickLogin: () => console.log("A iniciar sesion rey"),
};

VotoEnDesacuerdo.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idProposalsCard.default}`,
  },
};

export const SinRegistro = Template.bind({});

SinRegistro.args = {
  image: image,
  profile: {
    image:
      "https://s3-alpha-sig.figma.com/img/532a/82c6/ead2b66cdf154f171f47293d8beca457?Expires=1639958400&Signature=QnR8Xa908YhoarMhBsPurZ2Rz3mWtFiC-gke2kBruTBB4l0Tb7Q01I0U3CBABxG-nOOgmHO5k0cDLjlXD3FoCYPs7Ab0hV7dTWShSh96f8IQ1FMMW2WAkk5p-JaF7Zzpz~D6WtVaLFNT4b87md4zcEu2qJuMALAexXaxgBgLSG0J9-1FK9Hcf7A0KqfCT5Xavg6u14UMw7HD2I40qk4YvnoxtpX0KGbOc~y3e-3Yjjbd0Dk4GENRJDhv71IVI0fzH-HzUHB2miU5GwS2O9C406HLtQ1SNQJVmiHmuE8CefEpGKOANhorx2-slwLdtcAKN3VCGBQrFcD4kJSybfc~cQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
    name: "Matilda Cepedez",
  },
  title: "Necesidad de basureros en la zona de Zopocachi",
  description: {
    text: "La zona de Obrajes necesita dos basureros en la esquina de libertador y calama, toman por pasar con vehículos por la zona para llevar cargamento.",
    link: "#",
  },
  status: PROPOSAL_CONSTANTS.NOT_REGISTERED,
  onClickAgree: () => console.log("Estoy de acuerdo papi"),
  onClickDisagree: () => console.log("No estoy de acuerdo papi"),
  onClickShare: () => console.log("Vamos a invitar a unos panas"),
  onClickLogin: () => console.log("A iniciar sesion rey"),
};

SinRegistro.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idProposalsCard.default}`,
  },
};

export const DebateSinSesion = TemplateComment.bind({});

DebateSinSesion.args = {
  width: "400rem",
  height: "max-content",
  title: "Defensa de nuestra autonomía fiscal y universitaria.",
  description: {
    text: "Desde los últimos años, varios representantes de distintos territorios de Bolivia ningunear y menguar los recursos de Madrid, tanto ciudad como Comunidad Autónoma. Generalmente, los aspectos más sonantes versan sobre nuestra fiscalidad",
    link: "#",
  },
  isFinished: false,
  numberComments: 35,
  zones: ["all"],
  onClickLogin: () => console.log("A iniciar sesion"),
};

DebateSinSesion.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idProposalsCard.default}`,
  },
};

export const DebateFinalizado = TemplateComment.bind({});

DebateFinalizado.args = {
  width: "400rem",
  height: "max-content",
  title: "Defensa de nuestra autonomía fiscal y universitaria.",
  description: {
    text: "Desde los últimos años, varios representantes de distintos territorios de Bolivia ningunear y menguar los recursos de Madrid, tanto ciudad como Comunidad Autónoma. Generalmente, los aspectos más sonantes versan sobre nuestra fiscalidad",
    link: "#",
  },
  isFinished: true,
  numberComments: 35,
  zones: ["all"],
  onClickLogin: () => console.log("A iniciar sesion"),
};

DebateFinalizado.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idProposalsCard.default}`,
  },
};
