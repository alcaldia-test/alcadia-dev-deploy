import React from "react";
// Se importa decorador para anadir link de figma
import { withDesign } from "storybook-addon-designs";
// importamos componente al que le crearemos la story
import Card, { Grid, Meta } from "./Card";
import Avatar from "../Avatar";
import Button from "@components/Button";

const ExampleCover = (
  <img
    alt="cover example"
    src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
  />
);

// Datos necesarios para las stories
export default {
  title: "Components/Cards/Card Base",
  component: Card,
  // agregamos el decorador de figma
  decorators: [withDesign],
};

// Definimos un template para poder crear vistas por estados
const Template = (args) => {
  // Separar contenido de resto de argumentos
  const { children, ...rest } = args;
  return (
    <>
      <Card {...rest}>{children}</Card>
    </>
  );
};

/**
 * Empty card
 */

// Creamos la vista Empty
export const Empty = Template.bind({});

// Definimos los valores ejemplo segun el estado
Empty.args = {
  style: {
    width: "382px",
  },
};

// Link del embebido en figma para mostrar en la pestaña Design
Empty.parameters = {
  controls: { include: ["style"] },
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};

/**
 * Random content card
 */

// Creamos la vista Default
export const RandomContent = Template.bind({});

// Definimos los valores por defecto segun el estado
RandomContent.args = {
  children: (
    <>
      <Meta
        avatar={
          <Avatar size="small" src="https://joeschmoe.io/api/v1/random" />
        }
        title="José eduardo"
      />
      <h1>Content placeholder</h1>

      {[1, 2, 3].map((i) => (
        <p key={i}>Content placeholder {i}</p>
      ))}
      <Grid>Content</Grid>
      <Grid>Content</Grid>
      <Grid>Content</Grid>
      <Button size={"small"} type="secondary">
        Button placeholder
      </Button>
    </>
  ),
  hoverable: true,
  style: {
    width: "382px",
  },
  cover: ExampleCover,
  loading: false,
  size: "small",
};

// Link del embebido en figma para mostrar en la pestaña Design
RandomContent.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=748%3A7297",
  },
};
