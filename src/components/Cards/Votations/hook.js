import moment from "moment";

export default function useTextFormater(text) {
  if (text && text.length < 100) {
    return {
      descriptionText: text,
      linkText: "",
    };
  } else if (text && text.length > 100) {
    return {
      descriptionText: text.substring(0, 100),
      linkText: "...Leer mas",
    };
  }

  return {
    descriptionText: text,
    linkText: "",
  };
}

function useDateStatus(startDate, endDate) {
  moment.locale("es");
  const endDateFormat = moment(endDate)
    .format("DD MM YYYY")
    .split(" ")
    .join("/");
  const startDateFormat =
    moment(startDate).format("DD MM YYYY").split(" ").join("/") || "";

  const end = moment(endDate);
  const todayDate = moment();

  const ended = end.diff(todayDate, "days");

  if (!startDate && !endDate) {
    return {
      statusText: "",
      status: false,
    };
  }

  if (!startDate && ended < 0) {
    return {
      statusText: `La votación finaliza el ${endDateFormat}`,
      status: false,
    };
  }

  if (!startDate && endDate) {
    return {
      statusText: `La votación finaliza el ${endDateFormat}`,
      status: true,
    };
  }

  if (ended < 0) {
    return {
      statusText: `Desde el ${startDateFormat} hasta el ${endDateFormat}`,
      status: false,
    };
  }

  return {
    statusText: `Desde el ${startDateFormat} hasta el ${endDateFormat}`,
    status: true,
  };
}

function useJoinBySemiColumn(elArray) {
  if (!elArray || elArray.length === 0) {
    return {
      allZones: null,
      displayText: "Todas las zonas",
    };
  }

  if (elArray.length > 3) {
    return {
      allZones: elArray.join(" ,"),
      displayText: `${elArray.slice(0, 3).join(" ,")} ...`,
    };
  }

  if (elArray.length > 1) {
    return {
      displayText: elArray.join(", "),
      allZones: null,
    };
  }

  return {
    displayText: `${elArray[0]}`,
    allZones: null,
  };
}

export { useDateStatus, useJoinBySemiColumn };
