import React from "react";
import Image from "next/image";
import useTextFormater, { useDateStatus, useJoinBySemiColumn } from "./hook";
import { CheckCircleFilled } from "@ant-design/icons";
import { Tooltip } from "antd";
import Card from "../Card";
import "antd/lib/tooltip/style/index.css";

import styles from "./VotationCard.module.scss";

/**
 *
 * @param {starDate, dueDate} string son fechas con un formato dia/mes/año
 *
 * @param {content} string es el texto que se muestra en la tarjeta que de ser mas largo de lo establecido
 * se corta y se muestra un botton de leer mas
 *
 * @param {buttons} array es un arreglo de elementos de React que se mapean y se clonan
 * para que los stylos se muestren bien se debe mandar el componente Button
 *
 * @param {title} string corresponde al titulo de la tarjeta
 *
 * @param {zones} object trae dos propiedades zones que es un arreglo de lugares y zonesLink
 * zonesLink ques es un string con el link que se usa en el href
 * quedaria algo como este ejemplo
 * {
 *  zones: ["Achachicala", "Vino Tinto", "Villafatima"],
 *  zonesLink: "https://[algo].[algo]",
 * }
 * de no traer zonas el valor por defecto seria "varias zonas"
 *
 * @param {withStatus} boolean es un valor que define el estatus de la tarjeta
 * muestra en caso true "Votacion en proceso para:", caso false  "Votacion Finalizada para:"
 *
 * @param {voted} boolean muestra en caso true un icono de "voted"
 *
 * @returns React Component
 */

function VotationsCard({
  buttons,
  content,
  children,
  image,
  dueDate,
  onChange,
  startDate,
  title,
  voted,
  withStatus,
  zones,
  ...props
}) {
  const { statusText, status } = useDateStatus(startDate, dueDate);
  const { displayText, allZones } = useJoinBySemiColumn(zones && zones.zones);
  const showStatus = withStatus || false;
  const { descriptionText, linkText } = useTextFormater(content);

  return (
    <Card {...props} className={styles.card__votation}>
      {image && (
        <Image
          {...props}
          src={image}
          alt={title}
          className={styles.card__votation_image}
          layout="fill"
        />
      )}
      <span className={styles.card__title}>{title}</span>
      <div className={styles.description__container}>
        <p>
          {descriptionText}
          <a onClick={onChange}>{` ${linkText}`}</a>
        </p>
      </div>
      <div className={styles.card__items_container}>
        {voted ? (
          <span className={styles.card__voted_item}>
            votaste
            <CheckCircleFilled />
          </span>
        ) : (
          ""
        )}
        {!showStatus ? (
          ""
        ) : (
          <span
            className={`${styles.card__status_item} ${
              !status ? styles.card__ended_item : styles.card__process_item
            }`}
          >
            {!status
              ? "Votación Finalizada para:"
              : "Votación en proceso para:"}
          </span>
        )}
        {!allZones ? (
          <a className={styles.card__zones_link}>{displayText}</a>
        ) : (
          <Tooltip
            title={<span>{allZones}</span>}
            placement="topLeft"
            color="#53f3f3"
          >
            <a
              className={styles.card__zones_link}
              href={`${zones && zones.zonesLink}`}
            >
              {displayText}
            </a>
          </Tooltip>
        )}
        <span className={styles.card__votation_time}>{statusText}</span>
      </div>
      <div className={styles.card__buttons_containers}>{children}</div>
    </Card>
  );
}

export default VotationsCard;
