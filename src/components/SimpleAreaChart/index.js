import React from "react";
import { AreaChart, Area, Tooltip, ResponsiveContainer, XAxis } from "recharts";
import Card from "@components/Cards/Card";
import styles from "./SimpleAreaChart.module.scss";

const SimpleAreaChart = ({ title, data, yAxisKey, color1, color2, type }) => {
  return (
    <div className={styles.simple__area__chart}>
      <Card>
        <h6>{title}</h6>
        <ResponsiveContainer width="100%" height={150}>
          <AreaChart
            data={data}
            margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
          >
            <XAxis hide dataKey="name" />
            <defs>
              <linearGradient id="color" x1="0" y1="0" x2="1" y2="0">
                <stop offset="5%" stopColor={color1} stopOpacity={1} />
                <stop offset="95%" stopColor={color2} stopOpacity={1} />
              </linearGradient>
            </defs>
            <Tooltip />
            <Area
              type={type}
              dataKey={yAxisKey}
              strokeWidth={0}
              stroke={color1}
              fillOpacity={1}
              fill="url(#color)"
            />
          </AreaChart>
        </ResponsiveContainer>
      </Card>
    </div>
  );
};

export default SimpleAreaChart;
