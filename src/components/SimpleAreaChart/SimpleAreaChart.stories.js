import { withDesign } from "storybook-addon-designs";

import SimpleAreaChart from "./index.js";

export default {
  title: "Components/SimpleAreaChart",
  component: SimpleAreaChart,
  decorators: [withDesign],
  argTypes: {
    data: { control: "array" },
    xAxisKey: { control: "text" },
    yAxisKey: { control: "text" },
    color1: { control: "color" },
    color2: { control: "color" },
    xAxisLabel: { control: "text" },
    yAxisLabel: { control: "text" },
    type: {
      control: {
        type: "select",
        options: [
          "basis",
          "basisClosed",
          "basisOpen",
          "linear",
          "linearClosed",
          "natural",
          "monotoneX",
          "monotoneY",
          "monotone",
          "step",
          "stepBefore",
          "stepAfter",
        ],
      },
    },
  },
};

const Template = (args) => <SimpleAreaChart {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlSimpleAreaChart = {
  default: "748%3A7290",
};

export const Default = Template.bind({});
Default.args = {
  title: "Simple Area Chart",
  data: [
    { name: "Jan", visitors: 4000 },
    { name: "Feb", visitors: 3000 },
    { name: "Mar", visitors: 2000 },
    { name: "Apr", visitors: 2780 },
    { name: "May", visitors: 1890 },
    { name: "Jun", visitors: 2390 },
    { name: "Jul", visitors: 3490 },
  ],
  xAxisKey: "name",
  yAxisKey: "visitors",
  color1: "#1BC767",
  color2: "#65C1D5",
  xAxisLabel: "Month",
  yAxisLabel: "Visitors",
  type: "linear",
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlSimpleAreaChart.default}`,
  },
};
