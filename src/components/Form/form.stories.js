import React from "react";
import { withDesign } from "storybook-addon-designs";
import Form from "./Form";

import fileInfoLine from "../../../public/images/icon/file-info-line.svg";
import fileUserLine from "../../../public/images/icon/file-user-line.svg";
import mailLine from "../../../public/images/icon/mail-line.svg";
import userLine from "../../../public/images/icon/user-line.svg";
import lockLine from "../../../public/images/icon/lock-2-line.svg";

export default {
  title: "components/Form",
  component: Form,
  decorators: [withDesign],
};

const Template = (args) => <Form {...args} />;

export const Default = Template.bind({});
Default.args = {
  formData: (data) => console.log(data),
  buttonText: "Registrarme",
  elements: [
    {
      rightIcon: (
        <>
          <img src={fileInfoLine} alt="file" />
        </>
      ),
      type: "select",
      inputName: "tipoDeDocumento",
      label: "Tipo de documento",
      options: [
        { value: "1", label: "Cédula de Identidad", parent: "tipoDeDocumento" },
        { value: "2", label: "CI con complemento", parent: "tipoDeDocumento" },
        { value: "3", label: "Extranjero", parent: "tipoDeDocumento" },
        { value: "4", label: "Pasaporte", parent: "tipoDeDocumento" },
      ],
      defaultValue: {
        tipoDeDocumento: "Cédula de Identidad",
      },
    },
    {
      rightIcon: fileUserLine,
      type: "text",
      label: "Nro. de Documento",
      required: true,
      inputName: "NroDocumento",
      defaultValue: {
        NroDocumento: "",
      },
    },
    {
      rightIcon: mailLine,
      type: "text",
      label: "Tipo de documento",
      required: true,
      inputName: "documentName",
      defaultValue: {
        documentName: "",
      },
    },
    {
      rightIcon: userLine,
      type: "text",
      label: "Nombre completo",
      required: true,
      inputName: "fullName",
      defaultValue: {
        fullName: "",
      },
    },
    {
      rightIcon: lockLine,
      type: "password",
      label: "Contraseña",
      required: true,
      inputName: "password",
      defaultValue: {
        password: "",
      },
    },
    {
      type: "checkbox",
      children: (
        <>
          Acepto los <a>Términos y condiciones de servicio</a>
        </>
      ),
      inputName: "termsAndConditions",
      defaultValue: {
        no: false,
      },
    },
  ],
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=25%3A5154",
  },
};
