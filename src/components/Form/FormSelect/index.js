import React, { useState } from "react";
import { DownOutlined, UpOutlined } from "@ant-design/icons";

import styles from "./formselect.module.scss";

const FormSelect = ({ icon, options, onChange, error }) => {
  const [toggle, setToggle] = useState(false);
  const [select, setSelect] = useState(null);

  const handleSelect = (data) => {
    setToggle(false);
    setSelect(`${data.label}`);
    error = false;
    onChange(data);
  };

  return (
    <div
      className={
        error
          ? styles.selectContainerFocusError
          : select
          ? styles.selectContainerFocus
          : styles.selectContainer
      }
    >
      <div
        className={
          !toggle ? styles.iconSelectContainer : styles.iconContainerOpen
        }
      >
        {icon}
      </div>
      <div
        className={
          !toggle ? styles.selectInputContainer : styles.selectInputOpen
        }
        onClick={() => setToggle(!toggle)}
      >
        {select || "seleccionar"} {toggle ? <UpOutlined /> : <DownOutlined />}
      </div>
      <div className={!toggle ? styles.optionsHidden : styles.optionsContainer}>
        {options.map((op, i) => (
          <span
            className={styles.options}
            key={i}
            onClick={() => handleSelect(op)}
          >
            {op.label}
          </span>
        ))}
      </div>
    </div>
  );
};

export default FormSelect;
