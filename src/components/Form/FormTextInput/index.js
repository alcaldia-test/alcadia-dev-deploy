import React, { useState, useRef } from "react";

import styles from "./textinput.module.scss";

const FormTextInput = ({ type, label, icon, error, ...rest }) => {
  const [focused, isFocused] = useState(false);

  const inputELement = useRef(null);

  const inputClickHandler = () => {
    isFocused(true);
    inputELement.current.focus();
  };

  const focusEndHandler = () => {
    if (inputELement.current.value === "") {
      isFocused(false);
    }
  };

  return (
    <div
      className={
        error
          ? styles.inputContainerError
          : focused
          ? styles.inputContainerFocus
          : styles.inputContainer
      }
    >
      <div className={styles.iconContainer}>{icon}</div>
      <div className={styles.inputLabelContainer}>
        <span
          onClick={inputClickHandler}
          className={!focused ? styles.inputLabel : styles.inputLabelFocused}
        >
          {label}
        </span>
        <input
          ref={inputELement}
          onFocus={inputClickHandler}
          onBlur={focusEndHandler}
          className={styles.inputEl}
          type={type}
          {...rest}
          autoComplete="off"
        />
      </div>
    </div>
  );
};

export default FormTextInput;
