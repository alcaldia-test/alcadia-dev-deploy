import React, { useState } from "react";
import useForm from "./useFormHook";
import FormInput from "./FormTextInput";
import FormSelect from "./FormSelect";

import Checkbox from "@components/Checkboxes";

import styles from "./form.module.scss";

export function RegisterForm({ elements, buttonText, formData }) {
  const initialState = useForm(elements);
  const [data, setData] = useState(initialState);

  const handleInput = (e) => {
    if (e.parent) {
      console.log(e);
      setData({
        ...data,
        [e.parent]: e.label,
      });
      return 0;
    }

    if (e.target.type === "checkbox") {
      setData({
        ...data,
        [e.target.name]: e.target.checked,
      });
      return 0;
    }

    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (data) => {
    formData(data);
  };

  return (
    <>
      <div className={styles.formContainer}>
        <form
          className={styles.formElement}
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit(data);
          }}
        >
          {elements &&
            elements.map((el) => {
              if (el.type === "select") {
                console.log(el);
                return (
                  <FormSelect
                    icon={el.rightIcon}
                    onChange={handleInput}
                    options={el.options}
                  />
                );
              } else if (el.type === "checkbox") {
                return (
                  <div
                    className={styles.checkBoxContainer}
                    key={el.inputName}
                    onChange={handleInput}
                  >
                    <Checkbox
                      type="primary"
                      label={el.checkboxText}
                      name={el.inputName}
                    >
                      {el.children}
                    </Checkbox>
                  </div>
                );
              }
              return (
                <div key={el.inputName} className={styles.inputContainer}>
                  <FormInput
                    name={el.inputName}
                    type={el.type}
                    label={el.label}
                    maxLength={20}
                    icon={<img src={el.rightIcon} />}
                    onChange={handleInput}
                    required={el.required}
                  />
                </div>
              );
            })}
          <button className={styles.formButton}>{buttonText}</button>
        </form>
      </div>
    </>
  );
}
export default RegisterForm;
