export default function useEmptyFormState(el) {
  let initialState = {};
  el.forEach((el) => {
    initialState = { ...initialState, ...el.defaultValue };
    return initialState;
  });
  return initialState;
}
