import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Link from "next/link";
import "antd/lib/modal/style/index.css";
import { Modal as AntdModal } from "antd";
import ReactPlayer from "react-player";

import Checkboxes from "@components/Checkboxes";

import styles from "./PopUp.module.scss";

/**
 * @typedef Modal
 * @param {string} [params.title=""] - Title of the modal
 * @param {children} [params.children=""] - Content of the modal
 * @param {boolean} [params.visible=false] - True if the modal is visible
 * @param {function} [params.onCancel=() => {}] - Function to be called when the close button is clicked
 */
function Modal({ title, children, visible, onCancel, id, ...props }) {
  const { tutorials } = useSelector((storage) => storage.global);
  const [tutorialLink, setTutorialLink] = useState("");
  // const [duration, setDuration] = useState(0);

  useEffect(() => {
    if (tutorials.length > 0) {
      const tutorial = tutorials.find(({ key }) => key === "proposal-tutorial");
      if (tutorial) setTutorialLink(tutorial.content);
    }
  }, [tutorials]);

  const handleCheckboxChange = (e) => {
    if (e.target.checked) {
      localStorage.setItem("showPopUpProposal", false);
    } else {
      localStorage.setItem("showPopUpProposal", true);
    }
  };

  const handleSetDuration = (e) => {
    // setDuration(`${e / 60} min`);
  };

  return (
    <AntdModal
      {...props}
      title={title}
      visible={visible}
      className={styles.popup}
      onCancel={onCancel}
    >
      <p>
        Puedes ver el siguiente video en donde te enseñamos algunas indicaciones
      </p>
      {tutorialLink ? (
        <div className={styles.video__container}>
          <ReactPlayer
            url={tutorialLink}
            playing={visible}
            width="100%"
            height="100%"
            controls
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              borderRadius: "15px",
              overflow: "hidden",
            }}
            onDuration={handleSetDuration}
          />
        </div>
      ) : (
        <img
          src="/images/video.png"
          alt="video"
          className={styles.popup__image}
        />
      )}
      <div className={styles.popup__checkbox}>
        <Checkboxes
          type="primary"
          id="checkbox"
          onChange={handleCheckboxChange}
        />
        <label htmlFor="checkbox">No volver a mostrar</label>
      </div>
      <div className={styles.popup__buttons}>
        <button onClick={onCancel}>Cancelar</button>
        <Link
          href={`/propuestas_ciudadanas/[announcementId]/crear`}
          as={`/propuestas_ciudadanas/${id}/crear`}
        >
          <a>
            <button className={styles.popup__button_continue}>Continuar</button>
          </a>
        </Link>
      </div>
    </AntdModal>
  );
}
export default Modal;
