import React from "react";
import "antd/lib/carousel/style/index.css";
import styles from "./Carousel.module.scss";
import { Carousel as AntdCarousel } from "antd";
/**
 * @param width String that denotes width from all this component
 * @param height String that denotes height from all this component
 * @param items Array of objects for Render in this carousel
 * @param Props Default props of a Carousel.
 * @return A Carousel
 */
function Carousel({
  width = "100%",
  height = "auto",
  className = "",
  items,
  ...props
}) {
  const sizeStyles = { width: width, height: height };
  // Metodo que desustructura todas las propiedades del JSX Element y le ingresa el style para cambiarle la altura
  const getElementWithStyle = (item) => {
    return {
      ...item,
      props: { ...item.props, style: { ...item.props.style, ...sizeStyles } },
    };
  };

  return (
    <>
      <AntdCarousel
        {...props}
        className={`${styles.carousel} ${className}`}
        style={sizeStyles}
      >
        {items.map((item, index) => {
          item = typeof item !== "string" ? getElementWithStyle(item) : item;

          return (
            <div
              key={index}
              className={`${styles["carousel-item"]}`}
              // style={sizeStyles}
            >
              {item}
            </div>
          );
        })}
      </AntdCarousel>
    </>
  );
}
export default Carousel;
