import { CarouselCard } from "@components/Cards";
import React, { useEffect, useRef, useState } from "react";
import Carousel from "../Carousel";
import styles from "./AttainmentCarousel.module.scss";
import { moveToLeft, moveToRight } from "./hook.js";
import { ArrowLeftOutlined, ArrowRightOutlined } from "@ant-design/icons";
import Link from "next/link";
import Image from "next/image";
import { slicePhrase } from "@utils/capitalizeFirstLetter";
import { useRouter } from "next/router";
import Spin from "@components/Spin";

/**
 * Este carrousel tiene como objetivo mostrar una imagen principal
 * que sea la de cada Card que compone el Carrousel.
 * @param items Array de objetos con los siguientes tres atributos:
 *      * image - La imagen que se mostrara en al darse click
 *      * title - Titulo de la Card
 *      * description - Descripcion de la Card
 *      -    * Ejemplo:
 *          {
 *              image: "&lt;Image /&gt;",
 *              title: "&lt;Title /&gt;",
 *              description: "&lt;Description /&gt;"
 *          }
 * @param heightImage String that denotes the height of the image that show
 * @param heightCarousel String that denotes the height of the carousel
 * @param props Default props of a AttainmentCarousel.
 * @return A AttainmentCarousel
 */
function AttainmentCarousel({
  heightImage = "500rem",
  heightCarousel = "300rem",
  items,
  ...props
}) {
  const router = useRouter();
  const contentRef = useRef(null);
  const [firstImage, setFirstImage] = useState(items[0].image);
  const [firstTitle, setFirstTitle] = useState(items[0].title);
  const [firstId, setFirstId] = useState(items[0].id);
  const [loader, setLoader] = useState(false);
  const [showArrows, setShowArrows] = useState(false);

  const handleLoadImage = () => {
    setTimeout(() => {
      setLoader(false);
    }, 500);
  };

  const getContentCarousel = (item) => {
    return (
      <div className={`${styles["cardcarousel-container"]}`}>
        <div className={`${styles["cardcarousel-image"]}`}>
          <Image
            src={item?.image}
            width={160}
            height={90}
            layout="responsive"
          />
        </div>
        <div className={`${styles["cardcarousel-text"]}`}>
          <div className={`${styles["cardcarousel-title"]}`}>
            {slicePhrase(item?.title, 35)}
          </div>
          <div className={`${styles["cardcarousel-description"]}`}>
            {item?.description}
          </div>
        </div>
      </div>
    );
  };

  const getItems = (items) => [
    <div key={0} ref={contentRef} className={`${styles["cardcarousel-group"]}`}>
      {items.map((item, index) => {
        const { image, title, id } = item;
        return (
          <CarouselCard
            key={index}
            onClick={() => {
              setFirstImage(image);
              setFirstTitle(title);
              setFirstId(id);
              setLoader(true);
            }}
            onDoubleClick={() => router.push(`/logro/${id}`)}
          >
            {getContentCarousel(item)}
          </CarouselCard>
        );
      })}
    </div>,
  ];
  const carrouselParent = useRef(null);
  useEffect(() => {
    if (carrouselParent.current) {
      const width = carrouselParent.current.offsetWidth;
      if (width < 1000) {
        setShowArrows(true);
      } else if (items.length > 5) {
        setShowArrows(true);
      }
    }
  }, [carrouselParent]);
  return (
    <div
      className={`${styles.attainmentcarousel}`}
      {...props}
      ref={carrouselParent}
    >
      <div className={`${styles["carousel-image"]} `}>
        <Spin tip="Cargando..." size="small" spinning={loader}>
          <Link href={`/logro/${firstId} `}>
            <a>
              <div className={`${styles.banner} `}>
                <span>{slicePhrase(firstTitle, 75)}</span>
                <div className={`${styles.cover} `}></div>
                <img
                  onLoad={handleLoadImage}
                  src={firstImage}
                  alt={firstTitle}
                />
              </div>
            </a>
          </Link>
        </Spin>
      </div>
      {items.length > 1 && (
        <div className={`${styles["carousel-container"]}`}>
          {showArrows && (
            <ArrowLeftOutlined
              className={`${styles["carousel-arrow-left"]}`}
              onClick={(e) => moveToLeft(contentRef)}
            />
          )}
          <div className={`${styles["carousel-content"]}`}>
            <Carousel items={getItems(items)} height={heightCarousel} />
          </div>
          {showArrows && (
            <ArrowRightOutlined
              className={`${styles["carousel-arrow-right"]}`}
              onClick={(e) => moveToRight(contentRef)}
            />
          )}
        </div>
      )}
    </div>
  );
}
export default AttainmentCarousel;
