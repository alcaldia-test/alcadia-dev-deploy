const diffScroll = 200;

export const moveToLeft = (contentRef) => {
  const scrollMin = 0;
  contentRef.current.scrollTo({
    top: 0,
    left: Math.max(contentRef.current.scrollLeft - diffScroll, scrollMin),
    behavior: "smooth",
  });
};
export const moveToRight = (contentRef) => {
  const scrollMax = contentRef.current.clientWidth;
  contentRef.current.scrollTo({
    top: 0,
    left: Math.max(contentRef.current.scrollLeft + diffScroll, scrollMax),
    behavior: "smooth",
  });
};

// Funcion que agrupa los items por el numero de groupBy
export const getGroup = (items, groupBy) => {
  const getNext = (index) =>
    index + groupBy <= items.length ? index + groupBy : index.length;
  return items
    .map((_, index, array) =>
      index % groupBy === 0 ? array.slice(index, getNext(index)) : null
    )
    .filter((n) => n);
};

const getFirstElement = (array) => array && Array.isArray(array) && array[0];

export const getFirstImage = (items) => {
  console.log("khkjhlkj", items);
  return getFirstElement(items)?.image;
};
