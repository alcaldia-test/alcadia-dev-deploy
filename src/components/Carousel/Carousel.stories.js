import { withDesign } from "storybook-addon-designs";
import Carousel from "./Carousel.js";
import Button from "@components/Button";
import AttainmentCarousel from "./AttainmentCarousel/index.js";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idCarousel = {
  header: "27%3A5375",
  logros: "27%3A5308",
};

export default {
  title: "Components/Carousel",
  component: Carousel,
  decorators: [withDesign],
  argTypes: {},
};

const Template = (args) => <Carousel {...args} />;

export const Default = Template.bind({});

Default.args = {
  items: [
    <img
      width="300rem"
      height="300rem"
      style={{ background: "lightblue" }}
      key="Imagen 1"
    />,
    <img
      width="350rem"
      height="200rem"
      style={{ background: "pink" }}
      key="Imagen 2"
    />,
    <img
      width="200rem"
      height="400rem"
      style={{ background: "lightcoral" }}
      key="Imagen 3"
    />,
  ],
};

export const HeaderCarouel = Template.bind({});

const urlImageHeader =
  "https://s3-alpha-sig.figma.com/img/44fd/660a/2ba31af6d1702c421268de92f5fa0070?Expires=1639958400&Signature=JGgAdY0ydHCdmMhwkuT2yQTxYCFqd6b0bMQXPxPjS11ewdBPXO3OCeheS~c2xY-kZiuB7ajiiam9fICZ1dMLZSpejvJiqGJzIjBpvb~zw019Y0QORekk23rzPQkaGjM6h1stacK8E6JQbvqfu3QgomapDVpt20su1CIsKgUES6dwfhVmEof7J1lJcVVErx48gNldzA4-QhNrPYgsihCioZ1ChgiVnmm31hDbYxP-WFEZ56jUpEqoY8cTwVFusDgyzutrHcye0U9k-KI3xIeubmurTNenp-bC0rB5KnMHsZk1uMOCiAqmvW-0STAZ57L52f37vIHsW7HMDoqxiFGRqw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA";

const headerCarousel = (
  <div style={{ position: "relative" }}>
    <img
      style={{
        position: "absolute",
        zIndex: -1,
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        objectFit: "cover",
        background: `linear-gradient(0deg, rgba(39, 39, 39, 0.55), rgba(39, 39, 39, 0.55)), url('${urlImageHeader}') no-repeat center center / cover`,
      }}
    />
    <div
      style={{
        position: "absolute",
        zIndex: 1,
        top: "50%",
        transform: "translateY(-50%)",
        left: 0,
        width: "100%",
        height: "100%",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          height: "100%",
        }}
      >
        <p
          style={{
            fontFamily: "'Muller', sans-serif",
            fontSize: "16rem",
            fontWeight: "bold",
            textAlign: "center",
            color: "white",
          }}
        >
          En nuestra plataforma te invitamos a participar en la toma de
          decisiones municipales
        </p>
        <Button size="medium" type="primary">
          Leer más
        </Button>
      </div>
    </div>
  </div>
);

HeaderCarouel.args = {
  items: [headerCarousel, headerCarousel, headerCarousel],
};

HeaderCarouel.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idCarousel.header}`,
  },
};

const TemplateLogro = (args) => <AttainmentCarousel {...args} />;

const item1Carousel = {
  image: (
    <div
      style={{
        width: "100%",
        height: "100%",
        background:
          "linear-gradient(180.1deg, lightblue 0.09%, #1E1E1E 110.88%)",
      }}
    ></div>
  ),
  title: "Categoria o zona",
  description: "Este es un texto de pruebas",
};
const item2Carousel = {
  image: (
    <div
      style={{
        width: "100%",
        height: "100%",
        background: "linear-gradient(180.1deg, coral 0.09%, #1E1E1E 110.88%)",
      }}
    ></div>
  ),
  title: "Categoria o zona",
  description: "Este es un texto de pruebas",
};
const item3Carousel = {
  image: (
    <div
      style={{
        width: "100%",
        height: "100%",
        background: "linear-gradient(180.1deg, pink 0.09%, #1E1E1E 110.88%)",
      }}
    ></div>
  ),
  title: "Categoria o zona",
  description: "Este es un texto de pruebas",
};

export const LogrosCarousel = TemplateLogro.bind({});

LogrosCarousel.args = {
  items: [
    item1Carousel,
    item2Carousel,
    item3Carousel,
    item1Carousel,
    item2Carousel,
    item3Carousel,
  ],
  heightImage: "300rem",
  heightCarousel: "300rem",
};

LogrosCarousel.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idCarousel.logros}`,
  },
};
