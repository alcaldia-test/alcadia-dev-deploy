import React from "react";
import Image from "next/image";
import Styles from "./loader.module.scss";

const index = ({ title = "" }) => {
  return (
    <>
      <div className={Styles.background} />
      <div className={Styles.container}>
        <Image
          src="/gifts/Spin-1s-200px.gif"
          alt="loading"
          width={60}
          height={60}
          layout="fixed"
        />
        <div>{title}</div>
      </div>
    </>
  );
};

export default index;
