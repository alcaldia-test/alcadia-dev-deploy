import React, { useState, useMemo } from "react";
import ReactMap, { Marker, Source, Layer } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { heatmapLayer } from "./heatMapLayer";
import { pointsLayer } from "./pointsLayer";
import useGeoJsonFormat from "./hook";

import styles from "./heatMap.module.scss";

/**
 * @param {point} array puntos que recibe el heat map para crear los puntos de calor
 * se debe enviar una array de objetos con tres propiedades lat, long y votes
 * @param {viewport} object recibe un width, un height, y latiude y longitude para centrar el mapa
 * tambien recibe un zoom que se recomienda setear inicialmente entre 12 - 15
 */

const HeatMap = ({ points, defaultViewPort }) => {
  const [viewport, setViewPort] = useState({
    ...defaultViewPort,
  });

  const data = useMemo(() => useGeoJsonFormat(points), [points]);

  return (
    <div className={styles.heatMapContainer}>
      <ReactMap
        {...viewport}
        mapboxApiAccessToken="pk.eyJ1Ijoid2lsbGlhbWpwYXJyYSIsImEiOiJja2VrZnlpNXIwMWw2MnVqemN3b3ZtZ3B1In0.dCw2xZ16woazV15C0wlXEQ"
        onViewportChange={(e) => {
          setViewPort(e);
        }}
        mapStyle="mapbox://styles/mapbox/dark-v9"
      >
        {points && (
          <>
            <Source type="geojson" data={data}>
              <Layer {...heatmapLayer} />
              <Layer {...pointsLayer} />
            </Source>
            {viewport.zoom > 1
              ? points.map((marker, i) => (
                  <Marker
                    offsetLeft={-5}
                    offsetTop={-25}
                    key={i}
                    longitude={marker.long}
                    latitude={marker.lat}
                  >
                    <InfoText text={`${marker.name}: ${marker.votes}`} />
                  </Marker>
                ))
              : ""}
          </>
        )}
      </ReactMap>
    </div>
  );
};

const InfoText = ({ text }) => {
  const [showInfo, setShowInfo] = useState(false);

  return (
    <div
      className={styles.infoText}
      onMouseOver={() => setShowInfo(true)}
      onMouseLeave={() => setShowInfo(false)}
    >
      {showInfo === false ? "" : text}
    </div>
  );
};

export default HeatMap;
