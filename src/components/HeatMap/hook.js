export default function useGeoJsonFormat(points) {
  const dataFotmated = {
    type: "FeatureCollection",
    features: points.map((point) => {
      return {
        type: "Feature",
        properties: {
          id: Math.random().toString,
          value: point.votes,
        },
        geometry: {
          type: "Point",
          coordinates: [point.long, point.lat],
        },
      };
    }),
  };

  return dataFotmated;
}
