import React from "react";
import { withDesign } from "storybook-addon-designs";

import HeatMap from "./index";

export default {
  title: "Components/HeatMap",
  component: HeatMap,
  decorators: [withDesign],
};

const Template = (args) => <HeatMap {...args} />;

export const Default = Template.bind({});

/**
 * latitude y longitude son dos valores que se setean para que los puntos de calor aparezcan en el centro del mapa
 */

Default.args = {
  defaultViewPort: {
    width: "100%",
    height: "100%",
    latitude: -16.4897,
    longitude: -68.1192,
    zoom: 13,
  },
  points: [
    {
      lat: -16.4896,
      long: -68.1038,
      votes: 25,
    },
    {
      lat: -16.5896,
      long: -68.1338,
      votes: 173,
    },
    {
      lat: -16.4856,
      long: -68.5382,
      votes: 10,
    },
    {
      lat: -16.487014,
      long: -68.118736,
      votes: 147,
    },
    {
      lat: -16.489483,
      long: -68.12483,
      votes: 33,
    },
    {
      lat: -16.489483,
      long: -68.13483,
      votes: 36,
    },
    {
      lat: -16.489383,
      long: -68.13425,
      votes: 20,
    },
    {
      lat: -16.67383,
      long: -68.33425,
      votes: 76,
    },
    {
      lat: -16.579383,
      long: -68.32425,
      votes: 26,
    },
    {
      lat: -16.589383,
      long: -68.33445,
      votes: 3,
    },
    {
      lat: -16.789383,
      long: -68.323425,
      votes: 825,
    },
    {
      lat: -16.489383,
      long: -68.332425,
      votes: 86,
    },
  ],
};
