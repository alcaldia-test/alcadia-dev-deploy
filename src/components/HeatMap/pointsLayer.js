const minZoom = 19;

export const pointsLayer = {
  id: "trees-point",
  type: "circle",
  source: "trees",
  minzoom: minZoom,
  paint: {
    // increase the radius of the circle as the zoom level and dbh value increases
    "circle-radius": 16,
    "circle-color": {
      property: "dbh",
      type: "exponential",
      stops: [
        [0, "rgba(236,222,239,0)"],
        [10, "rgb(236,222,239)"],
        [20, "rgb(208,209,230)"],
        [30, "rgb(166,189,219)"],
        [40, "rgb(103,169,207)"],
        [50, "rgb(28,144,153)"],
        [60, "rgb(1,108,89)"],
      ],
    },
    "circle-stroke-color": "white",
    "circle-stroke-width": 1,
    "circle-opacity": {
      stops: [
        [14, 0],
        [15, 1],
      ],
    },
  },
};
