import React from "react";
import SeguredPage from "./hoc/securedPage";

export function Wrapper({ children, isAuth = true }) {
  if (!isAuth) {
    return <>{children}</>;
  }
  return <SeguredPage>{children}</SeguredPage>;
}

export default Wrapper;
