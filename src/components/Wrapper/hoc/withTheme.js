import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "../config/theme";

// eslint-disable-next-line react/display-name
const WithTheme = (ComposedComponent) => (props) =>
  (
    <ThemeProvider theme={theme}>
      <ComposedComponent {...props} />
    </ThemeProvider>
  );
export default WithTheme;
