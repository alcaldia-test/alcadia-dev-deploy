import React from "react";
import Layout from "../../core/Layout";

// eslint-disable-next-line react/display-name
const WithLayout = (ComposedComponent) => (props) =>
  (
    <Layout>
      <ComposedComponent {...props} />
    </Layout>
  );
export default WithLayout;
