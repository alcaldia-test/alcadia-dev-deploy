// eslint-disable-next-line no-unused-vars
import React from "react";
import securedPage from "./index";

export default securedPage(() => ({ children }) => ({ children }));
