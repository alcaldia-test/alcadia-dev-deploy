import React from "react";
import { ConfigProvider } from "antd";

// eslint-disable-next-line react/display-name
const WithLang = (ComposedComponent) => (props) =>
  (
    <ConfigProvider>
      <ComposedComponent {...props} />
    </ConfigProvider>
  );
export default WithLang;
