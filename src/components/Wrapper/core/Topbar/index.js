import React from "react";
import { Layout, Popover } from "antd";
import Link from "next/link";
import UserProfile from "../Sidebar/UserProfile";
import { toggleCollapsedSideNav } from "@redux/settings/actions";
import AppNotification from "@components/AppNotification";

import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_MINI_SIDEBAR,
  TAB_SIZE,
} from "@redux/settings/constants";
import { useDispatch, useSelector } from "react-redux";

const { Header } = Layout;

const Topbar = () => {
  const { width, navCollapsed, navStyle } = useSelector(
    ({ settings }) => settings
  );
  const dispatch = useDispatch();

  return (
    <Header>
      {navStyle === NAV_STYLE_DRAWER ||
      ((navStyle === NAV_STYLE_FIXED || navStyle === NAV_STYLE_MINI_SIDEBAR) &&
        width < TAB_SIZE) ? (
        <div className="gx-linebar gx-mr-3">
          <i
            className="gx-icon-btn icon icon-menu"
            onClick={() => dispatch(toggleCollapsedSideNav(!navCollapsed))}
          />
        </div>
      ) : null}
      <Link href="/">
        <img
          alt=""
          className="gx-d-block gx-d-lg-none gx-pointer"
          src={"/images/core.png"}
          style={{ height: 16 }}
        />
      </Link>

      <ul className="gx-header-notifications gx-ml-auto">
        <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none"></li>
        <li className="gx-notify">
          <Popover
            overlayClassName="gx-popover-horizantal"
            placement="bottomRight"
            content={<AppNotification />}
            trigger="click"
          >
            <span className="gx-pointer gx-d-block">
              <i className="icon icon-notification" />
            </span>
          </Popover>
        </li>
        <li className="gx-user-nav">
          <UserProfile />
        </li>
      </ul>
    </Header>
  );
};

export default Topbar;
