import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import { Avatar, Popover } from "antd";
import { useRouter } from "next/router";
import Link from "next/link";

const UserProfile = () => {
  const router = useRouter();
  const { dataUser } = useSelector(({ auth }) => auth);
  const { name, lastName } = useMemo(() => dataUser, []);

  const signOut = async () => {
    router.replace("/logout");
  };

  const userMenuOptions = (
    <ul className="gx-user-popover">
      <li>
        <Link href="/perfil">Mi cuenta</Link>
      </li>
      <li onClick={signOut}>Salir </li>
    </ul>
  );
  return (
    <div className="gx-flex-row gx-align-items-center gx-avatar-row">
      <Popover
        placement="bottomRight"
        content={userMenuOptions}
        trigger="click"
      >
        <Avatar
          src={"/images/avatar-placeholder.png"}
          className="gx-size-40 gx-pointer gx-mr-3"
          alt=""
        />
        <span className="gx-avatar-name">
          {`${name} ${lastName}`}
          <i className="icon icon-chevron-down gx-fs-xxs gx-ml-2" />
        </span>
      </Popover>
    </div>
  );
};

export default UserProfile;
