import { Radio } from "antd";
import { useState, useEffect } from "react";
import "antd/lib/radio/style/index.css";
import styles from "./radio.module.scss";
const RadioGroup = ({ options, onChange, valueDefault, ...props }) => {
  const [value, setValue] = useState(valueDefault);

  const handleChange = (e) => {
    setValue(e.target.value);
    onChange(e);
  };

  useEffect(() => {
    setValue(valueDefault);
  }, [valueDefault]);

  return (
    <Radio.Group {...props} onChange={(e) => handleChange(e)} value={value}>
      {options.map((option) => (
        <Radio id={option.id} key={option.id} value={option.name}>
          <span className={styles.radio}>{option.name}</span>
        </Radio>
      ))}
    </Radio.Group>
  );
};

export default RadioGroup;
