import { useRef, useState } from "react";
import Link from "next/link";
import { Input } from "antd";
import "antd/lib/input/style/index.css";
import styles from "./CommentField.module.scss";
import Avatar from "@components/Avatar";
import { PaperClipOutlined, SendOutlined } from "@ant-design/icons";
import message from "@components/Message";

const CommentField = ({
  placeholder,
  name,
  title,
  id,
  disabled,
  src,
  onClick,
  avatarSize = "medium",
  handleUpload,
  value,
  onChange,
  userId,
  ...args
}) => {
  // const [value, setValue] = useState("");
  const [uploadData, setUploadData] = useState();
  const fileInput = useRef(null);

  const handleUploadData = () => {
    handleUpload(uploadData);
    clearInput();
  };

  const handleUploadFile = (ev) => {
    const file = ev.target.files[0];

    if (file) {
      const size = (file.size ?? 0) / 1024 / 1024;

      const allowedTypes = [
        "image/png",
        "image/jpg",
        "image/jpeg",
        "application/pdf",
      ];

      if (size > 3) return message.error("Tamaño máximo permitido en 3Mb");
      if (!allowedTypes.includes(file.type))
        return message.error(
          "Sólo está permitido archivo tipo PDF, PNG, JPG y JPEG"
        );
    }

    setUploadData(file);
  };

  const clearInput = () => {
    const currentFile = fileInput.current;

    if (currentFile) currentFile.value = "";

    setUploadData();
  };

  return (
    <div className={styles.commentField} id={id}>
      <h5 className={styles.title}>{title}</h5>
      <hr />
      <div className={styles.content}>
        <div className={styles.avatarContainer}>
          <Link href="/usuario/[id]" as={`/usuario/${userId}`}>
            <a>
              <Avatar size={avatarSize} src={src} />
            </a>
          </Link>
        </div>
        <Input
          name={name}
          onChange={onChange}
          disabled={disabled}
          className={uploadData ? "with-file" : undefined}
          placeholder={placeholder}
          onPressEnter={handleUploadData}
          maxLength={1000}
          value={value}
          suffix={
            <>
              {uploadData && (
                <span
                  onClick={clearInput}
                  style={{ fontSize: 22, cursor: "pointer", paddingRight: 5 }}
                >
                  ×
                </span>
              )}
              <label
                className={styles.uploadFile}
                role={uploadData ? "figure" : "img"}
              >
                <PaperClipOutlined />
                <input
                  ref={fileInput}
                  multiple={false}
                  onChange={handleUploadFile}
                  type="file"
                />
              </label>
              <SendOutlined onClick={handleUploadData} />
            </>
          }
          {...args}
        />
      </div>
    </div>
  );
};

export default CommentField;
