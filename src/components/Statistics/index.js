import styles from "./Statistics.module.scss";
import HeatMap from "@components/HeatMap";
import SimpleAreaChart from "@components/SimpleAreaChart";
import CardStats from "@components/CardStats";
import Link from "next/link";

/**
 * Statistics Presentational Component
 */
const Statistics = ({ dataChart, dataSections, dataMap }) => {
  return (
    <>
      {dataMap && (
        <>
          <h2 className={styles.titles__staticstis}>{dataMap.title}</h2>
          <div className="mt-2">
            <HeatMap
              defaultViewPort={dataMap.data.defaultViewPort}
              points={dataMap.data.points}
            />
          </div>
        </>
      )}

      {dataChart && (
        <div className="w-3/6 mt-40 pt-20 mb-20 pb-30">
          <SimpleAreaChart
            title={dataChart.title}
            data={dataChart.data}
            xAxisKey="dia"
            yAxisKey={dataChart.yAxis}
            color1="#1BC767"
            color2="#65C1D5"
            type="linear"
          />
        </div>
      )}

      {dataSections?.map((section, index) => (
        <div key={"section_" + index} className={styles.section__container}>
          {section.title && (
            <h2 className={styles.titles__staticstis}>{section.title}</h2>
          )}
          {section.subtitle && (
            <h2 className={styles.stats__subtitle}>{section.subtitle}</h2>
          )}
          <div className={"flex flex-row flex-wrap " + styles.section__cards}>
            {section?.cards.map((card, i) =>
              card.subtitle ? (
                <div key={i} className={styles.stats__subtitle}>
                  <h2>{card?.subtitle}</h2>
                  <div
                    className={
                      "flex flex-row flex-wrap " + styles.section__cards
                    }
                  >
                    {card?.cards.map((card2) => (
                      <div
                        key={"card_" + card2.count}
                        className={styles.stats__container}
                      >
                        <CardStats
                          svg={card2.svg ? card2.svg : null}
                          icon={card2.icon ? card2.icon : null}
                          value={card2.count}
                          detail={card2.description}
                        />
                      </div>
                    ))}
                  </div>
                </div>
              ) : (
                <div key={i} className={styles.stats__container}>
                  <CardStats
                    svg={card.svg ? card.svg : null}
                    icon={card.icon ? card.icon : null}
                    value={card.count}
                    detail={card.description}
                  />
                </div>
              )
            )}
          </div>
          {section.url && (
            <Link href={section.url.url}>
              <a className={styles.url__section}>
                {section.url.title}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  width="18"
                  height="18"
                >
                  <path fill="none" d="M0 0h24v24H0z" />
                  <path d="M12 13H4v-2h8V4l8 8-8 8z" fill="rgba(0,179,186,1)" />
                </svg>
              </a>
            </Link>
          )}
        </div>
      ))}
    </>
  );
};

export default Statistics;
