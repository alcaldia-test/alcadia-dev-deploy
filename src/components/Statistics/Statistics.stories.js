import React from "react";
import { withDesign } from "storybook-addon-designs";
import Statistics from "./index";

export default {
  title: "Components/Statistics",
  component: Statistics,
  decorators: [withDesign],
};

const icon3 = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="32"
    height="32"
  >
    <path fill="none" d="M0 0h24v24H0z" />
    <path
      d="M14.6 8H21a2 2 0 0 1 2 2v2.104a2 2 0 0 1-.15.762l-3.095 7.515a1 1 0 0 1-.925.619H2a1 1 0 0 1-1-1V10a1 1 0 0 1 1-1h3.482a1 1 0 0 0 .817-.423L11.752.85a.5.5 0 0 1 .632-.159l1.814.907a2.5 2.5 0 0 1 1.305 2.853L14.6 8zM7 10.588V19h11.16L21 12.104V10h-6.4a2 2 0 0 1-1.938-2.493l.903-3.548a.5.5 0 0 0-.261-.571l-.661-.33-4.71 6.672c-.25.354-.57.644-.933.858zM5 11H3v8h2v-8z"
      fill="rgba(25,133,101,1)"
    />
  </svg>
);

const icon4 = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="32"
    height="32"
  >
    <path fill="none" d="M0 0h24v24H0z" />
    <path
      d="M9.4 16H3a2 2 0 0 1-2-2v-2.104a2 2 0 0 1 .15-.762L4.246 3.62A1 1 0 0 1 5.17 3H22a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1h-3.482a1 1 0 0 0-.817.423l-5.453 7.726a.5.5 0 0 1-.632.159L9.802 22.4a2.5 2.5 0 0 1-1.305-2.853L9.4 16zm7.6-2.588V5H5.84L3 11.896V14h6.4a2 2 0 0 1 1.938 2.493l-.903 3.548a.5.5 0 0 0 .261.571l.661.33 4.71-6.672c.25-.354.57-.644.933-.858zM19 13h2V5h-2v8z"
      fill="rgba(254,71,82,1)"
    />{" "}
  </svg>
);

const icon5 = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="32"
    height="32"
  >
    <path fill="none" d="M0 0h24v24H0z" />{" "}
    <path
      d="M6.455 19L2 22.5V4a1 1 0 0 1 1-1h18a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H6.455zm-.692-2H20V5H4v13.385L5.763 17zM11 10h2v2h-2v-2zm-4 0h2v2H7v-2zm8 0h2v2h-2v-2z"
      fill="rgba(133,133,133,1)"
    />
  </svg>
);

/**
 * dataMap => data del mapa
 * dataChart => data del chart
 * dataSections => data de las secciones
 *  Ejemplo de una seccion para el modulo convocatorias:
    {
      title: "Participaciones", // Titulo de la seccion
      cards: [ // Array de cards
        {
          count: "25%", // titulo del card num/string
          svg: icon3, //svg si es svg, icon si es imagen, null si no es a mostrar
          description: "Propuestas finalizadas" // descripcion
        },
        {
          count: "75%",
          svg: icon4,
          description: "Desacuerdos a propuestas"
        }
      ],
      //si requiere url abajo de la seccion
      url: {
        title: "Ir a propuestas finalizadas",
        url: "#"
      }
    },

    Ejemplo de una seccion para el modulo normativa 
    {
      title: "Comentarios normativa",
      cards: [
        {
          subtitle: "Introducción a la normativa",
          cards: [
            {
              count: "45",
              svg: icon5,
              description: "Comentarios"
            }
          ]
        },
        {
          subtitle: "Subtitle",
          cards: [
            {
              count: "45",
              svg: icon5,
              description: "Comentarios"
            },
            {
              count: "45",
              svg: icon5,
              description: "Comentarios"
            }
          ]
        }
      ]
    }
 */

const Template = (args) => <Statistics {...args} />;

export const Default = Template.bind({});
Default.args = {
  dataMap: {
    title: "Zonas donde hubo más participación",
    data: {
      defaultViewPort: {
        width: "100%",
        height: "100%",
        latitude: -16.4897,
        longitude: -68.1192,
        zoom: 13,
      },
      points: [
        {
          lat: -16.4896,
          long: -68.1038,
          votes: 25,
        },
        {
          lat: -16.5896,
          long: -68.1338,
          votes: 173,
        },
      ],
    },
  },
  dataChart: {
    title: "40 Propuestas creadas",
    data: [
      { name: "Jan", visitors: 4000 },
      { name: "Feb", visitors: 3000 },
    ],
  },
  dataSections: [
    {
      title: "Participaciones",
      cards: [
        {
          count: "25%",
          svg: icon3,
          description: "Propuestas finalizadas",
        },
        {
          count: "75%",
          svg: icon4,
          description: "Desacuerdos a propuestas",
        },
      ],
      url: {
        title: "Ir a propuestas finalizadas",
        url: "#",
      },
    },
    {
      title: "Comentarios normativa",
      cards: [
        {
          subtitle: "Introducción a la normativa",
          cards: [
            {
              count: "45",
              svg: icon5,
              description: "Comentarios",
            },
          ],
        },
        {
          subtitle: "Subtitle",
          cards: [
            {
              count: "45",
              svg: icon5,
              description: "Comentarios",
            },
            {
              count: "45",
              svg: icon5,
              description: "Comentarios",
            },
          ],
        },
      ],
    },
  ],
};

Default.parameters = {
  desing: {
    type: "figma",
    url: "https://www.figma.com/file/CgPBHjaHbUBF8imE32lMNe/Alcald%C3%ADa-%2F-5.-Dashboard-Product?node-id=6%3A37581",
  },
};
