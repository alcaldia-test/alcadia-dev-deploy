import React, { useState, useEffect, useRef } from "react";
import dynamic from "next/dynamic";

import { Card } from "antd";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertToRaw,
} from "draft-js";
import draftToHtml from "draftjs-to-html";

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import styles from "./WYSIWYG.module.scss";
const Editor = dynamic(
  () => import("react-draft-wysiwyg").then((mod) => mod.Editor),
  { ssr: false }
);

/**
 *
 * @param title Título del WYSIWYG
 * @param value Valor del WYSIWYG
 * @param onChange Función que se ejecuta al cambiar el valor del WYSIWYG
 * @param maxCharacter Número máximo de caracteres permitidos
 * @param minCharacter Número mínimo de caracteres permitidos
 *
 * @returns WYSIWYG
 */

export default function WYSIWYG({
  title,
  value,
  children,
  maxCharacter = 6000,
  minCharacter = 0,
  onChange,
  error,
  messageOfError,
  ...props
}) {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [err, setErr] = useState(error);

  const [textHasBennPasted, setTextHasBennPasted] = useState(false);

  const editorRef = useRef(null);

  const onEditorStateChange = (editorState) => {
    const _newValue = editorState.getCurrentContent().getPlainText().length;

    if (_newValue < minCharacter || _newValue > maxCharacter) {
      setErr(true);
    } else {
      setErr(false);
    }

    if (_newValue <= maxCharacter) {
      setEditorState(editorState);
    } else {
      const newEditorState = EditorState.createWithContent(
        ContentState.createFromBlockArray(convertFromHTML(draftToHtml(value)))
      );

      const updateSelection = {
        anchorOffset: maxCharacter,
        focusOffset: maxCharacter,
        isBackward: false,
      };

      const newEditorStateWithSelection = EditorState.acceptSelection(
        newEditorState,
        newEditorState.getSelection().merge(updateSelection)
      );

      setEditorState(newEditorStateWithSelection);
    }
  };

  const handleOnChange = (e) => {
    try {
      const _newValue = e.blocks[0].text.length;
      if (_newValue <= maxCharacter && onChange) {
        onChange(e);
      }
    } catch (error) {}
  };

  useEffect(() => {
    try {
      if (typeof value === "string") {
        setEditorState(
          EditorState.createWithContent(ContentState.createFromText(value))
        );
      } else {
        setEditorState(
          EditorState.createWithContent(
            ContentState.createFromBlockArray(
              convertFromHTML(draftToHtml(value))
            )
          )
        );
      }
    } catch (error) {
      setEditorState(EditorState.createEmpty());
    }
  }, []);

  useEffect(() => {
    if (textHasBennPasted) {
      const timer = setTimeout(() => {
        setTextHasBennPasted(false);
        const _newEditorState = EditorState.createWithContent(
          ContentState.createFromBlockArray(convertFromHTML(draftToHtml(value)))
        );
        setEditorState(_newEditorState);

        handleOnChange(convertToRaw(_newEditorState.getCurrentContent()));
      }, 100);
      return () => clearTimeout(timer);
    }
  }, [value]);

  const handlePaste = () => {
    setTextHasBennPasted(true);
  };

  return (
    <>
      {messageOfError && (
        <span className="text-red text-callout">{messageOfError}</span>
      )}
      <Card
        bordered={false}
        title={title}
        className={styles.card}
        onPaste={handlePaste}
      >
        <Editor
          {...props}
          editorState={editorState}
          editorRef={(ref) => (editorRef.current = ref)}
          // contentState={contentState}
          // onContentStateChange={onEditorStateChange}
          onEditorStateChange={onEditorStateChange}
          value={value}
          onChange={handleOnChange}
          toolbar={{
            options: [
              "inline",
              // "blockType",
              // "fontSize",
              // "list",
              // "textAlign",
              "history",
              "link",
              "fontFamily",
            ],
            inline: { inDropdown: true },
            list: { inDropdown: true },
            textAlign: { inDropdown: true },
            link: { inDropdown: true },
            history: { inDropdown: true },
            fontFamily: {
              options: ["Muller"],
              className: undefined,
              component: undefined,
              dropdownClassName: undefined,
            },
          }}
          className={styles.WYSIWYG}
          max
        />
        <div className={styles.counterArea}>
          <span className={err ? styles.error : null}>
            {editorState.getCurrentContent().getPlainText().length} /{" "}
            {maxCharacter}
          </span>
        </div>
        {children}
      </Card>
    </>
  );
}
