import { withDesign } from "storybook-addon-designs";

import WYSIWYG from "./index.js";

export default {
  title: "Components/WYSIWYG",
  component: WYSIWYG,
  decorators: [withDesign],
  argTypes: {
    onChange: { action: "onChange" },
  },
};

const Template = (args) => <WYSIWYG {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlWYSIWYG = {
  default: "748%3A7318",
};

export const Default = Template.bind({});
Default.args = {
  // Así se obtiene el valor del WYSIWYG
  onChange: (e) => console.log(e.blocks[0].text),
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlWYSIWYG.default}`,
  },
};
