import Head from "next/head";

/**
 *
 * @param {string} Title
 * @param {string} Description
 * @param {string} Image url
 * @param {string} Video url
 * @param {string} Children Other meta tags
 *
 * @returns {JSX.Element} SEO
 */

const SEO = ({
  title = "",
  description = "",
  image = "",
  video = "",
  children,
}) => {
  return (
    <Head>
      {title && (
        <>
          <title>{title}</title>
          <meta property="og:title" content={title} key="ogtitle" />
          <meta name="twitter:title" content={title} key="twittertitle" />
        </>
      )}
      {description && (
        <>
          <meta name="description" content={description} key="description" />
          <meta
            property="og:description"
            content={description}
            key="ogdescription"
          />
          <meta
            name="twitter:description"
            content={description}
            key="twitterdescription"
          />
        </>
      )}
      {image && (
        <>
          <meta
            name="twitter:card"
            content="summary_large_image"
            key="twittercard"
          />
          <meta name="image" content={image} key="image" />
          <meta property="og:image" content={image} key="ogimage" />
          <meta name="twitter:image" content={image} key="twitterimage" />
        </>
      )}
      {video && (
        <>
          <meta name="twitter:card" content="player" key="twittercard" />
          <meta name="twitter:player" content={video} key="twitterplayer" />
          <meta
            name="twitter:site"
            content="@criptoanalisis"
            key="twittersite"
          />
          <meta
            name="twitter:player:width"
            content="360"
            key="twitterplayerwidth"
          />
          <meta
            name="twitter:player:height"
            content="200"
            key="twitterplayerheight"
          />
          <meta
            name="twitter:image"
            content="https://participa.lapaz.bo/images/banner.jpg"
            key="twitterimage"
          />
        </>
      )}
      {children}
    </Head>
  );
};

export default SEO;
