import { useState } from "react";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";

import styles from "./Map.module.scss";

/**
 *
 * @param {latitud} latitud
 * @param {longitud} longitud
 * @param {zoom} zoom
 * @param {canChangeCoordinates} If the user can change the coordinates of the marker
 * @param {getCoordinates} Function to get the coordinates of the marker
 * @param {defaultMarker} If the map requires a marker for the ubication
 * @returns
 */

const MapContainer = ({
  latitud,
  longitud,
  zoom = 11,
  canChangeCoordinates = false,
  getCoordinates = () => {},
  defaultMarker = false,
}) => {
  const [lat, setLat] = useState(undefined);
  const [lng, setLng] = useState(undefined);

  const handleMapClick = (mapProps, map, clickEvent) => {
    setLat(clickEvent.latLng.lat());
    setLng(clickEvent.latLng.lng());
    getCoordinates(clickEvent.latLng.lat(), clickEvent.latLng.lng());
  };

  return (
    <div className={styles.map}>
      <Map
        google={window.google}
        zoom={zoom}
        initialCenter={{
          lat: latitud,
          lng: longitud,
        }}
        onClick={canChangeCoordinates ? handleMapClick : null}
      >
        {lat && lng && <Marker position={{ lat, lng }} />}
        {defaultMarker && <Marker position={{ lat: latitud, lng: longitud }} />}
      </Map>
    </div>
  );
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyBwa-KMwsm3mqNeV-ym2V-mU9NTHeSngXI",
})(MapContainer);
