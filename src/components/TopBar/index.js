import React from "react";
import Link from "next/link";
import Logo from "@components/Logo";
import Button from "@components/Button";
import { useDispatch, useSelector } from "react-redux";
import { removeCookies } from "@utils/cookiesMiddleware";
import { useRouter } from "next/router";
import { SIGNOUT_USER_SUCCESS } from "@redux/auth/constants";
import { routeRedirectRegister } from "@redux/auth/actions";

const defaultMenus = [
  {
    title: "Inicio",
    path: "/",
  },
  {
    title: "Tu voto cuenta",
    path: "/tu_voto_cuenta",
  },
  {
    title: "Propuestas ciudadanas",
    path: "/propuestas_ciudadanas",
  },
  {
    title: "Charlas",
    path: "/charlas",
  },
  {
    title: "Normativas",
    path: "/normativas",
  },
];

export function TopBar({ menus = { defaultMenus }, ...props }) {
  const { auth } = useSelector((store) => store);
  const router = useRouter();
  const dispatch = useDispatch();
  const setUrl = () => {
    dispatch(routeRedirectRegister(window?.location.pathname));
  };
  const geLogout = async () => {
    await removeCookies();
    router.replace("/");
    await dispatch({ type: SIGNOUT_USER_SUCCESS });
  };
  return (
    <nav className="bg-gray-100 border-b border-gray-700 " {...props}>
      <div className="max-w-6xl px-4 mx-auto">
        <div className="flex justify-center">
          <div className="flex space-x-4">
            {/* <!-- logo --> */}
            <div>
              <a
                href="#"
                className="flex items-center px-2 py-5 text-gray-700 hover:text-gray-900"
              >
                <Logo src="/icons/la-paz.svg" />
              </a>
            </div>

            {/* <!-- primary nav --> */}
            <div className="items-center hidden space-x-1 md:flex">
              {menus.map((item, index) => {
                return (
                  <Link key={index} href={item.path}>
                    <a className="px-3 py-5 text-gray-700 hover:text-gray-900">
                      {item.title}
                    </a>
                  </Link>
                );
              })}
            </div>
          </div>

          {/* <!-- secondary nav --> */}
          <div className="items-center hidden space-x-1 md:flex">
            <Link href={`/login?goto=${router.pathname}`}>
              <Button onClick={setUrl} type="primary">
                Iniciar sesión
              </Button>
            </Link>
            {auth.tokenUser ? (
              <Button onClick={geLogout} type="primary">
                Cerrar Sesion
              </Button>
            ) : (
              ""
            )}
          </div>

          {/* <!-- mobile button goes here --> */}
          <div className="flex items-center md:hidden">
            <button className="mobile-menu-button">
              <Logo src="/icons/menu-line.svg" />
            </button>
          </div>
        </div>
      </div>

      {/* <!-- mobile menu --> */}
      <div className="hidden mobile-menu md:hidden">
        {menus.map((item, index) => {
          return (
            <Link key={index} href={item.path}>
              <a className="px-3 py-5 text-gray-700 hover:text-gray-900">
                {item.title}
              </a>
            </Link>
          );
        })}
      </div>
    </nav>
  );
}

export default TopBar;
