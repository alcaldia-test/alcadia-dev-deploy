import React from "react";
import { DatePicker } from "antd";
import "antd/lib/date-picker/style/index.css";
import dateFormater from "./hook";
import moment from "moment";

/**
 * @size recibe un tamaño que puede ser default | large | small
 * @date es una variable que recibe un valor inicial para el date picker
 * de no recibirlo usara la fecha actual por defecto
 */

function DateComponent({ size, date, setDate = null, ...args }) {
  const componentSize = size || "default";
  const dateFormat = "DD/MM/YYYY";
  const dateMoment = moment(date);
  const defaultValue = date && dateMoment._isValid ? dateMoment : null;

  const handleDate = (e) => {
    date = dateFormater(e);
    if (setDate) setDate(date);
  };

  return (
    <DatePicker
      size={componentSize}
      onChange={handleDate}
      format={dateFormat}
      value={defaultValue}
      {...args}
    />
  );
}

export default DateComponent;
