import moment from "moment";

export default function dateFormater(date) {
  if (!date) return null;
  const dateFormat = "YYYY/MM/DD";
  const dateFormated = moment(date).format(dateFormat);
  return dateFormated;
}
