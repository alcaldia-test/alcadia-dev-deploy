import React from "react";
import { withDesign } from "storybook-addon-designs";
import DateComponent from "./index";

export default {
  title: "Components/DatePicker",
  component: DateComponent,
  decorators: [withDesign],
};

const Template = (args) => <DateComponent {...args} />;

export const Default = Template.bind({});
Default.args = {
  size: "default",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=700%3A6903",
  },
};

export const Large = Template.bind({});
Large.args = {
  size: "large",
  date: "12-12-2021",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=700%3A6903",
  },
};

export const Small = Template.bind({});
Small.args = {
  size: "small",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=700%3A6903",
  },
};
