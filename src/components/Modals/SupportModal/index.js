import React from "react";
import Modal from "../Modal";
import { Like, DisLike } from "@assets/icons";
import styles from "./SupportModal.module.scss";

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @returns A Share modal
 */

const SupportModal = ({
  visible,
  closable,
  href,
  onCancel,
  cancelText,
  okText,
  onOk,
  support,
  message,
  ...props
}) => {
  return (
    <Modal
      {...props}
      visible={visible}
      closable={closable}
      href={href}
      onCancel={onCancel}
      cancelText={cancelText}
      okText={okText}
      onOk={onOk}
      className={`${styles.modal} ${support ? styles.support : ""}`}
    >
      {support ? <Like /> : <DisLike />}
      {support ? (
        <p className={styles.modal__support}>
          {message?.yes || (
            <>
              ¿Quieres <span>Apoyar</span> esta propuesta?
            </>
          )}
        </p>
      ) : (
        <p>
          {message?.no || (
            <>
              ¿Seguro que <span>No apoyas</span> esta propuesta?
            </>
          )}
        </p>
      )}
    </Modal>
  );
};

export default SupportModal;
