import React from "react";
import "antd/lib/modal/style/index.css";
import { Modal as AntdModal } from "antd";

import { Close } from "@assets/icons";

import styles from "./SimpleModal.module.scss";
/**
 * @typedef Modal
 * @param {string} [params.title=""] - Title of the modal
 * @param {children} [params.children=""] - Content of the modal
 * @param {boolean} [params.visible=false] - True if the modal is visible
 * @param {function} [params.onCancel=() => {}] - Function to be called when the close button is clicked
 */
function Modal({ title, children, visible, onCancel, ...props }) {
  return (
    <AntdModal
      {...props}
      title={title}
      visible={visible}
      className={styles.modal}
      onCancel={onCancel}
    >
      <div className={styles.close} onClick={onCancel}>
        <Close />
      </div>
      {children}
    </AntdModal>
  );
}
export default Modal;
