import Modal from "./Modal";
import IconTextModal from "./IconTextModal/IconTextModal";
import TextAreaModal from "./TextAreaModal";

export default Modal;

export { IconTextModal, TextAreaModal };
