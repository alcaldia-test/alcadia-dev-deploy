import React from "react";
const ButtonContainer = ({ children }) => {
  return (
    <div className={`flex justify-between w-full mt-5 py-2`}>{children}</div>
  );
};

export default ButtonContainer;
