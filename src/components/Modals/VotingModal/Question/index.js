import React from "react";
import styles from "../voting.module.scss";
const Question = ({ children }) => {
  return (
    <div
      className={`flex-initial w-full text-center mt-2 py-1 ${styles.question}`}
    >
      {children}
    </div>
  );
};

export default Question;
