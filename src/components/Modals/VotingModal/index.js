import React, { useState } from "react";

import Card from "../Card/index";
import {
  DislikeOutlined,
  LikeOutlined,
  RightOutlined,
} from "@ant-design/icons";
import "antd/lib/modal/style/index.css";
import styles from "./voting.module.scss";
import Question from "./Question";
import ButtonContainer from "./ButtonContainer";
import Info from "../../Info";

const VotingModal = ({
  like = true,
  proposal = "El 31 de marzo la alcaldía anunciará las 5 propuestas con más votación",
  handleClick,
}) => {
  const [isModalVisible, setIsModalVisible] = useState(true);

  const handleClose = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      {isModalVisible && (
        <>
          <div
            className={`${styles.modalBackground}`}
            onClick={handleClose}
          ></div>
          <Card>
            <div className={`flex justify-center w-full`}>
              {like ? (
                <LikeOutlined className={`${styles.like}`} />
              ) : (
                <DislikeOutlined className={`${styles.dislike}`} />
              )}
            </div>

            <Info type="done">{proposal}</Info>
            <Question>
              ¿Quieres{" "}
              <span
                className={like ? `${styles.support}` : `${styles.unsupport}`}
              >
                Apoyar{" "}
              </span>
              esta propuesta?
            </Question>
            <ButtonContainer>
              <span className={`${styles.button}`} onClick={handleClose}>
                Cancelar
              </span>
              <span
                style={{ width: !like && "150px" }}
                className={`${styles.button_2}`}
                onClick={() => handleClick && handleClick()}
              >
                Sí, {!like && `No`} la apoyo
                <RightOutlined />
              </span>
            </ButtonContainer>
          </Card>
        </>
      )}
    </>
  );
};

export default VotingModal;
