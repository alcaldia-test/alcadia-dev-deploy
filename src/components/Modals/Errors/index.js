import Modal from "../Modal";

import styles from "./Errors.module.scss";

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @returns A Share modal
 */

const Errors = ({ visible, closable, onCancel, errors }) => {
  return (
    <Modal
      visible={visible}
      closable={closable}
      onCancel={onCancel}
      className={styles.modal}
      cancelText="Cerrar"
    >
      <img src="/icons/errorCircle.svg" alt="Error icon" />
      <p>Los siguientes campos son necesarios:</p>
      <ul>
        {Object.keys(errors).map((key) => {
          const text = key.includes("title")
            ? "Título"
            : key.includes("description")
            ? "Descripción"
            : key.includes("urlVideoOrImage")
            ? "Video o imagen"
            : key.includes("location")
            ? "Ubicación"
            : "";

          return (
            errors[key] && (
              <li key={key}>
                {text} {errors[key]}
              </li>
            )
          );
        })}
      </ul>
    </Modal>
  );
};

export default Errors;
