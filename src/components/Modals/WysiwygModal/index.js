import React, { useState } from "react";

import Modal from "@components/Modals/Modal";

import styles from "./WysiwygModal.module.scss";

import WYSIWYGCOMPONENET from "@components/WYSIWYG";

const WysiwygModal = ({ title, text, text2, id, placeholder, ...props }) => {
  // OnOk pass the ref textarea value
  const onOk = () => {
    const _c = content;
    setContent("");
    props.onOk(_c);
  };
  const okProps = { type: "primary", size: "small-x" };
  const cancelProps = { type: "secondary", size: "super-small" };

  const [content, setContent] = useState("");

  const handleChange = (content) => {
    if (content) {
      setContent(content);
    }
  };

  return (
    <Modal
      {...props}
      okButtonProps={okProps}
      cancelButtonProps={cancelProps}
      onOk={() => onOk()}
      className={styles.text__area__modal}
    >
      <h6 className={styles.text__area__modal__title}>{title}</h6>
      <div className={styles.text__area__container}>
        <label htmlFor={id} className={styles.text__area__modal__text}>
          {text}
        </label>
        <WYSIWYGCOMPONENET
          onChange={handleChange}
          row={7}
          value={content || {}}
          rawContentState={content || {}}
          minCharacter={300}
        />
        <label className={styles.text__area__modal__text__red}>{text2}</label>
      </div>
    </Modal>
  );
};

export default WysiwygModal;
