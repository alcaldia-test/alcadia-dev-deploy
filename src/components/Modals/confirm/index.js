import "antd/lib/modal/style/index.css";
import { Modal as AntdModal } from "antd";

const { confirm: antConfim } = AntdModal;

async function confirm(message = "", success, ok, no) {
  const result = await new Promise((resolve) => {
    antConfim({
      title: message,
      content: undefined,
      className: `custom-confirm-modal ${success ? "success-confirm" : ""}`,
      onOk() {
        resolve(true);
      },
      onCancel() {
        resolve(false);
      },
      okText: ok ?? "Estoy Seguro(a)",
      cancelText: no ?? "Cancelar",
    });
  });

  return result;
}

export default confirm;
