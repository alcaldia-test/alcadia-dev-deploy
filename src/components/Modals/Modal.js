import React from "react";
import "antd/lib/modal/style/index.css";
import styles from "./Modal.module.scss";
import buttonStyles from "@components/Button/Button.module.scss";
import { Modal as AntdModal } from "antd";
/**
 * @param cancelText Text of Cancel Button if is empty dont display Cancel Button
 * @param okText Text of Ok Button if is empty dont display Ok button
 * @param cancelButtonProps Object to define properties for this button, has type [primary,secondary,link] and size[large,medium,small,small-x,super-small] of Button component
 * @param okButtonProps Object to define properties for this button, has type [primary,secondary,link] and size[large,medium,small,small-x,super-small] of Button component
 * @param props Rest props of a Modal in Ant Design.
 * @return A Modal
 */
function Modal({
  children,
  cancelText,
  okText,
  cancelButtonProps = {},
  okButtonProps = {},
  className,
  ...props
}) {
  const okClassName = `${buttonStyles[okButtonProps?.type]} ${
    buttonStyles[okButtonProps?.size]
  } ${styles["modal-button"]}`;
  const cancelClassName = `${buttonStyles[cancelButtonProps?.type]} ${
    buttonStyles[cancelButtonProps?.size]
  } ${styles["modal-button"]}`;

  const cancelProps = {
    style: { display: !cancelText ? "none" : "inherit" },
    className: cancelClassName,
  };
  const okProps = {
    style: { display: !okText ? "none" : "inherit" },
    className: okClassName,
  };
  // Con estas props me quito un boton si no tiene texto!!!! Sea el de cancelar o el ok y le agrego los estilos del boton

  return (
    <AntdModal
      cancelButtonProps={{
        ...cancelButtonProps,
        ...cancelProps,
        type: undefined,
        size: undefined,
      }}
      okButtonProps={{
        ...okButtonProps,
        ...okProps,
        type: undefined,
        size: undefined,
      }}
      className={`${styles.modal} ${className}`}
      cancelText={cancelText}
      okText={okText}
      {...props}
    >
      {children}
    </AntdModal>
  );
}
export default Modal;
