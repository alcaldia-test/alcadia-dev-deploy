import Modal from "../Modal";

import styles from "./ZonesModal.module.scss";

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @returns A Share modal
 */

const ZonesModal = ({
  visible,
  closable,
  onCancel,
  macros = [],
  zones = [],
}) => {
  return (
    <Modal
      visible={visible}
      closable={closable}
      onCancel={onCancel}
      className={styles.modal}
      cancelText="Cerrar"
    >
      {macros.length > 0 && (
        <>
          <h6>Macro distritos permitidos para esta votación</h6>
          <p>
            {macros
              .map((macro) => macro.name)
              .join(", ")
              .concat(macros.length === 0 ? "" : ".")}
          </p>
        </>
      )}

      {zones.length > 0 && (
        <>
          <h6>Zonas permitidas para esta votación</h6>
          <p>
            {zones
              .map((zone) => zone.name)
              .join(", ")
              .concat(zones.length === 0 ? "" : ".")}
          </p>
        </>
      )}

      {macros.length === 0 && zones.length === 0 && (
        <>
          <h6>Zonas permitidas para esta votación</h6>
          <p>Todas las zonas son permitidas</p>
        </>
      )}
    </Modal>
  );
};

export default ZonesModal;
