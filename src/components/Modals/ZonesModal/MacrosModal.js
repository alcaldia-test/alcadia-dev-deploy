import { useEffect, useState } from "react";
import Modal from "../Modal";
import { VoteServices } from "@services/vote";
import Spin from "@components/Spin";

import styles from "./ZonesModal.module.scss";

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @returns A Share modal
 */

const ZonesModal = ({ closable, onCancel, macros = [] }) => {
  const [loading, setLoading] = useState(false);
  const [macrosRelated, setMacrosRelated] = useState([]);

  useEffect(() => {
    getMacroZones();
  }, []);

  const getMacroZones = async () => {
    try {
      setLoading(true);
      const vote = new VoteServices();
      let macrosId;

      if (macros.length) macrosId = macros.map(({ id }) => id);

      if (macrosId) {
        const macrosRelated = await vote.getMacrosZones(macrosId);

        console.log(macrosRelated);
        setMacrosRelated(macrosRelated);
      }
    } catch (e) {
      console.log(e);
    }

    setLoading(false);
  };

  return (
    <Modal
      visible={true}
      closable={closable}
      onCancel={onCancel}
      className={styles.modal}
      cancelText="Cerrar"
    >
      <Spin spinning={loading} size="large">
        {!!macrosRelated.length && (
          <>
            <h5>Macrodistritos y zonas permitidas</h5>
            {macrosRelated.map((macro) => {
              macro.zones.sort((a, b) => a.name.localeCompare(b.name));

              return (
                <div key={macro.id}>
                  <h6>{macro.name}</h6>
                  <ul>
                    {macro.zones.map((zone) => (
                      <li key={zone.id}>{zone.name}</li>
                    ))}
                    <p aria-hidden="true"></p>
                    <p aria-hidden="true"></p>
                  </ul>
                </div>
              );
            })}
          </>
        )}

        {!macrosRelated.length && (
          <>
            <h5>Zonas permitidas para esta votación</h5>
            <p>Todas las zonas son permitidas</p>
          </>
        )}
      </Spin>
    </Modal>
  );
};

export default ZonesModal;
