import { withDesign } from "storybook-addon-designs";
import IconTextModal from "./IconTextModal";
import {
  ArrowRightOutlined,
  CheckCircleFilled,
  DislikeOutlined,
  LikeOutlined,
} from "@ant-design/icons";
import Info from "@components/Info/index.js";
const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idIconTextModal = {
  default: "1361%3A7722",
  noapoyar: "20%3A5905",
  apoyar: "20%3A5876",
  aprobada: "20%3A6222",
};

export default {
  title: "Components/Modals/IconTextModal",
  component: IconTextModal,
  decorators: [withDesign],
  argTypes: {},
};

const Template = (args) => <IconTextModal {...args} />;

export const Default = Template.bind({});

Default.args = {
  title: <span>Texto Central</span>,
  description: "Texto Descriptivo",
  visible: true,
  width: "380rem",
  closable: false,
  cancelText: <span>Cancelado</span>,
  okText: (
    <>
      <span>Continuar</span>
      <ArrowRightOutlined />
    </>
  ),
  cancelButtonProps: {
    size: "small",
    type: "link",
  },
  okButtonProps: {
    size: "small",
    type: "link",
  },
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idIconTextModal.default}`,
  },
};

export const NoApoyar = Template.bind({});

NoApoyar.args = {
  image: <DislikeOutlined style={{ color: "red" }} />,
  title: (
    <Info type="info">
      El 31 de marzo la alcaldía anunciará las 5 propuestas con más votación
    </Info>
  ),
  description: (
    <span>
      ¿Seguro que <strong style={{ color: "red" }}>No apoyas</strong> esta
      propuesta?
    </span>
  ),
  visible: true,
  width: "380rem",
  closable: false,
  cancelText: <span>Cancelar</span>,
  okText: (
    <>
      <span>No la apoyo</span>
      <ArrowRightOutlined />
    </>
  ),
  cancelButtonProps: {
    size: "small",
    type: "link",
  },
  okButtonProps: {
    size: "small",
    type: "link",
  },
};

NoApoyar.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idIconTextModal.noapoyar}`,
  },
};

export const Apoyar = Template.bind({});

Apoyar.args = {
  image: <LikeOutlined style={{ color: "green" }} />,
  title: (
    <Info type="info">
      El 31 de marzo la alcaldía anunciará las 5 propuestas con más votación
    </Info>
  ),
  description: (
    <span>
      ¿Quieres <strong style={{ color: "green" }}>Apoyar</strong> esta
      propuesta?
    </span>
  ),
  visible: true,
  width: "380rem",
  closable: false,
  cancelText: <span>Cancelar</span>,
  okText: (
    <>
      <span>Si, La apoyo</span>
      <ArrowRightOutlined />
    </>
  ),
  cancelButtonProps: {
    size: "small",
    type: "link",
  },
  okButtonProps: {
    size: "small",
    type: "link",
  },
};

Apoyar.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idIconTextModal.apoyar}`,
  },
};

export const Aprobado = Template.bind({});

Aprobado.args = {
  image: <CheckCircleFilled style={{ color: "green" }} />,
  title: <span style={{ color: "green" }}>Propuesta Aprobada</span>,
  description: "",
  visible: true,
  width: "380rem",
  closable: false,
  okText: "Cerrar",
  okButtonProps: {
    size: "small",
    type: "link",
  },
};

Aprobado.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idIconTextModal.aprobada}`,
  },
};
