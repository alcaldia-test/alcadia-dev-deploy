import React from "react";
import "antd/lib/modal/style/index.css";
import styles from "./IconTextModal.module.scss";
import Modal from "../Modal";
import { FileJpgOutlined } from "@ant-design/icons";
/**
 * @param image Image in top of Modal
 * @param title Title of Modal
 * @param description Description of Modal
 * @param props Rest props of a modal base
 * @return An IconTextModal
 */
function IconTextModal({
  image = <FileJpgOutlined />,
  title = null,
  description = null,
  ...props
}) {
  const Image = typeof image === "string" ? <img src={image} /> : image;
  return (
    <Modal {...props} className={`${styles.icontextmodal}`}>
      <div className={`${styles["modal-image"]}`}>{Image}</div>
      <div className={`${styles["modal-title"]}`}>{title}</div>
      <div className={`${styles["modal-description"]}`}>{description}</div>
    </Modal>
  );
}
export default IconTextModal;
