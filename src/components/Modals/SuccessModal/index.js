import Modal from "../Modal";

import styles from "./SuccessModal.module.scss";

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @returns A Share modal
 */

const SuccessModal = ({
  visible,
  closable,
  href,
  onCancel,
  cancelText,
  children,
  ...props
}) => {
  return (
    <Modal
      {...props}
      visible={visible}
      closable={closable}
      href={href}
      onCancel={onCancel}
      cancelText={cancelText}
      className={styles.modal}
    >
      {children}
    </Modal>
  );
};

export default SuccessModal;
