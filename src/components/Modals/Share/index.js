import Modal from "../Modal";

import styles from "./Share.module.scss";

import IconGroup from "@components/SocialNetworks/IconGroup";

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @param userId The id of the user to share
 * @param postsId The id of the post that this modal is related to
 * @param proposalId The id of the proposal that this modal is related to (if required)
 *
 * @returns A Share modal
 */

const Share = ({
  visible,
  closable = false,
  href,
  onCancel,
  cancelText,
  userId = "",
  postsId = "",
  proposalId = "",
  ...props
}) => {
  return (
    <Modal
      {...props}
      visible={visible}
      closable={closable}
      href={href}
      onCancel={onCancel}
      cancelText={cancelText}
      className={styles.shareModal}
    >
      <h2 className={styles.modalTitle}> Invitar mediante: </h2>
      <IconGroup
        href={href}
        userId={userId}
        postsId={postsId}
        proposalId={proposalId}
      />
    </Modal>
  );
};

export default Share;

/**
 * socialNetwork: Enum: "WHATSAPP" "FACEBOOK" "TWITTER" "TELEGRAM" "LINK"
 * proposalId: String
 * postId: String
 * userId: String
 */
