import { withDesign } from "storybook-addon-designs";

import Share from "./index";

export default {
  title: "Components/Share",
  component: Share,
  decorators: [withDesign],
  argTypes: {
    handleClose: { action: "closed" },
    href: { action: "clicked" },
  },
};

const Template = (args) => <Share {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlShare = {
  default: "748%3A7238",
};

export const Default = Template.bind({});
Default.args = {
  href: "https://url.example",
  visible: true,
  type: "link",
  cancelText: <span>Cancelar</span>,
  closable: false,
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlShare.default}`,
  },
};
