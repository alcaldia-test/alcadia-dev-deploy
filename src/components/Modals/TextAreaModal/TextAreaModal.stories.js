import { withDesign } from "storybook-addon-designs";

import TextAreaModal from "./index.js";

export default {
  title: "Components/Modals/TextArea",
  component: TextAreaModal,
  decorators: [withDesign],
  argTypes: {
    onCancel: { action: "cancel" },
    onOk: { action: "ok" },
    title: { control: "text" },
    text: { control: "text" },
    placeholder: { control: "text" },
    id: { control: "text" },
    closable: { control: "boolean" },
    okText: { control: "text" },
    cancelText: { control: "text" },
  },
};

const Template = (args) => <TextAreaModal {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlTextAreaModal = {
  default: "20%3A6194",
};

export const Default = Template.bind({});
Default.args = {
  closable: false,
  cancelText: "Cerrar",
  id: "1",
  okText: "Bloquear",
  placeholder: "Contenido",
  text: "Motivo",
  title: "Motivo del bloqueo a usuario",
  visible: true,
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlTextAreaModal.default}`,
  },
};
