import React, { useRef } from "react";

import Modal from "@components/Modals/Modal";

import styles from "./TextAreaModal.module.scss";

const TextAreaModal = ({ title, text, id, placeholder, ...props }) => {
  const textAreaRef = useRef(null);

  // OnOk pass the ref textarea value
  const onOk = () => {
    props.onOk(textAreaRef.current.value);
    textAreaRef.current.value = "";
  };
  const okProps = { type: "primary", size: "small-x" };
  const cancelProps = { type: "secondary", size: "super-small" };

  return (
    <Modal
      {...props}
      okButtonProps={okProps}
      cancelButtonProps={cancelProps}
      onOk={() => onOk()}
      className={styles.text__area__modal}
    >
      <h6 className={styles.text__area__modal__title}>{title}</h6>
      <div className={styles.text__area__container}>
        <label htmlFor={id} className={styles.text__area__modal__text}>
          {text}
        </label>
        <textarea
          ref={textAreaRef}
          name={title}
          placeholder={placeholder}
          id={id}
        />
      </div>
    </Modal>
  );
};

export default TextAreaModal;
