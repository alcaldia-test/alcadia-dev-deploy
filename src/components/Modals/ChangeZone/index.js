import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  patchUserZone,
  postUserZone,
  getZonesOfMacro,
} from "@redux/voting/actions";
import moment from "moment";

import Autocomplete from "@components/Autocomplete/Autocomplete";
import Modal from "../Modal";
import RadioGroup from "@components/RadioGroup";

import { Warning } from "@assets/icons";

import styles from "./ChangeZone.module.scss";

require("moment/locale/es.js");

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @returns A Share modal
 */

const optionsModal = [
  { id: "FAMILY", name: "Sí, tengo familiares cercanos" },
  { id: "BUSINESS", name: "Sí, tengo mi negocio" },
  { id: "HOME", name: "Sí, vivo en la zona" },
  { id: "NO", name: "No tengo relación con las zonas" },
];

const ChangeZoneModal = ({
  visible,
  closable,
  href,
  onCancel,
  cancelText,
  okText,
  onOk,
  macros,
  userId,
  votingId,
}) => {
  const dispatch = useDispatch();
  // const { vote } = useSelector((state) => state.voting);
  const {
    dataUser: { favorites },
  } = useSelector((state) => state.auth);
  const [selectedOption, setSelectedOption] = useState({});
  const [macroAutocomplete, setMacroAutocomplete] = useState({});
  const macrosAutocomplete =
    macros?.length > 0 ? macros.map((macro) => ({ title: macro.name })) : [];
  const [error, setError] = useState(true);

  useEffect(() => {
    if (Object.keys(macroAutocomplete).length > 0) {
      dispatch(getZonesOfMacro(macroAutocomplete.id));
    }
  }, [macroAutocomplete]);

  const handleChange = (e) => {
    setSelectedOption(e.target);
  };

  const handleChangeAutocomplete = (e) => {
    if (macros?.length > 0) {
      const macroObj = macros.find((macro) => macro.name === e);
      if (macroObj) {
        setMacroAutocomplete(macroObj);
      } else {
        setMacroAutocomplete({});
      }
    }
  };

  const handleSubmit = () => {
    if (selectedOption.id === "NO") {
      localStorage.setItem(`${votingId}_${userId}`, "NO");
      onCancel();
    } else {
      if (Object.keys(macroAutocomplete).length > 0) {
        changeZone({ macroId: macroAutocomplete.id, tag: selectedOption.id });
      }
    }
  };

  const changeZone = ({ macroId = undefined, tag = undefined }) => {
    const methodData = {
      votingId,
      userId,
      data: {
        tag,
        userId,
        macroId,
        updatedAt: moment().format("YYYY-MM-DD HH:mm:ss"),
      },
    };

    if (favorites.length > 0) {
      const hasFavoriteWithTag = favorites.find(
        (favorite) => favorite.tag === selectedOption.id
      );
      if (hasFavoriteWithTag) {
        methodData.favoriteId = hasFavoriteWithTag.id;
        if (hasFavoriteWithTag.createdAt === hasFavoriteWithTag.updatedAt) {
          dispatch(patchUserZone(methodData));
          onOk();
        } else {
          if (
            moment(hasFavoriteWithTag.updatedAt).add(6, "months") < moment()
          ) {
            dispatch(patchUserZone(methodData));
            onOk();
          } else {
            const dateToUpdate = moment(hasFavoriteWithTag.updatedAt)
              .add(6, "months")
              .format("dddd D [de] MMMM [de] YYYY [a las] HH:mm");
            switch (selectedOption.id) {
              case "FAMILY":
                setError(
                  `Cambiaste la zona de tus familiares en los últimos 6 meses, podrás cambiarla el ${dateToUpdate} nuevamente.`
                );
                break;
              case "BUSINESS":
                setError(
                  `Cambiaste la zona de negocios cercanos en los últimos 6 meses, podrás cambiarla el ${dateToUpdate} nuevamente.`
                );
                break;
              case "HOME":
                setError(
                  `Cambiaste la zona de tu hogar en los últimos 6 meses, podrás cambiarla el ${dateToUpdate} nuevamente.`
                );
                break;
              default:
                setError(
                  `Cambiaste tu zona en los últimos 6 meses, podrás cambiarla de nuevo el ${dateToUpdate} nuevamente.`
                );
            }
          }
        }
      } else {
        dispatch(postUserZone(methodData));
        onOk();
      }
    } else {
      dispatch(postUserZone(methodData));
      onOk();
    }
  };

  return (
    <Modal
      visible={visible}
      closable={closable}
      href={href}
      onCancel={onCancel}
      okText={
        Object.keys(selectedOption).length > 0
          ? selectedOption.id === "NO"
            ? okText
            : Object.keys(macroAutocomplete).length > 0
            ? okText
            : null
          : null
      }
      cancelText={cancelText}
      className={styles.modal}
      onOk={() => {
        handleSubmit();
      }}
    >
      <div className={styles.warning}>
        <Warning />
        Usted no pertenece a esta zona, si añade esta zona no podrá actualizarla
        por los próximos 6 meses.
      </div>
      <h6>¿Tiene alguna relación con las zonas de la votación?</h6>
      <div className={styles.radioGroup}>
        <RadioGroup onChange={handleChange} options={optionsModal} />
      </div>
      {Object.keys(selectedOption).length === 0 ||
        (selectedOption.id !== "NO" && macros?.length > 0 && (
          <>
            <p className={styles.text}>Indique la macrozona</p>
            <div className={styles.autocomplete}>
              <Autocomplete
                dataSource={macrosAutocomplete}
                placeholder="Elija una macrozona"
                onChange={handleChangeAutocomplete}
              />
            </div>
          </>
        ))}
      {error && <div className={styles.error}>{error}</div>}
    </Modal>
  );
};

export default ChangeZoneModal;
