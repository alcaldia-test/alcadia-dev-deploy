import Modal from "../Modal";

import styles from "./VoteModal.module.scss";

/**
 *
 * @param visible If the modal is visible
 * @param closable If the modal must have a close button
 * @param href The link to share (By default, the current page))
 * @param onCancel The function to call when the modal is closed
 * @param cancelText The text to show in the close button
 * @returns A Share modal
 */

const VoteModal = ({
  visible,
  closable,
  href,
  onCancel,
  cancelText,
  okText,
  onOk,
  title,
}) => {
  return (
    <Modal
      visible={visible}
      closable={closable}
      href={href}
      onCancel={onCancel}
      cancelText={cancelText}
      okText={okText}
      className={styles.modal}
      onOk={onOk}
    >
      <h4>
        ¿Seguro que desas votar por <span>{title}</span>?
      </h4>
    </Modal>
  );
};

export default VoteModal;
