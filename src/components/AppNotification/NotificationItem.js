import React from "react";
import Image from "next/image";
import moment from "moment";

const NotificationItem = ({ notification }) => {
  const { image, title, created, type } = notification;
  return (
    <li className="gx-media">
      <Image className="gx-size-40 gx-mr-3" alt={image} src={image} />
      <div className="gx-media-body gx-align-self-center">
        <p className="gx-fs-sm gx-mb-0">{title}</p>
        <b className="gx-fs-sm gx-mb-0">{type}</b>
        <span className="gx-meta-date">
          <small>{moment(created).fromNow()}</small>
        </span>
      </div>
    </li>
  );
};

export default NotificationItem;
