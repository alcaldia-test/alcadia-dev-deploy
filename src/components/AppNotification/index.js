import React from "react";
// import { useSelector } from "react-redux";

const AppNotification = () => {
  // const { notifications } = useSelector(({ auth }) => auth);
  return (
    <>
      <div className="gx-popover-header">
        <h3 className="gx-mb-0">Notificaciones</h3>
        <i className="gx-icon-btn icon icon-charvlet-down" />
      </div>
      <div className="gx-popover-scroll">
        <ul className="gx-sub-popover"></ul>
      </div>
    </>
  );
};

export default AppNotification;
