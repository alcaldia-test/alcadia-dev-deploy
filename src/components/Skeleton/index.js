import React from "react";
import "antd/lib/skeleton/style/index.css";
import { Skeleton } from "antd";

function skeleton(props) {
  return <Skeleton {...props} />;
}

export default skeleton;
