import React from "react";
import { withDesign } from "storybook-addon-designs";
import BottomsActions from "./index";

export default {
  title: "Components/BottomsActions",
  component: BottomsActions,
  decorators: [withDesign],
};

const Template = (args) => <BottomsActions {...args} />;

/**
 * @param {getVote} function (vote) {
   el parametro get vote es una funcion que se ejecuta cuando se hace click en el boton de like o dislike
   en caso de dar like recibe una string con el valor "liked"
   en caso de dar dislike recibe una string con el valor "disliked"
 }
*/

export const Default = Template.bind({});
Default.args = {
  message: "¿Estas de acuerdo?",
  debate: false,
  isLogged: true,
  isNearly: true,
  commentButton: false,
  remainingDays: 15,
  voteData: {
    like: 15,
    dislike: 5,
  },
  getVote: (vote) => console.log(vote),
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A13243",
  },
};

export const commentButton = Template.bind({});
commentButton.args = {
  debate: false,
  isLogged: true,
  isNearly: true,
  commentButton: true,
  remainingDays: 15,
  voteData: {
    like: 15,
    dislike: 5,
  },
  getVote: (vote) => console.log(vote),
};

commentButton.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A13243",
  },
};

export const notNearly = Template.bind({});
notNearly.args = {
  debate: false,
  isLogged: true,
  isNearly: false,
  commentButton: false,
  remainingDays: 15,
  voteData: {
    like: 15,
    dislike: 5,
  },
  getVote: (vote) => console.log(vote),
};

notNearly.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A13243",
  },
};

export const notlogged = Template.bind({});
notlogged.args = {
  debate: false,
  isLogged: false,
  isNearly: false,
  commentButton: false,
  remainingDays: 15,
  voteData: {
    like: 15,
    dislike: 5,
  },
  getVote: (vote) => console.log(vote),
};

notlogged.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A13243",
  },
};

export const debate = Template.bind({});
debate.args = {
  debate: true,
  isLogged: false,
  isNearly: true,
  commentButton: false,
  remainingDays: 15,
  ended: false,
  message: "Esta normativa finalizó",
  voteData: {
    like: 15,
    dislike: 5,
  },
  getVote: (vote) => console.log(vote),
};

debate.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A13243",
  },
};
