import React, { useState } from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { useSetPercentages, useValidateVotes, useCheckValue } from "./hook";
import Link from "next/link";
import Modal from "@components/Modals/SupportModal";

import styles from "./bottomsActions.module.scss";
import Image from "next/image";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { routeRedirectRegister } from "@redux/auth/actions";

const BottomsActions = ({
  debate,
  isLogged,
  isNearly,
  commentButton,
  voteData,
  remainingDays,
  getVote,
  message,
  reaction,
}) => {
  const [like, setLike] = useState(!!(debate && reaction === "like"));
  const [dislike, setDislike] = useState(!!(debate && reaction === "like"));
  const [selected, setSelected] = useState(!!(debate && reaction !== "any"));
  const nearly = useCheckValue(isNearly);
  const logged = useCheckValue(isLogged);
  const votes = useValidateVotes(voteData);
  const [reactionModal, setReactionModal] = useState(false);
  const [okText, setOkText] = useState("Si, la apoyo");
  const [supportModal, setSupportModal] = useState(true);
  const { likePercent, dislikePercent } = useSetPercentages({ ...votes });
  const router = useRouter();
  const dispatch = useDispatch();
  const setUrl = (e) => {
    if (e.target.tagName === "A") {
      dispatch(routeRedirectRegister(window?.location.pathname));
    }
  };
  const Message = () => {
    if (!logged) {
      const goto = encodeURI(window.location.pathname);

      return (
        <div
          className={`${styles.infoContainer} block items-center justify-center`}
        >
          <span className="font-bold" onClick={setUrl}>
            Necesitas{" "}
            <Link href={`/login?goto=${goto}`}>iniciar sesi&oacute;n</Link> o{" "}
            <Link href="/register">registrarte</Link> para participar
          </span>
        </div>
      );
    }

    switch (logged) {
      case logged === true && nearly === false:
        return (
          <div
            className={`${styles.infoWarn} flex items-center flex-wrap justify-start `}
          >
            <ExclamationCircleOutlined className={`${styles.infoIcon} flex`} />
            <span className={`${styles.infoText} font-bold`}>
              No perteneces a esta zona, disculpe no puede participar
            </span>
          </div>
        );
      case logged && !selected:
        return (
          <div>
            {debate === true ? (
              <div className={`${styles.infoGhost} text-center font-bold`}>
                <span>{message}</span>
              </div>
            ) : (
              <div className={styles.infoContainer}>
                <span>{message}</span>
              </div>
            )}
          </div>
        );
      case logged && selected:
        return (
          <div>
            {debate === true ? (
              <div className={`${styles.infoGhost} text-center font-bold`}>
                <span className="voted">Ya participaste</span>
              </div>
            ) : (
              <div className={styles.infoContainer}>
                <span>
                  Los resultados se publicar&aacute;n en: {remainingDays || "0"}{" "}
                  d&iacute;as
                </span>
              </div>
            )}
          </div>
        );
      default:
        return (
          <div>
            {debate === true ? (
              <div className={`${styles.infoGhost} text-center font-bold`}>
                <span>{message}</span>
              </div>
            ) : (
              <div className={styles.infoContainer}>
                <span>
                  Tiempo restante para participar: {remainingDays || "0"}{" "}
                  d&iacute;as
                </span>
              </div>
            )}
          </div>
        );
    }
  };

  return (
    <>
      <Modal
        visible={reactionModal}
        closable={false}
        onCancel={() => {
          setReactionModal(false);
          getVote("any");
        }}
        cancelText="Cancelar"
        okText={okText}
        support={supportModal}
        onOk={() => {
          setReactionModal(false);
          if (supportModal) {
            setLike(!like);
            setSelected(!selected);
            getVote("liked");
          } else {
            getVote("disliked");
            setDislike(!dislike);
            setSelected(!selected);
          }
        }}
      />
      {commentButton ? (
        <button className={styles.commentButton}>
          <span className="font-bold">
            Dejanos tu opinion en los comentarios
          </span>
        </button>
      ) : (
        <div
          className={
            debate === true ? styles.buttonsActionsGhost : styles.buttonsActions
          }
        >
          <div
            className={`${styles.actionsContainer} flex w-full flex-col items-center`}
          >
            <Message />
            <div
              className={
                !selected
                  ? styles.buttonsContainer
                  : debate === true && selected === true
                  ? `${styles.buttonsContainerGhost} ${styles.percentLayout}`
                  : styles.buttonsContainerGhost
              }
            >
              <button
                disabled={nearly === false ? true : dislike}
                event={"like"}
                islogged={logged}
                onClick={() => {
                  !isLogged &&
                    router.push({
                      pathname: "/login",
                      query: { goto: encodeURI(window.location.pathname) },
                    });
                  setReactionModal(true);
                  setSupportModal(true);
                  setOkText("Estoy de acuerdo");
                }}
                name="like"
                className={
                  nearly === false || (debate === true && !selected)
                    ? "likeDisable"
                    : debate === true && selected && dislike
                    ? "percentLikeDisable"
                    : debate === true && selected && like
                    ? "percentLiked"
                    : dislike === false && !selected
                    ? "enabled"
                    : like === true && selected
                    ? "liked"
                    : "buttonDisable"
                }
                style={
                  debate === true && selected
                    ? { flexBasis: `${likePercent}%` }
                    : {}
                }
              >
                <span role="img">
                  <Image src="/icons/like.svg" width={30} height={30} />
                </span>
                <span className="buttonText">
                  {like === false && !selected
                    ? "La apoyo"
                    : debate === true && selected && like
                    ? `${likePercent}%`
                    : debate === true && selected && dislike
                    ? `${likePercent}%`
                    : like && selected
                    ? `Tu y ${votes.like}`
                    : votes.like}
                </span>
              </button>
              <button
                disabled={nearly === false ? true : like}
                event={"dislike"}
                islogged={logged}
                onClick={() => {
                  !isLogged &&
                    router.push({
                      pathname: "/login",
                      query: { goto: encodeURI(window.location.pathname) },
                    });
                  setReactionModal(true);
                  setSupportModal(false);
                  setOkText("Estoy en desacuerdo");
                }}
                name="dislike"
                className={
                  nearly === false || (debate === true && !selected)
                    ? "dislikeDisable"
                    : debate === true && selected && like
                    ? "percentDislikeDisable"
                    : debate === true && selected && dislike
                    ? "percentDisliked"
                    : like === false && !selected
                    ? "enabled"
                    : dislike === true && selected
                    ? "disliked"
                    : "buttonDisable"
                }
                style={
                  debate === true && selected
                    ? { flexBasis: `${dislikePercent}%` }
                    : {}
                }
              >
                <span role="img">
                  <Image src="/icons/dislike.svg" width={30} height={30} />
                </span>
                <span className="buttonText">
                  {dislike === false && !selected
                    ? "No la apoyo"
                    : debate === true && selected && dislike
                    ? `${dislikePercent}%`
                    : debate === true && selected && like
                    ? `${dislikePercent}%`
                    : dislike && selected
                    ? `tu y ${votes.dislike}`
                    : votes.dislike}
                </span>
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default BottomsActions;
