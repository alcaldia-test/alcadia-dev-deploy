/**
 * @param object con dos propiedades
 * @property like y dislike son dos numeros
 * @returns objecto con propiedades likePercent y dislikePercent
 * ambos valores son una integral redondeada
 */

export function useSetPercentages({ like, dislike }) {
  if (like === 0 && dislike === 0) {
    like = 1;
    dislike = 1;
  }

  const total = parseInt(like) + parseInt(dislike);
  const likePercent = Math.round((parseInt(like) * 100) / total);
  const dislikePercent = Math.round((parseInt(dislike) * 100) / total);

  return {
    likePercent,
    dislikePercent,
  };
}

/**
 * @param votes es un objeto que tiene dos valores like, dislike
 * like y dislike es una intgral
 * @returns un objeto de votos con dos valoker like y dislike
 * de no recibir el objeto de votes retorna por defecto
 * like 50 y dislikes 50
 */
export function useValidateVotes(votes) {
  if (!votes) {
    return {
      like: 50,
      dislike: 50,
    };
  }

  return {
    like: parseInt(votes.like),
    dislike: parseInt(votes.dislike),
  };
}

/**
 * esta funcion chequea que el valor de una vairable y retorna el mismo
 * de no recibir el parametro o el valor de la variable sea null
 * retorna false
 * @param value es una variable
 * @returns value si existe, de no ser asi retorna false
 */

export function useCheckValue(value) {
  return !value ? false : value;
}
