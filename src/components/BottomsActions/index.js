import React, { useState } from "react";
import { ExclamationCircleOutlined, RightOutlined } from "@ant-design/icons";
import { useSetPercentages, useValidateVotes, useCheckValue } from "./hook";
import Link from "next/link";

import styles from "./bottomsActions.module.scss";
import { useDispatch } from "react-redux";
import { routeRedirectRegister } from "@redux/auth/actions";

const BottomsActions = ({
  debate,
  isLogged,
  isNearly,
  commentButton,
  voteData,
  remainingDays,
  getVote,
  message,
  modalMessage,
  reaction = "any",
  ended,
}) => {
  const [like, setLike] = useState(!!(debate && reaction === "like"));
  const [dislike, setDislike] = useState(!!(debate && reaction === "dislike"));
  const [selected, setSelected] = useState(!!(debate && reaction !== "any"));
  const [open, setOpen] = useState(false);
  const nearly = useCheckValue(isNearly);
  const logged = useCheckValue(isLogged);
  const votes = useValidateVotes(voteData);
  const { likePercent, dislikePercent } = useSetPercentages({ ...votes });
  const dispatch = useDispatch();
  const setUrl = (e) => {
    if (e.target.tagName === "A") {
      dispatch(routeRedirectRegister(window?.location.pathname));
    }
  };
  const Message = () => {
    if (ended) {
      return (
        <div className={styles.infoContainer}>
          <span>{message}</span>
        </div>
      );
    }

    if (!logged) {
      const goto = encodeURI(window.location.pathname);

      return (
        <div className={styles.infoContainer}>
          <span onClick={setUrl}>
            Necesitas <Link href={`/login?goto=${goto}`}>iniciar sesión</Link> o{" "}
            <Link href="/register">registrarte</Link> para participar
          </span>
        </div>
      );
    }

    switch (logged) {
      case logged === true && nearly === false:
        return (
          <div className={styles.infoWarn}>
            <ExclamationCircleOutlined className={styles.infoIcon} />
            <span className={styles.infoText}>
              No perteneces a esta zona, disculpe no puede participar
            </span>
          </div>
        );
      case logged && !selected:
        return (
          <div>
            {debate === true ? (
              <div className={styles.infoGhost}>
                <span>{message}</span>
              </div>
            ) : (
              <div className={styles.infoContainer}>
                <span>{message}</span>
              </div>
            )}
          </div>
        );
      case logged && selected:
        return (
          <div>
            {debate === true ? (
              <div className={styles.infoGhost}>
                <span className="voted">Ya participaste</span>
              </div>
            ) : (
              <div className={styles.infoContainer}>
                <span>
                  Los resultados se publicar&aacute;n en: {remainingDays || "0"}{" "}
                  d&iacute;as
                </span>
              </div>
            )}
          </div>
        );
      default:
        return (
          <div>
            {debate === true ? (
              <div className={styles.infoGhost}>
                <span>{message}</span>
              </div>
            ) : (
              <div className={styles.infoContainer}>
                <span>
                  Tiempo restante para participar: {remainingDays || "0"}{" "}
                  d&iacute;as
                </span>
              </div>
            )}
          </div>
        );
    }
  };

  return (
    <>
      {open && (
        <div className={styles.confirmationModal}>
          <div className={like ? styles.modalLike : styles.modalDislike}>
            <span role="img">
              <svg
                viewBox="0 0 42 42"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M25.5501 14.0001H36.7501C37.6783 14.0001 38.5686 14.3689 39.2249 15.0253C39.8813 15.6816 40.2501 16.5719 40.2501 17.5001V21.1821C40.2505 21.6395 40.1614 22.0926 39.9876 22.5156L34.5713 35.6669C34.4392 35.9875 34.2148 36.2617 33.9266 36.4545C33.6384 36.6474 33.2993 36.7503 32.9526 36.7501H3.50006C3.03593 36.7501 2.59081 36.5658 2.26262 36.2376C1.93444 35.9094 1.75006 35.4643 1.75006 35.0001V17.5001C1.75006 17.036 1.93444 16.5909 2.26262 16.2627C2.59081 15.9345 3.03593 15.7501 3.50006 15.7501H9.59356C9.87373 15.7502 10.1498 15.683 10.3986 15.5542C10.6474 15.4254 10.8617 15.2387 11.0233 15.0099L20.5661 1.48764C20.6867 1.31668 20.8646 1.19462 21.0675 1.14357C21.2704 1.09252 21.4849 1.11587 21.6721 1.20939L24.8466 2.79664C25.7399 3.24317 26.4536 3.98194 26.8691 4.89019C27.2845 5.79844 27.3767 6.8215 27.1303 7.78939L25.5501 14.0001ZM12.2501 18.5291V33.2501H31.7801L36.7501 21.1821V17.5001H25.5501C25.017 17.5001 24.491 17.3782 24.0122 17.1439C23.5334 16.9096 23.1144 16.5691 22.7873 16.1482C22.4601 15.7274 22.2334 15.2374 22.1245 14.7156C22.0155 14.1938 22.0272 13.654 22.1586 13.1374L23.7388 6.92839C23.7883 6.73471 23.7699 6.52994 23.6868 6.34814C23.6037 6.16634 23.4609 6.01847 23.2821 5.92914L22.1253 5.35164L13.8828 17.0276C13.4453 17.6471 12.8853 18.1546 12.2501 18.5291ZM8.75006 19.2501H5.25006V33.2501H8.75006V19.2501Z" />
              </svg>
            </span>
            <h4>
              {like ? (
                <>
                  ¿quieres <span>Apoyar</span> {modalMessage.like}?
                </>
              ) : (
                <>
                  ¿Seguro que <span>No apoyas</span> {modalMessage.dislike}?
                </>
              )}
            </h4>
            <div className={styles.actionsButtons}>
              <span
                onClick={() => {
                  setSelected(false);
                  setDislike(false);
                  setLike(false);
                  setOpen(false);
                }}
              >
                Cancelar
              </span>
              <span
                onClick={() => {
                  setSelected(true);
                  if (like) {
                    getVote("liked");
                    setOpen(false);
                    setLike(true);
                    return;
                  }
                  setDislike(true);
                  getVote("dislike");
                  setOpen(false);
                }}
              >
                {like ? "Si, La apoyo" : "No la apoyo"} <RightOutlined />
              </span>
            </div>
          </div>
        </div>
      )}
      {commentButton ? (
        <button className={styles.commentButton}>
          <span>Dejanos tu opinion en los comentarios</span>
        </button>
      ) : (
        <div
          className={
            debate === true ? styles.buttonsActionsGhost : styles.buttonsActions
          }
        >
          <div className={styles.actionsContainer}>
            <Message />
            <div
              className={
                !selected
                  ? styles.buttonsContainer
                  : debate === true && selected === true
                  ? `${styles.buttonsContainerGhost} ${styles.percentLayout}`
                  : styles.buttonsContainerGhost
              }
            >
              <button
                disabled={nearly === false ? true : dislike}
                event={"like"}
                islogged={logged}
                onClick={() => {
                  if (ended || !logged) return;
                  setOpen(true);
                  setLike(true);
                }}
                name="like"
                className={
                  nearly === false || (debate === true && !selected)
                    ? "likeDisable"
                    : debate === true && selected && dislike
                    ? "percentLikeDisable"
                    : debate === true && selected && like
                    ? "percentLiked"
                    : dislike === false && !selected
                    ? "enabled"
                    : like === true && selected
                    ? "liked"
                    : "buttonDisable"
                }
                style={
                  debate === true && selected
                    ? { flexBasis: `${likePercent}%` }
                    : {}
                }
              >
                <span role="img">
                  <svg
                    viewBox="0 0 42 42"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M25.5501 14.0001H36.7501C37.6783 14.0001 38.5686 14.3689 39.2249 15.0253C39.8813 15.6816 40.2501 16.5719 40.2501 17.5001V21.1821C40.2505 21.6395 40.1614 22.0926 39.9876 22.5156L34.5713 35.6669C34.4392 35.9875 34.2148 36.2617 33.9266 36.4545C33.6384 36.6474 33.2993 36.7503 32.9526 36.7501H3.50006C3.03593 36.7501 2.59081 36.5658 2.26262 36.2376C1.93444 35.9094 1.75006 35.4643 1.75006 35.0001V17.5001C1.75006 17.036 1.93444 16.5909 2.26262 16.2627C2.59081 15.9345 3.03593 15.7501 3.50006 15.7501H9.59356C9.87373 15.7502 10.1498 15.683 10.3986 15.5542C10.6474 15.4254 10.8617 15.2387 11.0233 15.0099L20.5661 1.48764C20.6867 1.31668 20.8646 1.19462 21.0675 1.14357C21.2704 1.09252 21.4849 1.11587 21.6721 1.20939L24.8466 2.79664C25.7399 3.24317 26.4536 3.98194 26.8691 4.89019C27.2845 5.79844 27.3767 6.8215 27.1303 7.78939L25.5501 14.0001ZM12.2501 18.5291V33.2501H31.7801L36.7501 21.1821V17.5001H25.5501C25.017 17.5001 24.491 17.3782 24.0122 17.1439C23.5334 16.9096 23.1144 16.5691 22.7873 16.1482C22.4601 15.7274 22.2334 15.2374 22.1245 14.7156C22.0155 14.1938 22.0272 13.654 22.1586 13.1374L23.7388 6.92839C23.7883 6.73471 23.7699 6.52994 23.6868 6.34814C23.6037 6.16634 23.4609 6.01847 23.2821 5.92914L22.1253 5.35164L13.8828 17.0276C13.4453 17.6471 12.8853 18.1546 12.2501 18.5291ZM8.75006 19.2501H5.25006V33.2501H8.75006V19.2501Z" />
                  </svg>
                </span>
                <span className="buttonText">
                  {like === false && !selected
                    ? "La apoyo"
                    : debate === true && selected && like
                    ? `${likePercent}%`
                    : debate === true && selected && dislike
                    ? `${likePercent}%`
                    : like && selected
                    ? `Tu y ${votes.like}`
                    : votes.like}
                </span>
              </button>
              <button
                disabled={nearly === false ? true : like}
                event={"dislike"}
                islogged={logged}
                onClick={() => {
                  if (ended || !logged) return;
                  setOpen(true);
                  setDislike(true);
                }}
                name="dislike"
                className={
                  nearly === false || (debate === true && !selected)
                    ? "dislikeDisable"
                    : debate === true && selected && like
                    ? "percentDislikeDisable"
                    : debate === true && selected && dislike
                    ? "percentDisliked"
                    : like === false && !selected
                    ? "enabled"
                    : dislike === true && selected
                    ? "disliked"
                    : "buttonDisable"
                }
                style={
                  debate === true && selected
                    ? { flexBasis: `${dislikePercent}%` }
                    : {}
                }
              >
                <span role="img">
                  <svg
                    className="dislike"
                    viewBox="0 0 42 42"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M25.5501 14.0001H36.7501C37.6783 14.0001 38.5686 14.3689 39.2249 15.0253C39.8813 15.6816 40.2501 16.5719 40.2501 17.5001V21.1821C40.2505 21.6395 40.1614 22.0926 39.9876 22.5156L34.5713 35.6669C34.4392 35.9875 34.2148 36.2617 33.9266 36.4545C33.6384 36.6474 33.2993 36.7503 32.9526 36.7501H3.50006C3.03593 36.7501 2.59081 36.5658 2.26262 36.2376C1.93444 35.9094 1.75006 35.4643 1.75006 35.0001V17.5001C1.75006 17.036 1.93444 16.5909 2.26262 16.2627C2.59081 15.9345 3.03593 15.7501 3.50006 15.7501H9.59356C9.87373 15.7502 10.1498 15.683 10.3986 15.5542C10.6474 15.4254 10.8617 15.2387 11.0233 15.0099L20.5661 1.48764C20.6867 1.31668 20.8646 1.19462 21.0675 1.14357C21.2704 1.09252 21.4849 1.11587 21.6721 1.20939L24.8466 2.79664C25.7399 3.24317 26.4536 3.98194 26.8691 4.89019C27.2845 5.79844 27.3767 6.8215 27.1303 7.78939L25.5501 14.0001ZM12.2501 18.5291V33.2501H31.7801L36.7501 21.1821V17.5001H25.5501C25.017 17.5001 24.491 17.3782 24.0122 17.1439C23.5334 16.9096 23.1144 16.5691 22.7873 16.1482C22.4601 15.7274 22.2334 15.2374 22.1245 14.7156C22.0155 14.1938 22.0272 13.654 22.1586 13.1374L23.7388 6.92839C23.7883 6.73471 23.7699 6.52994 23.6868 6.34814C23.6037 6.16634 23.4609 6.01847 23.2821 5.92914L22.1253 5.35164L13.8828 17.0276C13.4453 17.6471 12.8853 18.1546 12.2501 18.5291ZM8.75006 19.2501H5.25006V33.2501H8.75006V19.2501Z" />
                  </svg>
                </span>
                <span className="buttonText">
                  {dislike === false && !selected
                    ? "No la apoyo"
                    : debate === true && selected && dislike
                    ? `${dislikePercent}%`
                    : debate === true && selected && like
                    ? `${dislikePercent}%`
                    : dislike && selected
                    ? `tu y ${votes.dislike}`
                    : votes.dislike}
                </span>
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default BottomsActions;
