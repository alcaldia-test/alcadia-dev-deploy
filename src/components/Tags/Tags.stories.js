import React from "react";
import { withDesign } from "storybook-addon-designs";
import Tags from "./index";

export default {
  title: "Components/Tags",
  component: Tags,
  decorators: [withDesign],
};

const Template = (args) => <Tags {...args} />;
export const Default = Template.bind({});

Default.args = {
  closable: true,
  tags: [
    { id: "1", name: "Tag numero 1" },
    { id: "2", name: "Tag numero 2" },
    { id: "3", name: "Tag numero 3" },
    { id: "4", name: "Tag numeor 4" },
  ],
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=677%3A6874",
  },
};
