import React from "react";
import { Tag } from "antd";
import { TagOutlined } from "@ant-design/icons";
import "antd/lib/tag/style/css";
import Styles from "./Tags.module.scss";

function Tags({ closable, tags, ...args }) {
  const handleClose = (id) => {
    tags = tags.filter((tag) => tag.id !== id);
  };
  return (
    <div className={`${Styles.tag}`}>
      {tags.map((tag) => (
        <Tag
          key={tag.id}
          closable={closable}
          onClose={(e) => {
            handleClose(tag.id);
          }}
          {...args}
        >
          {tag.name}
          <TagOutlined />
        </Tag>
      ))}
    </div>
  );
}

export default Tags;
