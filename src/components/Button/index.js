import React from "react";
import "antd/lib/button/style/index.css";
import { Button as AntButton } from "antd";
import styles from "./Button.module.scss";
/**
 *
 * @param children Children for this component
 * @param size Size of this component, can only be [ large , medium , small , small-x , super-small ]
 * @param type Type of this component, can only be [ primary , secondary , link ]
 * @param props Rest props of Button in AntDesign
 * @returns A Button component using AntDesign
 */

function Button({
  children,
  size = "large",
  type = "primary",
  shape = "round",
  className,
  ...props
}) {
  return (
    <AntButton
      {...props}
      className={`${styles.button} ${styles[type]} ${styles[size]} ${className}`}
      shape={shape}
    >
      {children}
    </AntButton>
  );
}

export default Button;
