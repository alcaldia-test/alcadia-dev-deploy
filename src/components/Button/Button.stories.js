import { withDesign } from "storybook-addon-designs";

import Button from "./index.js";

const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlButtons = {
  link: "16%3A6242",
  primary: "16%3A6216",
  secondary: "16%3A6227",
};

export default {
  title: "Components/Button",
  component: Button,
  decorators: [withDesign],
  argTypes: {
    size: {
      options: ["large", "medium", "small", "small-x", "super-small"],
      control: "select",
    },
    type: { options: ["primary", "secondary", "link"], control: "select" },
    block: { control: "boolean" },
    danger: { control: "boolean" },
    disabled: { control: "boolean" },
    ghost: { control: "boolean" },
    href: "string",
    htmlType: "button",
    icon: { ReactNode: "element" },
    loading: { control: "boolean" },
    shape: {
      options: ["default", "circle", "round", "circle-outline"],
      control: "select",
    },
    target: "Same as target attribute of a, works when href is specified	string",
  },
};

const Template = (args) => <Button {...args}>Button Text</Button>;

export const Primary = Template.bind({});
Primary.args = {
  type: "primary",
  shape: "round",
  size: "large",
};
Primary.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlButtons.primary}`,
  },
};

export const Secondary = Template.bind({});
Secondary.args = {
  type: "secondary",
  shape: "round",
  size: "large",
};
Secondary.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlButtons.secondary}`,
  },
};

export const Link = Template.bind({});
Link.args = {
  type: "link",
  shape: "round",
  size: "large",
};
Link.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlButtons.link}`,
  },
};
