import React from "react";
import ButtonComponent from "./index";
import Image from "next/image";

const statsButton = (props) => {
  return (
    <ButtonComponent {...props}>
      <div className="flex">
        <span className="text-callout mr-12">{"Estadísticas"}</span>
        <Image
          src="/icons/bar-char.svg"
          width={20}
          height={20}
          layout="fixed"
          alt="checkedred"
        />
      </div>
    </ButtonComponent>
  );
};
export default statsButton;
