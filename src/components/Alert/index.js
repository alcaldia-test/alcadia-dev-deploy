import { Alert } from "antd";
import "antd/lib/alert/style/index.css";

const Alerts = (props) => {
  return <Alert {...props} />;
};

export default Alerts;
