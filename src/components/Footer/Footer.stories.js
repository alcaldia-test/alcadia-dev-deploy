import { withDesign } from "storybook-addon-designs";

import Footer from "./index.js";

import {
  FacebookOutlined,
  InstagramOutlined,
  TwitterOutlined,
  YoutubeOutlined,
} from "@ant-design/icons";

export default {
  title: "Components/Footer",
  component: Footer,
  decorators: [withDesign],
};

const Template = (args) => (
  <Footer {...args}>
    {/* First section is the blue one */}
    <section>
      <hr />
      <section>
        {/* Each div will be an item in the grid */}
        <div>
          <h6>La paz decide</h6>
          <p>
            Es un espacio para fomentar la participacion de la ciudadania en la
            generacion de ideas para mejorar la gestión de la ciudad.
            <br />
            Si participas, decides.
          </p>
        </div>
        <div>
          <h6>Problemas Técnicos</h6>
          <a href="#">
            <u>Lee las preguntas frecuentes y resuelve tus dudas.</u>
          </a>
          <br />
          <a href="mailto:correspondencia@lapaz.bo">
            <u>correspondencia@lapaz.bo</u>
          </a>
        </div>
        <div>
          <h6>Contenido</h6>
          <a href="#">
            <u>Inicio</u>
          </a>
          <br />
          <a href="#">
            <u>Tu Voto Cuenta</u>
          </a>
          <br />
          <a href="#">
            <u>Propuestas ciudadanas</u>
          </a>
          <br />
          <a href="#">
            <u>Charlas Virtuales</u>
          </a>
          <br />
          <a href="#">
            <u>Normativa Colaborativa</u>
          </a>
        </div>
        <div>
          <h6>Transparencia</h6>
          <p>
            Conoce que hace el ayuntamiento de La Paz con qué recursos los
            consigue, por qué, para qué y para quién lo lleva a cabo.
          </p>
          <a href="#">
            <u>Portal de Transparencia</u>
          </a>
        </div>
        <div>
          <h6>Datos Abiertos</h6>
          <p>
            Conoce que hace el GAM de La Paz con qué recursos los consigue, por
            qué, para qué y para quién lo lleva a cabo.
          </p>
          <a href="#">
            <u>lapaz.bo</u>
          </a>
          <br />
          <a href="#">
            <u>Portal de Datos Abiertos</u>
          </a>
          <br />
          <a href="#">
            <u>Geoportal</u>
          </a>
        </div>
      </section>
    </section>
    <section>
      {/* Social Section */}
      <section>
        <a href="#" target="_blank" rel="noopener noreferrer">
          <FacebookOutlined />
        </a>
        <a href="#" target="_blank" rel="noopener noreferrer">
          <InstagramOutlined />
        </a>
        <a href="#" target="_blank" rel="noopener noreferrer">
          <TwitterOutlined />
        </a>
        <a href="#" target="_blank" rel="noopener noreferrer">
          <YoutubeOutlined />
        </a>
      </section>
      <hr />
      <div>
        <p>Gobierno Autónomo Municipal de La Paz</p>
        <p>Calle Mercado N° 1298, Edif. Palacio Consistorial, Zona Centro</p>
        <p>
          Central Telefónica: 2650000 - 2202000 -{" "}
          <a href="mailto:correspondencia@lapaz.bo">correspondencia@lapaz.bo</a>
        </p>
        <p>
          Derechos reservados Gobierno Autónomo Municipal de La Paz © 2021 -
          #PorElBienComun
        </p>
      </div>
    </section>
  </Footer>
);
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlFooter = {
  default: "16%3A12498",
};

export const Default = Template.bind({});
Default.args = {};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlFooter.default}`,
  },
};
