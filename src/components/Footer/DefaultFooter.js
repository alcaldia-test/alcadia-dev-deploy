import {
  FacebookOutlined,
  // InstagramOutlined,
  TwitterOutlined,
  YoutubeOutlined,
} from "@ant-design/icons";
import styles from "./Footer.module.scss";
import Link from "next/link";
import Image from "next/image";

function DefaultFooter() {
  return (
    <>
      <section className={`container mx-auto ${styles.links}`}>
        <hr />
        <section>
          {/* Each div will be an item in the grid */}
          <div>
            <h6>La Paz te escucha</h6>
            <p>
              Los datos públicos abiertos incrementan la transparencia sobre los
              gobiernos y sus actuaciones, nuestro municipio está trabajando
              para brindarte este servicio muy pronto.
              <br />
              Si participas, decides.
            </p>
          </div>
          <div>
            <h6>Problemas Técnicos</h6>
            <p>Por cualquier duda, favor enviar sus consultas a:</p>
            <a href="mailto:correspondencia@lapaz.bo">
              <u>correspondencia@lapaz.bo</u>
            </a>
          </div>
          <div>
            <h6>Con el apoyo de:</h6>
            <div className="w-full flex justify-around items-center">
              <Image
                width={94}
                height={60}
                src="/images/incidem.png"
                alt="Incidem"
              />
              <Image
                width={94}
                height={110}
                src="/images/madrid.png"
                alt="Madrid"
              />
            </div>
          </div>
          {/* <div>
            <h6>Contenido</h6>
            <ul>
              <li>
                -{" "}
                <Link href="/" passHref>
                  <a>
                    <u>Inicio</u>
                  </a>
                </Link>
              </li>
              <li>
                -{" "}
                <Link href="/tu_voto_cuenta" passHref>
                  <a>
                    <u>Tu Voto Cuenta</u>
                  </a>
                </Link>
              </li>
              <li>
                -{" "}
                <Link href="/propuestas_ciudadanas" passHref>
                  <a>
                    <u>Propuestas ciudadanas</u>
                  </a>
                </Link>
              </li>

              <li>
                -{" "}
                <Link href="/charlas" passHref>
                  <a>
                    <u>Charlas Virtuales</u>
                  </a>
                </Link>
              </li>
              <li>
                -{" "}
                <Link href="/normativas" passHref>
                  <a>
                    <u>Normativa Colaborativa</u>
                  </a>
                </Link>
              </li>
            </ul>
          </div> */}
          <div>
            <h6>Transparencia</h6>
            <p>
              Conoce que hace el GAM de La Paz con qué recursos los consigue,
              por qué, para qué y para quién lo lleva a cabo.
            </p>
            <Link href="https://www.lapaz.bo/transparencia" passHref>
              <a>
                <u>Portal de Transparencia</u>
              </a>
            </Link>
          </div>
          <div>
            <h6>Datos Abiertos</h6>
            <p>
              Conoce que hace el GAM de La Paz con qué recursos los consigue,
              por qué, para qué y para quién lo lleva a cabo.
            </p>
            <ul>
              <li>
                -{" "}
                <a href="https://www.lapaz.bo">
                  <u>Lapaz.bo</u>
                </a>
              </li>
              <li>
                -{" "}
                <u>
                  <a href="https://datosabiertos.lapaz.bo">Portal de Datos Abiertos</a>
                </u>
              </li>
              <li>
                -{" "}
                <u>
                  <a href="https://geo.gob.bo">Geoportal</a>
                </u>
              </li>
            </ul>
          </div>
        </section>
      </section>
      <section className={`container mx-auto ${styles.social}`}>
        {/* Social Section */}
        <section>
          <a
            href="https://www.facebook.com/MunicipioLaPaz"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FacebookOutlined />
          </a>
          {/* <a href="#" target="_blank" rel="noopener noreferrer">
            <InstagramOutlined />
          </a> */}
          <a
            href="https://mobile.twitter.com/lapazalcaldia"
            target="_blank"
            rel="noopener noreferrer"
          >
            <TwitterOutlined />
          </a>
          <a
            href="https://youtube.com/c/Alcald%C3%ADaLaPazGAMLP"
            target="_blank"
            rel="noopener noreferrer"
          >
            <YoutubeOutlined />
          </a>
        </section>
        <hr />
        <div>
          <p>Gobierno Autónomo Municipal de La Paz</p>
          <p>Calle Mercado N° 1298, Edif. Palacio Consistorial, Zona Centro</p>
          <p>
            Central Telefónica: 2650000 - 2202000 -{" "}
            <a href="mailto:correspondencia@lapaz.bo">
              correspondencia@lapaz.bo
            </a>
          </p>
          <p>
            Derechos reservados Gobierno Autónomo Municipal de La Paz © 2022 -
            #PorElBienComun
          </p>
        </div>
      </section>
    </>
  );
}
export default DefaultFooter;
