import React from "react";
import styles from "./Footer.module.scss";
import DefaultFooter from "@components/Footer/DefaultFooter";
export default function Footer({ children }) {
  const content = validateChildren(children) ? children : <DefaultFooter />;
  return (
    <footer id="footer" className={styles.footer}>
      {content}
    </footer>
  );
}

function validateChildren(children) {
  const childs = React.Children.toArray(children);
  try {
    if (childs.length > 0) {
      return childs.length > 1 || childs[0].trim().length > 0;
    }
    return false;
  } catch (error) {
    return true;
  }
}
