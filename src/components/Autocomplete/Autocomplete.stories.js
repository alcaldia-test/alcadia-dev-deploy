import React from "react";
import Autocomplete from "./index";

export default {
  title: "Components/Autocomplete",
  component: Autocomplete,
};

const Template = (args) => <Autocomplete {...args} />;
const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlAutocomplete = {
  default: "71%3A6268",
};

export const Default = Template.bind({});
Default.args = {
  options: [
    {
      title: "Ubicación 1",
    },
    {
      title: "Ubicación 2",
    },
    {
      title: "Ubicación 3",
    },
  ],
  placeholder: "Busque la ubicación del domicilio",
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlAutocomplete.default}`,
  },
};
