import { Icon, Input, AutoComplete } from "antd";
import "antd/lib/auto-complete/style/index.css";
import "antd/lib/select/style/index.css";
import styles from "./Autocomplete.module.scss";

/**
 *
 * @param dataSource Receives an array of objects with the possible values for the autocomplete
 * @param placeholder Receives a string with the placeholder for the input
 * @returns A Autocomplete component using AntDesign */

const { Option } = AutoComplete;
export default function Complete({
  dataSource = [],
  onSearch,
  defaultValue,
  placeholder,
  onChange,
  onKeyDown = () => {},
  value,
  ...props
}) {
  const options = dataSource.map((opt, i) => (
    <Option
      key={i}
      value={opt.title}
      className={styles.option}
      title={opt.title}
    >
      {opt.title}
    </Option>
  ));
  return (
    <AutoComplete
      className={styles.autocomplete}
      defaultValue={defaultValue}
      dataSource={options}
      filterOption={(inputValue, option) =>
        ~option.props.children.toUpperCase().indexOf(inputValue.toUpperCase())
      }
      onChange={onChange}
      {...props}
    >
      <Input
        placeholder={placeholder}
        suffix={<Icon onClick={onSearch} type="search" />}
        onKeyDown={onKeyDown}
        value={value}
      />
    </AutoComplete>
  );
}
