import { useState, useCallback } from "react";

export default function useMenu() {
  const [open, setOpen] = useState(false);

  const toggle = useCallback(() => {
    setOpen(!open);
  }, [open]);

  return [open, toggle];
}
