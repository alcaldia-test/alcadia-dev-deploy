import React, { useEffect, useState } from "react";
import Button from "@components/Button";
import {
  CloseOutlined,
  UserOutlined,
  DownOutlined,
  UpOutlined,
  MenuOutlined,
} from "@ant-design/icons";
import Link from "next/link";
// import de imagenes e iconos
import userIcon from "public/icons/user-circle.svg";
import "antd/lib/menu/style/index.css";
import "antd/lib/dropdown/style/index.css";
import useMenu from "./useMenu";
import Modal from "@components/Modals";
import Spin from "@components/Spin";
import { useDispatch, useSelector } from "react-redux";
import InputField from "@components/InputField";
import message from "@components/Message";

import Notification from "../Notification";

import styles from "./topBar.module.scss";
import {
  initialRequestSecretPassStart,
  patchSecretPassStart,
} from "@redux/global/actions";
import {
  registerUserSuccess,
  resendPasswordStart,
  spinLoginVerificated,
} from "@redux/auth/actions";
import Router from "next/router";
import { registerCodeSuccess } from "@redux/verification/actions";

export default function NavigationTopBar({
  isLogged,
  menus,
  logginLink,
  registerLink,
  currentPage = "/",
  handleCurrentPage,
  isAdmin,
  routeName,
}) {
  const dispatch = useDispatch();
  const { dataUser } = useSelector((storage) => storage.auth);
  const { common, global } = useSelector((store) => store);
  const [open, toggle] = useMenu();
  const [selected, setSelected] = useState(currentPage);
  const [modalCreate, setModalCreate] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [keyText, setKeyText] = useState("");
  const [keyTextchange, setKeyTextchange] = useState("");
  const handleSelect = (pageTitle) => {
    handleCurrentPage(pageTitle);
    setSelected(pageTitle);
  };
  useEffect(() => {
    if (dataUser?.roleMapping === "ADMIN") {
      dispatch(initialRequestSecretPassStart());
    }
    setSelected(currentPage);
  }, [currentPage]);

  const handleSecretPhrase = (e) => {
    e.preventDefault();
    setKeyText(global?.secretPass?.secretKey);
    setModalCreate(true);
  };
  const closeModalCreate = () => {
    setModalCreate(false);
  };
  const CopyText = (e) => {
    navigator.clipboard
      .writeText(keyText)
      .then(() => {
        message.success("Se copió el texto");
      })
      .catch((err) => {
        console.log("Something went wrong", err);
      });
  };
  const closeModalUpdate = () => {
    setModalUpdate(false);
  };
  const Updatekey = () => {
    setKeyTextchange(global?.secretPass?.secretKey);
    setModalUpdate(true);
  };
  const changeText = (e) => {
    setKeyTextchange(e.target.value);
  };
  const UpdateKey = (e) => {
    e.preventDefault();
    return new Promise((resolve, reject) => {
      dispatch(
        patchSecretPassStart({
          code: {
            newPassphrase: keyTextchange,
            oldPassphrase: keyText,
          },
          resolve,
          reject,
        })
      );
    })
      .then((res) => {
        message.success(res);
        setModalUpdate(false);
        setKeyText(keyTextchange);
      })
      .catch((res) => {
        message.error(res);
      });
  };
  const viewChange = (e) => {
    console.log(e);
  };

  useEffect(() => {
    if (dataUser?.roleMapping === "USER") {
      if (!dataUser?.emailVerified) resendCode();
      else if (!dataUser?.favorites) {
        dispatch(registerCodeSuccess({ token: dataUser.token, ...dataUser }));
        Router.replace("/verification/zones");
      } else dispatch(spinLoginVerificated({ spinLogin: "hidden" }));
    }
  }, []);

  const resendCode = async () => {
    dispatch(registerUserSuccess(dataUser));
    const identityCard = dataUser.identityCard;

    try {
      await new Promise((resolve, reject) =>
        dispatch(resendPasswordStart({ resolve, reject, identityCard }))
      );

      Router.replace("/verification");
    } catch {
      console.log("No se pudo enviar");
    }
  };

  if (isLogged && isAdmin) {
    return (
      <>
        <Modal visible={modalCreate} onCancel={closeModalCreate}>
          <Spin spinning={common.loader} tip="Actualizando...">
            <div className="my-10 flex flex-col justify-center align-middle items-center	">
              <p
                className={`mt-4 text-center text-2xl font-bold px-2 ${styles.color_green}`}
              >
                Comparta esta palabra secreta
              </p>
              <p
                className={`font-bold text-lg text-center mt-2 ${styles.color_subt}`}
              >
                Esta palabra secreta funciona para que usuarios puedan
                registrarse en el administrador
              </p>
              <div className="flex gap-x-1 items-center	">
                {" "}
                <div className="mt-2 cursor-pointer pointer-events-none	">
                  <InputField
                    rightIcon="/icons/save.svg"
                    type="text"
                    name="secret"
                    defaultValue={keyText}
                    onChange={viewChange}
                  />{" "}
                </div>
                <p
                  className={` text-center font-bold text-lg cursor-pointer mb-0 ${styles.color_green}`}
                  onClick={CopyText}
                >
                  Copiar
                </p>
              </div>
              <p
                className={` text-center font-bold text-lg mt-2 cursor-pointer ${styles.color_blue}`}
                onClick={Updatekey}
              >
                Actualizar palabra secreta
              </p>
            </div>
          </Spin>
        </Modal>
        <Modal visible={modalUpdate} onCancel={closeModalUpdate}>
          <Spin spinning={common.loader} tip="Cargando...">
            <div className="my-10 flex flex-col justify-center align-middle items-center	">
              <p
                className={`mt-4 text-center text-2xl font-bold px-2 ${styles.color_green}`}
              >
                Comparta esta palabra secreta
              </p>
              <p
                className={`font-bold text-lg text-center mt-2 ${styles.color_subt}`}
              >
                Esta palabra secreta funciona para que usuarios puedan
                registrarse en el administrador
              </p>
              <div className="my-4 cursor-pointer">
                <InputField
                  rightIcon="/icons/save.svg"
                  type="text"
                  name="secret"
                  defaultValue={keyTextchange}
                  onChange={changeText}
                />{" "}
              </div>
              <Button onClick={UpdateKey}>Asignar palabra secreta</Button>
            </div>
          </Spin>
        </Modal>
        <div className={styles.navSpacerAdmin}></div>
        <nav className={styles.navContainerAdmin}>
          <div className={styles.navBarAdmin}>
            <h6>{`${routeName}`}</h6>
            <div className={styles.adminControls}>
              <Button type="primary" size="small" onClick={handleSecretPhrase}>
                Compartir palabra secreta
              </Button>
              <Notification />
              <UserOutlined />
              <span className={styles.controlToggle}>administrador</span>
              {open ? (
                <UpOutlined onClick={toggle} />
              ) : (
                <DownOutlined onClick={toggle} />
              )}
            </div>
          </div>
          {open && (
            <div className={styles.adminMenuDropdown}>
              {menus.map((menuItem, i) => {
                return (
                  <span key={i} className={styles.adminMenuItem}>
                    <Link href={menuItem.path}>{menuItem.title}</Link>
                  </span>
                );
              })}
            </div>
          )}
        </nav>
      </>
    );
  }

  return (
    <>
      <div className={styles.navSpacer}></div>
      <nav className={styles.navContainer}>
        <div className={styles.navBar}>
          <Link href="/">
            <a>
              {" "}
              <img src="/images/logo.svg" alt="alcadia la paz logo" />
            </a>
          </Link>
          <div className={styles.fullWidthElementsContainer}>
            <div className={styles.menuItemsContainer}>
              {menus.map((menuItem) => (
                <span
                  key={menuItem.title}
                  onClick={() => handleSelect(menuItem.path)}
                >
                  <Link href={menuItem.path || "#"}>
                    <a
                      className={
                        selected === `${menuItem.path}`
                          ? styles.menuItemSelected
                          : styles.menuItem
                      }
                    >
                      {menuItem.title}
                    </a>
                  </Link>
                </span>
              ))}
            </div>
            <div className={styles.itemsContainer}>
              {isLogged ? (
                <>
                  <Notification />
                  <Link
                    href={"/usuario/[userId]"}
                    as={`/usuario/${dataUser.id}`}
                  >
                    <a>
                      <img
                        alt="icon"
                        className={styles.userIcon}
                        src={dataUser.avatar ?? userIcon}
                      />
                    </a>
                  </Link>
                </>
              ) : (
                <>
                  <Link href={logginLink}>
                    <a>
                      <Button size="small-x" type="primary">
                        Inicia Sesi&oacute;n
                      </Button>
                    </a>
                  </Link>
                  <Link href={registerLink}>
                    <a>
                      <Button size="small-x" type="link">
                        Registrarse
                      </Button>
                    </a>
                  </Link>
                </>
              )}
            </div>
          </div>
          <div className={styles.elementsContainer}>
            {!isLogged ? (
              <Link href={logginLink}>
                <a>
                  <Button size="small" type="primary">
                    Iniciar Sesi&oacute;n
                  </Button>
                </a>
              </Link>
            ) : (
              <div className={styles.statusIcons}>
                <Notification />
                <Link href={"/usuario/[userId]"} as={`/usuario/${dataUser.id}`}>
                  <a>
                    <img
                      alt="icon"
                      className={styles.userIcon}
                      src={dataUser.avatar || userIcon}
                    />
                  </a>
                </Link>
              </div>
            )}
            <div className={styles.ll}>
              {!open ? (
                <span className={styles.menuButton} onClick={toggle}>
                  Men&uacute; <MenuOutlined />
                </span>
              ) : (
                <span className={styles.menuButton} onClick={toggle}>
                  Cerrar <CloseOutlined />
                </span>
              )}
              <div
                className={open ? styles.menuElContainer : styles.menuElhidden}
              >
                {menus.map((menuItem, i) => {
                  return (
                    <span key={i}>
                      <Link href={menuItem.path}>{menuItem.title}</Link>
                    </span>
                  );
                })}
                {isLogged ? (
                  ""
                ) : (
                  <>
                    <hr />
                    <span className={styles.menuLogginButton}>
                      <Link href={"/login"}>
                        Registrate o inicia Sesi&oacute;n
                      </Link>
                    </span>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}
