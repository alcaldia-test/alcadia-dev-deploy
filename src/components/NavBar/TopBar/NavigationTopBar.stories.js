import React from "react";
import { withDesign } from "storybook-addon-designs";

import NavigationTopBar from "./index";

export default {
  title: "Components/navigation/topBar",
  component: NavigationTopBar,
  decorators: [withDesign],
};

const Template = (args) => <NavigationTopBar {...args} />;

export const Default = Template.bind({});

Default.args = {
  isLogged: true,
  userLink: "#",
  notificationLink: "#",
  logginLink: "/loggin",
  registerLink: "/register",
  menus: [
    {
      title: "Inicio",
      path: "/",
    },
    {
      title: "Tu Voto Cuenta",
      path: "/tu_voto_cuenta",
    },
    {
      title: "Propuestas ciudadanas",
      path: "/propuestas",
    },
    {
      title: "Charlas Virtuales",
      path: "/debates",
    },
    {
      title: "Normativas Colaborativa",
      path: "/normativas",
    },
  ],
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A12587",
  },
};

export const notLoggedIn = Template.bind({});

notLoggedIn.args = {
  userLink: "#",
  notificationLink: "#",
  logginLink: "/loggin",
  registerLink: "/register",
  menus: [
    {
      title: "Inicio",
      path: "/",
    },
    {
      title: "Tu Voto Cuenta",
      path: "/tu_voto_cuenta",
    },
    {
      title: "Propuestas ciudadanas",
      path: "/propuestas",
    },
    {
      title: "Charlas Virtuales",
      path: "/debates",
    },
    {
      title: "Normativas Colaborativa",
      path: "/normativas",
    },
  ],
};

notLoggedIn.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=16%3A12587",
  },
};
