import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import { useDispatch, useSelector, batch } from "react-redux";
import {
  getNotificationStart,
  emptyNotification,
  deleteNotificationStart,
  decreaseUserNotify,
} from "@redux/global/actions";
import Popover from "@components/Popover";
import Badge from "@components/Badge";
import { Notification } from "@assets/icons";
import message from "@components/Message";
import Spin from "@components/Spin";

import styles from "./notification.module.scss";
import DetailsModal from "./DetailsModal";

const NotificationData = ({ hanldleClicked }) => {
  const dispatch = useDispatch();
  const {
    userNotifications,
    notifications: { client },
  } = useSelector((storage) => storage.global);
  const [loading, setLoading] = useState(false);
  let [page, setPage] = useState(1);

  const defaultAvt = "https://dummyimage.com/150/198565/fff&text=V";

  useEffect(() => {
    if (client > 0) getNotifications();

    return () => dispatch(emptyNotification());
  }, []);

  const handleDelete = async (nid) => {
    batch(() => {
      dispatch(emptyNotification());
      dispatch(decreaseUserNotify());
    });

    try {
      setLoading(true);

      await new Promise((resolve, reject) =>
        dispatch(deleteNotificationStart({ id: nid, resolve, reject }))
      );
    } catch (error) {
      console.log(error);
      message.error("Problemas al obtener comentarios.");
    }

    setPage(1);
    setLoading(false);
  };

  const getNotifications = async () => {
    try {
      setLoading(true);

      let addition = 0;
      if (page !== 1) addition = 1;

      const filter = {
        limit: page === 1 ? 4 : 3,
        skip: (page - 1) * 3 + addition,
      };

      await new Promise((resolve, reject) =>
        dispatch(getNotificationStart({ ...filter, resolve, reject }))
      );
    } catch (error) {
      console.log(error);
      message.error("Problemas al obtener comentarios.");
    }

    setLoading(false);
  };

  const handleMoreNotifies = async () => {
    setPage(++page);
    await getNotifications();
  };

  return (
    <div className={styles["notification-box"]}>
      {userNotifications.map(
        (not, i) =>
          i !== page * 3 && (
            <a key={i} onClick={hanldleClicked.bind({}, not)}>
              <span
                onClick={async (e) => {
                  e.stopPropagation();
                  await handleDelete(not.id);
                }}
                role="button"
              >
                ✕
              </span>
              <img src={not.avatar ?? defaultAvt} />
              <div>
                <h6 title={not?.title ?? "Sin Titulo"}>
                  {not?.title ?? "Sin Titulo"}
                </h6>
                <span title={not?.subject ?? "Sin contendio"}>
                  {not?.subject ?? "Sin contendio"}
                </span>
              </div>
            </a>
          )
      )}
      {userNotifications.length === 0 && !loading && (
        <div style={{ textAlign: "center" }} className="py-3">
          Sin notificaciones....
        </div>
      )}
      {loading && (
        <div style={{ textAlign: "center" }} className="py-3">
          <Spin tip="Procesando..." size="large"></Spin>
        </div>
      )}
      {userNotifications.length > page * 3 && (
        <p onClick={handleMoreNotifies}>Ver Mas</p>
      )}
    </div>
  );
};

const Notify = () => {
  const [openNotifiction, setOpenNotifiction] = useState(false);
  const {
    notifications: { client },
  } = useSelector((storage) => storage.global);
  const [current, setCurrent] = useState();
  const router = useRouter();

  const hanldleClicked = (not) => {
    setOpenNotifiction(false);

    if (not?.type === "comment" || not?.type === "answer")
      setCurrent({ type: not.type, commentId: not?.controller });
    else router.push(not.controller);

    console.log(not);
  };

  return (
    <>
      {current && <DetailsModal {...current} onCancel={setCurrent} />}
      <Popover
        overlayClassName="notification-popover"
        content={
          openNotifiction && (
            <NotificationData hanldleClicked={hanldleClicked} />
          )
        }
        placement="bottomLeft"
        trigger="click"
        visible={openNotifiction}
        onVisibleChange={(vis) => setOpenNotifiction(vis)}
      >
        <a className="mx-3 cursor-pointer" style={{ lineHeight: 1 }}>
          <Badge overflowCount={9} count={client}>
            <Notification />
          </Badge>
        </a>
      </Popover>
    </>
  );
};

export default Notify;
