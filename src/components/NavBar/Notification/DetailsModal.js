import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "@components/AntModal";
import { getSingleComment } from "@redux/complaints/actions";
import message from "@components/Message";
import Spin from "@components/Spin";

import Comment from "./Comment";

function DetailsModal({ commentId, type, onCancel, setLoading: extLoading }) {
  const dispatch = useDispatch();
  const [comment, setComment] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    startFetching();
  }, []);

  const startFetching = async () => {
    try {
      setLoading(true);

      const filterObj = {
        id: commentId ?? "bca29540-a896-40ff-afef-d36328d4fa6b",
        type: type ?? "comment",
      };

      const comment = await new Promise((resolve, reject) =>
        dispatch(getSingleComment({ ...filterObj, resolve, reject }))
      );

      const allInOne = { ...comment, ...comment.user };

      delete allInOne?.user;

      setComment(allInOne);
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };

  return (
    <Modal
      className="simple-modal"
      style={{ borderRadius: 20 }}
      footer={false}
      visible={true}
      onCancel={() => onCancel(undefined)}
    >
      <h4 className="text-center text-2xl my-2">Comentario Quitado</h4>
      <Spin spinning={loading} size="large">
        <Comment user={comment} />
      </Spin>
    </Modal>
  );
}

export default DetailsModal;
