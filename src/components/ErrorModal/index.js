import React from "react";
import ImageNext from "next/image";
import Styles from "./errorModal.module.scss";
import ButtomComponent from "@components/Button";

const index = ({
  closeModal = () => {},
  Message = "Error",
  delteFuntion = () => {},
}) => {
  return (
    <>
      <div
        className={Styles.modalProposalBackground}
        onClick={() => {
          closeModal();
        }}
      />
      <div className={Styles.modalProposalViewMoreDetail}>
        {/* Body */}
        <div className={Styles.errorBodyScrol}>
          <div className="justify-center flex">
            <ImageNext
              src={"/icons/error-modal.svg"}
              layout="fixed"
              width={85}
              height={85}
              alt="xboxIcon"
            />
          </div>
          <p className="p-2 text-justify mt-2">
            Lamentablemente se reporta el siguiente error:
          </p>
          {Array.isArray(Message) ? (
            <>
              {Message.map((x, i) => (
                <div className="font-bold text-center p-1 text-red" key={i}>
                  {x}
                </div>
              ))}
            </>
          ) : (
            <p className="px-2 text-justify">
              <span className="font-bold text-red"> {Message} </span>.
            </p>
          )}
        </div>
        {/* Buttons */}
        <div className="flex items-center px-3 justify-center">
          <ButtomComponent
            size="small-x"
            type="link"
            onClick={() => {
              closeModal();
            }}
          >
            Cerrar
          </ButtomComponent>
        </div>
      </div>
    </>
  );
};

export default index;
