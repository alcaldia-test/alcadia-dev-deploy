import React from "react";
import { Pagination as AntdPagination } from "antd";
import "antd/lib/pagination/style/index.css";

import styles from "./Pagination.module.scss";

function Pagination({ className, ...props }) {
  return (
    <AntdPagination
      className={`${styles.pagination} ${className}`}
      {...props}
    />
  );
}

export default Pagination;
