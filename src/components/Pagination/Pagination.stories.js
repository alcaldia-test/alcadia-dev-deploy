import React from "react";
import { withDesign } from "storybook-addon-designs";
import Pagination from "./index";

export default {
  title: "Components/Pagination",
  component: Pagination,
  decorators: [withDesign],
};

const Template = (args) => <Pagination {...args} />;

export const Default = Template.bind({});
Default.args = {
  total: 500,
};

Default.parameters = {
  desing: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=10%3A5172",
  },
};
