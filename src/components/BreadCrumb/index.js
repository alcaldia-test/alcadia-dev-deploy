import React from "react";
import { Breadcrumb } from "antd";
import "antd/lib/breadcrumb/style/index.css";
import styles from "./BreadCrumb.module.scss";

import { textCapitalize } from "@utils/capitalizeFirstLetter";

function BreadCrumb({ pages }) {
  return (
    <div>
      <Breadcrumb>
        {pages?.map((p) => (
          <Breadcrumb.Item
            className={styles.breadcrumbItem}
            href={p.path}
            key={p.name}
          >
            {textCapitalize(p.name)}
          </Breadcrumb.Item>
        ))}
      </Breadcrumb>
    </div>
  );
}

export default BreadCrumb;
