import React from "react";
import { withDesign } from "storybook-addon-designs";
import Breadcrumb from "./index";

export default {
  title: "Components/BreadCrumb",
  component: Breadcrumb,
  decorators: [withDesign],
  argTypes: {
    pages: "array",
  },
};

const Template = (args) => <Breadcrumb {...args} />;

export const Default = Template.bind({});
Default.args = {
  pages: [
    {
      path: "#",
      name: "propuestas publicadas",
    },
    {
      path: "#",
      name: "propuesta de usuario",
    },
    {
      path: "#",
      name: "titulo de la propuesta",
    },
  ],
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/?node-id=12%3A6027",
  },
};
