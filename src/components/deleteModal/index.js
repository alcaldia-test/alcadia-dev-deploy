import React, { useState } from "react";
import Styles from "./deleteModal.module.scss";
import ButtomComponent from "@components/Button";
import Image from "next/image";
import { CloseOutlined } from "@ant-design/icons";

const index = ({
  closeModal = () => {},
  onlyClose = () => closeModal(),
  mainMessage = "¿Está seguro que desea borrar?",
  title = "",
  delteFuntion = () => {},
  simple = false,
  mainIconSrc = "/icons/Elim.svg",
}) => {
  const [titleConfirm, setTitleConfirm] = useState("");
  const [error, setError] = useState(false);

  const handleOnChange = (e) => {
    setTitleConfirm(e.target.value);
    setError(false);
  };

  const handleDelete = () => {
    if (title.trim() === titleConfirm.trim()) {
      delteFuntion();
    } else {
      setError(true);
    }
  };

  return (
    <>
      <div
        className={Styles.modalProposalBackground}
        onClick={() => {
          onlyClose();
        }}
      />
      {simple ? (
        <div className={Styles.modalProposalViewMoreDetailSimple}>
          <div className={Styles.close} onClick={() => onlyClose()}>
            <CloseOutlined />
          </div>
          <div className="flex justify-center items-center">
            <Image
              width={100}
              height={100}
              src={mainIconSrc}
              className={Styles.elimIcon}
              alt="elim"
            />
          </div>
          <div className="flex items-center justify-center text-H6 text-stateErrorAdvice pl-3">
            {mainMessage}
          </div>
          <span className="flex justify-center ">{title}</span>
          <div className="flex justify-around items-center pb-4">
            <ButtomComponent
              size="small-x"
              type="link"
              onClick={() => {
                closeModal();
              }}
            >
              No
            </ButtomComponent>
            <ButtomComponent size="small-x" onClick={delteFuntion}>
              Sí
            </ButtomComponent>
          </div>
        </div>
      ) : (
        <div className={Styles.modalProposalViewMoreDetail}>
          <div className={Styles.close} onClick={() => onlyClose()}>
            <CloseOutlined />
          </div>
          {/* Header */}
          <div className="border-b-2 border-greyheader flex justify-between">
            <span className="p-2">
              ¿Deseas borrar
              <span className="font-bold"> {title} </span>?
            </span>
          </div>
          {/* Body */}
          <div className="">
            <p className="p-2 text-justify mt-2">
              Esta acción borrará completamente la información del servidor
              asociada a <span className="font-bold"> {title} </span> y sus
              adjuntos, no se podrá deshacer esta acción.
            </p>
            <p className="px-2 text-justify mt-4">
              Para confirmar por favor escribe {'"'}
              <span className="font-bold">{title}</span>
              {'"'}
            </p>
            <div
              className={
                error ? Styles.externalInputError : Styles.externalInput
              }
            >
              <input
                className={Styles.inputnone}
                value={titleConfirm}
                onChange={handleOnChange}
              />
            </div>
          </div>
          {/* Buttons */}
          <div className="border-t-2 border-greyheader flex items-center px-3 justify-between">
            <ButtomComponent
              size="small-x"
              type="link"
              onClick={() => {
                closeModal();
              }}
            >
              Cancelar
            </ButtomComponent>
            <ButtomComponent size="small-x" onClick={handleDelete}>
              Borrar
            </ButtomComponent>
          </div>
        </div>
      )}
    </>
  );
};

export default index;
