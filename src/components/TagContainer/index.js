import React, { useState, useEffect } from "react";
import { AutoComplete } from "antd";
import Styles from "./tag.module.scss";
import "antd/lib/select/style/index.css";

const { Option } = AutoComplete;

const _options = [
  "Ambiente",
  "Política",
  "Cultura",
  "Inclusión Social",
  "Espacios públicos",
  "Seguridad",
  "Movilidad",
  "Territorio",
  "Infraestructura",
  "Desarrollo",
];

const Index = ({
  options = _options,
  selectedtag = [],
  onSelect = () => {},
}) => {
  const [remainingOptions, setRemainingOptions] = useState(options);

  const [selectedTags, setSelectedTags] = useState(selectedtag);

  useEffect(() => {
    if (onSelect) {
      onSelect(selectedTags);
    }
  }, [selectedTags]);

  useEffect(() => {
    setSelectedTags(selectedtag);
    const _remainingOptions = [];
    _options.forEach((x) => {
      if (!selectedtag.includes(x)) {
        _remainingOptions.push(x);
      }
    });
    setRemainingOptions(_remainingOptions);
  }, []);

  const handleOnSelectFromAutoComplete = (tag) => {
    const exitedtags = selectedTags.filter((x) => x !== tag);
    setSelectedTags([...exitedtags, tag]);
    const _remainingOptions = remainingOptions.filter((x) => x !== tag);
    setRemainingOptions(_remainingOptions);
  };

  const handleOnDeleteTag = (tag) => {
    const exitedtags = selectedTags.filter((x) => x !== tag);
    setSelectedTags([...exitedtags]);
    setRemainingOptions([...remainingOptions, tag]);
  };

  return (
    <div className={Styles.rootContainer}>
      {selectedTags.map((x, i) => (
        <TagCustom key={i} title={x} onDeleteTag={handleOnDeleteTag} />
      ))}
      <AutoCompleteCustom
        options={remainingOptions}
        onSelect={handleOnSelectFromAutoComplete}
      />
    </div>
  );
};

export default Index;

const TagCustom = ({ title, onDeleteTag }) => {
  const [isMouseHover, setIsMouseHover] = useState(false);

  const handleOnRemovetag = () => {
    if (onDeleteTag) {
      onDeleteTag(title);
    }
  };

  return (
    <div
      className={Styles.tagContainer}
      onMouseOver={() => setIsMouseHover(true)}
      onMouseLeave={() => setIsMouseHover(false)}
    >
      {isMouseHover && (
        <img
          alt="tagIcon"
          src="/icons/delete.svg"
          style={{ width: 17, height: 17 }}
          onClick={handleOnRemovetag}
        />
      )}
      <span>{title}</span>
      <img
        src="/icons/tags.svg"
        style={{ width: 17, height: 17 }}
        alt="tagIcon"
      />
    </div>
  );
};

const AutoCompleteCustom = ({ options, onSelect = () => {} }) => {
  const [onInputValue, setOnInputValue] = useState("");
  const [filteredOptions, setFilteredOptions] = useState(options);

  useEffect(() => {
    setFilteredOptions(options);
  }, [options]);

  const onSelectOption = (data) => {
    onSelect(data);
    setOnInputValue("");
  };

  const onChange = (data) => {
    const _data = _options?.filter((x) =>
      x.toLowerCase().includes(data.toLowerCase())
    );
    setFilteredOptions(_data);
    setOnInputValue(data);
  };

  const OnKeyPress = (event) => {
    if (event.key === "Enter") {
      onSelect(onInputValue);
      setOnInputValue("");
    }
  };

  return (
    <span onKeyPress={OnKeyPress}>
      <AutoComplete
        className={Styles.autocompleteContainer}
        onSelect={onSelectOption}
        onChange={onChange}
        placeholder="Añadir nueva etiqueta +"
        value={onInputValue}
      >
        {filteredOptions.sort().map((x, i) => (
          <Option key={i} value={x}>
            {x}
          </Option>
        ))}
      </AutoComplete>
    </span>
  );
};
