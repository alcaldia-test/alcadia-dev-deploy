import React from "react";
import { withDesign } from "storybook-addon-designs";
import Dropdown from "./index";

export default {
  title: "Components/Dropdown",
  component: Dropdown,
  decorators: [withDesign],
  argTypes: {
    items: "array",
    children: { ReactNode: "element" },
    trigger: { options: ["click", "hover"], control: "select" },
  },
};

const Template = (args) => <Dropdown {...args} />;

export const Default = Template.bind({});

Default.args = {
  items: [
    { id: "1", name: "Option 1" },
    { id: "2", name: "Option 2" },
    { id: "2", name: "Option 3" },
  ],
  children: <a>Selecciona la opcion</a>,
  trigger: "click",
};

Default.parameters = {
  design: {
    type: "figma",
    url: "https://www.figma.com/file/gvosqCTnzstry2TQ6ZgMcD/Alcald%C3%ADa-%2F-4.-Mobile-Product?node-id=166%3A20671",
  },
};
