import React from "react";
import { Menu, Dropdown as DropdownAntd } from "antd";
import "antd/lib/dropdown/style/index.css";
import Styles from "./Dropdown.module.scss";

function Dropdown({ items, trigger, children, ...args }) {
  return (
    <DropdownAntd
      overlay={
        <Menu>
          {items?.map((item) => {
            return (
              <Menu.Item key={item.id} className={Styles.dropdown}>
                <a rel="noopener noreferrer">{item.name}</a>
              </Menu.Item>
            );
          })}
        </Menu>
      }
      trigger={trigger}
    >
      <div className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
        {children}
      </div>
    </DropdownAntd>
  );
}

export default Dropdown;
