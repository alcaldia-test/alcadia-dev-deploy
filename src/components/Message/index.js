import { message } from "antd";
import "antd/lib/message/style/index.css";

message.config({
  top: 100,
  duration: 10,
});

export default message;

export const textError = (prefix, codeError) => {
  console.error("Receiving en textError", { codeError });
  switch (parseInt(codeError)) {
    case 404:
      return `${prefix} no encontrada`;
    case 406:
      return `${prefix} no permite acción duplicada`;

    default:
      return `Error al ejecutar su petición`;
  }
};
