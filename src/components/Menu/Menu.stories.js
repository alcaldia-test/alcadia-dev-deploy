import { withDesign } from "storybook-addon-designs";
import Menu from "./index";

const urlComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idMenu = {
  default: "10%3A6939",
};

export default {
  title: "Components/Menu",
  component: Menu,
  decorators: [withDesign],
  argTypes: {
    collapsed: {
      control: {
        type: "boolean",
      },
    },
    type: {
      control: {
        type: "select",
        options: ["normatives", "votations", "statistics", "admin"],
      },
    },
    defaultSelectedKey: {
      control: {
        type: "select",
        options: [
          ["1"],
          ["2"],
          ["3"],
          ["4"],
          ["5"],
          ["6"],
          ["7"],
          ["8"],
          ["9"],
          ["10"],
          ["11"],
          ["12"],
        ],
      },
    },
  },
};

const Template = (args) => <Menu {...args} />;

export const Default = Template.bind({});

Default.args = {
  collapsed: false,
  type: "admin",
  notifications: {
    calls: 1,
    talks: 1,
    regulations: 1,
    requests: 1,
  },
  defaultSelectedKey: "1",
};

Default.parameters = {
  design: {
    type: "figma",
    url: `${urlComponents}${idMenu.default}`,
  },
};
