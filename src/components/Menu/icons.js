import {
  QuillPen,
  GroupTwo,
  ThumbsUp,
  Message,
  Forbid,
  UserSettings,
  User,
  LogOut,
  Settings,
} from "@assets/icons";

export default {
  announcements: <GroupTwo />,
  normatives: <QuillPen />,
  voting: <ThumbsUp />,
  discussions: <Message />,
  complaints: <Forbid />,
  "access-controls": <UserSettings />,
  sites: <Settings />,
  users: <User />,
  logout: <LogOut />,
};
