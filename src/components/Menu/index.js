import React, { useCallback, useState } from "react";
import { Layout, Menu } from "antd";
import { Hamburger } from "@assets/icons";

import icons from "./icons";
import { getUserSidebar } from "@utils/generateSidebar";
import { useSelector, useDispatch } from "react-redux";

import "antd/lib/menu/style/index.css";
import "antd/lib/tooltip/style/index.css";
import styles from "./Menu.module.scss";
import Link from "next/link";
import { useRouter } from "next/router";
import { signOutStart } from "@redux/auth/actions";
import confirm from "@components/Modals/confirm";

const { Sider } = Layout;

const SubMenu = Menu.SubMenu;
const Item = Menu.Item;

export default function MenuComponent({ collapsed, defaultSelectedKey }) {
  const [collapse, setCollapse] = useState(collapsed);
  const router = useRouter();
  const dispatch = useDispatch();
  const {
    global: { notifications },
    auth: { dataUser },
  } = useSelector((storage) => storage);

  const sitebar = useCallback(getUserSidebar(dataUser?.permissions ?? []), [
    dataUser,
  ]);

  const handleLogout = async () => {
    const result = await confirm(
      "¿Seguro(a) en salir de administración?",
      true
    );
    if (!result) return;

    dispatch(signOutStart());
    router.replace(`/ingreso`);
  };

  return (
    <Sider
      trigger={null}
      collapsible
      collapsed={collapse}
      className={`${styles.wrapper} ${collapse ? styles.collapsed : ""}`}
    >
      <Menu
        defaultSelectedKeys={defaultSelectedKey}
        mode="inline"
        className={styles.menu}
      >
        <div className={styles.menu__logo}>
          <div
            onClick={() => setCollapse(!collapse)}
            className={styles.menu__logo__hamburger}
          >
            <Hamburger />
          </div>
          <div className={styles.menu__logo__image}>
            <img src="/images/logo.svg" alt="Logo" />
            <h6>La Paz Decide</h6>
          </div>
        </div>
        {/* -----------start Items--------------- */}
        {sitebar.map((item) => {
          let resp = <></>;

          if (item instanceof Array) {
            const first = item[0];
            const afterFirst = item.slice(1);

            resp = (
              <SubMenu
                key={first.key}
                className={styles.subMenu}
                title={
                  <span>
                    {icons[first?.key]}
                    <div>{first?.label}</div>
                  </span>
                }
              >
                {afterFirst.map((sub) => (
                  <Item key={sub.key} defaultChecked={false}>
                    <Link href={sub.path}>
                      <a>
                        <div>{sub.label}</div>
                      </a>
                    </Link>
                  </Item>
                ))}
              </SubMenu>
            );
          } else
            resp = (
              <Item key={item.key}>
                <Link href={item.path}>
                  <a>
                    <div
                      className={`
                        anticon
                        ${styles.notification__container}
                        ${notifications[item.key] && styles.notification}
                      `}
                    >
                      {icons[item.key]}
                    </div>
                    <div>{item.label}</div>
                  </a>
                </Link>
              </Item>
            );

          return resp;
        })}
        <Item key="logout" onClick={handleLogout}>
          <a>
            <div
              className={`
              anticon
              ${styles.notification__container}
            `}
            >
              {icons.logout}
            </div>
            <div>Cerrar Sesión</div>
          </a>
        </Item>
      </Menu>
    </Sider>
  );
}
