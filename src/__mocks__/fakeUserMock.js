export const dataUser = {
  tokenUser: "123456",
  dataUser: {
    userName: "verojacin",
    id: "351ba8-5cc-47ee-8c0-42397cd45483",
    email: "verojacin@gmail.com",
    location: "Miraflores",
  },
};

const action = {
  payload: {
    voteId: "2c57b0ba-4348-4778-89a0-08ea8b5dd315",
    userId: "351ba8-5cc-47ee-8c0-42397cd45483",
  },
};

export default action;
