export const CATEGORIES_IMAGES = [
  {
    label: "Phone",
    value: "phone",
  },
  {
    label: "Tablet",
    value: "tablet",
  },
  {
    label: "Desktop",
    value: "desktop",
  },
  {
    label: "Tv",
    value: "tv",
  },
];

export const defaultValues = {
  blockRepeats: 1,
  exerciseRepeats: 10,
  exerciseType: true, // True is repetitions, False is time
};
