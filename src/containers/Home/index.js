import React from "react";

import { customUseReducer } from "@utils/customHooks";

import { useSelector } from "react-redux";

const initialState = {
  loading: false,
  modal: false,
  selected: {},
};
export function IndexPage() {
  // eslint-disable-next-line no-unused-vars
  const [state, dispatchComponent] = customUseReducer(initialState);
  const { auth } = useSelector((store) => store);
  return (
    <>
      <div>
        <div className="gx-flex-column gx-justify-content-center gx-align-items-center">
          <img
            src="/images/logo.svg"
            height="40"
            style={{ marginBottom: 16 }}
          />
          <hr />
          <h1>Bienvenido al dashboard de Alcaldia</h1>
          <h1>
            <b>{`${auth.dataUser.name} ${auth.dataUser.lastName}`}</b>
          </h1>
        </div>
      </div>
    </>
  );
}

IndexPage.propTypes = {};

export default IndexPage;
