import { useState, useEffect, useRef } from "react";
import draftToHtml from "draftjs-to-html";
import Link from "next/link";
import moment from "moment";
import ReactPlayer from "react-player";

import { useSelector, useDispatch } from "react-redux";
import {
  getFavorites,
  postProposalSupport,
} from "@redux/announcements/actions";
import { validateAnnouncementState } from "@utils/announcementStates";
import { validateIfUserIsInZone } from "@utils/zoneValidator";

import Avatar from "@components/Avatar";
import Button from "@components/Button";
import Comments from "@components/Comments";
import IconGroup from "@components/SocialNetworks/IconGroup";
import Modal from "@components/Modals/SupportModal";
import SimpleModal from "@components/Modals/SimpleModal";
import Map from "@components/Map";
import Spin from "@components/Spin";

import {
  ArrowLeft,
  Comment,
  Check,
  DisLike,
  Like,
  Ubication,
  Warning,
  Info,
} from "@assets/icons";

import styles from "./Proposal.module.scss";

require("moment/locale/es.js");

const defaultProfile = {
  image: "/images/avatar.png",
  name: "Nombre",
};

export default function Proposal({ id }) {
  const [reactionModal, setReactionModal] = useState(false);
  const [supportModal, setSupportModal] = useState(true);
  const [okText, setOkText] = useState("Si, la apoyo");
  const [mapModal, setMapModal] = useState(false);

  const dispatch = useDispatch();
  const { proposal, waiting, favorites } = useSelector(
    (state) => state.announcements
  );

  const {
    receptionStart,
    receptionEnd,
    reviewStart,
    reviewEnd,
    supportingStart,
    supportingEnd,
    postResults,
    directPosts,
  } = proposal?.announcement || {};
  const {
    isDirectReception = false,
    waitingSupport = false,
    isSupporting = false,
    isEvaluating = false,
    isFinished = false,
  } = validateAnnouncementState({
    receptionStart,
    receptionEnd,
    reviewStart,
    reviewEnd,
    supportingStart,
    supportingEnd,
    postResults,
    directPosts,
  });

  const { dataUser } = useSelector((storage) => storage.auth);
  const [description, setDescription] = useState({});
  const [ncomments, setNcomments] = useState(0);
  const [likesPercent, setLikesPercent] = useState(0);
  const [dislikesPercent, setDislikesPercent] = useState(0);
  const [url, setUrl] = useState("");

  const isInVotingZone = validateIfUserIsInZone({
    favorites,
    zones: proposal?.zones,
    macros: proposal?.macros,
  });

  const fixedRef = useRef(null);

  useEffect(() => {
    setUrl(location.href);

    const windowWidth = window.innerWidth;
    if (windowWidth < 1024) {
      window.addEventListener("scroll", handleScroll);
    }
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    setNcomments(proposal?.comments);
  }, [proposal]);

  useEffect(() => {
    dispatch(getFavorites(dataUser.id));
  }, []);

  const supportProposal = (payload) => {
    dispatch(postProposalSupport({ ...payload, proposalId: id }));
  };

  useEffect(() => {
    if (!proposal?.content) return;
    try {
      const description = JSON.parse(proposal?.content);
      setDescription(description);
    } catch (error) {
      setDescription(proposal?.content);
    }

    const likes = proposal?.likes;
    const dislikes = proposal?.dislikes;
    const total = likes + dislikes;
    const likesPercent = (likes * 100) / total;
    const dislikesPercent = (dislikes * 100) / total;
    if (total === 0) {
      setLikesPercent(0);
      setDislikesPercent(0);
    } else {
      setLikesPercent(
        Number.isInteger(likesPercent) ? likesPercent : likesPercent.toFixed(2)
      );
      setDislikesPercent(
        Number.isInteger(dislikesPercent)
          ? dislikesPercent
          : dislikesPercent.toFixed(2)
      );
    }
  }, [proposal]);

  const handleScroll = () => {
    const footer = document.getElementById("footer");
    if (footer && fixedRef.current) {
      const footerPosition = footer.getBoundingClientRect().top;
      const windowHeight = window.innerHeight;

      if (footerPosition < windowHeight) {
        fixedRef.current.style.bottom = "-100%";
      } else {
        fixedRef.current.style.bottom = "0";
      }
    }
  };

  return (
    <>
      <Spin spinning={waiting} tip={`Enviando voto...`} size="large">
        <div className={styles.background}>
          <div className={styles.container}>
            <div className={styles.content}>
              <Link
                href="/propuestas_ciudadanas/[announcementId]"
                as={`/propuestas_ciudadanas/${proposal?.announcementId}`}
              >
                <a className={styles.back}>
                  <ArrowLeft /> Otras propuestas
                </a>
              </Link>
              {proposal?.state === "WINNER" && (
                <div className={styles.winner}>
                  <Check /> Esta propuesta fue una de las ganadoras
                </div>
              )}
              <h1 className={styles.title}>{proposal?.title}</h1>
              <div className={styles.social}>
                <div className={styles.user}>
                  <div className={styles.user__details}>
                    <Link
                      href="/usuario/[id]"
                      as={`/usuario/${proposal?.creator?.id}`}
                    >
                      <a>
                        <Avatar
                          src={
                            proposal?.creator?.avatar || defaultProfile.image
                          }
                          size="medium"
                          className={styles.card__profile_avatar}
                        />
                      </a>
                    </Link>
                    <p>
                      {proposal?.creator?.firstName || defaultProfile.name}{" "}
                      <span>
                        {moment(proposal?.createdAt)
                          .locale("es")
                          .format("MMM D")}
                      </span>
                    </p>
                  </div>
                </div>
                <div className={styles.share}>
                  <a href="#comments" className={styles.comments__button}>
                    <Comment />
                    {ncomments}
                  </a>
                  <div className={styles.share__button}>
                    <IconGroup
                      href={url}
                      userId={dataUser.id}
                      proposalId={id}
                    />
                  </div>
                </div>
              </div>
              {proposal?.fileLink && (
                <img
                  src={proposal?.fileLink}
                  alt={proposal?.title}
                  className={styles.image}
                />
              )}
              {proposal?.videoLink && (
                <div className={styles.video__container}>
                  <ReactPlayer
                    url={proposal?.videoLink}
                    playing={false}
                    width="100%"
                    height="100%"
                    style={{
                      position: "absolute",
                      top: 0,
                      left: 0,
                      borderRadius: "15px",
                      overflow: "hidden",
                    }}
                    controls
                  />
                </div>
              )}
              {proposal?.location && (
                <Button
                  type="secondary"
                  size="small"
                  block={true}
                  className={styles.button__ubication}
                  onClick={() => setMapModal(true)}
                >
                  Ver ubicación exacta <Ubication />
                </Button>
              )}
              <div className={styles.description}>
                {typeof description === "object" ? (
                  <div
                    dangerouslySetInnerHTML={{
                      __html: draftToHtml(description),
                    }}
                  />
                ) : (
                  description
                )}
              </div>
              <div className={styles.share}>
                <a href="#comments" className={styles.comments__button}>
                  <Comment />
                  {ncomments}
                </a>
                <div className={styles.share__button}>
                  <IconGroup href={url} userId={dataUser.id} proposalId={id} />
                </div>
              </div>
              {isEvaluating && (
                <p className={styles.evaluating}>
                  <Info />
                  Se están evaluando los apoyos de esta propuesta
                </p>
              )}
              {isFinished && (
                <div className={styles.result}>
                  <p className={styles.result__likes}>
                    <Like />
                    <span>
                      {proposal?.likes} - {likesPercent}%
                    </span>
                  </p>
                  <p className={styles.result__dislikes}>
                    <DisLike />
                    <span>
                      {proposal?.dislikes} - {dislikesPercent}%
                    </span>
                  </p>
                </div>
              )}
              <div id="comments" className={styles.comments}>
                <Comments
                  ncomments={ncomments}
                  module="proposal"
                  updateComments={setNcomments}
                  mid={proposal?.id}
                />
              </div>
            </div>
            {isEvaluating || isFinished ? (
              <div ref={fixedRef} className={styles.fixed__component_finished}>
                ¡Déjanos tu opinión en los comentarios!
              </div>
            ) : (
              <div ref={fixedRef} className={styles.fixed__component}>
                <span className={styles.fixed__component__text}>
                  {(isDirectReception || isSupporting) &&
                    "Los resultados se publicarán" +
                      moment(postResults)
                        .endOf("day")
                        .fromNow()
                        .replace("en", " en: ")}
                  {waitingSupport &&
                    "El periodo de apoyo comienza" +
                      moment(supportingStart)
                        .startOf("day")
                        .fromNow()
                        .replace("en", " en: ")}
                </span>
                {!isInVotingZone && dataUser.id && (
                  <p className={styles.not_in_zone}>
                    <Warning /> No perteneces a ninguna de las zonas
                  </p>
                )}
                {proposal?.reaction === ("any" || "omit" || "") &&
                  (dataUser.id ? (
                    <div className={styles.buttons}>
                      <Button
                        type="secondary"
                        size="super-small"
                        onClick={() => {
                          setReactionModal(true);
                          setSupportModal(true);
                          setOkText("Si, la apoyo");
                        }}
                        block={true}
                        disabled={
                          !isInVotingZone ||
                          waitingSupport ||
                          (!isDirectReception && !isSupporting)
                        }
                      >
                        <Like />
                        La apoyo
                      </Button>
                      <Button
                        type="secondary"
                        size="super-small"
                        onClick={() => {
                          setReactionModal(true);
                          setSupportModal(false);
                          setOkText("No la apoyo");
                        }}
                        block={true}
                        disabled={
                          !isInVotingZone ||
                          (!isDirectReception && !isSupporting)
                        }
                      >
                        <DisLike />
                        No la apoyo
                      </Button>
                    </div>
                  ) : (
                    <div className={styles.button__login}>
                      <Link href="/login" as="/login">
                        <a>
                          <Button type="secondary" size="small" block={true}>
                            <span>Inicia sesión para participar</span>
                          </Button>
                        </a>
                      </Link>
                    </div>
                  ))}
                {proposal?.reaction === "like" && (
                  <div className={styles.buttons__like}>
                    <span
                      className={styles.buttons__like__text}
                      onClick={() => {
                        supportProposal({
                          id: id,
                          reaction: "omit",
                          userId: dataUser.id,
                        });
                      }}
                    >
                      <Like />
                      {proposal?.likes > 1
                        ? `Tú y ${proposal?.likes - 1}`
                        : proposal?.likes}
                    </span>
                    <span
                      onClick={() => {
                        supportProposal({
                          id: id,
                          reaction: "dislike",
                          userId: dataUser.id,
                        });
                      }}
                    >
                      <DisLike />
                      {proposal?.dislikes}
                    </span>
                  </div>
                )}
                {proposal?.reaction === "dislike" && (
                  <div className={styles.buttons__dislike}>
                    <span
                      onClick={() => {
                        supportProposal({
                          id: id,
                          reaction: "like",
                          userId: dataUser.id,
                        });
                      }}
                    >
                      <Like />
                      {proposal?.likes}
                    </span>
                    <span
                      className={styles.buttons__dislike__text}
                      onClick={() => {
                        supportProposal({
                          id: id,
                          reaction: "omit",
                          userId: dataUser.id,
                        });
                      }}
                    >
                      <DisLike />
                      {proposal?.dislikes}
                    </span>
                  </div>
                )}
              </div>
            )}
            <Modal
              visible={reactionModal}
              closable={false}
              onCancel={() => setReactionModal(false)}
              cancelText="Cancelar"
              okText={okText}
              support={supportModal}
              onOk={() => {
                setReactionModal(false);
                supportModal
                  ? supportProposal({
                      id: id,
                      reaction: "like",
                      userId: dataUser.id,
                    })
                  : supportProposal({
                      id: id,
                      reaction: "dislike",
                      userId: dataUser.id,
                    });
              }}
            />
            <SimpleModal visible={mapModal} onCancel={() => setMapModal(false)}>
              <div className={styles.map}>
                <h6>Ubicación de la zona o barrio GPS</h6>
                <p>
                  Puede navegar por el mapa y ver la zona de esta propuesta
                  específicamente.
                </p>
              </div>
              <Map
                latitud={proposal?.location?.split(",")[0] || 0}
                longitud={proposal?.location?.split(",")[1] || 0}
                defaultMarker={true}
                zoom={15}
              />
            </SimpleModal>
          </div>
        </div>
      </Spin>
    </>
  );
}
