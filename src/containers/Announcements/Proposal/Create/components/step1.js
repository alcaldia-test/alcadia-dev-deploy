import { useSelector, useDispatch } from "react-redux";

import Button from "@components/Button";
import CustomInput from "./CustomInput";

import { changeDataLocalProposal } from "@redux/announcements/actions";

import styles from "../Create.module.scss";

const Step1 = ({ setStep }) => {
  const dispatch = useDispatch();
  const { localProposal } = useSelector((state) => state.announcements);

  const handleChange = (title) => {
    dispatch(changeDataLocalProposal({ title }));
    if (title.length >= 10) {
      dispatch(changeDataLocalProposal({ errorTitle: "" }));
    }
  };

  return (
    <>
      <div className={styles.step__description}>
        <h2>Escribe el título de tu propuesta</h2>
        <p>
          Capta la atención con un título corto y poderoso que les haga
          concentrarse en el cambio que buscas.
        </p>
      </div>
      <CustomInput
        label="Ej: Necesidad de basureros en..."
        value={localProposal?.title}
        onChange={(e) => handleChange(e.target.value)}
        onBlur={() => {
          if (localProposal?.title.length < 10) {
            dispatch(
              changeDataLocalProposal({
                errorTitle: "El título debe tener al menos 10 caracteres",
              })
            );
          }
        }}
        error={localProposal?.errorTitle}
        maxLength={200}
      />
      {localProposal?.errorTitle && (
        <p className={styles.step__error}>{localProposal?.errorTitle}</p>
      )}
      <Button
        block={true}
        className={styles.step__button}
        onClick={() => {
          setStep(localProposal?.step + 1);
        }}
        size="medium"
      >
        Siguiente
      </Button>
    </>
  );
};

export default Step1;
