import { useSelector, useDispatch } from "react-redux";

import Button from "@components/Button";
import WYSIWYG from "@components/WYSIWYG";

import { changeDataLocalProposal } from "@redux/announcements/actions";

import styles from "../Create.module.scss";

const Step2 = ({ setStep }) => {
  const dispatch = useDispatch();
  const { localProposal } = useSelector((state) => state.announcements);

  const handleChange = (e) => {
    dispatch(changeDataLocalProposal({ description: e }));
    const textDesc = e.blocks.reduce((a, b) => {
      return a + b.text;
    }, "");
    if (textDesc.length >= 100) {
      dispatch(changeDataLocalProposal({ errorDescription: "" }));
    }
  };

  return (
    <>
      <div className={styles.step__description}>
        <h2>Explicanos tu propuesta</h2>
        <p>
          Las peticiones más exitosas suelen tener 3 párrafos de largo. Te
          recomendamos que agregues aproximadamente entre <span>100</span> y{" "}
          <span>1000</span> caracteres.
        </p>
      </div>
      <WYSIWYG
        onChange={handleChange}
        value={localProposal?.description}
        maxCharacter={6000}
        minCharacter={100}
        error={localProposal?.errorDescription}
      />
      {localProposal?.errorDescription && (
        <p className={styles.step__error}>{localProposal?.errorDescription}</p>
      )}
      <Button
        block={true}
        className={styles.step__button}
        onClick={() => setStep(localProposal?.step + 1)}
        size="medium"
      >
        Siguiente
      </Button>
    </>
  );
};

export default Step2;
