import React, { useState, useEffect, useRef } from "react";

import styles from "./CustomInput.module.scss";

const CustomInput = ({
  type,
  label,
  icon,
  error = false,
  disabled = false,
  maxLength = false,
  ...rest
}) => {
  const [focused, isFocused] = useState(false);

  const inputELement = useRef(null);

  useEffect(() => {
    if (inputELement.current.value && !disabled) {
      isFocused(true);
    }
  }, []);

  const inputClickHandler = () => {
    if (!disabled) {
      isFocused(true);
      inputELement.current.focus();
    }
  };

  const focusEndHandler = () => {
    if (!inputELement.current.value) {
      isFocused(false);
    }
  };

  return (
    <div
      className={
        disabled
          ? styles.inputContainerDisabled
          : error
          ? styles.inputContainerError
          : focused
          ? styles.inputContainerFocus
          : styles.inputContainer
      }
    >
      <div className={styles.iconContainer}>{icon}</div>
      <div className={styles.inputLabelContainer}>
        <span
          onClick={inputClickHandler}
          className={!focused ? styles.inputLabel : styles.inputLabelFocused}
        >
          {label}
        </span>
        <input
          ref={inputELement}
          onFocus={inputClickHandler}
          onBlur={focusEndHandler}
          className={styles.inputEl}
          type={type}
          disabled={disabled}
          maxLength={maxLength}
          {...rest}
          autoComplete="off"
        />
      </div>
    </div>
  );
};

export default CustomInput;
