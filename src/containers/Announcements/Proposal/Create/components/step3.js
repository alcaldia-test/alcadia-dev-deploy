import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Button from "@components/Button";
import CustomInput from "./CustomInput";
import Map from "@components/Map";
import Switch from "@components/Switch";
import Upload from "@components/Upload";

import {
  changeDataLocalProposal,
  postProposal,
} from "@redux/announcements/actions";

import styles from "../Create.module.scss";

const urlRegex =
  /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g;

const Step3 = ({ setStep, showModal, setErrors }) => {
  const dispatch = useDispatch();
  const { localProposal } = useSelector((state) => state.announcements);
  const { dataUser } = useSelector((storage) => storage.auth);
  const { loader } = useSelector((state) => state.common);
  const [useLocation, setUseLocation] = useState(true);
  const [proposalCreated, setProposalCreated] = useState(false);

  useEffect(() => {
    if (!loader && proposalCreated) {
      setStep(3);
    }
  }, [loader, proposalCreated]);

  const handleChangeFile = (info) => {
    if (info.file.status === "uploading") {
      const file = info?.file;
      file.status = "done";
      dispatch(changeDataLocalProposal({ file: file }));
      dispatch(changeDataLocalProposal({ errorImgOrVideo: "" }));
    }

    if (info.file.status === "removed") {
      dispatch(changeDataLocalProposal({ file: "" }));
      dispatch(
        changeDataLocalProposal({
          errorImgOrVideo:
            "Debes subir un archivo o ingresar una url válida de un video",
        })
      );
    }
  };

  const getCoordinates = (lat, lon) => {
    if (lat && lon) {
      dispatch(changeDataLocalProposal({ location: `${lat},${lon}` }));
    }
  };

  const handleChange = (videoLink) => {
    // Check if is a valid url
    dispatch(changeDataLocalProposal({ videoLink }));
    if (videoLink?.match(urlRegex)) {
      dispatch(changeDataLocalProposal({ errorImgOrVideo: "" }));
    }
  };

  const handleCheckProposal = () => {
    /**
     * TODO: Condiciones para que se pueda crear la propuesta
     * 1. Título: Debe tener un título y tener al menos una cantidad de caracteres
     * 2. Descripción: Debe tener una descripción y tener al menos una cantidad de caracteres
     * 3. Archivo o video: Debe tener una imagen un una URL de video
     * 4. Locación: Debe tener una locación
     */
    const errors = {
      title: !!localProposal?.errorTitle || false,
      description: !!localProposal?.errorDescription || false,
      urlVideoOrImage: !!localProposal?.errorImgOrVideo || false,
      location: !!localProposal?.errorLocation || false,
    };
    const textDesc = localProposal?.description?.blocks.reduce((a, b) => {
      return a + b.text;
    }, "");

    if (!localProposal?.title || localProposal?.title.length < 10) {
      errors.title = true;
      dispatch(
        changeDataLocalProposal({
          errorTitle: "El título debe tener al menos 10 caracteres.",
        })
      );
    } else {
      errors.title = false;
      dispatch(changeDataLocalProposal({ errorTitle: "" }));
    }

    if (!localProposal?.description || textDesc.length < 100) {
      errors.description = true;
      dispatch(
        changeDataLocalProposal({
          errorDescription:
            "La descripción debe tener al menos 100 caracteres.",
        })
      );
    } else {
      errors.description = false;
      dispatch(changeDataLocalProposal({ errorDescription: "" }));
    }

    if (!localProposal?.file && !localProposal?.videoLink?.match(urlRegex)) {
      errors.urlVideoOrImage = true;
      dispatch(
        changeDataLocalProposal({
          errorImgOrVideo:
            "Debes subir un archivo o ingresar una url válida de un video.",
        })
      );
    } else {
      errors.urlVideoOrImage = false;
      dispatch(changeDataLocalProposal({ errorImgOrVideo: "" }));
    }

    if (useLocation && !localProposal?.location) {
      errors.location = true;
      dispatch(
        changeDataLocalProposal({
          errorLocation: "Debes seleccionar una ubicación en el mapa",
        })
      );
    } else {
      dispatch(changeDataLocalProposal({ errorLocation: "" }));
      errors.location = false;
    }

    if (errors.title) {
      setStep(0);
    } else if (errors.description) {
      setStep(1);
    } else if (errors.urlVideoOrImage || errors.location) {
      setStep(2);
    }

    if (
      localProposal?.title.length >= 10 &&
      textDesc.length >= 100 &&
      (localProposal?.file || localProposal?.videoLink?.match(urlRegex))
    ) {
      if (useLocation) {
        if (localProposal?.location) {
          dispatch(postProposal({ ...localProposal, creatorId: dataUser.id }));
          setProposalCreated(true);
        } else {
          showModal(true);
          setErrors(errors);
        }
      } else {
        dispatch(changeDataLocalProposal({ location: "" }));
        dispatch(postProposal({ ...localProposal, creatorId: dataUser.id }));
        setProposalCreated(true);
      }
    } else {
      showModal(true);
      setErrors(errors);
    }

    window.scrollTo(0, 0);
  };

  return (
    <>
      <div className={styles.step__description}>
        <h2>Agrega una foto o video</h2>
        <p>
          Las peticiones con foto o video consiguen más apoyos que peticiones
          que no los tienen. Incluye una foto o un vídeo que capture el lado
          emotivo de tu historia.
        </p>
      </div>
      <Upload
        textButton="Añadir Imagen"
        onChange={handleChangeFile}
        fileList={localProposal?.file ? [localProposal?.file] : []}
        disabled={localProposal?.videoLink?.match(urlRegex)}
        onlyImages={true}
      />
      <div className={styles.step__input}>
        <CustomInput
          label="Link del video"
          value={localProposal?.videoLink}
          onChange={(e) => handleChange(e.target.value)}
          error={localProposal?.errorImgOrVideo}
          disabled={localProposal?.file}
          onBlur={() => {
            if (
              !localProposal?.videoLink ||
              !localProposal?.videoLink?.match(urlRegex)
            ) {
              dispatch(
                changeDataLocalProposal({
                  errorImgOrVideo:
                    "Debes subir un archivo o ingresar una url válida de un video",
                })
              );
            }
          }}
          maxLength={200}
        />
      </div>
      {localProposal?.errorImgOrVideo && (
        <p className={styles.step__error}>{localProposal?.errorImgOrVideo}</p>
      )}
      <div className={styles.step__description}>
        <div className={styles.switch__container}>
          <Switch
            checked={useLocation}
            toggle={() => setUseLocation(!useLocation)}
          />
          <h2>Ubicación de la propuesta</h2>
        </div>
        {useLocation && (
          <>
            <p>
              Puede navegar por el mapa y seleccionar la zona especificamente
              con un click
            </p>
            {localProposal.errorLocation && (
              <p className={styles.step__error}>
                {localProposal?.errorLocation}
              </p>
            )}
            <Map
              latitud={-16.508274883283264}
              longitud={-68.10571263066589}
              canChangeCoordinates={true}
              getCoordinates={getCoordinates}
            />
          </>
        )}
      </div>
      <Button
        block={true}
        className={styles.step__button}
        onClick={handleCheckProposal}
        size="medium"
      >
        Crear Propuesta
      </Button>
    </>
  );
};

export default Step3;
