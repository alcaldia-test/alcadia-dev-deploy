import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import Image from "next/image";
import Head from "next/head";
import message from "@components/Message";
import { validateAnnouncementState } from "@utils/announcementStates";

import ErrorsModal from "@components/Modals/Errors";
import Step1 from "./components/step1";
import Step2 from "./components/step2";
import Step3 from "./components/step3";

import Button from "@components/Button";
import Spin from "@components/Spin";
import Stepper from "@components/Stepper";

import {
  changeStep,
  getFavorites,
  getAnnouncement,
  postProposalSuccess,
  changeDataLocalProposal,
} from "@redux/announcements/actions";

import { ArrowLeft } from "@assets/icons";

import styles from "./Create.module.scss";

const steps = [1, 2, 3];

export const CreateProposal = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { announcementId } = router.query;
  const { dataUser } = useSelector((state) => state.auth);
  const { announcement, localProposal, favorites } = useSelector(
    (state) => state.announcements
  );
  const { loader } = useSelector((state) => state.common);
  const [isInVotingZone, setIsInVotingZone] = useState(true);
  const [showErrorsModal, setShowErrorsModal] = useState(false);
  const [errors, setErrors] = useState([]);

  const { receptionStart, receptionEnd, postResults, directPosts } =
    announcement || {};
  const { isReception, isDirectReception, isFinished } =
    validateAnnouncementState({
      receptionStart,
      receptionEnd,
      postResults,
      directPosts,
    });

  useEffect(() => {
    dispatch(changeDataLocalProposal({ announcementId }));
    dispatch(getAnnouncement({ announcementId, userId: dataUser.id }));
    dispatch(getFavorites(dataUser.id));

    return () => {
      setTimeout(() => dispatch(postProposalSuccess()), 1000);
    };
  }, []);

  useEffect(() => {
    if (!Object.keys(announcement).length > 0) return;
    if (!dataUser?.id) {
      message.error("Necesitas iniciar sesión para poder crear una propuesta");
    } else if (announcement?.haveProposal) {
      message.error("Ya has creado una propuesta, la estamos revisando");
    } else if (!isInVotingZone) {
      message.error("No perteneces a ninguna de las zonas");
    } else if (!isReception && !isDirectReception && !isFinished) {
      message.error("El periodo de creación de propuestas ha finalizado");
    } else if (isFinished) {
      message.error("Esta convocatoria ha finalizado");
    }

    if (!dataUser?.id) {
      router.push({
        pathname: "/login",
        query: { goto: encodeURI(window.location.pathname) },
      });
    } else if (
      announcement?.haveProposal ||
      !isInVotingZone ||
      (!isReception && !isDirectReception) ||
      isFinished
    ) {
      router.push(
        "/propuestas_ciudadanas/[id]",
        `/propuestas_ciudadanas/${announcementId}`
      );
    }
  }, [
    announcement,
    isInVotingZone,
    isReception,
    isDirectReception,
    isFinished,
  ]);

  useEffect(() => {
    // Sólo vendrán o las zonas o las macrozonas
    if (!announcement?.zones && !announcement?.macros) {
      if (
        !announcement?.zones?.length > 0 &&
        !announcement?.macros?.length > 0
      ) {
        setIsInVotingZone(true);
      }
    } else if (announcement?.zones?.length > 0 && favorites?.length > 0) {
      const isInZone = announcement?.zones.some((zone) =>
        favorites.some((favorite) => favorite.zoneId === zone.id)
      );
      setIsInVotingZone(isInZone);
    } else if (announcement?.macros?.length > 0 && favorites?.length > 0) {
      const isInZone = announcement?.macros.some((macro) =>
        favorites.some((favorite) => favorite?.zone?.macroId === macro.id)
      );
      setIsInVotingZone(isInZone);
    }
  }, [favorites]);

  const handleChangeStep = (step) => {
    dispatch(changeStep(step));
  };

  return (
    <>
      <Head>
        <title>Crear propuesta - Paso {localProposal.step + 1}</title>
        <meta
          name="description"
          content="Crea una propuesta para una convocatoria"
          key="description"
        />
        <meta property="og:title" content="Crear propuesta" key="ogtitle" />
        <meta
          property="og:description"
          content="Crea una propuesta para una convocatoria"
          key="ogdescription"
        />
        <meta
          name="twitter:title"
          content="Crear propuesta"
          key="twittertitle"
        />
        <meta
          name="twitter:card"
          content="summary_large_image"
          key="twittercard"
        />
        <meta
          name="twitter:description"
          content="Crea una propuesta para una convocatoria"
          key="twitterdescription"
        />
      </Head>
      <Spin spinning={loader} tip={`Creando propuesta...`} size="large">
        <div className={styles.container}>
          <section className={styles.content}>
            {localProposal.step === 3 ? (
              <div className={styles.finished}>
                <h2>¡Excelente!, Hemos registrado tu propuesta.</h2>
                <div className={styles.finished__image}>
                  <Image
                    src="/images/create-proposal.png"
                    layout="fixed"
                    width={200}
                    height={200}
                  />
                </div>
                <p>
                  Te enviaremos un correo electrónico cuando tu propuesta haya
                  sido aprobada para que puedas compartirla y que otros la
                  apoyen
                </p>
                <Link
                  href="/propuestas_ciudadanas/[announcementId]"
                  as={`/propuestas_ciudadanas/${announcementId}`}
                >
                  <a>
                    <Button
                      block={true}
                      className={styles.step__button}
                      size="medium"
                      onClick={() =>
                        setTimeout(() => dispatch(postProposalSuccess()), 1000)
                      }
                    >
                      Salir y ver otras propuestas
                    </Button>
                  </a>
                </Link>
              </div>
            ) : (
              <>
                <Link
                  href="/propuestas_ciudadanas/[announcementId]"
                  as={`/propuestas_ciudadanas/${announcementId}`}
                >
                  <button className={styles.back__button}>
                    <ArrowLeft />
                    Volver
                  </button>
                </Link>
                <div className={styles.stepper}>
                  <Stepper
                    steps={steps}
                    step={localProposal?.step}
                    setStep={handleChangeStep}
                  />
                </div>
                <div className={styles.step}>
                  {localProposal?.step === 0 && (
                    <Step1 setStep={handleChangeStep} />
                  )}
                  {localProposal?.step === 1 && (
                    <Step2 setStep={handleChangeStep} />
                  )}
                  {localProposal?.step === 2 && (
                    <Step3
                      setStep={handleChangeStep}
                      showModal={setShowErrorsModal}
                      setErrors={setErrors}
                    />
                  )}
                </div>
              </>
            )}
          </section>
        </div>
      </Spin>
      <ErrorsModal
        visible={showErrorsModal}
        closable={false}
        onCancel={() => setShowErrorsModal(false)}
        cancelText="Cerrar"
        errors={errors}
      />
    </>
  );
};

export default CreateProposal;
