import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import Image from "next/image";
import SEO from "@components/SEO";

import {
  getFavorites,
  getAnnouncements,
  getCountAnnouncements,
  getTotalAnnouncements,
  changeFilterAnnouncements,
} from "@redux/announcements/actions";

import Autocomplete from "@components/Autocomplete";
import Banner from "@components/Banner";
import Button from "@components/Button";
import CardAnnouncements from "@components/Cards/Announcement";
import Pagination from "@components/Pagination";
import Select from "@components/Select";
import Share from "@components/Modals/Share";

import styles from "./Announcements.module.scss";
import { useRouter } from "next/router";

const baseSelectOptions = [
  { value: "active", label: "Activas" },
  { value: "popular", label: "Más Populares" },
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más Antiguas" },
  { value: "supporting", label: "En Apoyo" },
  { value: "finished", label: "Finalizadas" },
];

export default function Announcements() {
  const dispatch = useDispatch();
  const router = useRouter();
  const { dataUser } = useSelector((storage) => storage.auth);
  const {
    count,
    favorites,
    announcements,
    totalAnnouncements,
    filterAnnouncements,
    loadingAnnouncements,
  } = useSelector((state) => state.announcements);
  const { banners } = useSelector((storage) => storage.global);

  const [banner, setBanner] = useState("");
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");
  const [modalPostsIdShare, setModalPostsIdShare] = useState("");
  const [filter, setFilter] = useState(
    filterAnnouncements?.status
      ? filterAnnouncements?.status
      : filterAnnouncements?.order
      ? filterAnnouncements?.order
      : filterAnnouncements?.zones || filterAnnouncements?.macros
      ? "zones"
      : "DESC"
  );
  const [filterAutocomplete, setFilterAutocomplete] = useState(
    filterAnnouncements?.like || ""
  );
  const [currentPage, setCurrentPage] = useState(1);

  const [optionsSelect, setOptionsSelect] = useState([...baseSelectOptions]);
  const optionsAutocomplete =
    announcements.length > 0
      ? announcements?.map((item) => ({
          title: item.title,
        }))
      : [];

  useEffect(() => {
    dispatch(getTotalAnnouncements());
    if (!Object.keys(dataUser).length > 0) return;
    dispatch(getFavorites(dataUser.id));
    setOptionsSelect([
      ...baseSelectOptions,
      { value: "zones", label: "Mis Zonas" },
    ]);
  }, []);

  useEffect(() => {
    if (!banners?.length > 0) return;
    const bannerObj = banners.find((banner) => banner.key === "announcement");
    setBanner(bannerObj?.fileStorages[0]?.link);
  }, [banners]);

  useEffect(() => {
    const filterObj = {
      limit: 12,
      skip: (currentPage - 1) * 12,
      like: filterAutocomplete,
    };

    if (
      filter === "active" ||
      filter === "finished" ||
      filter === "supporting" ||
      filter === "popular"
    ) {
      filterObj.status = filter;
    } else if (filter === "DESC" || filter === "ASC") {
      filterObj.order = filter;
    } else if (filter === "zones") {
      if (favorites?.length > 0) {
        filterObj.zones = favorites.map((favorite) => favorite.zoneId);
        filterObj.macros = favorites.map((favorite) => favorite.zone.macroId);
      }
    }

    dispatch(changeFilterAnnouncements(filterObj));
  }, [filter, filterAutocomplete, currentPage]);

  useEffect(() => {
    dispatch(getAnnouncements(filterAnnouncements));
    dispatch(getCountAnnouncements(filterAnnouncements));
  }, [filterAnnouncements]);

  useEffect(() => {
    if (!count) return;
    if (count > 12 && currentPage > count / 12) {
      setCurrentPage(Math.ceil(count / 12));
    } else if (count <= 12) {
      setCurrentPage(1);
    }
  }, [count]);
  return (
    <>
      <SEO image={banner} />
      <Banner
        title="Propuestas Ciudadanas"
        subtitle="Participa en la creación de propuestas para los siguientes proyectos"
        image={banner}
      />
      <section className={styles.filter__container}>
        <div className={styles.filters}>
          <Select
            options={optionsSelect}
            placeholder="Filtrar..."
            onChange={(e) => setFilter(e[0]?.value)}
            className={styles.select}
            defaultValue={
              optionsSelect.findIndex((option) => option.value === filter) > -1
                ? optionsSelect.findIndex((option) => option.value === filter)
                : "DESC"
            }
          />
          <Autocomplete
            dataSource={optionsAutocomplete}
            placeholder="Buscar..."
            onChange={(e) => setFilterAutocomplete(e)}
            defaultValue={filterAutocomplete}
          />
        </div>
        <div className={styles.filter__content}>
          {loadingAnnouncements ? (
            <div className={styles.loading}>
              <Image
                src="/gifts/Spin-1s-200px.gif"
                width={50}
                height={50}
                objectFit="contain"
              />
            </div>
          ) : announcements?.length > 0 ? (
            announcements?.map(
              ({
                supportingStart,
                supportingEnd,
                receptionStart,
                receptionEnd,
                reviewStart,
                reviewEnd,
                directPosts,
                postResults,
                proposals,
                fileLink,
                title,
                areas,
                postsId,
                id,
              }) => (
                <Link
                  href="/propuestas_ciudadanas/[announcementId]"
                  as={`/propuestas_ciudadanas/${id}`}
                  key={id}
                >
                  <a>
                    <CardAnnouncements
                      receptionStart={receptionStart}
                      receptionEnd={receptionEnd}
                      reviewStart={reviewStart}
                      reviewEnd={reviewEnd}
                      supportingStart={supportingStart}
                      supportingEnd={supportingEnd}
                      postResults={postResults}
                      directPosts={directPosts}
                      proposals={proposals}
                      image={fileLink}
                      title={title}
                      zones={areas}
                      buttonChildren={true}
                    >
                      <Button
                        type="secondary"
                        shape="round"
                        size="small"
                        onClick={(e) => {
                          e.preventDefault();
                          console.log(postsId, id);
                          if (!dataUser.id) {
                            router.push({
                              pathname: "/login",
                              query: {
                                goto: encodeURI(window.location.pathname),
                              },
                            });
                          } else {
                            setModalURL(
                              `${location.origin}/propuestas_ciudadanas/${id}`
                            );
                            setModalOpen(true);
                            setModalPostsIdShare(postsId);
                          }
                        }}
                      >
                        Comparte
                      </Button>
                    </CardAnnouncements>
                  </a>
                </Link>
              )
            )
          ) : (
            <div className={styles.no__results}>
              {totalAnnouncements === 0 ? (
                <p>Aún no hay propuestas ciudadanas</p>
              ) : (
                <p>
                  No se encontraron resultados
                  {filterAutocomplete ? " para " + filterAutocomplete : "."}
                </p>
              )}
            </div>
          )}
        </div>
        <Pagination
          defaultPageSize={12}
          total={count}
          current={currentPage}
          onChange={(page) => {
            setCurrentPage(page);
          }}
        />
      </section>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
        userId={dataUser.id}
        postsId={modalPostsIdShare}
      />
    </>
  );
}
