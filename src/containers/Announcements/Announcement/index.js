import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import draftToHtml from "draftjs-to-html";
import Image from "next/image";
import Link from "next/link";

import {
  getFavorites,
  getTotalProposals,
  getProposals,
  postProposalSupport,
  getCountProposals,
  changeFilterProposals,
} from "@redux/announcements/actions";
import { validateAnnouncementState } from "@utils/announcementStates";
import { validateIfUserIsInZone } from "@utils/zoneValidator";

import Avatar from "@components/Avatar";
import Autocomplete from "@components/Autocomplete";
import BannerLarge from "@components/BannerLarge";
import Pagination from "@components/Pagination";
import { ProposalCard } from "@components/Cards";
import Select from "@components/Select";
import Spin from "@components/Spin";

import styles from "./Announcement.module.scss";

const defaultProfile = {
  image: "/images/avatar.png",
  name: "Nombre",
};

export default function Announcement({ announcementId, announcement }) {
  const dispatch = useDispatch();
  const { dataUser } = useSelector((state) => state.auth);
  const {
    favorites,
    // announcement,
    proposals,
    totalProposals,
    countProposals,
    waiting,
    loadingProposals,
    filterProposals,
  } = useSelector((state) => state.announcements);
  const [filter, setFilter] = useState(filterProposals?.order || "DESC");
  const [filterAutocomplete, setFilterAutocomplete] = useState(
    filterProposals.like || undefined
  );
  const [page, setPage] = useState(1);
  const [description, setDescription] = useState({});
  const [release, setRelease] = useState({});

  const { isSupporting, isEvaluating, isFinished } = validateAnnouncementState({
    reviewEnd: announcement?.reviewEnd,
    supportingStart: announcement?.supportingStart,
    supportingEnd: announcement?.supportingEnd,
    postResults: announcement?.postResults,
    directPosts: announcement?.directPosts,
  });

  const isInVotingZone = validateIfUserIsInZone({
    favorites,
    zones: announcement?.zones,
    macros: announcement?.macros,
  });

  const [optionsSelect] = useState([
    { value: "DESC", label: "Más Recientes" },
    { value: "ASC", label: "Más Antiguas" },
  ]);
  const optionsAutocomplete =
    proposals?.others?.length > 0
      ? proposals?.others?.map((item) => ({
          title: item?.title || "",
        }))
      : [];

  useEffect(() => {
    if (Object.keys(dataUser).length > 0) dispatch(getFavorites(dataUser.id));
    // dispatch(getAnnouncement({ announcementId, userId: dataUser?.id }));
    dispatch(getTotalProposals(announcementId));
  }, []);

  useEffect(() => {
    const filterObj = {
      limit: 12,
      skip: (page - 1) * 12,
      like: filterAutocomplete,
      id: announcementId,
    };

    if (dataUser.id) {
      filterObj.userId = dataUser.id;
    }

    if (filter === "DESC" || filter === "ASC") {
      filterObj.order = filter;
    }

    dispatch(changeFilterProposals(filterObj));
  }, [filter, filterAutocomplete, page]);

  useEffect(() => {
    if (
      announcement?.id === filterProposals?.id &&
      (announcement?.directPosts || isSupporting || isEvaluating || isFinished)
    ) {
      console.log(
        announcement?.id,
        filterProposals?.id,
        announcement?.directPosts,
        isSupporting,
        isEvaluating,
        isFinished
      );

      dispatch(getProposals(filterProposals));
      dispatch(getCountProposals(announcement?.id));
    }
  }, [filterProposals, announcement, isEvaluating, isSupporting, isFinished]);

  useEffect(() => {
    if (countProposals > 12 && page > countProposals / 12) {
      setPage(Math.ceil(countProposals / 12));
    } else if (countProposals <= 12) {
      setPage(1);
    }
  }, [countProposals]);

  useEffect(() => {
    if (!announcement?.content) return;
    try {
      const desc = JSON.parse(announcement?.content);
      setDescription(desc);
      // const seoDesc = desc.blocks.reduce((a, c) => a + c.text, "");
      // setSeoDescription(seoDesc);
    } catch (error) {
      setDescription(announcement?.content);
      // setSeoDescription(announcement?.content);
    }
    if (announcement?.winnerMessage) {
      try {
        const rel = JSON.parse(announcement?.winnerMessage);
        setRelease(rel);
      } catch (error) {
        setRelease(announcement?.winnerMessage);
      }
    }
  }, [announcement]);

  const supportProposal = (payload) => {
    dispatch(postProposalSupport({ ...payload, announcementId }));
  };

  return (
    <>
      <Spin spinning={waiting} size="large">
        <BannerLarge
          hasCreated={announcement?.haveProposal}
          attached={announcement?.attacher}
          announcementId={announcementId}
          file={announcement?.attachment}
          image={announcement?.banner}
          title={announcement?.title}
          subtitle={description}
          userId={dataUser?.id}
          receptionStart={announcement?.receptionStart}
          receptionEnd={announcement?.receptionEnd}
          reviewStart={announcement?.reviewStart}
          reviewEnd={announcement?.reviewEnd}
          supportingStart={announcement?.supportingStart}
          supportingEnd={announcement?.supportingEnd}
          postResults={announcement?.postResults}
          directPosts={announcement?.directPosts}
          isInVotingZone={isInVotingZone}
          zones={announcement?.zones}
          macros={announcement?.macros}
        />
        {isFinished && (
          <section className={styles.filter__container}>
            <h2 className={styles.filter__finished_title}>
              Propuestas ganadoras
            </h2>
            <div
              dangerouslySetInnerHTML={{ __html: draftToHtml(release) }}
              className={styles.filter__description}
            />
            {proposals?.winners?.length > 0 ? (
              proposals?.winners?.map(({ user, likes, title, id }) => (
                <Link key={id} href={`/propuesta[id]`} as={`/propuesta/${id}`}>
                  <div className={styles.proposal__winner}>
                    <div className={styles.proposal__winner_user}>
                      <Avatar
                        src={user?.avatar || defaultProfile.image}
                        size="small"
                        className={styles.card__profile_avatar}
                      />
                      <p>
                        {user?.firstName || defaultProfile.name}
                        <span>{likes} apoyos</span>
                      </p>
                    </div>
                    <p className={styles.proposal__winner_content}>{title}</p>
                  </div>
                </Link>
              ))
            ) : (
              <p className={styles.no__results}>
                No hay ganadores para esta convocatoria
              </p>
            )}
          </section>
        )}
        {(announcement?.directPosts ||
          isSupporting ||
          isEvaluating ||
          isFinished) && (
          <section className={styles.filter__container}>
            <div className={styles.filters}>
              <Select
                options={optionsSelect}
                placeholder="Filtrar..."
                onChange={(e) => setFilter(e[0]?.value)}
                defaultValue={optionsSelect.findIndex(
                  (option) => option.value === filter
                )}
              />
              <Autocomplete
                dataSource={optionsAutocomplete}
                placeholder="Buscar..."
                onChange={(e) => setFilterAutocomplete(e)}
                defaultValue={filterAutocomplete}
              />
            </div>
            <div className={styles.filter__content}>
              {loadingProposals ? (
                <Image
                  src="/gifts/Spin-1s-200px.gif"
                  width={50}
                  height={50}
                  objectFit="contain"
                />
              ) : proposals?.others?.length > 0 ? (
                proposals?.others?.map(
                  ({
                    content,
                    fileLink,
                    reaction,
                    title,
                    user,
                    stage,
                    id,
                    videoLink,
                    dislikes,
                    likes,
                  }) => (
                    <Link
                      href={`/propuesta/[id]`}
                      as={`/propuesta/${id}`}
                      key={id}
                    >
                      <a>
                        <ProposalCard
                          id={id}
                          title={title}
                          user={user}
                          image={fileLink}
                          description={content}
                          reaction={reaction}
                          stage={stage}
                          onOk={(payload) => supportProposal(payload)}
                          userId={dataUser.id}
                          isInVotingZone={isInVotingZone}
                          video={videoLink}
                          dislikes={dislikes}
                          likes={likes}
                        />
                      </a>
                    </Link>
                  )
                )
              ) : (
                <div className={styles.no__results}>
                  {totalProposals === 0 ? (
                    isFinished || isEvaluating ? (
                      <p>No se crearon propuestas en esta convocatoria.</p>
                    ) : (
                      <p>
                        Aún no se han creado propuestas ciudadanas, ¡Sé el
                        primero en crear una!
                      </p>
                    )
                  ) : (
                    <p>
                      No se encontraron resultados para:
                      {` "${filterAutocomplete}"`}
                    </p>
                  )}
                </div>
              )}
            </div>
            <Pagination
              defaultPageSize={12}
              total={countProposals}
              current={page}
              onChange={(p) => setPage(p)}
            />
          </section>
        )}
      </Spin>
    </>
  );
}
