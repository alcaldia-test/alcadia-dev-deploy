import Image from "next/image";
import React, { useEffect } from "react";
import styles from "../permissions.module.scss";
import Router from "next/router";

const PermissionsSuccess = () => {
  useEffect(() => {
    setTimeout(() => {
      Router.replace("/ingreso");
    }, 6000);
  }, []);

  return (
    <div className="flex justify-center flex-col w-full h-full">
      <Image
        src="/images/logo.svg"
        alt="Logo gobierno autonomo municipal de La Paz"
        width={200}
        height={200}
      />
      <p className={`text-3xl font-bold text-center mt-4  ${styles.user}`}>
        Se envió tu registro
      </p>
      <p className={` text-xl font-bold text-center mt-3 ${styles.color_sub}`}>
        Espere o contacte con su administrador para que su acceso sea habilitado
      </p>
    </div>
  );
};
export default PermissionsSuccess;
