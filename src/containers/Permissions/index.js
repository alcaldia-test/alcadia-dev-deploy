import Button from "@components/Button";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "./permissions.module.scss";
import Collapse from "@components/Collapse";
import { registerPermissionsAdminStart } from "@redux/permissions/actions";

export default function Permissions() {
  const dispatch = useDispatch();
  const { permissions = [] } = useSelector((state) => state);
  const { adminRegister } = useSelector((state) => state.auth);
  let values = [];
  const onChange = (e) => {
    if (e.target.checked) {
      values.push(e.target.value);
    } else {
      values = values.filter((el) => e.target.value !== el);
    }
  };
  const SendPermission = (e) => {
    e.preventDefault();
    dispatch(
      registerPermissionsAdminStart({
        values: values,
        id: adminRegister.id,
      })
    );
  };
  const superAdmin = (e) => {
    e.preventDefault();
    const admin = permissions.data.filter((e) => e.name === "Super Admin");
    dispatch(
      registerPermissionsAdminStart({
        values: [admin[0].id],
        id: adminRegister.id,
      })
    );
  };
  return (
    <>
      <div className="flex flex-row justify-center relative w-full mb-6 ">
        <div className="place-self-center	">
          <p className={`font-bold text-2xl text-center ${styles.user}`}>
            Por favor seleccione los permisos que desea tener
          </p>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 justify-items-center gap-y-px w-full	">
        <Collapse
          header={"Votaciones"}
          items={permissions.data.filter(
            (e) =>
              e.roleGroupId === "5b411897-5015-4be2-a5ac-d9623a7b1268" &&
              e.name !== "Gestionar todo en Votaciones"
          )}
          typeCheckBox={"primary"}
          onChange={onChange}
          bordered={false}
        />
        <Collapse
          header={"Convocatorias"}
          items={permissions.data.filter(
            (e) =>
              e.roleGroupId === "90adf2b9-00dc-4b4b-a9ed-990413202e92" &&
              e.name !== "Gestionar todo en Convocatorias"
          )}
          typeCheckBox={"primary"}
          onChange={onChange}
          bordered={false}
        />
        <Collapse
          header={"Charlas Virtuales"}
          items={permissions.data.filter(
            (e) =>
              e.roleGroupId === "747da2bd-d53e-485b-a5d1-4cdbfeb688b6" &&
              e.name !== "Gestionar todo en Charlas"
          )}
          typeCheckBox={"primary"}
          onChange={onChange}
          bordered={false}
        />
        <Collapse
          header={"Normativas"}
          items={permissions.data.filter(
            (e) =>
              e.roleGroupId === "c51440bb-d680-450c-9ee0-582066b92e33" &&
              e.name !== "Gestionar todo en Normativas"
          )}
          typeCheckBox={"primary"}
          onChange={onChange}
          bordered={false}
        />
        <Collapse
          header={"Usuarios"}
          items={permissions.data.filter(
            (e) =>
              e.name === "Bloquear Usuarios" || e.name === "Gestionar Denuncias"
          )}
          typeCheckBox={"primary"}
          onChange={onChange}
          bordered={false}
        />
        <Collapse
          header={"Página"}
          items={permissions.data.filter(
            (e) =>
              e.name === "Editar encabezados" ||
              e.name === "Gestionar Logros" ||
              e.name === "Ver Estadísticas"
          )}
          typeCheckBox={"primary"}
          onChange={onChange}
          bordered={false}
        />
      </div>
      <div className="flex flex-col justify-around items-center mt-6">
        <Button type="secondary" onClick={superAdmin}>
          Ser superadministrador
        </Button>
        <div className={`flex justify-center mt-7 `}>
          <Button size={"large"} className=" w-250 " onClick={SendPermission}>
            Finalizar Registro
          </Button>
        </div>{" "}
      </div>
    </>
  );
}
