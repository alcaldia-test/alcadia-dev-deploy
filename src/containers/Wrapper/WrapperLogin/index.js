import React from "react";
import Head from "next/head";
import Topbar from "@components/NavBar/TopBar";
import Menu from "@components/Menu";

import styles from "./wrapperloggin.module.scss";
const WrapperLogin = ({
  children,
  seo,
  defaultKey,
  description = "",
  routeName = "",
}) => {
  return (
    <>
      {seo && (
        <Head>
          <title>{seo}</title>
          <meta property="og:title" content={seo} key="title" />
          <meta name="description" content={description} />
        </Head>
      )}
      <div className={styles.container}>
        <aside>
          <Menu collapsed={false} defaultSelectedKey={defaultKey} />
        </aside>
        <main>
          <Topbar
            isAdmin={true}
            isLogged={true}
            logginLink="/login"
            registerLink="/register"
            userLink="#"
            notificationLink="#"
            routeName={routeName}
            menus={[
              {
                title: "Mi perfil",
                path: "/admin/perfil",
              },
              {
                title: "Tu Voto Cuenta",
                path: "/tu_voto_cuenta",
              },
              {
                title: "Propuestas ciudadanas",
                path: "/propuestas_ciudadanas",
              },
              {
                title: "Charlas",
                path: "/charlas",
              },
              {
                title: "Normativa Colaborativa",
                path: "/normativas",
              },
            ]}
          />
          {children}
        </main>
      </div>
    </>
  );
};

export default WrapperLogin;
