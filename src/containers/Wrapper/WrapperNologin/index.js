import React, { useEffect, useState } from "react";
// import SEO from "@components/SEO";
import { useRouter } from "next/router";
import Topbar from "@components/NavBar/TopBar";
import FooterComponent from "@components/Footer";
import { useDispatch, useSelector } from "react-redux";
import { changeCurrentPage } from "@redux/global/actions";
import Image from "next/image";

/**
 *
 * @param {children} children
 * @param {title} title
 * @param {description} description
 * @param {defaultImg} defaultImg If true set default image, also can be a string with the url of the image
 * @param {currentRoute} currentRoute
 *
 * @returns {JSX.Element} Wrapper
 */

const Wrapper = ({
  children,
  title = "",
  description = "",
  defaultImg = false,
  currentRoute,
}) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state);
  const router = useRouter();
  const [page, setPage] = useState(currentRoute || data.global.currentPage);

  const handlePage = (pageTitle) => {
    dispatch(changeCurrentPage(pageTitle));
  };
  const { auth } = useSelector((store) => store);

  useEffect(() => {
    const route = `${router.asPath}`;
    if (
      data.global.currentPage.length < 1 ||
      !data.global.currentPage.includes(`${route}`)
    ) {
      if (route.includes("normativas")) {
        dispatch(changeCurrentPage("/normativas"));
        setPage("/normativas");
        return;
      }
      if (route.includes("/tu_voto_cuenta")) {
        dispatch(changeCurrentPage("/tu_voto_cuenta"));
        setPage("/tu_voto_cuenta");
        return;
      }

      if (route.includes("/charlas")) {
        dispatch(changeCurrentPage("charlas"));
        setPage("/charlas");
        return;
      }
      if (route.match(/propuestas_ciudadanas|propuesta/)) {
        dispatch(changeCurrentPage("/propuestas_ciudadanas"));
        setPage("/propuestas_ciudadanas");
        return;
      }
      dispatch(changeCurrentPage("/"));
      setPage("/");
    }
  }, [router.asPath, page]);
  console.log(auth.spinLogin);
  return (
    <>
      <div className={`w-full h-full bg-white fixed z-50 ${auth.spinLogin}`}>
        <div className="w-full h-full flex justify-center items-center  ">
          <Image
            className="animate-pulse"
            src="/images/logo.svg"
            alt="Logo gobierno autonomo municipal de La Paz"
            width={200}
            height={200}
          />
        </div>
      </div>
      {/* <SEO
        title={title}
        description={description}
        image={
          typeof defaultImg === "string"
            ? defaultImg
            : typeof defaultImg === "boolean" && defaultImg
            ? "https://participa.lapaz.bo/images/banner.jpg"
            : ""
        }
      /> */}
      <Topbar
        isLogged={!!data.auth.tokenUser}
        logginLink="/login"
        registerLink="/register"
        notificationLink="#"
        currentPage={page}
        handleCurrentPage={handlePage}
        menus={[
          {
            title: "Inicio",
            path: "/",
          },
          {
            title: "Tu voto cuenta",
            path: "/tu_voto_cuenta",
          },
          {
            title: "Propuestas ciudadanas",
            path: "/propuestas_ciudadanas",
          },
          {
            title: "Charlas",
            path: "/charlas",
          },
          {
            title: "Normativa colaborativa",
            path: "/normativas",
          },
        ]}
      />
      <div>{children}</div>
      <FooterComponent />
    </>
  );
};

export default Wrapper;
