import React, { useEffect, useState } from "react";

import { Cropping } from "./index";
import message from "@components/Message";
import { useSelector, useDispatch } from "react-redux";
import Modal from "@components/Modals/SimpleModal";
import Button from "@components/Button";
import { base64ToFile } from "@utils/fileManager";
import { changeLandingStart } from "@redux/global/actions";
import Spin from "@components/Spin";

import Styles from "../logos.module.scss";
import StyleComp from "./edit-landing.module.scss";

let currectState = {
  title: "",
  content: "",
  link: "",
  button: "",
};

export const EditLanding = ({ index = 0, onCancel = () => {} }) => {
  const disptach = useDispatch();
  const { banners } = useSelector((storage) => storage.global);
  const [uri, setUri] = useState("");
  const [loading, setLoading] = useState(false);

  const ladding = banners.find(({ key }) => key === "landing");
  const ladingFiles = ladding?.fileStorages ?? [];

  const [link, setLink] = useState(ladingFiles[index]?.link ?? "");

  useEffect(() => {
    currectState = { ...ladding };
    delete currectState?.fileStorages;
  }, []);

  const handleChange = (ev) => {
    currectState[ev.target.name] = ev.target.value;
  };

  const handleChangeImage = (ev) => {
    const file = ev.target.files[0];

    const size = (file.size ?? 0) / 1024 / 1024;
    const allowedTypes = ["image/png", "image/jpg", "image/jpeg"];

    if (size > 3) return message.error("Tamaño máximo permitido en 3Mb");
    if (!allowedTypes.includes(file.type))
      return message.error("Sólo está permitido archivo tipo PNG, JPG y JPEG");

    const src = URL.createObjectURL(file);

    setUri(src);
  };

  const handleCropped = (result) => {
    if (result) {
      setLink(result);
    }

    setUri("");
  };

  const handleSave = async () => {
    let file, invalidMsg;
    let changedData = false;

    if (link && link !== ladingFiles[index]?.link)
      file = base64ToFile(link, "banner.png");

    console.log(currectState);

    const camps = {
      title: "Título",
      content: "Sub Título",
      button: "Boton",
      link: "Enlace",
    };

    for (const key in currectState) {
      if (currectState[key].length > 200 && key !== "button") {
        invalidMsg = `El tamaño del ${camps[key]}, debe ser menor a 200 caracteres.`;
        break;
      } else if (key === "button" && currectState[key].length > 30) {
        invalidMsg = `El tamaño del Buton, debe ser menor a 30 caracteres.`;
        break;
      }

      if (currectState[key] !== ladding[key]) {
        changedData = true;
        break;
      }
    }

    if (invalidMsg) return message.error(invalidMsg);

    const payload = {
      file,
      id: ladding.id,
      fileId: ladingFiles[index]?.id,
      changedData,
      ...currectState,
    };

    try {
      setLoading(true);
      const msg = await new Promise((resolve, reject) =>
        disptach(changeLandingStart({ ...payload, resolve, reject }))
      );
      message.success(msg);
    } catch (error) {
      message.error(error);
    }

    setLoading(false);
    onCancel();
  };

  return (
    <>
      {uri !== "" && <Cropping saveCropped={handleCropped} imgurl={uri} />}
      <Modal
        style={{ top: 50 }}
        visible={uri === ""}
        onCancel={() => onCancel()}
      >
        <Spin spinning={loading} size="large">
          <div className={StyleComp["edit-lading"]}>
            <h4>Editar imagen de cabecera #{index + 1}</h4>
            <p>Imagen</p>
            <div
              className={`${Styles["carroucel-item"]} mr-0`}
              style={{ marginRight: 0 }}
            >
              <label
                style={link ? { backgroundImage: `url(${link})` } : undefined}
                className={`
                ${Styles["custom-upload"]} 
                ${link ? Styles["with-image"] : ""} 
                ${StyleComp["upload-into-modal"]}
              `}
              >
                {!link ? `+ Imagen de Cabecera #${index + 1}` : ""}
                <input
                  accept="image/*"
                  type="file"
                  onInput={handleChangeImage}
                  name="filename"
                  maxLength={20}
                />
              </label>
            </div>
            <div className={StyleComp["custom-divider"]}></div>

            <h4>Editar contenido general de las cabeceras</h4>
            <div className={StyleComp["input-section"]}>
              <label>
                Título
                <input
                  type="text"
                  name="title"
                  placeholder="LA PAZ DECIDE"
                  defaultValue={ladding?.title}
                  onInput={handleChange}
                  maxLength={250}
                />
              </label>
              <label>
                Sub Título
                <textarea
                  name="content"
                  placeholder="La paz decide..."
                  defaultValue={ladding?.content}
                  onInput={handleChange}
                  maxLength={250}
                />
              </label>
              <label>
                Botón Principal
                <input
                  type="text"
                  name="button"
                  defaultValue={ladding?.button}
                  onInput={handleChange}
                  maxLength={50}
                />
                <input
                  type="text"
                  name="link"
                  defaultValue={ladding?.link}
                  onInput={handleChange}
                  maxLength={250}
                />
              </label>
            </div>
            <div className={Styles["section-buttons"]}>
              <Button onClick={() => onCancel()} type="secondary" size="small">
                Cancelar
              </Button>
              <Button onClick={handleSave} type="primary" size="small">
                Guardar
              </Button>
            </div>
          </div>
        </Spin>
      </Modal>
    </>
  );
};
