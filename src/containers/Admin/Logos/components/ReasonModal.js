import React, { useState } from "react";

import Modal from "@components/Modals/SimpleModal";
import Button from "@components/Button";
import Spin from "@components/Spin";
import styles from "./edit-landing.module.scss";
import { useDispatch } from "react-redux";
import { changePTuotialStart } from "@redux/global/actions";
import message from "@components/Message";

const currectState = {
  content: "",
};

export function ReasonModal({ tutorial = {}, onCancel }) {
  const disptach = useDispatch();
  const [loading, setLoading] = useState(false);

  const handleChange = (ev) => {
    currectState[ev.target.name] = ev.target.value;
  };

  const handleSave = async () => {
    if (!currectState.content) return message.error("¡Introduzca el link!");

    const payload = {
      id: tutorial?.id,
      key: "proposal-tutorial",
      ...currectState,
    };

    try {
      setLoading(true);

      const msg = await new Promise((resolve, reject) =>
        disptach(changePTuotialStart({ ...payload, resolve, reject }))
      );

      currectState.content = "";
      message.success(msg);
    } catch (error) {
      message.error(error);
    }

    onCancel();
  };

  return (
    <Modal visible={true} onCancel={onCancel}>
      <Spin spinning={loading} size="large">
        <div className={styles["input-section"]}>
          <h3>
            {tutorial.content ? "Cambiar link del video" : "Añade el video"}
          </h3>
          <input
            name="content"
            defaultValue={tutorial.content}
            placeholder="Link de video..."
            onChange={handleChange}
          />
        </div>
        <div className="flex justify-center mt-2">
          <Button onClick={handleSave} type="primary" size="small">
            {tutorial.content ? "Guardar cambios" : "Añadir link"}
          </Button>
        </div>
      </Spin>
    </Modal>
  );
}
