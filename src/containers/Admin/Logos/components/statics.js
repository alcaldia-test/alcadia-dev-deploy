export const titles = {
  announcement: "Cabecera - Propuestas Ciudadanas",
  normative: "Cabecera - Normativa Colaborativa",
  discussion: "Cabecera - Charlas Virtuales",
  voting: "Cabecera - Tu voto cuenta",
  landing: "Carrusel - Página Principal",
};

export default titles;
