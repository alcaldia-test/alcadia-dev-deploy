import React, { useState } from "react";

import { Cropping, EditLanding } from "./index";

import Styles from "../logos.module.scss";
import message from "@components/Message";
import { base64ToFile } from "@utils/fileManager";
import { deleteBannerStart, changeBannerStart } from "@redux/global/actions";
import { useDispatch } from "react-redux";
import { Trash } from "@assets/icons";

import confirm from "@components/Modals/confirm";

import { titles } from "./statics";
import Spin from "@components/Spin";

let selectFile = { id: "", fileId: "" };
export const BannerSection = ({ module = "", files = [] }) => {
  const [uri, setUri] = useState("");
  const [index, setIndex] = useState(-1);
  const disptach = useDispatch();

  const [loading, setLoading] = useState(false);

  const handleCropped = async (result) => {
    setUri("");
    if (result) {
      const file = base64ToFile(result, "banner.png");

      try {
        setLoading(true);
        const msg = await new Promise((resolve, reject) =>
          disptach(changeBannerStart({ ...selectFile, file, resolve, reject }))
        );
        message.success(msg);
      } catch (error) {
        message.error(error);
      }

      setLoading(false);
    }

    selectFile = { id: "", fileId: "" };
  };

  const handleChange = (ev, fileId, settingId) => {
    selectFile.fileId = fileId;
    selectFile.id = settingId;

    const file = ev.target.files[0];

    const size = (file.size ?? 0) / 1024 / 1024;

    const allowedTypes = ["image/png", "image/jpg", "image/jpeg"];

    if (size > 3) return message.error("Tamaño máximo permitido en 3Mb");
    if (!allowedTypes.includes(file.type))
      return message.error("Sólo está permitido archivo tipo PNG, JPG y JPEG");

    const src = URL.createObjectURL(file);

    setUri(src);
  };

  const handleDelete = async (fileId, settingId) => {
    const confirmed = await confirm("¿Seguro(a) que quiere borrar la imagen?");
    if (!confirmed) return;

    try {
      setLoading(true);
      const msg = await new Promise((resolve, reject) =>
        disptach(deleteBannerStart({ fileId, id: settingId, resolve, reject }))
      );
      message.success(msg);
    } catch (error) {
      message.error(error);
    }

    setLoading(false);
  };

  const handleOpenModal = (ev, i) => {
    if (module === "landing") {
      ev.preventDefault();

      setIndex(i);
    }
  };

  return (
    <>
      {uri !== "" && (
        <Cropping widthCrop={10} saveCropped={handleCropped} imgurl={uri} />
      )}
      <div className={Styles["banners-item"]}>
        <h4>{titles[module]}</h4>
        <Spin spinning={loading} size="large">
          <div className={Styles["carroucel-images"]}>
            {files.map(({ id, link, settingId }, i) => (
              <div key={i} className={Styles["carroucel-item"]}>
                {i > 0 && link && (
                  <i onClick={handleDelete.bind({}, id, settingId)}>
                    <span>
                      <Trash />
                    </span>
                  </i>
                )}
                {index === i && (
                  <EditLanding onCancel={() => setIndex(-1)} index={i} />
                )}
                <label
                  onClick={(ev) => handleOpenModal(ev, i)}
                  style={link ? { backgroundImage: `url(${link})` } : undefined}
                  className={`${Styles["custom-upload"]}
                   ${link ? Styles["with-image"] : ""}
                   ${module === "landing" ? Styles["landing-sizes"] : ""}
                  `}
                >
                  {i > 0 && !link ? `+ Imagen de Cabecera #${i + 1}` : ""}
                  <input
                    accept="image/*"
                    type="file"
                    onChange={(ev) => handleChange(ev, id, settingId)}
                    name="filename"
                  />
                </label>
                {link && module !== "landing" && (
                  <p className={Styles["dimenssion-logos"]}>
                    <span>Dimensiones</span>
                    <span>1280x350</span>
                  </p>
                )}
              </div>
            ))}
          </div>
        </Spin>
      </div>
    </>
  );
};

export default BannerSection;
