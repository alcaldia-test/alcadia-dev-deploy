import React, { useState, useCallback, useRef } from "react";

import Cropper from "react-easy-crop";

import Modal from "@components/Modals/SimpleModal";
import Button from "@components/Button";

import Styles from "../logos.module.scss";

let croppedAreaPixelsOut = {};

export const Cropping = ({
  imgurl = "",
  widthCrop = 8,
  saveCropped = () => {},
}) => {
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const canvas = useRef(null);

  const onCropComplete = useCallback((_, croppedAreaPixels) => {
    croppedAreaPixelsOut = croppedAreaPixels;
  }, []);

  const handleZoom = (plus) => {
    if (plus && zoom < 3) setZoom(zoom + 0.1);
    if (!plus && zoom > 1) setZoom(zoom - 0.1);

    if (zoom < 1) setZoom(1);
    if (zoom > 3) setZoom(3);
  };

  const heightCrop = 960 * (2.7 / widthCrop);

  const handleSave = () => {
    const node = canvas.current;
    const ctx = node.getContext("2d");

    const image = new Image();
    image.crossOrigin = "Anonymous";
    image.src = imgurl;

    const { width, height, x, y } = croppedAreaPixelsOut;

    image.onload = function () {
      try {
        ctx.drawImage(image, x, y, width, height, 0, 0, 960, heightCrop);

        const imgData = ctx.canvas.toDataURL();

        saveCropped(imgData);
      } catch (e) {
        console.log(e);
      }
    };
  };

  const handleCancel = () => {
    saveCropped(undefined);
  };

  return (
    <Modal visible={true} onCancel={handleCancel}>
      <canvas
        width={960}
        height={heightCrop}
        ref={canvas}
        style={{
          display: "none",
        }}
      ></canvas>
      <div className={Styles.cropper}>
        <Cropper
          image={imgurl}
          crop={crop}
          zoom={zoom}
          aspect={widthCrop / 2.7}
          onCropChange={setCrop}
          onCropComplete={onCropComplete}
          onZoomChange={setZoom}
          style={{
            containerStyle: {
              backgroundColor: "#000a",
            },
          }}
        />
      </div>
      <div className={Styles["zoom-buttons"]}>
        <Button
          onClick={handleZoom.bind({}, true)}
          className={Styles["custom-buttons"]}
          shape="circle"
          size="x-small"
        >
          +
        </Button>
        <Button
          onClick={handleZoom.bind({}, false)}
          className={Styles["custom-buttons"]}
          type="primary"
          shape="circle"
          size="x-small"
        >
          −
        </Button>
      </div>

      <div className={Styles["section-buttons"]}>
        <Button type="secondary" size="small" onClick={handleCancel}>
          Cancelar
        </Button>
        <Button size="small" onClick={handleSave}>
          Guardar
        </Button>
      </div>
    </Modal>
  );
};

export default Cropping;
