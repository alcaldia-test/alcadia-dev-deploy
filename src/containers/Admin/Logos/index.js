import React, { useState } from "react";

import Styles from "./logos.module.scss";
import { BannerSection, ReasonModal } from "./components";
import { useSelector, useDispatch } from "react-redux";
import { deletePTuotialStart } from "@redux/global/actions";
import message from "@components/Message";
import Button from "@components/Button";
import Spin from "@components/Spin";
import confirm from "@components/Modals/confirm";

const LogosContainer = () => {
  const { banners, tutorials } = useSelector((storage) => storage.global);
  const [tutorial, setTutorial] = useState(undefined);
  const [loading, setLoading] = useState(false);
  const disptach = useDispatch();
  const ladingFiles = [];
  const noLanding = [];

  const exitTutotial = tutorials.find(({ key }) => key === "proposal-tutorial");

  for (const banner of banners) {
    if (banner.key === "landing") {
      const files = banner?.fileStorages ?? [];
      for (let i = 0; i < 3; i++) {
        if (files[i]) ladingFiles.push(files[i]);
        else ladingFiles.push({ id: "", link: "" });
      }
    } else noLanding.push(banner);
  }

  const handleDeleteTuto = async () => {
    const result = await confirm("¿Seguro(a) en eliminar el tutorial?");

    if (!result) return;

    try {
      setLoading(true);

      const msg = await new Promise((resolve, reject) =>
        disptach(deletePTuotialStart({ id: exitTutotial.id, resolve, reject }))
      );
      setLoading(false);

      message.success(msg);
    } catch (error) {
      message.error(error);
    }
  };

  return (
    <Spin spinning={loading} size="large">
      {tutorial && (
        <ReasonModal
          tutorial={tutorial}
          onCancel={() => setTutorial(undefined)}
        />
      )}
      <div className={Styles["logos-container"]}>
        <div className={Styles["banner-module"]}>
          <BannerSection module="landing" files={ladingFiles} />
          {noLanding.map(({ key, fileStorages }, i) => (
            <BannerSection key={i} module={key} files={fileStorages} />
          ))}
          <div className={Styles["banners-item"]}>
            <h4>Video tutorial para crear propuestas ciudadanas</h4>
            <div className="flex">
              <Button
                type="primary"
                size="small"
                className="mr-2"
                onClick={() => setTutorial(exitTutotial ?? {})}
              >
                {exitTutotial
                  ? "Cambiar link del video"
                  : "Añadir link del video"}
              </Button>
              {exitTutotial && (
                <Button
                  onClick={handleDeleteTuto}
                  type="secondary"
                  size="small"
                >
                  Eliminar link
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
    </Spin>
  );
};

export default LogosContainer;
