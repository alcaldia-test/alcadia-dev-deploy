import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "@components/AntModal";
import Button from "@components/Button";
import styles from "./details.module.scss";
import { getSingleComment } from "@redux/complaints/actions";
import message from "@components/Message";
import Spin from "@components/Spin";

import ReasonModal from "./ReasonModal";

import ComplaintsList from "./ComplaintsList";
import Comment from "./Comment";

const moduleNameDic = {
  chapter: "Capítulos",
  normative: "normativas",
  proposal: "propuestas",
  discussion: "Charlas",
};

function DetailsModal({ user = {}, onCancel, module, setLoading: extLoading }) {
  const dispatch = useDispatch();
  const [comment, setComment] = useState({});
  const [loading, setLoading] = useState(false);
  const [current, setCurrent] = useState(undefined);

  // const handleClick = () => {
  //   let mod = "";
  //   let id = "";
  //   let query = "";

  //   if (module === "chapter") {
  //     mod = "normativas";
  //     id = user.normativeid;
  //   } else if (module === "normative") {
  //     mod = "normativas";
  //     id = user.postsid;
  //     query = "?posts=true";
  //   } else if (module === "proposal") {
  //     mod = "propuestas";
  //     id = user.proposalid;
  //   } else if (module === "discussion") {
  //     mod = "charlas";
  //     id = user.postsid;
  //     query = "?posts=true";
  //   }

  //   console.log(mod, id, query, user);
  // };

  useEffect(() => {
    startFetching();
  }, []);

  const startFetching = async () => {
    try {
      setLoading(true);

      const filterObj = {
        id: user.commentid ?? user.answerid,
        type: user.commentid ? "comment" : "answer",
      };

      const comment = await new Promise((resolve, reject) =>
        dispatch(getSingleComment({ ...filterObj, resolve, reject }))
      );

      setComment(comment ?? {});
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };

  const handleAction = async (lock) => {
    setCurrent({
      name: user.name,
      id: user.userid,
      commentId: user.commentid,
      answerId: user.answerid,
      lock,
      model: user.commentid ? "comment" : "answer",
    });
  };

  return (
    <>
      {current ? (
        <ReasonModal
          user={current}
          onCancel={onCancel}
          setLoading={extLoading}
        />
      ) : (
        <Modal
          className="simple-modal"
          style={{ minWidth: 780, borderRadius: 20 }}
          footer={false}
          visible={true}
          onCancel={onCancel}
        >
          <Spin spinning={loading}>
            <div className={styles["title-section"]}>
              <h3>Motivo de denuncia a {user.name}</h3>
              <span>
                Módulo: &nbsp;
                <a style={{ textTransform: "capitalize" }}>
                  {moduleNameDic[module]}
                </a>
              </span>
              {/* <a onClick={handleClick}>Ir a publicacion</a> */}
            </div>
            <div className={styles["modal-body-section"]}>
              <div className={styles["complaints-list"]}>
                <h6 className="mb-2">
                  Lista de denuncias para este comentario ({user.total})
                </h6>
                <ComplaintsList
                  count={user.total}
                  model={user.commentid ? "comment" : "answer"}
                  foreignId={user.commentid ?? user.answerid}
                />
              </div>
              <div className={styles.complainted}>
                <h6 className="text-center mb-2">Vista Previa</h6>
                <Comment user={{ ...comment, ...user }} />
              </div>
            </div>
            <div className={styles["section-buttons"]}>
              {!user.reviewed && (
                <Button
                  onClick={handleAction.bind({}, true)}
                  type="secondary"
                  size="x-small"
                >
                  Bloquear Usuario
                </Button>
              )}
              <div>
                <Button onClick={onCancel} type="secondary" size="x-small">
                  Cancelar
                </Button>
                {!user.reviewed && (
                  <Button
                    className="ml-2"
                    onClick={handleAction.bind({}, false)}
                    type="primary"
                    size="x-small"
                  >
                    Quitar comentario
                  </Button>
                )}
              </div>
            </div>
          </Spin>
        </Modal>
      )}
    </>
  );
}

export default DetailsModal;
