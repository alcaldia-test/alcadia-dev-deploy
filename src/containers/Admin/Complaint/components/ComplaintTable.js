import React, { useState } from "react";

import Table from "@components/Table";
import styles from "../complaint.module.scss";

import { UnknowDoc } from "@assets/icons";
// import confirm from "@components/Modals/confirm";
import DetailModal from "./DetailsModal";
import Tag from "@components/SimpleTag";

const DataTable = ({ module, complaints, setLoading }) => {
  const [current, setCurrent] = useState(undefined);

  const handleComment = async (data) => {
    setCurrent({ ...data.user, ...data });
  };

  // const handleShows = async (data) => {
  //   const msg = "Going to development other programmers";
  //   const result = await confirm(msg);

  //   console.log(result);
  // };

  return (
    <>
      {current && (
        <DetailModal
          setLoading={setLoading}
          user={current}
          module={module}
          onCancel={() => setCurrent(undefined)}
        />
      )}
      <Table
        data={complaints}
        columns={[
          {
            title: "Usuario",
            dataIndex: "user",
            render: (user) => {
              return (
                <div className={styles.userdata}>
                  <img src={user.avatar} alt="av" />
                  <span>{user.name}</span>
                </div>
              );
            },
          },
          {
            title: "Denuncias",
            dataIndex: "total",
            render: (cant) => (
              <div className={styles.complaints}>
                <UnknowDoc size="15" color="#FE4752" />
                <span>{cant}</span>
              </div>
            ),
          },
          {
            title: "Tipo",
            dataIndex: "type",
          },
          {
            title: "Estado",
            dataIndex: "reviewed",
            render: (cant) => (
              <Tag color={cant ? "#008A3D" : "#F8973E"}>
                {cant ? "Atendidas" : "Sin Atender"}
              </Tag>
            ),
          },
          {
            title: "Acciones",
            index: "action",
            render: (data) => (
              <>
                {/* <button
                  onClick={handleShows.bind({}, data)}
                  className="actionButton"
                >
                  <a>Ver Publicacion</a>
                </button> */}
                <button
                  onClick={handleComment.bind({}, data)}
                  className="actionButton ml-2"
                >
                  <a>Atender Denuncia</a>
                </button>
              </>
            ),
          },
        ]}
      />
    </>
  );
};

export default DataTable;
