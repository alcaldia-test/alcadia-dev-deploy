import React, { useState, useRef, useEffect } from "react";
import styles from "./details.module.scss";
import { Attached, Like, DisLike } from "@assets/icons";

function CommentComplait({ user = {} }) {
  const reference = useRef(null);
  const [readmore, setReadmore] = useState(false);
  const [reading, setReading] = useState(false);

  const handleClick = () => {
    const node = reference.current;

    if (reading) node.style.display = "-webkit-box";
    else node.style.display = "block";

    setReading(!reading);
  };

  useEffect(() => {
    setTimeout(handleReadmore, 100);
  }, []);

  const handleReadmore = () => {
    const node = reference.current;
    if (node.scrollHeight > 63) setReadmore(true);
  };

  return (
    <>
      <div className={styles["comment-modal"]}>
        <img src={user.avatar} alt="av" />
        <div>
          <span>{user.name}</span>
          <p ref={reference}>{user.content}</p>
          {readmore && (
            <em onClick={handleClick}>{reading ? "Leer menos" : "Leer más"}</em>
          )}
          {user.fileLink && (
            <a
              href="http://placehold.jp/3b5998/ffffff/150x150.png"
              target="_blank"
              download
              rel="noreferrer"
            >
              <Attached color="#fff" size="16" /> Ver archivo adjunto
            </a>
          )}
        </div>
      </div>
      <div className={`${styles["comment-section"]} flex justify-end mt-1`}>
        <span className="flex items-center">
          <Like color="#545454" size="22" />
          &nbsp; {user.likes}
        </span>
        <span className="flex items-center ml-2 mr-1">
          <DisLike color="#545454" size="22" /> &nbsp; {user.likes}
        </span>
      </div>
    </>
  );
}

export default CommentComplait;
