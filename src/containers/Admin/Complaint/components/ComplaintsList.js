import React, { useState, useEffect } from "react";
import styles from "./details.module.scss";
import Pagination from "@components/Pagination";
import message from "@components/Message";
import { useDispatch } from "react-redux";
import { getListComplaints } from "@redux/complaints/actions";
import Spin from "@components/Spin";
import moment from "moment";

function ComplaintList({ count = 0, foreignId, model }) {
  const [currentPage, setCurrentPage] = useState(1);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [complaints, setComplaints] = useState([]);

  useEffect(() => {
    getComaplaintsData();
  }, [currentPage]);

  const getComaplaintsData = async () => {
    try {
      setLoading(true);

      const filterObj = {
        limit: 4,
        skip: (currentPage - 1) * 4,
        id: foreignId,
        model,
      };

      const complains = await new Promise((resolve, reject) =>
        dispatch(getListComplaints({ ...filterObj, resolve, reject }))
      );

      setComplaints(complains ?? []);
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };

  return (
    <Spin spinning={loading}>
      {complaints.map(({ user, description, createdAt }, i) => (
        <React.Fragment key={i}>
          <div className="flex justify-between">
            <div className={styles["userdata-modal"]}>
              <img src={user.avatar} alt="av" />
              <span>{user.firstName}</span>
            </div>
            <div className={styles["datail-complaint"]}>
              <p>{description}</p>
              <span>{moment(createdAt).format("DD/MM/YYYY hh:mm:ss")}</span>
            </div>
          </div>
          <div className={styles["custom-divider"]}></div>
        </React.Fragment>
      ))}
      {count > 4 && (
        <div style={{ marginTop: (4 - complaints.length) * 53 }}>
          <Pagination
            defaultPageSize={4}
            size="small"
            total={count}
            current={currentPage}
            onChange={(page) => {
              setCurrentPage(page);
            }}
          />
        </div>
      )}
    </Spin>
  );
}

export default ComplaintList;
