import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";

import styles from "./complaint.module.scss";
import message from "@components/Message";
import { CharBar } from "@assets/icons";

import Pagination from "@components/Pagination";
import Button from "@components/Button";

import { requestComplaintsStart } from "@redux/complaints/actions";
import DataTable from "./components/ComplaintTable";
import Spin from "@components/Spin";

import SelectWraper from "@components/Select";

const optionsSelect1 = [
  { value: "proposal", label: "Propuestas Ciudadanas" },
  { value: "chapter", label: "Capítulos de Normativas" },
  { value: "normative", label: "Normativas" },
  { value: "discussion", label: "Charlas Virtuales" },
];

const optionsSelect2 = [
  { value: "comment", label: "Todos los Comentarios" },
  { value: "comment.takes", label: "Comentarios Atendidos" },
  { value: "comment.notTakes", label: "Comentarios Sin Atender" },
  { value: "answer", label: "Todos las Respuestas" },
  { value: "answer.takes", label: "Respuestas Atendidos" },
  { value: "answer.notTakes", label: "Respuestas Sin Atender" },
];

const optionsSelect3 = [
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más Antiguas" },
];

let withCount = true;
export default function UsersModuel() {
  const [type, setType] = useState("comment");
  const [order, setOrder] = useState("DESC");
  const [module, setModule] = useState("proposal");
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  const { datas, count } = useSelector((state) => state.complaints);

  useEffect(() => {
    getComaplaintsData();
  }, [currentPage, type, order, module]);

  const handleLoader = (loader, islock) => {
    if (!loader && islock) {
      withCount = true;
      getComaplaintsData();
    } else setLoading(loader);
  };

  const getComaplaintsData = async (filter = {}) => {
    try {
      setLoading(true);

      const filterObj = {
        ...filter,
        skip: (currentPage - 1) * 10,
        order,
        complaintType: type,
        module,
        withCount,
      };

      await new Promise((resolve, reject) =>
        dispatch(requestComplaintsStart({ ...filterObj, resolve, reject }))
      );
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };

  return (
    <section className={styles.filter__container}>
      <div className={`${styles.list__proposals__container} user-tabs`}>
        <div className="flex flex-column nowrap w-100 justify-between">
          <div className={styles.filter_select}>
            <div className="flex">
              <SelectWraper
                onChange={(e) => {
                  setModule(e[0].value);
                }}
                defaultValue={0}
                options={optionsSelect1}
              />
              &nbsp;&nbsp;
              <SelectWraper
                style={{ minWidth: 160 }}
                onChange={(e) => {
                  setType(e[0].value);
                }}
                defaultValue={0}
                options={optionsSelect2}
              />
              &nbsp;&nbsp;
              <SelectWraper
                style={{ minWidth: 160 }}
                onChange={(e) => {
                  withCount = false;
                  setOrder(e[0].value);
                }}
                defaultValue={0}
                options={optionsSelect3}
              />
            </div>
            <Link href="/admin/estadisticas/denuncias">
              <a>
                <Button type="secondary">
                  Estadísticas Totales &nbsp;&nbsp;
                  <CharBar color="#333" size="20" />
                </Button>
              </a>
            </Link>
          </div>
        </div>
        <Spin spinning={loading} size="large">
          <DataTable
            module={module}
            setLoading={handleLoader}
            complaints={datas}
          />
        </Spin>
      </div>
      <Pagination
        total={count}
        current={currentPage}
        onChange={(page) => {
          withCount = false;
          setCurrentPage(page);
        }}
      />
    </section>
  );
}
