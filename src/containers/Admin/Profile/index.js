import React from "react";

import style from "./profile.module.scss";
import { useSelector } from "react-redux";

import { User, Email } from "@assets/icons";

const ContainerProfile = () => {
  const {
    dataUser: { firstName, avatar, email },
  } = useSelector((storage) => storage.auth);

  return (
    <div className={style["profile-module"]}>
      <div className={style["avatar-section"]}>
        <img
          src={avatar ?? "https://robohash.org/kjhsdlfa.png?size=150x150"}
          alt="avatar"
        />
        <h3>Mi Cuenta</h3>
        <span>✓</span>
      </div>

      <div className={style["input-section"]}>
        <div>
          <label>
            Nombre de usuario
            <input
              type="text"
              defaultValue={firstName ?? "Sin Nombre"}
              disabled
            />
          </label>
          <span>
            <User size="20" color="#000b" />
          </span>
        </div>
        <div>
          <label>
            Correo Electrónico
            <input
              type="text"
              defaultValue={email ?? "some@example.com"}
              disabled
            />
          </label>
          <span>
            <Email size="18" color="#000b" />
          </span>
        </div>
      </div>
    </div>
  );
};

export default ContainerProfile;
