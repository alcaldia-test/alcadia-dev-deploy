import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Inputs from "../ControlsInputs";
import Pagination from "@components/Pagination";
import {
  getFilterDiscussionsAdmin,
  getZone,
  getMacroZones,
} from "@redux/discussions/actions";

const DashboardContainer = ({ permissions, discussions, children }) => {
  const [filterData, setFilterData] = useState({
    status: null,
    order: "DESC",
    text: "",
    offset: 1,
  });
  const optionsAutocomplete =
    discussions?.length > 0
      ? discussions?.map((item) => ({
          title: item.title,
        }))
      : [];

  const limit = 8;
  const { discussionsCount, zones, macroZones, reloadCards } = useSelector(
    (state) => state.discussion
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      getFilterDiscussionsAdmin({
        status:
          filterData.status === "creacion"
            ? "creating"
            : filterData.status === "innactivas"
            ? "finished"
            : filterData.status === "activas"
            ? "active"
            : null,
        order: filterData.order.toUpperCase(),
        content: filterData.text,
        offset: filterData.offset === 1 ? 0 : (filterData.offset - 1) * limit,
      })
    );
  }, [filterData, reloadCards]);

  useEffect(() => {
    zones?.length === 0 && dispatch(getZone());
    macroZones?.length === 0 && dispatch(getMacroZones());
  }, []);

  return (
    <div className="bg-white mb-96">
      <Inputs
        permissions={permissions}
        statusSelector={(status) =>
          setFilterData({
            ...filterData,
            status,
            offset: 1,
          })
        }
        orderSelector={(order) =>
          setFilterData({
            ...filterData,
            order,
            offset: 1,
          })
        }
        searchInput={(text) =>
          setFilterData({
            ...filterData,
            text,
            offset: 1,
          })
        }
        optionsAutocomplete={optionsAutocomplete}
      />
      <div className="flex flex-wrap justify-around p-1 gap-1">{children}</div>
      {discussionsCount > 8 && (
        <Pagination
          total={discussionsCount}
          pageSize={8}
          onChange={(e) => setFilterData({ ...filterData, offset: e })}
        />
      )}
    </div>
  );
};

export default DashboardContainer;
