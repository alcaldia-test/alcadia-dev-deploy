import React, { useState, useEffect, memo } from "react";
import ErrorModal from "@components/ErrorModal";
import Image from "next/image";
import DeleteModal from "@components/deleteModal";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { IconTextModal } from "@components/Modals";
import { useRouter } from "next/router";
import {
  creatingDiscussion,
  errorMessages,
  modalSaveOnExit,
} from "@redux/createDiscussion/actions";

function ModalSave() {
  const router = useRouter();
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const { localPost, showModalCreated, messagesError, saveOnExit } =
    useSelector((state) => state.creatingDiscussion);
  const [showDeleteModal, setShowDeleteModal] = useState(saveOnExit);

  const modalDeleteOk = () => {
    dispatch(creatingDiscussion({ ...localPost, completed: false }));
  };
  useEffect(() => {
    messagesError && messagesError.length > 0 && setShowModal(true);
  }, [messagesError]);

  useEffect(() => {
    setShowDeleteModal(saveOnExit);
  }, [saveOnExit]);

  return (
    <>
      {showDeleteModal && (
        <DeleteModal
          onlyClose={() => {
            dispatch(modalSaveOnExit(false));
          }}
          closeModal={() => {
            router.push(`/admin/charlas`);
            dispatch(modalSaveOnExit(false));
          }}
          mainMessage="¿Desea guardar los cambios efectuados hasta aquí?"
          delteFuntion={modalDeleteOk}
          simple={true}
          mainIconSrc="/icons/save.svg"
        />
      )}
      {showModal && (
        <ErrorModal
          Message={messagesError}
          closeModal={() => {
            dispatch(errorMessages([]));
            setShowModal(false);
          }}
        />
      )}
      <IconTextModal
        visible={showModalCreated}
        cancelText={""}
        okText={
          <Link href={`/admin/charlas`}>
            <a className="pt-20">Ok</a>
          </Link>
        }
        closable={false}
        image={
          <Image
            alt="okIcon"
            src="/icons/successAction.svg"
            width={50}
            height={50}
            layout="fixed"
          />
        }
        title={localPost?.id ? "Charla editada" : "Charla creada"}
        description=""
      ></IconTextModal>
    </>
  );
}

export default memo(ModalSave);
