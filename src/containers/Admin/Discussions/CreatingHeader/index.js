import React from "react";
import Router from "next/router";
import Stepper from "@components/Stepper";
import styles from "./creatingHeader.module.scss";
import { modalSaveOnExit } from "@redux/createDiscussion/actions";
import { useDispatch } from "react-redux";
import { LeftOutlined } from "@ant-design/icons";

const CreatingHeader = ({ step = 1, localPost }) => {
  const dispatch = useDispatch();

  const handleStepper = (step) => {
    Router.push(`/admin/charlas/creando/paso-${step + 1}`);
  };

  const handleExit = () => {
    if (localPost && Object.keys(localPost).length > 0) {
      dispatch(modalSaveOnExit(true));
    } else {
      Router.push(`/admin/charlas/`);
    }
  };

  return (
    <header className={styles.container}>
      <div className={styles.controls}>
        <div className={styles.leftButtons}>
          <span className={styles.button} onClick={handleExit}>
            <LeftOutlined /> Salir
          </span>
          <span className={styles.textHeader}>Creaci&oacute;n de Charla</span>
        </div>
        <div className={styles.rightButtons}>
          Guardando...{" "}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="24"
            height="24"
          >
            <path fill="none" d="M0 0h24v24H0z" />
            <path
              d="M18 19h1V6.828L17.172 5H16v4H7V5H5v14h1v-7h12v7zM4 3h14l2.707 2.707a1 1 0 0 1 .293.707V20a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1zm4 11v5h8v-5H8z"
              fill="rgba(133,133,133,1)"
            />
          </svg>
        </div>
      </div>
      <div className={styles.stepperContainer}>
        <Stepper steps={[1, 2, 3, 4]} step={step} setStep={handleStepper} />
      </div>
    </header>
  );
};

export default CreatingHeader;
