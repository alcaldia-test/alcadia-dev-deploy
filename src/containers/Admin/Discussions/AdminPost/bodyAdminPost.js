import React, { memo } from "react";
import stylesUser from "../../../DiscussionsUser/Description/charlasDescription.module.scss";
import BottomActions from "@components/BottomActions";
import Skeleton from "@components/Skeleton";
import { DownloadOutlined } from "@ant-design/icons";
import ZonesAllowed from "./zonesAllowed";
import { getNearly } from "@utils/zoneValidator";
import { parseDraft } from "@utils/draftToHtml";

const BodyAdminPost = ({ _props: { localPost, favorites, dataAuth } }) => {
  const { zonesId, title, content, extraFile } = localPost;
  const isNearly = getNearly(zonesId, favorites);

  return localPost ? (
    <>
      <h2 className={`${stylesUser.main_title} font-bold tracking-normal `}>
        {title?.value}
      </h2>
      {/* <SocialNetwork url={url} comments={0} month={month} day={day} /> */}
      <div className="flex justify-between mb-5">
        <ZonesAllowed localPost={localPost} />
        {/* <DiscussionEnd localPost={localPost} /> */}
      </div>

      <div
        className={`${stylesUser.main_text_description} mb-30 mt-5 `}
        dangerouslySetInnerHTML={{
          __html: parseDraft(content?.value || content),
        }}
      />
      {extraFile?.value && (
        <div className="text-center">
          <div className="my-96 py-96">
            <a
              target="_blank"
              className={stylesUser.downLoadButton}
              rel="noreferrer"
            >
              {extraFile?.value?.name} <DownloadOutlined />
            </a>
          </div>
        </div>
      )}

      <div className={`${stylesUser.buttons_actions} text-center`}>
        <div className={`${stylesUser.button_actions} mx-auto`}>
          <BottomActions
            bottomProps={{
              inSide: false,
              isAdmin: true,
              dataAuth,
              isNearly,
              localPost,
              title: "¿Estás de acuerdo?",
              titleStats: "Participaciones de la Charla",
              bodyMessage: {
                yes: (
                  <>
                    ¿Estás de <span>acuerdo</span> con el tema de la charla?
                  </>
                ),
                no: (
                  <>
                    ¿Estás en <span>desacuerdo</span> con el tema de la charla?
                  </>
                ),
              },
            }}
          />
        </div>
      </div>
    </>
  ) : (
    <Skeleton />
  );
};

export default memo(BodyAdminPost);
