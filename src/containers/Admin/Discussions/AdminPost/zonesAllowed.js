import React, { useState } from "react";
import ZonesModal from "@components/Modals/ZonesModal/MacrosModal";

function ZonesAllowed({ localPost }) {
  const [showZonesModal, setShowZonesModal] = useState(false);
  const { zonesId } = localPost;

  return (
    <>
      <div className="flex gap-3 items-center text-body">
        {localPost.zonesId?.value ? (
          <div
            className="bg-stateConfirmed p-2 rounded-sm text-zones cursor-pointer font-bold"
            onClick={() => setShowZonesModal(true)}
          >
            Ver zonas válidas
          </div>
        ) : (
          <div className="bg-stateConfirmed p-2 rounded-sm text-zones  font-bold">
            Todas las zonas pueden participar
          </div>
        )}
      </div>
      {showZonesModal && (
        <ZonesModal
          closable={true}
          onCancel={() => setShowZonesModal(false)}
          macros={zonesId?.value}
        />
      )}
    </>
  );
}

export default ZonesAllowed;
