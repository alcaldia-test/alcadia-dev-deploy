import React from "react";
import Link from "next/link";
import ButtonComponent from "@components/Button";
import ButtonStats from "@components/Button/stats";

export default function ButtonsHeader({ post }) {
  return (
    <div className=" h-5  flex justify-between">
      <Link href="/admin/charlas">
        <a>
          <ButtonComponent size="super-small" type="secondary">
            <div className="flex">
              <span className="text-callout">{"< Volver"}</span>
            </div>
          </ButtonComponent>
        </a>
      </Link>
      {post.state !== "CREATING" && (
        <Link href={`/admin/estadisticas/charlas/${post.id}`}>
          <a>
            <ButtonStats size="super-small" type="secondary" />
          </a>
        </Link>
      )}
    </div>
  );
}
