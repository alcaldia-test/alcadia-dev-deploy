import React, { useEffect, useState } from "react";
import ButtonsHeader from "./buttonsHeader";
import Wrapped from "@containers/Wrapper/WrapperLogin";
import Spin from "@components/Spin";
import moment from "moment";
import Styles from "./adminPost.module.scss";
import { useSelector, useDispatch } from "react-redux";
import BodyAdminPost from "./bodyAdminPost";
import Comments from "@components/Comments";
import { editPost } from "@redux/createDiscussion/actions";

function AdminPost() {
  const { localPost, loading } = useSelector(
    (state) => state.creatingDiscussion
  );
  const dataAuth = useSelector((state) => state.auth);
  const [ncomments, setNcomments] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    localPost.state = setStatus(localPost);
    setNcomments(localPost.comments);
  }, [localPost]);

  return (
    <Wrapped
      seo={localPost?.title?.value}
      description={localPost?.content?.value}
    >
      <div className={Styles.voteContainer}>
        <ButtonsHeader post={localPost} />
        <Spin spinning={loading} tip={`Cargando ...`} size="large">
          <div className="mt-5">
            <BodyAdminPost _props={{ localPost, favorites: [], dataAuth }} />
          </div>
        </Spin>
        <Comments
          isAdmin
          ncomments={ncomments}
          onTakeComplaint={() => {
            dispatch(editPost({ id: localPost?.id }));
          }}
          module="posts"
          updateComments={setNcomments}
          mid={localPost?.postsId?.value}
        />
      </div>
    </Wrapped>
  );
}

function setStatus(discussion) {
  switch (discussion.completed) {
    case true:
      if (moment()._d < moment(discussion.dueDate)._d) {
        return "FINISHED";
      } else {
        return "PUBLISHED";
      }
    default:
      return "CREATING";
  }
}

export default AdminPost;
