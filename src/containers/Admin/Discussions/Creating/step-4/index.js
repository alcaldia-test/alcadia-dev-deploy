import React from "react";
import ButtonComponent from "@components/Button";
import StepsContainer from "../StepsContainer";
import CreatingHeader from "../../CreatingHeader";
import styles from "./preview.module.scss";
import Spin from "@components/Spin";
import BodyAdminPost from "../../AdminPost/bodyAdminPost";
import { setDataDispatch } from "@utils/validateFields";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";

import {
  creatingDiscussion,
  changeDataLocalPost,
} from "@redux/createDiscussion/actions";

const StepFour = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const dataAuth = useSelector((state) => state.auth);
  const { localPost, loading } = useSelector(
    (state) => state.creatingDiscussion
  );
  const { title, dueDate, content, tags, zonesId, completed } = localPost;
  const setDispatch = (name, value) =>
    setDataDispatch(dispatch, changeDataLocalPost, name, value);

  // before step validation
  !title && setDispatch("title", title?.value);
  !dueDate && setDispatch("dueDate", dueDate?.value);
  !content && setDispatch("content", content?.value);
  !tags && setDispatch("tags", tags?.value);
  !zonesId && setDispatch("zonesId", undefined);

  const savePost = (completed) => {
    dispatch(creatingDiscussion({ ...localPost, completed }));
    if (title.error) {
      router.push(`/admin/charlas/creando/paso-1`);
    }
  };
  console.warn("Localpost en paso 4:", localPost);

  return (
    <Spin spinning={loading} tip={`Guardando ...`} size="large">
      <StepsContainer>
        <CreatingHeader step={4} localPost={localPost} />
        <div className="font-bold text-center text-H4 mb-6 mt-3">
          Previsualización de la Charla Virtual
        </div>
        <section className={styles.section}>
          <BodyAdminPost _props={{ localPost, favorites: [], dataAuth }} />
        </section>

        <div className="flex m-5 justify-around">
          {!completed && (
            <ButtonComponent
              size="super-small"
              type="link"
              onClick={() => savePost(false)}
            >
              Terminar más tarde
            </ButtonComponent>
          )}
          <ButtonComponent
            size="super-small"
            type="primary"
            onClick={() => savePost(true)}
            style={{ padding: "5px 70px" }}
          >
            Publicar
          </ButtonComponent>
        </div>
      </StepsContainer>
    </Spin>
  );
};

export default StepFour;
