import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Button from "@components/Button";
import RadialComponent from "@components/RadioGroup";
import SelectListComponenet from "@components/SelectList";
import TagContainer from "@components/TagContainer";
import styles from "./step-3.module.scss";
import CreatingHeader from "../../CreatingHeader";
import StepsContainer from "../StepsContainer";
import { changeDataLocalPost } from "@redux/createDiscussion/actions";
import Link from "next/link";
import { setDataDispatch } from "@utils/validateFields";

const _optionsMenu = [
  { id: "1", name: "Disponible para todas las zonas" },
  { id: "2", name: "Zonal" },
];

const StepThree = () => {
  const { localPost } = useSelector((state) => state.creatingDiscussion);
  const { title, dueDate, content, zonesId, tags } = localPost;
  const { macroZones } = useSelector((state) => state.discussion);
  const [macroZonesOrdered, setMacroZonesOrdered] = useState([]);
  console.warn("Desde reducer paso 3:", localPost);
  const [isZoneSelected, setIsZoneSelected] = useState(
    "Disponible para todas las zonas"
  );

  const dispatch = useDispatch();
  const setDispatch = (name, value) =>
    setDataDispatch(dispatch, changeDataLocalPost, name, value);
  // before step validation
  !title && setDispatch("title", title?.value);
  !dueDate && setDispatch("dueDate", dueDate?.value);
  !content && setDispatch("content", content?.value);

  useEffect(() => {
    if (zonesId?.value) {
      setIsZoneSelected("Zonal");
    }
  }, [localPost]);

  useEffect(() => {
    const _macroZonesOrdered = [];

    macroZones.forEach((macroZone) => {
      const _zones = [];
      let checkedMainZone = false;

      macroZone?.zones?.forEach((zone) => {
        let checked = false;
        zonesId?.value?.forEach((zonesInLocalPost) => {
          const isChecked = zonesInLocalPost?.selected?.find(
            (z) => z.id === zone.id
          );

          if (isChecked) {
            checked = true;
            checkedMainZone = true;
          }
        });
        _zones.push({ id: zone.id, zone: zone.name, checked });
      });

      const _macroZone = {
        id: macroZone.id,
        name: macroZone.name,
        zones: _zones,
        checked: checkedMainZone,
      };
      _macroZonesOrdered.push(_macroZone);
    });
    setMacroZonesOrdered(_macroZonesOrdered);
  }, [macroZones, localPost]);

  const handleChangeZone = (e) => {
    if (e?.target?.id === "2") {
      setDispatch("zonesId", []);
    } else if (e?.target?.id === "1") {
      setDispatch("zonesId", undefined);
    }
  };

  const handleSelectZones = (e) => {
    console.warn("Click check", e);
    const _zonesId = zonesId?.value?.filter((x) => x.id !== e?.id) || [];
    if (e?.selected.length > 0) {
      _zonesId.push(e);
    }
    setDispatch("zonesId", _zonesId);
  };

  const handleChangeTags = (tagList) => {
    setDispatch("tags", tagList);
  };

  const handleNext = () => {
    (tags === undefined || !tags?.value?.length > 0) && setDispatch("tags", []);
    zonesId === undefined && setDispatch("zonesId", undefined);
  };

  return (
    <StepsContainer>
      <CreatingHeader step={2} localPost={localPost} />
      <section className={styles.section}>
        <div className="flex flex-col gap-3 p-2">
          <div className="font-bold text-H5 text-center mb-2">
            Ya casi terminamos, ¿Esta charla será zonal o pueden participar los
            ciudadanos de todas la zonas?
          </div>
          <span className="text-bodyMedium">
            Establece la petición para zonas especificas de La Paz o configurala
            para que todos los ciudadanos de La Paz puedan participar
          </span>

          <div className=" w-1/6">
            <RadialComponent
              onChange={handleChangeZone}
              options={_optionsMenu}
              valueDefault={isZoneSelected}
            />
          </div>
          {zonesId?.value && (
            <div className="mx-3">
              <SelectListComponenet
                onSelect={handleSelectZones}
                list={macroZonesOrdered}
                showZones={false}
              />
            </div>
          )}
          {zonesId?.error && (
            <span className="text-redDart mt-3">
              * Elija una opción por favor
            </span>
          )}

          <span className="h-50" />
          <span className="font-bold text-H5">
            Selecione o añada etiquetas relacionadas a esta charla
          </span>
          <TagContainer
            onSelect={handleChangeTags}
            selectedtag={tags?.value || []}
          />
          {tags?.error && (
            <span className="text-redDart mt-3">
              * Añada al menos una etiqueta por favor
            </span>
          )}

          <div className="flex items-center justify-center">
            <Button type="primary" size="large" onClick={handleNext}>
              <Link href="/admin/charlas/creando/paso-4">Siguiente</Link>
            </Button>
          </div>
        </div>
      </section>
    </StepsContainer>
  );
};
export default StepThree;
