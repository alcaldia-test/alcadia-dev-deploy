import React from "react";
import {
  changeDataLocalPost,
  deleteFile,
} from "@redux/createDiscussion/actions";
import StepsContainer from "../StepsContainer";
import CreatingHeader from "../../CreatingHeader";
import Button from "@components/Button";
import WYSIWYG from "@components/WYSIWYG";
import { useSelector, useDispatch } from "react-redux";
import UploadComponent from "@components/Upload";
import styles from "./step-2.module.scss";
import Link from "next/link";
import { setDataDispatch } from "@utils/validateFields";

const StepTwo = () => {
  const { localPost } = useSelector((state) => state.creatingDiscussion);
  const { title, dueDate, postsId, content, extraFile, attached } = localPost;
  console.log("Localpost paso 2", localPost);

  const dispatch = useDispatch();
  const setDispatch = (name, value) =>
    setDataDispatch(dispatch, changeDataLocalPost, name, value);
  // before step validation
  !title && setDispatch("title", title?.value);
  !dueDate && setDispatch("dueDate", dueDate?.value);

  const handleChange = (e) => {
    setDispatch("content", e);
  };

  const handleChangeFile = (info) => {
    console.warn("handleChangeFile:", info);
    if (info.file.status === "uploading") {
      const _file = info?.file;
      _file.status = "done";
      setDispatch("extraFile", _file);
    }

    if (info.file.status === "removed") {
      setDispatch("extraFile", undefined);
    }
  };

  const handleOnDeleteFile = (tag) => {
    console.warn("handleOnDeleteFile", tag);
    const payload = {
      tag: "attached",
      id: postsId?.value,
      PostionFieldName: "attached",
    };
    dispatch(deleteFile(payload));
  };
  const handleNext = () => {
    setDispatch("content", content?.value);
  };

  return (
    <StepsContainer>
      <CreatingHeader step={1} localPost={localPost} />
      <section className={styles.section}>
        <h6>Explica la idea de la charla</h6>
        <p>
          Las explicaciones de charlas más exitosas suelen tener 3 párrafos de
          largo. Te recomendamos que agregues aproximadamento{" "}
          <span className="text-redDart">1000</span> caracteres
        </p>
        <div>
          <WYSIWYG
            onChange={handleChange}
            row={5}
            value={content?.value || {}}
            rawContentState={content?.value || {}}
          />
        </div>
        {content?.error && (
          <span className="text-redDart mt-3">
            * Su contenido debe tener al menos 100 caracteres y menos de 6000
          </span>
        )}
        <div className={styles.uploadSection}>
          <h6>Puede subir archivos tambien</h6>
          <p>
            Si es necesario mostrar a los ciudadanos más información puede subir
            los documentos acá para que puedan descargarlos.
          </p>
          <UploadComponent
            onChange={handleChangeFile}
            sizeButton="small-x"
            fileList={extraFile?.value ? [extraFile?.value] : []}
            filesInServer={attached ? [attached.value] : []}
            allowAddMoreToServer={false}
            onDeleteFunction={handleOnDeleteFile}
            accept=".pdf"
          />
        </div>

        <Button type="primary" size="large" onClick={handleNext}>
          <Link href="/admin/charlas/creando/paso-3">Siguiente</Link>
        </Button>
      </section>
    </StepsContainer>
  );
};

export default StepTwo;
