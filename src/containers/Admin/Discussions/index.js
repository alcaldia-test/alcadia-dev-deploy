import React, { useEffect, useState } from "react";
import DashboardContainer from "./DashboardCardsContainer";
import DeleteModal from "@components/deleteModal";
import Spin from "@components/Spin";
import Router from "next/router";
import Card from "@components/Cards/CardNormativesAdmin";
import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import { deleteDiscussion, editPost } from "@redux/createDiscussion/actions";
import { checkPermision } from "@utils/adminPermission";

const Discussions = () => {
  const { dataUser } = useSelector((state) => state.auth);
  const { discussions } = useSelector((data) => data.discussion);
  const { loader } = useSelector((data) => data.common);
  const [showModal, setShowModal] = useState(false);
  const [deleteModalProperties, setDeleteModalProperties] = useState({
    title: "",
    id: null,
  });
  const dispatch = useDispatch();

  const handleDetele = ({ title, id }) => {
    setShowModal(true);
    setDeleteModalProperties({ title, id });
  };
  const handleEdit = (id) => {
    dispatch(editPost({ id }));
    Router.push("/admin/charlas/creando/paso-1");
  };

  const [permissions, setPermissions] = useState({
    canDelete: false,
    canCreate: false,
    canEdit: false,
  });

  useEffect(() => {
    const _permissions = checkPermision(dataUser, "discussion");
    setPermissions(_permissions);
  }, [dataUser]);

  return (
    <>
      {showModal && (
        <DeleteModal
          closeModal={() => setShowModal(false)}
          title={deleteModalProperties.title}
          delteFuntion={() => {
            dispatch(
              deleteDiscussion({
                id: deleteModalProperties.id,
              })
            );
            setShowModal(false);
          }}
        />
      )}
      <Spin spinning={loader} tip={`Cargando ...`} size="large">
        <DashboardContainer permissions={permissions} discussions={discussions}>
          {discussions?.length > 0 ? (
            discussions.map((discussion) => {
              discussion.state = setStatus(discussion);
              return (
                <Card
                  permissions={permissions}
                  key={discussion.id}
                  id={discussion.id}
                  title={discussion.title}
                  content={discussion.content}
                  state={discussion.state}
                  comments={discussion.comments}
                  complaints={discussion.complaints}
                  handleDelete={handleDetele}
                  handleEdit={() => handleEdit(discussion.id)}
                  handleStatistics={() =>
                    Router.push(`/admin/estadisticas/charlas/${discussion.id}`)
                  }
                  handleAdmin={() =>
                    Router.push(`/admin/charlas/${discussion.id}`)
                  }
                />
              );
            })
          ) : (
            <span className="text-H5">No hubo resultados</span>
          )}
        </DashboardContainer>
      </Spin>
    </>
  );
};

function setStatus(discussion) {
  switch (discussion.completed) {
    case true:
      if (moment()._d > moment(discussion.dueDate)._d) {
        return "FINISHED";
      } else {
        return "PUBLISHED";
      }

    default:
      return "CREATING";
  }
}

export default Discussions;
