import React, { memo } from "react";
import StepsContainer from "../StepsContainer";
import CreatingHeader from "../../CreatingHeader";
import Button from "@components/Button";
import TagContainer from "@components/TagContainer";
import WYSIWYG from "@components/WYSIWYG";
import UploadComponent from "@components/Upload";
import styles from "./step-2.module.scss";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import {
  changeDataLocalPost,
  deleteFile,
} from "@redux/createNormative/actions";
import { setDataDispatch } from "@utils/validateFields";

const StepTwo = () => {
  const { localPost } = useSelector((state) => state.creatingNormative);
  const { title, dueDate, postsId, content, extraFile, attached, tags } =
    localPost;
  const dispatch = useDispatch();
  const setDispatch = (name, value) =>
    setDataDispatch(dispatch, changeDataLocalPost, name, value);

  // before step validation
  !title && setDispatch("title", title?.value);
  !dueDate && setDispatch("dueDate", dueDate?.value);

  const handleChangeContent = (e) => {
    setDispatch("content", e);
  };
  const handleChangeFile = (info) => {
    if (info.file.status === "uploading") {
      const _file = info?.file;
      _file.status = "done";
      setDispatch("extraFile", _file);
    }

    if (info.file.status === "removed") {
      setDispatch("extraFile", undefined);
    }
  };

  const handleOnDeleteFile = (tag) => {
    console.warn("handleOnDeleteFile", tag);
    const payload = {
      tag: "attached",
      id: postsId?.value,
      PostionFieldName: "attached",
    };
    dispatch(deleteFile(payload));
  };
  const handleChangeTags = (tagList) => {
    console.warn("handleChangeTags", tagList);
    setDispatch("tags", tagList);
  };
  const handleNext = () => {
    setDispatch("content", content?.value);
    setDispatch("tags", tags?.value);
  };

  return (
    <StepsContainer>
      <CreatingHeader step={1} localPost={localPost} />
      <section className={styles.section}>
        <h6>Redacta una breve introducción a la normativa</h6>
        <p>
          Las introducciones más exitosas suelen tener 3 párrafos de largo. Te
          recomendamos que agregues aproximadamento{" "}
          <span className="text-redDart">1000</span> caracteres
        </p>
        <div>
          <WYSIWYG
            onChange={handleChangeContent}
            row={5}
            value={content?.value || {}}
            rawContentState={content?.value || {}}
          />
        </div>
        {content?.error && (
          <span className="text-redDart mt-3">
            * Su contenido debe tener al menos 100 caracteres y menos de 6000
          </span>
        )}
        <div className={styles.uploadSection}>
          <h6>Puede subir archivos tambien</h6>
          <p>
            Si es necesario mostrar a los ciudadanos más información puede subir
            los documentos acá para que puedan descargarlos.
          </p>
          <UploadComponent
            onChange={handleChangeFile}
            sizeButton="small-x"
            fileList={extraFile?.value ? [extraFile?.value] : []}
            filesInServer={attached ? [attached.value] : []}
            allowAddMoreToServer={false}
            onDeleteFunction={handleOnDeleteFile}
            accept=".pdf"
          />
        </div>

        <span className="h-50" />
        <h6>Añada etiquetas relacionadas a esta charla</h6>
        <TagContainer
          onSelect={handleChangeTags}
          selectedtag={tags?.value || []}
        />
        {tags?.error && (
          <span className="text-redDart mt-3">
            * Añada al menos una etiqueta por favor
          </span>
        )}

        <Button type="primary" size="large" onClick={handleNext}>
          <Link href="/admin/normativas/creando/paso-3">Siguiente</Link>
        </Button>
      </section>
    </StepsContainer>
  );
};

export default memo(StepTwo);
