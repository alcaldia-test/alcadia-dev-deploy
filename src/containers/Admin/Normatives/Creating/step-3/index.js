import React, { useState, useEffect } from "react";
import StepsContainer from "../StepsContainer";
import CreatingHeader from "../../CreatingHeader";
import styles from "./step-3.module.scss";
import AddChapterTitle from "./Components/AddChapterTitle";
import ChaptersList from "./Components/ChaptersList";
import ChapterEditor from "./Components/ChapterEditor";
import { useDispatch, useSelector } from "react-redux";
import { changeDataLocalPost } from "@redux/createNormative/actions";
import { setDataDispatch } from "@utils/validateFields";

const StepThree = () => {
  const { localPost } = useSelector((state) => state.creatingNormative);
  const [chapters, setChapters] = useState(localPost?.chapters?.value);
  const { title, dueDate, content, tags } = localPost;
  console.warn("Reducer paso 3 render?", localPost);

  const [showTitleComponent, setShowTitleComponent] = useState(false);
  const [index, setIndex] = useState(0);
  const [isEditing, setIsEditing] = useState(false);
  const dispatch = useDispatch();
  const setDispatch = (name, value) =>
    setDataDispatch(dispatch, changeDataLocalPost, name, value);

  // before step validation
  !title && setDispatch("title", title?.value);
  !dueDate && setDispatch("dueDate", dueDate?.value);
  !content && setDispatch("content", content?.value);
  !tags && setDispatch("tags", tags?.value);

  // (R) al añadir el título, lo guarda en chapters un array local
  const handleAdd = (chapterTitle) => {
    const chapterToAdd = {
      cod: chapters?.length > 0 ? chapters.length : 0,
      normativeId: undefined,
      id: "",
      title: chapterTitle,
      articles: [
        {
          title: chapterTitle,
        },
      ],
    };
    chapters
      ? setChapters([...chapters, chapterToAdd])
      : setChapters([chapterToAdd]);
    setShowTitleComponent(false);
  };

  // simplemente añade el index, y el editing en true
  const editChapter = (index) => {
    setIndex(index);
    setIsEditing(true);
  };

  // (R) cada vez que cambia chapters, lo enviamos a localpost
  useEffect(() => {
    console.warn("CAMBIA chapters", chapters);
    chapters?.length > 0 &&
      setDispatch(
        "chapters",
        chapters.map((chapter, i) => ({
          ...chapter,
          cod: i,
          articles: chapter.articles.map((article) => ({ ...article, cod: i })),
        }))
      );
  }, [JSON.stringify(chapters)]);

  return (
    <StepsContainer>
      <CreatingHeader step={2} localPost={localPost} />
      {localPost && (
        <section className={styles.section}>
          {showTitleComponent && (
            <AddChapterTitle
              props={{
                styles,
                chapters,
                setShowTitleComponent,
                handleAdd,
              }}
            />
          )}

          {!isEditing ? (
            <ChaptersList
              props={{
                chapters,
                setShowTitleComponent,
                editChapter,
                localPost,
                setDispatch,
              }}
            />
          ) : (
            <ChapterEditor
              props={{
                styles,
                index,
                setIsEditing,
                chapters,
                setDispatch,
                localPost,
              }}
            />
          )}
        </section>
      )}
    </StepsContainer>
  );
};

export default StepThree;
