import React, { useState } from "react";
import { PlusOutlined } from "@ant-design/icons";
import ArticleEditor from "./ArticleEditor";
import Button from "@components/Button";
import styles from "./articles.module.scss";
const contentParsed = (content) => {
  try {
    return JSON.parse(content);
  } catch (error) {
    return content;
  }
};

const ArticlesList = ({ index, chapters, setDispatch, setIsEditing }) => {
  const [openCollapses, setOpenCollapses] = useState(Date.now());
  const [articles, setArticles] = useState(chapters[index].articles || []);

  const updateChapters = () => {
    chapters[index] = {
      ...chapters[index],
      cod: chapters[index].cod,
      articles: articles.map((article, i) => {
        const articleToReturn = {
          status: true,
          title: chapters[index].title,
          cod: i,
          chapterId: chapters[index].id || "",
          content: article.content,
        };
        if (article.id) {
          articleToReturn.id = article.id;
        }
        return articleToReturn;
      }),
    };
    setDispatch("chapters", chapters);
  };

  const handleEditor = (el) => {
    articles[el.i].content = JSON.stringify(el.data);
    setArticles([...articles]);
  };

  const deleteArticle = (i) => {
    articles.splice(i, 1);
    setArticles([...articles]);
  };

  // añade un articulo vacío
  const AddArticle = () => {
    setArticles([
      ...articles,
      {
        content: "",
      },
    ]);

    setOpenCollapses(Date.now());
  };

  const SaveChapter = () => {
    updateChapters();
    setIsEditing(false);
  };

  return (
    <div className={styles.articlesContainer}>
      <h6>Redacte los artículos para este capítulo</h6>
      <p>
        Puede redactar cada uno de los articulos que posee el capitulo en esta
        sección
      </p>
      {articles
        ?.sort((a, b) => a.cod - b.cod)
        .map((article, i) => {
          return (
            <ArticleEditor
              openCollapses={openCollapses}
              key={i}
              i={i}
              deleteArticle={() => deleteArticle(i)}
              handleChange={(data) => handleEditor({ data, i })}
              rawData={contentParsed(article?.content)}
              last={i + 1 === articles.length}
            />
          );
        })}
      <div className={styles.addArticleButton}>
        <div className={styles.addMoreArticles} onClick={AddArticle}>
          Añadir artículo{" "}
          {chapters[index].articles ? chapters[index].articles.length + 1 : 1}{" "}
          <PlusOutlined />
        </div>
      </div>
      <hr style={{ color: "#0004" }} className="mt-5 mb-3" />
      <Button type="primary" size="small" onClick={() => SaveChapter(index)}>
        Guardar Capítulo
      </Button>
    </div>
  );
};

export default ArticlesList;
