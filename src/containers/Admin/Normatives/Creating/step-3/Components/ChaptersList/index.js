import React, { useState } from "react";
import Button from "@components/Button";
import Link from "next/link";
import styles from "./index.module.scss";
import Chapter from "./Chapter";

function ChapterList({ props }) {
  const {
    chapters,
    setShowTitleComponent,
    editChapter,
    localPost,
    setDispatch,
  } = props;
  const [errorLocal, setErrorLocal] = useState(localPost?.chapters?.error);

  const handleNext = () => {
    if (!chapters) {
      setErrorLocal(!chapters);
      setDispatch("chapters", undefined);
    }
  };

  const handleRemove = (index) => {
    chapters.splice(index, 1);
    console.log("handleRemove:", chapters);
    setDispatch("chapters", chapters);
  };

  return (
    <>
      <h6>Redacte los capítulos y artículos de la normativa</h6>
      <p>Agrega capítulos y coloque los articulos correspondientes</p>
      <div className={styles.addChaptersElsContainer}>
        {chapters
          ?.sort((a, b) => a.cod - b.cod)
          .map((chapter, i) => (
            <Chapter
              i={i}
              key={i}
              chapter={chapter}
              handleRemove={handleRemove}
              editChapter={editChapter}
            />
          ))}
        <span
          className={styles.addButton}
          onClick={() => setShowTitleComponent(true)}
        >
          Añadir Capítulo {`${chapters ? chapters.length + 1 : 1}`} +
        </span>
        <span className="text-redDart text-center w-100 my-54">
          {errorLocal && "* Debes añadir al menos un capítulo y artículo"}
        </span>
      </div>

      <div className={styles.bottomButtons}>
        <Link href="/admin/normativas/creando/paso-4">
          <a>
            <Button type="primary" size="large" onClick={handleNext}>
              Siguiente
            </Button>
          </a>
        </Link>
      </div>
    </>
  );
}

export default ChapterList;
