import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import styles from "./index.module.scss";

function Chapter({ i, chapter, handleRemove, editChapter }) {
  const quantityArticles = (articles) =>
    !articles || (articles.length === 1 && !articles[0].content)
      ? `Sin artículos`
      : `${articles.length} artículo${articles.length > 1 ? `s` : ``}`;

  return (
    <>
      <div className={styles.chapterEL}>
        <div className={styles.leftEl}>
          <span className={styles.title}>
            Capítulo {i + 1}: {chapter.title}
          </span>
          <span className={styles.dataVisual}>
            {quantityArticles(chapter.articles)}
          </span>
        </div>
        <div className={styles.rightEl}>
          <span className={styles.deleteButton} onClick={() => handleRemove(i)}>
            Eliminar capítulo
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              width="12"
              height="12"
            >
              <path fill="none" d="M0 0h24v24H0z" />
              <path
                d="M7 4V2h10v2h5v2h-2v15a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V6H2V4h5zM6 6v14h12V6H6zm3 3h2v8H9V9zm4 0h2v8h-2V9z"
                fill="rgba(254,71,82,1)"
              />
            </svg>
          </span>
          <span
            className={styles.addChaptersButton}
            onClick={() => editChapter(i)}
          >
            Añadir Artículos <PlusOutlined />
          </span>
        </div>
      </div>
    </>
  );
}

export default Chapter;
