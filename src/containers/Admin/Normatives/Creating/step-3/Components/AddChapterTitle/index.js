import React, { useState } from "react";
import Button from "@components/Button";
import { CloseOutlined } from "@ant-design/icons";
import styles from "./index.module.scss";
/**
 * @param setShowTitleComponent Muestra u oculta este componente
 * @param setChapterTitle Guarda una variable title
 * @param handleAdd guarda el título del capítulo
 * @return An upload component using AntDesign
 */
function AddChapterTitle({ props }) {
  let title;
  const { chapters, setShowTitleComponent, handleAdd } = props;
  const [error, setError] = useState(false);
  const handleCreateChapter = (_title) => {
    setError(_title.length < 2 || _title.length > 100);
    title = _title;
  };

  return (
    <div className={styles.addingContainer}>
      <div className={styles.inputContainer}>
        <header>
          <h6>
            Escribe el título del Capítulo {chapters ? chapters.length + 1 : 1}:
          </h6>
          <span
            className={styles.close}
            onClick={() => setShowTitleComponent(false)}
          >
            <CloseOutlined />
          </span>
        </header>
        <p></p>
        <input
          type="text"
          placeholder="Ej: Necesidad de basureros zona Miraflores"
          onChange={(e) => handleCreateChapter(e.target.value)}
        />

        {error && (
          <>
            <br />
            <span className="text-redDart">
              Ingrese un título entre 1 y 100 caracteres por favor
            </span>
          </>
        )}

        <br />
        <Button
          type="primary"
          size="large"
          disabled={error}
          onClick={() => handleAdd(title)}
        >
          Crear Capítulo {chapters ? chapters.length + 1 : 1}
        </Button>
      </div>
    </div>
  );
}

export default AddChapterTitle;
