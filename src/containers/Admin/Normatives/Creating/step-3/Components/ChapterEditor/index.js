import React from "react";
import ArticlesList from "../ArticlesList";
import { LeftOutlined } from "@ant-design/icons";
import styles from "./index.module.scss";

function ChapterEditor({ props }) {
  const { chapters, index, setIsEditing, setDispatch } = props;

  return (
    <>
      <div className={styles.header}>
        <span
          className={styles.volverButton}
          onClick={() => setIsEditing(false)}
        >
          <LeftOutlined /> volver
        </span>
        <span className={styles.status}>
          Artículos del capítulo {index + 1}
        </span>
      </div>
      <ArticlesList
        chapters={chapters}
        setDispatch={setDispatch}
        index={index}
        setIsEditing={setIsEditing}
      />
    </>
  );
}

export default ChapterEditor;
