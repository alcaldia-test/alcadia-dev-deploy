import React from "react";
import ButtonComponent from "@components/Button";
import StepsContainer from "../StepsContainer";
import CreatingHeader from "../../CreatingHeader";
import styles from "./preview.module.scss";
import Spin from "@components/Spin";
import BodyAdminPostNormativa from "../../AdminPost/bodyAdminPost";
import { useSelector, useDispatch } from "react-redux";
import { setDataDispatch } from "@utils/validateFields";
import { useRouter } from "next/router";

import {
  creatingNormative,
  changeDataLocalPost,
} from "@redux/createNormative/actions";
// import BodyNormative from "@containers/Normative/BodyNormative";

const StepFour = () => {
  const router = useRouter();

  const dispatch = useDispatch();
  const dataAuth = useSelector((state) => state.auth);
  const { localPost, loading } = useSelector(
    (state) => state.creatingNormative
  );
  const { title, dueDate, content, tags, chapters } = localPost;
  const setDispatch = (name, value) =>
    setDataDispatch(dispatch, changeDataLocalPost, name, value);

  // before step validation
  !title && setDispatch("title", title?.value);
  !dueDate && setDispatch("dueDate", dueDate?.value);
  !content && setDispatch("content", content?.value);
  !tags && setDispatch("tags", tags?.value);
  !chapters && setDispatch("chapters", chapters?.value);

  const savePost = (completed) => {
    dispatch(creatingNormative({ ...localPost, completed }));
    if (title.error) {
      router.push(`/admin/normativas/creando/paso-1`);
    }
  };

  const normative = {};
  for (const key in localPost) {
    console.log(key, localPost[key]);
    normative[key] = localPost[key].value || localPost[key];
  }
  console.log({ normative });
  return (
    <Spin spinning={loading} tip={`Guardando ...`} size="large">
      <StepsContainer>
        <CreatingHeader step={4} localPost={localPost} />
        <div className="font-bold text-center text-H4 mb-6 mt-3">
          Previsualización de la Normativa
        </div>
        <section className={styles.section}>
          {/* <BodyNormative
            isAdmin={true}
            normative={normative}
            dataUser={dataAuth.dataUser}
          /> */}
          <BodyAdminPostNormativa
            _props={{ localPost, favorites: [], dataAuth }}
          />
        </section>

        <div className="flex m-5 justify-around">
          {!localPost?.completed && (
            <ButtonComponent
              size="super-small"
              type="link"
              onClick={() => savePost(false)}
            >
              Terminar más tarde
            </ButtonComponent>
          )}
          <ButtonComponent
            size="super-small"
            type="primary"
            onClick={() => savePost(true)}
            style={{ padding: "5px 70px" }}
          >
            Publicar
          </ButtonComponent>
        </div>
      </StepsContainer>
    </Spin>
  );
};

export default StepFour;
