import React, { memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import StepsContainer from "../StepsContainer";
import CreatingHeader from "../../CreatingHeader";
import Button from "@components/Button";
import styles from "./step-1.module.scss";
import { changeDataLocalPost } from "@redux/createNormative/actions";
import DatePickerComponent from "@components/DatePicker";
import Link from "next/link";
import { setDataDispatch } from "@utils/validateFields";

const StepOne = () => {
  const { localPost } = useSelector((state) => state.creatingNormative);
  const { title, dueDate } = localPost;

  console.warn("localpost paso 1:", localPost);
  const dispatch = useDispatch();
  const setDispatch = (name, value) =>
    setDataDispatch(dispatch, changeDataLocalPost, name, value);

  const handleTitle = (e) => {
    const { name, value } = e.target;
    setDispatch(name, value);
  };
  const handleDueDate = (e) => {
    setDispatch("dueDate", e);
  };
  const handleNext = () => {
    setDispatch("title", title?.value);
    setDispatch("dueDate", dueDate?.value);
  };

  return (
    <StepsContainer>
      <CreatingHeader step={0} localPost={localPost} />
      <section className={styles.section}>
        <h6>Escribe el título de la normativa</h6>
        <p>
          Capta la atención con un título corto que haga comprender a los
          ciudadanos sobre que temática trata esta normativa
        </p>
        <input
          type="text"
          placeholder={"Ej: Ley municipal para el expendio de bebidas"}
          onChange={handleTitle}
          name="title"
          value={title?.value || ""}
        />
        {title?.error && (
          <p className="text-redDart">
            Ingrese un título entre 1 y 100 caracteres por favor
          </p>
        )}
        <h6>¿Hasta cuándo será visible la normativa?</h6>
        <div className="mt-5">
          <DatePickerComponent
            setDate={handleDueDate}
            date={dueDate?.value || null}
            placeholder={"Seleccione fecha"}
          />
        </div>
        {dueDate?.error && (
          <p className="text-redDart">Ingrese una fecha válida por favor</p>
        )}
        <Button type="primary" size="large" onClick={handleNext}>
          <Link href="/admin/normativas/creando/paso-2">Siguiente</Link>
        </Button>
      </section>
    </StepsContainer>
  );
};

export default memo(StepOne);
