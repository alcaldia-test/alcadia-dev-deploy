import React from "react";
import ModalSave from "../../CreatingHeader/modalsSave";
import styles from "./container.module.scss";

export default function Container({ children }) {
  return (
    <div className={styles.container}>
      <div className={styles.childContainer}>{children}</div>
      <ModalSave />
    </div>
  );
}
