import React, { useState } from "react";
import { DownOutlined, RightOutlined } from "@ant-design/icons";
import styles from "./chapters.module.scss";
import { parseDraft } from "@utils/draftToHtml";

const Chapters = ({ chapter, index }) => {
  const [open, setOpen] = useState(false);
  // const contentParsed = (content) => {
  //   console.log(content);
  //   try {
  //     return JSON.parse(content);
  //   } catch (error) {
  //     return content;
  //   }
  // };
  // console.warn({ chapter });

  return (
    <div className={styles.articlesSection}>
      <div
        onClick={() => setOpen(!open)}
        className={`${styles.toggle} text-xl`}
      >
        {open ? <DownOutlined /> : <RightOutlined />} Capítulo {index + 1}:{" "}
        {chapter.title}
      </div>
      <div className={open ? styles.articlesContainer : styles.hidden}>
        {chapter.articles.map((article, i) => (
          <div key={i} className={"text-justify px-28 pt-16"}>
            <h2 className="text-xl">Artículo {i + 1}.</h2>
            <div
              dangerouslySetInnerHTML={{
                __html: parseDraft(article.content),
              }}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Chapters;
