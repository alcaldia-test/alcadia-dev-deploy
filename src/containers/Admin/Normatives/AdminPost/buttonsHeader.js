import React from "react";
import Link from "next/link";
import ButtonComponent from "@components/Button";
import Image from "next/image";

export default function ButtonsHeader({ post }) {
  return (
    <div className=" h-5  flex justify-between">
      <Link href="/admin/charlas">
        <a>
          <ButtonComponent size="super-small" type="secondary">
            <div className="flex">
              <span className="text-callout">{"< Volver"}</span>
            </div>
          </ButtonComponent>
        </a>
      </Link>
      {post.state !== "CREATING" && (
        <Link href={`/admin/estadisticas/charlas/${post.id}`}>
          <a>
            <ButtonComponent size="super-small" type="secondary">
              <div className="flex">
                <span className="text-callout mr-12">
                  {"Estadísticas de la charla"}
                </span>
                <Image
                  src="/icons/bar-char.svg"
                  width={20}
                  height={20}
                  layout="fixed"
                  alt="checkedred"
                />
              </div>
            </ButtonComponent>
          </a>
        </Link>
      )}
    </div>
  );
}
