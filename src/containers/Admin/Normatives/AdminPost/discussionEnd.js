import moment from "moment";
import Image from "next/image";
import React from "react";

function DiscussionEnd({ localPost }) {
  const endDate = moment(localPost?.dueDate?.value);

  return localPost?.completed ? (
    moment().diff(moment(localPost?.dueDate?.value)) > 0 ? (
      <div className="my-3 text-red text-bodyBold bg-errorBackground p-12 w-380 rounded-sm h-43 flex">
        <span className="ml-18">
          <Image
            src="/icons/checked-red.svg"
            width={20}
            height={20}
            layout="fixed"
            alt="checkedred"
          />
        </span>
        <span className="mt-11 ml-12">La charla finalizó</span>
      </div>
    ) : (
      <div className="my-3 text-greenText text-headline">
        <span>{`La votación finaliza el ${endDate.format("DD-MM-YYYY")}`}</span>
      </div>
    )
  ) : (
    <div className="my-3 text-fgCreating text-headline bg-bgCreating w-1/5 rounded-sm h-43 flex p-1">
      <span>En creación</span>
    </div>
  );
}

export default DiscussionEnd;
