import React, { useState } from "react";
import ZonesModal from "@components/Modals/ZonesModal";
const getMacros = (zonesId) =>
  zonesId ? zonesId.map((z) => ({ name: z.name })) : [];
const getZones = (zonesId) => {
  const zones = [];
  if (zonesId) {
    zonesId.forEach((macro) => {
      macro.selected
        .map((s) => ({ name: s.zone }))
        .forEach((z) => {
          zones.push(z);
        });
    });
  }
  return zones;
};

function ZonesAllowed({ localPost }) {
  const [showZonesModal, setShowZonesModal] = useState(false);
  const macros = getMacros(localPost.zonesId?.value);
  const zones = getZones(localPost.zonesId?.value);

  return (
    <>
      <div className="flex gap-3 items-center text-body">
        {localPost.zonesId?.value ? (
          <div
            className="bg-stateConfirmed p-2 rounded-sm text-zones cursor-pointer font-bold"
            onClick={() => setShowZonesModal(true)}
          >
            Ver zonas válidas
          </div>
        ) : (
          <div className="bg-stateConfirmed p-2 rounded-sm text-zones  font-bold">
            Todas las zonas pueden participar
          </div>
        )}
      </div>

      <ZonesModal
        visible={showZonesModal}
        closable={false}
        onCancel={() => setShowZonesModal(false)}
        macros={macros}
        zones={zones}
      />
    </>
  );
}

export default ZonesAllowed;
