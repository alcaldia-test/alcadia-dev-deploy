import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Inputs from "../ControlsInputs";
import Pagination from "@components/Pagination";
import { getZone, getMacroZones } from "@redux/discussions/actions";
import { getFilterNormativeAdmin } from "@redux/normatives/actions";

const DashboardContainer = ({ permissions, normatives, children }) => {
  const [filterData, setFilterData] = useState({
    state: null,
    order: "DESC",
    text: "",
    offset: 1,
  });
  const optionsAutocomplete =
    normatives?.length > 0
      ? normatives?.map((item) => ({
          title: item.title,
        }))
      : [];

  const limit = 8;
  const { zones, macroZones } = useSelector((state) => state.discussion);
  const { normativesCount, reloadCards } = useSelector(
    (state) => state.normatives
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      getFilterNormativeAdmin({
        state:
          filterData.state === "creacion"
            ? "CREATING"
            : filterData.state === "innactivas"
            ? "FINISHED"
            : filterData.state === "activas"
            ? "ACTIVE"
            : null,
        order: filterData.order.toUpperCase(),
        content: filterData.text,
        offset: filterData.offset === 1 ? 0 : (filterData.offset - 1) * limit,
      })
    );
  }, [filterData, reloadCards]);

  useEffect(() => {
    zones?.length === 0 && dispatch(getZone());
    macroZones?.length === 0 && dispatch(getMacroZones());
  }, []);

  return (
    <div className="bg-white mb-96">
      <Inputs
        permissions={permissions}
        statusSelector={(state) =>
          setFilterData({
            ...filterData,
            state,
            offset: 1,
          })
        }
        orderSelector={(order) =>
          setFilterData({
            ...filterData,
            order,
            offset: 1,
          })
        }
        searchInput={(text) =>
          setFilterData({
            ...filterData,
            text,
            offset: 1,
          })
        }
        optionsAutocomplete={optionsAutocomplete}
      />
      <div className="flex flex-wrap justify-around p-1 gap-1">{children}</div>
      {normativesCount > 8 && (
        <Pagination
          total={normativesCount}
          pageSize={8}
          onChange={(e) => setFilterData({ ...filterData, offset: e })}
        />
      )}
    </div>
  );
};

export default DashboardContainer;
