import React, { useEffect, useState } from "react";
import DashboardContainer from "./DashboardCardsContainer";
import DeleteModal from "@components/deleteModal";
import { useSelector, useDispatch } from "react-redux";
import Spin from "@components/Spin";
import { deleteNormative, editPost } from "@redux/createNormative/actions";
import Router from "next/router";
import Card from "@components/Cards/CardNormativesAdmin";
import { checkPermision } from "@utils/adminPermission";
import moment from "moment";

const Normatives = () => {
  const { normatives } = useSelector((state) => state.normatives);
  const { dataUser } = useSelector((state) => state.auth);
  const { loader } = useSelector((data) => data.common);
  const [showModal, setShowModal] = useState(false);
  const [deleteModalProperties, setDeleteModalProperties] = useState({
    title: "",
    id: null,
  });
  const dispatch = useDispatch();
  const handleDetele = ({ title, id }) => {
    setShowModal(true);
    setDeleteModalProperties({ title, id });
  };
  const handleEdit = (id) => {
    dispatch(editPost({ id }));
    Router.push("/admin/normativas/creando/paso-1");
  };
  const [permissions, setPermissions] = useState({
    canDelete: false,
    canCreate: false,
    canEdit: false,
  });
  useEffect(() => {
    const _permissions = checkPermision(dataUser, "normatives");
    setPermissions(_permissions);
  }, [dataUser]);

  return (
    <>
      {showModal && (
        <DeleteModal
          closeModal={() => setShowModal(false)}
          title={deleteModalProperties.title}
          delteFuntion={() => {
            dispatch(
              deleteNormative({
                id: deleteModalProperties.id,
              })
            );
            setShowModal(false);
          }}
        />
      )}
      <Spin spinning={loader} tip={`Cargando ...`} size="large">
        <DashboardContainer permissions={permissions} normatives={normatives}>
          {normatives?.length > 0 ? (
            normatives.map((normative) => (
              <Card
                permissions={permissions}
                key={normative.id}
                id={normative.id}
                title={normative.title}
                content={normative.content}
                state={setStatus(normative)}
                comments={normative.comments}
                complaints={normative.complaints}
                handleDelete={handleDetele}
                handleEdit={() => handleEdit(normative.id)}
                handleStatistics={() =>
                  Router.push(`/admin/estadisticas/normativas/${normative.id}`)
                }
                handleAdmin={() =>
                  Router.push(`/admin/normativas/${normative.id}`)
                }
              />
            ))
          ) : (
            <span className="text-H5 mt-5">No hubo resultados</span>
          )}
        </DashboardContainer>
      </Spin>
    </>
  );
};

function setStatus(post) {
  switch (post.state) {
    case "PUBLISHED":
      if (moment()._d > moment(post.dueDate)._d) {
        return "FINISHED";
      } else {
        return "PUBLISHED";
      }

    default:
      return "CREATING";
  }
}

export default Normatives;
