import React from "react";
import Select from "@components/Select";
import Autocomplete from "@components/Autocomplete";
import Button from "@components/Button";
import styles from "./controls.module.scss";
import Link from "next/link";
import { PlusOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { emptyLocalPost } from "@redux/createDiscussion/actions";

const Inputs = ({
  statusSelector,
  orderSelector,
  searchInput,
  permissions,
  optionsAutocomplete,
}) => {
  const dispatch = useDispatch();

  return (
    <div className={styles.controlsContainer}>
      <div className={styles.controls}>
        <Select
          options={[
            { value: "Todas", label: "Todas" },
            { value: "Activas", label: "En curso" },
            { value: "Innactivas", label: "Finalizadas" },
            { value: "Creacion", label: "En creación" },
          ]}
          defaultValue="Todas"
          onChange={(e) => statusSelector(e[0].value.toLowerCase())}
          backspaceDeleted={true}
          placeholder={"todas"}
        />
        <Select
          defaultValue="Más Recientes"
          options={[
            { value: "DESC", label: "Más Recientes" },
            { value: "ASC", label: "Más Antiguas" },
          ]}
          onChange={(e) => orderSelector(e[0].value.toLowerCase())}
          backspaceDeleted={true}
          placeholder={"Más Recientes"}
        />
        <Autocomplete
          dataSource={optionsAutocomplete}
          placeholder="Buscar..."
          onChange={(e) => searchInput(e)}
        />
      </div>
      {permissions?.canCreate && (
        <Link href="/admin/normativas/creando/paso-1">
          <a>
            <Button
              type="primary"
              size="small"
              onClick={() => dispatch(emptyLocalPost())}
            >
              Crear Normativa
              <PlusOutlined />
            </Button>
          </a>
        </Link>
      )}
    </div>
  );
};

export default Inputs;
