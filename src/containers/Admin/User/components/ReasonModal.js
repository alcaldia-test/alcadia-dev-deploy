import React from "react";

import Modal from "@components/Modals/SimpleModal";
import Button from "@components/Button";
import styles from "../users.module.scss";
import { useDispatch } from "react-redux";
import { deleteUsersStart } from "@redux/users/actions";
import message from "@components/Message";

const currectState = {
  message: "",
};

function ReasonModal({ user = {}, onCancel, setLoading }) {
  const disptach = useDispatch();

  const handleChange = (ev) => {
    currectState[ev.target.name] = ev.target.value;
  };

  const handleSave = async () => {
    if (!currectState.message) return message.error("¡Introduzca el motivo!");

    const payload = {
      userId: user.id,
      ...currectState,
    };

    onCancel();

    try {
      setLoading(true);

      await new Promise((resolve, reject) =>
        disptach(deleteUsersStart({ ...payload, resolve, reject }))
      );

      message.success("Usuario bloqueado con éxito.");
      currectState.message = "";
      setLoading(false);
    } catch (error) {
      message.error(error);
    }
  };

  return (
    <Modal visible={true} onCancel={onCancel}>
      <div className={styles["input-section"]}>
        <h3>
          Motivo de Bloqueo a{" "}
          <span className="text-capitalize">{user.name}</span>
        </h3>
        <label>
          Motivo
          <textarea
            name="message"
            placeholder="Contenido..."
            onChange={handleChange}
          />
        </label>
      </div>
      <div className={styles["section-buttons"]}>
        <Button onClick={onCancel} type="secondary" size="small">
          Cancelar
        </Button>
        <Button onClick={handleSave} type="primary" size="small">
          Bloquear
        </Button>
      </div>
    </Modal>
  );
}

export default ReasonModal;
