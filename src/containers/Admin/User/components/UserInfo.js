import React, { useEffect, useState } from "react";

import { useDispatch } from "react-redux";
import Modal from "@components/Modals/SimpleModal";
import styles from "../users.module.scss";
import message from "@components/Message";

import Spin from "@components/Spin";
import { requestSingleUser } from "@redux/users/actions";

// const user = {
//   name: "User Name",
//   email: "examaple@gmail.com",
//   avatar:
//     "https://robohash.org/amhvbWFuaWRldiU0MGdtYWlsLmNvbQ==.png?size=150x150",
//   zones: [
//     { tag: "Por familia", name: "Mock" },
//     { tag: "Por negocio", name: "Mock 2" },
//   ],
// };

function UserInfo({ userId, onCancel }) {
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState({});
  const dispatch = useDispatch();

  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    try {
      setLoading(true);

      const result = await new Promise((resolve, reject) =>
        dispatch(requestSingleUser({ id: userId, resolve, reject }))
      );

      setUser(result);
    } catch (error) {
      message.error(error);
    }

    setLoading(false);
  };

  return (
    <Modal visible={true} onCancel={onCancel}>
      <Spin spinning={loading} size="large">
        <div className={styles["user-info"]}>
          <h3>Más información</h3>
          <div className={styles.userdata}>
            <img src={user?.avatar} alt="avatar" />
            <span>{user?.name}</span>
          </div>
          <div className="my-2">
            <b>Correo</b>
            <p className="pl-1">{user?.email}</p>
          </div>
          <b>Zonas de interés:</b>
          <ul>
            {user?.zones?.map(({ tag, name }, i) => (
              <li key={i}>
                <b>{tag}:</b> {name}
              </li>
            ))}
          </ul>
        </div>
      </Spin>
    </Modal>
  );
}

export default UserInfo;
