import React, { useState } from "react";

import Table from "@components/Table";
import styles from "../users.module.scss";

import { UnknowDoc } from "@assets/icons";
import confirm from "@components/Modals/confirm";

import ReasonModal from "./ReasonModal";
import UserInfo from "./UserInfo";
import ReadReason from "./ReadReason";

import { changeUsersStart } from "@redux/users/actions";
import { useDispatch } from "react-redux";
import message from "@components/Message";
import { capitalize } from "@utils/utils";

const DataTable = ({ users, inlock = false, setLoading }) => {
  const [current, setCurrent] = useState(undefined);
  const [showinfo, setShowinfo] = useState(undefined);
  const [readReason, setReadReason] = useState(undefined);
  const disptach = useDispatch();

  const handleLocking = async (data) => {
    if (inlock) {
      const msg = `¿Seguro que quiere desbloquear a ${capitalize(
        data.user?.name
      )}?`;
      const result = await confirm(msg);
      if (!result) return;

      const payload = {
        userId: data.id,
      };

      try {
        setLoading(true);
        await new Promise((resolve, reject) =>
          disptach(changeUsersStart({ ...payload, resolve, reject }))
        );

        message.success("Usuario desbloqueado con éxito.");
        setLoading(false);
      } catch (error) {
        message.error(error);
      }
      console.log(result);
    } else setCurrent({ ...data.user, id: data.id });
  };

  const handleShows = async (data) => {
    if (!inlock) setShowinfo(data.id);
    else setReadReason(data);
  };

  return (
    <>
      {current && (
        <ReasonModal
          setLoading={setLoading}
          user={current}
          onCancel={() => setCurrent(undefined)}
        />
      )}
      {showinfo && (
        <UserInfo userId={showinfo} onCancel={() => setShowinfo(undefined)} />
      )}
      {readReason && (
        <ReadReason
          user={readReason}
          onCancel={() => setReadReason(undefined)}
        />
      )}
      <Table
        data={users}
        columns={[
          {
            title: "Usuario",
            dataIndex: "user",
            render: (user) => {
              return (
                <div className={styles.userdata}>
                  <img src={user.avatar} alt="avatar" />
                  <span>{user.name}</span>
                </div>
              );
            },
          },
          {
            title: "Nro de Documento",
            dataIndex: "identityCard",
          },
          {
            title: "Zonas de Interés",
            dataIndex: "areas",
            render: (areas) => `${areas} Zonas de Interés`,
          },
          {
            title: "Denuncias",
            dataIndex: "complaints",
            render: (cant) => (
              <div className={styles.complaints}>
                <UnknowDoc size="15" color="#FE4752" />
                <span>{cant}</span>
              </div>
            ),
          },
          {
            title: "Acciones",
            index: "action",
            render: (data) => (
              <>
                <button
                  onClick={handleShows.bind({}, data)}
                  className="actionButton"
                >
                  {inlock ? <a>Mostrar Motivo</a> : <a>Ver Información</a>}
                </button>
                <button
                  onClick={handleLocking.bind({}, data)}
                  className="actionButton ml-2"
                >
                  <a>{inlock ? "Desbloquear Usuario" : "Bloquear Usuario"}</a>
                </button>
              </>
            ),
          },
        ]}
      />
    </>
  );
};

export default DataTable;
