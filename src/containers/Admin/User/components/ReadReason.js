import React from "react";

import Modal from "@components/Modals/SimpleModal";
import styles from "../users.module.scss";
const mockUser = {
  name: "Juan Carlos",
  message: "Es muy molestoso",
};

function ReadReason({ user = mockUser, onCancel }) {
  return (
    <Modal visible={true} onCancel={onCancel}>
      <div className={styles["user-info"]}>
        <h3>
          Se bloqueó a
          <span className="text-capitalize"> {user?.user?.name} </span>por:
        </h3>
        <p>{user.message}</p>
      </div>
    </Modal>
  );
}

export default ReadReason;
