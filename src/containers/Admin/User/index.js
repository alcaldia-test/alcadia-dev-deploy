import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";

import Tab from "@components/Tab";
import styles from "./users.module.scss";
import message from "@components/Message";

import Button from "@components/Button";
import Pagination from "@components/Pagination";

import { requestUsersStart } from "@redux/users/actions";
import DataTable from "./components/UserTable";
import Spin from "@components/Spin";
import { CharBar } from "@assets/icons";

import SelectWraper from "@components/Select";
import Autocomplete from "@components/Autocomplete";

const optionsSelect2 = [
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más Antiguas" },
];

let searchValue = "";
let withCount = true;
export default function UsersModuel({ registered = 0 }) {
  const [select, setSelect] = useState("DESC");
  const [currentPage, setCurrentPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [tab, setTap] = useState(registered);

  const dispatch = useDispatch();
  const { datas, lockeds, lockedsCount, datasCount, newset } = useSelector(
    (state) => state.users
  );

  useEffect(() => {
    getUserData();
  }, [currentPage, select]);

  const getUserData = async (filter = {}) => {
    try {
      setLoading(true);

      const filterObj = {
        ...{ enable: tab },
        ...filter,
        skip: (currentPage - 1) * 10,
        like: searchValue,
        order: select,
        withCount,
      };

      await new Promise((resolve, reject) =>
        dispatch(requestUsersStart({ ...filterObj, resolve, reject }))
      );
    } catch (error) {
      message.error(error);
    }

    setLoading(false);
  };

  const EachTab = ({ inlock }) => (
    <>
      <div className="flex flex-column nowrap w-100 justify-between">
        <div className={"w-max flex flex-row pb-40 " + styles.filter_select}>
          <div className="flex items-center">
            <span className="mx-2">Ordenar por</span>
            <SelectWraper
              onChange={(e) => {
                withCount = false;
                setSelect(e[0].value);
              }}
              defaultValue={select === "DESC" ? 0 : 1}
              options={optionsSelect2}
            ></SelectWraper>
          </div>
          <Autocomplete
            placeholder="Buscar..."
            defaultValue={searchValue}
            onChange={(value) => {
              searchValue = value;
              withCount = true;
              getUserData();
            }}
          />
        </div>
      </div>
      <Spin spinning={loading} size="large">
        <DataTable
          setLoading={setLoading}
          inlock={inlock}
          users={inlock ? lockeds : datas}
        />
      </Spin>
    </>
  );

  return (
    <section className={styles.userContainer}>
      <div className={`${styles.list__proposals__container} user-tabs`}>
        <div className={styles["buttons-group"]}>
          <Link href="/admin/estadisticas/usuarios">
            <a>
              <Button type="secondary">
                Estadísticas Totales &nbsp;&nbsp;
                <CharBar color="#333" size="20" />
              </Button>
            </a>
          </Link>
        </div>
        <Tab
          defaultActiveKey={`${tab}`}
          onChange={(num) => {
            const tab = parseInt(num);
            if (newset) {
              withCount = true;
              getUserData({ enable: tab, newset: false });
            }
            setTap(tab);
          }}
          content={[
            {
              title: "Registrados",
              children: <EachTab inlock={false} />,
            },
            {
              title: "Bloqueados",
              children: <EachTab inlock={true} />,
            },
          ]}
        />
      </div>
      <Pagination
        total={tab === 0 ? datasCount : lockedsCount}
        current={currentPage}
        onChange={(page) => {
          withCount = false;
          setCurrentPage(page);
        }}
      />
    </section>
  );
}
