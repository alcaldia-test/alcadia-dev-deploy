import {
  commentIcon,
  complaintIcon,
  likeIcon,
  disLikeIcon,
  waitingIcon,
  approvedIcon,
  whatsappIcon,
  facebookIcon,
  twitterIcon,
  telegramIcon,
  linkIcon,
  winnerIcon,
  rejectedIcon,
  lockIcon,
} from "./icons";

const socialMedia = (datas = []) => {
  return [
    {
      count: `${datas[0] ?? 0} Compartidas`,
      svg: whatsappIcon,
      description: "Whatsapp",
    },
    {
      count: `${datas[1] ?? 0} Compartidas`,
      svg: facebookIcon,
      description: "Facebook",
    },
    {
      count: `${datas[2] ?? 0} Compartidas`,
      svg: twitterIcon,
      description: "Twitter",
    },
    {
      count: `${datas[3] ?? 0} Compartidas`,
      svg: telegramIcon,
      description: "Telegram",
    },
    {
      count: `${datas[4] ?? 0} Compartidas`,
      svg: linkIcon,
      description: "Por link",
    },
  ];
};

export default function getModuleData(
  {
    id,
    like = 0,
    dislike = 0,
    shareds = {},
    complaints = 0,
    comments = 0,
    chapters,
    byState,
    favorites,
    votes,
    locked,
    serviced,
  },
  mod,
  partial
) {
  let res;

  const totalReactions = like + dislike === 0 ? 1 : like + dislike;
  const likesP = ((like / totalReactions) * 100).toFixed();
  const dislikesP = ((dislike / totalReactions) * 100).toFixed();

  if (mod === "a")
    res = [
      {
        title: "Propuestas",
        cards: [
          {
            count: byState?.APPROVED ?? 0,
            svg: approvedIcon,
            description: "Propuestas aprobadas",
          },
          {
            count: byState?.WINNER ?? 0,
            svg: winnerIcon,
            description: "Propuestas ganadoras",
          },
          {
            count: byState?.WAITING ?? 0,
            svg: waitingIcon,
            description: "Propuestas en espera",
          },
          {
            count: byState?.REJECTED ?? 0,
            svg: rejectedIcon,
            description: "Propuestas rechazadas",
          },
        ],
        url: {
          title: "Ir a propuestas finalizadas",
          url: "/admin/convocatorias/" + id + "/propuestas/finalizadas",
        },
      },
      {
        title: "Tipos de participaciones",
        cards: [
          {
            count: `${favorites?.BUSINESS ?? 0} Usuario(s)`,
            description: "Con interés de negocio",
          },
          {
            count: `${favorites?.FAMILY ?? 0} Usuario(s)`,
            description: "Con interés de familiares",
          },
          {
            count: `${favorites?.HOME ?? 0} Usuario(s)`,
            description: "Con interés de domicilio",
          },
        ],
      },
      {
        title: "Apoyos y Desacuerdos",
        cards: [
          {
            count: `${likesP}% - ${like}`,
            svg: likeIcon,
            description: "Apoyos a propuestas",
          },
          {
            count: `${dislikesP}% - ${dislike}`,
            svg: disLikeIcon,
            description: "Desacuerdos a propuestas",
          },
        ],
      },
      {
        title: "Comentarios",
        cards: [
          {
            count: comments ?? 0,
            svg: commentIcon,
            description: "Comentarios",
          },
          {
            count: complaints ?? 0,
            svg: complaintIcon,
            description: "Denuncias a comentarios",
          },
        ],
      },
      {
        title: "Compartidas",
        cards: socialMedia(Object.values(shareds)),
      },
    ];
  if (mod === "n") {
    const chapterReaction = [];
    const chpaterComments = [];
    const chpaterComplaints = [];

    chapters?.forEach(({ like, dislike, ...chapter }) => {
      const totalReactions = like + dislike === 0 ? 1 : like + dislike;
      const likesP = ((like / totalReactions) * 100).toFixed();
      const dislikesP = ((dislike / totalReactions) * 100).toFixed();

      const subtitle = `Capitulo ${chapter.cod}`;

      chapterReaction.push({
        subtitle,
        cards: [
          {
            count: `${likesP}% - ${like}`,
            svg: likeIcon,
            description: "Apoyos al capitulo",
          },
          {
            count: `${dislikesP}% - ${dislike}`,
            svg: disLikeIcon,
            description: "Desacuerdos al capitulo",
          },
        ],
      });

      chpaterComments.push({
        subtitle,
        cards: [
          {
            count: chapter.comments ?? 0,
            svg: commentIcon,
            description: "Comentarios",
          },
        ],
      });

      chpaterComplaints.push({
        subtitle,
        cards: [
          {
            count: chapter.complaints ?? 0,
            svg: complaintIcon,
            description: "Denuncias",
          },
        ],
      });
    });

    res = [
      {
        title: "Apoyos y Desacuerdos",
        subtitle: "Introducción a la normativa",
        cards: [
          {
            count: `${likesP}% - ${like}`,
            svg: likeIcon,
            description: "Apoyos a la publicacion",
          },
          {
            count: `${dislikesP}% - ${dislike}`,
            svg: disLikeIcon,
            description: "Desacuerdos a la publicacion",
          },
        ],
      },
      ...chapterReaction,
      {
        title: "Comentarios",
        subtitle: "Introducción a la normativa",
        cards: [
          {
            count: comments ?? 0,
            svg: commentIcon,
            description: "Comentarios",
          },
        ],
      },
      ...chpaterComments,
      {
        title: "Denuncias (en comentarios y respuestas)",
        subtitle: "Introducción a la normativa",
        cards: [
          {
            count: complaints ?? 0,
            svg: complaintIcon,
            description: "Denuncias a comentarios",
          },
        ],
      },
      ...chpaterComplaints,
      {
        title: "Compartidas",
        cards: socialMedia(Object.values(shareds)),
      },
    ];
  }
  if (mod === "v") {
    const voteSuppot = [];
    let total = 0;
    const likes = [];

    votes?.forEach(({ cant, title }) => {
      total += cant;
      likes.push(cant);
      voteSuppot.push({
        count: `${cant}% - ${like}`,
        svg: likeIcon,
        description: title,
      });
    });
    likes.forEach((item, i) => {
      total = total === 0 ? 1 : total;
      const likesP = ((item / total) * 100).toFixed();

      voteSuppot[i].count = `${likesP}% - ${item} Voto(s)`;
    });

    if (partial)
      res = [
        {
          title: "Compartidas",
          cards: socialMedia(Object.values(shareds)),
        },
      ];
    else
      res = [
        {
          title: "Votos",
          cards: [...voteSuppot],
        },
        {
          title: "Tipos de participaciones",
          cards: [
            {
              count: `${favorites?.BUSINESS ?? 0} Usuario(s)`,
              description: "Con interés de negocio",
            },
            {
              count: `${favorites?.FAMILY ?? 0} Usuario(s)`,
              description: "Con interés de familiares",
            },
            {
              count: `${favorites?.HOME ?? 0} Usuario(s)`,
              description: "Con interés de domicilio",
            },
          ],
        },
        {
          title: "Compartidas",
          cards: socialMedia(Object.values(shareds)),
        },
      ];
  }
  if (mod === "d") {
    res = [
      {
        title: "Apoyos y Desacuerdos",
        cards: [
          {
            count: `${likesP}% - ${like}`,
            svg: likeIcon,
            description: "Apoyos a la charla",
          },
          {
            count: `${dislikesP}% - ${dislike}`,
            svg: disLikeIcon,
            description: "Desacuerdos a la charla",
          },
        ],
      },
      {
        title: "Comentarios",
        cards: [
          {
            count: comments ?? 0,
            svg: commentIcon,
            description: "Comentarios",
          },
        ],
      },
      {
        title: "Denuncias (en comentarios y respuestas)",
        cards: [
          {
            count: complaints ?? 0,
            svg: complaintIcon,
            description: "Denuncias a comentarios",
          },
        ],
      },
      {
        title: "Tipos de participaciones",
        cards: [
          {
            count: `${favorites?.BUSINESS ?? 0} Usuario(s)`,
            description: "Con interés de negocio",
          },
          {
            count: `${favorites?.FAMILY ?? 0} Usuario(s)`,
            description: "Con interés de familiares",
          },
          {
            count: `${favorites?.HOME ?? 0} Usuario(s)`,
            description: "Con interés de domicilio",
          },
        ],
      },
      {
        title: "Compartidas",
        cards: socialMedia(Object.values(shareds)),
      },
    ];
  }
  if (mod === "u")
    res = [
      {
        title: "Intereses",
        cards: [
          {
            count: `${favorites?.BUSINESS ?? 0} Usuario(s)`,
            description: "Con interés de negocio",
          },
          {
            count: `${favorites?.FAMILY ?? 0} Usuario(s)`,
            description: "Con interés de familiares",
          },
          {
            count: `${favorites?.HOME ?? 0} Usuario(s)`,
            description: "Con interés de domicilio",
          },
        ],
      },
      {
        title: "Bloqueados",
        cards: [
          {
            count: `${locked ?? 0} Usuario(s)`,
            svg: lockIcon,
            description: "Bloqueados",
          },
        ],
      },
    ];
  if (mod === "c")
    res = [
      {
        title: "Denuncias a comentarios",
        cards: [
          {
            count: `${serviced ?? 0} Atendida(s)`,
            svg: lockIcon,
            description: "Denuncias",
          },
        ],
      },
    ];

  return res;
}
