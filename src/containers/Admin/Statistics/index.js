import React, { useEffect, useState, useMemo } from "react";
import { useRouter } from "next/router";

import styles from "./statistics.module.scss";
import moment from "moment";
import Spin from "@components/Spin";

import SelectWraper from "@components/Select";
import RangePicker from "@components/DatePickerRange";
import Button from "@components/Button";
import Statistics from "@components/Statistics";
import { Download } from "@assets/icons";
import { StatisticServices } from "@services/index";
import { getReportData } from "./convertToReport";
import { getWorldTime } from "@utils/request";

import getModuleData from "./getMouduleData";
import message from "@components/Message";

import { useSelector } from "react-redux";

const optionsSelect2 = [
  { value: "this_week", label: "Esta semana" },
  { value: "last_week", label: "La semana pasada" },
  { value: "this_month", label: "Este Mes" },
  { value: "range", label: "Filtrar por fecha" },
];

const titles = {
  v: "Estadísticas de la votación",
  a: "Estadísticas de la convocatoria",
  d: "Estadísticas de la charla",
  n: "Estadísticas de la normativa",
  c: "Estadísticas de denuncias",
  u: "Estadísticas de usuarios",
};

let havePermission;
export default function StatisticsModule({ mod = "n", id }) {
  const [dropdown, setDropdown] = useState("this_week");
  const [loading, setLoading] = useState(false);
  const [datas, setDatas] = useState({});
  const [utcTime, setUtcTime] = useState();
  const {
    dataUser: { permissions, id: userId },
  } = useSelector((state) => state.auth);
  const router = useRouter();

  useEffect(() => {
    havePermission = permissions?.find(({ module }) =>
      ["statistics", ".*"].includes(module)
    );
  }, []);

  useEffect(() => {
    if (dropdown !== "range" && havePermission) getStatisticRequest();
  }, [dropdown]);

  const getStatisticRequest = async (range) => {
    const inst = new StatisticServices();
    try {
      setLoading(true);

      const datas = await inst.getStatistics({
        ...getDateFilter(range),
        [mod + "id"]: id,
      });

      setDatas(datas ?? {});
      console.log(datas);
    } catch (err) {
      console.log(err);
    }

    const worldDate = await getWorldTime();
    setUtcTime(worldDate);

    setLoading(false);
  };

  // const finished = false;
  const partial = useMemo(() => {
    let resp = false;

    if (mod === "v" && moment(datas?.dueDate).isSameOrAfter(moment(utcTime)))
      resp = true;

    console.log(resp, datas?.dueDate, utcTime);

    return resp;
  }, [utcTime, datas]);

  const getDateFilter = (date) => {
    let highDate;
    let lowDate;

    if (dropdown === "this_week") {
      highDate = moment(utcTime).endOf("day");
      lowDate = moment(utcTime)
        .subtract(1, "week")
        .add(1, "day")
        .startOf("day");
    } else if (dropdown === "last_week") {
      highDate = moment(utcTime).subtract(1, "week").endOf("day");
      lowDate = moment(utcTime)
        .subtract(2, "weeks")
        .add(1, "day")
        .startOf("day");
    } else if (dropdown === "this_month") {
      highDate = moment(utcTime).endOf("day");
      lowDate = moment(utcTime).startOf("month").startOf("days");
    } else {
      lowDate = moment(date[0]).startOf("day");
      highDate = moment(date[1]).endOf("day");
    }

    return {
      highDate,
      lowDate,
    };
  };

  async function onChange(date, dateString) {
    if (date.length > 0) {
      if (Math.abs(date[0].diff(date[1], "days")) > 60)
        message.error("La máxima cantidad de días es 60");
      else await getStatisticRequest(dateString);
    }
  }

  const description = `${
    datas?.createdAt
      ? "Inicio: " + moment(datas.createdAt).format("DD/MM/YYYY")
      : ""
  } ${
    datas?.dueDate
      ? " - Culminación: " + moment(datas.dueDate).format("DD/MM/YYYY")
      : ""
  }`;

  const downloadReport = async () => {
    const repData = getReportData(datas, mod);
    const metadatas = {
      title: datas.title,
      description: description !== "" ? description : undefined,
      caption: datas?.areas?.join(", "),
    };
    const inst = new StatisticServices();

    try {
      setLoading(true);

      const res = await inst.getReportUrl(
        { datas: repData, metadatas },
        userId,
        mod
      );
      const aTag = document.createElement("a");
      aTag.href = res?.url;
      aTag.download = "reporte.xlsx";
      aTag.target = "_blank";

      aTag.click();
      console.log(aTag);
    } catch (err) {
      console.log(err);
    }

    setLoading(false);
  };

  if (!["n", "a", "d", "v", "u", "c"].includes(mod))
    return (
      <section
        className={styles.filter__container}
        key={"SectionContainerConvocatorias"}
      >
        <div className={styles.header} key={"SectionHeaderPropuestas"}>
          <Button
            className={styles.filter_button + " mr-40"}
            type="secondary"
            size="big"
            onClick={(e) => {
              e.preventDefault();
              router.back();
            }}
          >
            &#8617; &nbsp;Volver
          </Button>
          <h1>Modulo no encontrado!!!</h1>
        </div>
      </section>
    );

  if (!havePermission)
    return (
      <section
        className={styles.filter__container}
        key={"SectionContainerConvocatorias"}
      >
        <div className={styles.header} key={"SectionHeaderPropuestas"}>
          <Button
            className={styles.filter_button + " mr-40"}
            type="secondary"
            size="big"
            onClick={(e) => {
              e.preventDefault();
              router.back();
            }}
          >
            &#8617; &nbsp;Volver
          </Button>
          <h1 className="mt-5 pt-3">
            ! Ops ¡
            <br />
            <br />
            Usted no cuenta con el siguiente permiso: &quot;Ver
            Estadísticas&quot;
          </h1>
        </div>
      </section>
    );

  if (!datas.title && !["u", "c"].includes(mod))
    return (
      <section
        className={styles.filter__container}
        key={"SectionContainerConvocatorias"}
      >
        <div className={styles.header} key={"SectionHeaderPropuestas"}>
          <Button
            className={styles.filter_button + " mr-40"}
            type="secondary"
            size="big"
            onClick={(e) => {
              e.preventDefault();
              router.back();
            }}
          >
            &#8617; &nbsp;Volver
          </Button>
          <h1> {loading ? "Cargando..." : "Registro no encontrado!!!"}</h1>
        </div>
      </section>
    );

  if (!datas.title && !["u", "c"].includes(mod))
    return (
      <section
        className={styles.filter__container}
        key={"SectionContainerConvocatorias"}
      >
        <div className={styles.header} key={"SectionHeaderPropuestas"}>
          <Button
            className={styles.filter_button + " mr-40"}
            type="secondary"
            size="big"
            onClick={(e) => {
              e.preventDefault();
              router.back();
            }}
          >
            &#8617; &nbsp;Volver
          </Button>
          <h1> {loading ? "Cargando..." : "Registro no encontrado!!!"}</h1>
        </div>
      </section>
    );

  return (
    <>
      <section
        className={styles.filter__container}
        key={"SectionContainerConvocatorias"}
      >
        <div className={styles.header} key={"SectionHeaderPropuestas"}>
          <div className="flex">
            <Button
              className={styles.filter_button + " mr-40"}
              type="secondary"
              size="big"
              onClick={(e) => {
                e.preventDefault();
                router.back();
              }}
            >
              &#8617; &nbsp;Volver
            </Button>
          </div>
          <h1>{datas.title}</h1>
          <div className={styles.information}>
            {datas.areas && (
              <span title={datas.areas.join(", ")}>
                {datas.areas.join(", ")}
              </span>
            )}
            <em>{description}</em>
          </div>
          {mod === "a" && (
            <div className={styles.information}>
              <em>
                Recepción de Propuestas:{" "}
                {moment(datas?.receptionEnd).format("DD/MM/YYYY")}
              </em>
            </div>
          )}
        </div>
        <h1 className={styles.title__staticstis}>{titles[mod]}</h1>
        <div className="flex flex-column nowrap w-100 justify-between">
          <div className={"w-max flex flex-row mb-30 " + styles.filter_select}>
            <SelectWraper
              onChange={(e) => {
                setDropdown(e[0].value);
              }}
              defaultValue={dropdown}
              options={optionsSelect2}
            ></SelectWraper>
            {dropdown === "range" && (
              <RangePicker
                fechaFinal={null}
                fechaInicial={null}
                separator="_"
                defaultPickerValue={[moment().subtract(1, "month"), moment()]}
                className={styles["custom-date-picket"]}
                disabledDate={(c) => {
                  const end = moment().endOf("day");

                  return c && end < c;
                }}
                dropdownClassName="custom-date-picket-dw"
                placeholder={["Desde", "Hasta"]}
                showTime={false}
                onChange={onChange}
              />
            )}
          </div>
          <div className="w-min mr-8">
            {!partial && (
              <Button
                className={styles.filter_button}
                type="primary"
                size="small"
                onClick={downloadReport}
              >
                Descargar reporte &nbsp;&nbsp;
                <Download />
              </Button>
            )}
          </div>
        </div>

        <Spin spinning={loading} size="large">
          <Statistics
            dataMap={
              datas?.points && !partial
                ? {
                    title: "Zonas donde hubo más participación",
                    data: {
                      defaultViewPort: {
                        width: "100%",
                        height: "100%",
                        latitude: -16.49173528499954,
                        longitude: -68.14757199433264,
                        zoom: 11.51762370911119,
                      },
                      points: datas.points,
                    },
                  }
                : undefined
            }
            dataChart={!partial ? datas.chart : undefined}
            dataSections={getModuleData(datas, mod, partial)}
          />
        </Spin>
      </section>
    </>
  );
}
