const daysChartTitle = {
  a: "Propuestas Creadas Por Dia",
  c: "Denuncias Recibidos Por Dia",
  u: "Usuarios Registrados Por Dia",
};

const favoritesTitle = {
  a: "Tipos de Participacion",
  c: "Tipos de Participacion",
  d: "Tipos de Participacion",
  v: "Tipos de Participacion",
  u: "Intereses Registrados",
  n: "Tipos de Participacion",
};

const generalTitle = {
  a: "Estadísticas Generales",
  c: "Estadísticas Generales",
  d: "Estadísticas Generales",
  u: "Estadísticas Generales",
  n: "Introduccion a la Normativa",
};

export function getReportData(datas, mod) {
  const allTables = [];
  const {
    daysChart,
    points,
    shareds,
    favorites,
    locked,
    serviced,
    like,
    dislike,
    complaints,
    comments,
    byState,
    votes,
    chapters,
  } = datas;

  if (mod !== "v") {
    const general = {
      title: generalTitle[mod],
      header: ["Nombre", "Cantidad"],
      datas: [],
    };

    if (locked) general.datas.push(["Bloqueados", parseInt(locked)]);
    if (serviced) general.datas.push(["Total Atendidas", parseInt(serviced)]);
    if (typeof like === "number") general.datas.push(["Apoyos", like]);
    if (typeof dislike === "number")
      general.datas.push(["Desacuerdos", dislike]);
    if (typeof complaints === "number")
      general.datas.push(["Denuncias", complaints]);
    if (typeof comments === "number")
      general.datas.push(["Comentarios", comments]);

    allTables.push(general);
  }

  chapters?.forEach(({ cod, comments, complaints, dislike, like }) => {
    const chapter = {
      title: `Datos de Capitulo ${cod}`,
      header: ["Nombre", "Cantidad"],
      datas: [
        ["Comentarios", comments],
        ["Denuncias", complaints],
        ["Apoyos", dislike],
        ["Desacuerdos", like],
      ],
    };

    allTables.push(chapter);
  });

  if (votes) {
    const vote = {
      title: "Votos Emitidos",
      header: ["Propuesta", "Votos"],
      datas: [],
    };
    votes.forEach(({ cant, title }) => {
      vote.datas.push([title, cant]);
    });

    allTables.push(vote);
  }

  if (daysChart) {
    const chart = {
      title: daysChartTitle[mod],
      header: ["Fecha", "Cantidad"],
      datas: [],
    };
    daysChart.forEach(({ day, month, year, cant }) => {
      chart.datas.push([`${day}/${month}/${year}`, cant]);
    });

    allTables.push(chart);
  }

  if (favorites) {
    const { BUSINESS, FAMILY, HOME } = favorites;

    const fav = {
      title: favoritesTitle[mod],
      header: ["Interes", "Cantidad"],
      datas: [
        ["Domicilio", HOME],
        ["Familiares", FAMILY],
        ["Negocio", BUSINESS],
      ],
    };

    allTables.push(fav);
  }

  if (byState) {
    const { APPROVED, REJECTED, WAITING, WINNER } = byState;

    const state = {
      title: "Propuestas Por Estado",
      header: ["Estado", "Cantidad"],
      datas: [
        ["Aprobadas", APPROVED],
        ["Ganadoras", WINNER],
        ["Rechazadas", REJECTED],
        ["Pendientes", WAITING],
      ],
    };

    allTables.push(state);
  }

  if (shareds) {
    const { FACEBOOK, LINK, TELEGRAM, TWITTER, WHATSAPP } = shareds;

    const shar = {
      title: "Total Compartidas",
      header: ["Red Social", "Cantidad"],
      datas: [
        ["Facebook", FACEBOOK],
        ["Por Link", LINK],
        ["Telegram", TELEGRAM],
        ["Twitter", TWITTER],
        ["Whatsapp", WHATSAPP],
      ],
    };

    allTables.push(shar);
  }

  if (points) {
    const location = {
      title: "Zonas de Participacion",
      header: ["ID", "Nombre", "Cantidad"],
      datas: [],
    };

    points.forEach(({ id, name, votes }) => {
      location.datas.push([id, name, votes]);
    });

    allTables.push(location);
  }

  return allTables;
}
