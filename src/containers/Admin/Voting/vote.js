import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import Wrapped from "@containers/Wrapper/WrapperLogin";
import ButtonComponent from "@components/Button";
import { OptionsProposal } from "./components/optionProsal";
import moment from "moment";
import Styles from "./adminVooting.module.scss";
import draftToHtml from "draftjs-to-html";
import ZonesModal from "@components/Modals/ZonesModal";

import { Download } from "@assets/icons";
import { getWorldTime } from "@utils/request";

const Vote = () => {
  const { activePost } = useSelector((state) => state.adminVoting);

  return (
    <Wrapped seo={activePost?.title}>
      <ActivePost />
    </Wrapped>
  );
};

export default Vote;

const ActivePost = () => {
  const { activePost } = useSelector((state) => state.adminVoting);
  const router = useRouter();
  const [imageUrl, setImageUrl] = useState("/images/banner.jpg");
  const [htmlContent, setHtmlContent] = useState("");
  const [textContent, setTextContent] = useState("");
  const [totalVotes, setTotalVotes] = useState(0);
  const [dateTextToShow, setDateTextToShow] = useState("");
  const [proposalWinnerIndex, setProposalWinnerIndex] = useState(0);
  const [ShowMoreProposalActive, setShowMoreProposalActive] = useState(null);
  const [showZonesModal, setShowZonesModal] = useState(false);
  const [isFinished, setIsFinished] = useState(false);

  useEffect(() => {
    // Image
    setImageUrl(
      activePost?.fileLink !== "" ? activePost?.fileLink : "/images/banner.jpg"
    );
    // Text Context
    try {
      setHtmlContent(JSON.parse(activePost?.content));
      setTextContent("");
    } catch (error) {
      setHtmlContent("");
      setTextContent(activePost?.content);
    }
    // Votes
    try {
      let _total = 0;
      let maxPosition = 0;
      let maxVotes = 0;
      for (let index = 0; index < activePost?.proposals.length; index++) {
        const element = activePost?.proposals[index];
        _total = _total + element.votes;
        if (element.votes > maxVotes) {
          maxVotes = element.votes;
          maxPosition = index;
        }
      }
      setTotalVotes(_total);
      setProposalWinnerIndex(maxPosition);
    } catch (error) {
      setTotalVotes(0);
    }

    setWorldDate();
  }, [activePost]);

  const setWorldDate = async () => {
    const worldDate = await getWorldTime();

    // DateTextToShow
    try {
      const now = moment(worldDate);
      const startDate = moment(activePost?.startDate);
      const endDate = moment(activePost?.dueDate);

      if (startDate.diff(now, "days") > 0) {
        setDateTextToShow(
          `La votación empieza el ${startDate.format("DD-MM-YYYY")}`
        );
      } else {
        setDateTextToShow(
          `La votación finaliza el ${endDate.format("DD-MM-YYYY")}`
        );
      }
    } catch (error) {
      setDateTextToShow(
        `La votación finaliza el ${activePost?.dueDate.substring(0, 10)}`
      );
    }

    // Check if is finished
    try {
      const finished = activePost?.completed
        ? moment(worldDate).diff(moment(activePost?.dueDate)) > 0
        : false;

      setIsFinished(finished);
    } catch (error) {
      setIsFinished(false);
    }
  };

  const handleShowMoreProposal = (option) => {
    try {
      if (option === null) {
        setShowMoreProposalActive(option);
      } else {
        const _proposal = { ...option, content: JSON.parse(option?.content) };
        setShowMoreProposalActive(_proposal);
      }
    } catch (error) {
      setShowMoreProposalActive(option);
    }
  };

  if (activePost) {
    return (
      <>
        <div className={Styles.voteContainer}>
          {/* Button */}
          <div className=" h-5  flex justify-between">
            <ButtonComponent
              size="super-small"
              type="secondary"
              onClick={() => {
                router.back();
              }}
            >
              <div className="flex">
                <span className="text-callout">&#9666; Volver</span>
              </div>
            </ButtonComponent>
            {(isFinished || activePost?.completed) && (
              <ButtonComponent
                size="super-small"
                type="secondary"
                onClick={() => {
                  router.push(
                    `/admin/estadisticas/votaciones/${activePost.id}`
                  );
                }}
              >
                <div className="flex py-1">
                  <span className="text-callout">
                    Estadísticas de la votación &#11290;
                  </span>
                </div>
              </ButtonComponent>
            )}
          </div>

          <div className="font-bold text-H3 mb-3 font-body mt-4 text-center">
            <span>{activePost?.title}</span>
          </div>
          <div className="flex gap-3 items-center text-body">
            <div
              className="bg-stateConfirmed p-2 rounded-sm text-zones cursor-pointer font-bold"
              onClick={() => setShowZonesModal(true)}
            >
              Ver zonas
            </div>
            <div className="text-lightGrey text-callout">
              {activePost?.proposals?.length} opciones a votar
            </div>
          </div>

          {activePost?.completed ? (
            isFinished ? (
              <div className="my-3 text-red text-bodyBold bg-errorBackground p-12 w-380 rounded-sm h-43 flex">
                <span className="ml-18">
                  <Image
                    src="/icons/checked-red.svg"
                    width={20}
                    height={20}
                    layout="fixed"
                    alt="checkedred"
                  />
                </span>
                <span className="mt-11 ml-12">La votación finalizó</span>
              </div>
            ) : (
              <div className="my-3 text-greenText text-headline">
                <span>{dateTextToShow}</span>
              </div>
            )
          ) : (
            <div className="my-3 text-fgCreating text-headline bg-bgCreating w-1/5 rounded-sm h-43 flex p-1">
              <span>En creación</span>
            </div>
          )}

          <div className="grid justify-items-center mt-40">
            <img
              src={imageUrl}
              className="max-w-500 max-h-500 object-contain"
              alt="alter"
            />
          </div>
          {htmlContent !== "" ? (
            <div
              className="text-bodyMedium pb-3 leading-relaxed mt-40 text-justify mb-16 text-textColor4"
              dangerouslySetInnerHTML={{ __html: draftToHtml(htmlContent) }}
            />
          ) : (
            <div className="text-bodyMedium pb-3 leading-relaxed mt-40 text-justify mb-16 text-textColor4">
              {textContent}
            </div>
          )}
          {activePost?.attached && (
            <div className="flex p-2">
              <ButtonComponent
                size="super-small"
                type="secondary"
                href={`${activePost?.attached[0]}?name=${activePost?.attached[1]}`}
                download={`${activePost?.title}.pdf`}
              >
                <div className="flex gap-1 items-center p-3">
                  <span>{activePost?.attached[1]}</span>
                  <Download />
                </div>
              </ButtonComponent>
            </div>
          )}

          <div className="font-bold text-center text-H6 mb-5 text-greenText">
            ¿ {activePost?.question} ?
          </div>
          <div className={Styles.voteProposalContainer}>
            {activePost?.proposals?.map((x, i) => {
              return (
                <OptionsProposal
                  key={i}
                  option={x}
                  index={i}
                  handleShowMoreProposal={handleShowMoreProposal}
                  column={activePost?.proposals.length}
                  showResult={isFinished}
                  totalVots={totalVotes}
                  isWinner={proposalWinnerIndex === i}
                />
              );
            })}
          </div>
        </div>
        {/* Modal Proposal Active */}
        {ShowMoreProposalActive !== null && (
          <>
            <div
              className={Styles.modalProposalBackground}
              onClick={() => handleShowMoreProposal(null)}
            />
            <div className={Styles.modalProposalViewMoreDetail}>
              <div className="font-bold mb-3 text-center text-H6 bg-greyheader p-1 sticky top-0">
                {ShowMoreProposalActive?.title}
              </div>
              {ShowMoreProposalActive?.image !== "" && (
                <img
                  src={ShowMoreProposalActive?.image}
                  className="object-scale-down h-3/6 "
                  alt="alter"
                />
              )}
              {/* <div
                className="pb-3 text-justify p-3 text-xl"
                dangerouslySetInnerHTML={{
                  __html: draftToHtml(ShowMoreProposalActive?.content),
                }}
              /> */}
              {ShowMoreProposalActive?.content?.blocks ? (
                <div
                  className="pb-3 text-justify p-3 text-xl"
                  dangerouslySetInnerHTML={{
                    __html: draftToHtml(ShowMoreProposalActive?.content),
                  }}
                />
              ) : (
                <div className="pb-3 text-justify p-3 text-xl">
                  {ShowMoreProposalActive?.content}
                </div>
              )}

              <div className="flex justify-around">
                {ShowMoreProposalActive?.attached && (
                  <ButtonComponent
                    size="super-small"
                    type="link"
                    href={`${ShowMoreProposalActive?.attached[0]}?name=${ShowMoreProposalActive?.attached[1]}`}
                    target="_blank"
                    download
                  >
                    <div className="flex gap-1">
                      <Image
                        src="/icons/attache.svg"
                        width={20}
                        height={20}
                        alt="file"
                        layout="fixed"
                      />
                      <span>Ver Adjunto</span>
                    </div>
                  </ButtonComponent>
                )}

                <ButtonComponent
                  size="super-small"
                  type="secondary"
                  onClick={() => handleShowMoreProposal(null)}
                  className="mx-3 w-2/6"
                >
                  <span className="text-headline">Cerrar</span>
                </ButtonComponent>
              </div>
              <div className="h-6" />
            </div>
          </>
        )}
        <ZonesModal
          visible={showZonesModal}
          closable={false}
          onCancel={() => setShowZonesModal(false)}
          macros={activePost?.macros}
          zones={activePost?.zones}
        />
      </>
    );
  }

  return <div></div>;
};
