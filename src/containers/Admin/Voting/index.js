import React, { useEffect } from "react";
import Image from "next/image";
import { useSelector, useDispatch } from "react-redux";
import ButtonComponent from "@components/Button";
import DeleteModal from "@components/deleteModal";
import ErrorModal from "@components/ErrorModal";
import Loader from "@components/loader";

import Wrapped from "@containers/Wrapper/WrapperLogin";

import { BodySection } from "./components/admin_BodySection";
import { PaginationCustom } from "./components/admin_PaginationSection";

import { CreatingHeader } from "./components/creating_header";
import { CreatingStepBar } from "./components/creating_stepBar";
import { CreatingStep1 } from "./components/creating_step1";
import { CreatingStep2 } from "./components/creating_step2";
import { CreatingStep3 } from "./components/creating_step3";
import { CreatingStep4 } from "./components/creating_step4";
import { CreatingStep5 } from "./components/creating_step5";

import Styles from "./adminVooting.module.scss";

import {
  closeCreatingModal,
  getVotingList,
  getZone,
  getTotalPost,
  getMacroZones,
  closeDeleteModal,
  deleteProposal,
  closeErrorModal,
} from "@redux/admin_voting/actions";

const AdminVotingContainer = () => {
  const dispatch = useDispatch();
  const { creating, filters, deleting, localPost, loading, error } =
    useSelector((state) => state.adminVoting);

  useEffect(() => {
    dispatch(getVotingList(filters));
    dispatch(getTotalPost(filters));
  }, [filters]);

  useEffect(() => {
    dispatch(getZone());
    dispatch(getMacroZones());
  }, []);

  return (
    <>
      <Wrapped
        seo="Tu Voto Cuenta"
        defaultKey="voting"
        routeName="Tu voto cuenta"
      >
        <BodySection />
        <PaginationCustom />
        {creating && (
          <div className={Styles.rootModalCreating}>
            <AdminCreatingVotingScreen />
          </div>
        )}
        {deleting && (
          <DeleteModal
            closeModal={() => dispatch(closeDeleteModal())}
            title={localPost?.title}
            delteFuntion={() => dispatch(deleteProposal(localPost?.id))}
          />
        )}
        {loading && creating && <Loader title="Guardando..." />}
        {loading && !creating && <Loader title="Cargando..." />}
        {error && (
          <ErrorModal
            Message={error}
            closeModal={() => dispatch(closeErrorModal())}
          />
        )}
      </Wrapped>
    </>
  );
};

export default AdminVotingContainer;

const AdminCreatingVotingScreen = () => {
  const dispatch = useDispatch();

  const { creatingStep, showModalCreated, localPost } = useSelector(
    (state) => state.adminVoting
  );

  const handleOnCloseModal = () => {
    dispatch(closeCreatingModal());
  };

  return (
    <>
      <div className={`flex bg-white  h-screen justify-center pt-3`}>
        <div className={`h-screen w-4/5 bg-white px-2 flex flex-col gap-4`}>
          <CreatingHeader />
          <CreatingStepBar />
          <div
            className={`${
              creatingStep !== 4 ? "border-2 border-greyheader h-4/6" : "h-5/6"
            }  rounded-sm p-1 overflow-auto hover:overflow-scroll`}
          >
            {creatingStep === 0 && <CreatingStep1 />}
            {creatingStep === 1 && <CreatingStep2 />}
            {creatingStep === 2 && <CreatingStep3 />}
            {creatingStep === 3 && <CreatingStep4 />}
            {creatingStep === 4 && <CreatingStep5 />}
          </div>
        </div>
      </div>
      {showModalCreated && (
        <div className={Styles.modalCreatedSuccessfully}>
          <Image
            alt="okIcon"
            src="/icons/successAction.svg"
            width={50}
            height={50}
            layout="fixed"
          />
          <div className="text-greenText font-bold text-2xl px-2 text-center">
            ¡Excelente! Se {localPost.id ? "actualizó" : "creó"} la votación
          </div>
          <ButtonComponent
            size="super-small"
            type="link"
            onClick={handleOnCloseModal}
          >
            Cerrar
          </ButtonComponent>
        </div>
      )}
    </>
  );
};
