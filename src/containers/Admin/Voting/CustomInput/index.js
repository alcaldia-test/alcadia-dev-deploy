import React, { useEffect, useState } from "react";
import styles2 from "./customInput.module.scss";

const CustomImput = ({
  onChange,
  value,
  name,
  whiteBackGround = false,
  includeSimboleStart = "",
  includeSimboleEnd = "",
  placeholder,
  validateField = false,
  minLength = 10,
  messageOfError = "",
}) => {
  const [errorMessage, setErrorMessage] = useState();

  useEffect(() => {
    if (validateField) {
      if (value.length < minLength) {
        setErrorMessage(messageOfError);
      } else {
        setErrorMessage(null);
      }
    }
  }, [validateField, value]);

  return (
    <>
      <div className={errorMessage ? styles2.externalError : styles2.external}>
        {includeSimboleStart !== "" && <span>{includeSimboleStart}</span>}
        <input
          name={name}
          className={
            whiteBackGround ? styles2.externalWhite : styles2.inputnone
          }
          value={value}
          onChange={onChange}
          autoComplete="off"
          placeholder={placeholder}
        />
        {includeSimboleEnd !== "" && <span>{includeSimboleEnd}</span>}
      </div>
      {errorMessage && (
        <span className="text-red text-callout">{errorMessage}</span>
      )}
    </>
  );
};

export default CustomImput;
