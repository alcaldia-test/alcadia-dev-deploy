import React, { useState, useEffect } from "react";
import ButtonComponent from "@components/Button";

export const OptionsProposal = ({
  option,
  index = 0,
  handleShowMoreProposal,
  showResult = false,
  totalVots = 1,
  isWinner = false,
  column = 3,
}) => {
  const [imageUrl, setImageUrl] = useState("");

  const [color, setColor] = useState("bg-redCard border-redCard");

  const [content, setContent] = useState("");

  const [width, setWidth] = useState("w-full");

  const [procentaje, setPorcentaje] = useState("");

  useEffect(() => {
    try {
      if (option?.image?.originFileObj) {
        setImageUrl(URL.createObjectURL(option?.image?.originFileObj));
      } else {
        if (typeof option?.fileLink === "string") {
          setImageUrl(option?.fileLink);
        } else {
          setImageUrl("");
        }
      }
    } catch (error) {
      setImageUrl("");
    }
  }, [option]);

  useEffect(() => {
    try {
      if (Number.isInteger(index / 3)) {
        setColor("bg-purpleCard border-purpleCard");
      } else if (Number.isInteger(index / 2)) {
        setColor("bg-greenCard border-greenCard");
      }
    } catch (error) {}
  }, [option]);

  useEffect(() => {
    try {
      if (option?.content?.blocks?.length > 0) {
        setContent(option?.content?.blocks[0]?.text?.substring(0, 100));
      } else {
        const _obj = JSON.parse(option?.content);
        setContent(_obj?.blocks[0]?.text?.substring(0, 100));
      }
    } catch (error) {
      setContent(option?.content?.substring(0, 100));
    }
  }, [option]);

  useEffect(() => {
    if (column >= 3) {
      setWidth("w-30p");
    } else if (column >= 2) {
      setWidth("w-45p");
    } else {
      setWidth("w-full");
    }
  }, [option]);

  useEffect(() => {
    let _porcentaje = 0;

    if (totalVots !== 0) _porcentaje = (option?.votes / totalVots) * 100;

    setPorcentaje(_porcentaje);
  }, [option, totalVots]);

  return (
    // overflow-hidden flex flex-col justify-around
    <div
      className={`border rounded-card ${color} ${width} p-16 overflow-hidden h-1/5`}
    >
      <div className="font-bold leading-relaxed text-H6 mt-16 text-textColor1 overflow-hidden max-h-111">
        {option?.title}
      </div>
      {!showResult && (
        <>
          {imageUrl !== "" && (
            <div className="mt-16 grid justify-center">
              <img
                className="object-contain h-180 "
                src={imageUrl}
                alt="alter"
              />
            </div>
          )}
          <div className="text-bodyMedium pb-3 leading-relaxed mt-16 text-justify mb-16 text-textColor4">
            {content}
          </div>
        </>
      )}
      <div className="align-middle justify-center grid">
        <ButtonComponent
          size="super-small"
          type="link"
          onClick={() => handleShowMoreProposal({ ...option, image: imageUrl })}
        >
          <span className="text-headline">Ver más información</span>
        </ButtonComponent>
      </div>
      {showResult && (
        <div
          className={`${isWinner ? "bg-blueLight" : "bg-backgroundColorGrey"} ${
            isWinner ? "text-white" : "text-textColor1"
          }  text-center p-12 rounded-full`}
        >
          <span className="text-headline">{Math.round(procentaje)}%</span>
        </div>
      )}
    </div>
  );
};
