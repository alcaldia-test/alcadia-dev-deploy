import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import WYSIWYGCOMPONENET from "@components/WYSIWYG";
import ButtonComponent from "@components/Button";
import UploadComponent from "@components/Upload";

// import { fetchFiles } from "@utils/fetch_custom";
import {
  changeDataLocalPost,
  changeCreatingStep,
  deleteFile,
} from "@redux/admin_voting/actions";

export const CreatingStep2 = () => {
  const { localPost, validateField } = useSelector(
    (state) => state.adminVoting
  );

  const [textLength, setTextLength] = useState(0);

  useEffect(() => {
    try {
      if (localPost?.content?.blocks) {
        let _textLength = 0;
        for (
          let index = 0;
          index < localPost?.content?.blocks?.length;
          index++
        ) {
          const block = localPost?.content?.blocks[index];
          _textLength = _textLength + block?.text?.length;
        }
        // setTextLength(localPost?.content?.blocks[0]?.text?.length);
        setTextLength(_textLength);
      } else {
        setTextLength(0);
      }
    } catch (error) {
      setTextLength(0);
    }
  }, [localPost]);

  const dispatch = useDispatch();

  const handleChange = (e) => {
    if (e) {
      dispatch(changeDataLocalPost({ name: "content", value: e }));
    }
  };

  const handleChangeFile = (info) => {
    if (info.file.status === "uploading") {
      const _file = info?.file;
      // console.log(_file);
      // services
      //   .createPostUploadFiles({
      //     id: "d27442f0-6f0f-11ec-8168-19c43aaae9ff",
      //     file: _file?.originFileObj,
      //     container: "posts",
      //     tag: "banner",
      //   })
      //   .then((x) => {
      //     console.log(x);
      //   });
      _file.status = "done";
      dispatch(changeDataLocalPost({ name: "extraFile", value: _file }));
    }

    if (info.file.status === "removed") {
      dispatch(changeDataLocalPost({ name: "extraFile", value: undefined }));
    }
  };

  const handleOnClick = () => {
    dispatch(changeCreatingStep(2));
  };

  const handleOnDeleteFile = (tag) => {
    const payload = {
      tag: "attached",
      id: localPost?.postsId,
      PostionFieldName: "attached",
    };
    dispatch(deleteFile(payload));
  };

  return (
    <div className="flex flex-col gap-3 p-2">
      <span className="font-bold text-H6">
        Redacta una breve introducción a la votación
      </span>
      <span className="text-bodyMedium mb-2">
        Las introducciones más exitosas suelen tener 3 párrafos. Te recomendamos
        que agregues aproximadamente <span className="text-redDart">1000</span>{" "}
        caracteres
      </span>
      <WYSIWYGCOMPONENET
        onChange={handleChange}
        row={5}
        value={localPost?.content || {}}
        minCharacter={100}
        rawContentState={localPost?.content || {}}
      />
      {validateField && textLength < 98 && (
        <span className="text-red text-callout">
          Es necesario añadir una descripción de por lo menos 100 caracteres
        </span>
      )}
      <UploadComponent
        onChange={handleChangeFile}
        sizeButton="small-x"
        fileList={localPost?.extraFile ? [localPost?.extraFile] : []}
        filesInServer={localPost?.attached ? [localPost?.attached] : []}
        allowAddMoreToServer={false}
        onDeleteFunction={handleOnDeleteFile}
        accept=".pdf"
      />
      <div className="flex items-center justify-center">
        <ButtonComponent size="small-x" onClick={handleOnClick}>
          <span className="text-bodyMedium">Siguiente</span>
        </ButtonComponent>
      </div>
    </div>
  );
};
