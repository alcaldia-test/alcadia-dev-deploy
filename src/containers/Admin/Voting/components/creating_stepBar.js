import React from "react";
import { useSelector, useDispatch } from "react-redux";
import StepBar from "@components/Stepper";
import { changeCreatingStep } from "@redux/admin_voting/actions";

const styles = {
  class1: "w-5/6 place-self-center items-center ",
};

export const CreatingStepBar = () => {
  const dispatch = useDispatch();

  const { creatingStep } = useSelector((state) => state.adminVoting);

  const setStep = (e) => {
    dispatch(changeCreatingStep(e));
  };

  return (
    <div className={styles.class1}>
      <StepBar step={creatingStep} setStep={setStep} />
    </div>
  );
};
