import React, { useState } from "react";
import Image from "next/image";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import {
  showHideCreatingSection,
  changevalidatingField,
  openErrorModal,
  creatingVotingProposal,
  changeCreatingStep,
} from "@redux/admin_voting/actions";
import ButtonComponent from "@components/Button";
import DeleteModal from "@components/deleteModal";

export const CreatingHeader = () => {
  const dispatch = useDispatch();

  const { loading, localPost, validateField, vote } = useSelector(
    (state) => state.adminVoting
  );

  const [clickInButton, setClickInButton] = useState(0);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const handleHideModal = () => {
    setShowDeleteModal(false);
    dispatch(showHideCreatingSection());
  };

  const handleSavePost = () => {
    setShowDeleteModal(false);

    dispatch(openErrorModal());

    console.log(validateField, vote, localPost);

    if (validateField) {
      dispatch(changevalidatingField());
    }

    const errors = localPost?.completed
      ? validarCampos()
      : validarCamposTerminarLuego();
    // const errors = validarCamposTerminarLuego();

    if (errors.length === 0) {
      const _localPost = { ...localPost };
      const completed = localPost?.completed || false;
      if (!_localPost?.startDate) {
        const _startDate = moment().startOf("day").toISOString();
        _localPost.startDate = _startDate;
      }
      if (!_localPost?.dueDate) {
        const _dueDate = moment().endOf("day").toISOString();
        _localPost.dueDate = _dueDate;
      }

      dispatch(creatingVotingProposal({ ..._localPost, completed }));
    } else {
      // if (clickInButton === 0) {
      dispatch(openErrorModal(errors));
      dispatch(changevalidatingField());
      setClickInButton(clickInButton + 1);
      // } else {
      //   dispatch(showHideCreatingSection());
      // }
    }
  };

  const validarCampos = () => {
    const errors = [];
    let returnToPage = null;

    if (!localPost?.dueDate) {
      errors.push("Es necesario añadir una fecha de inicio y culminación");
      returnToPage = 3;
    }

    if (moment(localPost?.dueDate).diff(moment(), "seconds") < 0) {
      errors.push(
        "La fecha de finalizacion de la votacion no puede ser antes de la fecha actual"
      );
      returnToPage = 3;
    }

    if (!localPost?.question || localPost?.question?.length < 3) {
      errors.push("Es necesario añadir una pregunta");
      returnToPage = 2;
    }

    if (!localPost?.proposals || localPost?.proposals?.length < 1) {
      errors.push("Es necesario añadir una opción a votar");
      returnToPage = 2;
    }

    localPost?.proposals?.forEach((x) => {
      if (!x?.title || x?.title.length < 2) {
        errors.push("Todas las opciones deberian tener por lo menos un titulo");
        returnToPage = 2;
      }
    });

    try {
      let _textLength = 0;

      if (typeof localPost?.content === "object")
        for (
          let index = 0;
          index < localPost?.content?.blocks?.length;
          index++
        ) {
          const block = localPost?.content?.blocks[index];
          _textLength = _textLength + (block?.text?.length ?? 0);
        }
      else _textLength = localPost?.content?.length ?? 0;

      if (_textLength < 100) {
        errors.push("Es necesario añadir una descripción");
        returnToPage = 1;
      }
    } catch (error) {}

    if (!localPost?.title || localPost?.title?.length < 3) {
      errors.push("Es necesario añadir un título");
      returnToPage = 0;
    }
    if (returnToPage !== null) {
      dispatch(changeCreatingStep(returnToPage));
      if (!validateField) {
        dispatch(changevalidatingField());
      }
    }
    if (returnToPage !== null) {
      dispatch(changeCreatingStep(returnToPage));
      if (!validateField) {
        dispatch(changevalidatingField());
      }
    }

    return errors;
  };

  const handleShowHideModal = () => {
    setShowDeleteModal(true);
  };

  const validarCamposTerminarLuego = () => {
    const errors = [];
    let returnToPage = null;
    if (!localPost?.title || localPost?.title?.length < 3) {
      errors.push(
        "La votación no se guardará, es necesario añadir un título si desea salir puedes hacer click en salir de nuevo"
      );
      returnToPage = 0;
    }
    if (returnToPage !== null) {
      dispatch(changeCreatingStep(returnToPage));
      // if (validateField) {
      //   dispatch(changevalidatingField());
      // }
    }

    return errors;
  };

  return (
    <div className="flex h-6">
      {/* Button Left */}
      <div className="flex items-center">
        <div className=" h-5 w-2/5">
          <ButtonComponent
            size="super-small"
            type="secondary"
            onClick={handleShowHideModal}
          >
            <div className="flex">
              <span className="text-callout">&#9666; Salir</span>
            </div>
          </ButtonComponent>
        </div>
      </div>
      {/* Text Center */}
      <div className="w-4/5 flex items-center ml-3 ">
        <span className="font-bold text-headline">Creación de Votaciones</span>
      </div>
      {/* Button Right */}
      <div className="w-1/5 flex justify-center items-center">
        {loading && (
          <div className="flex items-center justify-center">
            <span style={{ fontSize: 10 }}>Guardando...</span>
            <Image
              src="/icons/save.svg"
              layout="fixed"
              width={18}
              height={18}
              alt="xboxIcon"
            />
          </div>
        )}
      </div>
      {showDeleteModal && (
        <DeleteModal
          onlyClose={() => {
            setShowDeleteModal(false);
          }}
          closeModal={handleHideModal}
          mainMessage="¿Desea guardar los cambios efectuados hasta aquí?"
          delteFuntion={handleSavePost}
          simple={true}
          mainIconSrc="/icons/save.svg"
        />
      )}
    </div>
  );
};
