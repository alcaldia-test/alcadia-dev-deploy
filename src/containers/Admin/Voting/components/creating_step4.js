import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import DateField from "@components/DatePickerRange";
import ButtonComponent from "@components/Button";
import RadialComponent from "@components/RadioGroup";
import SelectListComponenet from "@components/SelectList";
import { getWorldTime } from "@utils/request";
import TagContainer from "@components/TagContainer";

import {
  changeDataLocalPost,
  changeCreatingStep,
} from "@redux/admin_voting/actions";

const _optionsMenu = [
  { id: "1", name: "Disponible para todas las zonas" },
  { id: "2", name: "Zonal" },
];

export const CreatingStep4 = () => {
  const { macroZones, localPost, validateField } = useSelector(
    (state) => state.adminVoting
  );

  const [macroZonesOrdered, setMacroZonesOrdered] = useState([]);
  const [utcTime, setUtcTime] = useState();

  const [isZoneSelected, setIsZoneSelected] = useState(
    "Disponible para todas las zonas"
  );

  const [showSubZones, setshowSubZones] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    if (localPost?.macrosId?.length > 0) {
      setIsZoneSelected("Zonal");
      setshowSubZones(true);
    }
  }, [localPost]);

  useEffect(() => {
    const _macroZonesOrdered = [];

    macroZones.forEach((macroZone) => {
      const checked = localPost?.macrosId?.includes(macroZone.id) || false;

      const _macroZone = {
        id: macroZone.id,
        name: macroZone.name,
        zones: [],
        checked,
      };

      _macroZonesOrdered.push(_macroZone);
    });

    setMacroZonesOrdered(_macroZonesOrdered);
    setWorldDate();
  }, []);

  const setWorldDate = async () => {
    const worldDate = await getWorldTime();
    setUtcTime(worldDate);
  };

  const handleChangeDatePicker = (e) => {
    try {
      const _startDate = e[0]?.startOf("day").toISOString();
      const _dueDate = e[1]?.endOf("day").toISOString();
      dispatch(
        changeDataLocalPost({
          name: "startDate",
          value: _startDate,
        })
      );
      dispatch(
        changeDataLocalPost({
          name: "dueDate",
          value: _dueDate,
        })
      );
    } catch (error) {}
  };

  const handleChange = (e) => {
    if (e?.target?.id === "2") {
      setshowSubZones(true);
      dispatch(changeDataLocalPost({ name: "macrosId", value: [] }));
    } else if (e?.target?.id === "1") {
      setshowSubZones(false);
      dispatch(changeDataLocalPost({ name: "macrosId", value: undefined }));
    }
  };

  const handleSelectZones = (e) => {
    const newMarcros = macroZonesOrdered.map((item) =>
      item.id === e.id
        ? {
            ...item,
            checked: !item?.checked,
          }
        : item
    );

    const _macrosId = [];

    newMarcros?.forEach((x) => {
      if (x.checked) _macrosId.push(x.id);
    });

    setMacroZonesOrdered(newMarcros);
    dispatch(changeDataLocalPost({ name: "macrosId", value: _macrosId }));
  };

  const handleOnClick = () => {
    dispatch(changeCreatingStep(4));
  };

  const handleChangeTags = (tagList) => {
    dispatch(changeDataLocalPost({ name: "tags", value: tagList }));
  };

  const disabledDate = (current) => {
    // Can not select days before today and today
    // const worldDate = await getWorldTime();

    return current && current < moment(utcTime).startOf("day");
  };

  // TODO EL INPUT NO RETORNA NAME

  return (
    <div className="flex flex-col gap-3 p-2">
      <div className="font-bold text-H5 text-center mb-2">
        Ya casi terminamos
      </div>
      <span className="font-bold text-H5">
        Establece la fecha de inicio y culminación de esta votación
      </span>
      <span className="text-bodyMedium">
        Configura la fecha en la que se anunciará el comienzo y culminación para
        la recolección de votos. Es importante que las personas sepan que los
        escuchamos.
      </span>
      <span className="grid border-2 border-greyheader   p-10 rounded-sm justify-around w-3/6">
        <DateField
          onChange={handleChangeDatePicker}
          fechaInicial={localPost?.startDate}
          fechaFinal={localPost?.dueDate}
          // value={[localPost?.startDate || null, localPost?.dueDate || null]}
          dateFormat="YYYY-MM-DD"
          disabledDate={disabledDate}
          showTime={false}
        />
      </span>
      {validateField && !localPost?.dueDate && (
        <span className="text-red text-callout">
          Es necesario añadir una fecha de inicio y culminación
        </span>
      )}
      <span className="h-50" />
      <span className="font-bold text-H5">
        Seleccione o añada etiquetas relacionadas a esta votación
      </span>
      <TagContainer
        onSelect={handleChangeTags}
        selectedtag={localPost?.tags || []}
      />
      <span className="font-bold text-H5 mt-3">
        ¿Esta votación será zonal o pueden participar los ciudadanos de todas
        las zonas?
      </span>
      <span className="text-bodyMedium">
        Establece la petición para zonas específicas de La Paz o configúrala
        para que todos los ciudadanos puedan participar.
      </span>
      <div className=" w-1/6">
        <RadialComponent
          onChange={handleChange}
          options={_optionsMenu}
          valueDefault={isZoneSelected}
        />
      </div>
      {showSubZones && (
        <div className="mx-3">
          <SelectListComponenet
            onSelect={handleSelectZones}
            list={macroZonesOrdered}
          />
        </div>
      )}

      <div className="flex items-center justify-center">
        <ButtonComponent size="small-x" onClick={handleOnClick}>
          <span className="text-bodyMedium">Siguiente</span>
        </ButtonComponent>
      </div>
    </div>
  );
};
