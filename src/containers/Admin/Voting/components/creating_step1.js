import React from "react";
import { useSelector, useDispatch } from "react-redux";
// import InputField from "@components/InputField";
// import InputField from "@components/Form/FormTextInput";
import ButtonComponent from "@components/Button";
import UploadComponent from "@components/Upload";
import CustomInput from "../CustomInput";

import {
  changeDataLocalPost,
  changeCreatingStep,
  deleteFile,
} from "@redux/admin_voting/actions";

export const CreatingStep1 = () => {
  const { localPost, validateField } = useSelector(
    (state) => state.adminVoting
  );

  const dispatch = useDispatch();

  const handleChange = (e) => {
    dispatch(
      changeDataLocalPost({
        name: e.target.name,
        value: e.target.value.substr(0, 200),
      })
    );
  };

  const handleChangeFile = (info) => {
    if (info.file.status === "uploading") {
      const _file = info?.file;
      _file.status = "done";
      dispatch(changeDataLocalPost({ name: "banner", value: _file }));
    }

    if (info.file.status === "removed") {
      dispatch(changeDataLocalPost({ name: "banner", value: undefined }));
    }
  };

  const handleOnClick = () => {
    dispatch(changeCreatingStep(1));
  };

  const handleOnDeleteFile = () => {
    const payload = {
      tag: "banner",
      id: localPost?.postsId,
      PostionFieldName: "fileLink",
    };
    dispatch(deleteFile(payload));
  };

  // TODO EL INPUT NO RETORNA NAME

  return (
    <div className="flex flex-col gap-3 p-2">
      <span className="font-bold text-H6">
        Escribe el título de la votación
      </span>
      <span className="text-bodyMedium">
        Capta la atención con un título corto que haga comprender a los
        ciudadanos sobre que temática trata la votación
      </span>
      {/* <InputField onChange={handleChange} /> */}
      <CustomInput
        onChange={handleChange}
        name="title"
        value={localPost?.title || ""}
        validateField={validateField}
        messageOfError="Es necesario añadir un título"
        minLength={3}
      />
      <span className="font-bold text-H5">Agrega una foto</span>
      <span className="text-bodyMedium">
        Esta es la foto que acompañará la presentación de la votación
      </span>
      <UploadComponent
        onChange={handleChangeFile}
        sizeButton="small-x"
        accept="image/*"
        textButton="Subir Imagen"
        multiple={false}
        fileList={localPost?.banner ? [localPost?.banner] : []}
        filesInServer={localPost?.fileLink ? [localPost?.fileLink] : []}
        allowAddMoreToServer={false}
        onDeleteFunction={handleOnDeleteFile}
      />
      <div className="flex items-center justify-center">
        <ButtonComponent size="small-x" onClick={handleOnClick}>
          <span className="text-bodyMedium">Siguiente</span>
        </ButtonComponent>
      </div>
    </div>
  );
};
