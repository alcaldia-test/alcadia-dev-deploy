import MenuComponent from "@components/Menu";

export const MenuCustom = () => {
  return (
    <MenuComponent
      collapsed={false}
      type="ADMIN"
      defaultSelectedKey=""
      notifications={{}}
    />
  );
};
