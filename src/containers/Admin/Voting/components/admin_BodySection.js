import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";
import {
  showHideCreatingSection,
  changeDataLocalFilter,
  changeDataLocalTextInAutoComplete,
  openDeleteModal,
  editPost,
  changevalidatingField,
} from "@redux/admin_voting/actions";

import ButtonComponent from "@components/Button";
import AutoCompleteComponent from "@components/Autocomplete";
import DashBoardComponent from "@components/Cards/Dashboard/DashboardCard";

import SelectComponent from "@components/Select";
import { getWorldTime } from "@utils/request";

import Styles from "../adminVooting.module.scss";
import moment from "moment";
import { checkPermision } from "@utils/adminPermission";

const optionsSelect = [
  // { value: "Actives", label: "Activas" },
  { value: "Recents", label: "Más Recientes" },
  { value: "Oldest", label: "Más Antiguas" },
  // { value: "Finished", label: "Finalizadas" },
];

const optionsSelect2 = [
  { value: "null", label: "Todas" },
  { value: "active", label: "En Curso" },
  { value: "finished", label: "Finalizadas" },
  { value: "creating", label: "En Creación" },
  { value: "pending", label: "Sin Empezar" },
];

const menuEnCurso = [
  { tag: "content", name: "Ver Contenido", permission: "any" },
  { tag: "stadistic", name: "Ver Estadísticas", permission: "any" },
  { tag: "edit", name: "Editar", permission: "edit" },
  { tag: "delete", name: "Eliminar Votación", permission: "delete" },
];

const menuEnCreacion = [
  { tag: "edit", name: "Continuar Creando", permission: "edit" },
  { tag: "delete", name: "Eliminar Votación", permission: "delete" },
];

const menuFinalizado = [
  { tag: "stadistic", name: "Ver Estadísticas", permission: "any" },
  { tag: "content", name: "Ver Resultados", permission: "any" },
  { tag: "delete", name: "Eliminar Votación", permission: "delete" },
];

export const BodySection = () => {
  // const { height } = useWindowSizeChange();

  const { listOfVoting, validateField } = useSelector(
    (state) => state.adminVoting
  );
  const { dataUser } = useSelector((state) => state.auth);

  const dispatch = useDispatch();
  const router = useRouter();

  const [cards, setCards] = useState([]);
  const [permissions, setPermissions] = useState({
    canDelete: false,
    canCreate: false,
    canEdit: false,
  });

  const optionsAutocomplete =
    listOfVoting.length > 0
      ? listOfVoting?.map((item) => ({
          title: item.title,
        }))
      : [];

  useEffect(() => {
    const _permissions = checkPermision(dataUser, "voting");
    setPermissions(_permissions);
  }, [dataUser]);

  useEffect(() => {
    setVotingCards();
  }, [listOfVoting]);

  const setVotingCards = async () => {
    const worldDate = await getWorldTime();

    const _cardsOrdered = [];
    try {
      listOfVoting?.forEach((card) => {
        const id = card.id;
        const image = card?.fileLink ? card?.fileLink : "/images/banner.jpg";
        const title = card?.title || "";
        const timeTask = card?.dueDate || "";
        const startDate = card?.startDate || "";
        const zones = card?.areas ? card?.areas : "";
        const finished = moment(worldDate).isAfter(moment(card?.dueDate))
          ? "finished"
          : "active";

        const state = card?.completed ? finished : "creating";
        _cardsOrdered.push({
          id,
          image,
          title,
          timeTask,
          startDate,
          zones,
          state,
          button: "",
          vignette: "",
          now: worldDate,
        });
      });
    } catch (error) {}

    setCards(_cardsOrdered);
  };

  const handleShowHideModal = () => {
    if (validateField) {
      dispatch(changevalidatingField());
    }
    dispatch(showHideCreatingSection());
  };

  const handleOnChangeSelect = (e) => {
    const selected = e[0];
    if (selected?.value === "Recents") {
      dispatch(changeDataLocalFilter({ name: "order", value: "DESC" }));
    } else if (selected?.value === "Oldest") {
      dispatch(changeDataLocalFilter({ name: "order", value: "ASC" }));
    } else if (selected?.value === "null") {
      dispatch(changeDataLocalFilter({ name: "completed", value: null }));
    } else {
      dispatch(
        changeDataLocalFilter({ name: "completed", value: selected?.value })
      );
    }
  };

  const handleSearch = (e) => {
    dispatch(changeDataLocalTextInAutoComplete(e));
    dispatch(changeDataLocalFilter({ name: "like", value: e }));
  };

  return (
    <div className={Styles.listCardContainer}>
      {/* Filters */}
      <div className={Styles.controlsContainer}>
        <div className={Styles.controls}>
          <SelectComponent
            options={optionsSelect2}
            onChange={handleOnChangeSelect}
          />
          <SelectComponent
            options={optionsSelect}
            onChange={handleOnChangeSelect}
            defaultValue={0}
          />
          <AutoCompleteComponent
            dataSource={optionsAutocomplete}
            placeholder={"Buscar"}
            onChange={handleSearch}
          />
        </div>
        {permissions?.canCreate && (
          <div className="flex items-center justify-end w-2/5 mr-16">
            <ButtonComponent size="small" onClick={handleShowHideModal}>
              Crear Votación +
            </ButtonComponent>
          </div>
        )}
      </div>

      {/* GRID */}
      <div className="flex flex-wrap justify-around p-1 gap-1">
        {cards?.map((x, i) => {
          const Vignette = () => {
            return <div>...</div>;
          };

          const ButtonBottom = () => {
            return (
              <ButtonComponent
                size="small-x"
                type="secondary"
                style={{ width: 270 }}
                onClick={() => {
                  if (x.state === "active" || x.state === "finished") {
                    handleOnMenuClick("content");
                  } else {
                    if (permissions.canEdit) {
                      handleOnMenuClick("edit");
                    }
                  }
                }}
              >
                {x.state === "active" && <span>Ver Contenido</span>}
                {x.state === "creating" && <span>Continuar Creando</span>}
                {x.state === "finished" && <span>Ver Resultados</span>}
              </ButtonComponent>
            );
          };

          const ImageCard = () => {
            return <img src={x.image} alt="alter" />;
          };

          const handleOnMenuClick = (e) => {
            if (e === "delete") {
              dispatch(openDeleteModal(x));
            }
            if (e === "content") {
              router.push(`/admin/votaciones/${x.id}`);
            }
            if (e === "edit") {
              dispatch(editPost(x?.id));
            }

            if (e === "stadistic") {
              router.push(`/admin/estadisticas/votaciones/${x.id}`);
            }
          };

          return (
            <DashBoardComponent
              key={i}
              width={300}
              // height={340}
              image={<ImageCard />}
              title={x.title}
              timeTask={x.timeTask}
              startDate={x.startDate}
              zones={x.zones}
              button={<ButtonBottom />}
              vignette={<Vignette />}
              state={x.state}
              onMenuClick={handleOnMenuClick}
              userPermision={permissions}
              filterMenuBypermision={true}
              menustatuscurso={menuEnCurso}
              menuencreacion={menuEnCreacion}
              menufinalizado={menuFinalizado}
              now={x.now}
            />
          );
        })}
        {cards.length === 0 && (
          <div className="flex items-center text-center justify-center w-full h-150">
            <span className="text-H5">No hubo resultados</span>
          </div>
        )}
      </div>
    </div>
  );
};
