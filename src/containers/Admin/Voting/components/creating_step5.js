import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useSelector, useDispatch } from "react-redux";
import ButtonComponent from "@components/Button";
import {
  creatingVotingProposal,
  openErrorModal,
  changeCreatingStep,
  changevalidatingField,
} from "@redux/admin_voting/actions";
import Styles from "../adminVooting.module.scss";
import draftToHtml from "draftjs-to-html";
import { OptionsProposal } from "./optionProsal";
import moment from "moment";

export const CreatingStep5 = () => {
  const dispatch = useDispatch();
  const { macroZones, localPost, validateField } = useSelector(
    (state) => state.adminVoting
  );

  const [ShowMoreProposalActive, setShowMoreProposalActive] = useState(null);

  const [imageUrl, setImageUrl] = useState("/images/banner.jpg");
  const [zones, setZones] = useState("");

  useEffect(() => {
    try {
      if (localPost?.fileLink) {
        setImageUrl(localPost.fileLink);
      } else {
        setImageUrl(URL.createObjectURL(localPost?.banner?.originFileObj));
      }
    } catch (error) {
      setImageUrl("/images/banner.jpg");
    }
  }, [localPost]);

  useEffect(() => {
    let message = "Disponible para Múltiples Zonas.";

    if (localPost?.macrosId) {
      const selectZones = [];

      macroZones.forEach((macroZone) => {
        const checked = localPost?.macrosId?.includes(macroZone.id) || false;

        if (checked) selectZones.push(macroZone.name);
      });

      if (selectZones.length < 5)
        message = `Disponible para ${selectZones.join(", ")}.`;
    }

    setZones(message);
  }, [localPost]);

  const savePost = (completed) => {
    dispatch(openErrorModal());
    if (!validateField) {
      dispatch(changevalidatingField());
    }
    const errors = completed ? validarCampos() : validarCamposTerminarLuego();
    if (errors.length === 0) {
      const _localPost = { ...localPost };
      if (!_localPost?.startDate) {
        const _startDate = moment().startOf("day").toISOString();
        _localPost.startDate = _startDate;
      }
      if (!_localPost?.dueDate) {
        const _dueDate = moment().endOf("day").toISOString();
        _localPost.dueDate = _dueDate;
      }
      dispatch(creatingVotingProposal({ ..._localPost, completed }));
    } else {
      dispatch(openErrorModal(errors));
    }
  };

  const handleShowMoreProposal = (option) => {
    setShowMoreProposalActive(option);
  };

  const validarCampos = () => {
    const errors = [];
    let returnToPage = null;

    if (!localPost?.dueDate) {
      errors.push("Es necesario añadir una fecha de inicio y culminación");
      returnToPage = 3;
    }

    if (!localPost?.question || localPost?.question?.length < 3) {
      errors.push("Es necesario añadir una pregunta");
      returnToPage = 2;
    }

    if (!localPost?.proposals || localPost?.proposals?.length < 1) {
      errors.push("Es necesario añadir una opción a votar");
      returnToPage = 2;
    }

    localPost?.proposals?.forEach((x) => {
      if (!x?.title || x?.title.length < 2) {
        errors.push("Todas las opciones deberian tener por lo menos un titulo");
        returnToPage = 2;
      }
    });

    try {
      let _textLength = 0;

      if (typeof localPost?.content === "object")
        for (
          let index = 0;
          index < localPost?.content?.blocks?.length;
          index++
        ) {
          const block = localPost?.content?.blocks[index];
          _textLength = _textLength + (block?.text?.length ?? 0);
        }
      else _textLength = localPost?.content?.length ?? 0;

      if (_textLength < 100) {
        errors.push("Es necesario añadir una descripción");
        returnToPage = 1;
      }
    } catch (error) {}

    if (!localPost?.title || localPost?.title?.length < 3) {
      errors.push("Es necesario añadir un título");
      returnToPage = 0;
    }
    if (returnToPage !== null) {
      dispatch(changeCreatingStep(returnToPage));
      if (!validateField) {
        dispatch(changevalidatingField());
      }
    }
    if (returnToPage !== null) {
      dispatch(changeCreatingStep(returnToPage));
      if (!validateField) {
        dispatch(changevalidatingField());
      }
    }

    return errors;
  };

  const validarCamposTerminarLuego = () => {
    const errors = [];
    let returnToPage = null;
    if (!localPost?.title || localPost?.title?.length < 3) {
      errors.push("Es necesario añadir un título");
      returnToPage = 0;
    }
    if (returnToPage !== null) {
      dispatch(changeCreatingStep(returnToPage));
      if (!validateField) {
        dispatch(changevalidatingField());
      }
    }

    return errors;
  };

  return (
    <>
      <div className="px-3">
        <div className="font-bold text-center text-H4 mb-6 mt-3">
          Previsualización de la votación
        </div>
        <div className="font-bold text-H4 text-center mb-4">
          {localPost?.title}
        </div>
        <div className="grid justify-items-center mb-3 ">
          <img
            src={imageUrl}
            className="max-w-500 max-h-500 object-contain"
            alt="alter"
          />
        </div>
        <div
          className="text-bodyMedium pb-3 leading-relaxed mt-40 text-justify mb-16 text-textColor4"
          dangerouslySetInnerHTML={{ __html: draftToHtml(localPost?.content) }}
        />
        {(localPost?.extraFile || localPost?.attached) && (
          <div className="flex p-2">
            <ButtonComponent
              size="super-small"
              type="secondary"
              href={localPost?.extraFile || localPost?.attached}
              download={`${localPost?.title}.pdf`}
              disabled={typeof localPost?.attached !== "string"}
            >
              <div className="flex gap-1 items-center p-2">
                <Image
                  src={
                    typeof localPost?.attached === "string"
                      ? "/icons/attache.svg"
                      : "/icons/fileToUpload.svg"
                  }
                  width={20}
                  height={20}
                  alt="file"
                  layout="fixed"
                />
                {typeof localPost?.attached === "string" ? (
                  <span>Ver Adjunto</span>
                ) : (
                  <span>
                    Cuando el post haya sido creada se podra descargar el
                    archivo
                  </span>
                )}
              </div>
            </ButtonComponent>
          </div>
        )}
        <div className="text-bodyMedium text-center mb-5">{zones}</div>
        <div className="font-bold text-center text-H6 mb-5 text-greenText">
          ¿ {localPost?.question} ?
        </div>
        <div className={Styles.voteProposalContainer}>
          {localPost?.proposals?.map((x, i) => {
            return (
              <OptionsProposal
                column={localPost?.proposals.length}
                key={i}
                option={x}
                index={i}
                handleShowMoreProposal={handleShowMoreProposal}
              />
            );
          })}
        </div>
        <div className="flex m-5 justify-around">
          {!localPost?.completed && (
            <ButtonComponent
              size="super-small"
              type="link"
              onClick={() => savePost(false)}
            >
              Terminar más tarde
            </ButtonComponent>
          )}
          <ButtonComponent
            size="super-small"
            type="primary"
            onClick={() => savePost(true)}
            style={{ padding: "5px 70px" }}
          >
            Publicar
          </ButtonComponent>
        </div>
      </div>
      {/* Modal Proposal Active */}
      {ShowMoreProposalActive !== null && (
        <>
          <div
            className={Styles.modalProposalBackground}
            onClick={() => handleShowMoreProposal(null)}
          />
          <div className={Styles.modalProposalViewMoreDetail}>
            <div className="font-bold mb-3 text-center text-H6 bg-greyheader p-1 sticky top-0">
              {ShowMoreProposalActive?.title}
            </div>
            {ShowMoreProposalActive?.image !== "" && (
              <img
                src={ShowMoreProposalActive?.image}
                className="object-scale-down h-3/6 "
                alt="alter"
              />
            )}
            {ShowMoreProposalActive?.content?.blocks ? (
              <div
                className="pb-3 text-justify p-3 text-xl"
                dangerouslySetInnerHTML={{
                  __html: draftToHtml(ShowMoreProposalActive?.content),
                }}
              />
            ) : (
              <div className="pb-3 text-justify p-3 text-xl">
                {ShowMoreProposalActive?.content}
              </div>
            )}
            <div
              className={`flex ${
                ShowMoreProposalActive?.attached || ShowMoreProposalActive?.file
                  ? "justify-between"
                  : "justify-end"
              } `}
            >
              {(ShowMoreProposalActive?.attached ||
                ShowMoreProposalActive?.file) && (
                <ButtonComponent
                  size="super-small"
                  type="link"
                  href={
                    ShowMoreProposalActive?.attached ||
                    ShowMoreProposalActive?.file
                  }
                  download={`${ShowMoreProposalActive?.title}.pdf`}
                >
                  <div className="flex gap-1">
                    <Image
                      src={
                        typeof ShowMoreProposalActive?.attached === "string"
                          ? "/icons/attache.svg"
                          : "/icons/fileToUpload.svg"
                      }
                      width={20}
                      height={20}
                      alt="file"
                      layout="fixed"
                    />
                    <span>Ver Adjunto</span>
                  </div>
                </ButtonComponent>
              )}

              <ButtonComponent
                size="super-small"
                type="secondary"
                onClick={() => handleShowMoreProposal(null)}
                className="mx-3 w-2/6"
              >
                <span className="text-headline">Cerrar</span>
              </ButtonComponent>
            </div>
            <div className="h-6" />
          </div>
        </>
      )}
    </>
  );
};
