import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Pagination from "@components/Pagination";

import { changeDataLocalFilter } from "@redux/admin_voting/actions";

export const PaginationCustom = () => {
  const { filters, total } = useSelector((state) => state.adminVoting);

  const dispatch = useDispatch();

  const handleSetPage = (e) => {
    dispatch(changeDataLocalFilter({ name: "page", value: e }));
  };

  return (
    <div className="flex py-2 justify-center bg-white">
      <Pagination
        defaultPageSize={9}
        current={filters?.page || 1}
        setPage={handleSetPage}
        total={total}
        onChange={handleSetPage}
      />
    </div>
  );
};
