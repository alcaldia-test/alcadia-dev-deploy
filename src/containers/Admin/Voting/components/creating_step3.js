import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useSelector, useDispatch } from "react-redux";
// import InputField from "@components/InputField";
import ButtonComponent from "@components/Button";
import WYSIWYGCOMPONENET from "@components/WYSIWYG";
import UploadComponent from "@components/Upload";
import DeleteModal from "@components/deleteModal";

import CustomInput from "../CustomInput";

import {
  changeDataLocalPost,
  changeCreatingStep,
  deleteFile,
  deleteproposalinServer,
  // addproposalinServer,
} from "@redux/admin_voting/actions";

export const CreatingStep3 = () => {
  const [optionOpen, setOptionOpen] = useState(0);

  const { localPost, validateField } = useSelector(
    (state) => state.adminVoting
  );

  const dispatch = useDispatch();

  const handleChange = (e) => {
    dispatch(
      changeDataLocalPost({
        name: e.target.name,
        value: e.target.value?.replace("¿", "").replace("?", ""),
      })
    );
  };

  const handleOptionClick = (e) => {
    if (e === optionOpen) {
      setOptionOpen(-1);
    } else {
      setOptionOpen(e);
    }
  };

  const handleOnClick = () => {
    dispatch(changeCreatingStep(3));
  };

  const handleOnAddOption = () => {
    let newIndex = 0;
    const lastIndex = localPost?.proposals?.at(-1)?.id || 0;
    if (typeof lastIndex === "number") {
      newIndex = lastIndex + 1;
    } else {
      newIndex = 1;
    }
    const actualArray = localPost?.proposals || [];
    const emptyProposal = {
      id: newIndex,
      title: "",
      content: "",
      image: null,
      file: null,
    };
    dispatch(
      changeDataLocalPost({
        name: "proposals",
        value: [...actualArray, emptyProposal],
      })
    );
  };

  return (
    <div className="flex flex-col gap-3 p-2">
      <span className="font-bold text-H6">Añade las opciones a votar</span>
      <span className="text-bodyMedium">
        Describa y agregue imágenes o archivos que complementen las opciones a
        votar para los ciudadanos
      </span>

      <span>Esta es la pregunta que apoya la encuesta de la votación</span>
      {/* <InputField onChange={handleChange} /> */}
      <CustomInput
        onChange={handleChange}
        value={localPost?.question || ""}
        name="question"
        includeSimboleStart="¿"
        includeSimboleEnd="?"
        placeholder="Pregunta de la Votación"
        validateField={validateField}
        messageOfError="Es necesario añadir una pregunta"
        minLength={3}
      />
      {localPost?.proposals?.map((x, i) => {
        return (
          <OpcionElement
            key={i}
            openPosition={optionOpen}
            onPositioClickOpen={handleOptionClick}
            position={i}
            element={x}
          />
        );
      })}

      <div className=" h-5 w-2/5">
        <ButtonComponent
          size="small-x"
          type="secondary"
          onClick={handleOnAddOption}
        >
          Añadir opción +
        </ButtonComponent>
      </div>

      {validateField && !localPost?.proposals?.length > 0 && (
        <span className="text-red text-callout">
          Es necesario añadir una opción a votar
        </span>
      )}

      <div className="flex items-center justify-center">
        <ButtonComponent size="small-x" onClick={handleOnClick}>
          <span className="text-bodyMedium">Siguiente</span>
        </ButtonComponent>
      </div>
    </div>
  );
};

const OpcionElement = ({
  element,
  openPosition,
  position = 0,
  onPositioClickOpen = () => {},
}) => {
  const { localPost, validateField } = useSelector(
    (state) => state.adminVoting
  );

  const dispatch = useDispatch();

  const [state, setstate] = useState(element);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  useEffect(() => {
    if (state?.id !== element?.id) {
      setstate(element);
    }
  }, [element]);

  useEffect(() => {
    try {
      const actualArray = localPost?.proposals;
      const indexToChange = localPost?.proposals?.findIndex(
        (x) => x.id === element.id
      );
      actualArray[indexToChange] = state;

      dispatch(
        changeDataLocalPost({
          name: "proposals",
          value: [...actualArray],
        })
      );
    } catch (error) {}
  }, [state]);

  const handleChange = (e) => {
    setstate({ ...state, [e.target.name]: e.target.value });
  };

  const handleChangeWYSY = (e) => {
    setstate({ ...state, content: e });
  };

  const handleChangeImage = (info) => {
    if (info.file.status === "uploading") {
      const _file = info?.file;
      _file.status = "done";
      setstate({ ...state, image: _file });
    }
    if (info.file.status === "removed") {
      setstate({ ...state, image: undefined });
    }
  };

  const handleChangeAttached = (info) => {
    if (info.file.status === "uploading") {
      const _file = info?.file;
      _file.status = "done";
      setstate({ ...state, file: _file });
    }
    if (info.file.status === "removed") {
      setstate({ ...state, file: undefined });
    }
  };

  const handleOnRemoveOption = () => {
    const actualArray =
      localPost?.proposals?.filter((x) => x.id !== element.id) || [];

    dispatch(
      changeDataLocalPost({
        name: "proposals",
        value: [...actualArray],
      })
    );
  };

  const handleRemoveOptionServer = () => {
    const remaininProposal =
      localPost?.proposals?.filter((x) => x.id !== element.id) || [];
    setShowDeleteModal(false);
    dispatch(deleteproposalinServer({ id: element?.id, remaininProposal }));
  };

  const handleOnDeleteFile = (tag) => {
    const field = tag === "banner" ? "fileLink" : "attached";
    const proposals = [...localPost?.proposals];
    proposals[position][field] = "";
    const payload = {
      tag,
      id: element?.id,
      PostionFieldName: `proposals`,
      value: proposals,
      isProposal: true,
    };
    dispatch(deleteFile(payload));
  };

  // const handleSaveProposalChangesInServer = () => {
  //   const payload = {
  //     vootingId: localPost?.id,
  //     proposalRaw: element,
  //     id: element.id.length > 3 ? element.id : undefined,
  //   };
  //   dispatch(addproposalinServer(payload));
  // };

  return (
    <>
      <div>
        <div className="flex justify-between">
          <span
            className="flex align-bottom w-1/6 cursor-pointer"
            onClick={() => onPositioClickOpen(position)}
          >
            <Image
              src={
                openPosition === position
                  ? "/icons/arrow-down.svg"
                  : "/icons/arrow-chevron-right.svg"
              }
              layout="fixed"
              width={20}
              height={20}
              alt="xboxIcon"
            />
            <span className="text-bodyMedium font-bold">
              Opcion {position + 1}
            </span>
          </span>
          <span
            className="color-red text-red cursor-pointer"
            onClick={
              localPost?.id && typeof element?.id === "string"
                ? () => setShowDeleteModal(true)
                : handleOnRemoveOption
            }
          >
            Eliminar
          </span>
        </div>
        {openPosition === position ? (
          <div className="m-2 flex flex-col bg-greLight rounded-sm p-3">
            <span className="text-bodyMedium font-bold mb-2">
              Este es el título para la opción a votar
            </span>
            <CustomInput
              whiteBackGround={true}
              onChange={handleChange}
              name="title"
              value={element?.title}
              validateField={validateField}
              messageOfError="Todas las opciones deberian tener por lo menos un titulo"
              minLength={2}
            />
            <span className="text-bodyMedium mt-4 mb-1">
              Sube una Imagen que represente la opción a votar{" "}
            </span>
            <UploadComponent
              accept="image/*"
              onChange={handleChangeImage}
              sizeButton="small-x"
              textButton="Subir Imagen"
              fileList={element?.image ? [element?.image] : []}
              filesInServer={element?.fileLink ? [element?.fileLink] : []}
              allowAddMoreToServer={false}
              onDeleteFunction={handleOnDeleteFile}
              deleteTag="banner"
            />
            <span className="h-60" />
            <span className="text-bodyMedium mt-2 mb-1">
              Puede dar una breve descripción de esta opción{" "}
            </span>
            <WYSIWYGCOMPONENET
              onChange={handleChangeWYSY}
              value={element?.content || {}}
            />
            <span className="h-60" />
            <UploadComponent
              onChange={handleChangeAttached}
              sizeButton="small-x"
              fileList={element?.file ? [element?.file] : []}
              filesInServer={element?.attached ? [element?.attached] : []}
              allowAddMoreToServer={false}
              onDeleteFunction={handleOnDeleteFile}
              deleteTag="attached"
              accept=".pdf"
            />
            {/* {localPost?.id && (
              <div className="flex items-center justify-center">
                <ButtonComponent
                  size="small-x"
                  onClick={handleSaveProposalChangesInServer}
                >
                  <span className="text-bodyMedium">Guardar Cambios</span>
                </ButtonComponent>
              </div>
            )} */}
          </div>
        ) : (
          <div className="h-1 border-t-2 border-greyheader mx-4" />
        )}
      </div>
      {showDeleteModal && (
        <DeleteModal
          closeModal={() => setShowDeleteModal(false)}
          title="¿Seguro que deseas eliminar esta opción?"
          delteFuntion={() => handleRemoveOptionServer()}
          simple={true}
        />
      )}
    </>
  );
};
