export const styles = {
  textMain: "font-bold text-lg",
  textMainCenter: "font-bold text-lg text-center",
  textSecond: "font-bold text-base",
};
