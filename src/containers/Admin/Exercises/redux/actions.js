import {
  DELETE_EXERCISES_START,
  DELETE_EXERCISES_SUCCESS,
  REQUEST_EXERCISES_START,
  REQUEST_EXERCISES_SUCCESS,
  LOADING_EXERCISES_HIDE,
  LOADING_EXERCISES_SHOW,
  REGISTER_EXERCISES_START,
  REGISTER_EXERCISES_SUCCESS,
  UPDATE_EXERCISES_START,
  UPDATE_EXERCISES_SUCCESS,
} from "./constants";

export const requestExercisesStart = (payload = {}) => ({
  type: REQUEST_EXERCISES_START,
  payload,
});
export const requestExercisesSuccess = (payload = {}) => ({
  type: REQUEST_EXERCISES_SUCCESS,
  payload,
});

export const registerExercisesStart = (payload = {}) => ({
  type: REGISTER_EXERCISES_START,
  payload,
});
export const registerExercisesSuccess = (payload = {}) => ({
  type: REGISTER_EXERCISES_SUCCESS,
  payload,
});

export const deleteExercisesStart = (payload = {}) => ({
  type: DELETE_EXERCISES_START,
  payload,
});
export const deleteExercisesSuccess = (payload = {}) => ({
  type: DELETE_EXERCISES_SUCCESS,
  payload,
});

export const updateExercisesStart = (payload = {}) => ({
  type: UPDATE_EXERCISES_START,
  payload,
});
export const updateExercisesSuccess = (payload = {}) => ({
  type: UPDATE_EXERCISES_SUCCESS,
  payload,
});

export const loadingExercisesShow = (payload = {}) => ({
  type: LOADING_EXERCISES_SHOW,
  payload,
});
export const loadingExercisesHide = (payload = {}) => ({
  type: LOADING_EXERCISES_HIDE,
  payload,
});
