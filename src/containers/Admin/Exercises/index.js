import React, { useEffect } from "react";

export function IndexPage() {
  useEffect(() => {}, []);

  return (
    <div>
      <div>
        <div>
          <p className="gx-text-grey">Detalles del Contenedor</p>
          <h2 className="gx-text-uppercase gx-text-black gx-font-weight-bold gx-fnd-title">
            REGISTRO DE EJERCICIOS
          </h2>
          <p>
            Este contenedor tiene como funcionalidad el registro de los Ideas
            del Landing Page
          </p>
        </div>
      </div>
      <hr />
      <div>
        <div span={24}>
          <h1 className="gx-text-grey">Historial de Registros</h1>
        </div>
      </div>
    </div>
  );
}

IndexPage.propTypes = {};

export default IndexPage;
