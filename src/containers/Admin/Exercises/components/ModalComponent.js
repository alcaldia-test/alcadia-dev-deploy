import React, { useEffect } from "react";
import { Input, Spin, Form, message, Select } from "antd";
import Modal from "antd/lib/modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "antd/lib/form/Form";
import { registerExercisesStart, updateExercisesStart } from "../redux/actions";
import TextArea from "antd/lib/input/TextArea";
import SearchFilesStorage from "@components/SearchFilesStorage";
import { customUseReducer } from "@utils/customHooks";
import { SelectComponent } from "@components/SelectComponent";

const nameProduct = "Ejercicios";

const initialState = {
  image: {},
  video: {},
  poster: {},
  loading: false,
};

function ModalComponent({ visible, handleClosed, selected }) {
  const [form] = useForm();
  const dispatch = useDispatch();
  const typesExercise = useSelector(({ global }) => global.typesExercise);
  const workTeam = useSelector(({ workTeam }) => workTeam);
  const isRegister = Object.keys(selected).length === 0;
  const [state, dispatchComponent] = customUseReducer(initialState);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      ...selected,
      ...(!isRegister && {
        // Si es modificación, aquí se agregarán los datos extras
        // priceUp: selected?.inputPrice[0],
        // priceDown: selected?.inputPrice[1],
        type: selected.type?.id,
        equipment: selected.equipment?.id,
      }),
    });
  }, [visible]);

  const onFinish = (values) => {
    dispatchComponent({ loading: true });
    return new Promise((resolve, reject) => {
      if (isRegister) {
        dispatch(
          registerExercisesStart({
            ...values,
            type: typesExercise.find((value) => value.id === values.type),
            equipment: workTeam.data.find(
              (value) => value.id === values.equipment
            ),
            resolve,
            reject,
          })
        );
      } else {
        dispatch(
          updateExercisesStart({
            ...values,
            id: selected.id,
            type: typesExercise.find((value) => value.id === values.type),
            equipment: workTeam.data.find(
              (value) => value.id === values.equipment
            ),
            resolve,
            reject,
          })
        );
      }
    })
      .then((res) => {
        message.success(res);
        handleClosed();
      })
      .catch((err) => {
        message.error(err);
        dispatchComponent({ loading: false });
      });
  };

  return (
    <>
      <Modal
        title={isRegister ? `Nuevo ${nameProduct}` : `Modificar ${nameProduct}`}
        centered
        visible={visible}
        onCancel={handleClosed}
        okText={isRegister ? "Publicar" : "Modificar"}
        cancelText="Cerrar"
        onOk={() => form.submit()}
        confirmLoading={state.loading}
      >
        <Spin
          spinning={state.loading}
          tip={`${
            isRegister ? "Registrando" : "Modificando"
          } los datos en backend`}
          size="large"
        >
          <Form
            onFinish={onFinish}
            form={form}
            name="basic"
            className="ant-advanced-search-form"
            layout="vertical"
          >
            <Form.Item
              label="Titulo"
              rules={[{ required: true, message: "Ingresa un título" }]}
              name="title"
            >
              <Input placeholder="Título" />
            </Form.Item>
            <Form.Item
              label="Descripción"
              rules={[
                { required: true, message: "Ingrese una breve descripción" },
              ]}
              extra="Ingrese una breve descripción de lo que hará el ejercicio"
              name="description"
            >
              <TextArea placeholder="Ingrese una breve descripción" rows={4} />
            </Form.Item>
            <Form.Item
              label="Tipo de Ejercicio"
              rules={[
                { required: true, message: "Seleccione el tipo de Ejercicio" },
              ]}
              name="type"
            >
              <SelectComponent.TypesExercise />
            </Form.Item>
            <Form.Item
              label="Equipo de trabajo"
              rules={[
                {
                  required: true,
                  message: "Ingrese con que equipo se trabajará",
                },
              ]}
              name="equipment"
            >
              <Select placeholder="Por favor, seleccione un equipo de trabajo">
                {workTeam.data.map((value, key) => (
                  <Select.Option key={key} value={value.id}>
                    {value.title}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              label="Imagen del Ejercicio"
              rules={[{ required: true, message: "Seleccione una imagen" }]}
              name="image"
            >
              <SearchFilesStorage
                type="image"
                value={state.image}
                onChange={(image) => dispatchComponent({ image })}
              />
            </Form.Item>
            <Form.Item
              label="Video del Ejercicio"
              rules={[{ required: true, message: "Seleccione un video" }]}
              name="video"
            >
              <SearchFilesStorage
                type="video"
                textButtom="SELECCIONAR VIDEO"
                value={state.video}
                onChange={(video) => dispatchComponent({ video })}
              />
            </Form.Item>
            <Form.Item
              label="Poster del vídeo / Preview del video"
              rules={[{ required: true, message: "Seleccione una imagen" }]}
              name="poster"
            >
              <SearchFilesStorage
                type="image"
                textButtom="SELECCIONAR IMAGEN"
                value={state.poster}
                onChange={(poster) => dispatchComponent({ poster })}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
}

export default ModalComponent;
