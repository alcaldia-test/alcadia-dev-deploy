import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Table, Dropdown, Menu, Input, Button, Divider, Row, Col } from "antd";
import { deleteExercisesStart, requestExercisesStart } from "../redux/actions";
import {
  RetweetOutlined,
  FunnelPlotOutlined,
  PlusCircleOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { customUseReducer } from "@utils/customHooks";
import { FilterComponent } from "./FilterComponent";

const getColumn = ({ handleEdit, handleDelete }) => {
  return [
    {
      width: 90,
      title: "Fecha",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (value) => moment(value).format("YYYY/MM/DD"),
    },
    {
      width: "20%",
      title: "Titulo",
      dataIndex: "title",
      key: "title",
    },
    {
      width: "40%",
      ellipsis: true,
      title: "Descripción",
      dataIndex: "description",
      key: "description",
    },
    {
      width: "20%",
      title: "Tipo",
      dataIndex: "type",
      key: "type",
      render: (values) => values.title,
    },
    {
      width: 60,
      title: "",
      // eslint-disable-next-line react/display-name
      render: (_, record) => (
        <div style={{ marginTop: -5, marginBottom: -5 }}>
          <Dropdown
            overlay={
              <Menu>
                <Menu.Item onClick={() => handleEdit(record)}>Editar</Menu.Item>
                <Menu.Item onClick={() => handleDelete(record)}>
                  Eliminar
                </Menu.Item>
              </Menu>
            }
            placement="bottomRight"
            trigger={["click"]}
            size="small"
          >
            <i className="gx-icon-btn icon icon-ellipse-v" />
          </Dropdown>
        </div>
      ),
    },
  ];
};

const initialState = {
  modalFilter: false,
  search: "",
  filter: {
    typeExerciseId: undefined,
  },
};

export const TableComponent = ({ openModal }) => {
  const [state, dispatchComponent] = customUseReducer(initialState);
  const dispatch = useDispatch();
  const exercises = useSelector(({ exercises }) => exercises);

  const handleEdit = (values) => {
    openModal({ modal: true, selected: values });
  };

  const handleDelete = (values) => {
    dispatch(deleteExercisesStart(values));
  };

  const columns = getColumn({ handleDelete, handleEdit });

  const initialRequest = async () => {
    dispatch(requestExercisesStart());
  };

  const renderFilter = () => {
    return (
      <Row gutter={24} style={{ flexDirection: "row" }}>
        <Col
          xl={16}
          lg={16}
          md={16}
          sm={24}
          xs={24}
          className="row-col-default"
        >
          <Input.Search
            placeholder="Buscar por nombre de ejercicio"
            allowClear
            enterButton="Buscar"
            onSearch={(search) => dispatchComponent({ search })}
          />
        </Col>
        <Col xl={4} lg={4} md={4} sm={24} xs={24} className="row-col-default">
          <Button
            block
            icon={<FunnelPlotOutlined />}
            onClick={() => dispatchComponent({ modalFilter: true })}
          >
            FILTRAR
          </Button>
        </Col>
        <Col xl={2} lg={2} md={2} sm={24} xs={24} className="row-col-default">
          <Button
            block
            type="dashed"
            icon={<RetweetOutlined />}
            title="Actualizar"
            onClick={initialRequest}
          />
        </Col>
        <Col xl={2} lg={2} md={2} sm={24} xs={24} className="row-col-default">
          <Button
            block
            type="dashed"
            icon={<PlusCircleOutlined />}
            title="Nuevo"
            onClick={() => handleEdit({})}
          />
        </Col>
      </Row>
    );
  };

  return (
    <>
      {renderFilter()}
      <Divider dashed />
      <Table
        size="small"
        loading={exercises.loading}
        className="gx-table-responsive"
        columns={columns}
        dataSource={exercises?.data
          .filter((exercise) =>
            state.filter.typeExerciseId === "" || !state.filter.typeExerciseId
              ? true
              : exercise.type.id === state.filter.typeExerciseId
          )
          .filter((exercise) =>
            state.search === "" ? true : exercise.title.includes(state.search)
          )
          .map((video, key) => ({ key, ...video }))}
      />
      <FilterComponent
        visible={state.modalFilter}
        initialState={initialState.filter}
        handleFilter={(filter) =>
          dispatchComponent({ filter, modalFilter: false })
        }
        filter={state.filter}
        handleClosed={() => dispatchComponent({ modalFilter: false })}
      />
    </>
  );
};
