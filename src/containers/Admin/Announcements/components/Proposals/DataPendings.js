import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import Table from "@components/Table";
import styles from "../Announcements.module.scss";
import moment from "moment";

import SelectWraper from "@components/Select";
import Autocomplete from "@components/Autocomplete";
import Button from "@components/Button";

import {
  getProposalsAdmin,
  postProposalAction,
} from "@redux/announcements/actions";

import SimpleModal from "@components/Modals/SimpleModal";
import message from "@components/Message";

const optionsAutocomplete = [];

export default function DataPendings(props) {
  const rows = [];
  const [filter, setFilter] = useState(undefined);
  const [filterAutocomplete, setFilterAutocomplete] = useState(undefined);
  const [currentPage /*, setCurrentPage */] = useState(1);
  const [updateData, setUpdateData] = useState(false);

  const router = useRouter();
  const { announcementId } = router.query;

  const dispatch = useDispatch();
  const { adminProposalsReceived, waiting, announcement } = useSelector(
    (state) => state.announcements
  );

  const [showModalApproved, setShowModalApproved] = useState(false);
  const [onQuit, setOnQuit] = useState(false);
  const [idSelected, setIdSelected] = useState(false);
  const today = moment();

  const onHandleQuit = () => {
    setShowModalApproved(false);
    setOnQuit(true);
    try {
      dispatch(
        postProposalAction({
          id: idSelected?.id,
          action: "WAITING",
          message: "",
          userId: idSelected?.user?.id,
        })
      );
    } catch (error) {
      message.error("Problemas al quitar aprobación.");
    }
  };

  useEffect(() => {
    if (!waiting && onQuit) {
      setUpdateData(!updateData);
    }
  }, [waiting, onQuit]);

  const _initSetData = (data) => {
    data?.map((d) => {
      const _data = {
        key: d.id,
        user: d.user,
        propuesta:
          d?.title?.split(" ").length === 1
            ? d?.title?.substr(0, 50) + "..."
            : d?.title,
        id: d.id,
        updatedAt: d.updatedAt,
        state: d.state,
      };
      rows.push(_data);
      return null;
    });
  };

  _initSetData(adminProposalsReceived);

  useEffect(() => {
    const filterObj = {
      limit: 10,
      skip: (currentPage - 1) * 10,
      like: filterAutocomplete,
      id: announcementId,
    };

    if (props && props?.status !== "") {
      filterObj.state = props?.status;
    }

    if (filter !== "") {
      filterObj.order = filter;
    }

    try {
      const fieldState = props?.fieldState;
      if (props?.dateStart || props?.dateEnd) {
        const _dateStart = moment(props?.dateStart?.split("T")[0]).startOf(
          "day"
        );
        const _dateEnd = moment(props?.dateEnd?.split("T")[0]).endOf("day");
        if (today >= _dateStart && today < _dateEnd) {
          dispatch(getProposalsAdmin(filterObj, fieldState));
        }
      } else {
        dispatch(getProposalsAdmin(filterObj, fieldState));
      }
    } catch (error) {
      message.error("Problemas al obtener propuestas.");
    }
  }, [filter, filterAutocomplete, currentPage, updateData]);

  const optionsSelect2 = [
    { value: "", label: "Todas" },
    { value: "DESC", label: "Más Recientes" },
    { value: "ASC", label: "Más Antiguas" },
  ];

  const update = () => {
    setUpdateData(!updateData);
  };

  const columns = [
    {
      title: "Usuario",
      dataIndex: "user",
      render: (user) => {
        return (
          <div className={styles.userdata}>
            <img src={user?.avatar} alt="avatar" />
            <span>{user?.firstName}</span>
          </div>
        );
      },
    },
    {
      title: "Propuesta Ciudadana",
      dataIndex: "propuesta",
      sorter: (a, b) => a.propuesta - b.propuesta,
      render: (propuesta, data) => {
        return (
          <div>
            <p
              className="font-bold mb-0 cursor-pointer"
              onClick={() => {
                router.push(
                  "/admin/convocatorias/" +
                    announcementId +
                    "/propuesta/" +
                    data?.id
                );
              }}
            >
              {propuesta}
            </p>
            <span className={styles.userdate}>
              {data?.updatedAt
                ? moment(data?.updatedAt).format("DD/MM/YYYY hh:mm")
                : ""}
              {data?.state === "APPROVED" && (
                <span className={styles.tag__approvated}>APROBADA</span>
              )}
            </span>
          </div>
        );
      },
    },
    {
      title: "Acciones",
      dataIndex: "id",
      render: (value, data) => {
        return (
          <span className="actionButton">
            {data.state === "APPROVED" ? (
              <a
                href={"#"}
                key={value}
                onClick={(e) => {
                  e.stopPropagation();
                  setIdSelected(data);
                  setShowModalApproved(true);
                }}
              >
                Quitar aprobación
              </a>
            ) : (announcement?.directPosts === false &&
                today >=
                  moment(announcement?.reviewStart?.split("T")[0]).startOf(
                    "day"
                  ) &&
                today <
                  moment(announcement?.supportingStart?.split("T")[0]).startOf(
                    "day"
                  )) ||
              (announcement?.directPosts === true &&
                today >=
                  moment(announcement?.receptionStart?.split("T")[0]).startOf(
                    "day"
                  ) &&
                today <
                  moment(announcement?.postResults?.split("T")[0]).startOf(
                    "day"
                  )) ? (
              <a
                onClick={(e) => {
                  e.stopPropagation();

                  router.push(
                    "/admin/convocatorias/" +
                      announcementId +
                      "/propuesta/" +
                      value
                  );
                }}
                href={"#"}
              >
                Evaluar propuesta
              </a>
            ) : (
              <a className={styles.not__evaluate} href={"#"}>
                {" "}
                Evaluar propuesta{" "}
              </a>
            )}
          </span>
        );
      },
    },
  ];

  return (
    <>
      <SimpleModal
        visible={showModalApproved}
        onCancel={() => setShowModalApproved(false)}
      >
        <>
          <div className={styles.title__modal__quit}>
            ¿Desea quitar la aprobación de esta propuesta?
          </div>
          <div className="flex justify-center gap-2 mt-20">
            <Button
              className={""}
              type="secondary"
              size="small-x"
              onClick={() => onHandleQuit()}
            >
              Si
            </Button>
            <Button
              className={""}
              type="secondary"
              size="small-x"
              onClick={() => setShowModalApproved(false)}
            >
              No
            </Button>
          </div>
        </>
      </SimpleModal>

      <div className="mb-40 pb-40">
        <table className={styles.filter_table}>
          <td>
            <SelectWraper
              onChange={(e) => {
                setFilter(e[0].value);
              }}
              placeholder="Todas"
              options={optionsSelect2}
            ></SelectWraper>
          </td>
          <td>
            <Autocomplete
              dataSource={optionsAutocomplete}
              placeholder="Buscar..."
              onChange={(e) => {
                setFilterAutocomplete(e);
              }}
            />
          </td>
        </table>
        <div className={styles.btn_actualizar}>
          <Button
            className={styles.filter_button}
            type="primary"
            size="small"
            onClick={update}
          >
            {" "}
            Actualizar{" "}
          </Button>
        </div>
      </div>
      {rows?.length > 0 && <Table data={rows} columns={columns} />}
      {rows?.length === 0 && <span className="pl-20">No data.</span>}
    </>
  );
}
