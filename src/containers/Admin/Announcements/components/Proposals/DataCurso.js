import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import Table from "@components/Table";
import styles from "../Announcements.module.scss";
import moment from "moment";

import SelectWraper from "@components/Select";
import Autocomplete from "@components/Autocomplete";
import Button from "@components/Button";
import TextAreaModal from "@components/Modals/TextAreaModal";
import { UnknowDoc, Comment, Like, DisLike } from "@assets/icons";
import {
  getProposalsAdmin,
  postProposalAction,
} from "@redux/announcements/actions";
import message from "@components/Message";

const optionsAutocomplete = [];

export default function DataCurso(props) {
  const rows = [];
  const [filter, setFilter] = useState(undefined);
  const [filterAutocomplete, setFilterAutocomplete] = useState(undefined);
  const [currentPage /*, setCurrentPage */] = useState(1);
  const [updateData, setUpdateData] = useState(false);
  const [deleted, setDeleted] = useState(false);

  const router = useRouter();
  const { announcementId } = router.query;

  const dispatch = useDispatch();
  const { adminProposalsPublished, waiting } = useSelector(
    (state) => state.announcements
  );

  const [showModalApproved, setShowModalApproved] = useState(false);
  const [idSelected, setIdSelected] = useState(false);

  const setActionRejected = (text) => {
    setShowModalApproved(false);
    setDeleted(true);
    try {
      dispatch(
        postProposalAction({
          id: idSelected?.id,
          action: "REJECTED",
          message: text,
          userId: idSelected?.user?.id,
        })
      );
    } catch (error) {
      message.error("Problemas al rechazar propuesta.");
    }
  };

  useEffect(() => {
    if (!waiting && deleted) {
      setUpdateData(!updateData);
    }
  }, [waiting, deleted]);

  const _initSetData = (data) => {
    data?.map((d) => {
      const _data = {
        key: d.id,
        user: d.user,
        propuesta:
          d?.title?.split(" ").length === 1
            ? d?.title?.substr(0, 50) + "..."
            : d?.title,
        id: d.id,
        updatedAt: d.updatedAt,
        state: d.state,
        dislikes: d.dislikes ? d.dislikes : 0,
        likes: d.likes ? d.likes : 0,
        comments: d.comments ? d.comments : 0,
        complaints: d.complaints ? d.complaints : 0,
      };
      rows.push(_data);
      return null;
    });
  };

  _initSetData(adminProposalsPublished);

  useEffect(() => {
    const filterObj = {
      limit: 10,
      skip: (currentPage - 1) * 10,
      like: filterAutocomplete,
      id: announcementId,
    };

    if (props && props?.status !== "") {
      filterObj.state = props?.status;
    }

    if (filter !== "") {
      filterObj.order = filter;
    }

    try {
      const fieldState = props?.fieldState;
      if (props?.dateStart || props?.dateEnd) {
        const today = moment();
        const _dateStart = moment(props?.dateStart?.split("T")[0]).startOf(
          "day"
        );
        const _dateEnd = moment(props?.dateEnd?.split("T")[0]).endOf("day");
        if (today >= _dateStart && today < _dateEnd) {
          dispatch(getProposalsAdmin(filterObj, fieldState));
        }
      } else {
        dispatch(getProposalsAdmin(filterObj, fieldState));
      }
    } catch (error) {
      message.error("Problemas al obtener propuestas.");
    }
  }, [filter, filterAutocomplete, currentPage, updateData]);

  const optionsSelect2 = [
    { value: "", label: "Todas" },
    { value: "DESC", label: "Más Recientes" },
    { value: "ASC", label: "Más Antiguas" },
  ];

  const update = () => {
    setUpdateData(!updateData);
  };

  const columns = [
    {
      title: "Usuario",
      dataIndex: "user",
      render: (user) => {
        return (
          <div className={styles.userdata}>
            <img src={user?.avatar} alt="avatar" />
            <span>{user?.firstName}</span>
          </div>
        );
      },
    },
    {
      title: "Propuesta Ciudadana",
      dataIndex: "propuesta",
      sorter: (a, b) => a.propuesta - b.propuesta,
      render: (propuesta, data) => {
        return (
          <div>
            <p
              className="font-bold mb-0 cursor-pointer"
              onClick={() => {
                router.push(
                  "/admin/convocatorias/" +
                    announcementId +
                    "/propuesta/" +
                    data?.id
                );
              }}
            >
              {propuesta}
            </p>
            <span className={styles.userdate}>
              {" "}
              {data?.updatedAt
                ? moment(data?.updatedAt).format("DD/MM/YYYY hh:mm")
                : ""}
            </span>
          </div>
        );
      },
      responsive: ["md"],
    },
    {
      title: "Apoyos",
      dataIndex: "likes",
      sorter: (a, b) => a.likes - b.likes,
      render: (likes, data) => {
        return (
          <div className="flex">
            <div className={styles.td_complaints + " mr-20"}>
              <div className={styles.likes}>
                <Like size="24" />
              </div>
              <span>{likes}</span>
            </div>
            <div className={styles.td_complaints}>
              <div className={styles.dislikes}>
                <DisLike size="24" />
              </div>
              <span>{data.dislikes}</span>
            </div>
          </div>
        );
      },
    },
    {
      title: "Comentarios",
      dataIndex: "comments",
      sorter: (a, b) => a.comments - b.comments,
      render: (comments) => {
        return (
          <div className={styles.td_complaints}>
            <div className={styles.comments}>
              <Comment size="15" />
            </div>
            <span>{comments}</span>
          </div>
        );
      },
    },
    {
      title: "Denuncias",
      dataIndex: "complaints",
      sorter: (a, b) => a.complaints - b.complaints,
      render: (complaints) => {
        return (
          <div className={styles.td_complaints}>
            <UnknowDoc size="15" color="#929292" />
            <span>{complaints}</span>
          </div>
        );
      },
    },
    {
      title: "Acciones",
      dataIndex: "id",
      render: (value, data) => {
        return (
          <span className="actionButton">
            <a
              href={"#"}
              key={value}
              onClick={(e) => {
                e.stopPropagation();
                console.log(data);
                setIdSelected(data);
                setShowModalApproved(true);
              }}
            >
              Rechazar propuesta
            </a>
          </span>
        );
      },
    },
  ];

  return (
    <>
      <TextAreaModal
        closable={true}
        cancelText={"Cerrar"}
        id={"1"}
        okText={"Rechazar y enviar mensaje"}
        placeholder={"Contenido"}
        text={"Motivo"}
        title={"Nota para usuario - Rechazo de propuesta"}
        visible={showModalApproved}
        onCancel={() => setShowModalApproved(false)}
        onOk={(text) => setActionRejected(text)}
      ></TextAreaModal>

      <div className="mb-40 pb-40">
        <table className={styles.filter_table}>
          <td>
            <SelectWraper
              onChange={(e) => {
                setFilter(e[0].value);
              }}
              placeholder="Todas"
              options={optionsSelect2}
            ></SelectWraper>
          </td>
          <td>
            <Autocomplete
              dataSource={optionsAutocomplete}
              placeholder="Buscar..."
              onChange={(e) => {
                setFilterAutocomplete(e);
              }}
            />
          </td>
        </table>
        <div className={styles.btn_actualizar}>
          <Button
            className={styles.filter_button}
            type="primary"
            size="small"
            onClick={update}
          >
            {" "}
            Actualizar{" "}
          </Button>
        </div>
      </div>
      {rows?.length > 0 && <Table data={rows} columns={columns} />}
      {rows?.length === 0 && <span className="pl-20">No data.</span>}
    </>
  );
}
