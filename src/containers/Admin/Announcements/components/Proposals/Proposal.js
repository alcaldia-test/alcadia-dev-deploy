import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";

import styles from "../Announcements.module.scss";
import Button from "@components/Button";
import Avatar from "@components/Avatar";
import BreadCrumb from "@components/BreadCrumb";
import Image from "next/image";
import { LeftOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { IconTextModal } from "@components/Modals";
import TextAreaModal from "@components/Modals/TextAreaModal";
import draftToHtml from "draftjs-to-html";
import Map from "@components/Map";
import ReactPlayer from "react-player";

import { postProposalAction, getProposal } from "@redux/announcements/actions";
import Spin from "@components/Spin";

import moment from "moment";
import Comments from "@components/Comments";

import message from "@components/Message";

const defaultProfile = {
  image: "/images/avatar.png",
  name: "Nombre",
};

export default function Proposal({ id }) {
  const router = useRouter();
  const { proposal, waiting, announcement } = useSelector(
    (state) => state.announcements
  );
  const { loader } = useSelector((data) => data.common);
  const dispatch = useDispatch();

  const { announcementId } = router.query;

  const [showModalAproved, setShowModalAproved] = useState(false);
  const [showModalRejected, setShowModalRejected] = useState(false);
  const [showModalText, setShowModalText] = useState(false);
  const [approved, setApproved] = useState(false);
  const [deleted, setDeleted] = useState(false);

  const today = moment();

  const setAction = (action) => {
    if (action === "APPROVED") {
      setApproved(true);

      try {
        dispatch(
          postProposalAction({
            id: proposal?.id,
            action: action,
            message: "",
            userId: proposal?.user?.id
              ? proposal?.user?.id
              : proposal?.creatorId,
          })
        );
      } catch (error) {
        message.error("Problemas al aprobar propuesta.");
      }
    }
    if (action === "REJECTED") {
      setShowModalText(true);
    }
  };

  const setActionRejected = (text) => {
    setShowModalText(false);
    setDeleted(true);
    try {
      dispatch(
        postProposalAction({
          id: proposal?.id,
          action: "REJECTED",
          message: text,
          userId: proposal?.user?.id ? proposal?.user?.id : proposal?.creatorId,
        })
      );
    } catch (error) {
      message.error("Problemas al rechazar propuesta.");
    }
  };

  useEffect(() => {
    if (!waiting && approved) {
      setShowModalAproved(true);
    }
  }, [waiting, approved]);

  useEffect(() => {
    if (!waiting && deleted) {
      setShowModalRejected(true);
    }
  }, [waiting, deleted]);

  useEffect(() => {
    try {
      dispatch(getProposal({ id, userId: null }));
    } catch (error) {
      message.error("Problemas al obtener propuesta.");
    }
  }, []);

  const testJSON = (text) => {
    try {
      if (typeof text !== "string") {
        return false;
      } else {
        JSON.parse(text);
        return true;
      }
    } catch (error) {
      return false;
    }
  };

  return (
    <Spin spinning={loader} tip={`Cargando ...`} size="large">
      <div className={styles.header} key={"SectionHeaderPropuestas"}>
        <div className="flex">
          <Button
            className={styles.filter_button + " mr-40"}
            type="secondary"
            size="big"
            onClick={(e) => {
              e.preventDefault();
              router.push("/admin/convocatorias/" + announcementId);
            }}
          >
            <LeftOutlined />
            <span className="mt-10">Volver</span>
          </Button>
          <div className="mt-2">
            <BreadCrumb
              pages={[
                {
                  path: "/admin/convocatorias",
                  name: "Convocatorias a propuestas",
                },
                {
                  path: "/admin/convocatorias/" + announcementId,
                  name: "Administrador de propuestas",
                },
                {
                  path: "#",
                  name:
                    proposal && proposal.title?.length > 0
                      ? proposal.title
                      : "",
                },
              ]}
            />
          </div>
        </div>
      </div>

      <section
        className={styles.filter__container}
        key={"SectionContainerConvocatorias"}
      >
        {((announcement?.directPosts === false &&
          today >=
            moment(announcement?.reviewStart?.split("T")[0]).startOf("day") &&
          today <
            moment(announcement?.supportingStart?.split("T")[0]).startOf(
              "day"
            )) ||
          (announcement?.directPosts === true &&
            today >=
              moment(announcement?.receptionStart?.split("T")[0]).startOf(
                "day"
              ) &&
            today <
              moment(announcement?.postResults?.split("T")[0]).startOf(
                "day"
              ))) &&
          (proposal?.state === "WAITING" || proposal?.state === "PENDING") && (
            <div className={styles.proposal__botonera}>
              <Button
                type="secondary"
                size="small"
                className={""}
                onClick={() => {
                  setAction("REJECTED");
                }}
              >
                Rechazar
              </Button>

              <Button
                type="primary"
                size="small"
                className={styles.banner__button_create}
                onClick={() => {
                  setAction("APPROVED");
                }}
              >
                Aprobar
              </Button>
            </div>
          )}

        <p className={styles.title__proposal + " mt-10 mb-30"}>
          {proposal?.title}
        </p>
        <div className={styles.proposal__user}>
          <Avatar
            src={
              proposal && proposal?.creator?.avatar
                ? proposal?.creator?.avatar
                : defaultProfile.image
            }
            size="small"
          />
          <p className="pt-1 font-bold">{proposal?.creator?.firstName}</p>
          <p className="pt-1">
            {proposal?.createdAt
              ? moment(proposal?.createdAt).format("DD/MM/YYYY")
              : ""}
          </p>
        </div>
        {proposal && proposal?.fileLink && (
          <div className="w-4/6">
            <img className={""} src={proposal?.fileLink} />
          </div>
        )}
        {typeof proposal?.content === "object" ||
        testJSON(proposal?.content) ? (
          <div
            className={styles.step__description}
            dangerouslySetInnerHTML={{
              __html: draftToHtml(JSON.parse(proposal?.content)),
            }}
          />
        ) : (
          <div className={styles.step__description}>{proposal?.content}</div>
        )}

        {proposal?.videoLink && (
          <div className={styles.video__container}>
            <ReactPlayer
              url={proposal?.videoLink}
              playing={false}
              width="100%"
              height="100%"
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                borderRadius: "15px",
                overflow: "hidden",
              }}
              controls
            />
          </div>
        )}

        {proposal?.location !== "" &&
          proposal?.location !== null &&
          proposal?.location !== undefined && (
            <>
              <p className={styles.title__proposal + " mt-40 mb-20"}>
                Ubicación de la zona o barrio GPS
              </p>
              <p className={styles.proposal__description}>
                Puede navegar por el mapa y seleccionar la zona especificamente
                con un click.
              </p>
              <div className={styles.container__map}>
                <Map
                  latitud={proposal?.location?.split(",")[0]}
                  longitud={proposal?.location?.split(",")[1]}
                  canChangeCoordinates={false}
                  getCoordinates={() => {
                    console.log("Show mmap");
                  }}
                  defaultMarker={true}
                  zoom={9}
                />
              </div>
            </>
          )}

        {((announcement?.directPosts === false &&
          today >=
            moment(announcement?.reviewStart?.split("T")[0]).startOf("day") &&
          today <
            moment(announcement?.supportingStart?.split("T")[0]).startOf(
              "day"
            )) ||
          (announcement?.directPosts === true &&
            today >=
              moment(announcement?.receptionStart?.split("T")[0]).startOf(
                "day"
              ) &&
            today <
              moment(announcement?.postResults?.split("T")[0]).startOf(
                "day"
              ))) &&
          (proposal?.state === "WAITING" || proposal?.state === "PENDING") && (
            <div className={styles.proposal__botonera + " mt-40 pt-40"}>
              <Button
                type="secondary"
                size="small"
                className={styles.banner__button_download}
                onClick={() => {
                  setAction("REJECTED");
                }}
              >
                Rechazar
              </Button>

              <Button
                type="primary"
                size="small"
                className={styles.banner__button_create}
                onClick={() => {
                  setAction("APPROVED");
                }}
              >
                Aprobar
              </Button>
            </div>
          )}
      </section>

      <div style={{ maxWidth: 800, margin: "0 auto" }}>
        <Comments
          isAdmin
          module="proposal"
          mid={id}
          onTakeComplaint={() => {
            dispatch(getProposal({ id, userId: null }));
          }}
        />
      </div>

      <IconTextModal
        visible={showModalAproved}
        onOk={(e) => {
          e.preventDefault();
          router.push("/admin/convocatorias/" + announcementId);
        }}
        cancelText={""}
        okText={
          <>
            <span className="pt-20">Ok</span>
          </>
        }
        closable={false}
        image={
          <Image
            alt="okIcon"
            src="/icons/successAction.svg"
            width={50}
            height={50}
            layout="fixed"
          />
        }
        title={"¡Excelente!, Se aprobó la propuesta."}
        description=""
      ></IconTextModal>

      <IconTextModal
        visible={showModalRejected}
        onOk={(e) => {
          e.preventDefault();
          router.push("/admin/convocatorias/" + announcementId);
        }}
        cancelText={""}
        okText={
          <>
            <span className="pt-20">Ok</span>
          </>
        }
        closable={false}
        image={
          <Image
            alt="okIcon"
            src="/icons/errorCircle.svg"
            width={50}
            height={50}
            layout="fixed"
          />
        }
        title={"¡Propuesta rechazada!"}
        description=""
      ></IconTextModal>

      <TextAreaModal
        closable={false}
        cancelText={"Cerrar"}
        id={"1"}
        okText={"Rechazar y enviar mensaje"}
        placeholder={"Contenido"}
        text={"Motivo"}
        title={"Nota para usuario - Rechazo de propuesta"}
        visible={showModalText}
        onCancel={() => setShowModalText(false)}
        onOk={(text) => setActionRejected(text)}
      ></TextAreaModal>
    </Spin>
  );
}
