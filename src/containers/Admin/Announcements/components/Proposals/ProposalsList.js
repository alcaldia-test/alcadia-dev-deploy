import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import Tab from "@components/Tab";
import styles from "../Announcements.module.scss";
import moment from "moment";
import Image from "next/image";
import Button from "@components/Button";
import BreadCrumb from "@components/BreadCrumb";

import { LeftOutlined } from "@ant-design/icons";
import Spin from "@components/Spin";
import {
  getAnnouncement,
  pachtAnnouncement,
} from "@redux/announcements/actions";

import DataPendings from "./DataPendings";
import DataCurso from "./DataCurso";
import DataRejected from "./DataRejected";
import DataFinished from "./DataFinished";

import WysiwygModal from "@components/Modals/WysiwygModal";
import { IconTextModal } from "@components/Modals";
import draftToHtml from "draftjs-to-html";
import message from "@components/Message";

export default function ProposalsList() {
  const dispatch = useDispatch();
  const router = useRouter();

  const { announcement, adminProposalsFinished } = useSelector(
    (state) => state.announcements
  );
  const { loader } = useSelector((data) => data.common);

  const { announcementId } = router.query;
  const { tab } = router.query;
  const [windowDimensions, setWindowDimensions] = useState(0);
  const [showPrev, setShowPrev] = useState(false);
  const [tabActive, settabActive] = useState(
    tab ? (tab === "finalizadas" ? "3" : "0") : "0"
  );
  const [showWisy, setShowWisy] = useState(false);
  const [showModalCreated, setShowModalCreated] = useState(false);
  const [anunciateCreated, setAnunciateCreated] = useState(false);

  const today = moment();

  useEffect(() => {
    try {
      dispatch(getAnnouncement({ announcementId }));
    } catch (error) {
      message.error("Problemas al obtener convocatoria.");
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      setWindowDimensions(window.innerWidth);
    };

    setWindowDimensions(window.innerWidth);

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const etiquetaPropuesta = (detail) => {
    const receptionStart = moment(
      detail?.receptionStart?.split("T")[0]
    ).startOf("day");

    const receptionEnd = moment(detail?.receptionEnd?.split("T")[0]).endOf(
      "day"
    );
    const postResults = moment(detail?.postResults?.split("T")[0]).endOf("day");

    if (detail?.directPosts) {
      if (today < receptionStart) return "Esperando inicio.";
      if (today >= receptionStart && today <= receptionEnd)
        return "Recibiendo y publicando propuestas.";
      if (today >= receptionEnd && today < postResults)
        return "Esperando resultados.";
      if (today >= postResults) return "Resultados publicados.";
    } else {
      const receptionEnd = moment(detail?.receptionEnd?.split("T")[0]).endOf(
        "day"
      );
      const supportingStart = moment(
        detail?.supportingStart?.split("T")[0]
      ).startOf("day");
      const supportingEnd = moment(detail?.supportingEnd?.split("T")[0]).endOf(
        "day"
      );
      const reviewStart = moment(detail?.reviewStart?.split("T")[0]).startOf(
        "day"
      );
      const reviewEnd = moment(detail?.reviewEnd?.split("T")[0]).endOf("day");

      if (today < receptionStart) return "Esperando inicio.";
      if (today >= receptionStart && today <= receptionEnd)
        return "Recibiendo propuestas.";
      if (today >= reviewStart && today <= reviewEnd)
        return "Evaluando propuestas.";
      if (today >= supportingStart && today <= supportingEnd)
        return "En proceso de recoleccion de apoyo.";
      if (today > supportingEnd && today < postResults)
        return "Evaluando resultados.";
      if (today >= postResults) return "Resultados publicados.";
    }
    return "Esperando siguiente etapa";
  };

  const testJSON = (text) => {
    try {
      if (typeof text !== "string") {
        return false;
      } else {
        JSON.parse(text);
        return true;
      }
    } catch (error) {
      return false;
    }
  };

  const onHandleAnunciate = (text) => {
    setShowWisy(false);
    setAnunciateCreated(true);
    try {
      dispatch(
        pachtAnnouncement({
          id: announcementId,
          winnerMessage: JSON.stringify(text),
        })
      );
    } catch (error) {
      message.error("Problemas al guardar anuncio.");
    }
  };

  const haveWinner = () => {
    let winner = false;
    adminProposalsFinished?.map((_finished) => {
      if (_finished.state === "WINNER" && winner === false) {
        winner = true;
      }
      return null;
    });

    return winner;
  };

  const zonesTag = (announcement) => {
    if (announcement) {
      const areas = [];
      announcement?.macros?.forEach(({ name }) => areas.push(name));
      announcement?.zones?.forEach(({ name }) => areas.push(name));
      return areas.join(", ");
    }

    return "";
  };

  useEffect(() => {
    if (!loader && anunciateCreated) {
      setShowModalCreated(true);
      setAnunciateCreated(false);
    }
  }, [loader, anunciateCreated]);

  return showPrev === false ? (
    <Spin spinning={loader} tip={`Cargando ...`} size="large">
      <>
        <WysiwygModal
          closable={true}
          cancelText={"Cerrar"}
          id={"1"}
          okText={"Publicar comunicado"}
          placeholder={""}
          text={
            "Este comunicado se mostrará en la descripción de la convocatoria."
          }
          text2={
            "El comunicado será visible para los ciudadanos una vez se publiquen los ganadores"
          }
          title={"Comunicado de propuestas ganadoras"}
          visible={showWisy}
          onCancel={() => setShowWisy(false)}
          onOk={(text) => onHandleAnunciate(text)}
        ></WysiwygModal>

        <IconTextModal
          visible={showModalCreated}
          onOk={() => setShowModalCreated(false)}
          cancelText={""}
          okText={
            <>
              <span className="pt-20">Ok</span>
            </>
          }
          closable={false}
          image={
            <Image
              alt="okIcon"
              src="/icons/successAction.svg"
              width={50}
              height={50}
              layout="fixed"
            />
          }
          title={"Comunicado agregado."}
          description=""
        ></IconTextModal>

        <div className={styles.header} key={"SectionHeaderPropuestas"}>
          <div className="flex flex-column nowrap w-100 justify-between mb-40">
            <div className="w-max flex flex-row">
              <Button
                className={styles.filter_button + " mr-40"}
                type="secondary"
                size="big"
                onClick={(e) => {
                  e.preventDefault();
                  router.push("/admin/convocatorias/");
                }}
              >
                <LeftOutlined />
                <span className="mt-10">Volver</span>
              </Button>
              <div className="mt-20 pt-20">
                <BreadCrumb
                  pages={[
                    {
                      path: "/admin/convocatorias",
                      name: "Convocatorias a propuestas",
                    },
                    {
                      path: "#",
                      name: "Administrador de propuestas",
                    },
                  ]}
                />
              </div>
            </div>
            <div className="w-min mr-8">
              <Button
                className={styles.filter_button}
                type="secondary"
                size="big"
                onClick={() => {
                  setShowPrev(true);
                }}
              >
                <span className="mt-1 mr-1">Previsualización</span>
              </Button>
            </div>
            <div className="w-min mr-8">
              <Button
                className={styles.filter_button + " mr-40"}
                type="secondary"
                size="big"
                onClick={(e) => {
                  e.preventDefault();
                  router.push(
                    "/admin/estadisticas/convocatorias/" + announcementId
                  );
                }}
              >
                <span className="mt-1 mr-1">Estadísticas Totales</span>

                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  width="18"
                  height="18"
                >
                  <path fill="none" d="M0 0h24v24H0z" />
                  <path
                    d="M2 13h6v8H2v-8zm14-5h6v13h-6V8zM9 3h6v18H9V3zM4 15v4h2v-4H4zm7-10v14h2V5h-2zm7 5v9h2v-9h-2z"
                    fill="rgba(51,51,51,1)"
                  />
                </svg>
              </Button>
            </div>
          </div>

          <p className={styles.title_banner_0 + " mt-20 mb-20"}>
            {announcement ? announcement.title : ""}
          </p>
          <div className="flex justify-center mb-20">
            <p className={styles.zona_detail}>
              {announcement && (announcement?.zones || announcement?.macros)
                ? zonesTag(announcement)
                : "Todas las zonas"}
            </p>
            <p className={styles.date_detail}>
              Inicio:{" "}
              {announcement
                ? moment(announcement?.receptionStart?.split("T")[0]).format(
                    "DD-MM-YYYY"
                  )
                : "00/00/0000"}
            </p>
            <p className={styles.date_detail}>
              Culminación:{" "}
              {announcement
                ? moment(announcement?.postResults?.split("T")[0]).format(
                    "DD-MM-YYYY"
                  )
                : "00/00/0000"}
            </p>
          </div>
          <p className={styles.recepcion_detail}>
            {etiquetaPropuesta(announcement)}
          </p>
        </div>

        <section
          className={styles.filter__container}
          key={"SectionContainerConvocatorias"}
        >
          {announcement?.id && (
            <div
              className={styles.list__proposals__container}
              style={{ width: windowDimensions - 316 }}
            >
              {tabActive === "3" && haveWinner() === true && (
                <div
                  onClick={() => {
                    setShowWisy(true);
                  }}
                  className={styles.link__add__comunnicate}
                >
                  Agregar comunicado de propuestas ganadoras
                </div>
              )}
              <Tab
                defaultActiveKey={tabActive}
                onChange={(e) => settabActive(e)}
                content={[
                  {
                    title: "Propuestas recibidas",
                    children: (
                      <DataPendings
                        key={"listing_proposals_received"}
                        status={
                          announcement?.directPosts === false
                            ? today >=
                                moment(
                                  announcement?.receptionStart?.split("T")[0]
                                ).startOf("day") &&
                              today <
                                moment(
                                  announcement?.reviewStart?.split("T")[0]
                                ).endOf("day")
                              ? "PENDING"
                              : "WAITING"
                            : "WAITING"
                        }
                        fieldState={"adminProposalsReceived"}
                      />
                    ),
                  },
                  {
                    title: "En Curso",
                    children: (
                      <DataCurso
                        key={"listing_proposals_published"}
                        status={"FINISHED"}
                        fieldState={"adminProposalsPublished"}
                        dateStart={
                          announcement?.directPosts === false
                            ? announcement?.reviewStart
                            : announcement?.receptionStart
                        }
                        dateEnd={announcement?.postResults}
                      />
                    ),
                  },
                  {
                    title: "Rechazadas",
                    children: (
                      <DataRejected
                        key={"listing_proposals_rejected"}
                        status={"REJECTED"}
                        fieldState={"adminProposalsRejected"}
                      />
                    ),
                  },
                  {
                    title: "Finalizadas",
                    children: (
                      <DataFinished
                        key={"listing_proposals_finished"}
                        status={"FINISHED"}
                        fieldState={"adminProposalsFinished"}
                        dateStart={announcement?.postResults}
                      />
                    ),
                  },
                ]}
              />
            </div>
          )}
        </section>
      </>
    </Spin>
  ) : (
    <>
      <div className={styles.header} key={"SectionHeaderPropuestas"}>
        <div className="flex flex-column nowrap w-100 justify-between mb-40">
          <div className="w-max flex flex-row">
            <Button
              className={styles.filter_button + " mr-40"}
              type="secondary"
              size="big"
              onClick={() => {
                setShowPrev(false);
              }}
            >
              <LeftOutlined />
              <span className="mt-10">Volver</span>
            </Button>
            <div className="mt-20 pt-20">
              <BreadCrumb
                pages={[
                  {
                    path: "/admin/convocatorias",
                    name: "Convocatorias a propuestas",
                  },
                  {
                    path: "#",
                    name: "Administrador de propuestas",
                  },
                ]}
              />
            </div>
          </div>
        </div>

        <p className={styles.title_banner_1 + " mt-20 mb-20"}>
          {announcement ? announcement.title : ""}
        </p>

        <div className={"w-5/6 " + styles.prev__content_imagen}>
          <img
            className={styles.prev__imagen}
            src={
              announcement && announcement?.banner !== ""
                ? announcement?.banner
                : "http://placehold.jp/63452c/ffffff/640x350.png"
            }
          />
        </div>

        <div className={styles.prev_date}>
          Las propuestas ganadoras se anunciarán el: <br />
          {moment(announcement?.postResults?.split("T")[0]).format(
            "DD-MM-YYYY"
          )}
        </div>

        {typeof announcement?.content === "object" ||
        testJSON(announcement?.content) ? (
          <div
            className={styles.step__description + " pt-5 pb-5"}
            dangerouslySetInnerHTML={{
              __html: draftToHtml(JSON.parse(announcement?.content)),
            }}
          />
        ) : (
          <div className={styles.step__description + " p-5"}>
            {announcement?.content}
          </div>
        )}

        <div className={"w-2/6 mb-5"}>
          {announcement?.attacher && announcement?.attacher !== "" && (
            <Button
              type="secondary"
              size="small"
              href={`${announcement?.attacher[0]}?name=${announcement?.attacher[1]}`}
              className={styles.banner__button_download}
            >
              Descargar Documento
            </Button>
          )}
        </div>
        <div className={"w-4/6 " + styles.step__description}>
          Etapas de esta convocatoria
          {announcement?.directPosts === true && (
            <ul className={styles.prev__dates}>
              <li>
                Recepción y publicación de propuestas del{" "}
                {moment(announcement?.receptionStart?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}{" "}
                hasta el{" "}
                {moment(announcement?.receptionEnd?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
              <li>
                Evaluación de resultados del{" "}
                {moment(announcement?.receptionEnd?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}{" "}
                hasta el{" "}
                {moment(announcement?.postResults?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
              <li>
                Publicación de resultados{" "}
                {moment(announcement?.postResults?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
            </ul>
          )}
          {announcement?.directPosts === false && (
            <ul className={styles.prev__dates}>
              <li>
                Recepción de propuestas del{" "}
                {moment(announcement?.receptionStart?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}{" "}
                hasta el{" "}
                {moment(announcement?.receptionEnd?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
              <li>
                Evaluación de propuestas del{" "}
                {moment(announcement?.reviewStart?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}{" "}
                hasta el{" "}
                {moment(announcement?.reviewEnd?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
              <li>
                Recolección de apoyos del{" "}
                {moment(announcement?.supportingStart?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}{" "}
                hasta el{" "}
                {moment(announcement?.supportingEnd?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
              <li>
                Evaluación de resultados del{" "}
                {moment(announcement?.supportingEnd?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}{" "}
                hasta el{" "}
                {moment(announcement?.postResults?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
              <li>
                Publicación de resultados{" "}
                {moment(announcement?.postResults?.split("T")[0]).format(
                  "DD-MM-YYYY"
                )}
              </li>
            </ul>
          )}
        </div>
      </div>
    </>
  );
}
