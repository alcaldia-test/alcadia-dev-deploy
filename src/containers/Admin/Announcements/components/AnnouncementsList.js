import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import SelectWraper from "@components/Select";
import Autocomplete from "@components/Autocomplete";
import Button from "@components/Button";
import DashboardCard from "@components/Cards/Dashboard/DashboardCard";
import Pagination from "@components/Pagination";
import DeleteModal from "@components/deleteModal";
import Spin from "@components/Spin";
import { getMacroZones } from "@redux/admin_voting/actions";

import {
  getAnnouncements,
  changeDetailAnnouncement,
  changeDataLocalAnnouncement,
  deleteAnnouncement,
  getCountAnnouncements,
  editPost,
} from "@redux/announcements/actions";
import styles from "./Announcements.module.scss";
import { RightOutlined } from "@ant-design/icons";

import moment from "moment";
import { checkPermision } from "@utils/adminPermission";
import { validateAnnouncementState } from "@utils/announcementStates";
import message from "@components/Message";

const optionsSelect2 = [
  // { value: "Actives", label: "Activas" },
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más Antiguas" },
  // { value: "Finished", label: "Finalizadas" },
];

const optionsSelect1 = [
  { value: "all", label: "Todas" },
  { value: "active", label: "En Curso" },
  { value: "finished", label: "Finalizadas" },
  { value: "creating", label: "En Creación" },
];

const menuEnCurso = [
  { tag: "content", name: "Administrar propuestas", permission: "any" },
  { tag: "stadistic", name: "Ver Estadísticas", permission: "any" },
  { tag: "edit", name: "Editar", permission: "edit" },
  { tag: "delete", name: "Eliminar convocatoria", permission: "delete" },
];

const menuEnCreacion = [
  { tag: "edit", name: "Continuar Creando", permission: "edit" },
  { tag: "delete", name: "Eliminar convocatoria", permission: "delete" },
];

const menuFinalizado = [
  { tag: "stadistic", name: "Ver Estadísticas", permission: "any" },
  { tag: "results", name: "Ver Resultados", permission: "any" },
  { tag: "delete", name: "Eliminar", permission: "delete" },
];

export const AnnouncementsList = () => {
  const [filter, setFilter] = useState("all");
  const [order, setOrder] = useState(undefined);
  const [filterAutocomplete, setFilterAutocomplete] = useState(undefined);
  const [currentPage, setCurrentPage] = useState(1);
  const [updateData, setUpdateData] = useState(false);

  const { loader } = useSelector((data) => data.common);
  const router = useRouter();

  const dispatch = useDispatch();
  const { announcements, count } = useSelector((state) => state.announcements);

  const [currentDelete, setCurrentDelete] = useState({
    title: "",
    id: "",
  });

  const [announcementDeleted, setannouncementDeleted] = useState(false);
  const [announcementConsulted, setAnnouncementConsulted] = useState(false);

  const { dataUser } = useSelector((state) => state.auth);

  const [permissions, setPermissions] = useState({
    canDelete: false,
    canCreate: false,
    canEdit: false,
  });

  useEffect(() => {
    const _permissions = checkPermision(dataUser, "announcements");
    setPermissions(_permissions);
  }, [dataUser]);

  useEffect(() => {
    try {
      dispatch(getMacroZones());
    } catch (error) {
      message.error("Problemas al obtener zonas.");
    }
  }, []);

  useEffect(() => {
    if (!loader && announcementDeleted) {
      setUpdateData(!updateData);
      setannouncementDeleted(false);
    }
  }, [loader, announcementDeleted]);

  useEffect(() => {
    if (!loader && announcementConsulted) {
      router.push("/admin/convocatorias/crear");
    }
  }, [loader, announcementConsulted]);

  useEffect(() => {
    getDatas();
  }, [filter, order, currentPage, updateData]);

  const getDatas = (other = {}) => {
    const filterObj = {
      like: filterAutocomplete,
      limit: 9,
      skip: (currentPage - 1) * 9,
      order: "DESC",
      status: filter,
      ...other,
    };

    if (order === "ASC" || order === "DESC") {
      filterObj.order = order;
    }
    try {
      dispatch(getAnnouncements(filterObj));
      dispatch(getCountAnnouncements());
    } catch (error) {
      message.error("Problemas al obtener convocatorias.");
    }
  };

  const optionsAutocomplete =
    announcements && announcements.length > 0
      ? announcements.map((item) => ({
          title: item.title,
        }))
      : [];

  const handleOnClick = (announcement) => {
    dispatch(changeDetailAnnouncement(announcement));
    router.push("/admin/convocatorias/" + announcement.id);
  };

  const handleOnClickCreate = () => {
    const localAnnouncement = {
      step: 0,
      title: "",
      content: "",
      directPosts: false,
      supportingLimitStart: "",
      supportingLimitEnd: "",
      receptionStart: "",
      receptionEnd: "",
      postResults: "",
      reviewLimitStart: "",
      reviewLimitEnd: "",
      zonesId: undefined,
      macrosId: [],
      tags: [],
      file: "",
      fileLink: "",
      extraFile: "",
      errors: [],
      id: null,
    };
    dispatch(changeDataLocalAnnouncement(localAnnouncement));
    router.push("/admin/convocatorias/crear");
  };

  const _initDelete = (post) => {
    const title = post.title;
    const id = post.id;
    window.scrollTo(0, 0);
    setCurrentDelete({ title, id });
  };

  const _initEdit = (post) => {
    const announcementId = post.id;
    setAnnouncementConsulted(true);

    try {
      dispatch(editPost({ announcementId }));
    } catch (error) {
      message.error("Problemas al obtener la convocatoria.");
    }
  };

  const onMenuClick = (tag, post) => {
    switch (tag) {
      case "content":
        router.push("/admin/convocatorias/" + post.id);
        break;
      case "results":
        router.push(
          "/admin/convocatorias/" + post.id + "/propuestas/finalizadas"
        );
        break;
      case "stadistic":
        _initGotoStatistics(post);
        break;
      case "delete":
        _initDelete(post);
        break;
      case "edit":
        _initEdit(post);
        break;
      default:
        break;
    }
  };

  const _initGotoStatistics = (announcement) => {
    dispatch(changeDetailAnnouncement(announcement));
    router.push("/admin/estadisticas/convocatorias/" + announcement.id);
  };

  const _announcementStates = (detail) => {
    const _s = validateAnnouncementState({
      receptionStart: detail?.receptionStart,
      receptionEnd: detail?.receptionEnd,
      reviewStart: detail?.reviewStart,
      reviewEnd: detail?.reviewEnd,
      supportingStart: detail?.supportingStart,
      supportingEnd: detail?.supportingEnd,
      postResults: detail?.postResults,
      directPosts: detail?.directPosts,
    });

    const receptionEnd = moment(detail?.receptionEnd?.split("T")[0]).endOf(
      "day"
    );
    const postResults = moment(detail?.postResults?.split("T")[0]).endOf("day");

    if (_s.waitingReception)
      return {
        date: detail?.receptionStart,
        tag: "Esperando inicio.",
        time: "Sin empezar",
      };
    if (_s.isFinished)
      return {
        date: detail?.postResults,
        tag: "Resultados publicados.",
        time: "Finalizado",
      };
    if (_s.isEvaluating)
      return {
        date: detail?.postResults,
        tag: "Esperando resultados.",
        time: "Esperando " + tagDate(postResults),
      };

    if (detail?.directPosts) {
      if (_s.isDirectReception)
        return {
          date: detail?.receptionEnd,
          tag: "Recibiendo y publicando propuestas.",
          time: tagDate(receptionEnd),
        };
    } else {
      const supportingStart = moment(
        detail?.supportingStart?.split("T")[0]
      ).startOf("day");
      const supportingEnd = moment(detail?.supportingEnd?.split("T")[0]).endOf(
        "day"
      );
      const reviewStart = moment(detail?.reviewStart?.split("T")[0]).startOf(
        "day"
      );
      const reviewEnd = moment(detail?.reviewEnd?.split("T")[0]).endOf("day");

      if (_s.isReception)
        return {
          date: detail?.receptionEnd,
          tag: "Recibiendo propuestas.",
          time: tagDate(receptionEnd),
        };
      if (_s.waitingReview)
        return {
          date: detail?.reviewStart,
          tag: "Esperando siguiente etapa.",
          time: "Esperando " + tagDate(reviewStart),
        };
      if (_s.isReview)
        return {
          date: detail?.reviewEnd,
          tag: "Evaluando propuestas.",
          time: tagDate(reviewEnd),
        };
      if (_s.waitingSupport)
        return {
          date: detail?.supportingStart,
          tag: "Esperando siguiente etapa.",
          time: "Esperando " + tagDate(supportingStart),
        };
      if (_s.isSupporting)
        return {
          date: detail?.supportingEnd,
          tag: "En proceso de recoleccion de apoyo.",
          time: tagDate(supportingEnd),
        };
    }

    return null;
  };

  const tagDate = (date) => {
    return `${Math.trunc(getSecondsMinOrHours(date, "days"))}d: ${Math.trunc(
      getSecondsMinOrHours(date, "hours")
    )}h: ${Math.trunc(getSecondsMinOrHours(date))}min`;
  };

  const getSecondsMinOrHours = (dueDate, md = "seconds") => {
    const now = moment();
    const _diffSecondsTotal = dueDate.diff(now, "seconds"); // That give the result in seconds.
    const _diffDays = _diffSecondsTotal / (60 * 60 * 24); // we get the different divide into 86400 (number of seconds in a day)
    const _diffHours = (_diffDays % 1) * 24; // we get decimal part and multiply it by 24 to convert that tu hours
    const _diffSeconds = (_diffHours % 1) * 60; // we get the decimal part of the remanin hours and conert that to minutes

    if (md === "hours") return _diffHours;
    if (md === "days") return _diffDays;
    return _diffSeconds;
  };

  return (
    <>
      {currentDelete.id !== "" && (
        <DeleteModal
          closeModal={() =>
            setCurrentDelete({
              title: "",
              id: "",
            })
          }
          title={currentDelete.title}
          delteFuntion={() => {
            dispatch(deleteAnnouncement(currentDelete.id));
            setCurrentDelete({
              title: "",
              id: "",
            });
            setannouncementDeleted(true);
          }}
        />
      )}

      <Spin spinning={loader} tip={`Cargando ...`} size="large">
        <section
          className={styles.filter__container}
          key={"SectionContainerConvocatorias"}
        >
          <p className={styles.title_welcome}>¡Bienvenido al administrador!</p>

          <div className="flex flex-column nowrap w-full justify-between">
            <div className={"w-max flex flex-row " + styles.filter_select}>
              <SelectWraper
                onChange={(e) => {
                  setCurrentPage(1);
                  setFilter(e[0].value);
                }}
                placeholder="Todas"
                options={optionsSelect1}
              ></SelectWraper>
              <SelectWraper
                onChange={(e) => {
                  setCurrentPage(1);
                  setOrder(e[0].value);
                }}
                placeholder="Todas"
                options={optionsSelect2}
              ></SelectWraper>
              <Autocomplete
                dataSource={optionsAutocomplete}
                placeholder="Buscar..."
                onChange={(e) => {
                  setFilterAutocomplete(e);
                  getDatas({ like: e });
                }}
              />
            </div>
            <div className="w-min mr-8">
              {permissions?.canCreate && (
                <Button
                  className={styles.filter_button}
                  type="primary"
                  size="small"
                  onClick={handleOnClickCreate}
                >
                  Crear Convocatoria
                  <span className={styles.filter_button_span}> +</span>
                </Button>
              )}
            </div>
          </div>

          <div className={styles.cards__announcement}>
            {announcements && announcements.length > 0 ? (
              announcements.map((posts, index) => (
                <div
                  key={"cards_announcements_" + index}
                  className={styles.cards__container_announcement}
                >
                  <DashboardCard
                    filterMenuBypermision={true}
                    userPermision={permissions}
                    remainingFunction={() => {
                      return _announcementStates(posts)?.time;
                    }}
                    startDate={posts}
                    menustatuscurso={menuEnCurso}
                    menuencreacion={menuEnCreacion}
                    menufinalizado={menuFinalizado}
                    statusCard={_announcementStates(posts)?.tag}
                    onMenuClick={(e) => {
                      onMenuClick(e, posts);
                    }}
                    state={posts?.completed === false ? "creating" : undefined}
                    key={posts.id}
                    timeTask={_announcementStates(posts)?.date}
                    title={posts.title}
                    zones={posts.areas}
                    button={
                      <Button
                        className={styles.card__button}
                        type="secondary"
                        size="small-x"
                        onClick={(e) => {
                          e.preventDefault();
                          posts?.completed === false
                            ? _initEdit(posts)
                            : handleOnClick(posts);
                        }}
                      >
                        {posts?.completed === false
                          ? "Seguir creando"
                          : "Administrar propuestas"}
                        <RightOutlined />
                      </Button>
                    }
                    vignette={<span>...</span>}
                    image={
                      <img
                        className={styles.imagen_convocatoria}
                        src={posts.fileLink}
                        alt="imagen-convocatoria"
                        onClick={() => {
                          posts?.completed === false
                            ? _initEdit(posts)
                            : handleOnClick(posts);
                        }}
                        onError={(e) => {
                          e.target.src = "/images/banner.jpg";
                        }}
                      />
                    }
                    onGoTo={() => {
                      posts?.completed === false
                        ? _initEdit(posts)
                        : handleOnClick(posts);
                    }}
                  />
                </div>
              ))
            ) : (
              <p className={styles.no__results}>No se encontraron resultados</p>
            )}
          </div>
          <Pagination
            total={count}
            current={currentPage}
            onChange={(page) => {
              setCurrentPage(page);
            }}
            setPage={(page) => {
              setCurrentPage(page);
            }}
            defaultPageSize={9}
          />
        </section>
      </Spin>
    </>
  );
};
