import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Button from "@components/Button";
import styles from "./Create.module.scss";
import Image from "next/image";
import { useRouter } from "next/router";
import draftToHtml from "draftjs-to-html";
import { IconTextModal } from "@components/Modals";
import {
  postAnnouncement,
  changeDataLocalAnnouncement,
} from "@redux/announcements/actions";
import message from "@components/Message";

export const Step5 = ({ setStep }) => {
  const dispatch = useDispatch();
  const { localAnnouncement, waiting } = useSelector(
    (state) => state.announcements
  );
  const router = useRouter();
  const [announcementCreated, setAnnouncementCreated] = useState(false);
  const [announcementIncompleted, setAnnouncementIncompleted] = useState(false);
  const [errors, setErrors] = useState(false);
  const [showModalCreated, setShowModalCreated] = useState(false);

  const [stepReturn, setStepReturn] = useState(0);
  const [isCompleted, setIsCompleted] = useState(true);

  const handleShowHideModal = () => {
    const inputErrors = [];
    setIsCompleted(false);
    if (localAnnouncement.title === "") {
      setStepReturn(0);
      inputErrors.push("title");
    }

    if (localAnnouncement.title === "") {
      setErrors(true);
    } else {
      localAnnouncement.completed = false;
      dispatch(postAnnouncement(localAnnouncement));
      setAnnouncementIncompleted(true);
      window.scrollTo(0, 0);
    }

    const errors = inputErrors;
    dispatch(changeDataLocalAnnouncement({ errors }));
  };

  const handleCheckAnnouncement = (e) => {
    e.preventDefault();

    setIsCompleted(true);
    const inputErrors = [];

    /*
    if (localAnnouncement.tags.length === 0) {
      setStepReturn(3);
      inputErrors.push("tags");
    }
    */
    if (localAnnouncement.postResults === "") {
      setStepReturn(2);
      inputErrors.push("postResults");
    }
    if (
      localAnnouncement.reviewLimitStart === "" &&
      !localAnnouncement.directPosts
    ) {
      setStepReturn(2);
      inputErrors.push("reviewLimitStart");
    }
    if (
      localAnnouncement.supportingLimitStart === "" &&
      !localAnnouncement.directPosts
    ) {
      setStepReturn(2);
      inputErrors.push("supportingLimitStart");
    }
    if (localAnnouncement.receptionStart === "") {
      setStepReturn(2);
      inputErrors.push("receptionStart");
    }
    if (localAnnouncement.content === "") {
      setStepReturn(1);
      inputErrors.push("content");
      inputErrors.push("content-length");
    }
    if (localAnnouncement.content !== "") {
      if (typeof localAnnouncement?.content === "object") {
        let _count = 0;

        localAnnouncement?.content.blocks.map((_text) => {
          _count = _count + _text.text.length;
          return null;
        });
        if (_count < 100) {
          setStepReturn(1);
          inputErrors.push("content-length");
        }
      } else {
        if (localAnnouncement?.content.length < 100) {
          setStepReturn(1);
          inputErrors.push("content-length");
        }
      }
    }
    if (localAnnouncement.file === "" && localAnnouncement?.id === null) {
      setStepReturn(0);
      inputErrors.push("file");
    }

    if (
      localAnnouncement.file === "" &&
      localAnnouncement.fileLink === "" &&
      localAnnouncement?.id !== null
    ) {
      setStepReturn(0);
      inputErrors.push("file");
    }

    if (localAnnouncement.title === "") {
      setStepReturn(0);
      inputErrors.push("title");
    }

    const errors = inputErrors;
    dispatch(changeDataLocalAnnouncement({ errors }));

    if (inputErrors.length > 0) {
      setErrors(true);
    } else {
      localAnnouncement.completed = true;

      try {
        dispatch(postAnnouncement(localAnnouncement));
        setAnnouncementCreated(true);
      } catch (error) {
        message.error("Problemas al crear convocatoria.");
      }

      window.scrollTo(0, 0);
    }
  };

  const handleOnCloseModal = () => {
    setShowModalCreated(false);
    router.push("/admin/convocatorias");
  };

  const testJSON = (text) => {
    try {
      if (typeof text !== "string") {
        return false;
      } else {
        const _x = text.replace(/[^a-zA-Z ]/g, "");
        return JSON.parse(_x);
      }
    } catch (error) {
      return false;
    }
  };

  useEffect(() => {
    if (
      localAnnouncement &&
      localAnnouncement.file !== "" &&
      typeof localAnnouncement?.file === "object"
    ) {
      try {
        const fr = new FileReader();
        fr.onload = () => {
          document.getElementById("imagenConvocatoria").src = fr.result;
        };
        fr.readAsDataURL(localAnnouncement.file.originFileObj);
      } catch (error) {
        console.log(error);
      }
    }
  }, []);

  useEffect(() => {
    if (!waiting && announcementCreated) {
      setShowModalCreated(true);
    }
  }, [waiting, announcementCreated]);

  useEffect(() => {
    if (!waiting && announcementIncompleted) {
      router.push("/admin/convocatorias");
    }
  }, [waiting, announcementIncompleted]);

  const setZones = (zones) => {
    const _zones = [];
    zones?.map((z) => {
      _zones.push(z.name);
      return null;
    });

    return _zones.toString();
  };

  const ContainerErrors = () => {
    const errores = [];

    if (localAnnouncement.title === "") {
      errores.push(<li key={"errorTitle"}>Es necesario añadir un titulo</li>);
    }

    if (isCompleted === true) {
      if (localAnnouncement.file === "" && localAnnouncement?.id === null) {
        errores.push(
          <li key={"errorImagen"}>Es necesario añadir una imagen </li>
        );
      }
      if (
        localAnnouncement.file === "" &&
        localAnnouncement.fileLink === "" &&
        localAnnouncement?.id !== null
      ) {
        errores.push(
          <li key={"errorImagen"}>Es necesario añadir una imagen </li>
        );
      }

      if (localAnnouncement.content === "") {
        errores.push(
          <li key={"errorContent1"}>Es necesario añadir una descripción</li>
        );
        errores.push(
          <li key={"errorContent111"}>
            La descripción debe ser mayor de 100 caracteres
          </li>
        );
      }

      if (localAnnouncement.content !== "") {
        if (typeof localAnnouncement?.content === "object") {
          let _count = 0;
          localAnnouncement?.content.blocks.map((_text) => {
            _count = _count + _text.text.length;
            return null;
          });
          if (_count < 300) {
            errores.push(
              <li key={"errorContent111"}>
                La descripción debe ser mayor de 300 caracteres
              </li>
            );
          }
        } else {
          if (localAnnouncement?.content.length < 300) {
            errores.push(
              <li key={"errorContent111"}>
                La descripción debe ser mayor de 300 caracteres
              </li>
            );
          }
        }
      }

      if (localAnnouncement.receptionStart === "") {
        errores.push(
          <li key={"errorContent2"}>
            Es necesario añadir fecha de inicio y culminación
          </li>
        );
      }

      if (
        localAnnouncement.reviewLimitStart === "" &&
        !localAnnouncement.directPosts
      ) {
        errores.push(
          <li key={"errorContent4"}>
            Es necesario añadir el plazo de evaluar propuesta
          </li>
        );
      }

      if (
        localAnnouncement.supportingLimitStart === "" &&
        !localAnnouncement.directPosts
      ) {
        errores.push(
          <li key={"errorContent5"}>
            Es necesario añadir el periodo de recolección de apoyos
          </li>
        );
      }

      if (localAnnouncement.postResults === "") {
        errores.push(
          <li key={"errorContent3"}>Es necesario añadir fecha de resultados</li>
        );
      }
      /*
      if (localAnnouncement.tags.length === 0) {
        errores.push(
          <li key={"errorContent41"}>Es necesario añadir etiquetas</li>
        );
      }
      */
    }

    if (errores.length > 0) {
      return (
        <div className={styles.errores}>
          <ul>{errores}</ul>
        </div>
      );
    }

    return <></>;
  };

  return (
    <div className={styles.step}>
      <div className={"text-center mt-40 " + styles.step__description}>
        <h2>Previsualización de la convocatoria</h2>
      </div>

      <div className={"mt-40 " + styles.step__description}>
        <h2>{localAnnouncement.title}</h2>
      </div>

      <div className="">
        <img
          className={"m-auto " + styles.imagen__convocatoria}
          id="imagenConvocatoria"
          src={
            localAnnouncement?.file === "" && localAnnouncement?.id
              ? localAnnouncement?.fileLink !== ""
                ? localAnnouncement?.fileLink
                : "/images/banner.jpg"
              : "/images/banner.jpg"
          }
        />
      </div>

      {typeof localAnnouncement?.content === "object" ? (
        <div
          className={"text-justify mt-40 pt-40 " + styles.step__description}
          dangerouslySetInnerHTML={{
            __html: draftToHtml(localAnnouncement?.content),
          }}
        />
      ) : testJSON(localAnnouncement?.content) ? (
        <div
          className={styles.step__description_text}
          dangerouslySetInnerHTML={{
            __html: draftToHtml(testJSON(localAnnouncement?.content)),
          }}
        />
      ) : (
        <div className={styles.step__description_text}>
          {localAnnouncement?.content}
        </div>
      )}

      <div className={"mt-40 text-center " + styles.step__description}>
        <h2>
          Ubicación:
          {localAnnouncement.zonesId === undefined
            ? " Todas las zonas"
            : " " + setZones(localAnnouncement.zonesId)}
        </h2>
      </div>

      <div className={"flex justify-center " + styles.content__button}>
        <span className="actionButton pt-20 mt-20 font-bold">
          <a onClick={handleShowHideModal}>Terminar más tarde</a>
        </span>
        <Button
          block={true}
          className={styles.save__button}
          onClick={handleCheckAnnouncement}
          size="medium"
        >
          Publicar convocatoria
        </Button>
      </div>

      <IconTextModal
        visible={showModalCreated}
        onOk={handleOnCloseModal}
        cancelText={""}
        okText={
          <>
            <span className="pt-20">Ok</span>
          </>
        }
        closable={false}
        image={
          <Image
            alt="okIcon"
            src="/icons/successAction.svg"
            width={50}
            height={50}
            layout="fixed"
          />
        }
        title={
          localAnnouncement?.id ? "Convocatoria editada" : "Convocatoria creada"
        }
        description=""
      ></IconTextModal>

      <IconTextModal
        visible={errors}
        onOk={() => {
          setErrors(false);
          setStep(stepReturn);
          setStepReturn(0);
          window.scrollTo(0, 0);
        }}
        cancelText={""}
        okText={
          <>
            <span className="pt-20">Ok</span>
          </>
        }
        closable={false}
        image={
          <Image
            alt="okIcon"
            src="/icons/errorCircle.svg"
            width={80}
            height={80}
            layout="fixed"
          />
        }
        description={<ContainerErrors />}
      ></IconTextModal>
    </div>
  );
};
