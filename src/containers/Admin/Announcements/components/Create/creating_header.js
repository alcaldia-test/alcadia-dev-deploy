import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useSelector, useDispatch } from "react-redux";
import ButtonComponent from "@components/Button";
import { useRouter } from "next/router";
import { LeftOutlined } from "@ant-design/icons";
import DeleteModal from "@components/deleteModal";
import { postAnnouncement } from "@redux/announcements/actions";

export const CreatingHeader = () => {
  const router = useRouter();
  const { localAnnouncement, waiting } = useSelector(
    (state) => state.announcements
  );
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [announcementCreated, setAnnouncementCreated] = useState(false);
  const dispatch = useDispatch();

  const handleShowHideModal = () => {
    if (
      localAnnouncement.title !== "" ||
      localAnnouncement.file !== "" ||
      localAnnouncement.content !== "" ||
      localAnnouncement.receptionStart !== "" ||
      localAnnouncement.reviewLimitStart !== "" ||
      localAnnouncement.supportingLimitStart !== "" ||
      localAnnouncement.postResults !== "" ||
      localAnnouncement.tags.length !== 0
    ) {
      setShowDeleteModal(true);
    } else {
      router.push("/admin/convocatorias/");
    }
  };

  const modalDeleteOk = () => {
    localAnnouncement.completed = false;
    if (localAnnouncement.title === "")
      localAnnouncement.title = "Titulo provisional";
    dispatch(postAnnouncement(localAnnouncement));
    setAnnouncementCreated(true);
  };

  useEffect(() => {
    if (!waiting && announcementCreated) {
      setShowDeleteModal(false);
      router.push("/admin/convocatorias/");
    }
  }, [waiting, announcementCreated]);

  return (
    <div className="flex h-6">
      {showDeleteModal && (
        <DeleteModal
          onlyClose={() => {
            setShowDeleteModal(false);
          }}
          closeModal={() => {
            setShowDeleteModal(false);
            router.push("/admin/convocatorias/");
          }}
          mainMessage="¿Desea guardar los cambios efectuados hasta aquí?"
          delteFuntion={modalDeleteOk}
          simple={true}
          mainIconSrc="/icons/save.svg"
        />
      )}

      {/* Button Left */}
      <div className="w-1/6 flex items-center">
        <div className=" h-5 w-2/5">
          <ButtonComponent
            size="super-small"
            type="secondary"
            onClick={handleShowHideModal}
          >
            <div className="flex pt-20 pb-10">
              <LeftOutlined />
              <span className="ml-20 mr-20">Salir</span>
            </div>
          </ButtonComponent>
        </div>
      </div>
      {/* Text Center */}
      <div className="w-4/5 flex items-center">
        <span className="font-bold" style={{ fontSize: 20 }}>
          Creación de convocatoria
        </span>
      </div>
      {/* Button Right */}
      <div className="w-1/5 flex justify-center items-center">
        <div className="flex items-center justify-center">
          <span className="mr-20" style={{ fontSize: 12 }}>
            Guardando...
          </span>
          <Image
            src="/icons/save.svg"
            layout="fixed"
            width={18}
            height={18}
            alt="xboxIcon"
          />
        </div>
      </div>
    </div>
  );
};
