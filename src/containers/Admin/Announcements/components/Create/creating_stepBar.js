import React from "react";
import { useSelector, useDispatch } from "react-redux";
import StepBar from "@components/Stepper";
import { changeDataLocalAnnouncement } from "@redux/announcements/actions";

const styles = {
  class1: "w-5/6 place-self-center items-center ",
};

export const CreatingStepBar = () => {
  const dispatch = useDispatch();

  const { localAnnouncement } = useSelector((state) => state.announcements);

  const setStep = (step) => {
    dispatch(changeDataLocalAnnouncement({ step }));
  };

  return (
    <div className={styles.class1}>
      <StepBar step={localAnnouncement.step} setStep={setStep} />
    </div>
  );
};
