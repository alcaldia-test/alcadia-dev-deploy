import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Button from "@components/Button";
import styles from "./Create.module.scss";
// import TagContainer from "@components/TagContainer";
import RadialComponent from "@components/RadioGroup";
import { changeDataLocalAnnouncement } from "@redux/announcements/actions";
import SelectListComponenet from "@components/SelectList";

const _optionsMenu = [
  { id: "1", name: "Disponible para todas las zonas" },
  { id: "2", name: "Zonal" },
];

export const Step4 = ({ setStep }) => {
  const { macroZones } = useSelector((state) => state.adminVoting);
  const { localAnnouncement } = useSelector((state) => state.announcements);

  const [macroZonesOrdered, setMacroZonesOrdered] = useState([]);
  const [isZoneSelected, setIsZoneSelected] = useState(
    "Disponible para todas las zonas"
  );

  const dispatch = useDispatch();

  /*  
  const handleChangeTags = (tagList) => {
    const tags = tagList;
    dispatch(changeDataLocalAnnouncement({ tags }));

    if (tags.length > 0) {
      const inputErrors = localAnnouncement?.errors?.filter((e) => {
        return e !== "tags";
      });
      const errors = inputErrors;
      dispatch(changeDataLocalAnnouncement({ errors }));
    }
  };
  */

  useEffect(() => {
    if (localAnnouncement?.zonesId) {
      setIsZoneSelected("Zonal");
    }
  }, [localAnnouncement]);

  useEffect(() => {
    const _macroZonesOrdered = [];
    macroZones.forEach((macroZone) => {
      const _zones = [];
      let checkedMainZone = false;
      macroZone?.zones?.forEach((zone) => {
        let checked = false;
        localAnnouncement?.zonesId?.forEach((zonesInLocalPost) => {
          const isChecked = zonesInLocalPost?.selected?.find(
            (z) => z.id === zone.id
          );

          if (isChecked) {
            checked = true;
            checkedMainZone = true;
          }
        });
        _zones.push({ id: zone.id, zone: zone.name, checked });
      });
      const _macroZone = {
        id: macroZone.id,
        name: macroZone.name,
        zones: _zones,
        checked: checkedMainZone,
      };
      _macroZonesOrdered.push(_macroZone);
    });

    setMacroZonesOrdered(_macroZonesOrdered);
  }, [macroZones, localAnnouncement]);

  const handleChange = (e) => {
    if (e?.target?.id === "2") {
      const zonesId = [];
      dispatch(changeDataLocalAnnouncement({ zonesId }));
    } else if (e?.target?.id === "1") {
      const zonesId = undefined;
      dispatch(changeDataLocalAnnouncement({ zonesId }));
    }
  };

  const handleSelectZones = (e) => {
    const zonesId =
      localAnnouncement?.zonesId?.filter((x) => x.id !== e?.id) || [];
    if (e?.selected.length > 0) {
      zonesId.push(e);
    }
    dispatch(changeDataLocalAnnouncement({ zonesId }));
  };

  return (
    <div className={styles.step}>
      <div className={styles.step__description}>
        <h2>
          Ya casi terminamos, ¿Esta convocatoria será zonal o pueden participar
          los ciudadanos de todas las zonas?
        </h2>
        <p>
          Establece la petición para zonas específicas de la paz o configúrala
          para que todos los ciudadanos de la paz puedan participar.
        </p>
      </div>

      <div className="w-1/6 mb-40">
        <RadialComponent
          onChange={handleChange}
          options={_optionsMenu}
          valueDefault={isZoneSelected}
        />
      </div>

      {localAnnouncement?.zonesId && (
        <div className="mx-3 mb-40 pb-40">
          <SelectListComponenet
            onSelect={handleSelectZones}
            list={macroZonesOrdered}
            showZones={false}
          />
        </div>
      )}

      {/*
      <div className={styles.step__description}>
        <h2>Seleccione o añada etiquetas relacionadas a esta convocatoria</h2>
      </div>
        <div className="w-5/6">
          <TagContainer
            onSelect={handleChangeTags}
            selectedtag={localAnnouncement?.tags || []}
          />
        </div>
        {localAnnouncement?.errors && localAnnouncement?.errors.includes("tags") && (
          <span className={styles.errores}>
            {" "}
            <br></br>Es necesario anadir etiquetas
          </span>
        )}

      */}

      <Button
        block={true}
        className={styles.step__button}
        onClick={() => {
          setStep(localAnnouncement?.step + 1);
        }}
        size="medium"
      >
        Siguiente
      </Button>
    </div>
  );
};
