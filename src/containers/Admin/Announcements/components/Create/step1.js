import { useSelector, useDispatch } from "react-redux";
import Button from "@components/Button";
import message from "@components/Message";
import {
  changeDataLocalAnnouncement /* postAnnouncement */,
  deleteFile,
} from "@redux/announcements/actions";
import styles from "./Create.module.scss";
import UploadComponent from "@components/Upload";
export const Step1 = ({ setStep }) => {
  const dispatch = useDispatch();
  const { localAnnouncement } = useSelector((state) => state.announcements);

  const handleChange = (_title) => {
    const inputErrors = localAnnouncement?.errors?.filter((e) => {
      return e !== "title";
    });
    const errors = inputErrors;

    const title = _title.substr(0, 200);
    dispatch(changeDataLocalAnnouncement({ errors }));
    dispatch(changeDataLocalAnnouncement({ title }));
  };

  const handleChangeFile = (info) => {
    if (info.file.status === "uploading") {
      const reader = new FileReader();
      reader.readAsDataURL(info?.file?.originFileObj);
      reader.onload = function (e) {
        const image = new Image();
        image.src = e.target.result;
        image.onload = function () {
          const height = this.height;
          const width = this.width;
          if (width > 1900 || height > 1900) {
            message.error(
              "La imagen no tiene las dimensiones adecuadas, procure que el ancho sea de maximo 1900 pixeles "
            );
            return false;
          } else {
            const file = info?.file;
            file.status = "done";
            dispatch(changeDataLocalAnnouncement({ file: file }));

            const inputErrors = localAnnouncement?.errors?.filter((e) => {
              return e !== "file";
            });
            const errors = inputErrors;
            dispatch(changeDataLocalAnnouncement({ errors }));
          }
        };
      };
    }

    if (info.file.status === "removed") {
      dispatch(changeDataLocalAnnouncement({ file: "" }));
    }
  };

  const handleOnDeleteFile = () => {
    const payload = {
      tag: "banner",
      id: localAnnouncement?.postsId,
      PostionFieldName: "fileLink",
      value: "",
    };
    try {
      dispatch(deleteFile(payload));
    } catch (error) {
      message.error("Problemas al eliminar imagen.");
    }
  };

  return (
    <div className={styles.step}>
      <div className={styles.step__description}>
        <h2>Escribe el título de la convocatoria para propuestas</h2>
        <p>
          Capta la atención con un título corto que haga comprender a los
          ciudadanos que temática tienen que crear sus propuestas.
        </p>
      </div>
      <div className={styles.custom__external}>
        <input
          type="text"
          className="input__internal"
          placeholder={"Ej: Necesidad de basureros en..."}
          onChange={(e) => handleChange(e.target.value)}
          name="title"
          value={localAnnouncement?.title || ""}
        />
      </div>

      {localAnnouncement?.errors &&
        localAnnouncement?.errors.includes("title") && (
          <span className={styles.errores}>
            {" "}
            <br></br>El título debe tener al menos 10 caracteres
          </span>
        )}
      <div className={styles.step__description + " mt-40 pt-20"}>
        <h2>Agrega una foto</h2>
        <p>
          Las convocatorias con foto incentivan más a los ciudadanos a que
          participen y creen sus propuestas.
        </p>

        <div className=" h-5 w-2/5">
          <UploadComponent
            textButton={"Añadir Imagen"}
            onChange={handleChangeFile}
            accept="image/*"
            multiple={false}
            fileList={localAnnouncement?.file ? [localAnnouncement?.file] : []}
            filesInServer={
              localAnnouncement?.fileLink ? [localAnnouncement?.fileLink] : []
            }
            allowAddMoreToServer={false}
            onDeleteFunction={handleOnDeleteFile}
          />
        </div>
      </div>
      {localAnnouncement?.errors && localAnnouncement?.errors.includes("file") && (
        <span className={styles.errores}>
          {" "}
          <br></br>Es necesario añadir una imagen
        </span>
      )}
      <Button
        block={true}
        className={styles.step__button}
        onClick={() => {
          setStep(localAnnouncement?.step + 1);
        }}
        size="medium"
      >
        Siguiente
      </Button>
    </div>
  );
};
