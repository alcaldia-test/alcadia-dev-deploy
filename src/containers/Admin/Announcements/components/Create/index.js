import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { CreatingHeader } from "./creating_header";
import { CreatingStepBar } from "./creating_stepBar";
import { Step1 } from "./step1";
import { Step2 } from "./step2";
import { Step3 } from "./step3";
import { Step4 } from "./step4";
import { Step5 } from "./step5";
import styles from "./Create.module.scss";
import { changeDataLocalAnnouncement } from "@redux/announcements/actions";
import { getMacroZones } from "@redux/admin_voting/actions";
import message from "@components/Message";
import Spin from "@components/Spin";

const AdminCreatingAnnouncementScreen = () => {
  const dispatch = useDispatch();
  const { localAnnouncement } = useSelector((state) => state.announcements);
  const { loader } = useSelector((data) => data.common);

  const handleChangeStep = (step) => {
    dispatch(changeDataLocalAnnouncement({ step }));
  };

  useEffect(() => {
    try {
      dispatch(getMacroZones());
    } catch (error) {
      message.error("Problemas al obtener zonas.");
    }
  }, []);

  return (
    <>
      <Spin spinning={loader} tip={`Cargando ...`} size="large">
        <div className="flex h-screen justify-center pt-40">
          <div
            className={
              "h-screen bg-white px-2 flex flex-col gap-4 " + styles.container
            }
          >
            <CreatingHeader />
            <CreatingStepBar />
            <div
              className={
                "border-2 border-greyheader rounded-sm p-4 " + styles.content
              }
            >
              {localAnnouncement?.step === 0 && (
                <Step1 setStep={handleChangeStep} />
              )}
              {localAnnouncement?.step === 1 && (
                <Step2 setStep={handleChangeStep} />
              )}
              {localAnnouncement?.step === 2 && (
                <Step3 setStep={handleChangeStep} />
              )}
              {localAnnouncement?.step === 3 && (
                <Step4 setStep={handleChangeStep} />
              )}
              {localAnnouncement?.step === 4 && (
                <Step5 setStep={handleChangeStep} />
              )}
            </div>
          </div>
        </div>
      </Spin>
    </>
  );
};

export default AdminCreatingAnnouncementScreen;
