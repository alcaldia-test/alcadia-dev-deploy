import { useSelector, useDispatch } from "react-redux";

import Button from "@components/Button";
import WYSIWYGCOMPONENET from "@components/WYSIWYG";
import styles from "./Create.module.scss";
import {
  changeDataLocalAnnouncement,
  deleteFile,
} from "@redux/announcements/actions";
import UploadComponent from "@components/Upload";
import message from "@components/Message";

export const Step2 = ({ setStep }) => {
  const dispatch = useDispatch();
  const { localAnnouncement } = useSelector((state) => state.announcements);

  const handleChange = (content) => {
    if (content) {
      const inputErrors = localAnnouncement?.errors?.filter((e) => {
        return e !== "content" && e !== "content-length";
      });
      const errors = inputErrors;
      dispatch(changeDataLocalAnnouncement({ errors }));
      dispatch(changeDataLocalAnnouncement({ content }));
    }
  };

  const handleChangeFile = (info) => {
    if (info.file.status === "uploading") {
      const extraFile = info?.file;
      extraFile.status = "done";
      dispatch(changeDataLocalAnnouncement({ extraFile: extraFile }));
    }

    if (info.file.status === "removed") {
      dispatch(changeDataLocalAnnouncement({ extraFile: "" }));
    }
  };

  const handleOnDeleteFile = () => {
    const payload = {
      tag: "attached",
      id: localAnnouncement?.postsId,
      PostionFieldName: "attacher",
      value: "",
    };
    try {
      dispatch(deleteFile(payload));
    } catch (error) {
      message.error("Problemas al eliminar archivo.");
    }
  };

  return (
    <div className={styles.step}>
      <div className={styles.step__description}>
        <h2>Explica la convocatoria para propuestas</h2>
        <p>
          Las explicaciones de convocatorias más exitosas suelen tener 3
          párrafos de largo. Te recomendamos que agregues aproximadamente 1000
          caracteres.
        </p>
      </div>
      <WYSIWYGCOMPONENET
        onChange={handleChange}
        row={7}
        value={localAnnouncement?.content || {}}
        rawContentState={localAnnouncement?.content || {}}
        minCharacter={100}
      />
      {localAnnouncement?.errors &&
        localAnnouncement?.errors.includes("content") && (
          <span className={styles.errores}>
            {" "}
            <br></br>Es necesario una descripción
          </span>
        )}
      {localAnnouncement?.errors &&
        localAnnouncement?.errors.includes("content-length") && (
          <span className={styles.errores}>
            {" "}
            <br></br>La descripción debe ser mayor de 100 caracteres
          </span>
        )}
      <div className={styles.step__description + " mt-40 pt-40"}>
        <h2>Puede subir archivos también</h2>
        <p>
          Si es necesario mostrar a los ciudadanos más información puede subir
          los documentos acá para que puedan descargarlos siempre.
        </p>

        <div className=" h-5 w-2/5">
          <UploadComponent
            textButton={"Subir archivos"}
            onChange={handleChangeFile}
            accept=".pdf"
            multiple={false}
            fileList={
              localAnnouncement?.extraFile ? [localAnnouncement?.extraFile] : []
            }
            filesInServer={
              localAnnouncement?.attacher ? [localAnnouncement?.attacher] : []
            }
            allowAddMoreToServer={false}
            onDeleteFunction={handleOnDeleteFile}
          />
        </div>
      </div>
      <Button
        block={true}
        className={styles.step__button}
        onClick={() => {
          setStep(localAnnouncement?.step + 1);
        }}
        size="medium"
      >
        Siguiente
      </Button>
    </div>
  );
};
