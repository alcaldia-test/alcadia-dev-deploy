import React from "react";

import { useSelector, useDispatch } from "react-redux";
import Button from "@components/Button";

import DatePickerComponent from "@components/DatePicker";
import DateField from "@components/DatePickerRange";
import Switch from "@components/Switch";

import { changeDataLocalAnnouncement } from "@redux/announcements/actions";
import styles from "./Create.module.scss";
import moment from "moment";

export const Step3 = ({ setStep }) => {
  const dispatch = useDispatch();
  const { localAnnouncement } = useSelector((state) => state.announcements);

  const handleChangeDateRangePicker = (e) => {
    let receptionStart = "";
    let receptionEnd = "";

    if (e.length > 0) {
      receptionStart = moment(e[0]).startOf("day");
      receptionEnd = moment(e[1]).endOf("day");
    }

    dispatch(changeDataLocalAnnouncement({ receptionStart }));
    dispatch(changeDataLocalAnnouncement({ receptionEnd }));

    const inputErrors = localAnnouncement?.errors?.filter((e) => {
      return e !== "receptionStart";
    });
    const errors = inputErrors;
    dispatch(changeDataLocalAnnouncement({ errors }));
  };

  const handleDatePostResult = (e) => {
    let postResults = "";
    if (e) {
      postResults = e;
    }
    dispatch(changeDataLocalAnnouncement({ postResults }));

    const inputErrors = localAnnouncement?.errors?.filter((e) => {
      return e !== "postResults";
    });
    const errors = inputErrors;
    dispatch(changeDataLocalAnnouncement({ errors }));
  };

  const handleDateSupportLimit = (e) => {
    let supportingLimitStart = "";
    let supportingLimitEnd = "";
    if (e.length > 0) {
      supportingLimitStart = e[0];
      supportingLimitEnd = e[1];
    }
    dispatch(changeDataLocalAnnouncement({ supportingLimitStart }));
    dispatch(changeDataLocalAnnouncement({ supportingLimitEnd }));

    const inputErrors = localAnnouncement?.errors?.filter((e) => {
      return e !== "supportingLimitStart";
    });
    const errors = inputErrors;
    dispatch(changeDataLocalAnnouncement({ errors }));
  };

  const handleDateReviewLimit = (e) => {
    let reviewLimitStart = "";
    let reviewLimitEnd = "";
    if (e.length > 0) {
      reviewLimitStart = e[0];
      reviewLimitEnd = e[1];
    }
    dispatch(changeDataLocalAnnouncement({ reviewLimitStart }));
    dispatch(changeDataLocalAnnouncement({ reviewLimitEnd }));

    const inputErrors = localAnnouncement?.errors?.filter((e) => {
      return e !== "reviewLimitStart";
    });
    const errors = inputErrors;
    dispatch(changeDataLocalAnnouncement({ errors }));
  };

  const disabledDateRange = (current) => {
    // Can not select days before today and today
    return current && current < moment().subtract(1, "days").endOf("days");
  };

  const disabledDateReview = (current) => {
    // Can not select days before today and today
    return (
      current && current < moment(localAnnouncement?.receptionEnd).endOf("days")
    );
  };

  const disabledDateSupport = (current) => {
    // Can not select days before today and today
    return (
      current &&
      current < moment(localAnnouncement?.reviewLimitEnd).endOf("days")
    );
  };

  const disabledDate = (current) => {
    // Can not select days before today and today
    if (localAnnouncement?.directPosts) {
      return (
        current &&
        current < moment(localAnnouncement?.receptionEnd).endOf("days")
      );
    } else {
      return (
        current &&
        current < moment(localAnnouncement?.supportingLimitEnd).endOf("days")
      );
    }
  };

  return (
    <div className={styles.step}>
      <div className={styles.step__description}>
        <h2>
          Establece la fecha de Inicio y culminación de recepción de propuestas
        </h2>
        <p>
          Configura la fecha en la que se anunciarán las propuestas ciudadanas
          que hayan recogido más votos. Es importante que las personas sepan que
          los escuchamos.
        </p>
      </div>
      <span
        className={
          "grid border-2 border-greyheader p-10 rounded-sm justify-center w-3/6 " +
          (localAnnouncement?.errors &&
          localAnnouncement?.errors.includes("receptionStart")
            ? "border-red"
            : "")
        }
      >
        <DateField
          onChange={handleChangeDateRangePicker}
          value={[
            localAnnouncement?.receptionStart,
            localAnnouncement?.receptionEnd,
          ]}
          dateFormat="YYYY-MM-DD"
          disabledDate={disabledDateRange}
          showTime={false}
        />
      </span>
      {localAnnouncement?.errors &&
        localAnnouncement?.errors.includes("receptionStart") && (
          <span className={styles.errores}>
            {" "}
            <br></br>Es necesario una fecha de inicio y culminación
          </span>
        )}

      <div className={styles.container__switch}>
        <Switch
          size="lg"
          isChecked={localAnnouncement?.directPosts}
          toggle={() => {
            const directPosts = !localAnnouncement?.directPosts;
            dispatch(changeDataLocalAnnouncement({ directPosts }));
          }}
        />
        <p>Evaluar propuestas y hacer públicas al instante</p>
      </div>

      {localAnnouncement?.directPosts === false && (
        <>
          <div className={styles.step__description + " pt-40 mt-40"}>
            <h2>
              Establece el plazo de evaluar propuesta una vez finalizada la
              recepción.
            </h2>
            <p>
              Con este plazo podrás tener un tiempo para evaluar las propuestas
              antes del periodo de recolección de apoyos.
            </p>
          </div>
          <span
            className={
              "grid border-2 border-greyheader  p-10 rounded-sm justify-center w-3/6 " +
              (localAnnouncement?.errors &&
              localAnnouncement?.errors.includes("reviewLimitStart")
                ? "border-red"
                : "")
            }
          >
            <DateField
              disabledDate={disabledDateReview}
              onChange={handleDateReviewLimit}
              value={[
                localAnnouncement?.reviewLimitStart,
                localAnnouncement?.reviewLimitEnd,
              ]}
              dateFormat="YYYY-MM-DD"
              showTime={false}
              disabled={localAnnouncement.receptionStart === ""}
            />
          </span>
          {localAnnouncement?.errors &&
            localAnnouncement?.errors.includes("reviewLimitStart") && (
              <span className={styles.errores}>
                {" "}
                <br></br>Es necesario anadir el plazo de evaluar propuesta
              </span>
            )}

          <div className={styles.step__description + " pt-40 mt-40"}>
            <h2>
              ¿Hasta cuándo durará el periodo de recolección de apoyos para las
              propuestas?
            </h2>
          </div>
          <span
            className={
              "grid border-2 border-greyheader  p-10 rounded-sm justify-center w-3/6 " +
              (localAnnouncement?.errors &&
              localAnnouncement?.errors.includes("supportingLimitStart")
                ? "border-red"
                : "")
            }
          >
            <DateField
              onChange={handleDateSupportLimit}
              disabledDate={disabledDateSupport}
              value={[
                localAnnouncement?.supportingLimitStart,
                localAnnouncement?.supportingLimitEnd,
              ]}
              dateFormat="YYYY-MM-DD"
              showTime={false}
              disabled={localAnnouncement.reviewLimitStart === ""}
            />
          </span>
          {localAnnouncement?.errors &&
            localAnnouncement?.errors.includes("supportingLimitStart") && (
              <span className={styles.errores}>
                {" "}
                <br></br>Es necesario anadir el periodo de recolección de apoyos
              </span>
            )}
        </>
      )}

      <div className={styles.step__description + " pt-40 mt-40"}>
        <h2>Establece la fecha de muestra de resultados.</h2>
      </div>
      <span
        className={
          "grid border-2 border-greyheader  p-10 rounded-sm justify-center w-3/6 " +
          (localAnnouncement?.errors &&
          localAnnouncement?.errors.includes("postResults")
            ? "border-red"
            : "")
        }
      >
        <DatePickerComponent
          setDate={handleDatePostResult}
          date={localAnnouncement?.postResults}
          disabledDate={disabledDate}
          placeholder={"Seleccione fecha"}
          disabled={
            !(
              (localAnnouncement?.directPosts === true &&
                localAnnouncement?.receptionStart !== "") ||
              (localAnnouncement?.directPosts === false &&
                localAnnouncement?.supportingLimitStart !== "")
            )
          }
        />
      </span>

      {localAnnouncement?.errors &&
        localAnnouncement?.errors.includes("postResults") && (
          <span className={styles.errores}>
            {" "}
            <br></br>Es necesario una fecha de inicio y culminación
          </span>
        )}

      <Button
        block={true}
        className={styles.step__button}
        onClick={() => {
          setStep(localAnnouncement?.step + 1);
        }}
        size="medium"
      >
        Siguiente
      </Button>
    </div>
  );
};
