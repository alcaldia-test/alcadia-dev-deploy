import { AnnouncementsList } from "./components/AnnouncementsList";
const AdminAnnouncementsContainer = () => {
  return (
    <>
      <AnnouncementsList />
    </>
  );
};

export default AdminAnnouncementsContainer;
