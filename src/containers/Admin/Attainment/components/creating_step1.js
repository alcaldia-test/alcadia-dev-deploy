import React from "react";
import ButtonComponent from "@components/Button";
import UploadComponent from "@components/Upload";
import CustomInput from "../CustomInput";

export const CreatingStep1 = ({
  current = {},
  setCreatingStep,
  setCurrent,
  validate,
}) => {
  const handleChange = (e) => {
    setCurrent({ ...current, title: e.target.value });
  };

  const handleChangeFile = (info) => {
    console.log(info);
    if (info.file.status === "uploading") {
      const _file = info.file;
      _file.status = "done";

      setCurrent({ ...current, banner: _file });
    }

    if (info.file.status === "removed") {
      setCurrent({ ...current, banner: undefined });
    }
  };

  const handleOnClick = () => {
    setCreatingStep(1);
  };

  return (
    <div className="flex flex-col gap-3 p-2">
      <span className="font-bold text-H6">Escribe el título del logro</span>
      <span className="text-bodyMedium">
        Capta la atención con un título corto que haga comprender a los
        ciudadanos sobre que temática trata el Logro
      </span>
      <CustomInput
        onChange={handleChange}
        name="title"
        value={current?.title || ""}
        validateField={validate === "title" || validate === "all"}
        messageOfError="Es necesario añadir un titulo"
        minLength={3}
      />
      <span className="font-bold text-H5">Agrega una foto</span>
      <span className="text-bodyMedium">
        Esta es la foto que acompañará la presentación del Logro
      </span>
      <UploadComponent
        onChange={handleChangeFile}
        sizeButton="small-x"
        accept="image/*"
        textButton="Subir Imagen"
        multiple={false}
        fileList={current?.banner ? [current?.banner] : []}
        filesInServer={current?.fileLink ? [current?.fileLink] : []}
        allowAddMoreToServer={false}
        onDeleteFunction={false}
        onDeleteClick={() => setCurrent({ ...current, fileLink: "" })}
      />
      <div className="flex items-center justify-center">
        <ButtonComponent size="small-x" onClick={handleOnClick}>
          <span className="text-bodyMedium">Siguiente</span>
        </ButtonComponent>
      </div>
    </div>
  );
};
