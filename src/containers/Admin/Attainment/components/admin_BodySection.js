import React from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import ButtonComponent from "@components/Button";
import DashBoardComponent from "@components/Cards/Dashboard/DashboardCard";
import confirm from "@components/Modals/confirm";

import {
  deleteAttainment,
  showHideCreatingSection,
  editAttainment,
} from "@redux/attainments/actions";
import message from "@components/Message";

import Styles from "../adminVooting.module.scss";

const menuEnCurso = [
  { tag: "content", name: "Ver Contenido" },
  { tag: "edit", name: "Editar" },
  { tag: "delete", name: "Eliminar Logro" },
  { tag: "disabled", name: "Ocultar Logro" },
];

const menuEnCreacion = [
  { tag: "edit", name: "Continuar Creando" },
  { tag: "delete", name: "Eliminar Logro" },
];

const menuFinalizado = [
  { tag: "content", name: "Ver Contenido" },
  { tag: "edit", name: "Editar" },
  { tag: "delete", name: "Eliminar Logro" },
  { tag: "active", name: "Mostrar Logro" },
];

export const BodySection = ({ datas, setLoading, setSeleted }) => {
  const dispatch = useDispatch();

  const router = useRouter();

  const handleDelete = async (x) => {
    const result = await confirm(`¿Seguro(a) de eliminar ${x.title}?`);
    if (!result) return;

    try {
      setLoading(true);

      await new Promise((resolve, reject) =>
        dispatch(deleteAttainment({ id: x.id, resolve, reject }))
      );
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };
  const handleActiveArchive = async (state, id) => {
    try {
      setLoading(true);

      await new Promise((resolve, reject) =>
        dispatch(
          editAttainment({ state, id, fileLink: "dumn", resolve, reject })
        )
      );
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };

  const openEdition = (x) => {
    setSeleted(x);
    dispatch(showHideCreatingSection(true));
  };

  return (
    <div className={Styles.listCardContainer}>
      {/* GRID */}
      <div className="flex flex-wrap justify-around p-1 gap-1">
        {datas?.map((x, i) => {
          const ButtonBottom = () => (
            <ButtonComponent
              size="small-x"
              type="secondary"
              style={{ width: "90%" }}
              onClick={() => {
                if (x.state === "creating") openEdition(x);
                else router.push(`/admin/config/logros/${x.id}`);
              }}
            >
              {x.state === "creating" ? (
                <span>Continuar Creando</span>
              ) : (
                <span>Ver Contenido</span>
              )}
            </ButtonComponent>
          );

          const ImageCard = () => {
            return <img src={x.fileLink} alt="alter" />;
          };

          const handleOnMenuClick = async (e) => {
            if (e === "delete") await handleDelete(x);
            if (e === "content") router.push(`/admin/config/logros/${x.id}`);
            if (e === "edit") openEdition(x);
            if (e === "disabled") await handleActiveArchive("DISABLED", x.id);
            if (e === "active") await handleActiveArchive("PUBLISHED", x.id);
          };

          return (
            <DashBoardComponent
              key={i}
              width={300}
              height={340}
              image={<ImageCard />}
              title={x.title}
              button={<ButtonBottom />}
              vignette="…"
              state={x.state}
              withInfo={false}
              statusCard={<span role={`status-${x.state}`}>{x.tag}</span>}
              onMenuClick={handleOnMenuClick}
              menustatuscurso={menuEnCurso}
              menuencreacion={menuEnCreacion}
              menufinalizado={menuFinalizado}
            />
          );
        })}
        {datas?.length === 0 && (
          <div className="flex items-center text-center justify-center w-full h-150">
            <span className="text-H5">No hubo resultados</span>
          </div>
        )}
      </div>
    </div>
  );
};
