import React from "react";
import Image from "next/image";
import { useDispatch } from "react-redux";
import confirm from "@components/Modals/confirm";

import { showHideCreatingSection } from "@redux/attainments/actions";
import ButtonComponent from "@components/Button";

export const CreatingHeader = ({ loading, onSaveData, current, old }) => {
  const dispatch = useDispatch();

  const handleShowHideModal = async () => {
    const curentContent = JSON.stringify(current.content);
    if (
      current.title !== old.title &&
      (curentContent !== old.content || curentContent)
    ) {
      const result = await confirm(
        "¿Deseas guardar los cambios?",
        true,
        "Si",
        "No"
      );

      if (result) await onSaveData(false);
    }

    dispatch(showHideCreatingSection(false));
  };

  return (
    <div className="flex h-6">
      {/* Button Left */}
      <div className="flex items-center">
        <div className=" h-5 w-2/5">
          <ButtonComponent
            size="super-small"
            type="secondary"
            onClick={handleShowHideModal}
          >
            <div className="flex">
              <span className="text-callout">{"< Salir"}</span>
            </div>
          </ButtonComponent>
        </div>
      </div>
      {/* Text Center */}
      <div className="w-4/5 flex items-center ml-3 ">
        <span className="font-bold text-headline">Creación de Logro</span>
      </div>
      {/* Button Right */}
      <div className="w-1/5 flex justify-center items-center">
        {loading && (
          <div className="flex items-center justify-center">
            <span style={{ fontSize: 10 }}>Guardando...</span>
            <Image
              src="/icons/save.svg"
              layout="fixed"
              width={18}
              height={18}
              alt="xboxIcon"
            />
          </div>
        )}
      </div>
    </div>
  );
};
