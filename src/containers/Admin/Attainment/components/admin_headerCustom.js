import React from "react";
import Image from "next/image";

import ButtonComponent from "@components/Button";

export const HeaderCustom = () => {
  const handleOnClick = () => {};

  return (
    <div className="items-center bg-greyheader flex justify-between p-20">
      <div className="font-bold text-center">Tu Voto Cuenta (Votaciones)</div>
      <div className="flex gap-60 justify-center">
        {/* TO ASK QUESTION BUTTON TO LARGE IT IS NOT RESPONSIVE */}
        <ButtonComponent size="small-x" onClick={handleOnClick}>
          Compartir Palabra Secreta
        </ButtonComponent>
        {/* TO ASK QUESTION ICONS BUTTONS ARE NOT DESIGN, HOW TO IMPORT SVG */}
        <span className="flex  cursor-pointer items-center">
          <Image
            src="/icons/notification.svg"
            layout="fixed"
            width={25}
            height={25}
            alt="xboxIcon"
          />
        </span>
        {/* TO ASK QUESTION ICONS BUTTONS ARE NOT DESIGN, HOW TO IMPORT SVG */}
        <span className="flex  cursor-pointer items-center">
          <Image
            src="/icons/user-circle.svg"
            layout="fixed"
            width={25}
            height={25}
            alt="xboxIcon"
          />
          <span>Administrador</span>
          <Image
            src="/icons/arrow-down.svg"
            layout="fixed"
            width={20}
            height={20}
            alt="xboxIcon"
          />
        </span>
      </div>
    </div>
  );
};
