import React, { useState, useEffect } from "react";
import ButtonComponent from "@components/Button";
import draftToHtml from "draftjs-to-html";

export const CreatingStep5 = ({
  current = {},
  setCreatingStep,
  setValidate,
  onSaveData,
}) => {
  const [imageUrl, setImageUrl] = useState("/images/banner.jpg");

  useEffect(() => {
    try {
      if (current?.fileLink) {
        setImageUrl(current.fileLink);
      } else {
        setImageUrl(URL.createObjectURL(current?.banner?.originFileObj));
      }
    } catch (error) {
      setImageUrl("/images/banner.jpg");
    }
  }, [current]);

  const savePost = (completed) => {
    let goto = validateTitle();

    if (!completed) setValidate("title");
    else setValidate("all");

    if (goto !== -1) setCreatingStep(goto);
    else if (completed) {
      goto = validateContent();

      if (goto !== -1) setCreatingStep(goto);
    }

    if (goto === -1) onSaveData(completed);
  };

  const validateTitle = () => {
    let resp = 0;

    if (current.title?.length >= 3 && current.title?.length <= 200) resp = -1;

    return resp;
  };
  const validateContent = () => {
    let resp = -1;
    let text = current.content;

    if (typeof text !== "string") text = draftToHtml(text);

    let parsed = text.replace(/(\n|&nbsp;)/g, " ");
    parsed = parsed.replace(/(<\/?.{1,10}> | &lt; | &gt;)/g, "");

    if (parsed.length < 300) resp = 1;

    return resp;
  };

  const content =
    typeof current.content === "string"
      ? current?.content
      : draftToHtml(current?.content);

  return (
    <>
      <div className="px-3">
        <div className="font-bold text-center text-H4 mb-6 mt-3">
          Previsualización del logro
        </div>
        <div className="font-bold text-H4 text-center mb-4">
          {current?.title}
        </div>
        <div className="grid justify-items-center mb-3 ">
          <img
            src={imageUrl}
            className="max-w-500 max-h-500 object-contain"
            alt="alter"
          />
        </div>
        <div
          className="text-bodyMedium pb-3 leading-relaxed mt-40 text-justify mb-16 text-textColor4"
          dangerouslySetInnerHTML={{
            __html: content,
          }}
        />
        <div className="flex m-5 justify-around">
          {(current?.state === "creating" || !current.id) && (
            <ButtonComponent
              size="super-small"
              type="link"
              onClick={() => savePost(false)}
            >
              Terminar más tarde
            </ButtonComponent>
          )}
          <ButtonComponent
            size="super-small"
            type="primary"
            onClick={() => savePost(true)}
            style={{ padding: "5px 70px" }}
          >
            {current?.id && current.state !== "creating"
              ? "Editar"
              : "Publicar"}
          </ButtonComponent>
        </div>
      </div>
    </>
  );
};
