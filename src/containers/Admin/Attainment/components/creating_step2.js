import React from "react";
import WYSIWYGCOMPONENET from "@components/WYSIWYG";
import ButtonComponent from "@components/Button";

// import { fetchFiles } from "@utils/fetch_custom";
import draftToHtml from "draftjs-to-html";

export const CreatingStep2 = ({
  current = {},
  setCreatingStep,
  setCurrent,
  validate,
}) => {
  const handleChange = (e) => {
    if (e) setCurrent({ ...current, content: e });
  };

  const handleOnClick = () => {
    setCreatingStep(2);
  };

  const getTextLength = (text) => {
    if (typeof text !== "string") text = draftToHtml(text);

    let parsed = text.replace(/(\n|&nbsp;)/g, " ");
    parsed = parsed.replace(/(<\/?.{1,10}> | &lt; | &gt;)/g, "");

    return parsed.length;
  };

  return (
    <div className="flex flex-col gap-3 p-2">
      <span className="font-bold text-H6">
        Redacta una breve introducción del Logro{" "}
      </span>
      <span className="text-bodyMedium mb-2">
        Las introducciones más exitosas suelen tener 3 párrafos. Te recomendamos
        que agregues aproximadamente <span className="text-redDart">1000</span>{" "}
        caracteres
      </span>
      <WYSIWYGCOMPONENET
        onChange={handleChange}
        row={5}
        value={current?.content || {}}
        minCharacter={299}
        rawContentState={current?.content || {}}
      />
      {validate === "all" && getTextLength(current?.content) < 300 && (
        <span className="text-red text-callout">
          Es necesario añadir una descripción de por lo menos 300 caracteres
        </span>
      )}
      <div className="flex items-center justify-center">
        <ButtonComponent size="small-x" onClick={handleOnClick}>
          <span className="text-bodyMedium">Siguiente</span>
        </ButtonComponent>
      </div>
    </div>
  );
};
