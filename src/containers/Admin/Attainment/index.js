import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ButtonComponent from "@components/Button";
import ErrorModal from "@components/ErrorModal";
import Spin from "@components/Spin";

import Wrapped from "@containers/Wrapper/WrapperLogin";

import { BodySection } from "./components/admin_BodySection";
import Pagination from "@components/Pagination";

import { CreatingHeader } from "./components/creating_header";
import { CreatingStep1 } from "./components/creating_step1";
import { CreatingStep2 } from "./components/creating_step2";
import { CreatingStep5 } from "./components/creating_step5";

import Styles from "./adminVooting.module.scss";
import message from "@components/Message";
import StepBar from "@components/Stepper";

import { closeErrorModal } from "@redux/admin_voting/actions";

import {
  getAttainmentsStart,
  showHideCreatingSection,
  postAttainment,
  editAttainment,
} from "@redux/attainments/actions";
import AutoCompleteComponent from "@components/Autocomplete";
import SelectComponent from "@components/Select";

const optionsSelect = [
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más Antiguas" },
];

const optionsSelect2 = [
  { value: "null", label: "Todas" },
  { value: "PUBLISHED", label: "Activas" },
  { value: "DISABLED", label: "Ocultadas" },
  { value: "CREATING", label: "En creación" },
];

let searchValue = "";
let withCount = true;
const AdminVotingContainer = () => {
  const dispatch = useDispatch();
  const { error } = useSelector((state) => state.adminVoting);
  const [order, setOrder] = useState("DESC");
  const [state, setState] = useState("null");
  const [currentPage, setCurrentPage] = useState(1);
  const [seleted, setSeleted] = useState();
  const [loading, setLoading] = useState(false);
  const { datas, creating, count } = useSelector((state) => state.attainment);

  useEffect(() => {
    getInitialData();
  }, [order, state, currentPage]);

  const getInitialData = async () => {
    try {
      setLoading(true);
      console.log(order);

      console.log(searchValue);
      const filterObj = {
        skip: (currentPage - 1) * 6,
        limit: 6,
        like: searchValue,
        state: state === "null" ? undefined : state,
        order,
        withCount,
      };

      await new Promise((resolve, reject) =>
        dispatch(getAttainmentsStart({ ...filterObj, resolve, reject }))
      );
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };

  const handleShowHideModal = () => {
    dispatch(showHideCreatingSection(!creating));
  };

  const onSearch = () => {
    withCount = true;
    getInitialData();
  };

  return (
    <Wrapped
      seo="Logros - Alcaldía"
      defaultKey="attainments"
      routeName="Logros - Alcaldía"
    >
      {/* Filters */}
      <div className={Styles.controlsContainer}>
        <div className={Styles.controls}>
          <SelectComponent
            options={optionsSelect2}
            onChange={(e) => {
              withCount = true;
              setState(e[0].value);
            }}
            defaultValue={0}
          />
          <SelectComponent
            options={optionsSelect}
            onChange={(e) => {
              withCount = false;
              setOrder(e[0].value);
            }}
            defaultValue={0}
          />
          <AutoCompleteComponent
            dataSource={[]}
            placeholder={"Buscar"}
            onChange={(value) => {
              searchValue = value;
              onSearch();
            }}
          />
        </div>
        <div className="flex items-center justify-end w-2/5 mr-16">
          <ButtonComponent size="small" onClick={handleShowHideModal}>
            Crear Logro &#43;
          </ButtonComponent>
        </div>
      </div>
      {creating ? (
        <div className={Styles.rootModalCreating}>
          <AdminCreatingVotingScreen
            setSeleted={setSeleted}
            seleted={seleted}
          />
        </div>
      ) : (
        <>
          <Spin spinning={loading} size="large">
            <BodySection
              setLoading={setLoading}
              setSeleted={setSeleted}
              datas={datas}
            />
          </Spin>
          <div className="mt-3 mb-5">
            <Pagination
              total={count}
              current={currentPage}
              onChange={(page) => {
                withCount = false;
                setCurrentPage(page);
              }}
            />
          </div>
        </>
      )}
      {error && (
        <ErrorModal
          Message={error}
          closeModal={() => dispatch(closeErrorModal())}
        />
      )}
    </Wrapped>
  );
};

const AdminCreatingVotingScreen = ({ seleted = {}, setSeleted }) => {
  const [creatingStep, setCreatingStep] = useState(0);
  const [validate, setValidate] = useState("");
  const [current, setCurrent] = useState(seleted);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    try {
      const content = JSON.parse(seleted.content);
      setCurrent({ ...current, content });
    } catch {}

    const html = document.querySelector("html");
    if (html) html.style.overflow = "hidden";
    return () => {
      if (html) html.style.overflow = "auto";
      setSeleted({});
    };
  }, []);

  const setStep = (e) => {
    setCreatingStep(e);
  };

  const saveDataOnServer = async (completed) => {
    try {
      setLoading(true);

      const body = {
        ...current,
        state: completed ? "PUBLISHED" : undefined,
        content: JSON.stringify(current.content),
      };

      await new Promise((resolve, reject) => {
        if (current.id) dispatch(editAttainment({ ...body, resolve, reject }));
        else dispatch(postAttainment({ ...body, resolve, reject }));
      });
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
    dispatch(showHideCreatingSection(false));
  };

  return (
    <>
      <div className="flex justify-center">
        <div
          style={{ maxWidth: 900 }}
          className={`w-4/5 bg-white px-2 flex flex-col gap-4`}
        >
          <div
            style={{
              position: "sticky",
              top: "-18rem",
              zIndex: 2,
              width: "100%",
              padding: "10rem 30rem",
              backgroundColor: "#fff",
            }}
          >
            <CreatingHeader
              old={seleted}
              current={current}
              onSaveData={saveDataOnServer}
              loading={loading}
            />
            <div className="mt-2 place-self-center items-center">
              <StepBar
                step={creatingStep}
                setStep={setStep}
                steps={[1, 2, 3]}
              />
            </div>
          </div>
          <Spin spinning={loading} size="large">
            <div
              className={`${
                creatingStep !== 2 ? "border-2 border-greyheader" : ""
              }  rounded-sm overflow-auto`}
            >
              {creatingStep === 0 && (
                <CreatingStep1
                  current={current}
                  validate={validate}
                  setCurrent={setCurrent}
                  setCreatingStep={setCreatingStep}
                />
              )}
              {creatingStep === 1 && (
                <CreatingStep2
                  current={current}
                  validate={validate}
                  setCurrent={setCurrent}
                  setCreatingStep={setCreatingStep}
                />
              )}
              {creatingStep === 2 && (
                <CreatingStep5
                  current={current}
                  onSaveData={saveDataOnServer}
                  setValidate={setValidate}
                  setCreatingStep={setCreatingStep}
                />
              )}
            </div>
          </Spin>
        </div>
      </div>
    </>
  );
};

export default AdminVotingContainer;
