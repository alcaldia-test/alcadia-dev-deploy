import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Wrapped from "@containers/Wrapper/WrapperLogin";
import ButtonComponent from "@components/Button";
import Styles from "./adminVooting.module.scss";
import draftToHtml from "draftjs-to-html";
import Spin from "@components/Spin";
import message from "@components/Message";
import { AttainementServices } from "@services/attainements";

const Attainment = ({ id }) => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (id) getInitialData();
  }, [id]);

  const getInitialData = async () => {
    const attaiment = new AttainementServices();
    try {
      setLoading(true);

      const resp = await attaiment.getAttainement(id);
      console.log(resp);
      setData(resp);
    } catch (error) {
      console.log(error);
      message.error("Problemas con el servidor");
    }

    setLoading(false);
  };

  return (
    <Wrapped seo={data?.title}>
      <Spin spinning={loading} size="large">
        <ActivePost activePost={data} />
      </Spin>
    </Wrapped>
  );
};

const ActivePost = ({ activePost }) => {
  const router = useRouter();

  const [imageUrl, setImageUrl] = useState("/images/banner.jpg");

  const [htmlContent, setHtmlContent] = useState("");

  const [textContent, setTextContent] = useState("");

  useEffect(() => {
    setImageUrl(
      activePost?.fileLink ? activePost?.fileLink : "/images/banner.jpg"
    );
    try {
      setHtmlContent(JSON.parse(activePost?.content));
      setTextContent("");
    } catch (error) {
      setHtmlContent("");
      setTextContent(activePost?.content);
    }
  }, [activePost]);

  if (activePost) {
    return (
      <>
        <div className={Styles.voteContainer}>
          {/* Button */}
          <div className=" h-5  flex justify-between">
            <ButtonComponent
              size="super-small"
              type="secondary"
              onClick={() => {
                router.back();
              }}
            >
              <div className="flex">
                <span className="text-callout">{"< Volver"}</span>
              </div>
            </ButtonComponent>
          </div>

          <div className="font-bold text-H3 mb-3 font-body mt-4 text-center">
            <span>{activePost?.title}</span>
          </div>
          <div className={`${Styles.listCardContainer} text-center`}>
            <span role={`status-${activePost.state}`}>{activePost.tag}</span>
          </div>

          <div className="grid justify-items-center mt-40">
            <img
              src={imageUrl}
              className="max-w-500 max-h-500 object-contain"
              alt="alter"
            />
          </div>
          {htmlContent !== "" ? (
            <div
              className="text-bodyMedium pb-3 leading-relaxed mt-40 text-justify mb-16 text-textColor4"
              dangerouslySetInnerHTML={{ __html: draftToHtml(htmlContent) }}
            />
          ) : (
            <div className="text-bodyMedium pb-3 leading-relaxed mt-40 text-justify mb-16 text-textColor4">
              {textContent}
            </div>
          )}
        </div>
      </>
    );
  }

  return <div></div>;
};

export default Attainment;
