import {
  DELETE_DIFFICULTY_START,
  DELETE_DIFFICULTY_SUCCESS,
  REQUEST_DIFFICULTY_START,
  REQUEST_DIFFICULTY_SUCCESS,
  LOADING_DIFFICULTY_HIDE,
  LOADING_DIFFICULTY_SHOW,
  REGISTER_DIFFICULTY_START,
  REGISTER_DIFFICULTY_SUCCESS,
  UPDATE_DIFFICULTY_START,
  UPDATE_DIFFICULTY_SUCCESS,
} from "./constants";

export const requestDifficultyStart = (payload = {}) => ({
  type: REQUEST_DIFFICULTY_START,
  payload,
});
export const requestDifficultySuccess = (payload = {}) => ({
  type: REQUEST_DIFFICULTY_SUCCESS,
  payload,
});

export const registerDifficultyStart = (payload = {}) => ({
  type: REGISTER_DIFFICULTY_START,
  payload,
});
export const registerDifficultySuccess = (payload = {}) => ({
  type: REGISTER_DIFFICULTY_SUCCESS,
  payload,
});

export const deleteDifficultyStart = (payload = {}) => ({
  type: DELETE_DIFFICULTY_START,
  payload,
});
export const deleteDifficultySuccess = (payload = {}) => ({
  type: DELETE_DIFFICULTY_SUCCESS,
  payload,
});

export const updateDifficultyStart = (payload = {}) => ({
  type: UPDATE_DIFFICULTY_START,
  payload,
});
export const updateDifficultySuccess = (payload = {}) => ({
  type: UPDATE_DIFFICULTY_SUCCESS,
  payload,
});

export const loadingDifficultyShow = (payload = {}) => ({
  type: LOADING_DIFFICULTY_SHOW,
  payload,
});
export const loadingDifficultyHide = (payload = {}) => ({
  type: LOADING_DIFFICULTY_HIDE,
  payload,
});
