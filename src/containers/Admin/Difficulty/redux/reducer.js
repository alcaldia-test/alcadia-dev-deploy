import {
  DELETE_DIFFICULTY_SUCCESS,
  REQUEST_DIFFICULTY_SUCCESS,
  LOADING_DIFFICULTY_HIDE,
  LOADING_DIFFICULTY_SHOW,
  REGISTER_DIFFICULTY_SUCCESS,
  UPDATE_DIFFICULTY_SUCCESS,
} from "./constants";

const initialState = {
  data: [],
  filter: {},
  loading: false,
};

export function difficultyReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_DIFFICULTY_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        filter: action.payload.filter ?? {},
      };
    case REGISTER_DIFFICULTY_SUCCESS:
      return { ...state, data: [...state.data, action.payload] };
    case DELETE_DIFFICULTY_SUCCESS:
      return {
        ...state,
        data: state.data.filter((values) => values.id !== action.payload.id),
      };
    case UPDATE_DIFFICULTY_SUCCESS:
      return {
        ...state,
        data: state.data.map((values) => {
          return values.id === action.payload.id
            ? { ...values, ...action.payload.values }
            : values;
        }),
      };
    case LOADING_DIFFICULTY_SHOW:
      return { ...state, loading: true };
    case LOADING_DIFFICULTY_HIDE:
      return { ...state, loading: false };
    default:
      return state;
  }
}
