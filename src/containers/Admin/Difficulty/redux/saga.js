import { put, takeLatest, call } from "redux-saga/effects";
import request, {
  deleteOptions,
  patchOptions,
  postOptions,
  showMessageError,
} from "@utils/request";

import {
  DELETE_DIFFICULTY_START,
  REQUEST_DIFFICULTY_START,
  REGISTER_DIFFICULTY_START,
  UPDATE_DIFFICULTY_START,
} from "./constants";
import {
  loadingDifficultyShow as showLoader,
  loadingDifficultyHide as hideLoader,
  requestDifficultySuccess,
  updateDifficultySuccess,
  registerDifficultySuccess,
  deleteDifficultySuccess,
} from "./actions";
import { message } from "antd";
import { DifficultyServices } from "@services/difficulty";

export function* requestDifficulty({
  payload: { resolve, reject, ...payload },
}) {
  const difficultyServices = new DifficultyServices();
  try {
    yield !resolve && put(showLoader());
    const _requestDifficulty = yield difficultyServices.getDifficulty();
    yield put(requestDifficultySuccess({ data: _requestDifficulty }));
  } catch (err) {
    yield showMessageError(err);
    yield !reject && call(reject, "Al parecer hubo un error");
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* registerDifficulty({
  payload: { resolve, reject, ...payload },
}) {
  console.log(payload);
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/difficulty`;
    const options = yield postOptions(payload);
    const registerDifficulty = yield call(request, url, options);

    yield put(registerDifficultySuccess(registerDifficulty));

    yield call(resolve || message.success, "Se registró con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo registrar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* updateDifficulty({
  payload: { resolve, reject, id, ...payload },
}) {
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/difficulty/${id}`;
    const options = yield patchOptions(payload);
    yield call(request, url, options);

    yield put(updateDifficultySuccess({ id, values: payload }));
    yield call(resolve || message.success, "Se modificó con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo modificar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* deleteDifficulty({ payload: { resolve, reject, id } }) {
  try {
    yield !resolve && put(showLoader());

    const url = `${process.env.NEXT_PUBLIC_URL_API}/difficulty/${id}`;
    const options = yield deleteOptions();
    yield call(request, url, options);

    yield put(deleteDifficultySuccess({ id }));

    yield call(resolve || message.success, "Se eliminó con éxito");
  } catch (err) {
    yield showMessageError(err);
    yield call(
      reject || message.error,
      "Al parecer, no se pudo eliminar en la base de datos"
    );
  } finally {
    yield !resolve && put(hideLoader());
  }
}

export function* difficultySaga() {
  yield takeLatest(REQUEST_DIFFICULTY_START, requestDifficulty);
  yield takeLatest(REGISTER_DIFFICULTY_START, registerDifficulty);
  yield takeLatest(DELETE_DIFFICULTY_START, deleteDifficulty);
  yield takeLatest(UPDATE_DIFFICULTY_START, updateDifficulty);
}
