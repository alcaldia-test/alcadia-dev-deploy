import React, { useState, useReducer, useRef } from "react";
import Image from "next/image";

import Button from "@components/Button";
import { loginAdminStart } from "@redux/auth/actions";
import { useDispatch, useSelector } from "react-redux";
import InputField from "@components/InputField";
import Link from "next/link";
import styles from "./Login.module.scss";
import Spin from "@components/Spin";
import message from "@components/Message";
import Router from "next/router";
import ReCAPTCHA from "react-google-recaptcha";

const formReducer = (state, event) => {
  return {
    ...state,
    [event.name]: event.value.trim(),
  };
};

export function IngresoAdmin() {
  const { common } = useSelector((store) => store);

  const [docValue, setDocValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [passwordValue, setpasswordValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [generalAuth, setGeneralAuth] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [stateCaptcha, setStateCaptcha] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const captcha = useRef(null);
  const dispatch = useDispatch();
  const [formData, setFormData] = useReducer(formReducer, {});

  const handleSubmit = (event) => {
    event.preventDefault();
    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordVerificated = /^.{4,44}$/;

    console.log(formData);

    if (formData.identityCard === undefined) {
      setDocValue({
        ...docValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!emailVerificated.test(formData.identityCard)) {
      setDocValue({
        ...docValue,
        text: "Debe ingresar un correo electrónico válido",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }

    if (formData.password === undefined) {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!passwordVerificated.test(formData.password)) {
      setpasswordValue({
        ...passwordValue,
        text: "La contraseña debe tener Mínimo cuatro caracteres.",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }

    if (!stateCaptcha.status) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "Debe validar el captcha",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }

    const { identityCard, password } = formData;

    if (docValue.status && passwordValue.status && stateCaptcha.status) {
      return new Promise((resolve, reject) => {
        dispatch(
          loginAdminStart({
            email: identityCard,
            password: password,
            resolve,
            reject,
          })
        );
      })
        .then((res) => {
          message.success(res);
          Router.replace("/admin/votaciones");
        })
        .catch((res) => {
          if (res.includes("405"))
            setGeneralAuth({
              text: "La cuenta aún no fue aceptada",
              colorText: "#FE4752",
              colorInput: "#FE4752",
            });
          else if (res.includes("403"))
            setGeneralAuth({
              text: "Lo sentimos, la cuenta fue rechazada",
              colorText: "#FE4752",
              colorInput: "#FE4752",
            });
          else
            setGeneralAuth({
              text: "Correo electrónico o contraseña incorrecto",
              colorText: "#FE4752",
              colorInput: "#FE4752",
            });

          setDocValue({
            ...docValue,
            text: "",
            colorInput: "#FE4752",
            status: true,
          });
          setpasswordValue({
            ...passwordValue,
            text: "",
            colorInput: "#FE4752",
            status: true,
          });
        });
    }
  };

  const handleChange = (event) => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };
  const changeDocument = (e) => {
    handleChange(e);
    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    handleChange(e);
    if (e.target.value === "") {
      setDocValue({
        ...docValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (emailVerificated.test(e.target.value)) {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const changePassword = (e) => {
    const passwordVerificated = /^.{4,44}$/;

    handleChange(e);
    if (e.target.value === "") {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (passwordVerificated.test(e.target.value)) {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const changeCaptcha = (e) => {
    if (captcha.current.getValue()) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  return (
    <>
      <div className="flex w-full  flex-col md:flex-row h-full justify-around md:justify-center">
        <div className="relative  w-full lg:w-2/5 flex-none h-111 lg:h-full md:hidden lg:block bg-white rounded-tl-sm rounded-bl-sm	">
          <div
            className="absolute top-25 lg:top-0 left-3  xl:left-7 h-full"
            style={{ width: "350rem" }}
          >
            <Image src="/images/logo.svg" layout="fill" />
          </div>
        </div>
        <div className="flex flex-col justify-start lg:justify-center px-3 lg:px-0 py-5 w-full h-4/5 lg:h-full lg:pr-2 lg:pl-2 bg-white rounded-tr-sm rounded-br-sm ">
          <p
            className="font-bold text-bodyMedium text-center"
            style={{
              color: "#198565",
            }}
          >
            Inicio de sesión de administradores de
          </p>
          <span
            className=" text-center mb-2 "
            style={{
              color: "#00B3BA",
              fontWeight: "700",
            }}
          >
            La Paz decide
          </span>

          <p
            className={`text-center font-bold mb-2 mt-1  ${styles.alert_color}`}
          >
            {generalAuth.text}
          </p>
          <Spin spinning={common.loader}>
            <form>
              <InputField
                rightIcon="/images/icon/mail-line.svg"
                type="text"
                name="identityCard"
                label="Correo Electrónico"
                onChange={changeDocument}
                value={docValue.value}
                colorInput={docValue.colorInput}
                colorText={docValue.colorText}
                color=""
              />
              <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                {docValue.text}
              </p>
              <InputField
                rightIcon="/images/icon/lock-2-line.svg"
                type="password"
                name="password"
                label="Contraseña"
                onChange={changePassword}
                value={passwordValue.value}
                colorInput={passwordValue.colorInput}
                colorText={passwordValue.colorText}
              />{" "}
              <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                {passwordValue.text}
              </p>
              <div className="flex justify-center mt-4 items-center flex-col ">
                <ReCAPTCHA
                  ref={captcha}
                  sitekey="6Leif5MfAAAAAFzqx5As7eERTVTvx_TfUH6Zh2HA"
                  onChange={changeCaptcha}
                />
                <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                  {stateCaptcha.text}
                </p>
              </div>
              <Link href="/recovery-password-admin">
                <p
                  className={` text-left text-lg font-bold py-2 ml-2 cursor-pointer ${styles.blue_color_subtitle_line}`}
                >
                  No recuerdo mi contraseña
                </p>
              </Link>
              <div className="flex flex-col justify-center items-center pb-4">
                <Button
                  className="w-full"
                  onClick={handleSubmit}
                  size="big"
                  type="primary"
                  style={{ height: "56rem" }}
                >
                  Iniciar Sesión
                </Button>
              </div>
            </form>
          </Spin>
          <div className="flex flex-col justify-center items-center pb-1">
            <Link href="/registro-admin">
              <p
                className="font-semibold cursor-pointer text-lg"
                style={{
                  color: "#00B3BA",
                  lineHeight: "24px",
                }}
              >
                No tengo cuenta como administrador
              </p>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
export default IngresoAdmin;
