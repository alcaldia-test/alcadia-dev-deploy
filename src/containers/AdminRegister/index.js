import React, { useReducer, useState, useRef } from "react";
import Button from "@components/Button";
import styles from "./adminRegister.module.scss";
import { useDispatch, useSelector } from "react-redux";
import InputField from "@components/InputField";
// eslint-disable-next-line no-unused-vars
import { useForm, Controller } from "react-hook-form";
import { registerAdminStart } from "@redux/auth/actions";
import Link from "next/link";
import Image from "next/image";
import Spin from "@components/Spin";
import message from "@components/Message";
import Router from "next/router";
import ReCAPTCHA from "react-google-recaptcha";

const AdminRegister = () => {
  const { common } = useSelector((store) => store);
  const dispatch = useDispatch();
  const [docValue, setDocValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [emailValue, setemailValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [nameValue, setNameValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [passwordValue, setpasswordValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [stateCaptcha, setStateCaptcha] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const captcha = useRef(null);

  const formReducer = (state, event) => {
    return {
      ...state,
      [event.name]: event.value,
    };
  };
  // eslint-disable-next-line no-unused-vars
  const { register, handleSubmit, control } = useForm();
  const onSubmit = (data) => console.log(data);
  const [formData, setFormData] = useReducer(formReducer, {});
  const handleChange = (event) => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };
  const [generalAuth, setGeneralAuth] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const enviarDatos = (e) => {
    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordVerificated =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)([A-Za-z\d$@$!%*?&]|[^ ]){8,44}$/;

    const nameVerificy = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]{3,50}$/g;
    formData.name = nameValue.value;
    if (formData.secretKey === undefined) {
      setDocValue({
        ...docValue,
        text: "Debe ingresar la palabra secreta compartida",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.email === undefined) {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!emailVerificated.test(formData.email)) {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar un correo electrónico válido",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.password === undefined) {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!passwordVerificated.test(formData.password)) {
      setpasswordValue({
        ...passwordValue,
        text: "La contraseña debe tener Mínimo ocho caracteres, al menos una letra mayúscula, una letra minúscula y un número",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.name === undefined) {
      setNameValue({
        ...nameValue,
        text: "Debe ingresar su nombre completo",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!nameVerificy.test(formData.name)) {
      setNameValue({
        ...nameValue,
        text: "Debe ingresar Nombre y Apellido válidos",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (!stateCaptcha.status) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "Debe validar el captcha",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (
      docValue.status &&
      emailValue.status &&
      nameValue.status &&
      passwordValue.status &&
      stateCaptcha.status
    ) {
      return new Promise((resolve, reject) => {
        dispatch(registerAdminStart({ resolve, reject, formData }));
      })
        .then((res) => {
          message.success(res);
          Router.replace("/registro-admin/permission");
        })
        .catch((res) => {
          console.log(res);
          if (res.includes("406")) {
            setemailValue({
              ...emailValue,
              text: `Este correo ya está registrado`,
              colorText: "#FE4752",
              colorInput: "#FE4752",
              status: false,
            });
          } else if (res.includes("405")) {
            setGeneralAuth({
              text: "Palabra clave incorrecta",
              colorText: "#FE4752",
              colorInput: "#FE4752",
              status: false,
            });
          } else {
            setGeneralAuth({
              text: "No se pudo registrar el usuario",
              colorText: "#FE4752",
              colorInput: "#FE4752",
              status: false,
            });
          }
        });
    }
  };
  const changeDocument = (e) => {
    handleChange(e);
    if (e.target.value === "") {
      setDocValue({
        ...docValue,
        text: "Debe ingresar su palabra secreta",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  const changeEmail = (e) => {
    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    handleChange(e);
    if (e.target.value === "") {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (emailVerificated.test(e.target.value)) {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const changeName = (e) => {
    const nameVerificy = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]{3,50}$/g;

    handleChange(e);
    if (e.target.value === "") {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "Debe ingresar su nombre completo",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (nameVerificy.test(e.target.value)) {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const changePassword = (e) => {
    const passwordVerificated =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)([A-Za-z\d$@$!%*?&]|[^ ]){8,44}$/;
    handleChange(e);
    if (e.target.value === "") {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (passwordVerificated.test(e.target.value)) {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const changeCaptcha = (e) => {
    if (captcha.current.getValue()) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  return (
    <>
      <div className="flex w-full flex-col md:flex-row h-full justify-around md:justify-center">
        <div className="relative w-2/5	flex-none h-full hidden	lg:block bg-white rounded-tl-sm rounded-bl-sm">
          <div
            className="absolute top-0 left-3 xl:left-7 h-full"
            style={{ width: "350rem" }}
          >
            <Image src="/images/logo.svg" layout="fill" />
          </div>
        </div>
        <div
          className="flex-none w-full md:w-3/5 bg-white flex justify-center rounded-tr-sm rounded-br-sm"
          style={{ padding: "32px 0 10px 0" }}
        >
          <div
            className="flex flex-col justify-center pb-3 w-full"
            style={{ paddingBottom: "20px" }}
          >
            <p
              className="font-semibold"
              style={{
                color: "#198565",
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              Registro para administradores de
            </p>
            <span
              style={{
                color: "#00B3BA",
                textAlign: "center",
                fontWeight: "bold",
                marginBottom: "20px",
              }}
            >
              La Paz decide
            </span>
            <p
              className={`text-center font-bold mb-2 mt-1  ${styles.alert_color}`}
            >
              {generalAuth.text}
            </p>
            <Spin spinning={common.loader}>
              <form className="mx-3 flex-1" onSubmit={handleSubmit(onSubmit)}>
                <InputField
                  rightIcon="/images/icon/user-line.svg"
                  type="text"
                  name="name"
                  label="Nombre Completo"
                  onChange={changeName}
                  value={nameValue.value}
                  colorInput={nameValue.colorInput}
                  colorText={nameValue.colorText}
                />{" "}
                <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                  {nameValue.text}
                </p>
                <InputField
                  rightIcon="/images/icon/mail-line.svg"
                  type="email"
                  name="email"
                  label="Correo Electrónico"
                  onChange={changeEmail}
                  value={emailValue.value}
                  colorInput={emailValue.colorInput}
                  colorText={emailValue.colorText}
                />{" "}
                <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                  {emailValue.text}
                </p>
                <InputField
                  rightIcon="/images/icon/lock-2-line.svg"
                  type="password"
                  name="password"
                  label="Contraseña"
                  onChange={changePassword}
                  value={passwordValue.value}
                  colorInput={passwordValue.colorInput}
                  colorText={passwordValue.colorText}
                />{" "}
                <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                  {passwordValue.text}
                </p>
                <InputField
                  rightIcon="/images/icon/lock-2-line.svg"
                  type="text"
                  name="secretKey"
                  label="Palabra secreta"
                  onChange={changeDocument}
                  value={docValue.value}
                  colorInput={docValue.colorInput}
                  colorText={docValue.colorText}
                  color=""
                />
                <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                  {docValue.text}
                </p>
                <div className="flex justify-center mt-4 items-center flex-col ">
                  <ReCAPTCHA
                    ref={captcha}
                    sitekey="6Leif5MfAAAAAFzqx5As7eERTVTvx_TfUH6Zh2HA"
                    onChange={changeCaptcha}
                  />
                  <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                    {stateCaptcha.text}
                  </p>
                </div>
                <div className="flex flex-col justify-center items-center pb-5">
                  <Button
                    onClick={handleSubmit(enviarDatos)}
                    size="big"
                    type="primary"
                    style={{ width: "100%", height: "56px" }}
                  >
                    Registrarme
                  </Button>
                </div>
              </form>
            </Spin>
            <div
              className="flex flex-col justify-center items-center"
              style={{ paddingBottom: "4px" }}
            >
              <Link href={"/ingreso"}>
                <p
                  className="font-semibold text-base cursor-pointer"
                  style={{
                    color: "#00B3BA",
                    fontSize: "16px",
                    lineHeight: "24px",
                  }}
                >
                  Ya tengo cuenta de administrador
                </p>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default AdminRegister;

/* <img
            className="h-full w-full "
            src="/images/logo_login.jpg"
            style={{
              objectFit: "cover",
              borderTopLeftRadius: "12px",
              borderEndStartRadius: "12px",
            }}
          /> */
