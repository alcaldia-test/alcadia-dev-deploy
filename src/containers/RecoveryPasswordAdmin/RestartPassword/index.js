import React, { useState } from "react";
import style from "../Login.module.scss";
import InputField from "@components/InputField";
import Button from "@components/Button";
import { useDispatch, useSelector } from "react-redux";
import { resetPasswordStart } from "@redux/recoveyPassword/actions";
import message from "@components/Message";
import Spin from "@components/Spin";
import Router from "next/router";

const RestartPassword = () => {
  const dispatch = useDispatch();
  const { common, recoveyPassword } = useSelector((store) => store);
  const [passwordValue, setpasswordValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [password2Value, setpassword2Value] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [generalCode, setGeneralCode] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [password, setpassword] = useState("");
  const changePassword = (e) => {
    setpassword(e.target.value);
    const passwordVerificated =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)([A-Za-z\d$@$!%*?&]|[^ ]){8,44}$/;
    if (e.target.value === "") {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (passwordVerificated.test(e.target.value)) {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const enviarDatos = (e) => {
    e.preventDefault();
    const passwordVerificated =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)([A-Za-z\d$@$!%*?&]|[^ ]){8,44}$/;
    if (password === "") {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!passwordVerificated.test(password)) {
      setpasswordValue({
        ...passwordValue,
        text: "La contraseña debe tener Mínimo ocho caracteres, al menos una letra mayúscula, una letra minúscula y un número",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (!(password2Value.value === password)) {
      setpassword2Value({
        ...password2Value,
        text: "La contraseñas deben coincidir",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (passwordValue.status && password2Value.status) {
      return new Promise((resolve, reject) => {
        dispatch(
          resetPasswordStart({
            newPassword: password,
            code: recoveyPassword.password,
            userId: recoveyPassword?.userId,
            resolve,
            reject,
          })
        );
      })
        .then((res) => {
          message.success(res);
          Router.replace("/ingreso");
        })
        .catch((res) => {
          setGeneralCode({
            text: res,
            colorText: "#FE4752",
            colorInput: "#FE4752",
          });
        });
    }
  };
  const changePassword2 = (e) => {
    if (e.target.value === "") {
      setpassword2Value({
        ...password2Value,
        value: e.target.value,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setpassword2Value({
        ...password2Value,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
      });
    }
    if (e.target.value === password) {
      setpassword2Value({
        ...password2Value,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setpassword2Value({
        ...password2Value,
        value: e.target.value,
        status: false,
      });
    }
  };
  return (
    <div className="flex flex-col align-middle justify-center w-full md:w-500 ">
      <h1
        className={`text-center font-bold text-2xl mb-5 mt-30 ${style.green_color_title}`}
      >
        Ingrese su nueva contraseña
      </h1>
      <p className={`text-center font-bold text-lg mb-2 ${style.opp_color}`}>
        Introduzca una nueva contraseña para su cuenta
      </p>
      <p className={`text-center font-bold  mb-5  ${style.alert_color}`}>
        {generalCode.text}
      </p>
      <Spin spinning={common.loader}>
        <InputField
          rightIcon="/images/icon/lock-2-line.svg"
          type="password"
          name="password"
          label="Contraseña"
          onChange={changePassword}
          value={passwordValue.value}
          colorInput={passwordValue.colorInput}
          colorText={passwordValue.colorText}
        />{" "}
        <p className={` mb-2 mt-1  ${style.alert_color}`}>
          {passwordValue.text}
        </p>
        <InputField
          rightIcon="/images/icon/lock-2-line.svg"
          type="password"
          name="password"
          label="Contraseña"
          onChange={changePassword2}
          value={password2Value.value}
          colorInput={password2Value.colorInput}
          colorText={password2Value.colorText}
        />{" "}
        <p className={` mb-2 mt-1  ${style.alert_color}`}>
          {password2Value.text}
        </p>
        <Button
          className=" mt-5 mb-4"
          /* disabled={valueChek} */
          onClick={enviarDatos}
          size="big"
          type="primary"
          style={{ width: "100%", height: "56rem" }}
        >
          Cambiar contraseña
        </Button>
      </Spin>
    </div>
  );
};
export default RestartPassword;
