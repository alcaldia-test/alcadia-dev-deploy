import React, { useState } from "react";
import style from "./Login.module.scss";
import InputField from "@components/InputField";
import Button from "@components/Button";
import { useDispatch, useSelector } from "react-redux";
import { recoveryPasswordEmailStart } from "@redux/recoveyPassword/actions";
import message from "@components/Message";
import Spin from "@components/Spin";
import Router from "next/router";

const RecoveryPassword = () => {
  const dispatch = useDispatch();
  const { common } = useSelector((store) => store);
  const [emailValue, setemailValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [generalCode, setGeneralCode] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [email, setEmail] = useState("");
  const changeEmail = (e) => {
    setEmail(e.target.value);
    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    if (e.target.value === "") {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (emailVerificated.test(e.target.value)) {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const enviarDatos = (e) => {
    e.preventDefault();
    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    if (email === "") {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!emailVerificated.test(email)) {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar un correo electrónico válido",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (emailValue.status) {
      return new Promise((resolve, reject) => {
        dispatch(
          recoveryPasswordEmailStart({
            email: email,
            resolve,
            reject,
          })
        );
      })
        .then((res) => {
          message.success(res);
          Router.replace("/recovery-password/code");
        })
        .catch((res) => {
          setGeneralCode({
            text: res,
            colorText: "#FE4752",
            colorInput: "#FE4752",
          });
        });
    }
  };
  return (
    <div className="flex flex-col align-middle justify-center w-full md:w-500 ">
      <h1
        className={`text-center font-bold text-2xl mb-5 mt-30 ${style.green_color_title}`}
      >
        ¿Olvidaste tu contraseña?
      </h1>
      <p className={`text-center font-bold text-lg mb-2 ${style.opp_color}`}>
        No te preocupes, recuperarla es fácil, solo tienes que ingresar tu
        correo electrónico con el que ingresaste en LaPazDecide
      </p>
      <p className={`text-center font-bold  mb-5  ${style.alert_color}`}>
        {generalCode.text}
      </p>
      <Spin spinning={common.loader}>
        <InputField
          rightIcon="/images/icon/mail-line.svg"
          type="email"
          name="email"
          label="Correo electrónico"
          onChange={changeEmail}
          value={emailValue.value}
          colorInput={emailValue.colorInput}
          colorText={emailValue.colorText}
        />{" "}
        <p className={` mb-2 mt-1  ${style.alert_color}`}>{emailValue.text}</p>
        <Button
          className=" mt-5 mb-4"
          /* disabled={valueChek} */
          onClick={enviarDatos}
          size="big"
          type="primary"
          style={{ width: "100%", height: "56rem" }}
        >
          Siguiente
        </Button>
      </Spin>
    </div>
  );
};
export default RecoveryPassword;
