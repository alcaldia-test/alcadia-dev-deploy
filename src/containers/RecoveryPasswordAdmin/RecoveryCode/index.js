import Button from "@components/Button";
import Code from "@components/Code";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "../Login.module.scss";
import Spin from "@components/Spin";
import message from "@components/Message";
import Router from "next/router";
import {
  recoveryPasswordEmailStart,
  recoveryPasswordCodeStart,
} from "@redux/recoveyPassword/actions";

const RecoveryCode = () => {
  const { common, recoveyPassword } = useSelector((store) => store);
  const dispatch = useDispatch();
  const [codeValue, setCodeValue] = useState("");
  const [buttonValue, setButtonValue] = useState(true);
  const { userRegister } = useSelector((datos) => datos.auth);
  const [generalCode, setGeneralCode] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const codeSelect = (e) => {
    if (e.length === 4) {
      setButtonValue(false);
    } else {
      setButtonValue(true);
    }
    setCodeValue(e);
  };
  const sendCode = (e) => {
    e.preventDefault();
    return new Promise((resolve, reject) => {
      dispatch(
        recoveryPasswordCodeStart({
          resolve,
          reject,
          code: codeValue,
          id: recoveyPassword?.userId,
        })
      );
    })
      .then((res) => {
        message.success(res);
        Router.replace("/recovery-password/reset-password");
      })
      .catch((res) => {
        setGeneralCode({
          text: res,
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      });
  };
  const resendCode = () => {
    const email = recoveyPassword?.email;
    console.log(email);
    return new Promise((resolve, reject) => {
      dispatch(
        recoveryPasswordEmailStart({
          email: email,
          resolve,
          reject,
        })
      );
    })
      .then(() => {
        message.success("Se volvió a enviar su código de verificación");
      })
      .catch((res) => {
        setGeneralCode({
          text: res,
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      });
  };
  return (
    <div>
      <Spin spinning={common.loader}>
        <h2
          className={`text-center font-bold text-2xl mt-4 md:mt-7 px-1 ${styles.color_title}`}
        >
          Verifique su correo electrónico
        </h2>
        <p
          className={` text-lg font-bold text-center mt-7 px-3 ${styles.subtitle} `}
        >
          Hemos enviado un código de verificación a este correo electrónico:
        </p>
        <p
          className={`text-center font-bold text-lg mt-2 lg:mt-4 ${styles.user}`}
        >
          {userRegister?.email}
        </p>
        <p className={`text-center font-bold  my-5  ${styles.alert_color}`}>
          {generalCode.text}
        </p>
        <div className=" mt-2 ">
          <Code codeSize={4} autoFocus={true} handleInput={codeSelect} />
        </div>
        <p
          onClick={resendCode}
          className={` text-right font-bold text-lg px-3 mt-3 cursor-pointer ${styles.code_send}`}
        >
          Reenviar código
        </p>
        <div className="flex justify-center mt-7">
          <Button
            disabled={buttonValue}
            size={"large"}
            className="w-full lg:w-3/6"
            onClick={sendCode}
          >
            Siguiente
          </Button>
        </div>
      </Spin>
    </div>
  );
};
export default RecoveryCode;
