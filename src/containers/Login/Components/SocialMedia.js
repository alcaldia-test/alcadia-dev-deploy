import io from "socket.io-client";
import { Facebook, Google } from "@assets/icons";
import { loginDataStart } from "@redux/auth/actions";
import { useDispatch, useSelector } from "react-redux";
import Router from "next/router";
import styles from "../Login.module.scss";
import { useEffect, useMemo } from "react";
import { randomLetter } from "@utils/generator";

let currentWindow;

export default function SocialMedia({ setGeneralAuth }) {
  const { auth } = useSelector((store) => store);
  const dispatch = useDispatch();

  const room = useMemo(() => randomLetter(12).toLowerCase(), []);

  useEffect(() => {
    const socket = io(`${process.env.NEXT_PUBLIC_URL_API}`, {
      reconnect: true,
      transports: ["websocket"],
      extraHeaders: {
        header: "Access-Control-Allow-Origin: *",
        reconnectionAttempts: "Infinity",
        timeout: 10000,
      },
    });

    socket.on("connect", () => console.log("conneted to Auth Socket"));
    console.log(room);

    socket.on(`${room}_success`, onSuccessLogin);
    socket.on(`${room}_error`, onErrorLogin);

    socket.on("disconnect", function () {
      console.error(
        `SE PERDIO LA CONEXIÓN CON ${process.env.NEXT_PUBLIC_URL_API}`
      );

      socket.disconnect();
    });

    return () => socket.disconnect();
  }, [room]);

  const startLogin = (provider) => {
    console.log(room);
    currentWindow = window.open(
      `${process.env.NEXT_PUBLIC_URL_API}/auth/thirdparty/${provider}?room=${room}`,
      "_blank",
      "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=450,height=600"
    );

    currentWindow.focus();
  };

  const onSuccessLogin = (payload) => {
    currentWindow?.close();

    dispatch(loginDataStart(payload));
    console.log(payload);

    if (!payload?.token) Router.replace("/registro");
    else {
      if (!auth?.RouterLogin) Router.replace("/");
      else Router.replace(auth?.RouterLogin);
    }
  };

  const onErrorLogin = (payload) => {
    currentWindow?.close();
    let message = "Problemas con el servidor";

    if (payload?.code === 403)
      message = "La cuenta ingresada se encuentra bloqueada";

    setGeneralAuth({
      text: message,
      colorText: "#FE4752",
      colorInput: "#FE4752",
    });
  };

  return (
    <div className="flex flex-col justify-center items-center pb-3">
      <div>
        <p className={`font-bold text-lg ${styles.opp_color}`}>
          O iniciar sesión mediante:
        </p>
        <div className="w-full flex justify-around pt-3 ">
          <button
            onClick={() => startLogin("google")}
            className=" rounded-full p-3"
            style={{ backgroundColor: "#FF5832" }}
          >
            <Google />
          </button>
          <button
            onClick={() => startLogin("facebook")}
            className=" rounded-full p-3"
            style={{ backgroundColor: "#3B5998" }}
          >
            <Facebook color="#fff" />
          </button>
        </div>
      </div>
    </div>
  );
}
