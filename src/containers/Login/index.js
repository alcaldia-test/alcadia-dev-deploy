import React, { useState, useReducer, useEffect, useRef } from "react";
import Image from "next/image";
import Button from "@components/Button";
import { loginStart } from "@redux/auth/actions";
import { useDispatch, useSelector } from "react-redux";
import InputField from "@components/InputField";
import Link from "next/link";
import styles from "./Login.module.scss";
import Spin from "@components/Spin";
// import message from "@components/Message";
import Router from "next/router";
import { hideLoader } from "@redux/common/actions";
import ReCAPTCHA from "react-google-recaptcha";

import SocialMedia from "./Components/SocialMedia";

const formReducer = (state, event) => {
  return {
    ...state,
    [event.name]: event.value,
  };
};

export function Login() {
  const { common, auth } = useSelector((store) => store);

  const [docValue, setDocValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [passwordValue, setpasswordValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [generalAuth, setGeneralAuth] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [stateCaptcha, setStateCaptcha] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(hideLoader());
  }, []);
  const captcha = useRef(null);
  const [formData, setFormData] = useReducer(formReducer, {});
  const [submitting] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();

    if (formData.identityCard === undefined) {
      setDocValue({
        ...docValue,
        text: "Debe ingresar su número de documento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.password === undefined) {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (!stateCaptcha.status) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "Debe validar el captcha",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    const { identityCard, password } = formData;
    if (docValue.status && passwordValue.status && stateCaptcha.status) {
      return new Promise((resolve, reject) => {
        dispatch(
          loginStart({
            identityCard,
            password: password,
            resolve,
            reject,
          })
        );
      })
        .then((redirect) => {
          console.log(auth?.RouterLogin);
          if (auth?.RouterLogin) Router.replace(auth?.RouterLogin);
          else Router.replace(redirect);
        })
        .catch((res) => {
          if (res.includes("403")) {
            setGeneralAuth({
              text: "Esta cuenta se encuentra bloqueada, contáctese con un Administrador",
              colorText: "#FE4752",
              colorInput: "#FE4752",
            });
          } else {
            setGeneralAuth({
              text: "Usuario o contraseñas incorrectos",
              colorText: "#FE4752",
              colorInput: "#FE4752",
            });
          }
          setDocValue({
            ...docValue,
            text: "",
            colorInput: "#FE4752",
            status: true,
          });
          setpasswordValue({
            ...passwordValue,
            text: "",
            colorInput: "#FE4752",
            status: true,
          });
        });
    }
  };
  const handleChange = (event) => {
    setFormData({
      name: event.target.name,
      value: event.target.value.trim(),
    });
  };
  const changeDocument = (e) => {
    handleChange(e);
    if (e.target.value === "") {
      setDocValue({
        ...docValue,
        text: "Debe ingresar su número de documento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  const changePassword = (e) => {
    handleChange(e);
    if (e.target.value === "") {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };

  const changeCaptcha = (e) => {
    if (captcha.current.getValue()) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  return (
    <>
      <div className="flex flex-row justify-center items-center h-111 relative w-full">
        <div className="absolute top-0 left-0 lg:left-3 w-54 h-54 lg:w-111 lg:h-111">
          <Image src="/images/logo.svg" layout="fill" />
        </div>
        <div className="place-self-center	">
          <p className={`font-bold text-2xl ${styles.green_color_title}`}>
            Inicio de sesión {submitting}
          </p>
        </div>
        <div
          className={`absolute top-0 right-0 lg:right-5 lg:top-3 font-bold text-lg cursor-pointer ${styles.blue_color_subtitle_line}`}
        >
          <Link href={"/"}>
            <p>Ir al sitio Web</p>
          </Link>
        </div>
      </div>
      <div className="md:w-2/4">
        <div className="flex justify-center pb-3">
          <p className={`font-bold text-xl ${styles.green_color_subtitle}`}>
            ¿Ya estas registrado en el{" "}
            <a
              href="https://igob247.lapaz.bo"
              target="_blank"
              rel="noreferrer"
              className={` cursor-pointer ${styles.blue_color_subtitle}`}
            >
              iGOB?
            </a>
          </p>
        </div>
        <div className="py-4">
          <p className={`text-center font-bold ${styles.alert_color}`}>
            {generalAuth.text || docValue.text || stateCaptcha.text}
          </p>
          <Spin spinning={common.loader}>
            <form>
              <InputField
                rightIcon="/images/icon/file-user-line.svg"
                type="text"
                name="identityCard"
                label="Nro. de Documento"
                onChange={changeDocument}
                value={docValue.value}
                colorInput={docValue.colorInput}
                colorText={docValue.colorText}
                color=""
              />
              <p className={styles.caption}>C.I. con complemento: 1234-abc</p>
              {/* <p className={styles.alert_color}>{docValue.text}</p> */}
              <InputField
                rightIcon="/images/icon/lock-2-line.svg"
                type="password"
                name="password"
                label="Contraseña"
                onChange={changePassword}
                value={passwordValue.value}
                colorInput={passwordValue.colorInput}
                colorText={passwordValue.colorText}
              />{" "}
              {/* <p className={styles.alert_color}>{passwordValue.text}</p> */}
              <div className="flex justify-center my-4 items-center flex-col ">
                <ReCAPTCHA
                  ref={captcha}
                  sitekey="6Leif5MfAAAAAFzqx5As7eERTVTvx_TfUH6Zh2HA"
                  onChange={changeCaptcha}
                  hl="ES"
                />
                <p className={styles.alert_color}>{stateCaptcha.text}</p>
              </div>
              <div className="w-full flex justify-end">
                <Link href="recovery-password">
                  <p
                    className={`text-right text-lg font-bold py-4 cursor-pointer ${styles.blue_color_subtitle_line}`}
                  >
                    No recuerdo mi contraseña
                  </p>
                </Link>
              </div>
              <div className="flex flex-col justify-center items-center pb-4">
                <Button
                  className="w-full"
                  onClick={handleSubmit}
                  size="big"
                  type="primary"
                  style={{ height: "56rem" }}
                >
                  Iniciar Sesión
                </Button>
              </div>
            </form>
          </Spin>
        </div>
        <SocialMedia setGeneralAuth={setGeneralAuth} />
        <div className="flex flex-col justify-center items-center pb-1 ">
          <Link href={"/registro"}>
            <p
              className={`font-bold text-lg cursor-pointer ${styles.blue_color_subtitle_line}`}
            >
              No estoy registrado
            </p>
          </Link>
        </div>
      </div>
    </>
  );
}
export default Login;
