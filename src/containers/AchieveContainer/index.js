import { getAttainmentDetail } from "@redux/attainments/actions";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import styles from "./attainment.module.scss";
import Banner from "@components/Banner";
import Skeleton from "@components/Skeleton";
import { parseDraft, parseDraftToText } from "@utils/draftToHtml";
import SEO from "@components/SEO";

export default function AchieveContainer() {
  const { attainment } = useSelector((state) => state.attainment);
  const router = useRouter();
  const dispatch = useDispatch();
  const { fileLink, title, content } = attainment || {};
  useEffect(() => {
    dispatch(getAttainmentDetail({ id: router.query.id }));
  }, []);
  return (
    <section className={`${styles.attainment} text-center`}>
      <SEO
        image={fileLink}
        title={title}
        description={parseDraftToText(content)}
      />

      {attainment ? (
        <>
          <Banner image={fileLink} objectFit="cover" subtitle={title} />
          <p
            className="text-left"
            dangerouslySetInnerHTML={{
              __html: parseDraft(content),
            }}
          />
        </>
      ) : (
        <div className={styles.loading}>
          <Skeleton active />
        </div>
      )}
    </section>
  );
}
