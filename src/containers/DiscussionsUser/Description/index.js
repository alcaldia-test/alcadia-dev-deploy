import React, { useState } from "react";
import dynamic from "next/dynamic";
import Link from "next/link";
import Image from "next/image";
import { DownloadOutlined } from "@ant-design/icons";
import draftToHtml from "draftjs-to-html";
import Comments from "@components/Comments";
import Styles from "./charlasDescription.module.scss";
import Skeleton from "@components/Skeleton";
import { useSelector } from "react-redux";
import { SupportDiscussions } from "./supportDiscussions";
import SocialNetwork from "@components/SocialNetwork";
import ZonesModal from "@components/Modals/ZonesModal/MacrosModal";
import moment from "moment";
import "moment/locale/es";
import SEO from "@components/SEO";
import { parseDraftToText } from "@utils/draftToHtml";
const ListRelatedContent = dynamic(
  () => import("@components/ListRelatedContent"),
  {
    ssr: false,
  }
);

const adjustContent = (content) => {
  try {
    return JSON.parse(content);
  } catch (error) {
    return content;
  }
};

export const Description = ({ discussion, hostname }) => {
  const { dataUser } = useSelector((state) => state.auth);
  const { id, comments, content, createdAt, postsId, title, attached, macros } =
    discussion || {};

  console.warn("Macros tienen ID?", macros);

  const contentAdjusted = adjustContent(content);
  const date = moment(createdAt);
  const [ncomments, setNcomments] = useState(0);
  const [showZonesModal, setShowZonesModal] = useState(false);

  const MainContent = () => (
    <>
      <SEO title={title} description={parseDraftToText(content)} />

      <h2 className={`${Styles.main_title} font-bold tracking-normal `}>
        {title}
      </h2>
      <SocialNetwork
        url={`${hostname}/charlas/${id}`}
        comments={comments}
        month={date.format("MMMM")}
        day={date.format("DD")}
        userId={dataUser.id}
        postsId={postsId}
        proposalId={id}
      />

      {typeof contentAdjusted === "object" ? (
        <div
          dangerouslySetInnerHTML={{
            __html: draftToHtml(contentAdjusted),
          }}
        />
      ) : (
        <p className={`${Styles.main_text_description} mb-16`}>
          {contentAdjusted}
        </p>
      )}

      <div className="text-center">
        <div className="my-96 py-96">
          {attached && (
            <a
              target="_blank"
              className={Styles.downLoadButton}
              href={`${attached[0]}?name=${attached[1]}`}
              rel="noreferrer"
            >
              Descargar PDF <DownloadOutlined />
            </a>
          )}
        </div>
      </div>
    </>
  );

  const BackButton = () => (
    <div className={`${Styles.main_link} flex`}>
      <button className="flex">
        <Link href="/charlas">
          <a>
            <Image
              alt="arrow-icon"
              src="/icons/arrow-left-line.svg"
              width={24}
              height={24}
            />
          </a>
        </Link>
        <Link href="/charlas">
          <p className={`${Styles.text_link} ml-32 font-bold`}>Otras charlas</p>
        </Link>
      </button>
    </div>
  );

  return (
    <main
      className={`${Styles.main_container} w-full h-full px-48 mx-auto  pb-30`}
    >
      <div className="flex w-full justify-between">
        <BackButton />
        <div className={Styles.zone} onClick={() => setShowZonesModal(true)}>
          Zonas habilitadas
        </div>
      </div>
      <div
        className={`${Styles.container_description} w-full relative mx-auto`}
      >
        {discussion ? (
          <>
            <MainContent />
            <SupportDiscussions discussion={discussion} />
            <Comments
              ncomments={ncomments}
              module="posts"
              updateComments={setNcomments}
              mid={postsId}
            />
            <ListRelatedContent post={discussion} module={"charlas"} />
          </>
        ) : (
          <Skeleton active />
        )}
      </div>

      {showZonesModal && (
        <ZonesModal
          closable={true}
          onCancel={() => setShowZonesModal(false)}
          macros={macros}
        />
      )}
    </main>
  );
};
