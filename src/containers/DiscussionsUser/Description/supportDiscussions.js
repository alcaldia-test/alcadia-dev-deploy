import React from "react";
import { useSelector } from "react-redux";
import BottomActions from "@components/BottomActions";
import Styles from "./charlasDescription.module.scss";
import { validateIfUserIsInZone } from "@utils/zoneValidator";

export const SupportDiscussions = ({ discussion }) => {
  const { zones, macros } = discussion;
  const dataAuth = useSelector((state) => state.auth);
  const { favorites } = useSelector((state) => state.voting);
  const isNearly = validateIfUserIsInZone({ favorites, zones, macros });
  const propsBottomsActions = {
    inSide: true,
    isAdmin: false,
    dataAuth,
    isNearly,
    localPost: discussion,
    favorites,
    title: "¿Estás de acuerdo?",
    titleStats: "Participaciones de la Charla",
    module: "discussion",
    bodyMessage: {
      yes: (
        <>
          ¿Estás de <span>acuerdo</span> con el tema de la charla?
        </>
      ),
      no: (
        <>
          ¿Estás en <span>desacuerdo</span> con el tema de la charla?
        </>
      ),
    },
  };

  return (
    <aside className={Styles.aside_menu}>
      <div className={`${Styles.buttons_actions} text-center`}>
        <div className={`${Styles.button_actions} mx-auto`}>
          <BottomActions bottomProps={propsBottomsActions} />
        </div>
      </div>
    </aside>
  );
};
