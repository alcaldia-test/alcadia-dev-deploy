import React, { useEffect, useState } from "react";
import { useDateIsBefore } from "@utils/dateHooks";
import { useRouter } from "next/router";
import Spin from "@components/Spin";
import Skeleton from "@components/Skeleton";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllDiscussions,
  discussionsCount,
} from "@redux/discussions/actions";

import Banner from "@components/Banner";
import Select from "@components/Select";
import { ProposalCardComment } from "@components/Cards";
import Pagination from "@components/Pagination";
import Shared from "@components/Modals/Share";
import Autocomplete from "@components/Autocomplete";
import Styles from "./charlas.module.scss";
import Link from "next/link";

export const DiscussionsUser = ({ children }) => {
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState(undefined);
  const [linkBanner, setLinkBanner] = useState("");
  const [filterAutocomplete, setFilterAutocomplete] = useState(undefined);
  const [modalURL, setModalURL] = useState("");
  const [changedPage, setChangedPage] = useState(false);
  const [loading, setLoading] = useState(false);
  const [postsId, setPostsId] = useState("");
  const [proposalId, setProposalId] = useState("");
  const [currentPage, setCurrentPage] = useState(1);

  const [filterState, setFilterState] = useState({
    text: "",
    selected: "Más Recientes",
    currentPage: 1,
  });

  const dispatch = useDispatch();
  const { dataUser, tokenUser } = useSelector((storage) => storage.auth);
  const { discussions, discussionsCount: count } = useSelector(
    (state) => state.discussion
  );
  const { banners } = useSelector((state) => state.global);
  const router = useRouter();

  useEffect(() => {
    setLoading(true);
    const filterObj = {
      limit: 6,
      skip: (filterState.currentPage - 1) * 6,
      like: filterAutocomplete,
      order: "DESC",
    };

    if (filter === "active" || filter === "finished") {
      filterObj.status = filter;
    } else if (filter === "DESC" || filter === "ASC") {
      filterObj.order = filter;
    } else if (filter === "zones" && dataUser?.zones && dataUser?.macros) {
      filterObj.zones = dataUser.zones;
      filterObj.macros = dataUser.macros;
    }
    if (changedPage) {
      setChangedPage(false);
    } else {
      filterObj.skip = 0;
    }

    dispatch(getAllDiscussions(filterObj));
    dispatch(discussionsCount());
    console.warn({ filter, filterObj });
  }, [filter, filterAutocomplete, filterState.currentPage]);

  useEffect(() => {
    if (banners[0]) {
      const { fileStorages } = banners.find(
        (item) => item.key === "discussion"
      );
      setLinkBanner(fileStorages[0].link);
    }
  }, [banners]);

  useEffect(() => {
    setLoading(false);
  }, [discussions]);

  const onClick = (isFinished, id) => {
    router.push(`charlas/${id}`);
  };
  const handleFilterChange = (value) => {
    setFilter(value);
    setCurrentPage(1);
    setFilterState({ ...filterState, currentPage: 1 });
  };
  const handleAutoChange = (value) => {
    setFilterAutocomplete(value);
    setCurrentPage(1);
    setFilterState({ ...filterState, currentPage: 1 });
  };
  const handlePagination = (pag) => {
    setCurrentPage(pag);
    setFilterState({ ...filterState, currentPage: pag });
    setChangedPage(true);
  };
  const handleShare = (ev, discussion) => {
    ev.preventDefault();
    setPostsId(discussion.postsId);
    setProposalId(discussion.id);
    setVisible(true);
    setModalURL(`${location.origin}/charlas/${discussion.id}`);
  };

  const optionsAutocomplete = discussions?.map((item) => ({
    title: item.title,
  }));
  return (
    <>
      <main>
        <Banner
          title="Charlas Virtuales"
          subtitle="Participa y opina en las charlas de la alcaldía"
          image={linkBanner}
          objectFit="cover"
        />
        <Spin spinning={loading} tip={`Cargando ...`} size="large">
          <div
            className={`${Styles.cardsContainer} w-full flex  flex-col items-center mt-0 mx-auto p-64`}
          >
            <div className={`w-full h-auto flex  justify-between`}>
              <Select
                options={tokenUser ? optionsSelectAuth : optionsSelect}
                placeholder="Filtrar..."
                onChange={(e) => handleFilterChange(e[0]?.value)}
              />
              <Autocomplete
                dataSource={optionsAutocomplete}
                placeholder="Buscar..."
                onChange={(e) => handleAutoChange(e)}
                defaultValue={filterAutocomplete}
              />
            </div>

            {discussions ? (
              <div
                className={`${Styles.cards_column_container} grid auto-rows-auto w-full my-96`}
              >
                {discussions.length > 0 ? (
                  discussions.map((discussion, i) => {
                    return (
                      <Link
                        href={`/charlas/${discussion.id}`}
                        key={discussion.id}
                      >
                        <a>
                          <ProposalCardComment
                            description={{
                              text: discussion.content,
                              link: `/charlas/${discussion.id}`,
                            }}
                            numberComments={discussion.comments}
                            title={discussion.title}
                            zones={discussion.areas}
                            isFinished={!useDateIsBefore(discussion.dueDate)}
                            width="100%"
                            height="500px"
                            onClickShare={(e) => handleShare(e, discussion)}
                            onClickLogin={() =>
                              onClick(
                                !useDateIsBefore(discussion.dueDate),
                                discussion.id
                              )
                            }
                            shareName="charla"
                          />
                        </a>
                      </Link>
                    );
                  })
                ) : (
                  <p className="my-96 text-center">
                    No se encontraron resultados
                  </p>
                )}
              </div>
            ) : (
              <Skeleton />
            )}

            <Pagination
              current={currentPage}
              total={count.count}
              pageSize={6}
              onChange={handlePagination}
            />
          </div>
        </Spin>
      </main>
      <Shared
        visible={visible}
        href={modalURL}
        onCancel={() => setVisible(false)}
        closable={false}
        cancelText="Cancelar"
        userId={dataUser.id}
        postsId={postsId}
        proposalId={proposalId}
      />
    </>
  );
};

const optionsSelectAuth = [
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más antiguas" },
  { value: "active", label: "Activas" },
  { value: "zones", label: "Mis zonas " },
  { value: "finished", label: "Finalizadas " },
];
const optionsSelect = [
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más antiguas" },
  { value: "active", label: "Activas" },
  { value: "finished", label: "Finalizadas " },
];
