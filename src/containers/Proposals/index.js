import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";

import {
  getAnnouncements,
  getAnnouncementsByActives,
  getAnnouncementsByLessRecent,
  getAnnouncementsByMoreRecent,
  getAnnouncementsByMyZones,
  getAnnouncementsByFinished,
} from "@redux/announcements/actions";

import Autocomplete from "@components/Autocomplete";
import Banner from "@components/Banner";
import Button from "@components/Button";
import CardAnnouncements from "@components/Cards/Announcement";
import Pagination from "@components/Pagination";
import Select from "@components/Select";
import Share from "@components/Modals/Share";

import styles from "./Announcements.module.scss";

const optionsSelect = [
  { value: "Actives", label: "Activas" },
  { value: "Recents", label: "Más Recientes" },
  { value: "Oldest", label: "Más Antiguas" },
  { value: "MyZones", label: "Mis zonas" },
  { value: "Finished", label: "Finalizadas" },
];

function Announcements(props) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");
  const [filter, setFilter] = useState("");
  const [filterAutocomplete, setFilterAutocomplete] = useState("");
  const [currentPage, setCurrentPage] = useState(1);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAnnouncements({ page: currentPage, limit: 10 }));
  }, []);

  useEffect(() => {
    if (!filter) {
      dispatch(getAnnouncements({ page: currentPage, limit: 10 }));
    }

    switch (filter) {
      case "Actives":
        dispatch(
          getAnnouncementsByActives({
            page: currentPage,
            limit: 10,
            where: { status: "active", like: filterAutocomplete },
          })
        );
        break;
      case "Recents":
        dispatch(
          getAnnouncementsByMoreRecent({
            page: currentPage,
            limit: 10,
            order: "DESC",
            where: {
              like: filterAutocomplete,
            },
          })
        );
        break;
      case "Oldest":
        setCurrentPage(1);
        dispatch(
          getAnnouncementsByLessRecent({
            page: currentPage,
            limit: 10,
            order: "ASC",
            where: {
              like: filterAutocomplete,
            },
          })
        );
        break;
      case "MyZones":
        // Enter an array with the id of the zones of the user
        dispatch(
          getAnnouncementsByMyZones({
            page: currentPage,
            limit: 10,
            where: {
              zones: [1, 2],
              like: filterAutocomplete,
            },
          })
        );
        break;
      case "Finished":
        dispatch(
          getAnnouncementsByFinished({
            page: currentPage,
            limit: 10,
            where: { status: "finished", like: filterAutocomplete },
          })
        );
        break;
      default:
        dispatch(
          getAnnouncements({
            page: currentPage,
            limit: 10,
            where: {
              like: filterAutocomplete,
            },
          })
        );
        break;
    }
  }, [filter, filterAutocomplete]);

  useEffect(() => {
    if (count.count / 10 < currentPage) {
      setCurrentPage(1);
    }
    switch (filter) {
      case "Actives":
        dispatch(
          getAnnouncementsByActives({
            page: currentPage,
            limit: 10,
            where: { status: "active", like: filterAutocomplete },
          })
        );
        break;
      case "Recents":
        dispatch(
          getAnnouncementsByMoreRecent({
            page: currentPage,
            limit: 10,
            order: "DESC",
            where: {
              like: filterAutocomplete,
            },
          })
        );
        break;
      case "Oldest":
        setCurrentPage(1);
        dispatch(
          getAnnouncementsByLessRecent({
            page: currentPage,
            limit: 10,
            order: "ASC",
            where: {
              like: filterAutocomplete,
            },
          })
        );
        break;
      case "MyZones":
        // Enter an array with the id of the zones of the user
        dispatch(
          getAnnouncementsByMyZones({
            page: currentPage,
            limit: 10,
            where: {
              zones: [1, 2],
              like: filterAutocomplete,
            },
          })
        );
        break;
      case "Finished":
        dispatch(
          getAnnouncementsByFinished({
            page: currentPage,
            limit: 10,
            where: { status: "finished", like: filterAutocomplete },
          })
        );
        break;
      default:
        dispatch(
          getAnnouncements({
            page: currentPage,
            limit: 10,
            like: filterAutocomplete,
          })
        );
        break;
    }
  }, [currentPage]);

  const { list, count } = useSelector((state) => state.announcements);

  const optionsAutocomplete = list.map((item) => ({
    title: item.title,
  }));

  return (
    <>
      <Banner
        title="Propuestas Ciudadanas"
        subtitle="Participa en la creación de propuestas para los siguientes proyectos"
        image="/images/banner.jpg"
        objectFit="cover"
      />
      <section className={styles.filter__container}>
        <div className={styles.filters}>
          <Select
            options={optionsSelect}
            placeholder="Filtrar..."
            onChange={(e) => {
              setFilter(e[0].value);
            }}
          />
          <Autocomplete
            dataSource={optionsAutocomplete}
            placeholder="Buscar..."
            onChange={(e) => setFilterAutocomplete(e)}
          />
        </div>
        <div className={styles.filter__content}>
          {list && list.length > 0 ? (
            list.map(
              ({
                supportingLimit,
                receptionStart,
                receptionEnd,
                reviewLimit,
                directPosts,
                proposals,
                fileLink,
                postsId,
                title,
                areas,
                id,
              }) => (
                <CardAnnouncements
                  supportingLimit={supportingLimit}
                  receptionStart={receptionStart}
                  receptionEnd={receptionEnd}
                  reviewLimit={reviewLimit}
                  directPosts={directPosts}
                  proposals={proposals}
                  image={fileLink}
                  title={title}
                  zones={areas}
                  key={id}
                >
                  <Link href={"/propuestas/[id]"} as={`/propuestas/${postsId}`}>
                    <Button type="primary" shape="round" size="small">
                      Ingresa y participa
                    </Button>
                  </Link>
                  <Button
                    type="secondary"
                    shape="round"
                    size="small"
                    onClick={() => {
                      setModalURL(`${location.origin}/propuesta/${postsId}`);
                      setModalOpen(!modalOpen);
                    }}
                  >
                    Comparte
                  </Button>
                </CardAnnouncements>
              )
            )
          ) : (
            <p className={styles.no__results}>No se encontraron resultados</p>
          )}
        </div>
        <Pagination
          total={count?.count}
          current={currentPage}
          onChange={(page) => {
            setCurrentPage(page);
            console.log(page);
          }}
        />
      </section>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
      />
    </>
  );
}

export default Announcements;
