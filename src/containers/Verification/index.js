import Button from "@components/Button";
import Code from "@components/Code";
import { registerCodeStart } from "@redux/verification/actions";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "./verification.module.scss";
import Spin from "@components/Spin";
import message from "@components/Message";
import Router from "next/router";
import { resendPasswordStart, signOutStart } from "@redux/auth/actions";
import { hideLoader } from "@redux/common/actions";

const Verification = () => {
  const { common } = useSelector((store) => store);
  const dispatch = useDispatch();
  const [codeValue, setCodeValue] = useState("");
  const [buttonValue, setButtonValue] = useState(true);
  const { userRegister } = useSelector((datos) => datos.auth);
  const [generalCode, setGeneralCode] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  useEffect(() => {
    dispatch(hideLoader());
  }, []);
  const codeSelect = (e) => {
    if (e.length === 4) {
      setButtonValue(false);
    } else {
      setButtonValue(true);
    }
    setCodeValue(e);
  };
  const sendCode = (e) => {
    e.preventDefault();
    return new Promise((resolve, reject) => {
      dispatch(
        registerCodeStart({
          resolve,
          reject,
          code: codeValue,
          id: userRegister.id,
        })
      );
    })
      .then((res) => {
        message.success(res);
        Router.replace("/verification/zones");
      })
      .catch((res) => {
        setGeneralCode({
          text: res,
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      });
  };

  const resendCode = async () => {
    const identityCard = userRegister?.identityCard;
    try {
      const res = await new Promise((resolve, reject) =>
        dispatch(resendPasswordStart({ resolve, reject, identityCard }))
      );

      message.success(res);
    } catch (res) {
      setGeneralCode({
        text: res,
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
  };

  const handleLogout = () => {
    dispatch(signOutStart());
    Router.replace(`/login`);
  };

  return (
    <div>
      <Spin spinning={common.loader}>
        <h2
          className={`text-center font-bold text-2xl mt-4 md:mt-7 px-1 ${styles.color_title}`}
        >
          Verifique su correo electrónico
        </h2>
        <p
          className={` text-lg font-bold text-center mt-7 px-3 ${styles.subtitle} `}
        >
          Hemos enviado un código de verificación a este correo electrónico:
        </p>
        <p
          className={`text-center font-bold text-lg mt-2 lg:mt-4 ${styles.user}`}
        >
          {userRegister?.email}
        </p>
        <p className={`text-center font-bold  my-5  ${styles.alert_color}`}>
          {generalCode.text}
        </p>
        <div className=" mt-2 ">
          <Code codeSize={4} autoFocus={true} handleInput={codeSelect} />
        </div>
        <p
          className={`text-right text-lg font-bold px-3 mt-3 ${styles.code_send}`}
        >
          <span onClick={handleLogout} className="mr-3 cursor-pointer">
            Cerrar Sesión
          </span>
          <span onClick={resendCode} className="mr-3 cursor-pointer">
            Reenviar código
          </span>
        </p>
        <div className="flex justify-center mt-7">
          <Button
            disabled={buttonValue}
            size={"large"}
            className="w-full lg:w-3/6"
            onClick={sendCode}
          >
            Siguiente
          </Button>
        </div>
      </Spin>
    </div>
  );
};
export default Verification;
