import Button from "@components/Button";
import SelectSearch from "@components/SelectSearch";
import { registerZoneStart } from "@redux/verification/actions";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "../verification.module.scss";
import Spin from "@components/Spin";
import Router from "next/router";
import message from "@components/Message";
import { hideLoader } from "@redux/common/actions";

const RegisterZones = () => {
  const dispatch = useDispatch();
  const { common } = useSelector((store) => store);
  const data = useSelector((store) => store.verification);
  const [negocioValue, setNegocioValue] = useState(true);
  const [familiarValue, setfamiliarValue] = useState(true);
  const [macrosValue, setMacrosValue] = useState({});
  const [ZoneValue, setZoneValue] = useState({});
  const [ZoneFamiliaValue, setZoneFamiliaValue] = useState({});
  const [buttonsValue, setButtonsValue] = useState({
    negocio: true,
    directo: true,
    familia: true,
    send: "hidden",
    negocioColor: "#dedede",
    familiaColor: "#dedede",
    negocioBorder: "none",
    familiaBorder: "none",
  });
  useEffect(() => {
    dispatch(hideLoader());
  }, []);
  const [generalCode, setGeneralCode] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const SelectMacro = (e) => {
    setButtonsValue({
      ...buttonsValue,
      negocio: false,
    });
    setMacrosValue(e);
  };
  const SelectZone = (e) => {
    setZoneValue(e);
    setButtonsValue({
      ...buttonsValue,
      familia: false,
    });
  };
  const SelectZoneFamilia = (e) => {
    setZoneFamiliaValue(e);
    setButtonsValue({
      ...buttonsValue,
      send: "block",
    });
  };
  const selectNegocio = (e) => {
    e.preventDefault();
    setButtonsValue({
      ...buttonsValue,
      negocioColor: "#dedede",
      negocioBorder: "none",
    });
    setNegocioValue(false);
  };
  const NoNegocio = (e) => {
    e.preventDefault();
    setButtonsValue({
      ...buttonsValue,
      familia: false,
      negocioColor: "#fafafa",
      negocioBorder: "#00b1ba",
    });
    setZoneValue({
      id: 0,
    });
  };
  const selectFamilia = (e) => {
    e.preventDefault();
    setButtonsValue({
      ...buttonsValue,
      familiaColor: "#dedede",
      familiaBorder: "none",
    });
    setfamiliarValue(false);
  };
  const selectNoFamilia = (e) => {
    e.preventDefault();
    setButtonsValue({
      ...buttonsValue,
      send: "block",
      familiaColor: "#fafafa",
      familiaBorder: "#00b1ba",
    });
    setZoneFamiliaValue({
      id: 0,
    });
  };

  const registerZone = (e) => {
    e.preventDefault();

    new Promise((resolve, reject) => {
      dispatch(
        registerZoneStart({
          resolve,
          reject,
          BUSINESS: ZoneValue,
          HOME: macrosValue,
          FAMILY: ZoneFamiliaValue,
        })
      );
    })
      .then((res) => {
        message.success(res);

        Router.replace("/verification/success-zone2");
      })
      .catch((res) => {
        setGeneralCode({
          text: res,
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      });
  };
  const cancelNegocio = (e) => {
    e.preventDefault();
    setButtonsValue({
      ...buttonsValue,
      familia: false,
    });
    setZoneValue({
      id: 0,
    });
    setNegocioValue(true);
  };
  const cancelFamilia = (e) => {
    e.preventDefault();
    setButtonsValue({
      ...buttonsValue,
      send: "block",
    });
    setZoneFamiliaValue({
      id: 0,
    });
    setfamiliarValue(true);
  };
  return (
    <div className="w-full lg:w-6/12">
      <h2
        className={`text-center font-bold text-2xl mt-4 md:mt-7 ${styles.color_title}`}
      >
        Por favor introduzca algunos macrodistritos de interés
      </h2>
      <p className={` text-lg font-bold text-center mt-4 ${styles.subtitle} `}>
        Añadir esta ubicación le permitirá participar en eventos de la
        plataforma que estén relacionados con su macrodistrito
      </p>
      <p className={`text-center font-bold  my-5  ${styles.alert_color}`}>
        {generalCode.text}
      </p>
      <Spin spinning={common.loader}>
        <p
          className={` text-xl font-bold text-center mt-5 ${styles.color_sub} `}
        >
          ¿En qué macrodistrito se encuentra su domicilio?
        </p>
        <div className="flex justify-center mt-2">
          <SelectSearch
            onchange={SelectMacro}
            data={data.macros}
            placeholder={"Busque la ubicación de su domicilio"}
          />
        </div>
        <p
          className={` text-xl font-bold text-center mt-5 ${styles.color_sub} `}
        >
          ¿Desea participar por negocio?
        </p>
        {negocioValue ? (
          <div className="flex justify-around mt-4 w-full">
            <Button
              disabled={buttonsValue.negocio}
              size={"large"}
              onClick={selectNegocio}
              type={"secondary"}
              className="w-full mr-1"
              style={{ backgroundColor: "#dedede" }}
            >
              Si
            </Button>
            <Button
              disabled={buttonsValue.negocio}
              onClick={NoNegocio}
              size={"large"}
              type={"secondary"}
              className="w-full ml-1"
              style={{
                backgroundColor: buttonsValue.negocioColor,
                borderColor: buttonsValue.negocioBorder,
              }}
            >
              No
            </Button>
          </div>
        ) : (
          <div className="flex justify-center items-center	 mt-2">
            <SelectSearch
              onchange={SelectZone}
              data={data?.macros}
              placeholder={"Busque la ubicación de su negocio"}
            />
            <div className=" flex justify-center cursor-pointer mx-2 w-4 h-4">
              <img onClick={cancelNegocio} src="/icons/delete.svg"></img>
            </div>
          </div>
        )}
        <p
          className={` text-xl font-bold text-center mt-5 ${styles.color_sub} `}
        >
          ¿Desea participar por un familiar directo?
        </p>
        {familiarValue ? (
          <div className="flex justify-around mt-4 w-full">
            <Button
              disabled={buttonsValue.familia}
              size={"large"}
              onClick={selectFamilia}
              type={"secondary"}
              className="w-full mr-1"
              style={{ backgroundColor: "#dedede" }}
            >
              Si
            </Button>
            <Button
              disabled={buttonsValue.familia}
              onClick={selectNoFamilia}
              size={"large"}
              type={"secondary"}
              className="w-full mr-1"
              style={{
                backgroundColor: buttonsValue.familiaColor,
                borderColor: buttonsValue.familiaBorder,
              }}
            >
              No
            </Button>
          </div>
        ) : (
          <div className="flex justify-center items-center mt-2">
            <SelectSearch
              onchange={SelectZoneFamilia}
              data={data?.macros}
              placeholder={"Busque la ubicación"}
            />
            <div className=" flex justify-center cursor-pointer mx-2 w-4 h-4">
              <img onClick={cancelFamilia} src="/icons/delete.svg"></img>
            </div>
          </div>
        )}
        <div className={`flex justify-center mt-7 ${buttonsValue.send}`}>
          <Button size={"large"} className=" w-full" onClick={registerZone}>
            Siguiente
          </Button>
        </div>
      </Spin>
    </div>
  );
};
export default RegisterZones;
