import Image from "next/image";
import React from "react";
import styles from "../verification.module.scss";

const RegisterSuccess = () => {
  return (
    <div>
      <div className="flex justify-center flex-col w-full h-full ">
        <Image
          src="/images/logo.svg"
          alt="Logo gobierno autonomo municipal de La Paz"
          width={200}
          height={200}
        />
        <p className={`text-3xl font-bold text-center mt-4  ${styles.user}`}>
          ¡Ya completaste el registro!
        </p>
        <p
          className={` text-xl font-bold text-center mt-3 ${styles.color_sub}`}
        >
          Bienvenido a La Paz Decide
        </p>
      </div>
    </div>
  );
};
export default RegisterSuccess;
