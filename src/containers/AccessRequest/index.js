// import SearchInput from "@components/SearchInput";
// import SelectComponent from "@components/Select";
import Tab from "@components/Tab";
// import { orderAdminRequestStart } from "@redux/accessRequest/actions";
import React from "react";
import AccessUsers from "./AccessUsers";
import AdmiRoles from "./AdmiRoles";

const AccessRequest = () => {
  return (
    <div className="bg-white w-full h-screen">
      <div
        className={`mx-4 my-5 px-5 py-4 border-2 rounded-large border-green-400`}
        style={{ borderColor: "rgba(156, 156, 156, 0.4)" }}
      >
        <Tab
          content={[
            {
              title: "Solicitudes nuevas",
              children: (
                <>
                  <AccessUsers />
                </>
              ),
            },
            {
              title: "Con permisos",
              children: (
                <>
                  <AdmiRoles />
                </>
              ),
            },
          ]}
        />
      </div>
    </div>
  );
};

export default AccessRequest;
