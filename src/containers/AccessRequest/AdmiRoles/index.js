import Collapse from "@components/Collapse";
import Modal from "@components/Modals";
import Table from "@components/Table";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "../accessRequest.module.scss";
import Spin from "@components/Spin";
import {
  deleteAdminRolesStart,
  initialRequestAdminRolesSuccess,
  updateAdminRolesStart,
} from "@redux/accessRequest/actions";
import message from "@components/Message";
import SelectWraper from "@components/Select";

const AdmiRoles = () => {
  const [modal, setModal] = useState(false);
  const [modalDelete, setModalDelte] = useState(false);
  const [values, setValues] = useState([]);
  const { adminSuccess } = useSelector((e) => e.accessRequest);
  const [datos, setdatos] = useState([]);
  const [userSelected, updateUserSelected] = useState([]);
  const { common, auth } = useSelector((store) => store);
  const dispatch = useDispatch();
  const aceptarPermisos = (e) => {
    updateUserSelected(e);
    e.permissions.forEach((elemen) => {
      e.roles.forEach((rol) => {
        if (elemen === rol.id) {
          rol.value = true;
          setValues((values) => [...values, rol.id]);
        }
      });
    });
    setdatos(e.roles);
    setModal(true);
  };
  const onChange = (e) => {
    if (e.target.checked) {
      setValues((values) => [...values, e.target.value]);
    } else {
      setValues(values.filter((el) => e.target.value !== el));
    }
  };
  const cancelModal = () => {
    setValues([]);
    setdatos([]);
    datos.forEach((element) => {
      if (element.value) {
        element.value = false;
      }
    });
    setModal(false);
  };
  const AsignarPermisos = () => {
    const requestSend = {
      permissionId: values,
      state: "APPROVED",
      reviewerId: auth.dataUser.id,
      message: "Actualizacion aceptada",
    };
    return new Promise((resolve, reject) => {
      dispatch(
        updateAdminRolesStart({
          requestSend,
          id: userSelected.id,
          resolve,
          reject,
        })
      );
    })
      .then((res) => {
        message.success(res);
        cancelModal();
      })
      .catch((res) => {
        message.error(res);
      });
  };
  const dataSource = [
    {
      title: "Usuario",
      dataIndex: "name",
    },
    {
      titl: "Correo electrónico",
      dataIndex: "email",
    },
    {
      title: "Fecha y hora",
      dataIndex: "createdAt",
      sorter: (a, b) =>
        new Date(a.createdRun).getTime() + new Date(b.createdRun).getTime(),
    },
    {
      title: "Acciones",
      key: "action",
      render: (id) => (
        <div className="flex justify-around">
          <span
            className="actionButton cursor-pointer"
            onClick={(e) => {
              rejectAdmin(id);
            }}
          >
            <a>Eliminar</a>
          </span>
          <span
            className="actionButton cursor-pointer"
            onClick={(e) => {
              aceptarPermisos(id);
            }}
          >
            <a>Cambiar permisos</a>
          </span>
        </div>
      ),
    },
  ];
  const cancelModalDelete = () => {
    setModalDelte(false);
  };

  const deleteAdmin = () => {
    return new Promise((resolve, reject) => {
      dispatch(
        deleteAdminRolesStart({
          id: userSelected.id,
          resolve,
          reject,
        })
      );
    })
      .then((res) => {
        message.success(res);
        setModalDelte(false);
      })
      .catch((res) => {
        message.error(res);
        setModalDelte(false);
      });
  };
  const rejectAdmin = (e) => {
    updateUserSelected(e);
    setModalDelte(true);
  };
  const optionsSelect = [
    { value: "DESC", label: "Más Recientes" },
    { value: "ASC", label: "Más Antiguas" },
  ];

  const filterDate = (e) => {
    adminSuccess.map((e) => (e.date = new Date(e.createdRun).getTime()));
    adminSuccess.sort((a, b) => a.date - b.date);
    if (e[0].value === "DESC") {
      adminSuccess.reverse();
      dispatch(initialRequestAdminRolesSuccess(adminSuccess));
    } else {
      dispatch(initialRequestAdminRolesSuccess(adminSuccess));
    }
  };
  return (
    <>
      <SelectWraper
        onChange={filterDate}
        placeholder="Todas"
        options={optionsSelect}
      ></SelectWraper>
      <Modal visible={modalDelete} onCancel={cancelModalDelete}>
        <Spin spinning={common.loader} tip="Eliminando usuario...">
          <div className="my-10 flex flex-col justify-center align-middle items-center	">
            <img src="/icons/delete-bin.svg" className={`${styles.image}`} />
            <p
              className={`mt-4 text-center text-2xl font-bold px-2 ${styles.color_delte}`}
            >
              ¿Seguro que desea eliminar este usuario?
            </p>
            <div className=" w-2/4 flex justify-around mt-5">
              <span
                className={`font-bold text-lg ${styles.color_option} cursor-pointer`}
              >
                <a onClick={cancelModalDelete}>No</a>
              </span>
              <span
                onClick={deleteAdmin}
                className={`font-bold text-lg ${styles.color_option} cursor-pointer`}
              >
                <a>Si</a>
              </span>
            </div>
          </div>
        </Spin>
      </Modal>
      <Modal visible={modal} width={1000} onCancel={cancelModal}>
        <div>
          <p className={` font-bold text-left text-2xl ${styles.color_modal}`}>
            Cambiar permisos
          </p>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 justify-items-center gap-y-px w-full mt-4 mb-6">
            <Collapse
              header={"Votaciones"}
              items={datos.filter(
                (e) =>
                  e.roleGroupId === "5b411897-5015-4be2-a5ac-d9623a7b1268" &&
                  e.name !== "Gestionar todo en Votaciones"
              )}
              typeCheckBox={"primary"}
              onChange={onChange}
              bordered={false}
            />
            <Collapse
              header={"Convocatorias"}
              items={datos.filter(
                (e) =>
                  e.roleGroupId === "90adf2b9-00dc-4b4b-a9ed-990413202e92" &&
                  e.name !== "Gestionar todo en Convocatorias"
              )}
              typeCheckBox={"primary"}
              onChange={onChange}
              bordered={false}
            />
            <Collapse
              header={"Charlas Virtuales"}
              items={datos.filter(
                (e) =>
                  e.roleGroupId === "747da2bd-d53e-485b-a5d1-4cdbfeb688b6" &&
                  e.name !== "Gestionar todo en Charlas"
              )}
              typeCheckBox={"primary"}
              onChange={onChange}
              bordered={false}
            />
            <Collapse
              header={"Normativas"}
              items={datos.filter(
                (e) =>
                  e.roleGroupId === "c51440bb-d680-450c-9ee0-582066b92e33" &&
                  e.name !== "Gestionar todo en Normativas"
              )}
              typeCheckBox={"primary"}
              onChange={onChange}
              bordered={false}
            />
            <Collapse
              header={"Usuarios"}
              items={datos.filter(
                (e) =>
                  e.name === "Bloquear Usuarios" ||
                  e.name === "Gestionar Denuncias"
              )}
              typeCheckBox={"primary"}
              onChange={onChange}
              bordered={false}
            />
            <Collapse
              header={"Página"}
              items={datos.filter(
                (e) =>
                  e.name === "Editar encabezados" ||
                  e.name === "Gestionar Logros" ||
                  e.name === "Ver Estadísticas"
              )}
              typeCheckBox={"primary"}
              onChange={onChange}
              bordered={false}
            />
          </div>
          <div className=" w-2/4 flex justify-around">
            <span
              onClick={cancelModal}
              className={`font-bold text-lg ${styles.color_option} cursor-pointer`}
            >
              <a>Cancelar</a>
            </span>
            <span
              className={`font-bold text-lg ${styles.color_option} cursor-pointer`}
              onClick={AsignarPermisos}
            >
              <a>Cambiar permisos</a>
            </span>
          </div>
        </div>
      </Modal>
      <div className="mt-4">
        <Spin spinning={common.loader} tip="Cargando...">
          <Table dataSource={adminSuccess} columns={dataSource} />
        </Spin>
      </div>
    </>
  );
};

export default AdmiRoles;
