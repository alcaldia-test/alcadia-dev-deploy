import React, { useState } from "react";
import Select from "@components/Select";
import Autocomplete from "@components/Autocomplete";
import styles from "./filterInputs.module.scss";

export default function filterInput({ onSearch, onSelectChange, normatives }) {
  const [filterAutocomplete, setFilterAutocomplete] = useState(undefined);
  const optionsAutocomplete = normatives?.map((item) => ({
    title: item.title,
  }));
  const handleAutoChange = (value) => {
    onSearch(value);
    setFilterAutocomplete(value);
  };

  return (
    <div
      className={`${styles.filterContainer} w-full h-auto flex justify-between`}
    >
      <Select
        onChange={(e) => onSelectChange(e[0].value)}
        defaultValue="Más Recientes"
        backspaceDeleted={true}
        placeholder={"Más reciente"}
        options={[
          { value: "DESC", label: "Más Recientes" },
          { value: "ASC", label: "Más Antiguas" },
          { value: "ACTIVE", label: "Activas" },
          { value: "FINISHED", label: "Finalizadas" },
        ]}
      ></Select>
      <Autocomplete
        dataSource={optionsAutocomplete}
        placeholder="Buscar..."
        onChange={(e) => handleAutoChange(e)}
        defaultValue={filterAutocomplete}
      />
    </div>
  );
}
