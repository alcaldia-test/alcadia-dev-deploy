import React, { useState, useEffect } from "react";
import Pagination from "@components/Pagination";
import FilterInput from "../FiltersInput";
import { useDispatch } from "react-redux";

import { getFilterNormative } from "@redux/normatives/actions";

import Styles from "./CardsContainer.module.scss";

const CardsContainer = ({ normatives, children, totalEls, pageSize = 6 }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const initialFilter = {
    content: undefined,
    currentPage: 1,
    state: "PUBLISHED",
    order: "DESC",
    limit: 6,
  };
  const [filterState, setFilterState] = useState(initialFilter);
  const dispatch = useDispatch();

  const handleFilter = (value) => {
    if (value === "DESC" || value === "ASC") {
      setFilterState({ ...initialFilter, order: value });
    } else if (value === "ACTIVE" || value === "FINISHED") {
      setFilterState({ ...initialFilter, state: value });
    } else {
      setFilterState({ ...initialFilter, content: value });
    }
    setCurrentPage(1);
  };

  const handlePagination = (pag) => {
    setFilterState({ ...filterState, currentPage: pag });
    setCurrentPage(pag);
  };

  useEffect(() => {
    dispatch(getFilterNormative(filterState));
  }, [filterState]);

  return (
    <div
      className={`${Styles.cardsContainer} w-full flex  flex-col items-center mt-0 mx-auto p-64`}
    >
      <FilterInput
        onSearch={handleFilter}
        onSelectChange={handleFilter}
        normatives={normatives}
      />
      <div
        className={`${Styles.cards_column_container} grid auto-rows-auto w-full my-96`}
      >
        {children}
      </div>
      <Pagination
        current={currentPage}
        total={totalEls}
        pageSize={pageSize}
        onChange={handlePagination}
      />
    </div>
  );
};

export default CardsContainer;
