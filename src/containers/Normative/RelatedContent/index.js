import React, { useEffect } from "react";
import Link from "next/link";
import { useStore, useDispatch } from "react-redux";
import { getContent } from "@redux/relatedContent/actions";
import moment from "moment";

import styles from "./related.module.scss";

const RealtedContent = ({ id, RealtedContent }) => {
  const data = useStore().getState();
  const dispatch = useDispatch();

  useEffect(() => {
    if (data.chapters.tags && id !== "") {
      dispatch(getContent({ tags: data.chapters.tags, id }));
    }
  }, [data.chapters.postId, data.chapters.id]);

  return (
    <div className={styles.relatedContentContainer}>
      <h6>Contenido Relacionado</h6>
      {RealtedContent.length > 0 &&
        RealtedContent.map((content, i) => {
          return (
            <Link key={i} href={`[id]`} as={`${content.id}`}>
              <a>
                <div className={styles.Content}>
                  <header>
                    <span className={styles.createdAt}>
                      {moment(content.createdAt).format("MM/DD/YYYY")}
                    </span>
                  </header>
                  <p>{content.title}</p>
                </div>
              </a>
            </Link>
          );
        })}
    </div>
  );
};

export default RealtedContent;
