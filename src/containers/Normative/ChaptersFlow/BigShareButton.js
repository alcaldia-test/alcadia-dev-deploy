import React from "react";
import IconGroup from "@components/SocialNetworks/IconGroup";

import styles from "./styles/bigShare.module.scss";

const BigShareButton = ({ title, href }) => {
  return (
    <div className={styles.shareContainer}>
      <h6>{title}</h6>
      <IconGroup href={href} />
    </div>
  );
};

export default BigShareButton;
