import React, { useEffect } from "react";
import SocialNetwork from "@components/SocialNetwork";
// import { voteNormative } from "@redux/normativesVote/actions";
import styles from "./styles/chapterView.module.scss";
import { parseDraft } from "@utils/draftToHtml";

const ChapterView = ({
  chapterNumber,
  chapters,
  date,
  dataUser,
  postsId,
  myRef,
}) => {
  const { articles, comments } = chapters[chapterNumber];
  const title = articles[0].title;

  useEffect(() => {
    console.warn({ myRef });
    if (myRef) {
      console.warn("Definido");

      myRef.current.scrollIntoView({ block: "start", behavior: "smooth" });
    } else {
      console.warn("Undefinido");

      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    }
  }, []);

  return (
    <>
      <section className={styles.mainNormativesContainer}>
        <div className={styles.chapterView}>
          <h3>
            Capítulo {chapterNumber + 1}: {title}
          </h3>
          <SocialNetwork
            url={`/normativas/${chapters.id}`}
            comments={comments}
            month={date.format("MMMM")}
            day={date.format("DD")}
            userId={dataUser.id}
            postsId={postsId}
            proposalId={chapters[chapterNumber].id}
          />

          {articles.map((paragraph, i) => (
            <div key={i}>
              <h5>{`Artículo ${i + 1}.`}</h5>

              <p
                className={`${styles.description} mb-16`}
                dangerouslySetInnerHTML={{
                  __html: parseDraft(paragraph.content),
                }}
              />
            </div>
          ))}
          {/* <div className={styles.Reaction}>
          <ButtonsActions
            message={"¿Apoyas esta normativa?"}
            debate={true}
            isLogged={!!auth.tokenUser}
            isNearly={true}
            voteData={{
              like: chapters.chapters[chapterNumber - 1].likes,
              dislike: chapters.chapters[chapterNumber - 1].dislikes,
            }}
            reaction={chapters.chapters[chapterNumber - 1].vote}
            getVote={handleVote}
            modalMessage={{
              like: "este capitulo",
              dislike: "este capitulo",
            }}
          />
        </div> */}

          {/* <Comments
          ncomments={ncomments}
          module="chapter"
          updateComments={setNcomments}
          mid={chapters.chapters[chapterNumber - 1].id}
        /> */}
        </div>
      </section>
    </>
  );
};

export default ChapterView;
