import React from "react";
import BodyNormative from "../BodyNormative";
import styles from "./styles/chaptersWrapper.module.scss";
import Skeleton from "@components/Skeleton";
import { useSelector } from "react-redux";
import dynamic from "next/dynamic";
const ListRelatedContent = dynamic(
  () => import("@components/ListRelatedContent"),
  {
    ssr: false,
  }
);

const Chapters = ({ normative, hostname }) => {
  // const normative = useSelector((state) => state.chapters);
  const { dataUser } = useSelector((state) => state.auth);
  return (
    <div className={styles.wrapper}>
      <div className={styles.elementsContainer}>
        {normative.id ? (
          <>
            <BodyNormative
              normative={normative}
              dataUser={dataUser}
              isAdmin={false}
              hostname={hostname}
            />
            <div className={styles.relatedContent}>
              <ListRelatedContent post={normative} module={"normativas"} />
            </div>
          </>
        ) : (
          <Skeleton active />
        )}
      </div>
    </div>
  );
};

export default Chapters;
