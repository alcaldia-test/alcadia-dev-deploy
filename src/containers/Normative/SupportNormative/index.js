import React from "react";
import { useSelector } from "react-redux";
import BottomActions from "@components/BottomActions";
import Styles from "./index.module.scss";

export const SupportNormative = ({ normative, chapter, isAdmin, page }) => {
  const dataAuth = useSelector((state) => state.auth);
  const complement = page ? "este capítulo" : "esta normativa";
  const propsBottomsActions = {
    inSide: !isAdmin,
    isAdmin,
    dataAuth,
    isNearly: true,
    localPost: page ? chapter : normative,
    title: "¿Estás de acuerdo?",
    titleStats: `Participaciones en ${complement}`,
    module: page ? "chapter" : "normatives",
    bodyMessage: {
      yes: (
        <>
          ¿Estás de <span>acuerdo</span> con {complement}?
        </>
      ),

      no: (
        <>
          ¿Estás en <span>desacuerdo</span> con {complement}?
        </>
      ),
    },
  };

  return (
    <aside className={isAdmin ? "" : Styles.aside_menu}>
      <div className={`${Styles.buttons_actions} text-center`}>
        <div className={`${Styles.button_actions} mx-auto`}>
          <BottomActions bottomProps={propsBottomsActions} />
        </div>
      </div>
    </aside>
  );
};
