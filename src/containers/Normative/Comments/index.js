import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Router from "next/router";
import CommentField from "@components/CommentField";
import Comments from "@components/Comments";
import { DownOutlined, UpOutlined } from "@ant-design/icons";
import {
  likeComment,
  postAnswer,
  complaintComment,
} from "@redux/normativesComments/actions";

import Styles from "./Comments.module.scss";

const CommentsLayout = ({
  dataComments,
  currentId,
  handleCommentData,
  keepOpen = true,
}) => {
  const [open, setOpen] = useState(keepOpen);
  const data = useSelector((data) => data);
  const router = Router;
  const dispatch = useDispatch();

  const handleComment = (comment, file) => {
    if (!data.auth.tokenUser)
      router.push({
        pathname: "/login",
        query: { goto: encodeURI(window.location.pathname) },
      });
    else handleCommentData({ comment, file, currentId });
  };

  const handleAnswer = (e) => {
    dispatch(
      postAnswer({
        commentId: e.id,
        content: e.data.text,
        userId: data.auth.dataUser.id,
        currentState: data.normativeComments.comments,
        fileData: {
          file: e.data.data[0],
          container: "comments",
          tag: "banner",
        },
      })
    );
  };

  const handleLike = ({ type, id, responseType }) => {
    console.log(type, id, responseType);
    dispatch(
      likeComment({
        id,
        vote: type === "none" ? "omit" : type === "liked" ? "like" : "dislike",
        endpoint: responseType === "comment" ? "comments" : "answers",
        userId: data.auth.dataUser.id,
        currentState: data.normativeComments.comments,
        currentEnpoint: "posts",
        currentId,
      })
    );
  };

  const handleCompaint = (data) => {
    dispatch(
      complaintComment({
        denuncias: data.denuncias,
        commentId: data.type === "comment" ? data.id : "",
        answerId: data.type === "answer" ? data.id : "",
      })
    );
  };

  useEffect(() => {}, [dataComments, data.normativeComments.comments]);

  return (
    <div className={Styles.CommentSection}>
      <h5>Comentarios</h5>
      {data.chapters.state !== "FINISHED" && (
        <CommentField
          avatarSize={"medium"}
          src={data.auth.dataUser.avatar}
          placeholder="Deja tu comentario..."
          handleUpload={handleComment}
        />
      )}
      <div className={Styles.CommentsToggle} onClick={() => setOpen(!open)}>
        <span className={Styles.toggleButton}>
          Activas
          {open === true ? <UpOutlined /> : <DownOutlined />}
        </span>
        <span className={Styles.commentsAmount}>
          {data.normativeComments.comments.length}{" "}
          {data.normativeComments.comments.length > 1
            ? "comentarios"
            : "comentario"}
        </span>
      </div>
      <div className={Styles.CommentsOpen}>
        {dataComments && (
          <>
            <Comments
              isEnded={data.chapters.state === "FINISHED"}
              endMessage={"Esta normativa finalizó ya no se puede comentar"}
              setDenuncias={handleCompaint}
              currentUser={data.auth.dataUser.avatar}
              auth={!!data.auth.tokenUser}
              isAdmin={false}
              handleCommentResponse={handleAnswer}
              handleLikes={handleLike}
            />
          </>
        )}
      </div>
    </div>
  );
};

export default CommentsLayout;
