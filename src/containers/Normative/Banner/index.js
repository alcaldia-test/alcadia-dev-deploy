import React, { useEffect } from "react";
import Banner from "@components/Banner";
import { useSelector } from "react-redux";

export default function BannerComponent() {
  const data = useSelector((data) => data.global.banners);
  const index = data.findIndex((banner) => banner.key === "normative");
  useEffect(() => {}, [data]);
  return (
    <Banner
      image={(data.length > 0 && data[index].fileStorages[0].link) || ""}
      title={"Normativa Colaborativa"}
      subtitle={"Danos tu opinión en las siguientes normativas a implementar"}
      large={false}
      objectFit="cover"
    />
  );
}
