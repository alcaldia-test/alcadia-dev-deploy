import React, { useState } from "react";
import Router from "next/router";
import { useSelector } from "react-redux";
import CardsContainer from "./CardsContainer";
import ProposalCard from "@components/Cards/Proposals/ProposalCardComment";
import Shared from "@components/Modals/Share";
import Spin from "@components/Spin";
import Banner from "./Banner";
import moment from "moment";

function Normatives() {
  const { count, normatives } = useSelector((store) => store.normatives);
  const { dataUser } = useSelector((storage) => storage.auth);

  const { loader } = useSelector((store) => store.common);
  const [visible, setVisible] = useState(false);
  const [modalURL, setModalURL] = useState("");
  const [postsId, setPostsId] = useState("");
  const [proposalId, setProposalId] = useState("");
  const hasFinished = (dueDate) => moment()._d > moment(dueDate)._d;
  const handleShare = (ev, normative) => {
    ev.preventDefault();
    setPostsId(normative.postsId);
    setProposalId(normative.id);
    setVisible(true);
    setModalURL(`${location.origin}/normativas/${normative.id}`);
  };

  return (
    <div>
      <Banner
        title="Normativa Colaborativa"
        subTitle="Danos tu opinion en las siguientes normativas a implementar"
      />
      <Spin tip="Cargando ..." size="large" spinning={loader}>
        <CardsContainer normatives={normatives} totalEls={count} pageSize={6}>
          {normatives.length > 0 ? (
            normatives.map((norm, i) => {
              return (
                <ProposalCard
                  key={i}
                  width={"100%"}
                  height={"max-content"}
                  title={norm.title}
                  description={{
                    text: `${norm.content}`,
                    link: `/normativas/${norm.id}`,
                  }}
                  isFinished={hasFinished(norm.dueDate)}
                  numberComments={norm.comments}
                  zones={["all"]}
                  onClickLogin={() => Router.push(`/normativas/${norm.id}`)}
                  onClickShare={(e) => handleShare(e, norm)}
                />
              );
            })
          ) : (
            <p className="my-96 text-center">No se encontraron resultados</p>
          )}
          <Shared
            visible={visible}
            href={modalURL}
            onCancel={() => setVisible(false)}
            closable={false}
            cancelText="Cancelar"
            userId={dataUser.id}
            postsId={postsId}
            proposalId={proposalId}
          />
        </CardsContainer>
      </Spin>
    </div>
  );
}

export default Normatives;
