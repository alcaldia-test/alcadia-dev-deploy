import React from "react";
import Link from "next/link";
import { ArrowLeftOutlined, ArrowRightOutlined } from "@ant-design/icons";
import StatsButton from "@components/Button/stats";
import styles from "../ChaptersFlow/styles/pageSelect.module.scss";

export default function PageSelect({
  _props: { pageNumber, currentPage, setPage, isAdmin, id },
}) {
  return (
    <div className={styles.pageSelectContainer}>
      {currentPage === 0 ? (
        <span className={styles.pageSelectLeft} onClick={() => setPage(0)}>
          <Link href={isAdmin ? "/admin/normativas" : "/normativas"}>
            <a>
              <ArrowLeftOutlined />
              Otras Normativas
            </a>
          </Link>
        </span>
      ) : currentPage === 1 ? (
        <span className={styles.pageSelectLeft} onClick={() => setPage(0)}>
          <ArrowLeftOutlined /> Inicio
        </span>
      ) : (
        <span
          className={styles.pageSelectLeft}
          onClick={() => setPage(currentPage - 1)}
        >
          <ArrowLeftOutlined /> Capítulo {currentPage - 1}
        </span>
      )}

      {currentPage === 0
        ? isAdmin && (
            <Link href={`/admin/estadisticas/normativas/${id}`}>
              <a>
                <StatsButton size="super-small" type="secondary" />
              </a>
            </Link>
          )
        : pageNumber > currentPage && (
            <span
              className={styles.pageSelectRight}
              onClick={() => setPage(currentPage + 1)}
            >
              <>
                Capítulo {currentPage + 1}
                <ArrowRightOutlined />
              </>
            </span>
          )}
    </div>
  );
}
