import React from "react";
import SocialNetwork from "@components/SocialNetwork";
import { RightOutlined, DownloadOutlined } from "@ant-design/icons";
import styles from "./index.module.scss";
import { parseDraft } from "@utils/draftToHtml";
import moment from "moment";
import "moment/locale/es";

const DescriptionView = ({
  _props: { normative, dataUser, buttons, setPage, hostname },
}) => {
  const { title, comments, content, createdAt, id, postsId, attached } =
    normative || {};
  const date = moment(createdAt);
  // console.log("Content", content);

  return (
    <>
      <section className={styles.mainContainer}>
        <div className={styles.descriptionViewContainer}>
          <h5>{title}</h5>
          <SocialNetwork
            url={`${hostname}/normativas/${id}`}
            comments={comments}
            month={date.format("MMMM")}
            day={date.format("DD")}
            userId={dataUser.id}
            postsId={postsId}
            proposalId={id}
          />

          <div
            className={`${styles.description} mb-16`}
            dangerouslySetInnerHTML={{
              __html: parseDraft(content),
            }}
          />
          <section>
            {attached && (
              <a
                target="_blank"
                className={styles.downLoadButton}
                href={`${attached[0]}?name=${attached[1]}`}
                rel="noreferrer"
              >
                Descargar PDF <DownloadOutlined />
              </a>
            )}
          </section>

          <section className={"mt-7"}>
            <h6>Capítulos de esta normativa</h6>

            {buttons.map((text, i) => (
              <span
                className={styles.chaptersButtons}
                onClick={() => setPage(i + 1)}
                key={i}
              >
                {text} <RightOutlined />
              </span>
            ))}
          </section>
        </div>
      </section>
    </>
  );
};
export default DescriptionView;
