import React, { useEffect, useState } from "react";
import PageSelect from "./PageSelect";
import styles from "./index.module.scss";
import ChapterView from "../ChaptersFlow/ChapterView";
import DescriptionView from "./descriptionView";
import moment from "moment";
import "moment/locale/es";
import { SupportNormative } from "../SupportNormative";
import Comments from "@components/Comments";
import Skeleton from "@components/Skeleton";

const BodyNormative = ({ isAdmin, normative, dataUser, myRef, hostname }) => {
  const { comments, chapters, postsId, state, id, createdAt } = normative || {};
  const [nComments, setNComments] = useState(comments);
  const [page, setPage] = useState(0);
  const date = moment(createdAt);
  const buttons = chapters.map((e, i) => `Ir a capítulo ${i + 1}`);
  const [chapter, setChapter] = useState({});

  useEffect(() => {
    if (page === 0) {
      setNComments(comments);
    } else {
      const chapfromArr = chapters[page - 1];
      setChapter({ ...chapfromArr, postsId: chapfromArr.id });
      setNComments(chapfromArr.comments);
    }
  }, [page, chapters]);

  return (
    <div ref={myRef} style={{ paddingTop: "20px" }}>
      <PageSelect
        _props={{
          pageNumber: buttons.length,
          currentPage: page,
          setPage,
          isAdmin,
          id,
        }}
      />
      {id ? (
        <div
          className={` ${styles.chapters} ${!isAdmin && styles.parcialWidth}`}
        >
          {page === 0 ? (
            <DescriptionView
              _props={{ normative, dataUser, buttons, setPage, hostname }}
            />
          ) : (
            <ChapterView
              state={state}
              chapterNumber={page - 1}
              chapters={chapters}
              date={date}
              dataUser={dataUser}
              postsId={postsId}
              myRef={myRef}
            />
          )}

          <SupportNormative
            normative={normative}
            chapter={chapter}
            isAdmin={isAdmin}
            page={page}
          />

          <Comments
            isAdmin={isAdmin}
            ncomments={nComments}
            module={page === 0 ? "posts" : "chapter"}
            updateComments={setNComments}
            mid={page === 0 ? postsId : chapters[page - 1].id}
          />
        </div>
      ) : (
        <Skeleton active />
      )}
    </div>
  );
};

export default BodyNormative;
