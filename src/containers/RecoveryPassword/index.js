import React, { useState } from "react";
import style from "./Login.module.scss";
import InputField from "@components/InputField";
import Button from "@components/Button";
import { useDispatch, useSelector } from "react-redux";
import { recoveryPasswordEmailStart } from "@redux/recoveyPassword/actions";
import message from "@components/Message";
import Spin from "@components/Spin";
import Router from "next/router";
import Link from "next/link";

const RecoveryPassword = () => {
  const dispatch = useDispatch();
  const { common } = useSelector((store) => store);
  const [emailValue, setemailValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [generalCode, setGeneralCode] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [identityCard, setIdentityCard] = useState("");

  const changeEmail = (e) => {
    setIdentityCard(e.target.value);
    if (e.target.value === "") {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    }

    setemailValue({
      ...emailValue,
      text: "",
      colorText: "#545454",
      colorInput: "rgba(26, 26, 26, 0.4)",
      status: true,
    });
  };

  const enviarDatos = async (e) => {
    e.preventDefault();

    if (identityCard === "") {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }

    if (emailValue.status) {
      try {
        const res = await new Promise((resolve, reject) =>
          dispatch(
            recoveryPasswordEmailStart({
              identityCard,
              resolve,
              reject,
            })
          )
        );
        message.success(res);
        Router.replace("/recovery-password/code");
      } catch (res) {
        setGeneralCode({
          text: res,
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      }
    }
  };
  return (
    <div className="flex flex-col align-middle justify-center w-full md:w-500 ">
      <div
        className={`absolute top-4 right-4 lg:right-200 lg:top-5 font-bold text-lg cursor-pointer ${style.blue_color_subtitle_line}`}
      >
        <Link href={"/"}>
          <p>Ir al sitio Web</p>
        </Link>
      </div>
      <h1
        className={`text-center font-bold text-2xl mb-5 mt-30 ${style.green_color_title}`}
      >
        ¿Olvidaste tu contraseña?
      </h1>
      <p className={`text-center font-bold text-lg mb-2 ${style.opp_color}`}>
        No te preocupes, recuperarla es fácil, solo tienes que ingresar tu
        correo electrónico con el que ingresaste en LaPazDecide
      </p>
      <p className={`text-center font-bold  mb-5  ${style.alert_color}`}>
        {generalCode.text || emailValue.text}
      </p>
      <Spin spinning={common.loader}>
        <InputField
          rightIcon="/images/icon/file-user-line.svg"
          name="email"
          label="Nro. de documento"
          onChange={changeEmail}
          value={emailValue.value}
          colorInput={emailValue.colorInput}
          colorText={emailValue.colorText}
        />
        <p className={style.caption}>C.I. con complemento: 1234-abc</p>
        <Button
          className=" mt-5 mb-4"
          onClick={enviarDatos}
          size="big"
          type="primary"
          style={{ width: "100%", height: "56rem" }}
        >
          Siguiente
        </Button>
      </Spin>
    </div>
  );
};
export default RecoveryPassword;
