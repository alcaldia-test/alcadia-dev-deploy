import React from "react";
import RegisterForm from "./components/Form";
import Image from "next/image";
import style from "./components/Register.module.scss";
import Link from "next/link";

export function Register() {
  return (
    <>
      <div className="flex flex-row justify-center items-center h-80 lg:h-111 relative w-full">
        <div className="absolute top-0 left-0 lg:left-3 w-54 h-54 lg:w-111 lg:h-111">
          <Image src="/images/logo.svg" layout="fill" />
        </div>
        <div className="place-self-center">
          <p className={`font-bold text-2xl ${style.primary_color}`}>
            Registro
          </p>
        </div>
        <div
          className={`absolute top-0 right-0 lg:right-5 lg:top-3 font-bold text-lg cursor-pointer ${style.blue_color}`}
        >
          <Link href={"/"}>
            <p>Ir al sitio Web</p>
          </Link>
        </div>
      </div>
      <div className="md:w-2/4 ">
        <div className="flex justify-center pb-3">
          <p className={`font-bold ${style.gris_color}`}>
            Llena los siguientes campos
          </p>
        </div>
        <RegisterForm />
      </div>
      <div className="flex justify-center w-full my-4">
        <p className={`text-lg font-bold mr-2 ${style.gris_color}`}>
          Ya tengo una cuenta
        </p>
        <Link href={"/login"}>
          <p
            className={`text-lg font-bold ml-2 cursor-pointer ${style.blue_color}`}
          >
            Iniciar Sesión
          </p>
        </Link>
      </div>
    </>
  );
}
export default Register;
