export const identificacion = [
  {
    value: "ci",
    label: "Cédula de Identidad",
    name: "typeDoc",
  },
  {
    value: "ci_with_comp",
    label: "CI con complemento",
    name: "typeDoc",
  },
  {
    value: "foreign",
    label: "Extranjero",
    name: "typeDoc",
  },
  {
    value: "passport",
    label: "Pasaporte",
    name: "typeDoc",
  },
];

export const typeDocDic = {
  passport: "Pasaporte",
  foreign: "Extranjero",
  ci_with_comp: "CI con complemento",
  ci: "Cédula de Identidad",
};

export const ciudades = [
  {
    value: 1,
    label: "La Paz",
    name: "issued",
  },
  {
    value: 2,
    label: "Cochabamba",
    name: "issued",
  },

  {
    value: 3,
    label: "Santa Cruz",
    name: "issued",
  },
  {
    value: 4,
    label: "Oruro",
    name: "issued",
  },
  {
    value: 5,
    label: "Potosi",
    name: "issued",
  },
  {
    value: 6,
    label: "Tarija",
    name: "issued",
  },
  {
    value: 7,
    label: "Beni",
    name: "issued",
  },
  {
    value: 8,
    label: "Pando",
    name: "issued",
  },
  {
    value: 9,
    label: "Chuquisaca",
    name: "issued",
  },
];

export const customStyles = (error = false) => ({
  option: (provided) => ({
    cursor: "pointer",
    color: "#1A1A1A",
    height: "50px",
    padding: "16px",
    fontWeight: 600,
    ":active": {
      ...provided[":active"],
      backgroundColor: "#00b1ba",
      color: "white",
    },
    ":hover": {
      borderRadius: "16px",
      backgroundColor: "#00b1ba",
      color: "white",
    },
  }),
  placeholder: (styles) => ({
    ...styles,
    color: "#959595",
  }),
  control: () => ({
    width: "100%",
    borderRadius: "16px",
    display: "flex",
    alignItems: "center",
    fontWeight: 600,
    height: "64px",
    border: `2px solid ${error ? "#fe4752" : "#9ca3af"}`,
    padding: "0 0 0 30px",
  }),
  menu: (provided) => ({
    ...provided,
    borderRadius: "16px",
    zIndex: 2,
  }),
});

export const optionsModal = [
  { id: "male", name: "Masculino" },
  { id: "female", name: "Femenino" },
  { id: "other", name: "Otro" },
];
