export const defaultForm = {
  birthDay: "",
  gender: "Masculino",
  terms: false,
  email: "",
  typeDoc: "",
  issued: "",
  captcha: null,
  comp: "",
  name: "",
  password: "",
  identityCard: "",
};

export const errorForm = {
  birthDay: undefined,
  email: undefined,
  typeDoc: undefined,
  password: undefined,
  identityCard: undefined,
  identityTaked: false,
};

const labels = {
  birthDay: "Fecha de Nacimieto",
  email: "Correo Electrónico",
  typeDoc: "Tipo de Documento",
  password: "Contraseña",
  identityCard: "Nro. de Documento",
};

export const validateRegister = (form = defaultForm) => {
  let errors = { ...errorForm };
  let allCorrect = true;

  for (const key in labels) {
    if (!form[key]) errors[key] = `El campo '${labels[key]}' debe ser llenado.`;
  }

  if (
    !form.firstName &&
    !form.name.match(/^\D{2,12}\s\D{2,12}(\s\D{2,12}){0,3}$/g)
  )
    errors.name = "Por favor, ingrese un nombre que sea valido.";

  if (!errors.password && !form.password.match(/^.{3,40}$/g))
    errors.password = "La contraseña no debe pasar los 40 caracteres.";

  if (!form.captcha) errors.captcha = "Por favor, marca la casilla";

  if (!form.issued && ["ci", "ci_with_comp"].includes(form.typeDoc))
    errors.issued = "El campo 'Expedido' debe ser llenado.";

  if (!errors.identityCard && !form.identityCard.match(/^[0-9]{3,13}$/g))
    errors.identityCard =
      "El nro. de documento tiene que ser entre 4 a 13 digitos.";

  if (form.typeDoc === "ci_with_comp" && !form.comp.match(/^[a-zA-Z]{3}$/g))
    errors.comp = "El complemento tiene que ser 3 letras.";

  if (!errors.email) {
    const validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
    if (!validEmail.test(form.email)) {
      errors.email = "Introduzca un email valido por favor....";
    }
  }

  for (const key in errors) {
    if (errors[key]) {
      allCorrect = false;
      break;
    }
  }

  if (allCorrect) errors = null;

  return errors;
};
