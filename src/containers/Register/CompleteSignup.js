import React, { useState } from "react";
import InputField from "@components/InputField";
import Button from "@components/Button";
import Select from "react-select";
import style from "./signup.module.scss";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";
import moment from "moment";
import RadioGroup from "@components/RadioGroup";
import {
  ciudades,
  identificacion,
  customStyles,
  optionsModal,
  typeDocDic,
} from "./assets";
import Spin from "@components/Spin";
import message from "@components/Message";

import DatePicker from "@components/DatePicker";
import Checkboxes from "@components/Checkboxes";
import Router from "next/router";

import Terms from "./components/Terms";
import { defaultForm, errorForm, validateRegister } from "./utils";
import { loginStart, registerUserStart } from "@redux/auth/actions";
import ReCAPTCHA from "react-google-recaptcha";

export function RegisterData() {
  const { dataUser } = useSelector((store) => store.auth);
  const [errors, setErrors] = useState(errorForm);
  const [termModal, setTermModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({ ...defaultForm, ...dataUser });

  const dispatch = useDispatch();

  const onChangeForm = (key, value) => {
    setForm({ ...form, [key]: value });
    setErrors({ ...errors, [key]: undefined });
  };

  const handleSubmit = async () => {
    const formData = {
      ...dataUser,
      ...form,
      birthDay: form.birthDay?._d,
    };

    const errors = validateRegister(formData);
    console.log(errors, formData);

    if (errors) return setErrors(errors);

    if (formData.typeDoc === "ci_with_comp")
      formData.identityCard += `-${formData.comp}`;

    delete formData.terms;
    delete formData.comp;
    delete formData.captcha;

    formData.typeDoc = typeDocDic[formData.typeDoc];

    try {
      setLoading(true);
      await new Promise((resolve, reject) =>
        dispatch(registerUserStart({ resolve, reject, formData }))
      );

      if (formData.identityCard && formData.password)
        await new Promise((resolve, reject) =>
          dispatch(
            loginStart({
              resolve,
              reject,
              identityCard: formData.identityCard,
              password: formData.password,
            })
          )
        );

      if (formData.emailVerified) Router.replace("/verification/zones");
      else Router.replace("/verification");
    } catch (error) {
      if (error.includes("409"))
        setErrors({
          identityTaked: true,
          identityCard: "El nro. del documento ya fue registrado",
        });
      else message.error("Ups! problemas con el servidor");

      setLoading(false);
    }
  };

  return (
    <>
      <div className={style.header}>
        <img src="/images/logo.svg" width={120} />
        <h4 className="text-2xl c-primary">Registro de Usuario</h4>
        <Link href={"/"}>
          <a className="c-turquoise font-bold">Ir al sitio Web</a>
        </Link>
      </div>
      <p className="font-bold text-center c-grey my-5">
        Llena los siguientes campos para el registro
      </p>
      <div className={style.container}>
        {!dataUser?.identityCard && (
          <>
            <div>
              <div className="relative rounded-2xl w-full items-stretch">
                <img
                  className={style["field-icon"]}
                  src="/images/icon/file-user-line.svg"
                  alt=""
                />
                <Select
                  placeholder="Tipo de documento"
                  styles={customStyles(!!errors.typeDoc)}
                  options={identificacion}
                  onChange={(tar) => onChangeForm("typeDoc", tar.value)}
                />
              </div>
              {errors.typeDoc && (
                <p className={style.error_message}>{errors.typeDoc}</p>
              )}
            </div>
            <div>
              <div className="flex gap-x-1">
                <InputField
                  rightIcon="/images/icon/file-user-line.svg"
                  type="text"
                  label="Nro. de Documento"
                  defaultValue={form.identityCard}
                  error={!!errors.identityCard}
                  onChange={(ev) =>
                    onChangeForm("identityCard", ev.target.value)
                  }
                />
                {form.typeDoc === "ci_with_comp" && (
                  <div className=" w-3/12 ">
                    <InputField
                      type="text"
                      label="Com..."
                      error={!!errors.comp}
                      defaultValue={form.comp}
                      onChange={(ev) => onChangeForm("comp", ev.target.value)}
                    />
                  </div>
                )}
              </div>
              {((errors.comp && form.typeDoc === "ci_with_comp") ||
                errors.identityCard) && (
                <p className={style.error_message}>
                  {errors.identityCard || errors.comp}
                  {errors.identityTaked && (
                    <Link href="/login">
                      <a className="font-bold c-turquoise">
                        &nbsp; Iniciar Sesion
                      </a>
                    </Link>
                  )}
                </p>
              )}
            </div>
            {["ci_with_comp", "ci"].includes(form.typeDoc) && (
              <div>
                <div className="relative rounded-2xl w-full items-stretch">
                  <img
                    className={style["field-icon"]}
                    src="/images/icon/file-user-line.svg"
                    alt="icon"
                  />
                  <Select
                    placeholder="Expedito en..."
                    styles={customStyles(!!errors.issued)}
                    options={ciudades}
                    onChange={(tar) => onChangeForm("issued", tar.label)}
                  />
                </div>
                {errors.issued && (
                  <p className={style.error_message}>{errors.issued}</p>
                )}
              </div>
            )}
          </>
        )}
        {!dataUser?.emailVerified && (
          <div>
            <InputField
              rightIcon="/images/icon/mail-line.svg"
              type="email"
              label="Correo Electrónico"
              error={!!errors.email}
              defaultValue={form.email}
              onChange={(ev) => onChangeForm("email", ev.target.value)}
            />
            {errors.email && (
              <p className={style.error_message}>{errors.email}</p>
            )}
          </div>
        )}
        {!dataUser?.password && (
          <>
            <div>
              <InputField
                rightIcon="/images/icon/user-line.svg"
                type="text"
                label="Nombre Completo"
                error={!!errors.name}
                onChange={(ev) => onChangeForm("name", ev.target.value)}
                value={form.name}
              />
              {errors.name && (
                <p className={style.error_message}>{errors.name}</p>
              )}
            </div>
            <div>
              <InputField
                rightIcon="/images/icon/lock-2-line.svg"
                type="password"
                label="Contraseña"
                error={!!errors.password}
                onChange={(ev) => onChangeForm("password", ev.target.value)}
                value={form.password}
              />
              {errors.password && (
                <p className={style.error_message}>{errors.password}</p>
              )}
            </div>
          </>
        )}
        <div>
          <DatePicker
            className={style.datepicker}
            style={{ borderColor: errors.birthDay ? "#fe4752" : "#9ca3af" }}
            disabledDate={(d) =>
              !d ||
              d.isSameOrBefore("1960-01-01") ||
              d.isSameOrAfter(moment().subtract(5, "years"))
            }
            format="DD-MM-YYYY"
            defaultPickerValue={moment("2002-12-31")}
            date={form.birthDay}
            placeholder="Fecha de nacimiento"
            onChange={(birth) => onChangeForm("birthDay", birth)}
          />
          {errors.birthDay && (
            <p className={style.error_message}>{errors.birthDay}</p>
          )}
        </div>
        <div>
          <h3 className="mb-2 text-center font-bold c-grey">Género</h3>
          <RadioGroup
            valueDefault="Masculino"
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "space-around",
            }}
            onChange={(ev) => onChangeForm("gender", ev.target.name)}
            options={optionsModal}
          />
        </div>
        <div className="flex justify-center my-4 items-center flex-col ">
          <ReCAPTCHA
            sitekey="6Leif5MfAAAAAFzqx5As7eERTVTvx_TfUH6Zh2HA"
            onChange={(captcha) => onChangeForm("captcha", captcha)}
            hl="ES"
          />
          {errors.captcha && (
            <p className={style.error_message}>{errors.captcha}</p>
          )}
        </div>
        <label className="text-lg font-bold c-grey my-3">
          <Checkboxes
            type="primary"
            onChange={(ev) => onChangeForm("terms", ev.target.checked)}
            required
          />
          Acepto los&nbsp;&nbsp;
          <span
            className="text-lg font-bold cursor-pointer c-turquoise"
            onClick={() => setTermModal(true)}
          >
            Términos y condiciones de servicio
          </span>
        </label>
        <Spin spinning={loading}>
          <div className="flex flex-col justify-center items-center py-4">
            <Button
              disabled={!form.terms}
              onClick={handleSubmit}
              size="big"
              type="primary"
              style={{ width: "100%", height: "56rem" }}
            >
              Registrarme
            </Button>
          </div>
        </Spin>
      </div>
      <div className="flex justify-center w-full my-4">
        <p className="text-lg font-bold mr-2 c-grey">
          Ya tengo una cuenta &nbsp;&nbsp;
          <Link href={"/login"}>
            <a className="c-turquoise">Iniciar Sesión</a>
          </Link>
        </p>
      </div>
      {termModal && <Terms setView={setTermModal} />}
    </>
  );
}

export default RegisterData;
