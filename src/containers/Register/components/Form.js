import React, { useReducer, useState, useEffect, useRef } from "react";
// import Link from "next/link";
// import { Button, Divider, Form, Input, Spin } from "antd";
// import { registerStart } from "@redux/auth/actions";
// import { useDispatch } from "react-redux";
import InputField from "@components/InputField";
import { useForm, Controller } from "react-hook-form";
// import Dropdown from "@components/Dropdown/Dropdown";
// import Checkboxes from "@components/Checkboxes/Checkboxes";
import Button from "@components/Button";
import Select from "react-select";
import styles from "./Register.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { registerUserStart } from "@redux/auth/actions";
import Spin from "@components/Spin";
import message from "@components/Message";
import Router from "next/router";
import Modal from "@components/Modals";
import Link from "next/link";
import { DatePicker } from "antd";
import moment from "moment";
import Checkboxes from "@components/Checkboxes";
import RadioGroup from "@components/RadioGroup";
import ReCAPTCHA from "react-google-recaptcha";

// eslint-disable-next-line no-unused-vars
import DatePickerComponent from "@components/DatePicker";
import { hideLoader } from "@redux/common/actions";
const customStyles = {
  option: (provided, state) => ({
    cursor: "pointer",
    color: "#1A1A1A",
    height: "50px",
    padding: "16px",
    // color: "rgba(156, 163, 175, 1)",
    fontWeight: 600,
    ":active": {
      ...provided[":active"],
      backgroundColor: "#00b1ba",
      color: "white",
    },
    ":hover": {
      borderRadius: "16px",
      backgroundColor: "#00b1ba",
      color: "white",
    },
  }),
  control: () => ({
    width: "100%",
    borderRadius: "16px",
    display: "flex",
    alignItems: "center",
    height: "64px",
    border: "2px solid #9ca3af",
    padding: "0 0 0 30px",
  }),
  menu: (provided, state) => ({
    ...provided,
    borderRadius: "16px",
  }),
};
const optionsModal = [
  { id: "Mujer", name: "Mujer" },
  { id: "Hombre", name: "Hombre" },
  { id: "Otro", name: "Otro" },
];
export function RegisterForm() {
  const { common } = useSelector((store) => store);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(hideLoader());
  }, []);
  const [docValue, setDocValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [docValueCom, setDocValueCom] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [birthValue, setbirthValue] = useState({
    value: null,
    text: "",
    colorText: "#545454",
    colorInput: "#9ca3af",
    status: false,
  });
  const [sexValue, setSexValue] = useState({
    value: "Mujer",
  });
  const [typeDocValue, setTypeDoc] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: true,
  });
  const [emailValue, setemailValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [nameValue, setNameValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [passwordValue, setpasswordValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [issuedValue, setissuedValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: true,
  });
  const [termValue, settermValue] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const [stateCaptcha, setStateCaptcha] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });
  const captcha = useRef(null);

  const [loginData, setloginData] = useState(false);
  const [loginData2, setloginData2] = useState(false);
  const [complement, setcomplement] = useState(false);

  const [modal, setModal] = useState(false);
  const identificacion = [
    {
      value: 1,
      label: "Cédula de Identidad",
      name: "typeDoc",
    },
    {
      value: 2,
      label: "CI con complemento",
      name: "typeDoc",
    },
    {
      value: 3,
      label: "Extranjero",
      name: "typeDoc",
    },
    {
      value: 4,
      label: "Pasaporte",
      name: "typeDoc",
    },
  ];

  const ciudades = [
    {
      value: 1,
      label: "La Paz",
      name: "issued",
    },
    {
      value: 2,
      label: "Cochabamba",
      name: "issued",
    },

    {
      value: 3,
      label: "Santa Cruz",
      name: "issued",
    },
    {
      value: 4,
      label: "Oruro",
      name: "issued",
    },
    {
      value: 5,
      label: "Potosi",
      name: "issued",
    },
    {
      value: 6,
      label: "Tarija",
      name: "issued",
    },
    {
      value: 7,
      label: "Beni",
      name: "issued",
    },
    {
      value: 8,
      label: "Pando",
      name: "issued",
    },
    {
      value: 9,
      label: "Chuquisaca",
      name: "issued",
    },
  ];
  const [generalAuth, setGeneralAuth] = useState({
    value: "",
    text: "",
    colorText: "#545454",
    colorInput: "rgba(26, 26, 26, 0.4)",
    status: false,
  });

  const formReducer = (state, event) => {
    return {
      ...state,
      [event.name]: event.value,
    };
  };
  const { register, handleSubmit, control } = useForm();
  const onSubmit = (data) => console.log(data);
  const [issues, setIssues] = useState(true);
  const [pasport, setPaspot] = useState(false);
  const [formData, setFormData] = useReducer(formReducer, {});
  const [valueChek, setValueChek] = useState(true);
  const handleChange = (event) => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };
  useEffect(() => {
    setFormData({
      name: "typeDoc",
      value: "Cédula de Identidad",
    });
    setFormData({
      name: "issued",
      value: "La Paz",
    });
  }, []);
  const handleSelect = (event) => {
    setFormData({
      name: event.name,
      value: event.label,
    });
  };
  const view = (e) => {
    if (e.target.checked) {
      setValueChek(false);
      settermValue({
        ...valueChek,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setValueChek(true);
      settermValue({
        ...valueChek,
        text: "Debe aceptar los términos y condiciones",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    }
  };
  const enviarDatos = (e) => {
    const numberVerificated = /^\d+$/;

    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordVerificated =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)([A-Za-z\d$@$!%*?&]|[^ ]){8,50}$/;

    const nameVerificy = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]{3,50}$/g;
    const passportVer = /^[A-Za-z0-9]+$/g;

    formData.name = nameValue.value;
    if (!pasport) {
      if (formData.identityCard === undefined) {
        setDocValue({
          ...docValue,
          text: "Debe ingresar su número de documento",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      } else if (!numberVerificated.test(formData.identityCard)) {
        setDocValue({
          ...docValue,
          text: "Solo puede ingresar números",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      } else if (formData.identityCard.length > 12) {
        setDocValue({
          ...docValue,
          text: "El número de dígitos no es válido",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      } else if (formData.identityCard.length < 4) {
        setDocValue({
          ...docValue,
          text: "El número de dígitos no es válido",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      }
    } else {
      if (formData.identityCard === undefined) {
        setDocValue({
          ...docValue,
          text: "Debe ingresar su número de documento",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      } else if (!passportVer.test(formData.identityCard)) {
        setDocValue({
          ...docValue,
          text: "Solo puede ingresar números y letras",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      } else if (formData.identityCard.length > 12) {
        setDocValue({
          ...docValue,
          text: "El número de dígitos no es válido",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      } else if (formData.identityCard.length < 4) {
        setDocValue({
          ...docValue,
          text: "El número de dígitos no es válido",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
      }
    }
    if (formData.typeDoc === undefined) {
      setTypeDoc({
        ...typeDocValue,
        text: "Debe seleccionar un tipo de documento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.email === undefined) {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!emailVerificated.test(formData.email)) {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar un correo electrónico válido",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.password === undefined) {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!passwordVerificated.test(formData.password)) {
      setpasswordValue({
        ...passwordValue,
        text: "La contraseña debe tener Mínimo ocho caracteres, al menos una letra mayúscula, una letra minúscula y un número",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.name === undefined) {
      setNameValue({
        ...nameValue,
        text: "Debe ingresar su nombre completo",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    } else if (!nameVerificy.test(formData.name)) {
      setNameValue({
        ...nameValue,
        text: "Debe ingresar Nombre y Apellido válidos",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.typeDoc === undefined) {
      setTypeDoc({
        ...typeDocValue,
        text: "Debe seleccionar un tipo de documento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (formData.issued === undefined) {
      setissuedValue({
        ...issuedValue,
        text: "Debe seleccionar un tipo de documento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (birthValue.value === undefined || birthValue.value === null) {
      setbirthValue({
        ...birthValue,
        text: "Debe seleccionar su fecha de nacimiento",
        colorText: "#545454",
        colorInput: "#FE4752",
      });
    } else {
      setbirthValue({
        ...birthValue,
        text: "",
        colorText: "#545454",
        colorInput: "#9ca3af",
      });
    }
    if (!stateCaptcha.status) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "Debe validar el captcha",
        colorText: "#FE4752",
        colorInput: "#FE4752",
      });
    }
    if (complement) {
      const numberVerificatedCom = /[a-zA-Z ]/;
      if (formData.complement === undefined) {
        setDocValueCom({
          ...docValueCom,
          text: "Debe ingresar su complemento",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
        return console.log("error");
      } else if (!numberVerificatedCom.test(formData.complement)) {
        setDocValueCom({
          ...docValueCom,
          text: "Solo puede ingresar 3 letras",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
        return console.log("error");
      } else if (formData.complement.length !== 3) {
        setDocValueCom({
          ...docValueCom,
          text: "Solo debe ingresar 3 caracteres",
          colorText: "#FE4752",
          colorInput: "#FE4752",
        });
        return console.log("error");
      }
      formData.identityCard = formData.identityCard + "-" + formData.complement;
    }
    formData.gender = sexValue.value;
    formData.birthDay = birthValue.value;
    if (
      docValue.status &&
      typeDocValue.status &&
      emailValue.status &&
      nameValue.status &&
      passwordValue.status &&
      termValue.status &&
      birthValue.status &&
      stateCaptcha.status
    ) {
      if (issues) {
        if (issuedValue.status) {
          return new Promise((resolve, reject) => {
            dispatch(
              registerUserStart({ resolve, reject, formData: formData })
            );
          })
            .then((res) => {
              message.success(res);
              Router.replace("/verification");
            })
            .catch((res) => {
              if (res.includes("409")) {
                setDocValue({
                  ...docValue,
                  text: `Este Nro. Ya está registrado en el sistema`,
                  colorText: "#FE4752",
                  colorInput: "#FE4752",
                  status: false,
                });
                setloginData2(true);
              } else if (res.includes("406")) {
                setemailValue({
                  ...emailValue,
                  text: `Este correo ya está registrado en el sistema`,
                  colorText: "#FE4752",
                  colorInput: "#FE4752",
                  status: false,
                });
                setloginData(true);
              } else if (res.includes("504")) {
                setGeneralAuth({
                  text: `Error del sistema IGOB, favor vuelve a intentar más tarde`,
                  colorText: "#FE4752",
                  colorInput: "#FE4752",
                  status: false,
                });
              } else {
                setGeneralAuth({
                  text: "No se pudo registrar el usuario",
                  colorText: "#FE4752",
                  colorInput: "#FE4752",
                  status: false,
                });
              }
            });
        }
      } else {
        formData.issued = "La paz";
        return new Promise((resolve, reject) => {
          dispatch(registerUserStart({ resolve, reject, formData: formData }));
        })
          .then((res) => {
            message.success(res);
            Router.replace("/verification");
          })
          .catch((res) => {
            console.log(res);
            console.log(res.includes("406"));
            if (res.includes("409")) {
              setDocValue({
                ...docValue,
                text: `Este Nro. Ya está registrado en el sistema`,
                colorText: "#FE4752",
                colorInput: "#FE4752",
                status: false,
              });
              setloginData2(true);
            } else if (res.includes("406")) {
              setemailValue({
                ...emailValue,
                text: `Este correo ya está registrado en el sistema`,
                colorText: "#FE4752",
                colorInput: "#FE4752",
                status: false,
              });
              setloginData(true);
            } else if (res.includes("504")) {
              setGeneralAuth({
                text: `Error del sistema IGOB, favor vuelve a intentar más tarde`,
                colorText: "#FE4752",
                colorInput: "#FE4752",
                status: false,
              });
            } else {
              setGeneralAuth({
                text: "No se pudo registrar el usuario",
                colorText: "#FE4752",
                colorInput: "#FE4752",
                status: false,
              });
            }
          });
      }
    }
  };
  const changeComplement = (e) => {
    handleChange(e);
    const numberVerificatedCom = /[a-zA-Z ]{2,254}/;
    if (e.target.value === "") {
      setDocValueCom({
        ...docValueCom,
        value: e.target.value,
        text: "Debe ingresar el complemento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setDocValueCom({
        ...docValueCom,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (numberVerificatedCom?.test(e.target.value)) {
      setDocValueCom({
        ...docValueCom,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setDocValueCom({
        ...docValueCom,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
    if (e.target.value.length !== 3) {
      setDocValueCom({
        ...docValueCom,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    } else {
      setDocValueCom({
        ...docValueCom,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  const changeDocument = (e) => {
    setloginData2(false);
    const numberVerificated = /^\d+$/;
    const passportVer = /^[A-Za-z0-9]+$/g;
    handleChange(e);
    if (e.target.value === "") {
      setDocValue({
        ...docValue,
        text: "Debe ingresar su número de documento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }

    if (!pasport) {
      if (numberVerificated?.test(e.target.value)) {
        setDocValue({
          ...docValue,
          text: "",
          colorText: "#545454",
          colorInput: "rgba(26, 26, 26, 0.4)",
          status: true,
        });
      } else {
        setDocValue({
          ...docValue,
          text: "",
          colorText: "#545454",
          colorInput: "rgba(26, 26, 26, 0.4)",
          status: false,
        });
      }
    } else {
      if (passportVer?.test(e.target.value)) {
        setDocValue({
          ...docValue,
          text: "",
          colorText: "#545454",
          colorInput: "rgba(26, 26, 26, 0.4)",
          status: true,
        });
      } else {
        setDocValue({
          ...docValue,
          text: "",
          colorText: "#545454",
          colorInput: "rgba(26, 26, 26, 0.4)",
          status: false,
        });
      }
    }

    if (e.target.value.length > 12) {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
    if (e.target.value.length < 4) {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    } else {
      setDocValue({
        ...docValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  const changeEmail = (e) => {
    setloginData(false);
    const emailVerificated =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    handleChange(e);
    if (e.target.value === "") {
      setemailValue({
        ...emailValue,
        text: "Debe ingresar su correo electrónico",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (emailVerificated.test(e.target.value)) {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setemailValue({
        ...emailValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const changeName = (e) => {
    const nameVerificy = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]{3,50}$/g;
    handleChange(e);
    if (e.target.value === "") {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "Debe ingresar su nombre completo",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (nameVerificy?.test(e.target.value)) {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setNameValue({
        ...nameValue,
        value: e.target.value,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const changePassword = (e) => {
    const passwordVerificated =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)([A-Za-z\d$@$!%*?&]|[^ ]){8,50}$/;
    handleChange(e);
    if (e.target.value === "") {
      setpasswordValue({
        ...passwordValue,
        text: "Debe ingresar su contraseña",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (passwordVerificated.test(e.target.value)) {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    } else {
      setpasswordValue({
        ...passwordValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: false,
      });
    }
  };
  const selectChange = (e) => {
    handleSelect(e);
    if (e.label === "Seleccione tipo de documento") {
      setTypeDoc({
        ...typeDocValue,
        text: "Debe seleccionar un tipo de documento",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setTypeDoc({
        ...typeDocValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
    if (e.label === "Cédula de Identidad") {
      setIssues(true);
    } else {
      setIssues(false);
    }
    if (e.label === "CI con complemento") {
      setcomplement(true);
    } else {
      setcomplement(false);
    }
    if (e.label === "Pasaporte") {
      setPaspot(true);
    } else {
      setPaspot(false);
    }
  };
  const selectIssues = (e) => {
    handleSelect(e);
    if (e.label === "Seleccione donde se expidió su C.I.") {
      setissuedValue({
        ...typeDocValue,
        text: "Debe seleccionar donde fue expedido su C.I.",
        colorText: "#FE4752",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setissuedValue({
        ...typeDocValue,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  const openModal = () => {
    setModal(true);
  };
  const cancelModal = () => {
    setModal(false);
  };
  const datas = new Date();
  const setDates = (e) => {
    if (e === null) {
      setbirthValue({
        ...birthValue,
        value: e?.format(),
        text: "Debe seleccionar su fecha de nacimiento",
        colorText: "#545454",
        colorInput: "#FE4752",
        status: false,
      });
    } else {
      setbirthValue({
        ...birthValue,
        value: e?.format(),
        text: "",
        colorText: "#545454",
        colorInput: "#9ca3af",
        status: true,
      });
    }
  };
  const handleChangeSex = (e) => {
    setSexValue({
      ...sexValue,
      value: e.target.value,
    });
  };
  const changeCaptcha = (e) => {
    if (captcha.current.getValue()) {
      setStateCaptcha({
        ...stateCaptcha,
        text: "",
        colorText: "#545454",
        colorInput: "rgba(26, 26, 26, 0.4)",
        status: true,
      });
    }
  };
  return (
    <>
      <p className={`text-center font-bold mb-2 mt-1  ${styles.alert_color}`}>
        {generalAuth.text}
      </p>
      <Spin spinning={common.loader}>
        <form className=" flex-1" onSubmit={handleSubmit(onSubmit)}>
          <div className="z-5 relative rounded-2xl content-center w-full items-stretch">
            <span
              className={`flex z-10 h-full leading-snug font-normal absolute text-center content-center justify-center w-5 pl-2 py-2 `}
            >
              <img src="/images/icon/file-user-line.svg" alt="" />
            </span>
            <Controller
              render={({ field }) => (
                <Select
                  {...field}
                  defaultValue={identificacion[0]}
                  styles={customStyles}
                  options={identificacion}
                  onChange={selectChange}
                />
              )}
              name="typeDoc"
              control={control}
            />
          </div>{" "}
          <p className={` mb-2 mt-1  ${styles.alert_color}`}>
            {typeDocValue.text}
          </p>
          <div className="flex gap-x-1">
            <InputField
              rightIcon="/images/icon/file-user-line.svg"
              type="text"
              name="identityCard"
              label="Nro. de Documento"
              onChange={changeDocument}
              value={docValue.value}
              colorInput={docValue.colorInput}
              colorText={docValue.colorText}
              color=""
            />
            {complement ? (
              <div className=" w-3/12 ">
                <InputField
                  type="text"
                  name="complement"
                  label="Com"
                  onChange={changeComplement}
                  colorInput={docValueCom.colorInput}
                  colorText={docValueCom.colorText}
                />
              </div>
            ) : (
              ""
            )}
          </div>
          <p className={` mb-2 mt-1  ${styles.alert_color}`}>
            {docValueCom.text}
          </p>
          <p className={` mb-2 mt-1  ${styles.alert_color}`}>
            {docValue.text}
            {loginData2 ? (
              <span
                className={`block lg:inline font-bold ${styles.blue_color}`}
              >
                <Link href="/login"> Iniciar Sesion</Link>
              </span>
            ) : (
              ""
            )}
          </p>
          {issues ? (
            <>
              <Controller
                render={({ field }) => (
                  <Select
                    {...field}
                    defaultValue={ciudades[0]}
                    styles={customStyles}
                    options={ciudades}
                    onChange={selectIssues}
                  />
                )}
                name="issued"
                control={control}
              />
              <p className={` mb-2 mt-1  ${styles.alert_color}`}>
                {issuedValue.text}
              </p>
            </>
          ) : (
            ""
          )}
          <InputField
            rightIcon="/images/icon/mail-line.svg"
            type="email"
            name="email"
            label="Correo Electrónico"
            onChange={changeEmail}
            value={emailValue.value}
            colorInput={emailValue.colorInput}
            colorText={emailValue.colorText}
          />{" "}
          <p className={` mb-2 mt-1  ${styles.alert_color}`}>
            {emailValue.text}{" "}
            {loginData ? (
              <span
                className={`block lg:inline font-bold ${styles.blue_color}`}
              >
                <Link href="/login"> Iniciar Sesion</Link>
              </span>
            ) : (
              ""
            )}
          </p>
          <InputField
            rightIcon="/images/icon/user-line.svg"
            type="text"
            name="name"
            label="Nombre Completo"
            onChange={changeName}
            value={nameValue.value}
            colorInput={nameValue.colorInput}
            colorText={nameValue.colorText}
          />{" "}
          <p className={` mb-2 mt-1  ${styles.alert_color}`}>
            {nameValue.text}
          </p>
          <InputField
            rightIcon="/images/icon/lock-2-line.svg"
            type="password"
            name="password"
            label="Contraseña"
            onChange={changePassword}
            value={passwordValue.value}
            colorInput={passwordValue.colorInput}
            colorText={passwordValue.colorText}
          />{" "}
          <p className={` mb-2 mt-2  ${styles.alert_color}`}>
            {passwordValue.text}
          </p>
          <div
            className="dateconente pl-4 cursor-pointer flex border-2 justify-center items-center rounded-large h-54"
            style={{ borderColor: birthValue.colorInput }}
          >
            <DatePicker
              style={{
                width: "100%",
                cursor: "pointer",
              }}
              name="birthDate"
              disabledDate={(d) =>
                !d ||
                d.isAfter(datas.toJSON()) ||
                d.isSameOrBefore("1960-01-01")
              }
              format="YYYY-MM-DD"
              defaultPickerValue={moment("2002-12-31")}
              // disabledDate={disabledDate}
              placeholder="Fecha de nacimiento"
              onChange={setDates}
            />
          </div>
          <p className={` mb-2 mt-2  ${styles.alert_color}`}>
            {birthValue.text}
          </p>
          <div className=" mt-2 mb-4 w-full">
            <h3 className={`mb-2 text-center font-bold ${styles.gris_color}`}>
              Sexo
            </h3>
            <RadioGroup
              valueDefault={"Mujer"}
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "space-around",
              }}
              onChange={handleChangeSex}
              options={optionsModal}
            />
          </div>
          <div className="flex justify-center my-4 items-center flex-col ">
            <ReCAPTCHA
              ref={captcha}
              sitekey="6Leif5MfAAAAAFzqx5As7eERTVTvx_TfUH6Zh2HA"
              onChange={changeCaptcha}
            />
            <p className={` mb-2 mt-1  ${styles.alert_color}`}>
              {stateCaptcha.text}
            </p>
          </div>
          <label className={`text-lg font-bold  ${styles.gris_white}`}>
            <Checkboxes
              type="primary"
              className="w-4 h-4 mx-3 mt-2"
              {...register("checkbox")}
              onChange={view}
              required
            />
            Acepto los{" "}
            <span
              className={`text-lg font-bold cursor-pointer ${styles.blue_color}`}
              onClick={openModal}
            >
              Términos y condiciones de servicio
            </span>
          </label>
          <p className={` mb-2 mt-1  ${styles.alert_color}`}>
            {termValue.text}
          </p>
          <div className="flex flex-col justify-center items-center py-4">
            <Button
              disabled={valueChek}
              onClick={handleSubmit(enviarDatos)}
              size="big"
              type="primary"
              style={{ width: "100%", height: "56rem" }}
            >
              Registrarme
            </Button>
          </div>
        </form>
      </Spin>
      <Modal visible={modal} width={1000} onCancel={cancelModal}>
        <div className=" px-2 ">
          <h2 className="font-bold text-3xl mt-2 text-center">
            CONDICIONES DE USO
          </h2>
          <h4 className="font-bold text-lg mt-1 text-center">
            PLATAFORMA TECNOLÓGICA DE PARTICIPACIÓN CIUDADANA
          </h4>
          <h4
            className={`font-bold text-lg text-center ${styles.primary_color}`}
          >
            “LA PAZ PARTICIPA”
          </h4>
          <p className="font-bold text-lg text-justify mt-1 text-textColor2">
            La Plataforma Tecnológica de Participación Ciudadana “La Paz
            Participa”, implementada por el Gobierno Autónomo Municipal de La
            Paz es una herramienta que busca fomentar la participación activa de
            las ciudadanas y los ciudadanos del Municipio de La Paz, como un
            canal director de comunicación con su Municipio. Proponiendo,
            apoyando, compartiendo y votando propuestas relacionadas con la
            mejora del Municipio y que contribuyan a la toma de decisiones.
          </p>
          <h4 className={`font-bold text-lg text-left mt-2`}>
            CONDICIONES DE USO
          </h4>
          <ol className=" list-decimal pl-4 text-textColor2">
            <li className="font-bold text-lg text-justify mt-1">
              Podrá participar cualquier ciudadana o ciudadano habiendo cumplido
              la mayoría de edad con plena capacidad de obrar por sí mismo en
              todos los actos de la vida civil, previo registro y validación
              voluntaria en la Plataforma Tecnológica de Participación Ciudadana
              “La Paz Participa”.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              Los menores de edad que quieran participar de manera voluntaria,
              lo realizarán por intermedio de sus padres y/o tutores, quienes
              serán responsables en el registro y validación en la plataforma,
              como también de la intervención activa que tengan estos en
              Plataforma Tecnológica de Participación Ciudadana “La Paz
              Participa”. Consiguientemente, la participación de un menor de
              edad en cualquier etapa de la plataforma, será de exclusiva
              responsabilidad de quien se encuentre registrado como padre o
              tutor.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              No podrá participar en la Plataforma Tecnológica de Participación
              Ciudadana “La Paz Participa” el personal relacionado con la
              gestión municipal dependiente del Gobierno Autónomo Municipal de
              La Paz, cualquiera sea la modalidad de contratación (Planta,
              Contrato, Consultoría).
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              El Gobierno Autónomo Municipal de La Paz, no se responsabiliza de
              las acciones, el contenido, la información o los datos que
              suministren los usuarios o terceros a través de la Plataforma
              Tecnológica de Participación Ciudadana “La Paz Participa”, y en
              consecuencia deslinda responsabilidad por los eventuales daños que
              pudieran generarse, así también, de las eventuales acciones
              administrativas o legales que pudieran atribuir a los
              responsables.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              El Gobierno Autónomo Municipal de La Paz se reserva, por
              consiguiente, el derecho a limitar el acceso a la Plataforma
              Tecnológica de Participación Ciudadana “La Paz Participa”, de
              opiniones, informaciones, comentarios o documentos que los
              usuarios quieran incorporar como los señalados a continuación:
              <ul className=" list-disc pl-4 my-8">
                <li className="font-bold text-lg text-justify mt-1">
                  Compartir cualquier contenido que pueda ser considerado como
                  una vulneración de los derechos fundamentales y/o
                  constitucionales, el honor, la dignidad, imagen e intimidad
                  personal y familiar de terceros.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Compartir imágenes o fotografías que exterioricen de manera
                  pública datos personales de terceros sin haber obtenido él
                  oportuno consentimiento de sus titulares.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Compartir cualquier contenido que vulnere el secreto en las
                  comunicaciones, la infracción de derechos de propiedad
                  industrial e intelectual o de las normas reguladoras de la
                  protección de datos de carácter personal.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Reproducir, distribuir, compartir contenidos, informaciones o
                  imágenes que hayan sido puestas a disposición por otros
                  usuarios sin la autorización expresa de los mismos.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  El mal uso de la plataforma con fines de publicidad.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Cualquier otra finalidad ilícita y/o que contraríe la moral
                  pública y las buenas costumbres.
                </li>
              </ul>
              La realización de cualquiera de los anteriores comportamientos
              permitirá el accionar del Gobierno Autónomo Municipal de La Paz, a
              suspender temporalmente la actividad de un participante,
              inhabilitar su cuenta y/o borrar su contenido, sin perjuicio de
              otras responsabilidades que puedan ser ejercidas, pudiendo adoptar
              todas las medidas que el caso amerite.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              En caso de que el contenido introducido por los usuarios incorpore
              un enlace a otro sitio web, el Gobierno Municipal no será
              responsable por los daños y perjuicios derivados del acceso al
              enlace o a sus contenidos. Siendo de entera responsabilidad del
              usuario.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              En caso de litigio de cualquier clase o por cualquier motivo entre
              los participantes en la plataforma web y/o un tercero, él Gobierno
              Autónomo Municipal de La Paz quedará exento de cualquier
              responsabilidad administrativa, civil o penal por reclamos,
              demandas o daños de cualquier naturaleza relacionados o derivados
              del litigio.
            </li>
          </ol>
          <h5 className={`font-bold text-lg text-center mt-2`}>
            DECLARO HABER TOMADO CONOCIMIENTO Y ACEPTADO TODAS Y CADA UNA DE LAS
            CONDICIONES DE USO DE MANERA VOLUNTARIA: PARA OBTENER LAS
            CREDENCIALES DE ACCESO A LA PLATAFORMA TECNOLÓGICA DE PARTICIPACIÓN
            CIUDADANA “LA PAZ PARTICIPA”.{" "}
          </h5>
          <div className="flex justify-center mt-4">
            <Button
              type="primary"
              style={{ height: "45rem" }}
              onClick={cancelModal}
            >
              Volver
            </Button>
          </div>
        </div>
      </Modal>
    </>
  );
}
export default RegisterForm;
