import React from "react";
import Button from "@components/Button";
import styles from "./Register.module.scss";
import Modal from "@components/Modals/SimpleModal";

export function AcceptTerms({ setView }) {
  const cancelModal = () => {
    setView(false);
  };
  return (
    <>
      <Modal
        visible={true}
        style={{ top: 10 }}
        onCancel={cancelModal}
        width={900}
      >
        <div className=" px-2 ">
          <h2 className="font-bold text-3xl mt-2 text-center">
            CONDICIONES DE USO
          </h2>
          <h4 className="font-bold text-lg mt-1 text-center">
            PLATAFORMA TECNOLÓGICA DE PARTICIPACIÓN CIUDADANA
          </h4>
          <h4
            className={`font-bold text-lg text-center ${styles.primary_color}`}
          >
            “LA PAZ PARTICIPA”
          </h4>
          <p className="font-bold text-lg text-justify mt-1 text-textColor2">
            La Plataforma Tecnológica de Participación Ciudadana “La Paz
            Participa”, implementada por el Gobierno Autónomo Municipal de La
            Paz es una herramienta que busca fomentar la participación activa de
            las ciudadanas y los ciudadanos del Municipio de La Paz, como un
            canal director de comunicación con su Municipio. Proponiendo,
            apoyando, compartiendo y votando propuestas relacionadas con la
            mejora del Municipio y que contribuyan a la toma de decisiones.
          </p>
          <h4 className={`font-bold text-lg text-left mt-2`}>
            CONDICIONES DE USO
          </h4>
          <ol className=" list-decimal pl-4 text-textColor2">
            <li className="font-bold text-lg text-justify mt-1">
              Podrá participar cualquier ciudadana o ciudadano habiendo cumplido
              la mayoría de edad con plena capacidad de obrar por sí mismo en
              todos los actos de la vida civil, previo registro y validación
              voluntaria en la Plataforma Tecnológica de Participación Ciudadana
              “La Paz Participa”.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              Los menores de edad que quieran participar de manera voluntaria,
              lo realizarán por intermedio de sus padres y/o tutores, quienes
              serán responsables en el registro y validación en la plataforma,
              como también de la intervención activa que tengan estos en
              Plataforma Tecnológica de Participación Ciudadana “La Paz
              Participa”. Consiguientemente, la participación de un menor de
              edad en cualquier etapa de la plataforma, será de exclusiva
              responsabilidad de quien se encuentre registrado como padre o
              tutor.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              No podrá participar en la Plataforma Tecnológica de Participación
              Ciudadana “La Paz Participa” el personal relacionado con la
              gestión municipal dependiente del Gobierno Autónomo Municipal de
              La Paz, cualquiera sea la modalidad de contratación (Planta,
              Contrato, Consultoría).
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              El Gobierno Autónomo Municipal de La Paz, no se responsabiliza de
              las acciones, el contenido, la información o los datos que
              suministren los usuarios o terceros a través de la Plataforma
              Tecnológica de Participación Ciudadana “La Paz Participa”, y en
              consecuencia deslinda responsabilidad por los eventuales daños que
              pudieran generarse, así también, de las eventuales acciones
              administrativas o legales que pudieran atribuir a los
              responsables.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              El Gobierno Autónomo Municipal de La Paz se reserva, por
              consiguiente, el derecho a limitar el acceso a la Plataforma
              Tecnológica de Participación Ciudadana “La Paz Participa”, de
              opiniones, informaciones, comentarios o documentos que los
              usuarios quieran incorporar como los señalados a continuación:
              <ul className=" list-disc pl-4 my-8">
                <li className="font-bold text-lg text-justify mt-1">
                  Compartir cualquier contenido que pueda ser considerado como
                  una vulneración de los derechos fundamentales y/o
                  constitucionales, el honor, la dignidad, imagen e intimidad
                  personal y familiar de terceros.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Compartir imágenes o fotografías que exterioricen de manera
                  pública datos personales de terceros sin haber obtenido él
                  oportuno consentimiento de sus titulares.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Compartir cualquier contenido que vulnere el secreto en las
                  comunicaciones, la infracción de derechos de propiedad
                  industrial e intelectual o de las normas reguladoras de la
                  protección de datos de carácter personal.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Reproducir, distribuir, compartir contenidos, informaciones o
                  imágenes que hayan sido puestas a disposición por otros
                  usuarios sin la autorización expresa de los mismos.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  El mal uso de la plataforma con fines de publicidad.
                </li>
                <li className="font-bold text-lg text-justify mt-1">
                  Cualquier otra finalidad ilícita y/o que contraríe la moral
                  pública y las buenas costumbres.
                </li>
              </ul>
              La realización de cualquiera de los anteriores comportamientos
              permitirá el accionar del Gobierno Autónomo Municipal de La Paz, a
              suspender temporalmente la actividad de un participante,
              inhabilitar su cuenta y/o borrar su contenido, sin perjuicio de
              otras responsabilidades que puedan ser ejercidas, pudiendo adoptar
              todas las medidas que el caso amerite.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              En caso de que el contenido introducido por los usuarios incorpore
              un enlace a otro sitio web, el Gobierno Municipal no será
              responsable por los daños y perjuicios derivados del acceso al
              enlace o a sus contenidos. Siendo de entera responsabilidad del
              usuario.
            </li>
            <li className="font-bold text-lg text-justify mt-1">
              En caso de litigio de cualquier clase o por cualquier motivo entre
              los participantes en la plataforma web y/o un tercero, él Gobierno
              Autónomo Municipal de La Paz quedará exento de cualquier
              responsabilidad administrativa, civil o penal por reclamos,
              demandas o daños de cualquier naturaleza relacionados o derivados
              del litigio.
            </li>
          </ol>
          <h5 className={`font-bold text-lg text-center mt-2`}>
            DECLARO HABER TOMADO CONOCIMIENTO Y ACEPTADO TODAS Y CADA UNA DE LAS
            CONDICIONES DE USO DE MANERA VOLUNTARIA: PARA OBTENER LAS
            CREDENCIALES DE ACCESO A LA PLATAFORMA TECNOLÓGICA DE PARTICIPACIÓN
            CIUDADANA “LA PAZ PARTICIPA”.{" "}
          </h5>
          <div className="flex justify-center mt-4">
            <Button
              type="primary"
              style={{ height: "45rem" }}
              onClick={cancelModal}
            >
              Volver
            </Button>
          </div>
        </div>
      </Modal>
    </>
  );
}
export default AcceptTerms;
