import React, { useEffect } from "react";
import DynamicCarrousel from "./components/DynamicCarrousel";
import HowParticipate from "./components/HowParticipate";
import FeaturedContentGrid from "@components/Grids/FeaturedContentGrid";
import { requestLandingStart } from "@redux/landing/actions";
import { useDispatch, useSelector } from "react-redux";
import OutstandingVotes from "@components/Grids/FeaturedContentGrid/outstandingVotes";
import OutstandingAnnouncements from "@components/Grids/FeaturedContentGrid/outstandingAnnouncements";
import OutstandingDiscussions from "@components/Grids/FeaturedContentGrid/outstandingDiscussions";
import OutstandingNormatives from "@components/Grids/FeaturedContentGrid/outstandingNormatives";
import Achievements from "./components/Achievements";
import Spin from "@components/Spin";

function LandingContainer() {
  const { achievements, announcements, discussions, votings, normatives } =
    useSelector((state) => state.landing);
  const { loader } = useSelector((data) => data.common);
  console.warn({ achievements });

  const achieves = achievements.map((ach) => ({
    image: ach.fileLink,
    title: ach.title,
    id: ach.id,
  }));

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(requestLandingStart({}));
  }, []);

  return (
    <Spin spinning={loader} tip={`Cargando ...`} size="large">
      <DynamicCarrousel />
      <HowParticipate />
      <FeaturedContentGrid
        linkLandingModule="/tu_voto_cuenta"
        title="Tu Opinión Cuenta - Destacados"
        mas="Ver más "
        elements={votings.slice(0, 3)}
        cardsType={<OutstandingVotes elements={votings.slice(0, 3)} />}
      />
      <FeaturedContentGrid
        linkLandingModule="/propuestas_ciudadanas"
        title="Propuestas Ciudadanas Destacadas"
        elements={announcements.slice(0, 3)}
        mas="Ir a Propuestas Ciudadanas"
        cardsType={
          <OutstandingAnnouncements elements={announcements.slice(0, 3)} />
        }
      />
      {achieves[0] && (
        <Achievements
          items={achieves}
          heightImage="200rem"
          heightCarousel="240rem"
        />
      )}

      <FeaturedContentGrid
        linkLandingModule="/charlas"
        title="Charlas Virtuales Destacadas"
        elements={discussions.slice(0, 3)}
        mas="Ir a Charlas Virtuales"
        cardsType={
          <OutstandingDiscussions elements={discussions.slice(0, 3)} />
        }
      />
      <FeaturedContentGrid
        linkLandingModule="/normativas"
        title="Normativas Colaborativas Destacadas"
        elements={normatives.slice(0, 3)}
        mas="Ir a normativas colaborativas"
        cardsType={<OutstandingNormatives elements={normatives.slice(0, 3)} />}
      />
    </Spin>
  );
}

export default LandingContainer;
