import React from "react";
import Image from "next/image";

export default function HowParticipate() {
  return (
    <div className="text-center mt-3">
      <Image
        alt="¿Cómo participar?"
        src="/images/participar.jpg"
        layout="intrinsic"
        width={1200}
        height={236}
      />
    </div>
  );
}
