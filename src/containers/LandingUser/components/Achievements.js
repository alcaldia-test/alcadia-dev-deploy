import React from "react";
import AttainmentCarousel from "@components/Carousel/AttainmentCarousel";
import styles from "../landing.module.scss";
import Title from "antd/lib/typography/Title";

export default function Achievements({ items, heightImage, heightCarousel }) {
  return (
    <section className={styles.achievements}>
      <Title className="text-2xl mb-1 text-center">Logros de la Alcaldía</Title>

      <AttainmentCarousel
        items={items}
        heightImage={heightImage}
        heightCarousel={heightCarousel}
      />
    </section>
  );
}
