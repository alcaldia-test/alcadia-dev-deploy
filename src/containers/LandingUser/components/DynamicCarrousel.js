import React, { useEffect, useRef, useState } from "react";
import Carousel from "@components/Carousel";
import styles from "../landing.module.scss";
import { useSelector } from "react-redux";
import Title from "antd/lib/typography/Title";
import Button from "@components/Button";
import Link from "next/link";
import Image from "next/image";

export default function DynamicCarrousel() {
  const [banner, setBanner] = useState({});
  const [width, setWidth] = useState(0);

  const [links, setLinks] = useState([]);
  const { banners } = useSelector((state) => state.global);

  useEffect(() => {
    const bannerFound = banners.find(({ key }) => key === "landing");
    console.warn({ bannerFound });
    const linksFound = [];
    bannerFound?.fileStorages?.forEach(({ link }) => linksFound.push(link));
    setBanner(bannerFound);
    setLinks(linksFound);
  }, [banners]);

  const carrouselParent = useRef(null);
  useEffect(() => {
    if (carrouselParent.current) {
      const width = carrouselParent.current.offsetWidth;
      setWidth(width);
    }
  }, [carrouselParent]);

  return (
    <div className={styles.landing} ref={carrouselParent}>
      {links.length > 0 ? (
        <>
          <Carousel
            width={width}
            height={width / 3}
            autoplay={true}
            items={links.map((link, i) => (
              <>
                <div className={styles.landingCover}></div>
                <Image
                  key={i}
                  src={link}
                  width={links.length === 1 ? width : 1200}
                  height={links.length === 1 ? width / 3 : 400}
                  layout={links.length === 1 ? "fixed" : "responsive"}
                />
              </>
            ))}
          />
          <div className={styles.littleDescription}>
            <Title className="text-2xl mb-1 text-center font-bold">
              {banner?.title}
            </Title>
            <p>{banner?.content}</p>
            <div className="flex justify-center mt-3 mb-1">
              <Button type="primary" size="small-x">
                <Link href={banner?.link}>{banner?.button}</Link>
              </Button>
            </div>
          </div>
        </>
      ) : (
        <div className={styles.loading}>
          <Image
            src="/gifts/Spin-1s-200px.gif"
            alt="loading"
            width={60}
            height={60}
            layout="fixed"
          />
        </div>
      )}
    </div>
  );
}
