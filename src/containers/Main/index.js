import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import {
  requestGlobalStart,
  notificationCountSuccess,
  increaseUserNotify,
} from "@redux/global/actions";
import io from "socket.io-client";

import { ConfigProvider } from "antd";
import esES from "antd/lib/locale/es_ES";

import { Notification } from "@assets/icons";
import notification from "@components/notification";
import { signOutStart, updateUserPerminsion } from "@redux/auth/actions";

import moment from "moment";
import { landingServices } from "@services/landing";
import { useRouter } from "next/router";

moment.locale("es");
const Main = ({ component: Component, pageProps }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const {
    dataUser: { id },
  } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(requestGlobalStart());
  }, []);

  useEffect(() => {
    const socket = io(process.env.NEXT_PUBLIC_URL_API, {
      reconnect: true,
      transports: ["websocket"],
      extraHeaders: {
        header: "Access-Control-Allow-Origin: *",
      },
    });

    socket.on("connect", () => console.log("conneted to Socket"));

    if (id) {
      socket.on(`${id}_notify`, (not) => {
        console.log(not);
        onNewNotifications(not);
      });

      socket.on(`${id}_permission`, (action) => {
        if (action === "update_permission") onUpdatePermision();
        if (action === "delete_permission") onDeletePermision();
      });

      getNotificationCount();
    }

    return () => socket.disconnect();
  }, [id]);

  const onNewNotifications = (not) => {
    notification.info({
      message: not?.title ?? "Sin Titulo",
      description: not?.subject ?? "Sin contenido",
      placement: "topLeft",
      icon: <Notification className="mt-2" />,
    });
    dispatch(increaseUserNotify());
  };

  const onDeletePermision = () => {
    dispatch(signOutStart());
    notification.info({
      message: "Lo sentimos, su cuenta fue eliminada.",
      description: "",
      placement: "topLeft",
      type: "info",
    });

    router.replace("/ingreso");
  };

  const onUpdatePermision = () => {
    dispatch(updateUserPerminsion());
    notification.info({
      message: "Sus permisos fueron actulizados.",
      description: "",
      placement: "topLeft",
      type: "info",
    });

    router.push("/admin/votaciones");
  };

  const getNotificationCount = async () => {
    const count = await landingServices.getNoticationCount(id);
    dispatch(notificationCountSuccess({ client: count }));
  };

  const Layout = Component.Layout ? Component.Layout : React.Fragment;

  return (
    <Layout>
      <ConfigProvider locale={esES}>
        <Component {...pageProps} />
      </ConfigProvider>
    </Layout>
  );
};

export default Main;
