import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { requestUserProfile, changeUserAvatar } from "@redux/users/actions";
import { signOutStart } from "@redux/auth/actions";
import { useRouter } from "next/router";
import SEO from "@components/SEO";
import message from "@components/Message";
import { base64ToFile } from "@utils/fileManager";
import Spin from "@components/Spin";

import Avatar from "@components/Avatar";
import Button from "@components/Button";
import Cropper from "./components/Cropper";
import InputField from "@components/InputField";

import { ArrowLeft, Edit, Warning } from "@assets/icons";

import ChangePassword from "./components/ChangePassword";

import styles from "./User.module.scss";
import confirm from "@components/Modals/confirm";

export default function User({ userId }) {
  const dispatch = useDispatch();
  const router = useRouter();
  const {
    dataUser: { id, avatar, roleMapping, typeDoc },
  } = useSelector((storage) => storage.auth);
  const { loader } = useSelector((state) => state.common);
  const [user, setUser] = useState({});
  const [uri, setUri] = useState("");

  const isUserPage = id === userId;

  useEffect(() => {
    initialRequest();
  }, [isUserPage, avatar]);

  const initialRequest = async () => {
    try {
      const result = await new Promise((resolve, reject) =>
        dispatch(requestUserProfile({ id: userId, resolve, reject }))
      );

      setUser(result);
    } catch (err) {
      console.log(err);
    }
  };

  const handleCropped = async (result) => {
    if (result) {
      const file = base64ToFile(result, "avatar.png");

      try {
        const msg = await new Promise((resolve, reject) =>
          dispatch(
            changeUserAvatar({
              file,
              userId: id,
              resolve,
              reject,
            })
          )
        );
        message.success(msg);
      } catch (error) {
        message.error(error);
      }
    }

    setUri("");
  };

  const handleChange = (ev) => {
    const file = ev.target.files[0];
    const size = (file.size ?? 0) / 1024 / 1024;
    const allowedTypes = ["image/png", "image/jpg", "image/jpeg"];

    if (size > 3) return message.error("Tamaño máximo permitido en 3Mb");
    if (!allowedTypes.includes(file.type))
      return message.error(
        "Solo están permitidos archivos tipo PNG, JPG y JPEG"
      );

    const src = URL.createObjectURL(file);

    setUri(src);
  };

  const handleLogout = async () => {
    const isAdmin = roleMapping === "ADMIN";

    const result = await confirm(
      `¿Seguro(a) que quiere salir de ${
        isAdmin ? "administración" : "sistema"
      }?`,
      true
    );

    if (!result) return;

    dispatch(signOutStart());
    router.replace(isAdmin ? "/ingreso" : "/login");
  };

  return (
    <>
      <SEO title={`Perfil de ${user?.name}`} />
      {isUserPage ? (
        <Spin spinning={loader} tip={`Cargando...`} size="large">
          <div className={styles.user}>
            <section className={styles.user__top}>
              <div className={styles.user__top__content}>
                <Avatar
                  src={user?.avatar}
                  size="large"
                  className={styles.avatar}
                />
                <div>
                  <h6>Mi Perfil</h6>
                  <p>{user?.name}</p>
                </div>
              </div>
              {uri !== "" && (
                <Cropper
                  widthCrop={10}
                  saveCropped={handleCropped}
                  imgurl={uri}
                />
              )}
              <label className={styles.user__custom__input}>
                <input
                  accept="image/*"
                  type="file"
                  onChange={(ev) => handleChange(ev)}
                  multiple={false}
                />
                <Edit /> Editar foto de perfil
              </label>
            </section>
            <section className={styles.user__data}>
              <h6>Mis Datos</h6>
              {roleMapping === "USER" && (
                <p>
                  Propuestas creadas: <span>{user?.proposals}</span>
                </p>
              )}
              <div className={styles.user__data__content}>
                <InputField
                  rightIcon="/images/icon/user-line.svg"
                  type="text"
                  label="Nombre de usuario"
                  defaultValue={user?.username ?? user?.name}
                  disabled={true}
                  style={{ textTransform: "capitalize" }}
                />
                <InputField
                  rightIcon="/images/icon/mail-line.svg"
                  type="email"
                  label="Correo Electrónico"
                  defaultValue={user?.email}
                  disabled={true}
                />
                {typeDoc && (
                  <InputField
                    rightIcon="/images/icon/file-info-line.svg"
                    type="text"
                    label="Tipo de documento"
                    defaultValue={typeDoc}
                    disabled={true}
                  />
                )}
                {roleMapping === "USER" && (
                  <InputField
                    rightIcon="/images/icon/file-user-line.svg"
                    type="text"
                    label="Nro. de Documento"
                    defaultValue={user?.identityCard}
                    disabled={true}
                  />
                )}
              </div>
              {user?.zones?.length > 0 && (
                <div className={styles.user__data__content}>
                  <h6>Mis zonas de interés</h6>
                  {user.zones.map((zone, i) => {
                    return (
                      <InputField
                        key={i}
                        type="text"
                        label={zone.tag}
                        defaultValue={zone.name}
                        disabled={true}
                      />
                    );
                  })}
                </div>
              )}
              <ChangePassword userId={userId} />
            </section>
            <Button
              size="small"
              className={styles.user__button}
              onClick={handleLogout}
            >
              Cerrar sesión
            </Button>
          </div>
        </Spin>
      ) : user ? (
        <Spin spinning={loader} tip={`Cargando...`} size="large">
          <div className={styles.user}>
            <div className={styles.top}>
              <a className={styles.back} onClick={() => router.back()}>
                <ArrowLeft /> Volver
              </a>
            </div>
            <section className={styles.user__top}>
              <div className={styles.user__top__content}>
                <Avatar
                  src={user?.avatar}
                  size="large"
                  className={styles.avatar}
                />
                <div>
                  <h6>Perfil</h6>
                  <p>{user?.name}</p>
                </div>
              </div>
            </section>
            <section className={styles.user__data}>
              <h6>Información general</h6>
              <p>
                Propuestas creadas: <span>{user?.proposals}</span>
              </p>
              <div className={styles.user__data__content}>
                <InputField
                  rightIcon="/images/icon/user-line.svg"
                  type="text"
                  label="Nombre de usuario"
                  defaultValue={user.username ?? user.firstName}
                  disabled={true}
                />
                <InputField
                  rightIcon="/images/icon/file-user-line.svg"
                  type="text"
                  label="Nro. de Documento"
                  defaultValue={user.identityCard ?? "No registrado"}
                  disabled={true}
                />
              </div>
            </section>
          </div>
        </Spin>
      ) : (
        <Spin spinning={loader} tip={`Cargando...`} size="large">
          <div className={styles.user}>
            <div className={styles.top}>
              <a className={styles.back} onClick={() => router.back()}>
                <ArrowLeft /> Volver
              </a>
            </div>
            <p className={styles.warning}>
              <Warning />
              {id
                ? "Usuario no encontrado"
                : "Inicia sesión para ver el perfil"}
            </p>
          </div>
        </Spin>
      )}
    </>
  );
}
