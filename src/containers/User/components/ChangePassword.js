import { useDispatch } from "react-redux";
import SimpleModal from "@components/Modals/SimpleModal";
import Button from "@components/Button";
import { RightArrow } from "@assets/icons";
import { useState } from "react";
import InputField from "@components/InputField";
import message from "@components/Message";

import { changePassowordStart } from "@redux/auth/actions";

const initialState = {
  oldPassword: "",
  newPassword: "",
  confirmPassword: "",
};
const validate = (form = {}) => {
  let allCorrect = true;
  let errors = {};

  for (const key in form) {
    if (!form[key].match(/^.{4,44}/))
      errors[key] = `Este campo debe tener de 4 a 44 caracteres.`;
  }

  if (!errors.confirmPassword && form.confirmPassword !== form.newPassword)
    errors.confirmPassword = "No coincide con la nueva contraseña.";

  for (const key in errors) {
    if (errors[key]) {
      allCorrect = false;
      break;
    }
  }

  if (allCorrect) errors = null;

  return errors;
};

export default function ChangePassword({ userId }) {
  const [visible, setVisible] = useState(false);
  const [form, setForm] = useState(initialState);
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const handleChange = (ev) => {
    setForm({
      ...form,
      [ev.target.name]: ev.target.value,
    });
    setErrors({ ...errors, [ev.target.name]: undefined });
  };

  const handleSubmit = async () => {
    const foundErros = validate(form);

    console.log(foundErros, form);

    if (foundErros) return setErrors(foundErros);

    const readyForm = {
      ...form,
      userId,
    };
    delete readyForm.confirmPassword;

    try {
      setLoading(true);
      const msg = await new Promise((resolve, reject) => {
        dispatch(changePassowordStart({ ...readyForm, resolve, reject }));
      });

      message.success(msg);

      setVisible(false);
      setForm(initialState);
    } catch (err) {
      if (err.message === "406")
        setErrors({ oldPassword: "La contraseña actual es incorrecta." });
      else message.error("Problemas con el servidor.");
    }

    setLoading(false);
  };

  return (
    <>
      <Button
        block
        className="mb-4"
        onClick={setVisible.bind({}, true)}
        type="secondary"
        size="small"
      >
        Cambiar contraseña <RightArrow />
      </Button>
      <SimpleModal onCancel={() => setVisible(false)} visible={visible}>
        <div className="flex flex-col gap-y-3 py-3 px-2">
          <div>
            <InputField
              rightIcon="/images/icon/lock-2-line.svg"
              type="password"
              name="oldPassword"
              label="Contraseña Actual"
              onChange={handleChange}
              defaultValue={form.oldPassword}
              error={!!errors.oldPassword}
            />
            {errors.oldPassword && (
              <p className="error_message">{errors.oldPassword}</p>
            )}
          </div>

          <div>
            <InputField
              rightIcon="/images/icon/lock-2-line.svg"
              type="password"
              name="newPassword"
              label="Nueva Contraseña"
              onChange={handleChange}
              defaultValue={form.newPassword}
              error={!!errors.newPassword}
            />
            {errors.newPassword && (
              <p className="error_message">{errors.newPassword}</p>
            )}
          </div>
          <div>
            <InputField
              rightIcon="/images/icon/lock-2-line.svg"
              type="password"
              name="confirmPassword"
              label="Confirmar Contraseña"
              onChange={handleChange}
              defaultValue={form.confirmPassword}
              error={!!errors.confirmPassword}
            />
            {errors.confirmPassword && (
              <p className="error_message">{errors.confirmPassword}</p>
            )}
          </div>
          <Button
            loading={loading}
            className="mt-3"
            onClick={handleSubmit}
            size="small"
          >
            Cambiar contraseña
          </Button>
        </div>
      </SimpleModal>
    </>
  );
}
