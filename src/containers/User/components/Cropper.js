import { useState, useCallback, useRef } from "react";

import Cropper from "react-easy-crop";

import Modal from "@components/Modals/SimpleModal";
import Button from "@components/Button";

import styles from "./Cropper.module.scss";

let croppedAreaPixelsOut = {};

export const Cropping = ({
  imgurl = "",
  widthCrop = 8,
  saveCropped = () => {},
}) => {
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const canvas = useRef(null);

  const onCropComplete = useCallback((_, croppedAreaPixels) => {
    croppedAreaPixelsOut = croppedAreaPixels;
  }, []);

  const handleZoom = (plus) => {
    if (plus && zoom < 3) setZoom(zoom + 0.1);
    if (!plus && zoom > 1) setZoom(zoom - 0.1);

    if (zoom < 1) setZoom(1);
    if (zoom > 3) setZoom(3);
  };

  const handleSave = () => {
    const node = canvas.current;
    const ctx = node.getContext("2d");

    const image = new Image();
    image.crossOrigin = "Anonymous";
    image.src = imgurl;

    const { width, height, x, y } = croppedAreaPixelsOut;

    image.onload = function () {
      try {
        ctx.drawImage(image, x, y, width, height, 0, 0, 100, 100);

        const imgData = ctx.canvas.toDataURL();

        saveCropped(imgData);
      } catch (e) {
        console.log(e);
      }
    };
  };

  const handleCancel = () => {
    saveCropped(undefined);
  };

  return (
    <Modal visible={true} onCancel={handleCancel}>
      <canvas
        width={100}
        height={100}
        ref={canvas}
        style={{
          display: "none",
        }}
      ></canvas>
      <div className={styles.cropper}>
        <Cropper
          image={imgurl}
          crop={crop}
          zoom={zoom}
          aspect={widthCrop / 10}
          onCropChange={setCrop}
          onCropComplete={onCropComplete}
          onZoomChange={setZoom}
        />
      </div>
      <div className={styles.zoom__buttons}>
        <Button
          onClick={handleZoom.bind({}, true)}
          className={styles.custom__buttons}
          shape="circle"
          size="x-small"
        >
          +
        </Button>
        <Button
          onClick={handleZoom.bind({}, false)}
          className={styles.custom__buttons}
          type="primary"
          shape="circle"
          size="x-small"
        >
          −
        </Button>
      </div>

      <div className={styles.section__buttons}>
        <Button type="secondary" size="small" onClick={handleCancel}>
          Cancelar
        </Button>
        <Button size="small" onClick={handleSave}>
          Guardar
        </Button>
      </div>
    </Modal>
  );
};

export default Cropping;
