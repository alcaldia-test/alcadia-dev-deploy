import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import Image from "next/image";
import SEO from "@components/SEO";

import { getCountVotes, getVotes, changeFilter } from "@redux/voting/actions";

import Autocomplete from "@components/Autocomplete";
import Banner from "@components/Banner";
import Button from "@components/Button";
import CardVoting from "@components/Cards/Voting";
import Pagination from "@components/Pagination";
import Select from "@components/Select";
import Share from "@components/Modals/Share";

import styles from "./Voting.module.scss";

const baseSelectOptions = [
  { value: "active", label: "Activas" },
  { value: "finished", label: "Finalizadas" },
  { value: "DESC", label: "Más Recientes" },
  { value: "ASC", label: "Más Antiguas" },
];

export default function Voting() {
  const dispatch = useDispatch();
  const { dataUser } = useSelector((storage) => storage.auth);
  const { votes, count, loadingVotes, filters, total } = useSelector(
    (state) => state.voting
  );
  const { banners } = useSelector((storage) => storage.global);

  const [banner, setBanner] = useState("");
  const [modalOpen, setModalOpen] = useState(false);
  const [modalURL, setModalURL] = useState("");
  const [modalPostsIdShare, setModalPostsIdShare] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [filter, setFilter] = useState(
    filters?.status
      ? filters?.status
      : filters?.order
      ? filters?.order
      : filters?.zones || filters?.macros
      ? "zones"
      : "DESC"
  );
  const [optionsSelect, setOptionsSelect] = useState([...baseSelectOptions]);
  const [optionsAutocomplete, setOptionsAutocomplete] = useState([]);
  const [filterAutocomplete, setFilterAutocomplete] = useState(
    filters?.like || undefined
  );

  useEffect(() => {
    const filterObj = {
      limit: 12,
      skip: (currentPage - 1) * 12,
      like: filterAutocomplete,
    };

    if (filter === "active" || filter === "finished") {
      filterObj.status = filter;
    } else if (filter === "DESC" || filter === "ASC") {
      filterObj.order = filter;
    } else if (filter === "zones") {
      const favorites = dataUser.favorites;

      if (favorites?.length > 0) {
        // filterObj.zones = favorites.map((favorite) => favorite.zoneId);
        filterObj.macros = favorites.map((favorite) => favorite.macroId);
      }
    }

    dispatch(changeFilter(filterObj));
  }, [filter, filterAutocomplete, currentPage]);

  useEffect(() => {
    dispatch(getVotes(filters));
    dispatch(getCountVotes());
  }, [filters]);

  useEffect(() => {
    if (count > 12 && currentPage > count / 12) {
      setCurrentPage(Math.ceil(count / 12));
    } else if (count <= 12) {
      setCurrentPage(1);
    }
  }, [count]);

  useEffect(() => {
    if (banners.length > 0) {
      const bannerObj = banners.find((banner) => banner.key === "voting");
      setBanner(bannerObj?.fileStorages[0]?.link);
    }
  }, [banners]);

  useEffect(() => {
    setOptionsAutocomplete(
      votes?.map((item) => ({
        title: item.title,
      })) || []
    );
    if (dataUser.id) {
      setOptionsSelect([
        ...baseSelectOptions,
        { value: "zones", label: "Mis Zonas" },
      ]);
    }
  }, [votes]);

  return (
    <>
      <SEO image={banner} />
      <Banner
        title="Tu Opinión Cuenta"
        subtitle="¡Bienvenido!"
        image={banner}
        objectFit="cover"
      />
      <section className={styles.filter__container}>
        <div className={styles.filters}>
          <Select
            options={optionsSelect}
            placeholder="Filtrar..."
            onChange={(e) => setFilter(e[0]?.value)}
            className={styles.select}
            defaultValue={
              optionsSelect.findIndex((option) => option.value === filter) > -1
                ? optionsSelect.findIndex((option) => option.value === filter)
                : "DESC"
            }
          />
          <Autocomplete
            dataSource={optionsAutocomplete}
            placeholder="Buscar..."
            onChange={(e) => setFilterAutocomplete(e)}
            defaultValue={filterAutocomplete}
          />
        </div>
        <div className={styles.filter__content}>
          {loadingVotes ? (
            <div className={styles.loading}>
              <Image
                src="/gifts/Spin-1s-200px.gif"
                width={50}
                height={50}
                objectFit="contain"
              />
            </div>
          ) : votes?.length > 0 ? (
            votes.map(
              ({ title, content, postsId, dueDate, fileLink, areas, id }) => (
                <Link
                  key={id}
                  href="/tu_voto_cuenta/[id]"
                  as={`/tu_voto_cuenta/${id}`}
                >
                  <a>
                    <CardVoting
                      postsId={postsId}
                      dueDate={dueDate}
                      content={content}
                      image={fileLink}
                      title={title}
                      zones={areas}
                      id={id}
                    >
                      <Button type="primary" shape="round" size="small">
                        Ingresa y participa
                      </Button>
                      <Button
                        type="secondary"
                        shape="round"
                        size="small"
                        onClick={(e) => {
                          e.preventDefault();
                          setModalURL(
                            `${location.origin}/tu_voto_cuenta/${id}`
                          );
                          setModalOpen(true);
                          setModalPostsIdShare(postsId);
                        }}
                      >
                        Comparte
                      </Button>
                    </CardVoting>
                  </a>
                </Link>
              )
            )
          ) : total > 0 ? (
            <div className={styles.no__results}>
              No se encontraron resultados para: {`"${filterAutocomplete}"`}
            </div>
          ) : (
            <div className={styles.no__results}>
              Aún no se han creado votaciones.
            </div>
          )}
        </div>
        <Pagination
          defaultPageSize={12}
          total={count}
          current={currentPage}
          onChange={(page) => {
            setCurrentPage(page);
          }}
        />
      </section>
      <Share
        visible={modalOpen}
        href={modalURL}
        onCancel={() => setModalOpen(false)}
        closable={false}
        cancelText="Cancelar"
        userId={dataUser.id}
        postsId={modalPostsIdShare}
      />
    </>
  );
}
