import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import draftToHtml from "draftjs-to-html";
import Link from "next/link";
import Image from "next/image";
import { getWorldTime } from "@utils/request";

import { postVote } from "@redux/voting/actions";
import { useFormateDate, useDateIsAfter } from "@utils/dateHooks";
import { validateIfUserIsInZone } from "@utils/zoneValidator";

import Button from "@components/Button";
import IconGroup from "@components/SocialNetworks/IconGroup";
import Modal from "@components/Modals/SimpleModal";
import Spin from "@components/Spin";
import SuccessModal from "@components/Modals/SuccessModal";
import VotingOption from "@components/VotingOption";
import ZonesModal from "@components/Modals/ZonesModal/MacrosModal";

import { ArrowLeft, Check, Download, Warning } from "@assets/icons";
import styles from "./Vote.module.scss";
import ListRelatedContent from "@components/ListRelatedContent";

let hrefLocal = "";
export default function Vote({ id, vote }) {
  const dispatch = useDispatch();
  const { loadingVotes } = useSelector((state) => state.voting);
  const { dataUser } = useSelector((state) => state.auth);
  const { loader } = useSelector((state) => state.common);

  const [winner, setWinner] = useState(0);
  const [totalVotes, setTotalVotes] = useState(0);
  const [description, setDescription] = useState({});

  // Modals
  const [showInfoModal, setShowInfoModal] = useState(false);
  const [titleInfoModal, setTitleInfoModal] = useState("");
  const [contentInfoModal, setContentInfoModal] = useState("");
  const [fileLinkInfoModal, setFileLinkInfoModal] = useState("");
  const [attachedInfoModal, setAttachedInfoModal] = useState("");
  const [showZonesModal, setShowZonesModal] = useState(false);
  const [isFinished, setIsFinished] = useState(false);

  const [hasVoted, setHasVoted] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);

  const isInVotingZone = validateIfUserIsInZone({
    favorites: dataUser?.favorites,
    zones: vote?.zones,
    macros: vote?.macros,
  });
  const [zoneRelation, setZoneRelation] = useState(isInVotingZone);

  const endDate = useFormateDate(vote?.dueDate);

  useEffect(() => {
    if (!vote?.content) return;

    try {
      const description = JSON.parse(vote?.content);
      setDescription(description);
    } catch (error) {
      setDescription(vote?.content);
    }

    if (vote?.proposals.length > 0) {
      const winner = vote.proposals.reduce((a, b) =>
        a.votes > b.votes ? a : b
      );
      const total = vote.proposals.reduce((a, b) => a + b.votes, 0);
      setTotalVotes(total);
      setWinner(winner.votes);
    }

    setWorldDate();

    if (window) hrefLocal = window.location.href;
  }, []);

  const setWorldDate = async () => {
    const worldDate = await getWorldTime();

    const finished = useDateIsAfter(vote?.dueDate, worldDate);

    setIsFinished(finished);
  };

  useEffect(() => {
    if (localStorage.getItem(`${id}_${dataUser.id}`) === "NO") {
      setZoneRelation(false);
    } else {
      setZoneRelation(true);
    }

    console.log(vote);
  }, [dataUser]);

  const voteProposal = async (payload) => {
    try {
      await new Promise((resolve, reject) =>
        dispatch(postVote({ ...payload, resolve, reject }))
      );

      setHasVoted(true);
      setShowSuccessModal(true);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <Spin spinning={!!loader} size="large">
        <div className={styles.container}>
          {loadingVotes ? (
            <div className={styles.loading}>
              <Image
                src="/gifts/Spin-1s-200px.gif"
                width={50}
                height={50}
                objectFit="contain"
              />
            </div>
          ) : (
            <>
              <div className={styles.top}>
                <Link href="/tu_voto_cuenta" as={`/tu_voto_cuenta`}>
                  <a className={styles.back}>
                    <ArrowLeft /> Otras Votaciones
                  </a>
                </Link>
                <div
                  className={styles.zone}
                  onClick={() => setShowZonesModal(true)}
                >
                  Ver zonas
                </div>
              </div>
              <div className={styles.content}>
                <div className={styles.title}>{vote?.title}</div>
                <div className={styles.info}>
                  <IconGroup
                    href={hrefLocal}
                    userId={dataUser.id}
                    postsId={vote?.postsId}
                  />
                  {isFinished ? (
                    <p className={styles.finished_date}>
                      <Check />
                      La votación finalizó el {endDate}
                    </p>
                  ) : (
                    <p className={styles.finish_date}>
                      La votación finaliza el {endDate}
                    </p>
                  )}
                </div>
                {dataUser.id && !isInVotingZone && !isFinished && vote.zones && (
                  <p className={styles.not_in_zone}>
                    <Warning /> Usted no pertenece a{" "}
                    {vote.zones.length === 1
                      ? "esta zona"
                      : "ninguna de estas zonas"}
                  </p>
                )}
                {(vote?.alreadyVoted || hasVoted) && (
                  <p className={styles.already_voted}>
                    <Check /> Ya participaste en esta votación
                  </p>
                )}
                {vote?.fileLink && (
                  <img
                    src={vote ? vote.fileLink : "/images/banner.jpg"}
                    alt={vote?.title}
                    className={styles.img}
                  />
                )}
                <div
                  className={styles.description}
                  dangerouslySetInnerHTML={{
                    __html: draftToHtml(description) || description,
                  }}
                />
                {vote?.attached && (
                  <Button
                    type="primary"
                    size="small"
                    href={`${vote?.attached[0]}?name=${vote?.attached[1]}`}
                    target="_blank"
                    className={styles.button_download}
                  >
                    Descargar Documento
                    <Download />
                  </Button>
                )}
                <h6 className={styles.question}>¿{vote?.question}?</h6>
                {isFinished && (
                  <h5 className={styles.winner}>
                    {vote?.proposals.filter(
                      (proposal) => proposal.votes === winner
                    ).length === 1 && totalVotes > 0
                      ? "Ganó la opción: "
                      : vote?.proposals.filter(
                          (proposal) => proposal.votes === winner
                        ).length > 1 &&
                        totalVotes > 0 &&
                        "Ganaron las opciones: "}
                    {totalVotes > 0 &&
                      vote?.proposals
                        .filter((proposal) => proposal.votes === winner)
                        .map((proposal, i) => {
                          const filterLength = vote?.proposals.filter(
                            (proposal) => proposal.votes === winner
                          ).length;
                          const index = vote?.proposals.indexOf(proposal);
                          return (
                            <span key={index}>
                              {index + 1}
                              {filterLength - 1 === i
                                ? "."
                                : i === filterLength - 2
                                ? " y "
                                : ", "}
                            </span>
                          );
                        })}
                  </h5>
                )}
                {!dataUser.id && !isFinished && (
                  <p className={styles.not_logged}>
                    Necesita registrarse o iniciar sesión para votar
                  </p>
                )}
                {vote?.proposals && vote.proposals.length > 0 && (
                  <div className={styles.proposals}>
                    {vote.proposals.map((proposal, i) => {
                      let proposalDescription = "";

                      try {
                        const obj = JSON.parse(proposal.content);
                        const text = obj?.blocks?.reduce(
                          (acc, curr) => acc + curr.text,
                          ""
                        );
                        if (text) {
                          proposalDescription = text;
                        } else {
                          proposalDescription =
                            "Esta opción no tiene descripción";
                        }
                      } catch (error) {
                        proposalDescription = proposal.content;
                      }

                      return (
                        <VotingOption
                          key={proposal.id}
                          id={proposal.id}
                          votingId={id}
                          title={proposal.title}
                          content={proposal.content}
                          fileLink={proposal.fileLink}
                          votes={proposal.votes}
                          totalVotes={totalVotes}
                          isFinished={isFinished}
                          userId={dataUser.id}
                          index={i}
                          onVote={voteProposal}
                          isWinner={winner > 0 && proposal.votes === winner}
                          alreadyVoted={vote.alreadyVoted || hasVoted}
                          isInVotingZone={isInVotingZone}
                          macros={vote?.macros}
                          zones={vote?.zones}
                          zoneRelation={zoneRelation}
                          setZoneRelation={setZoneRelation}
                          openModal={() => {
                            setShowInfoModal(true);
                            setTitleInfoModal(proposal.title);
                            setContentInfoModal(proposalDescription);
                            setFileLinkInfoModal(proposal.fileLink);
                            setAttachedInfoModal(proposal.attached);
                          }}
                        />
                      );
                    })}
                  </div>
                )}
                {isFinished && (
                  <Link
                    href="/tu_voto_cuenta/[id]/estadisticas"
                    as={`/tu_voto_cuenta/${id}/estadisticas`}
                  >
                    <a className={styles.stats}>
                      <Button size="medium" type="primary">
                        Ver otras estadisticas
                      </Button>
                    </a>
                  </Link>
                )}
                {!isFinished && !zoneRelation && (
                  <h6 className={styles.zone_relation}>
                    Gracias por tu interés, Podrás ver los resultados el{" "}
                    {endDate}
                  </h6>
                )}
                <h5 className={styles.invitation}>
                  {isFinished
                    ? "¡Comparte estos resultados con los demás!"
                    : "¡Invitemos a más personas a votar!"}
                </h5>
                <div className={styles.share}>
                  <IconGroup
                    href={hrefLocal}
                    userId={dataUser.id}
                    postsId={vote?.postsId}
                  />
                </div>
              </div>
              <ListRelatedContent post={vote} module="tu_voto_cuenta" />
            </>
          )}
          <Modal
            visible={showInfoModal}
            onCancel={() => setShowInfoModal(false)}
          >
            <div className={styles.modal_title}>{titleInfoModal}</div>
            {fileLinkInfoModal && (
              <img
                src={fileLinkInfoModal}
                alt={titleInfoModal}
                className={styles.modal_img}
              />
            )}
            <div className={styles.modal_description}>{contentInfoModal}</div>
            {attachedInfoModal && (
              <Button
                type="primary"
                size="small"
                href={`${attachedInfoModal[0]}?name=${attachedInfoModal[1]}`}
                className={styles.button_download}
                block={true}
              >
                Descargar Documento
                <Download />
              </Button>
            )}
          </Modal>
        </div>
      </Spin>
      <SuccessModal
        visible={showSuccessModal}
        onCancel={() => setShowSuccessModal(false)}
        cancelText={"Cerrar"}
      >
        <div className={styles.success_modal}>
          <Check />
          <h6>¡Excelente!, Se registró su voto</h6>
        </div>
      </SuccessModal>
      {showZonesModal && (
        <ZonesModal
          closable={false}
          onCancel={() => setShowZonesModal(false)}
          macros={vote?.macros}
        />
      )}
    </>
  );
}
