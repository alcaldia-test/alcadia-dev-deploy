import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Image from "next/image";
import Link from "next/link";
import SEO from "@components/SEO";

import CardStats from "@components/CardStats";

import { getVote, getStatistics } from "@redux/voting/actions";

import { ArrowLeft, Like, Warning } from "@assets/icons";

import styles from "./Statistics.module.scss";

export default function Statistics({ id }) {
  const dispatch = useDispatch();
  const { vote, loadingVotes, statistics } = useSelector(
    (state) => state.voting
  );
  const { dataUser } = useSelector((storage) => storage.auth);
  const [totalVotes, setTotalVotes] = useState(0);

  useEffect(() => {
    dispatch(getVote({ id, userId: dataUser.id }));
    dispatch(getStatistics(id));
  }, []);

  useEffect(() => {
    if (statistics?.votes) {
      setTotalVotes(statistics.votes.reduce((a, b) => a + b.cant, 0));
    }
  }, [statistics]);

  return (
    <>
      <SEO
        title={vote?.title}
        description={`Estadísticas de la votación: ${vote?.title}`}
        image={vote?.fileLink}
      />
      <div className={styles.container}>
        {loadingVotes ? (
          <div className={styles.loading}>
            <Image
              src="/gifts/Spin-1s-200px.gif"
              width={50}
              height={50}
              objectFit="contain"
            />
          </div>
        ) : statistics ? (
          <>
            <Link href="/tu_voto_cuenta" as={`/tu_voto_cuenta`}>
              <a className={styles.back}>
                <ArrowLeft /> Volver
              </a>
            </Link>
            <div className={styles.content}>
              <h1>Otras estadísticas</h1>
              <section className={styles.votes}>
                <h2>Votos</h2>
                <div className={styles.stats_container}>
                  {statistics?.votes?.length > 0 &&
                    statistics?.votes
                      .sort((a, b) => b.cant - a.cant)
                      .map((statistic) => {
                        let percentage = 0;
                        if (statistic.cant > 0) {
                          percentage = Number.isInteger(
                            (statistic.cant / totalVotes) * 100
                          )
                            ? (statistic.cant / totalVotes) * 100
                            : ((statistic.cant / totalVotes) * 100).toFixed(2);
                        }

                        return (
                          <CardStats
                            key={statistic.id}
                            value={`${percentage}% - ${statistic.cant} votos`}
                            detail={statistic.title}
                            svg={<Like />}
                          />
                        );
                      })}
                </div>
              </section>
              <section>
                <h2>Tipos de participantes</h2>
                <div className={styles.stats_container}>
                  {statistics?.favorites &&
                    Object.entries(statistics?.favorites).map((favorite, i) => (
                      <CardStats
                        key={i}
                        value={`${favorite[1]} participantes`}
                        detail={
                          favorite[0] === "BUSINESS"
                            ? "Con interés de negocio"
                            : favorite[0] === "FAMILY"
                            ? "Con interés de familiares"
                            : favorite[0] === "HOME"
                            ? "Con interés de domicilio"
                            : ""
                        }
                      />
                    ))}
                </div>
              </section>
              <section className={styles.shared}>
                <h2>Compartidas</h2>
                <div className={styles.stats_container}>
                  {statistics?.shareds &&
                    Object.entries(statistics?.shareds).map((shared, i) => (
                      <CardStats
                        key={i}
                        value={`${shared[1]} compartidas`}
                        detail={
                          shared[0].charAt(0).toUpperCase() +
                          shared[0].slice(1).toLowerCase()
                        }
                        icon={`/icons/${shared[0].toLowerCase()}.svg`}
                      />
                    ))}
                </div>
              </section>
            </div>
          </>
        ) : (
          <>
            <Link href="/tu_voto_cuenta" as={`/tu_voto_cuenta`}>
              <a className={styles.back}>
                <ArrowLeft /> Volver
              </a>
            </Link>
            <div className={styles.content}>
              <h1>Otras estadísticas</h1>
              <p className={styles.warning}>
                <Warning /> Necesitas iniciar sesión para ver las estadísticas
              </p>
            </div>
          </>
        )}
      </div>
    </>
  );
}
