import React, { useEffect } from "react";
import LoadingComponent from "@components/LoadingComponent";
import { batch, useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { signOutSuccess } from "@redux/actions";
import { requestGlobalSuccess } from "@redux/actions/global";

export const Logout = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    cerrarSesion();
  }, []);

  const cerrarSesion = async () => {
    batch(() => {
      dispatch(signOutSuccess());
      dispatch(requestGlobalSuccess({ finished: false }));
    });
    setTimeout(() => {
      router.replace("/login");
    }, 1000);
  };

  return <LoadingComponent title="Estamos cerrando su sesión..." />;
};

export default Logout;
