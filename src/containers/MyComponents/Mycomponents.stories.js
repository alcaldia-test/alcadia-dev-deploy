import React from "react";
import { withDesign } from "storybook-addon-designs";

import MyComponents from "./index.js";

const urlFigmaComponents =
  "https://www.figma.com/file/32ucYejuXOWDWf0wuOdRzs/Alcald%C3%ADa-%2F-3.-Components?node-id=";
const idUrlButtons = {
  link: "16%3A6242",
  primary: "16%3A6216",
  secondary: "16%3A6227",
};

export default {
  title: "Containers/Mis Componentes",
  component: MyComponents,
  decorators: [withDesign],
};

const Template = (args) => <MyComponents {...args} />;

export const Default = Template.bind({});
Default.args = {};
Default.parameters = {
  design: {
    type: "figma",
    url: `${urlFigmaComponents}${idUrlButtons.primary}`,
  },
};
