import Avatar from "@components/Avatar";
import Button from "@components/Button";
import { CarouselCard } from "@components/Cards";
import Carousel from "@components/Carousel";
import Modal, { IconTextModal, TextAreaModal } from "@components/Modals";
import React, { useState } from "react";

const contentCarousel = (
  <div style={{ width: "100%", height: "100%", fontWeight: "normal" }}>
    <div style={{ width: "100%", height: "50%" }}>
      <div
        style={{
          width: "100%",
          height: "100%",
          background:
            "linear-gradient(180.1deg, rgba(163, 169, 175, 0) 0.09%, #1E1E1E 110.88%)",
        }}
      ></div>
    </div>
    <div style={{ width: "100%", height: "50%", padding: "4rem" }}>
      <div style={{ fontSize: "14rem" }} className="old-primary-red">
        Categoria o zona
      </div>
      <div style={{ fontSize: "16rem" }}>Este es un texto de pruebas</div>
    </div>
  </div>
);

const item1Carousel = {
  image: (
    <div
      style={{
        width: "100%",
        height: "100%",
        background:
          "linear-gradient(180.1deg, lightblue 0.09%, #1E1E1E 110.88%)",
      }}
    ></div>
  ),
  title: "Categoria o zona",
  description: "Este es un texto de pruebas",
};
const item2Carousel = {
  image: (
    <div
      style={{
        width: "100%",
        height: "100%",
        background: "linear-gradient(180.1deg, coral 0.09%, #1E1E1E 110.88%)",
      }}
    ></div>
  ),
  title: "Categoria o zona",
  description: "Este es un texto de pruebas",
};
const item3Carousel = {
  image: (
    <div
      style={{
        width: "100%",
        height: "100%",
        background: "linear-gradient(180.1deg, pink 0.09%, #1E1E1E 110.88%)",
      }}
    ></div>
  ),
  title: "Categoria o zona",
  description: "Este es un texto de pruebas",
};

const MyComponents = function ({ ...props }) {
  const [isModalVisible, setIsModalVisible] = useState(true);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div {...props}>
      <div>
        <div>Avatar</div>
        <div>
          <Avatar src="/images/avatar.png"></Avatar>
        </div>
      </div>
      <div>
        <div>Button</div>
        <div>
          <Button type="primary" size="large">
            Button Primario Large
          </Button>
          <Button type="secondary" size="medium">
            Button Secundario Medium
          </Button>
          <Button type="link" size="small">
            Button Link Small
          </Button>
        </div>
      </div>
      <div>
        <div>Cards</div>
        <div>
          <div>Card de Carousel</div>
          <CarouselCard>{contentCarousel}</CarouselCard>
        </div>
      </div>
      <div>
        <div>Carousel</div>
        <div>
          <div>Carousel Base</div>
          <Carousel
            items={[
              <img key="1" src="/images/banner.jpg" />,
              <img key="2" src="/images/banner.jpg" />,
            ]}
          ></Carousel>
          <div>Carousel de Logros</div>
          <Carousel
            items={[item1Carousel, item2Carousel, item3Carousel, item1Carousel]}
          ></Carousel>
        </div>
      </div>
      <div>
        <div>Modales</div>
        <div>
          <Button onClick={showModal}>PRESIONA PARA ABRIR LOS MODALES</Button>
          <div>Modal Base</div>
          <Modal
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            closable={false}
            cancelText="Todo mal"
            okText="Todo fino"
          >
            Esto es un modal Base
          </Modal>
          <div>Modal Icono y Texto</div>
          <IconTextModal
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            closable={false}
            image="/images/banner.jpg"
            title="Titulo"
            description="Descripcion"
          ></IconTextModal>
          <div>Modal Text Area</div>
          <TextAreaModal
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            closable={false}
            cancelText="Cerrar"
            okText="Bloquear"
            placeholder="Contenido"
            text="Motivo"
            title="Motivo del Bloqueo a Usuario"
          ></TextAreaModal>
        </div>
      </div>
    </div>
  );
};

export default MyComponents;
